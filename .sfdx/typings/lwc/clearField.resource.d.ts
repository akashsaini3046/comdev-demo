declare module "@salesforce/resourceUrl/clearField" {
    var clearField: string;
    export default clearField;
}