declare module "@salesforce/label/c.ACCOUNT_HAS_ASSOCIATED_BUSINESS_LOCATION" {
    var ACCOUNT_HAS_ASSOCIATED_BUSINESS_LOCATION: string;
    export default ACCOUNT_HAS_ASSOCIATED_BUSINESS_LOCATION;
}
declare module "@salesforce/label/c.ACCOUNT_HAS_ASSOCIATED_BUSINESS_LOCATION_AND_CONTACT" {
    var ACCOUNT_HAS_ASSOCIATED_BUSINESS_LOCATION_AND_CONTACT: string;
    export default ACCOUNT_HAS_ASSOCIATED_BUSINESS_LOCATION_AND_CONTACT;
}
declare module "@salesforce/label/c.ACCOUNT_HAS_ASSOCIATED_CONTACTS" {
    var ACCOUNT_HAS_ASSOCIATED_CONTACTS: string;
    export default ACCOUNT_HAS_ASSOCIATED_CONTACTS;
}
declare module "@salesforce/label/c.ACCOUNT_HAS_ASSOCIATED_OPPORTUNITIES" {
    var ACCOUNT_HAS_ASSOCIATED_OPPORTUNITIES: string;
    export default ACCOUNT_HAS_ASSOCIATED_OPPORTUNITIES;
}
declare module "@salesforce/label/c.ACCOUNT_NAME" {
    var ACCOUNT_NAME: string;
    export default ACCOUNT_NAME;
}
declare module "@salesforce/label/c.AD_ACCOUNT_OWNER_S" {
    var AD_ACCOUNT_OWNER_S: string;
    export default AD_ACCOUNT_OWNER_S;
}
declare module "@salesforce/label/c.AD_ACCOUNT_S" {
    var AD_ACCOUNT_S: string;
    export default AD_ACCOUNT_S;
}
declare module "@salesforce/label/c.AD_AMOUNT_CONVERTER" {
    var AD_AMOUNT_CONVERTER: string;
    export default AD_AMOUNT_CONVERTER;
}
declare module "@salesforce/label/c.AD_LABEL_ALL_ACCOUNTS" {
    var AD_LABEL_ALL_ACCOUNTS: string;
    export default AD_LABEL_ALL_ACCOUNTS;
}
declare module "@salesforce/label/c.AD_LABEL_ASSIGNED_TO" {
    var AD_LABEL_ASSIGNED_TO: string;
    export default AD_LABEL_ASSIGNED_TO;
}
declare module "@salesforce/label/c.AD_LABEL_BUTTON_APPLY" {
    var AD_LABEL_BUTTON_APPLY: string;
    export default AD_LABEL_BUTTON_APPLY;
}
declare module "@salesforce/label/c.AD_LABEL_BUTTON_FILTER_BY" {
    var AD_LABEL_BUTTON_FILTER_BY: string;
    export default AD_LABEL_BUTTON_FILTER_BY;
}
declare module "@salesforce/label/c.AD_LABEL_BUTTON_RESET" {
    var AD_LABEL_BUTTON_RESET: string;
    export default AD_LABEL_BUTTON_RESET;
}
declare module "@salesforce/label/c.AD_LABEL_CLOSE" {
    var AD_LABEL_CLOSE: string;
    export default AD_LABEL_CLOSE;
}
declare module "@salesforce/label/c.AD_LABEL_CLOSED" {
    var AD_LABEL_CLOSED: string;
    export default AD_LABEL_CLOSED;
}
declare module "@salesforce/label/c.AD_LABEL_CREATED_DATE" {
    var AD_LABEL_CREATED_DATE: string;
    export default AD_LABEL_CREATED_DATE;
}
declare module "@salesforce/label/c.AD_LABEL_CREATED_DATE_TIME" {
    var AD_LABEL_CREATED_DATE_TIME: string;
    export default AD_LABEL_CREATED_DATE_TIME;
}
declare module "@salesforce/label/c.AD_LABEL_CURRENT_YEAR" {
    var AD_LABEL_CURRENT_YEAR: string;
    export default AD_LABEL_CURRENT_YEAR;
}
declare module "@salesforce/label/c.AD_LABEL_DUE_DATE_TIME" {
    var AD_LABEL_DUE_DATE_TIME: string;
    export default AD_LABEL_DUE_DATE_TIME;
}
declare module "@salesforce/label/c.AD_LABEL_ERROR" {
    var AD_LABEL_ERROR: string;
    export default AD_LABEL_ERROR;
}
declare module "@salesforce/label/c.AD_LABEL_LOADING" {
    var AD_LABEL_LOADING: string;
    export default AD_LABEL_LOADING;
}
declare module "@salesforce/label/c.AD_LABEL_MULTISELECT_LOOKUP_ASSIGNED_TO" {
    var AD_LABEL_MULTISELECT_LOOKUP_ASSIGNED_TO: string;
    export default AD_LABEL_MULTISELECT_LOOKUP_ASSIGNED_TO;
}
declare module "@salesforce/label/c.AD_LABEL_MULTISELECT_LOOKUP_OWNER_NAME" {
    var AD_LABEL_MULTISELECT_LOOKUP_OWNER_NAME: string;
    export default AD_LABEL_MULTISELECT_LOOKUP_OWNER_NAME;
}
declare module "@salesforce/label/c.AD_LABEL_OPEN" {
    var AD_LABEL_OPEN: string;
    export default AD_LABEL_OPEN;
}
declare module "@salesforce/label/c.AD_LABEL_OPEN_REPORT" {
    var AD_LABEL_OPEN_REPORT: string;
    export default AD_LABEL_OPEN_REPORT;
}
declare module "@salesforce/label/c.AD_LABEL_OPPORTUNITY_PIPELINE" {
    var AD_LABEL_OPPORTUNITY_PIPELINE: string;
    export default AD_LABEL_OPPORTUNITY_PIPELINE;
}
declare module "@salesforce/label/c.AD_LABEL_OPPORTUNITY_SIZE" {
    var AD_LABEL_OPPORTUNITY_SIZE: string;
    export default AD_LABEL_OPPORTUNITY_SIZE;
}
declare module "@salesforce/label/c.AD_LABEL_OPPORTUNITY_SIZE_IS_NOT_A_NUMBER" {
    var AD_LABEL_OPPORTUNITY_SIZE_IS_NOT_A_NUMBER: string;
    export default AD_LABEL_OPPORTUNITY_SIZE_IS_NOT_A_NUMBER;
}
declare module "@salesforce/label/c.AD_LABEL_OVERDUE" {
    var AD_LABEL_OVERDUE: string;
    export default AD_LABEL_OVERDUE;
}
declare module "@salesforce/label/c.AD_LABEL_PAGINATION_FIRST" {
    var AD_LABEL_PAGINATION_FIRST: string;
    export default AD_LABEL_PAGINATION_FIRST;
}
declare module "@salesforce/label/c.AD_LABEL_PAGINATION_LAST" {
    var AD_LABEL_PAGINATION_LAST: string;
    export default AD_LABEL_PAGINATION_LAST;
}
declare module "@salesforce/label/c.AD_LABEL_SORT_BY_CREATED_DATE" {
    var AD_LABEL_SORT_BY_CREATED_DATE: string;
    export default AD_LABEL_SORT_BY_CREATED_DATE;
}
declare module "@salesforce/label/c.AD_LABEL_SORT_BY_DUE_DATE" {
    var AD_LABEL_SORT_BY_DUE_DATE: string;
    export default AD_LABEL_SORT_BY_DUE_DATE;
}
declare module "@salesforce/label/c.AD_LABEL_STATUS" {
    var AD_LABEL_STATUS: string;
    export default AD_LABEL_STATUS;
}
declare module "@salesforce/label/c.AD_LABEL_VALUES_ARE_IN" {
    var AD_LABEL_VALUES_ARE_IN: string;
    export default AD_LABEL_VALUES_ARE_IN;
}
declare module "@salesforce/label/c.AD_LABEL_VIEW_ALL_NOTES" {
    var AD_LABEL_VIEW_ALL_NOTES: string;
    export default AD_LABEL_VIEW_ALL_NOTES;
}
declare module "@salesforce/label/c.AD_LABEL_VIEW_ALL_TASKS" {
    var AD_LABEL_VIEW_ALL_TASKS: string;
    export default AD_LABEL_VIEW_ALL_TASKS;
}
declare module "@salesforce/label/c.AD_LABEL_YEAR" {
    var AD_LABEL_YEAR: string;
    export default AD_LABEL_YEAR;
}
declare module "@salesforce/label/c.AD_MESSAGE_NO_ACTIVE_USER_EXIST" {
    var AD_MESSAGE_NO_ACTIVE_USER_EXIST: string;
    export default AD_MESSAGE_NO_ACTIVE_USER_EXIST;
}
declare module "@salesforce/label/c.AD_MESSAGE_NO_CHILD_ACCOUNT_EXIST" {
    var AD_MESSAGE_NO_CHILD_ACCOUNT_EXIST: string;
    export default AD_MESSAGE_NO_CHILD_ACCOUNT_EXIST;
}
declare module "@salesforce/label/c.AD_MESSAGE_NO_RECORDS_FOUND" {
    var AD_MESSAGE_NO_RECORDS_FOUND: string;
    export default AD_MESSAGE_NO_RECORDS_FOUND;
}
declare module "@salesforce/label/c.AD_MESSAGE_UNAPPLIED_FILTER" {
    var AD_MESSAGE_UNAPPLIED_FILTER: string;
    export default AD_MESSAGE_UNAPPLIED_FILTER;
}
declare module "@salesforce/label/c.AD_NEXT_HOW_MANY_YEAR" {
    var AD_NEXT_HOW_MANY_YEAR: string;
    export default AD_NEXT_HOW_MANY_YEAR;
}
declare module "@salesforce/label/c.AD_OPP_REPORT_ID" {
    var AD_OPP_REPORT_ID: string;
    export default AD_OPP_REPORT_ID;
}
declare module "@salesforce/label/c.AD_PARENT_ACCOUT_APPEND_WITH" {
    var AD_PARENT_ACCOUT_APPEND_WITH: string;
    export default AD_PARENT_ACCOUT_APPEND_WITH;
}
declare module "@salesforce/label/c.AD_PICKLIST_VAL_ALL_ACCOUNTS" {
    var AD_PICKLIST_VAL_ALL_ACCOUNTS: string;
    export default AD_PICKLIST_VAL_ALL_ACCOUNTS;
}
declare module "@salesforce/label/c.AD_PICKLIST_VAL_ALL_YEAR" {
    var AD_PICKLIST_VAL_ALL_YEAR: string;
    export default AD_PICKLIST_VAL_ALL_YEAR;
}
declare module "@salesforce/label/c.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME" {
    var AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME: string;
    export default AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
}
declare module "@salesforce/label/c.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_OWNER" {
    var AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_OWNER: string;
    export default AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_OWNER;
}
declare module "@salesforce/label/c.AD_PICKLIST_VAL_NONE" {
    var AD_PICKLIST_VAL_NONE: string;
    export default AD_PICKLIST_VAL_NONE;
}
declare module "@salesforce/label/c.AD_UNIT_NAME" {
    var AD_UNIT_NAME: string;
    export default AD_UNIT_NAME;
}
declare module "@salesforce/label/c.AD_VALUE_SCALE" {
    var AD_VALUE_SCALE: string;
    export default AD_VALUE_SCALE;
}
declare module "@salesforce/label/c.AD_YEAR_START" {
    var AD_YEAR_START: string;
    export default AD_YEAR_START;
}
declare module "@salesforce/label/c.ATN_TD_CLOSE" {
    var ATN_TD_CLOSE: string;
    export default ATN_TD_CLOSE;
}
declare module "@salesforce/label/c.ATN_TD_OPEN" {
    var ATN_TD_OPEN: string;
    export default ATN_TD_OPEN;
}
declare module "@salesforce/label/c.ATN_TH_CLOSE" {
    var ATN_TH_CLOSE: string;
    export default ATN_TH_CLOSE;
}
declare module "@salesforce/label/c.ATN_TH_OPEN" {
    var ATN_TH_OPEN: string;
    export default ATN_TH_OPEN;
}
declare module "@salesforce/label/c.ATN_TR_CLOSE" {
    var ATN_TR_CLOSE: string;
    export default ATN_TR_CLOSE;
}
declare module "@salesforce/label/c.ATN_TR_OPEN" {
    var ATN_TR_OPEN: string;
    export default ATN_TR_OPEN;
}
declare module "@salesforce/label/c.Account_Name_Length" {
    var Account_Name_Length: string;
    export default Account_Name_Length;
}
declare module "@salesforce/label/c.Address_Line1_Length" {
    var Address_Line1_Length: string;
    export default Address_Line1_Length;
}
declare module "@salesforce/label/c.BL_DEACTIVATION_ERROR_MESSAGE" {
    var BL_DEACTIVATION_ERROR_MESSAGE: string;
    export default BL_DEACTIVATION_ERROR_MESSAGE;
}
declare module "@salesforce/label/c.BOL_ATTACHMENT_PARENT_OBJ_API" {
    var BOL_ATTACHMENT_PARENT_OBJ_API: string;
    export default BOL_ATTACHMENT_PARENT_OBJ_API;
}
declare module "@salesforce/label/c.BUSINESS_ADDRESS" {
    var BUSINESS_ADDRESS: string;
    export default BUSINESS_ADDRESS;
}
declare module "@salesforce/label/c.CC_RouteFinder" {
    var CC_RouteFinder: string;
    export default CC_RouteFinder;
}
declare module "@salesforce/label/c.CONFIRMATION_EMAIL_ON_FULL_TRANSFER" {
    var CONFIRMATION_EMAIL_ON_FULL_TRANSFER: string;
    export default CONFIRMATION_EMAIL_ON_FULL_TRANSFER;
}
declare module "@salesforce/label/c.CUSTOMERCOMMUNITY_URL" {
    var CUSTOMERCOMMUNITY_URL: string;
    export default CUSTOMERCOMMUNITY_URL;
}
declare module "@salesforce/label/c.City_Length" {
    var City_Length: string;
    export default City_Length;
}
declare module "@salesforce/label/c.CrowleyCommunity_BaseURL" {
    var CrowleyCommunity_BaseURL: string;
    export default CrowleyCommunity_BaseURL;
}
declare module "@salesforce/label/c.CrowleyCommunity_Bookings" {
    var CrowleyCommunity_Bookings: string;
    export default CrowleyCommunity_Bookings;
}
declare module "@salesforce/label/c.CrowleyCommunity_ContactUs" {
    var CrowleyCommunity_ContactUs: string;
    export default CrowleyCommunity_ContactUs;
}
declare module "@salesforce/label/c.CrowleyCommunity_DashboardHeading" {
    var CrowleyCommunity_DashboardHeading: string;
    export default CrowleyCommunity_DashboardHeading;
}
declare module "@salesforce/label/c.CrowleyCommunity_FAQs" {
    var CrowleyCommunity_FAQs: string;
    export default CrowleyCommunity_FAQs;
}
declare module "@salesforce/label/c.CrowleyCommunity_FARHeading" {
    var CrowleyCommunity_FARHeading: string;
    export default CrowleyCommunity_FARHeading;
}
declare module "@salesforce/label/c.CrowleyCommunity_FindMyShipment" {
    var CrowleyCommunity_FindMyShipment: string;
    export default CrowleyCommunity_FindMyShipment;
}
declare module "@salesforce/label/c.CrowleyCommunity_Ideas" {
    var CrowleyCommunity_Ideas: string;
    export default CrowleyCommunity_Ideas;
}
declare module "@salesforce/label/c.CrowleyCommunity_MyDocuments" {
    var CrowleyCommunity_MyDocuments: string;
    export default CrowleyCommunity_MyDocuments;
}
declare module "@salesforce/label/c.CrowleyCommunity_Register" {
    var CrowleyCommunity_Register: string;
    export default CrowleyCommunity_Register;
}
declare module "@salesforce/label/c.CrowleyCommunity_RequestAQuote" {
    var CrowleyCommunity_RequestAQuote: string;
    export default CrowleyCommunity_RequestAQuote;
}
declare module "@salesforce/label/c.CrowleyCommunity_STHeading" {
    var CrowleyCommunity_STHeading: string;
    export default CrowleyCommunity_STHeading;
}
declare module "@salesforce/label/c.CrowleyCommunity_VesselLocation" {
    var CrowleyCommunity_VesselLocation: string;
    export default CrowleyCommunity_VesselLocation;
}
declare module "@salesforce/label/c.CrowleyCommunity_VesselSchedule" {
    var CrowleyCommunity_VesselSchedule: string;
    export default CrowleyCommunity_VesselSchedule;
}
declare module "@salesforce/label/c.Crowley_Logo_URL" {
    var Crowley_Logo_URL: string;
    export default Crowley_Logo_URL;
}
declare module "@salesforce/label/c.CustomerCommunity_ArticleDetailUrl" {
    var CustomerCommunity_ArticleDetailUrl: string;
    export default CustomerCommunity_ArticleDetailUrl;
}
declare module "@salesforce/label/c.CustomerCommunity_BaseURL" {
    var CustomerCommunity_BaseURL: string;
    export default CustomerCommunity_BaseURL;
}
declare module "@salesforce/label/c.CustomerCommunity_BillOfLading" {
    var CustomerCommunity_BillOfLading: string;
    export default CustomerCommunity_BillOfLading;
}
declare module "@salesforce/label/c.CustomerCommunity_BillOfLadingController" {
    var CustomerCommunity_BillOfLadingController: string;
    export default CustomerCommunity_BillOfLadingController;
}
declare module "@salesforce/label/c.CustomerCommunity_Booking" {
    var CustomerCommunity_Booking: string;
    export default CustomerCommunity_Booking;
}
declare module "@salesforce/label/c.CustomerCommunity_BookingErrorMsg" {
    var CustomerCommunity_BookingErrorMsg: string;
    export default CustomerCommunity_BookingErrorMsg;
}
declare module "@salesforce/label/c.CustomerCommunity_BookingSuccessMsg" {
    var CustomerCommunity_BookingSuccessMsg: string;
    export default CustomerCommunity_BookingSuccessMsg;
}
declare module "@salesforce/label/c.CustomerCommunity_BookingURLMessage" {
    var CustomerCommunity_BookingURLMessage: string;
    export default CustomerCommunity_BookingURLMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_Bookings" {
    var CustomerCommunity_Bookings: string;
    export default CustomerCommunity_Bookings;
}
declare module "@salesforce/label/c.CustomerCommunity_CaseErrorMessage" {
    var CustomerCommunity_CaseErrorMessage: string;
    export default CustomerCommunity_CaseErrorMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_CaseURL" {
    var CustomerCommunity_CaseURL: string;
    export default CustomerCommunity_CaseURL;
}
declare module "@salesforce/label/c.CustomerCommunity_Cases" {
    var CustomerCommunity_Cases: string;
    export default CustomerCommunity_Cases;
}
declare module "@salesforce/label/c.CustomerCommunity_ConnectUsOnSocialMedia" {
    var CustomerCommunity_ConnectUsOnSocialMedia: string;
    export default CustomerCommunity_ConnectUsOnSocialMedia;
}
declare module "@salesforce/label/c.CustomerCommunity_ContactUs" {
    var CustomerCommunity_ContactUs: string;
    export default CustomerCommunity_ContactUs;
}
declare module "@salesforce/label/c.CustomerCommunity_Contracts" {
    var CustomerCommunity_Contracts: string;
    export default CustomerCommunity_Contracts;
}
declare module "@salesforce/label/c.CustomerCommunity_CopyRight" {
    var CustomerCommunity_CopyRight: string;
    export default CustomerCommunity_CopyRight;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxDetail1" {
    var CustomerCommunity_DashboardBoxDetail1: string;
    export default CustomerCommunity_DashboardBoxDetail1;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxDetail2" {
    var CustomerCommunity_DashboardBoxDetail2: string;
    export default CustomerCommunity_DashboardBoxDetail2;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxDetail3" {
    var CustomerCommunity_DashboardBoxDetail3: string;
    export default CustomerCommunity_DashboardBoxDetail3;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxDetail41" {
    var CustomerCommunity_DashboardBoxDetail41: string;
    export default CustomerCommunity_DashboardBoxDetail41;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxDetail42" {
    var CustomerCommunity_DashboardBoxDetail42: string;
    export default CustomerCommunity_DashboardBoxDetail42;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxHeading1" {
    var CustomerCommunity_DashboardBoxHeading1: string;
    export default CustomerCommunity_DashboardBoxHeading1;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxHeading2" {
    var CustomerCommunity_DashboardBoxHeading2: string;
    export default CustomerCommunity_DashboardBoxHeading2;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxHeading3" {
    var CustomerCommunity_DashboardBoxHeading3: string;
    export default CustomerCommunity_DashboardBoxHeading3;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardBoxHeading4" {
    var CustomerCommunity_DashboardBoxHeading4: string;
    export default CustomerCommunity_DashboardBoxHeading4;
}
declare module "@salesforce/label/c.CustomerCommunity_DashboardHeading" {
    var CustomerCommunity_DashboardHeading: string;
    export default CustomerCommunity_DashboardHeading;
}
declare module "@salesforce/label/c.CustomerCommunity_English" {
    var CustomerCommunity_English: string;
    export default CustomerCommunity_English;
}
declare module "@salesforce/label/c.CustomerCommunity_FAQErrorMessage" {
    var CustomerCommunity_FAQErrorMessage: string;
    export default CustomerCommunity_FAQErrorMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_FAQHeading" {
    var CustomerCommunity_FAQHeading: string;
    export default CustomerCommunity_FAQHeading;
}
declare module "@salesforce/label/c.CustomerCommunity_FAQs" {
    var CustomerCommunity_FAQs: string;
    export default CustomerCommunity_FAQs;
}
declare module "@salesforce/label/c.CustomerCommunity_FARChatbotErrorMessage" {
    var CustomerCommunity_FARChatbotErrorMessage: string;
    export default CustomerCommunity_FARChatbotErrorMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_FARErrorMessage" {
    var CustomerCommunity_FARErrorMessage: string;
    export default CustomerCommunity_FARErrorMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_FARHeading" {
    var CustomerCommunity_FARHeading: string;
    export default CustomerCommunity_FARHeading;
}
declare module "@salesforce/label/c.CustomerCommunity_FARInputMessage" {
    var CustomerCommunity_FARInputMessage: string;
    export default CustomerCommunity_FARInputMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_FARPageMessage" {
    var CustomerCommunity_FARPageMessage: string;
    export default CustomerCommunity_FARPageMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_FindARouteButton" {
    var CustomerCommunity_FindARouteButton: string;
    export default CustomerCommunity_FindARouteButton;
}
declare module "@salesforce/label/c.CustomerCommunity_FindARouteUrl" {
    var CustomerCommunity_FindARouteUrl: string;
    export default CustomerCommunity_FindARouteUrl;
}
declare module "@salesforce/label/c.CustomerCommunity_FindMyShipment" {
    var CustomerCommunity_FindMyShipment: string;
    export default CustomerCommunity_FindMyShipment;
}
declare module "@salesforce/label/c.CustomerCommunity_ForgetPassword" {
    var CustomerCommunity_ForgetPassword: string;
    export default CustomerCommunity_ForgetPassword;
}
declare module "@salesforce/label/c.CustomerCommunity_Hello" {
    var CustomerCommunity_Hello: string;
    export default CustomerCommunity_Hello;
}
declare module "@salesforce/label/c.CustomerCommunity_Idea" {
    var CustomerCommunity_Idea: string;
    export default CustomerCommunity_Idea;
}
declare module "@salesforce/label/c.CustomerCommunity_Ideaa" {
    var CustomerCommunity_Ideaa: string;
    export default CustomerCommunity_Ideaa;
}
declare module "@salesforce/label/c.CustomerCommunity_Login" {
    var CustomerCommunity_Login: string;
    export default CustomerCommunity_Login;
}
declare module "@salesforce/label/c.CustomerCommunity_MyDocuments" {
    var CustomerCommunity_MyDocuments: string;
    export default CustomerCommunity_MyDocuments;
}
declare module "@salesforce/label/c.CustomerCommunity_Name" {
    var CustomerCommunity_Name: string;
    export default CustomerCommunity_Name;
}
declare module "@salesforce/label/c.CustomerCommunity_NewUser" {
    var CustomerCommunity_NewUser: string;
    export default CustomerCommunity_NewUser;
}
declare module "@salesforce/label/c.CustomerCommunity_Password" {
    var CustomerCommunity_Password: string;
    export default CustomerCommunity_Password;
}
declare module "@salesforce/label/c.CustomerCommunity_ReadyToShip" {
    var CustomerCommunity_ReadyToShip: string;
    export default CustomerCommunity_ReadyToShip;
}
declare module "@salesforce/label/c.CustomerCommunity_Reports" {
    var CustomerCommunity_Reports: string;
    export default CustomerCommunity_Reports;
}
declare module "@salesforce/label/c.CustomerCommunity_RequestAQuote" {
    var CustomerCommunity_RequestAQuote: string;
    export default CustomerCommunity_RequestAQuote;
}
declare module "@salesforce/label/c.CustomerCommunity_RequestAQuoteButton" {
    var CustomerCommunity_RequestAQuoteButton: string;
    export default CustomerCommunity_RequestAQuoteButton;
}
declare module "@salesforce/label/c.CustomerCommunity_RequestAQuoteDescription" {
    var CustomerCommunity_RequestAQuoteDescription: string;
    export default CustomerCommunity_RequestAQuoteDescription;
}
declare module "@salesforce/label/c.CustomerCommunity_STChatbotErrorMessage" {
    var CustomerCommunity_STChatbotErrorMessage: string;
    export default CustomerCommunity_STChatbotErrorMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_STErrorMessage1" {
    var CustomerCommunity_STErrorMessage1: string;
    export default CustomerCommunity_STErrorMessage1;
}
declare module "@salesforce/label/c.CustomerCommunity_STErrorMessage2" {
    var CustomerCommunity_STErrorMessage2: string;
    export default CustomerCommunity_STErrorMessage2;
}
declare module "@salesforce/label/c.CustomerCommunity_STHeading" {
    var CustomerCommunity_STHeading: string;
    export default CustomerCommunity_STHeading;
}
declare module "@salesforce/label/c.CustomerCommunity_STPageInfo" {
    var CustomerCommunity_STPageInfo: string;
    export default CustomerCommunity_STPageInfo;
}
declare module "@salesforce/label/c.CustomerCommunity_STPageMessage" {
    var CustomerCommunity_STPageMessage: string;
    export default CustomerCommunity_STPageMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_TrackShipmentButton" {
    var CustomerCommunity_TrackShipmentButton: string;
    export default CustomerCommunity_TrackShipmentButton;
}
declare module "@salesforce/label/c.CustomerCommunity_TrackShipments" {
    var CustomerCommunity_TrackShipments: string;
    export default CustomerCommunity_TrackShipments;
}
declare module "@salesforce/label/c.CustomerCommunity_TrackShipmentsDescription" {
    var CustomerCommunity_TrackShipmentsDescription: string;
    export default CustomerCommunity_TrackShipmentsDescription;
}
declare module "@salesforce/label/c.CustomerCommunity_Username" {
    var CustomerCommunity_Username: string;
    export default CustomerCommunity_Username;
}
declare module "@salesforce/label/c.CustomerCommunity_VehicleShipping" {
    var CustomerCommunity_VehicleShipping: string;
    export default CustomerCommunity_VehicleShipping;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselErrorMessage" {
    var CustomerCommunity_VesselErrorMessage: string;
    export default CustomerCommunity_VesselErrorMessage;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselErrorMessage_Germany" {
    var CustomerCommunity_VesselErrorMessage_Germany: string;
    export default CustomerCommunity_VesselErrorMessage_Germany;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselErrorMessage_Singapore" {
    var CustomerCommunity_VesselErrorMessage_Singapore: string;
    export default CustomerCommunity_VesselErrorMessage_Singapore;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselLocation" {
    var CustomerCommunity_VesselLocation: string;
    export default CustomerCommunity_VesselLocation;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselSchedule" {
    var CustomerCommunity_VesselSchedule: string;
    export default CustomerCommunity_VesselSchedule;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselScheduleButton" {
    var CustomerCommunity_VesselScheduleButton: string;
    export default CustomerCommunity_VesselScheduleButton;
}
declare module "@salesforce/label/c.CustomerCommunity_VesselScheduleUrl" {
    var CustomerCommunity_VesselScheduleUrl: string;
    export default CustomerCommunity_VesselScheduleUrl;
}
declare module "@salesforce/label/c.DELETION_NOT_ALLOWED_PLEASE_CONTACT_MANAGER" {
    var DELETION_NOT_ALLOWED_PLEASE_CONTACT_MANAGER: string;
    export default DELETION_NOT_ALLOWED_PLEASE_CONTACT_MANAGER;
}
declare module "@salesforce/label/c.EMAIL_OR_PHONE" {
    var EMAIL_OR_PHONE: string;
    export default EMAIL_OR_PHONE;
}
declare module "@salesforce/label/c.EMAIL_TEMPLATE_NOTIFICATION_ACC_DELETED" {
    var EMAIL_TEMPLATE_NOTIFICATION_ACC_DELETED: string;
    export default EMAIL_TEMPLATE_NOTIFICATION_ACC_DELETED;
}
declare module "@salesforce/label/c.EMPTY_FILE" {
    var EMPTY_FILE: string;
    export default EMPTY_FILE;
}
declare module "@salesforce/label/c.ENGLISH" {
    var ENGLISH: string;
    export default ENGLISH;
}
declare module "@salesforce/label/c.ERROR_JSON_BAD_FORMAT" {
    var ERROR_JSON_BAD_FORMAT: string;
    export default ERROR_JSON_BAD_FORMAT;
}
declare module "@salesforce/label/c.ERROR_JSON_EMPTY" {
    var ERROR_JSON_EMPTY: string;
    export default ERROR_JSON_EMPTY;
}
declare module "@salesforce/label/c.ERROR_OP_FAILED" {
    var ERROR_OP_FAILED: string;
    export default ERROR_OP_FAILED;
}
declare module "@salesforce/label/c.ERROR_REQUEST_FAILED_NO_ERROR" {
    var ERROR_REQUEST_FAILED_NO_ERROR: string;
    export default ERROR_REQUEST_FAILED_NO_ERROR;
}
declare module "@salesforce/label/c.ERROR_UNAUTHORIZED_ENDPOINT" {
    var ERROR_UNAUTHORIZED_ENDPOINT: string;
    export default ERROR_UNAUTHORIZED_ENDPOINT;
}
declare module "@salesforce/label/c.Edit_CDM_CVIF_Fields_Profile" {
    var Edit_CDM_CVIF_Fields_Profile: string;
    export default Edit_CDM_CVIF_Fields_Profile;
}
declare module "@salesforce/label/c.Edit_Idea_Comment_Page" {
    var Edit_Idea_Comment_Page: string;
    export default Edit_Idea_Comment_Page;
}
declare module "@salesforce/label/c.Edit_Idea_Page" {
    var Edit_Idea_Page: string;
    export default Edit_Idea_Page;
}
declare module "@salesforce/label/c.Error_Opportunity_Team_Member_Sales_Agent_Role" {
    var Error_Opportunity_Team_Member_Sales_Agent_Role: string;
    export default Error_Opportunity_Team_Member_Sales_Agent_Role;
}
declare module "@salesforce/label/c.FILE_MISSING" {
    var FILE_MISSING: string;
    export default FILE_MISSING;
}
declare module "@salesforce/label/c.FIRST_NAME" {
    var FIRST_NAME: string;
    export default FIRST_NAME;
}
declare module "@salesforce/label/c.GTR_ACCOUT_TYPE_PROSPECT" {
    var GTR_ACCOUT_TYPE_PROSPECT: string;
    export default GTR_ACCOUT_TYPE_PROSPECT;
}
declare module "@salesforce/label/c.GTR_DATA_TYPE_LOOKUP" {
    var GTR_DATA_TYPE_LOOKUP: string;
    export default GTR_DATA_TYPE_LOOKUP;
}
declare module "@salesforce/label/c.GTR_DATA_TYPE_LOOK_UP" {
    var GTR_DATA_TYPE_LOOK_UP: string;
    export default GTR_DATA_TYPE_LOOK_UP;
}
declare module "@salesforce/label/c.GTR_OBJECT_NAME_ACCOUNT" {
    var GTR_OBJECT_NAME_ACCOUNT: string;
    export default GTR_OBJECT_NAME_ACCOUNT;
}
declare module "@salesforce/label/c.GTR_OBJECT_NAME_BUSINESS_LOCATION" {
    var GTR_OBJECT_NAME_BUSINESS_LOCATION: string;
    export default GTR_OBJECT_NAME_BUSINESS_LOCATION;
}
declare module "@salesforce/label/c.GTR_OPERATION_MODIFIED" {
    var GTR_OPERATION_MODIFIED: string;
    export default GTR_OPERATION_MODIFIED;
}
declare module "@salesforce/label/c.GTR_OPERATION_TYPE_CREATED" {
    var GTR_OPERATION_TYPE_CREATED: string;
    export default GTR_OPERATION_TYPE_CREATED;
}
declare module "@salesforce/label/c.GTR_OPERATION_TYPE_DELETED" {
    var GTR_OPERATION_TYPE_DELETED: string;
    export default GTR_OPERATION_TYPE_DELETED;
}
declare module "@salesforce/label/c.GTR_PROFILE_DATA_STEWARD" {
    var GTR_PROFILE_DATA_STEWARD: string;
    export default GTR_PROFILE_DATA_STEWARD;
}
declare module "@salesforce/label/c.GTR_PROFILE_INTEGRATION_USER" {
    var GTR_PROFILE_INTEGRATION_USER: string;
    export default GTR_PROFILE_INTEGRATION_USER;
}
declare module "@salesforce/label/c.INSUFFICIENT_ACCESS_TO_DELETE_LEAD_RECORD" {
    var INSUFFICIENT_ACCESS_TO_DELETE_LEAD_RECORD: string;
    export default INSUFFICIENT_ACCESS_TO_DELETE_LEAD_RECORD;
}
declare module "@salesforce/label/c.INTEGRATION_USER" {
    var INTEGRATION_USER: string;
    export default INTEGRATION_USER;
}
declare module "@salesforce/label/c.INVALID_FILE_ROW_SIZE_DOESNT_MATCH" {
    var INVALID_FILE_ROW_SIZE_DOESNT_MATCH: string;
    export default INVALID_FILE_ROW_SIZE_DOESNT_MATCH;
}
declare module "@salesforce/label/c.Idea_Community_Path" {
    var Idea_Community_Path: string;
    export default Idea_Community_Path;
}
declare module "@salesforce/label/c.Idea_Community_URI" {
    var Idea_Community_URI: string;
    export default Idea_Community_URI;
}
declare module "@salesforce/label/c.Idea_Delivered_Notification_Template_Id" {
    var Idea_Delivered_Notification_Template_Id: string;
    export default Idea_Delivered_Notification_Template_Id;
}
declare module "@salesforce/label/c.Idea_Detail_Page" {
    var Idea_Detail_Page: string;
    export default Idea_Detail_Page;
}
declare module "@salesforce/label/c.Idea_Moderator" {
    var Idea_Moderator: string;
    export default Idea_Moderator;
}
declare module "@salesforce/label/c.Idea_Trigger_Flag" {
    var Idea_Trigger_Flag: string;
    export default Idea_Trigger_Flag;
}
declare module "@salesforce/label/c.Ideas_List_Page" {
    var Ideas_List_Page: string;
    export default Ideas_List_Page;
}
declare module "@salesforce/label/c.LA_Chat_ExtendedHeader_ChatStateHeaderGreeting_04I0t0000000043_4908324" {
    var LA_Chat_ExtendedHeader_ChatStateHeaderGreeting_04I0t0000000043_4908324: string;
    export default LA_Chat_ExtendedHeader_ChatStateHeaderGreeting_04I0t0000000043_4908324;
}
declare module "@salesforce/label/c.LA_Chat_ExtendedHeader_ChatStateHeaderGreeting_04I0t000000004X_3350060" {
    var LA_Chat_ExtendedHeader_ChatStateHeaderGreeting_04I0t000000004X_3350060: string;
    export default LA_Chat_ExtendedHeader_ChatStateHeaderGreeting_04I0t000000004X_3350060;
}
declare module "@salesforce/label/c.LOG_ATLEAST_ONE_FOLLOW_UP_ACTIVITY" {
    var LOG_ATLEAST_ONE_FOLLOW_UP_ACTIVITY: string;
    export default LOG_ATLEAST_ONE_FOLLOW_UP_ACTIVITY;
}
declare module "@salesforce/label/c.MARKETING_TEAM_EMAIL" {
    var MARKETING_TEAM_EMAIL: string;
    export default MARKETING_TEAM_EMAIL;
}
declare module "@salesforce/label/c.MSG_CHECK_API_NAMES" {
    var MSG_CHECK_API_NAMES: string;
    export default MSG_CHECK_API_NAMES;
}
declare module "@salesforce/label/c.MSG_CS_MDT_EXISTS" {
    var MSG_CS_MDT_EXISTS: string;
    export default MSG_CS_MDT_EXISTS;
}
declare module "@salesforce/label/c.MSG_CS_MDT_FIELD_NAME_FORMAT" {
    var MSG_CS_MDT_FIELD_NAME_FORMAT: string;
    export default MSG_CS_MDT_FIELD_NAME_FORMAT;
}
declare module "@salesforce/label/c.MSG_CS_MDT_FIELD_NAME_REQ" {
    var MSG_CS_MDT_FIELD_NAME_REQ: string;
    export default MSG_CS_MDT_FIELD_NAME_REQ;
}
declare module "@salesforce/label/c.MSG_CS_MDT_MAPPING_REQ" {
    var MSG_CS_MDT_MAPPING_REQ: string;
    export default MSG_CS_MDT_MAPPING_REQ;
}
declare module "@salesforce/label/c.MSG_CS_MDT_REQ" {
    var MSG_CS_MDT_REQ: string;
    export default MSG_CS_MDT_REQ;
}
declare module "@salesforce/label/c.MSG_CUSTOM_SETTINGS_EXISTS" {
    var MSG_CUSTOM_SETTINGS_EXISTS: string;
    export default MSG_CUSTOM_SETTINGS_EXISTS;
}
declare module "@salesforce/label/c.MSG_JSON_API_NAMES" {
    var MSG_JSON_API_NAMES: string;
    export default MSG_JSON_API_NAMES;
}
declare module "@salesforce/label/c.MSG_JSON_FORMAT" {
    var MSG_JSON_FORMAT: string;
    export default MSG_JSON_FORMAT;
}
declare module "@salesforce/label/c.MSG_MDT_END" {
    var MSG_MDT_END: string;
    export default MSG_MDT_END;
}
declare module "@salesforce/label/c.MSG_MIGRATION_COMPLETED" {
    var MSG_MIGRATION_COMPLETED: string;
    export default MSG_MIGRATION_COMPLETED;
}
declare module "@salesforce/label/c.MSG_MIGRATION_IN_PROGRESS" {
    var MSG_MIGRATION_IN_PROGRESS: string;
    export default MSG_MIGRATION_IN_PROGRESS;
}
declare module "@salesforce/label/c.MSG_NOT_SUPPORTED" {
    var MSG_NOT_SUPPORTED: string;
    export default MSG_NOT_SUPPORTED;
}
declare module "@salesforce/label/c.Name_Change_Error_Message_1" {
    var Name_Change_Error_Message_1: string;
    export default Name_Change_Error_Message_1;
}
declare module "@salesforce/label/c.Name_Change_Error_Message_2" {
    var Name_Change_Error_Message_2: string;
    export default Name_Change_Error_Message_2;
}
declare module "@salesforce/label/c.New_Idea_Page" {
    var New_Idea_Page: string;
    export default New_Idea_Page;
}
declare module "@salesforce/label/c.New_Idea_Post_Template_Id" {
    var New_Idea_Post_Template_Id: string;
    export default New_Idea_Post_Template_Id;
}
declare module "@salesforce/label/c.OWNER_CHANGE_BATCH_JOB_ERROR" {
    var OWNER_CHANGE_BATCH_JOB_ERROR: string;
    export default OWNER_CHANGE_BATCH_JOB_ERROR;
}
declare module "@salesforce/label/c.OWNER_CHANGE_FOOTER" {
    var OWNER_CHANGE_FOOTER: string;
    export default OWNER_CHANGE_FOOTER;
}
declare module "@salesforce/label/c.OWNER_CHANGE_HEADER_NEW_OWNER" {
    var OWNER_CHANGE_HEADER_NEW_OWNER: string;
    export default OWNER_CHANGE_HEADER_NEW_OWNER;
}
declare module "@salesforce/label/c.OpportunityPopupMessageBody" {
    var OpportunityPopupMessageBody: string;
    export default OpportunityPopupMessageBody;
}
declare module "@salesforce/label/c.OpportunityPopupMessageHeader" {
    var OpportunityPopupMessageHeader: string;
    export default OpportunityPopupMessageHeader;
}
declare module "@salesforce/label/c.OpportunityStagePopupMessage" {
    var OpportunityStagePopupMessage: string;
    export default OpportunityStagePopupMessage;
}
declare module "@salesforce/label/c.PROFILE_DATA_STEWARD" {
    var PROFILE_DATA_STEWARD: string;
    export default PROFILE_DATA_STEWARD;
}
declare module "@salesforce/label/c.Phone_Validation" {
    var Phone_Validation: string;
    export default Phone_Validation;
}
declare module "@salesforce/label/c.Premium_High" {
    var Premium_High: string;
    export default Premium_High;
}
declare module "@salesforce/label/c.Premium_Low" {
    var Premium_Low: string;
    export default Premium_Low;
}
declare module "@salesforce/label/c.Premium_Medium" {
    var Premium_Medium: string;
    export default Premium_Medium;
}
declare module "@salesforce/label/c.Profiles_with_Account_Team_Permission" {
    var Profiles_with_Account_Team_Permission: string;
    export default Profiles_with_Account_Team_Permission;
}
declare module "@salesforce/label/c.RECORD_ALREADY_SYNCHED_WITH_CDM" {
    var RECORD_ALREADY_SYNCHED_WITH_CDM: string;
    export default RECORD_ALREADY_SYNCHED_WITH_CDM;
}
declare module "@salesforce/label/c.SYSTEM_ADMINISTRATOR" {
    var SYSTEM_ADMINISTRATOR: string;
    export default SYSTEM_ADMINISTRATOR;
}
declare module "@salesforce/label/c.Sales_Profiles" {
    var Sales_Profiles: string;
    export default Sales_Profiles;
}
declare module "@salesforce/label/c.Sales_Roles" {
    var Sales_Roles: string;
    export default Sales_Roles;
}
declare module "@salesforce/label/c.Standard_High" {
    var Standard_High: string;
    export default Standard_High;
}
declare module "@salesforce/label/c.Standard_Low" {
    var Standard_Low: string;
    export default Standard_Low;
}
declare module "@salesforce/label/c.Standard_Medium" {
    var Standard_Medium: string;
    export default Standard_Medium;
}
declare module "@salesforce/label/c.String_Separator" {
    var String_Separator: string;
    export default String_Separator;
}
declare module "@salesforce/label/c.System_Admin_User" {
    var System_Admin_User: string;
    export default System_Admin_User;
}
declare module "@salesforce/label/c.TYPE_OPTION_NOT_SELECTED" {
    var TYPE_OPTION_NOT_SELECTED: string;
    export default TYPE_OPTION_NOT_SELECTED;
}
declare module "@salesforce/label/c.USER_MUST_ENTER_CONTACT_DETAILS" {
    var USER_MUST_ENTER_CONTACT_DETAILS: string;
    export default USER_MUST_ENTER_CONTACT_DETAILS;
}
declare module "@salesforce/label/c.XMLP_BOOLEAN_METADATA_NAME" {
    var XMLP_BOOLEAN_METADATA_NAME: string;
    export default XMLP_BOOLEAN_METADATA_NAME;
}
declare module "@salesforce/label/c.YOUR_ATTEMPT_TO_DELETE" {
    var YOUR_ATTEMPT_TO_DELETE: string;
    export default YOUR_ATTEMPT_TO_DELETE;
}