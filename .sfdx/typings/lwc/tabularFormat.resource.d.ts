declare module "@salesforce/resourceUrl/tabularFormat" {
    var tabularFormat: string;
    export default tabularFormat;
}