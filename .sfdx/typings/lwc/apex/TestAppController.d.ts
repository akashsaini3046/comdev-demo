declare module "@salesforce/apex/TestAppController.savePDFOnAccount" {
  export default function savePDFOnAccount(): Promise<any>;
}
