declare module "@salesforce/apex/CustomerCommunity_PreferencesController.updateDetails" {
  export default function updateDetails(param: {userEmail: any, idUser: any, listSmsNumbers: any, listWhatsAppNumbers: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_PreferencesController.fetchUserDetails" {
  export default function fetchUserDetails(): Promise<any>;
}
