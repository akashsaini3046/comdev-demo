declare module "@salesforce/apex/ADTasksController.fetchTasks" {
  export default function fetchTasks(param: {accountId: any, objMasterFilter: any}): Promise<any>;
}
declare module "@salesforce/apex/ADTasksController.applyFilter" {
  export default function applyFilter(param: {accountId: any, startDate: any, endDate: any, dateFilter: any, selectedOwners: any, taskList: any}): Promise<any>;
}
declare module "@salesforce/apex/ADTasksController.applySorting" {
  export default function applySorting(param: {sortedDateOptionSelected: any, taskList: any}): Promise<any>;
}
