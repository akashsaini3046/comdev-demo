declare module "@salesforce/apex/CustomerCommunity_AllBookingController.fetchBookingData" {
  export default function fetchBookingData(param: {recordLimit: any, recordOffset: any, fieldName: any, order: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_AllBookingController.getTotalBookings" {
  export default function getTotalBookings(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_AllBookingController.fetchIframeUrl" {
  export default function fetchIframeUrl(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_AllBookingController.sendEmailPDF" {
  export default function sendEmailPDF(param: {bookingId: any, emailAddress: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_AllBookingController.getRates" {
  export default function getRates(param: {IdBooking: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_AllBookingController.validateIMDG" {
  export default function validateIMDG(param: {IdBooking: any}): Promise<any>;
}
