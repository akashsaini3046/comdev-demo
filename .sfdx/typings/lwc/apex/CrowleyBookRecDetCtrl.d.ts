declare module "@salesforce/apex/CrowleyBookRecDetCtrl.getBookingDetail" {
  export default function getBookingDetail(param: {bookingId: any, selectedMenu: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookRecDetCtrl.getSectionWiseData" {
  export default function getSectionWiseData(param: {sectionNameVsFieldsList: any, sectionName: any, fieldSetName: any, objectAPIName: any, parentIdsList: any, parentObjectRelationName: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookRecDetCtrl.getFieldNameLabel" {
  export default function getFieldNameLabel(param: {fieldSetName: any, ObjectName: any, sectionName: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookRecDetCtrl.getFieldsDefinition" {
  export default function getFieldsDefinition(param: {selectedMenu: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookRecDetCtrl.getRatingDetail" {
  export default function getRatingDetail(param: {bookingId: any}): Promise<any>;
}
