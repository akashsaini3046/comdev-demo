declare module "@salesforce/apex/OpportunityStageField.getOpportunityStageValue" {
  export default function getOpportunityStageValue(param: {opportunityId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityStageField.getQuotedPriceOfOpportunity" {
  export default function getQuotedPriceOfOpportunity(param: {opportunityId: any}): Promise<any>;
}
