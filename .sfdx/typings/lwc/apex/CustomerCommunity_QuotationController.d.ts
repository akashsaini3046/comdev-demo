declare module "@salesforce/apex/CustomerCommunity_QuotationController.getDestinationPorts" {
  export default function getDestinationPorts(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationController.getQuotationRates" {
  export default function getQuotationRates(param: {destinationCountry: any, weight: any, volume: any, originCountry: any, originPostalCode: any, currentUnit: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationController.calculateOceanRate" {
  export default function calculateOceanRate(param: {quoteRate: any, weight: any, volume: any, currentUnit: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationController.getPorts" {
  export default function getPorts(param: {zipCode: any}): Promise<any>;
}
