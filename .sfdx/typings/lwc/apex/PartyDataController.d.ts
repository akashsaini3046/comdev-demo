declare module "@salesforce/apex/PartyDataController.getPartyData" {
  export default function getPartyData(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/PartyDataController.getStringQuery" {
  export default function getStringQuery(param: {mapFieldSet: any, type: any, objectAPIName: any}): Promise<any>;
}
declare module "@salesforce/apex/PartyDataController.getFieldsDefinition" {
  export default function getFieldsDefinition(): Promise<any>;
}
