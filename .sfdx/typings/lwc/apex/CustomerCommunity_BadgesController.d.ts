declare module "@salesforce/apex/CustomerCommunity_BadgesController.checkBadges" {
  export default function checkBadges(param: {username: any}): Promise<any>;
}
