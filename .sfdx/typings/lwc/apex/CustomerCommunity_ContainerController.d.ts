declare module "@salesforce/apex/CustomerCommunity_ContainerController.fetchContainerList" {
  export default function fetchContainerList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ContainerController.getSubstancesRecord" {
  export default function getSubstancesRecord(param: {searchKey: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ContainerController.getVehicles" {
  export default function getVehicles(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ContainerController.fetchRatesForMultiContainer" {
  export default function fetchRatesForMultiContainer(param: {bookingWrapper: any}): Promise<any>;
}
