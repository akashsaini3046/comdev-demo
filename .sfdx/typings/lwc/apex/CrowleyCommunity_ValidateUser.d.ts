declare module "@salesforce/apex/CrowleyCommunity_ValidateUser.validateUserDetail" {
  export default function validateUserDetail(param: {userEmail: any}): Promise<any>;
}
