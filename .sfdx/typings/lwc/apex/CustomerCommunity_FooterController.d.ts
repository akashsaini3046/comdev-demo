declare module "@salesforce/apex/CustomerCommunity_FooterController.queryColumnOneValues" {
  export default function queryColumnOneValues(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FooterController.queryColumnTwoValues" {
  export default function queryColumnTwoValues(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FooterController.queryColumnThreeValues" {
  export default function queryColumnThreeValues(): Promise<any>;
}
