declare module "@salesforce/apex/CustomerCommunity_SocialSignOnController.fetchUserSocialDetails" {
  export default function fetchUserSocialDetails(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_SocialSignOnController.disconnectSocialAccount" {
  export default function disconnectSocialAccount(param: {socialId: any}): Promise<any>;
}
