declare module "@salesforce/apex/CustomerCommunity_InfoPanelController.getUserData" {
  export default function getUserData(): Promise<any>;
}
