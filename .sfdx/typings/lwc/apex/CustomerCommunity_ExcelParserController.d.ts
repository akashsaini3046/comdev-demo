declare module "@salesforce/apex/CustomerCommunity_ExcelParserController.getFieldLabels" {
  export default function getFieldLabels(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ExcelParserController.createBLRecords" {
  export default function createBLRecords(param: {listFormData: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ExcelParserController.createFileBooking" {
  export default function createFileBooking(param: {fileName: any, base64Data: any, contentType: any}): Promise<any>;
}
