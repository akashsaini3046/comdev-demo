declare module "@salesforce/apex/CustomerCommunity_SendRegistrationLink.sentRegistrationEmail" {
  export default function sentRegistrationEmail(param: {username: any}): Promise<any>;
}
