declare module "@salesforce/apex/ADOpportunityPipelineChartController.getOpportunityChartData" {
  export default function getOpportunityChartData(param: {idAccountId: any, objMasterFilter: any}): Promise<any>;
}
