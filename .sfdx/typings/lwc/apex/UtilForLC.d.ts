declare module "@salesforce/apex/UtilForLC.fetchRecords" {
  export default function fetchRecords(param: {jsonString: any}): Promise<any>;
}
