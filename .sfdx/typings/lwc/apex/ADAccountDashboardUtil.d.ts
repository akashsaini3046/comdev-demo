declare module "@salesforce/apex/ADAccountDashboardUtil.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyWord: any, ExcludeitemsList: any, ownerIdList: any}): Promise<any>;
}
