declare module "@salesforce/apex/CustomerCommunity_ShippingRelated.fetchChargeLine" {
  export default function fetchChargeLine(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingRelated.fetchEquipments" {
  export default function fetchEquipments(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingRelated.fetchLineItems" {
  export default function fetchLineItems(param: {recordId: any}): Promise<any>;
}
