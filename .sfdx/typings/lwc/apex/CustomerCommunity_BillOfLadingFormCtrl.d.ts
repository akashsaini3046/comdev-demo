declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.fetchBillOfLadingRecord" {
  export default function fetchBillOfLadingRecord(param: {billOfLadingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.fetchPartyRecords" {
  export default function fetchPartyRecords(param: {billOfLadingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.fetchBillItemRecords" {
  export default function fetchBillItemRecords(param: {billOfLadingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.fetchChargeLineItems" {
  export default function fetchChargeLineItems(param: {billOfLadingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.fetchEquipmentRecords" {
  export default function fetchEquipmentRecords(param: {billOfLadingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.getCommunityUrlPathPrefix" {
  export default function getCommunityUrlPathPrefix(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.handleGetRates" {
  export default function handleGetRates(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingFormCtrl.handleGetRatesTest" {
  export default function handleGetRatesTest(param: {bookingId: any}): Promise<any>;
}
