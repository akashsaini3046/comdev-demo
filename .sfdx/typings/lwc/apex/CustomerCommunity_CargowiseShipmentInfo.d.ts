declare module "@salesforce/apex/CustomerCommunity_CargowiseShipmentInfo.parse" {
  export default function parse(param: {json: any}): Promise<any>;
}
