declare module "@salesforce/apex/SK_LightningTreeCmpController.findHierarchyData" {
  export default function findHierarchyData(param: {recId: any, sObjectName: any, parentFieldAPIname: any, labelFieldAPIName: any}): Promise<any>;
}
