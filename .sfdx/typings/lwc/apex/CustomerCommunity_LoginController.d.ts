declare module "@salesforce/apex/CustomerCommunity_LoginController.checkPortal" {
  export default function checkPortal(param: {username: any, password: any, currentURL: any}): Promise<any>;
}
