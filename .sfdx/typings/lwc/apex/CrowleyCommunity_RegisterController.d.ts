declare module "@salesforce/apex/CrowleyCommunity_RegisterController.checkAccountExists" {
  export default function checkAccountExists(param: {accountDetails: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyCommunity_RegisterController.registerUser" {
  export default function registerUser(param: {userDetails: any, contactId: any}): Promise<any>;
}
