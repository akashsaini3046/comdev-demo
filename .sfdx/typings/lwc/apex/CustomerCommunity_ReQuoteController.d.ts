declare module "@salesforce/apex/CustomerCommunity_ReQuoteController.fetchQuoteData" {
  export default function fetchQuoteData(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ReQuoteController.fetchQuoteDataSorted" {
  export default function fetchQuoteDataSorted(param: {fieldName: any, order: any}): Promise<any>;
}
