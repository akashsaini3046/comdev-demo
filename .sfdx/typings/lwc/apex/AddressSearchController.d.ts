declare module "@salesforce/apex/AddressSearchController.getAddressDetailsbyId" {
  export default function getAddressDetailsbyId(param: {id: any}): Promise<any>;
}
declare module "@salesforce/apex/AddressSearchController.saveAddressDetailsbyId" {
  export default function saveAddressDetailsbyId(param: {id: any, addDetails: any}): Promise<any>;
}
declare module "@salesforce/apex/AddressSearchController.getAddressSet" {
  export default function getAddressSet(param: {SearchText: any}): Promise<any>;
}
declare module "@salesforce/apex/AddressSearchController.getAddressDetailsByPlaceId" {
  export default function getAddressDetailsByPlaceId(param: {PlaceID: any}): Promise<any>;
}
