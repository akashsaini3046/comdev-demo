declare module "@salesforce/apex/CustomerCommunity_CaseHandler.fetchCaseRecords" {
  export default function fetchCaseRecords(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CaseHandler.getQuickLinks" {
  export default function getQuickLinks(): Promise<any>;
}
