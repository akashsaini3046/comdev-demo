declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaDetails" {
  export default function getIdeaDetails(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getUser" {
  export default function getUser(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getZonesList" {
  export default function getZonesList(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getBenefits" {
  export default function getBenefits(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaStatuses" {
  export default function getIdeaStatuses(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaCategories" {
  export default function getIdeaCategories(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.saveIdeaRecord" {
  export default function saveIdeaRecord(param: {ideaRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.deleteIdeaRecord" {
  export default function deleteIdeaRecord(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.findSimilarIdeas" {
  export default function findSimilarIdeas(param: {communityId: any, title: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaFieldDescribe" {
  export default function getIdeaFieldDescribe(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaDescribe" {
  export default function getIdeaDescribe(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaComments" {
  export default function getIdeaComments(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.upvoteIdea" {
  export default function upvoteIdea(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.downvoteIdea" {
  export default function downvoteIdea(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.likeComment" {
  export default function likeComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.unlikeComment" {
  export default function unlikeComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.deleteComment" {
  export default function deleteComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.addComment" {
  export default function addComment(param: {param: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeaCommentRecord" {
  export default function getIdeaCommentRecord(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.saveIdeaCommentRecord" {
  export default function saveIdeaCommentRecord(param: {comment: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getIdeasList" {
  export default function getIdeasList(param: {communityId: any, statuses: any, searchText: any, categories: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.fetchRecordTypeSpecificPickListvalues" {
  export default function fetchRecordTypeSpecificPickListvalues(param: {recordTypeId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getRecordTypeId" {
  export default function getRecordTypeId(param: {zoneId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailLightningController.getExpertsGroupMembers" {
  export default function getExpertsGroupMembers(param: {ideaId: any}): Promise<any>;
}
