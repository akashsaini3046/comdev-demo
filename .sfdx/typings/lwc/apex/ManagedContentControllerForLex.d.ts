declare module "@salesforce/apex/ManagedContentControllerForLex.getMContent" {
  export default function getMContent(param: {contentType: any, managedContentIds_str: any, topicNames_str: any, language: any}): Promise<any>;
}
