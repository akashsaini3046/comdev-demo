declare module "@salesforce/apex/AutocompleteController.getSuggestions" {
  export default function getSuggestions(param: {sObjectType: any, term: any, fieldsToGet: any, limitSize: any}): Promise<any>;
}
