declare module "@salesforce/apex/CustomerCommunity_HeaderPanelController.getUserName" {
  export default function getUserName(): Promise<any>;
}
