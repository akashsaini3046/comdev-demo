declare module "@salesforce/apex/ADAccountDashboardController.getCurrentYear" {
  export default function getCurrentYear(): Promise<any>;
}
