declare module "@salesforce/apex/CrowleyCommunity_VerticalMenuController.getMenuItems" {
  export default function getMenuItems(): Promise<any>;
}
