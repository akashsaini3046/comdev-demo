declare module "@salesforce/apex/FileAttacherExtensionController.saveAttachment" {
  export default function saveAttachment(param: {parentId: any, fileName: any, base64Data: any, contentType: any}): Promise<any>;
}
declare module "@salesforce/apex/FileAttacherExtensionController.getAttachments" {
  export default function getAttachments(param: {parentId: any}): Promise<any>;
}
