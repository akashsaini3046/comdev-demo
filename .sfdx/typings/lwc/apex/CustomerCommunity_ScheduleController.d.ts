declare module "@salesforce/apex/CustomerCommunity_ScheduleController.getSailingSchedule" {
  export default function getSailingSchedule(): Promise<any>;
}
