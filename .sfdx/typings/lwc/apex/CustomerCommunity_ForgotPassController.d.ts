declare module "@salesforce/apex/CustomerCommunity_ForgotPassController.usernameExists" {
  export default function usernameExists(param: {username: any}): Promise<any>;
}
