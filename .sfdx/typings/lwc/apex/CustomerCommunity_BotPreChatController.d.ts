declare module "@salesforce/apex/CustomerCommunity_BotPreChatController.getCurrentUser" {
  export default function getCurrentUser(): Promise<any>;
}
