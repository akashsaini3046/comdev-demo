declare module "@salesforce/apex/CustomerCommunity_PDFViewerController.getPublicURL" {
  export default function getPublicURL(param: {ShippingInstructionId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_PDFViewerController.getBlobFile" {
  export default function getBlobFile(param: {documentId: any}): Promise<any>;
}
