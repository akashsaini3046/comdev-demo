declare module "@salesforce/apex/ADNotesController.fetchNotes" {
  export default function fetchNotes(param: {accountId: any, objMasterFilter: any}): Promise<any>;
}
declare module "@salesforce/apex/ADNotesController.applyFilter" {
  export default function applyFilter(param: {accountId: any, startDate: any, endDate: any, selectedOwners: any, notesList: any}): Promise<any>;
}
declare module "@salesforce/apex/ADNotesController.applySorting" {
  export default function applySorting(param: {notesList: any, sortedDateOptionSelected: any}): Promise<any>;
}
