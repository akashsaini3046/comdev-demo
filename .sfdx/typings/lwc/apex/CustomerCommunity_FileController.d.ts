declare module "@salesforce/apex/CustomerCommunity_FileController.getValidBookingId" {
  export default function getValidBookingId(param: {bookingName: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.getValidBooking" {
  export default function getValidBooking(param: {bookingName: any, documentType: any, headerSource: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.getAllBookings" {
  export default function getAllBookings(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.readFileContent" {
  export default function readFileContent(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.deleteDocumentRecord" {
  export default function deleteDocumentRecord(param: {recordId: any, documentType: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.softShipApi" {
  export default function softShipApi(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.createShippingInstructionRecord" {
  export default function createShippingInstructionRecord(param: {bookingRecordId: any, bookingName: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.createHazardousDocumentRecord" {
  export default function createHazardousDocumentRecord(param: {bookingRecordId: any, bookingName: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.getFileType" {
  export default function getFileType(param: {contentDocumentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.getExcelFieldLabels" {
  export default function getExcelFieldLabels(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.getExcelFieldValidations" {
  export default function getExcelFieldValidations(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.createBLRecords" {
  export default function createBLRecords(param: {listFormData: any, headerRecordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FileController.createFileRecord" {
  export default function createFileRecord(param: {fileName: any, base64Data: any, contentType: any, headerRecordId: any}): Promise<any>;
}
