declare module "@salesforce/apex/ADOpportunityConversionRateController.getOpportunityConversionRate" {
  export default function getOpportunityConversionRate(param: {idAccountId: any, objMasterFilter: any}): Promise<any>;
}
