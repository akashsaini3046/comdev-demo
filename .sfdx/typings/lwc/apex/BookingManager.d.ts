declare module "@salesforce/apex/BookingManager.handleGetRates" {
  export default function handleGetRates(param: {bookingNumber: any}): Promise<any>;
}
