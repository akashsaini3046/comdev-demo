declare module "@salesforce/apex/CustomerCommunity_BillOfLadingController.fetchBillOfLadingData" {
  export default function fetchBillOfLadingData(param: {recordLimit: any, recordOffset: any, fieldName: any, order: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingController.getTotalNumberOfBillOfLadings" {
  export default function getTotalNumberOfBillOfLadings(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_BillOfLadingController.fetchCommunityName" {
  export default function fetchCommunityName(): Promise<any>;
}
