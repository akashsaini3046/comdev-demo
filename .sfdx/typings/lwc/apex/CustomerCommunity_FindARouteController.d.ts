declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getOriginPorts" {
  export default function getOriginPorts(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.handleTest11" {
  export default function handleTest11(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.handleTest22" {
  export default function handleTest22(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getFCLPorts" {
  export default function getFCLPorts(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getLCLPorts" {
  export default function getLCLPorts(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getFCLPortsMap" {
  export default function getFCLPortsMap(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getLCLPortsMap" {
  export default function getLCLPortsMap(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getSailingWeeks" {
  export default function getSailingWeeks(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getDestinationPorts" {
  export default function getDestinationPorts(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getRoutes" {
  export default function getRoutes(param: {originPort: any, destinationPort: any, shipmentType: any, sailingWeeks: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_FindARouteController.getRoutesTest" {
  export default function getRoutesTest(param: {routeRequestWrapper: any}): Promise<any>;
}
