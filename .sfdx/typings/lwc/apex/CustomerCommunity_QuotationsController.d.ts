declare module "@salesforce/apex/CustomerCommunity_QuotationsController.fetchContactsAndAddresses" {
  export default function fetchContactsAndAddresses(param: {partyCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getContactDetails" {
  export default function getContactDetails(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getCustomerCVIF" {
  export default function getCustomerCVIF(param: {contactRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.fetchIncoTermList" {
  export default function fetchIncoTermList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.fetchReceiptDeliveryTermsList" {
  export default function fetchReceiptDeliveryTermsList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.fetchCommodityList" {
  export default function fetchCommodityList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.fetchContainerTypeList" {
  export default function fetchContainerTypeList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getLocationDetails" {
  export default function getLocationDetails(param: {termCode: any, locCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createBlankCargoRecord" {
  export default function createBlankCargoRecord(param: {cargoRecordList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getCarriers" {
  export default function getCarriers(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getCarrierRates" {
  export default function getCarrierRates(param: {listOfAllCarrierCodes: any, listOfDisplayedQuotes: any, quoteString: any, cargodetailList: any, chargeLineValue: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createRequest" {
  export default function createRequest(param: {newCodeList: any, quotationWrapper: any, cargoWrapperList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.sendRateRequest" {
  export default function sendRateRequest(param: {quoteRequest: any, carriedCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.sendSoftShipRequestFCL" {
  export default function sendSoftShipRequestFCL(param: {quoteWrapperString: any, cargodetailList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createQuoteRecord" {
  export default function createQuoteRecord(param: {quoteWrapperString: any, cargodetailList: any, freightType: any, totalDimensions: any, packageDetails: any, reQuoteId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.sendCargoWiseRequest" {
  export default function sendCargoWiseRequest(param: {quoteWrapperString: any, cargodetailList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createCargoWiseRequest" {
  export default function createCargoWiseRequest(param: {quoteRequestBody: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getCFSLocations" {
  export default function getCFSLocations(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createBlankQuotationRecord" {
  export default function createBlankQuotationRecord(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createCacheRecord" {
  export default function createCacheRecord(param: {quoteWrapperString: any, userId: any, quoteRateId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getCachedRecord" {
  export default function getCachedRecord(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getAllCarrierRates" {
  export default function getAllCarrierRates(param: {listOfDisplayedQuotes: any, quoteString: any, cargodetailList: any, chargeLineValue: any, listChargeLine: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.createLFSCWRequest" {
  export default function createLFSCWRequest(param: {quoteString: any, cargoDetailList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.processResponse" {
  export default function processResponse(param: {labels: any, state: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getReQuoteData" {
  export default function getReQuoteData(param: {quoteId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.getQuoteRates" {
  export default function getQuoteRates(param: {quoteId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsController.saveQuoteRates" {
  export default function saveQuoteRates(param: {fclRatingResponseString: any, lfsRatesBody: any, receiptTerm: any, deliveryTerm: any, rateId: any, freightType: any, quote: any, isInitiatedForBooking: any}): Promise<any>;
}
