declare module "@salesforce/apex/CC_FindARouteController.getLocations" {
  export default function getLocations(param: {type: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_FindARouteController.getSublocations" {
  export default function getSublocations(param: {type: any, loctionCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_FindARouteController.getRoutesList" {
  export default function getRoutesList(param: {originCode: any, destinationCode: any, originType: any, destinationType: any, readyDate: any, receiptTypeCode: any, deliveryTypeCode: any, toSubLocation: any, fromSubLocation: any, description: any}): Promise<any>;
}
