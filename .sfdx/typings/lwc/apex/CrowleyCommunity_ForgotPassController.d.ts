declare module "@salesforce/apex/CrowleyCommunity_ForgotPassController.usernameExists" {
  export default function usernameExists(param: {username: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyCommunity_ForgotPassController.setNewPassword" {
  export default function setNewPassword(param: {idContact: any, password: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyCommunity_ForgotPassController.Login" {
  export default function Login(param: {username: any, password: any, currentURL: any}): Promise<any>;
}
