declare module "@salesforce/apex/CC_IdeaDetailController.getIdeaDetails" {
  export default function getIdeaDetails(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getUser" {
  export default function getUser(): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getZonesList" {
  export default function getZonesList(): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.saveIdeaRecord" {
  export default function saveIdeaRecord(param: {ideaRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.deleteIdeaRecord" {
  export default function deleteIdeaRecord(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.findSimilarIdeas" {
  export default function findSimilarIdeas(param: {communityId: any, title: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getIdeaFieldDescribe" {
  export default function getIdeaFieldDescribe(): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getIdeaDescribe" {
  export default function getIdeaDescribe(): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getIdeaComments" {
  export default function getIdeaComments(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.upvoteIdea" {
  export default function upvoteIdea(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.downvoteIdea" {
  export default function downvoteIdea(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.likeComment" {
  export default function likeComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.unlikeComment" {
  export default function unlikeComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.deleteComment" {
  export default function deleteComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.addComment" {
  export default function addComment(param: {param: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getIdeaCommentRecord" {
  export default function getIdeaCommentRecord(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.saveIdeaCommentRecord" {
  export default function saveIdeaCommentRecord(param: {comment: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getIdeasList" {
  export default function getIdeasList(param: {communityId: any, statuses: any, searchText: any, categories: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.fetchRecordTypeSpecificPickListvalues" {
  export default function fetchRecordTypeSpecificPickListvalues(param: {zoneId: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_IdeaDetailController.getCommunityNetworkName" {
  export default function getCommunityNetworkName(): Promise<any>;
}
