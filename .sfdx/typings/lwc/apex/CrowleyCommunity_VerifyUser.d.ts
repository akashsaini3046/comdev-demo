declare module "@salesforce/apex/CrowleyCommunity_VerifyUser.checkforDuplicate" {
  export default function checkforDuplicate(param: {userName: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyCommunity_VerifyUser.getContactDetails" {
  export default function getContactDetails(param: {conEmail: any}): Promise<any>;
}
