declare module "@salesforce/resourceUrl/verifiedEmail" {
    var verifiedEmail: string;
    export default verifiedEmail;
}