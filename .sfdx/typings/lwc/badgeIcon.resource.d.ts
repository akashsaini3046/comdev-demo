declare module "@salesforce/resourceUrl/badgeIcon" {
    var badgeIcon: string;
    export default badgeIcon;
}