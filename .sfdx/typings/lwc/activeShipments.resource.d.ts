declare module "@salesforce/resourceUrl/activeShipments" {
    var activeShipments: string;
    export default activeShipments;
}