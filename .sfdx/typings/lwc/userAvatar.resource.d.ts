declare module "@salesforce/resourceUrl/userAvatar" {
    var userAvatar: string;
    export default userAvatar;
}