declare module "@salesforce/resourceUrl/analyticsGraph" {
    var analyticsGraph: string;
    export default analyticsGraph;
}