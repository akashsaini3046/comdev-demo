import { LightningElement, api } from 'lwc';

export default class CustomDataTableDataElement extends LightningElement {
    @api rowItem;

    isUrl = false;
    isDate = false;
    isText = false;
    value;
    hrefValue;
    target;

    connectedCallback() {
        let dataType = this.rowItem.dataType;
        if (dataType === "url") {
            this.isUrl = true;
            this.value = this.rowItem.value;
            this.hrefValue = this.rowItem.redirectUrl;
            if (this.rowItem.target) {
                this.target = this.rowItem.target;
            } else {
                this.target = '_blank';
            }
        }
        if (dataType === "text") {
            this.isText = true;
            this.value = this.rowItem.value;
        }
        if (dataType === "date") {
            this.isDate = true;
            if (this.rowItem.dateFormat) {
                let date = new Date(this.rowItem.value);
                let newDate = this.rowItem.dateFormat;
                let dateValue =  this.pad(date.getDate(), 2);
                let yearValue =  this.pad(date.getFullYear(), 4);
                let monthValue =  this.pad(date.getMonth() + 1, 2);
                newDate = newDate.replace('DD', dateValue);
                newDate = newDate.replace('YYYY', yearValue);
                newDate = newDate.replace('MM', monthValue);
                this.value = newDate;
            } else {
                this.value = this.rowItem.value;
            }
        }
    }

    pad(value, size) {
        let s = value.toString();
        while (s.length < (size || 2)) {
            s = "0" + s;
        }
        return s;
    }
}