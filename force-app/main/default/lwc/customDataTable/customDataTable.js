import { LightningElement, track, api } from 'lwc';
import fetchRecords from '@salesforce/apex/UtilForLC.fetchRecords';

export default class CustomDataTable extends LightningElement {
    @track columns = [];
    @track data = [];
    @api jsonString;

    connectedCallback() {
        this.createJsonString();
        this.fetchData();
    }

    createJsonString() {
        this.jsonString = '{"sObjectApiName":"Booking__c","fieldsToFetch":[{"label": "Booking Number","fieldName":"Booking_Number__c","dataType": "url","isTableColumn" : true,"order": 2, "redirectUrl" : "/{Id}", "target":"_self"},{"label": "Booking Date","fieldName":"Booked_Date__c","dataType": "date","isTableColumn" : true,"order": 1,"dateFormat":"DD-MM-YYYY"},{"label": "Origin Type","fieldName":"Origin_Type__c","dataType": "text","isTableColumn" : false,	"order": 3},{"label": "Booking Type","fieldName":"Booking_Type__c","dataType": "text","isTableColumn" : true,"order": 4}],"filters":[{"refId":"1","fieldName":"Booking_Type__c","values":["NB", "ER"],"operator":"IN"}],"filterLogic":"{1}","limitRecords":10,"offset":10,"sortBy":[{"fieldName":"Booking_Number__c","order":"desc"}]}';
    }

    fetchData() {
        fetchRecords({ jsonString: this.jsonString })
            .then(result => {
                this.setData(result);
            })
            .catch(error => {
                console.log('Error ', error);
            });
    }

    setData(result) {
        let json = JSON.parse(this.jsonString);
        let idObj = {
            "label": "Id",
            "order": 0,
            "fieldName": "Id",
            "isTableColumn": false,
            "dataType": "text"
        };
        this.columns.push(idObj);
        for (let key in json.fieldsToFetch) {
            if (json.fieldsToFetch[key].isTableColumn) {
                let columnObj = {};
                columnObj = {
                    "label": json.fieldsToFetch[key].label,
                    "order": json.fieldsToFetch[key].order,
                    "fieldName": json.fieldsToFetch[key].fieldName,
                    "dataType": json.fieldsToFetch[key].dataType,
                    "isTableColumn": json.fieldsToFetch[key].isTableColumn
                };
                if (json.fieldsToFetch[key].dataType === "date") {
                    columnObj["dateFormat"] = json.fieldsToFetch[key].dateFormat;
                } else if (json.fieldsToFetch[key].dataType === "url") {
                    let redirectUrl = json.fieldsToFetch[key].redirectUrl;
                    let target = json.fieldsToFetch[key].target;
                    if (target) {
                        columnObj["target"] = target;
                    }
                    columnObj["redirectUrl"] = redirectUrl;
                }
                this.columns.push(columnObj);
            }
        }
        this.columns = this.sortData("order", "asc", this.columns);

        let finalData = [];
        for (let key1 in result) {
            let rowData = [];
            let row = result[key1];
            let Id;
            for (let key2 in this.columns) {
                let field = this.columns[key2].fieldName;
                let dataType = this.columns[key2].dataType;
                let value = row[field];
                if (field === 'Id') {
                    Id = value;
                } else {
                    if (this.columns[key2].isTableColumn) {
                        if (dataType === 'date') {
                            rowData.push({
                                "value": value,
                                "dataType": dataType,
                                "dateFormat": this.columns[key2].dateFormat
                            });
                        } else if (dataType === 'url') {
                            let target = this.columns[key2].target;
                            let obj = {
                                "value": value,
                                "dataType": dataType,
                                "redirectUrl": this.columns[key2].redirectUrl.replace("{Id}", Id)
                            };
                            if(target){
                                obj["target"] = target;
                            }
                            rowData.push(obj);
                        } else {
                            rowData.push({
                                "value": value,
                                "dataType": dataType
                            });
                        }
                    }
                }
            }
            finalData.push(rowData);
        }
        this.data = finalData;
        this.columns = this.columns.filter(obj => obj.isTableColumn);
        console.log(JSON.stringify(finalData));
    }

    sortData(fieldname, direction, parsedJson) {
        let parseData = parsedJson;
        let keyValue = (a) => {
            return a[fieldname];
        };
        let isReverse = direction === 'asc' ? 1 : -1;
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : '';
            y = keyValue(y) ? keyValue(y) : '';

            return isReverse * ((x > y) - (y > x));
        });
        return parseData;
    }

    onColumnSort(event){
        /*this.data = null;
        this.newjsonString = this.jsonString;
        if(this.newjsonString.sortBy){
            this.newjsonString.sortBy[0].fieldName = event.target.value;
        }
        fetchRecords({ jsonString: this.jsonString })
            .then(result => {
                this.setData(result);
            })
            .catch(error => {
                console.log('Error ', error);
            });*/
    }
}