import { LightningElement, wire, api } from 'lwc';
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import IDEA_OBJECT from '@salesforce/schema/Community';

export default class TestUIAPIRecordType extends LightningElement {

    @api recType;

    @wire(getPicklistValuesByRecordType, { objectApiName: IDEA_OBJECT, recordTypeId: '$recType' })
    wiredValues({ error, data }) {
        if (data) {
            let value = data.picklistFieldValues;
            console.log('LWC Values : ', value);
            this.dispatchEvent(new CustomEvent("picklistvalue", {
                detail: { value }
            }));
        }
        console.log(error);
    }
}