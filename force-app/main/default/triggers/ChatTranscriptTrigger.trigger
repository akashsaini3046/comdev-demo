trigger ChatTranscriptTrigger on LiveChatTranscript (before insert) {
    if(Trigger.isBefore && Trigger.isInsert){
        ChatTranscriptTriggerHandler.populateContact(Trigger.New);
    }
}