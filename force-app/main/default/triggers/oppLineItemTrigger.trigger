trigger oppLineItemTrigger on OpportunityLineItem (after insert, after update, after delete, after undelete) {
    Map<Id, Double> mapOppId = new Map<Id, Double>();
    set<Id> setOppId = new Set<Id>();
    List<Opportunity> listOpp = new List<Opportunity>();
    Map<Id, Double> mapAccount = new Map<Id, Double>();
    List<Account> listAccount = new List<Account>();
    List<OpportunityLineItem> listOLI = new List<OpportunityLineItem>();
    List<AggregateResult> listAggresult = new List<AggregateResult>();
    
    if(Trigger.isInsert || Trigger.isUndelete){
        for(OpportunityLineItem OLI : Trigger.New){
            if(mapOppId.isEmpty() || !mapOppId.containsKey(OLI.OpportunityId))
                mapOppId.put(OLI.OpportunityId, OLI.UnitPrice);
            else if(!mapOppId.isEmpty() && mapOppId.containsKey(OLI.OpportunityId) && OLI.UnitPrice > mapOppId.get(OLI.OpportunityId))
                mapOppId.put(OLI.OpportunityId, OLI.UnitPrice);
        }
    }
    if(Trigger.IsUpdate){
        for(OpportunityLineItem OLI : Trigger.New){
            if(OLI.UnitPrice != Trigger.OldMap.get(OLI.Id).UnitPrice){
                if(mapOppId.isEmpty() || !mapOppId.containsKey(OLI.OpportunityId))
                    mapOppId.put(OLI.OpportunityId, OLI.UnitPrice);
                else if(!mapOppId.isEmpty() && mapOppId.containsKey(OLI.OpportunityId) && OLI.UnitPrice > mapOppId.get(OLI.OpportunityId))
                    mapOppId.put(OLI.OpportunityId, OLI.UnitPrice);
            }
        }
    }
    if(Trigger.isDelete){
        for(OpportunityLineItem OLI : Trigger.Old){
            setOppId.add(OLI.OpportunityId);
        }
    }
    
    if(!mapOppId.isEmpty()){
        listOpp = [SELECT Id, AccountId FROM Opportunity WHERE Id IN :mapOppId.keySet()];
        for(Opportunity opp : listOpp){
            if(opp.AccountId != Null){
                if(mapAccount.isEmpty() || !mapAccount.containsKey(opp.AccountId))
                    mapAccount.put(opp.AccountId, mapOppId.get(opp.Id));
                else if(!mapAccount.isEmpty() && mapAccount.containsKey(opp.AccountId) && mapOppId.get(opp.Id) > mapAccount.get(opp.AccountId))
                    mapAccount.put(opp.AccountId, mapOppId.get(opp.Id));
            }
        }
        if(!mapAccount.isEmpty()){
            for(Id accountId : mapAccount.keySet()){
                Account acc = new Account();
                acc.Id = accountId;
                //acc.Amount = mapAccount.get(accountId);
                listAccount.add(acc);
            }
        }
    }
    
    if(!setOppId.isEmpty()){
        // Write Query to get all the related Opp Line Item for the Opportunities in set
        // Then write aggregare soql to get the maximum Unit Price from Opp Line item
        // Then Update the related parent account with that amount
        
    }
    
    if(!listAccount.isEmpty())
        update listAccount;
}