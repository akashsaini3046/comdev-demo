trigger OpportunityTrigger on Opportunity (before delete, before insert, before update, after insert, after update) {
    new OpportunityTriggerHandler().run();
}