trigger bookingTrigger on Booking__c (before update, after insert, after update) {
    /*Set<Id> setBookingIds = new Set<Id>();
    if(Trigger.isAfter && Trigger.isInsert){
        for(Booking__c bookingRecord : Trigger.New){
            if(bookingRecord.Status__c == 'HazCheck Not Required')
                setBookingIds.add(bookingRecord.Id);
        }
        System.enqueueJob(new CustomerCommunity_CreateCICSBooking(setBookingIds));
        //BookingTriggerHandler.createCICSBooking(setBookingIds); 
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        for(Booking__c bookingRecord : Trigger.New){
            if(bookingRecord.Status__c == 'HazCheck Accepted')
                setBookingIds.add(bookingRecord.Id);
        }
        System.enqueueJob(new CustomerCommunity_CreateCICSBooking(setBookingIds));
        //BookingTriggerHandler.createCICSBooking(setBookingIds);
    }*/
    
    if(Trigger.isBefore && Trigger.isUpdate){
        for(Booking__c booking : Trigger.New){        
            if(booking.Description__c == 'PP'){
                booking.Customer_Origin_Code__c = (String.isBlank(booking.Customer_Origin_Code__c)) ? trigger.oldMap.get(booking.id).Customer_Origin_Code__c : booking.Customer_Origin_Code__c;
                booking.Customer_Destination_Code__c = (String.isBlank(booking.Customer_Destination_Code__c )) ? trigger.oldMap.get(booking.id).Customer_Destination_Code__c : booking.Customer_Destination_Code__c ;
            } else if(booking.Description__c == 'PD'){
                booking.Customer_Origin_Code__c = (String.isBlank(booking.Customer_Origin_Code__c)) ? trigger.oldMap.get(booking.id).Customer_Origin_Code__c : booking.Customer_Origin_Code__c;            
            } else if(booking.Description__c == 'DP'){
                booking.Customer_Destination_Code__c = (String.isBlank(booking.Customer_Destination_Code__c )) ? trigger.oldMap.get(booking.id).Customer_Destination_Code__c : booking.Customer_Destination_Code__c ;
            }
        }
        
        
    }
}