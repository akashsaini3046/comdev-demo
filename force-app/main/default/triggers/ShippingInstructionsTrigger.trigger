trigger ShippingInstructionsTrigger on Header__c (after insert,after update) {    
    if(Trigger.isAfter && Trigger.isInsert){
        Set<ID> setHeaderID = new Set<ID>();
        for(Header__c objHeader: Trigger.New) {
            setHeaderID.add(objHeader.ID);
        }    
        ShippingInstructionsTriggerHandler.updateShortenURL(setHeaderID);     
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        
        List<Header__c> listHeader = new List<Header__c>();
        for(Header__c objHeader: Trigger.New) {
            if(objHeader.Status__c != Trigger.oldMap.get(objHeader.Id).Status__c){
                listHeader.add(objHeader);
            }
        }
        if(!listHeader.isEmpty()){
            ShippingInstructionsTriggerHandler.updateBookingFields(listHeader);
        }
    }   
}