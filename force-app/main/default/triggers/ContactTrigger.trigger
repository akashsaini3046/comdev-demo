trigger ContactTrigger on Contact (before insert, after insert, before update, before delete, after update) {
    new ContactTriggerHandler().run();
}