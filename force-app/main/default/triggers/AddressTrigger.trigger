trigger AddressTrigger on Address__c (before insert, before update, before delete) {
    new AddressTriggerHandler().run();
}