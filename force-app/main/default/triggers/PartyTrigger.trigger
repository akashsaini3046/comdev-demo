trigger PartyTrigger on Party__c (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        PartyTriggerHandler.updateAccountOnBooking(Trigger.New);
     }
}