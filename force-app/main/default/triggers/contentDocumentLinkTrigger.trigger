trigger contentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        contentDocumentLinkTriggerHandler.createContentDistributionRecord(Trigger.New);
        contentDocumentLinkTriggerHandler.uploadToEsker(Trigger.New);
    }
}