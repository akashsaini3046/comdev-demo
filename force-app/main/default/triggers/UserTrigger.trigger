trigger UserTrigger on User (after update) {
    if(Trigger.isAfter && Trigger.isUpdate){
        if(CustomerCommunity_StaticVariables.runOnce()){
            UserTriggerHandler.updateContactEmail(Trigger.New);
        }
    }
}