trigger CaseTrigger on Case (before insert, before update, after insert) {
    //new CaseTriggerHandler().run();
    if(Trigger.isBefore && Trigger.isInsert){        
        system.debug('Trigger.New2233'+Trigger.New);
        CaseTriggerHandler.checkDuplicateCase(Trigger.New);              
        CaseTriggerHandler.contactLinking(Trigger.New);
        CaseTriggerHandler.updateEntitlementId(Trigger.New);
        CaseTriggerHandler.updateCaseDetails(Trigger.New);
    }
    if(Trigger.isBefore){
        CaseTriggerHandler.updateIssueType(Trigger.New);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
       //CaseTriggerHandler.checkDuplicateCase(Trigger.New);  
    }    
    
    
    /*if(Trigger.isBefore && Trigger.isUpdate){
        CaseTriggerHandler.updateEntitlementId(Trigger.New);
    }*/
    if(Trigger.isAfter && Trigger.isInsert &&  ConstantClass.runFutureMethod){
        Set<Id> setCaseId = new Set<Id>();
        for(Case cs : Trigger.New)
            setCaseId.add(cs.Id);
        CaseTriggerHandler.includeCaseType(setCaseId);        
    }
}