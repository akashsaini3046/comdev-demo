public class PartyTriggerHandler {
    public static void updateAccountOnBooking(List < Party__c > listOfParty) {
        Map < String, Booking__c > mapOfCVIFAndBooking = new Map < String, Booking__c > ();
        Map < String, ID > mapOfCVIFAccountId = new Map < String, ID > ();
        Map < ID, String > mapOfBookingAndCVIF = new Map < ID, String > ();
        Set < ID > setOfBookingId = new Set < ID > ();
        Set < String > setOfCVIF = new Set < String > ();
        List < Booking__c > listBooking = new List < Booking__c > ();
        for (Party__c objParty: listOfParty) {
            if (objParty.Type__c == 'SHP') {
                setOfBookingId.add(objParty.Booking__c);
                setOfCVIF.add(objParty.CVIF__c);
                mapOfBookingAndCVIF.put(objParty.Booking__c, objParty.CVIF__c);
            }
        }
        for (Booking__c objbooking: [Select id from Booking__c where id in : setOfBookingId]) {
            mapOfCVIFAndBooking.put(mapOfBookingAndCVIF.get(objbooking.id), objbooking);
        }
        for (Account accRec: [Select id, CVIF__c from Account where CVIF__c in : setOfCVIF]) {
            mapOfCVIFAccountId.put(accRec.CVIF__c, accRec.id);
        }
        if (!mapOfBookingAndCVIF.isEmpty()&& !mapOfCVIFAccountId.isEmpty()) {
            for (Booking__c objbooking: mapOfCVIFAndBooking.values()) {
                if (mapOfBookingAndCVIF.get(objbooking.id) != null) {
                    objbooking.Account__c = mapOfCVIFAccountId.get(mapOfBookingAndCVIF.get(objbooking.id));
                    listBooking.add(objbooking);
                }
            }
        }
        if (!listBooking.isEmpty()) {
            update listBooking;
        }
    }
}