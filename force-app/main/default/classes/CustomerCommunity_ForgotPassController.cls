global without sharing class CustomerCommunity_ForgotPassController {
    
    @AuraEnabled
    public static String usernameExists(String username) {
        List<User> commUser = [SELECT Id, Username FROM User WHERE (Username = :username) AND (Profile.Name = :CustomerCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME 
                         OR Profile.Name = :CustomerCommunity_Constants.CUSTOMER_MEMBER_PROFILE_NAME OR Profile.Name = :CustomerCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME_USER_CUSTOM
                         OR Profile.Name = :CustomerCommunity_Constants.CUSTOMER_PROFILE_NAME_USER_CUSTOM) AND isActive = True LIMIT 1];
        if(!commUser.isEmpty()) {
            Boolean emailSent = Site.forgotPassword(username);
            if(emailSent)
            	return CustomerCommunity_Constants.TRUE_MESSAGE;
            else
                return CustomerCommunity_Constants.ERROR_MESSAGE;
        }
        return CustomerCommunity_Constants.FALSE_MESSAGE;
    }
}