public with sharing class CustomerCommunity_RouteRequest {
    @AuraEnabled public String originPort {get;set;}
    @AuraEnabled public String destPort {get;set;}
    @AuraEnabled public String FclLclData {get;set;}
    @AuraEnabled public String displayRoutes {get;set;}
    @AuraEnabled public String cargoReadyDate {get;set;} 
}