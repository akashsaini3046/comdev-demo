/******************************************************************************************
Class Name:  AddressSearchController
Description: This class is used t0 call google location API for auto complete functionality
******************************************************************************************/
public class AddressSearchController {
    
    //Method to get address details in the initial load
    @AuraEnabled
    public static Address__c getAddressDetailsbyId(String id){
        Address__c ad = new Address__c();
        ad = [select Id, City__c, State__c, Country_Text__c, Postal_Code__c, Address_Line_1__c  from Address__c where Id=: id];
        return ad;
    }
    
    //Method to save address details on click on Save button
    @AuraEnabled
    public static String saveAddressDetailsbyId(String id, Address__c addDetails){
        system.debug('saveAddressDetailsbyId method');
        Address__c obj = [select Id from Address__c where Id=: id];
        obj.City__c = addDetails.City__c;
        obj.State__c = addDetails.State__c;
        obj.Country_Text__c = addDetails.Country_Text__c;
        obj.Postal_Code__c = addDetails.Postal_Code__c;
        obj.Address_Line_1__c = addDetails.Address_Line_1__c;
        update obj;
        return 'Success';
    }
    
    //Method to call google API and fetch the address recommendations 
    @AuraEnabled
    public static String getAddressSet(String SearchText){
        String APIKey = 'AIzaSyDeKRpiLf5V6qUaPWeUbDrwC1wMTGD6Ry8';
        String result = null;
        system.debug('SearchText is ' + SearchText);
        try{
            if(SearchText != null){
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + SearchText.replace(' ', '%20') + '&key=' + APIKey; 
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('res-->'+res.getBody()+'statusCode is ' + statusCode);
                if(statusCode == 200){
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e){
            //Handling exception
            system.debug(e.getMessage());
        }
        return result;
    }
    
    //Method to call google API and fetch the address details by addressID 
    @AuraEnabled
    public static String getAddressDetailsByPlaceId(String PlaceID){
        String APIKey = 'AIzaSyDeKRpiLf5V6qUaPWeUbDrwC1wMTGD6Ry8';
        String result = null;
        system.debug('SearchText is ' + PlaceID);
        try{
            if(PlaceID != null){
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + PlaceId.replace(' ', '%20') + '&key=' + APIKey; 
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('res11111111-->'+res.getBody()+'statusCode is ' + statusCode);
                if(statusCode == 200){
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e){
            //Handling exception
            system.debug(e.getMessage());
        }
        return result;
    }
}