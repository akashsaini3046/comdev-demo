public class CustomerCommunity_FARChatbot {
    public class RouteParameters {
        @InvocableVariable(required=true)
        public String sShipment;
        @InvocableVariable(required=true)
        public String sOrigin;
        @InvocableVariable(required=true)
        public String sDestination;
        @InvocableVariable(required=true)
        public String sWeeks;
    }
    public class ResponseData {
        @InvocableVariable(required=true)
        public String sResponseRoutes;
    }
    @InvocableMethod(label='Find Route')
    public static List<ResponseData> FindARoute(List<RouteParameters> routeParam) {
        String ServiceName = CustomerCommunity_Constants.FAR_SERVICE_NAME;
        List<ResponseData> listResponses = new List<ResponseData>();
        ResponseData routeResponse = new ResponseData();
        String rbody = CustomerCommunity_Constants.EMPTY_STRING;
        String response = CustomerCommunity_Constants.EMPTY_STRING;
        Date cargoSailDate = System.today();
        Map<String, Integer> mapFromDate = new Map<String, Integer>();
        mapFromDate.put('2nd Week',1); mapFromDate.put('3rd Week',2); mapFromDate.put('4th Week',3); mapFromDate.put('5th Week',4); mapFromDate.put('6th Week',5);
        Integer iterate=1;
        try{
            Schema.DescribeFieldResult portFieldResult = Find_Route__c.Origin_Port__c.getDescribe();
            List<Schema.PicklistEntry> portList = portFieldResult.getPicklistValues();
            for (Schema.PicklistEntry port: portList) {
                if(port.getLabel().substring(4).equalsIgnoreCase(routeParam[0].sOrigin) || port.getValue().equalsIgnoreCase(routeParam[0].sOrigin)) {
                    routeParam[0].sOrigin = port.getValue();
                }
                if(port.getLabel().substring(4).equalsIgnoreCase(routeParam[0].sDestination) || port.getValue().equalsIgnoreCase(routeParam[0].sDestination)) {
                    routeParam[0].sDestination = port.getValue();
                }
            }
            if(!mapFromDate.isEmpty() && mapFromDate.containsKey(routeParam[0].sWeeks) && mapFromDate.get(routeParam[0].sWeeks) != Null && mapFromDate.get(routeParam[0].sWeeks) != 0){
                cargoSailDate = Date.valueOf(System.now() + ((mapFromDate.get(routeParam[0].sWeeks)*7) - Integer.valueOf(System.now().format('F'))));
                routeParam[0].sWeeks = '6 weeks';
            }
            rbody  = '{"originPort":"' + routeParam[0].sOrigin + '","destPort":"' + routeParam[0].sDestination + '","cargoReadyDate":"' + cargoSailDate + '","FclLclData":"' + routeParam[0].sShipment.toUpperCase() + '","displayRoutes":"' + routeParam[0].sWeeks +'"}';     
            response = CustomerCommunity_APICallouts.getAPIResponse(ServiceName, rbody, Null);
            List<CustomerCommunity_RouteInformation> routeInformationList=(List<CustomerCommunity_RouteInformation>)JSON.deserialize(response, List<CustomerCommunity_RouteInformation>.class);
            if(routeInformationList.size()>0)
            {
                routeResponse.sResponseRoutes = CustomerCommunity_Constants.EMPTY_STRING;
                for(CustomerCommunity_RouteInformation currentRoute : routeInformationList) {
                    routeResponse.sResponseRoutes = routeResponse.sResponseRoutes + CustomerCommunity_Constants.FAR_RESPONSE_STARTDATE + currentRoute.startDate + CustomerCommunity_Constants.FAR_RESPONSE_ARRIVALDATE + currentRoute.arrivalDate + CustomerCommunity_Constants.FAR_RESPONSE_TRANSITTIME + currentRoute.transitTime + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                    iterate=1;
                    for(CustomerCommunity_DetailInfo currentRouteDetails : currentRoute.details) {
                        if(currentRoute.details.size() == 1)
                            routeResponse.sResponseRoutes = routeResponse.sResponseRoutes + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                        else if(currentRoute.details.size() != iterate && iterate == 1)
                            routeResponse.sResponseRoutes = routeResponse.sResponseRoutes + currentRouteDetails.originPort + CustomerCommunity_Constants.FAR_RESPONSE_RIGHTARROW + currentRouteDetails.destinationPort + CustomerCommunity_Constants.FAR_RESPONSE_RIGHTARROW;
                        else if(currentRoute.details.size() != iterate && iterate != 1)
                            routeResponse.sResponseRoutes = routeResponse.sResponseRoutes + currentRouteDetails.destinationPort + CustomerCommunity_Constants.FAR_RESPONSE_RIGHTARROW;
                        else
                            routeResponse.sResponseRoutes = routeResponse.sResponseRoutes + currentRouteDetails.destinationPort + CustomerCommunity_Constants.NEWLINE_CHARACTER + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                        iterate++;
                    }
                }
                listResponses.add(routeResponse);
            }
            else {
                routeResponse.sResponseRoutes = Label.CustomerCommunity_FARChatbotErrorMessage;
                listResponses.add(routeResponse);
            }
            return listResponses;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            routeResponse.sResponseRoutes = Label.CustomerCommunity_FARChatbotErrorMessage;
            listResponses.add(routeResponse);
            return listResponses;
        }
    }
}