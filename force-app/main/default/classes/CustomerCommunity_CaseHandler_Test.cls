@isTest
public class CustomerCommunity_CaseHandler_Test {
    
    @testSetup static void methodName() {
        Account accObj = TestDataUtility.createAccount('Test Account', null, null, false, 1)[0];
        User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
        Case caseObj = TestDataUtility.createCase(accObj.Id, 'Web', currentUser.Id, 'New', 1)[0];
    }
    
    static testMethod void fetchCaseRecords_TestMethod(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs ( thisUser )
        {
            User communityUser = TestDataUtility.createCommunityUser('Test12','testCommunity@email.com');
            test.startTest();
            System.runAs(communityUser)
            {
                CustomerCommunity_CaseHandler.fetchCaseRecords();
            }
            test.stopTest();
        }
    }
    
    static testMethod void getQuickLinks_TestMethod(){
        Test.startTest();
        CustomerCommunity_CaseHandler.getQuickLinks();
        Test.stopTest();
    }
    
    static testMethod void updateUserDetailsOnCase_TestMethod(){        
        Case caseObj = [Select Id from Case];
        System.assertNotEquals(NULL, caseObj.Id);
        
    //    User communityUser = TestDataUtility.createCommunityUser();
        Test.startTest();
     //   System.runAs(communityUser)
        {
            CustomerCommunity_CaseHandler.updateUserDetailsOnCase(new List<Case>{caseObj});
        }
        Test.stopTest();
    }
}