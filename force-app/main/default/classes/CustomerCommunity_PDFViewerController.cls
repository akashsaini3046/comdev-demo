public class CustomerCommunity_PDFViewerController {
    @AuraEnabled
	public static Map<String,String> getPublicURL(Id ShippingInstructionId) {
        Map<String,String> publicUrlPagesMap = new Map<String,String>();
        String  BASE_URL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('BASE_URL  : '+BASE_URL);
        String publicUrl = '';
        List<ContentDocumentLink> listContentDocumentLink = new List<ContentDocumentLink>();
        List<ContentDistribution> listContentDistribution = new List<ContentDistribution>();
        try{
            if(ShippingInstructionId != Null){
                listContentDocumentLink = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :ShippingInstructionId LIMIT 1];
                if(!listContentDocumentLink.isEmpty()){
                    listContentDistribution = [SELECT Id,DistributionPublicUrl,PdfDownloadUrl,ContentVersionId,ContentDownloadUrl FROM ContentDistribution 
                                               WHERE ContentDocumentId = :listContentDocumentLink[0].ContentDocumentId];
                   
                    if(!listContentDistribution.isEmpty() && listContentDistribution[0].DistributionPublicUrl != Null){
                        ContentVersion ver = [SELECT id,VersionData FROM ContentVersion WHERE ContentDocumentId = : listContentDocumentLink[0].ContentDocumentId and id=:listContentDistribution[0].ContentVersionId LIMIT 1];
                    	publicUrlPagesMap.put('Base64Data',EncodingUtil.base64Encode(ver.VersionData));
                        //System.debug('>>>' + EncodingUtil.base64Decode(EncodingUtil.base64Encode(ver.VersionData)).toString());
                        //publicUrl = listContentDistribution[0].DistributionPublicUrl;
                        //
                        //
                        //List<ContentDistribution> listContentDistribution = [SELECT Id,DistributionPublicUrl,PdfDownloadUrl,ContentVersionId FROM ContentDistribution  WHERE ContentDocumentId = '0690t000001EjPNAA0' limit 1];
                        
                        if(listContentDistribution[0].PdfDownloadUrl != null)
                            publicUrl = listContentDistribution[0].PdfDownloadUrl;
                        else
                            publicUrl = listContentDistribution[0].ContentDownloadUrl;
                        System.debug('public URL initial : '+publicUrl);
                        List<String> listStr= publicUrl.split('/version/');
                        List<String> listStr1= listContentDistribution[0].DistributionPublicUrl.split('/a/');   
                        System.debug('listStr'+listStr);
                        System.debug('listStr1'+listStr1);
                        publicUrl = listStr[0]+'/version/renditionDownload?rendition=SVGZ&operationContext=DELIVERY&dpt=null&viewId=&versionId='+listContentDistribution[0].ContentVersionId+'&oid='+UserInfo.getOrganizationId();
                        publicUrl+='&d=/a/'+listStr1[1];
                        publicUrl+='&page=';  
                        publicUrlPagesMap.put('publicURL',publicUrl);
                    }
                }
            }
            System.debug('publicUrl --> '+publicUrl);
            System.debug('publicUrlPagesMap --> '+publicUrlPagesMap);
            return publicUrlPagesMap;
        }
        catch (Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return Null;
		}
	}
    

     @AuraEnabled
    public static String getBlobFile(Id documentId){
        try {
            ContentVersion ver = [SELECT id,VersionData FROM ContentVersion WHERE ContentDocumentId = : documentId LIMIT 1];
            //return EncodingUtil.convertToHex(ver.VersionData);
            return EncodingUtil.base64Encode(ver.VersionData);
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
}