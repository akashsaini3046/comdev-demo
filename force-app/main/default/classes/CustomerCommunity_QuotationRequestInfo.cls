public class CustomerCommunity_QuotationRequestInfo {
		public LTLRateShipmentSimple LTLRateShipmentSimple{get;set;}
	public class LTLRequestDetail{
		public String weight{get;set;}
		public String nmfcClass{get;set;}
	}
	public class LTLRateShipmentSimpleRequest{
		public String originCountry{get;set;}
		public String shipmentDateCCYYMMDD{get;set;}
		public String originPostalCode{get;set;}
		public String shipmentID{get;set;}
		public details details{get;set;}
		public String tariffName{get;set;}
		public String destinationPostalCode{get;set;}
		public String destinationCountry{get;set;}
	}
	public class LTLRateShipmentSimple{
		public LTLRateShipmentSimpleRequest LTLRateShipmentSimpleRequest{get;set;}
	}
	public class details{
		public LTLRequestDetail LTLRequestDetail{get;set;}
	}
}