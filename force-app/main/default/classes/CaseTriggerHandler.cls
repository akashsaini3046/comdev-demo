public class CaseTriggerHandler {   
    
    public static void updateEntitlementId (List<Case> list_Case) {
        for(Case objCase : list_Case){
            System.debug('@@@@@ '+objCase.ContactSupportSubscription__c);
            //System.debug('objCase.Contact.Support_Subscription__c -- @@ '+objCase.Contact.Support_Subscription__c );
            if(objCase.ContactSupportSubscription__c == 'Premium'){
                //objCase.EntitlementId = '5500t000000t2Xa';
                if(objCase.Priority == 'Low')
                    objCase.EntitlementId = System.Label.Premium_Low;
                else if(objCase.Priority == 'Medium')
                    objCase.EntitlementId = System.Label.Premium_Medium;
                else if(objCase.Priority == 'High')
                    objCase.EntitlementId = System.Label.Premium_High;
            }
            else if (objCase.ContactSupportSubscription__c == 'Standard'){
                //objCase.EntitlementId = '5500t000000t2XV';
                if(objCase.Priority == 'Low')
                    objCase.EntitlementId = System.Label.Standard_Low;
                else if(objCase.Priority == 'Medium')
                    objCase.EntitlementId = System.Label.Standard_Medium;
                else if(objCase.Priority == 'High')
                    objCase.EntitlementId = System.Label.Standard_High;
                //objCase.EntitlementId = System.Label.Entitlement_General_Label;
            }
        }
    }
    public static void updateIssueType (List<Case> list_Case) {
        for(Case objCase : list_Case){
            if(objCase.Origin.contains('Email -')){
                list<string> str = objCase.Origin.split(' - ');
                objCase.Type = str[1];
            }
        }
    }
    public static void updateCaseDetails(List<Case> newCase){
        CustomerCommunity_CaseHandler.updateUserDetailsOnCase(newCase);
    }
    
    public static void contactLinking (List<Case> caseList){
        System.debug('INSIDE CONTACT LINKING'+caseList);
        
        /*Map<String, Contact> contactMap = new Map<String, Contact>();
for(Contact c: [SELECT Id, Email FROM Contact]){
contactMap.put(c.Email, c);
}       
List<Contact> contactList = new List<Contact>();
Map<String, Case> emailIdCaseMap = new Map<String, Case>();
for(Case objCase : caseList){
System.debug('objCase.ContactId->@@ '+objCase.Contact.Name);
if(contactMap!=null && contactMap.keySet()!=null && !contactMap.keySet().isEmpty() && !contactMap.containsKey(objCase.SuppliedEmail)){
contactList.add(new Contact(LastName = objCase.SuppliedName, email = objCase.SuppliedEmail));
}
}
List<Account> accountList = new List<Account>();
if(contactList!=null && !contactList.isEmpty()){
insert contactList;
for(Contact con: contactList){
contactMap.put(con.Email, con);
}
}
for(Case objCase : caseList){
if(contactMap!=null && contactMap.keySet()!=null && !contactMap.keySet().isEmpty() && contactMap.containsKey(objCase.SuppliedEmail)){
objCase.ContactId = contactMap.get(objCase.SuppliedEmail).Id;
System.debug('objCase.ContactId->@@@@ '+objCase.Contact.Name);
}
} */
        Set<String> setEmail = new Set<String>();
        Set<String> setPhone = new Set<String>();
        Set<Id> setAccountId = new Set<Id>();
        List<Contact> listContact = new List<Contact>();
        Map<String, Contact> mapContactEmail = new Map<String, Contact>();
        Map<String, Contact> mapContactPhone = new Map<String, Contact>();
        Map<Id, Account> mapAccount;
        Map<Id, Account> mapAccountContact = new Map<Id, Account>();
        for(Case cs : caseList) {
            if(cs.SuppliedEmail != Null)
                setEmail.add(cs.SuppliedEmail);
            if(cs.SuppliedPhone != Null)
                setPhone.add(cs.SuppliedPhone);
        }
        if(!setEmail.isEmpty()) {
            listContact = [SELECT Id, Email, Phone, MobilePhone, Language_Preference__c, AccountId FROM Contact WHERE Email IN :setEmail OR Phone IN :setPhone OR MobilePhone IN :setPhone];
            for(Contact con : listContact) {
                if(con.AccountId != Null)
                    setAccountId.add(con.AccountId);
            }
            if(!listContact.isEmpty()) {
                if(!setAccountId.isEmpty())
                    mapAccount = new Map<Id, Account>([SELECT Id, Region__c FROM Account WHERE Id IN :setAccountId]);
                for(Contact con : listContact) {
                    if(con.Email != Null)
                        mapContactEmail.put(con.Email, con);
                    if(con.Phone != Null)
                        mapContactPhone.put(con.Phone, con);
                    if(con.MobilePhone != Null)
                        mapContactPhone.put(con.MobilePhone, con);
                    if(!mapAccount.isEmpty() && mapAccount.containsKey(con.AccountId)) 
                        mapAccountContact.put(con.Id, mapAccount.get(con.AccountId));
                }
                if(!mapContactEmail.isEmpty() || !mapContactPhone.isEmpty()) {
                    for(Case cs : caseList) {
                        if(!mapContactPhone.isEmpty() && mapContactPhone.containsKey(cs.SuppliedPhone)) {
                            cs.ContactId = mapContactPhone.get(cs.SuppliedPhone).Id;
                            if(mapContactPhone.get(cs.SuppliedPhone).Language_Preference__c != Null && cs.Preferred_Language__c == Null)
                                cs.Preferred_Language__c = mapContactPhone.get(cs.SuppliedPhone).Language_Preference__c;
                            if(!mapAccountContact.isEmpty() && mapAccountContact.containsKey(mapContactPhone.get(cs.SuppliedPhone).Id) && mapAccountContact.get(mapContactPhone.get(cs.SuppliedPhone).Id).Region__c != Null && cs.Company_Region__c == Null) 
                                cs.Company_Region__c = mapAccountContact.get(mapContactPhone.get(cs.SuppliedPhone).Id).Region__c;
                        }
                        if(!mapContactEmail.isEmpty() && mapContactEmail.containsKey(cs.SuppliedEmail)) {
                            cs.ContactId = mapContactEmail.get(cs.SuppliedEmail).Id;
                            if(mapContactEmail.get(cs.SuppliedEmail).Language_Preference__c != Null && cs.Preferred_Language__c == Null)
                                cs.Preferred_Language__c = mapContactEmail.get(cs.SuppliedEmail).Language_Preference__c;
                            if(!mapAccountContact.isEmpty() && mapAccountContact.containsKey(mapContactEmail.get(cs.SuppliedEmail).Id) && mapAccountContact.get(mapContactEmail.get(cs.SuppliedEmail).Id).Region__c != Null && cs.Company_Region__c == Null) 
                                cs.Company_Region__c = mapAccountContact.get(mapContactEmail.get(cs.SuppliedEmail).Id).Region__c;
                        }
                    }
                }
            }
        }
    }
    @future
    public static void includeCaseType(Set<Id> setCaseId) {
        //Deprected
    }
    
    public static void checkDuplicateCase (List<Case> list_Case) {
        system.debug('testDebug::list_Case'+list_Case);
        set<string> caseSubject = new set<string>();
        for(case c : list_Case){
            caseSubject.add(c.subject);
        }
        
        List<case> caselist = [select id,status,contactId,accountid,subject from case where subject in: caseSubject and status !=null ];
        system.debug('caselist'+caselist);
        if(caselist != null){
            for(case c:list_Case){
                for(case cList : caseList){
                    system.debug('c.subject'+c.subject);
                    system.debug('cList.Subject'+cList.Subject);
                    if(c.subject == cList.Subject && c.contactId==cList.ContactId){
                        c.adderror('Case is already exist with same subject.Please choose another subject.');
                    }                       
                }                      
            }                
        }              
    }    
}