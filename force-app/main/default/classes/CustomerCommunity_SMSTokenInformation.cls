public class CustomerCommunity_SMSTokenInformation {
    @AuraEnabled public String accessToken{get; set;}
    @AuraEnabled public String expiresIn{get; set;}
}