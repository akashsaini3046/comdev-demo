public class CustomerCommunity_CICSBookingChatbot {
    /*public class InputFields {
        @InvocableVariable(required=true)
        public String sBookingRequestNumber;
    }  
    public class BookingResponse {
        @InvocableVariable(required=true)
        public String sBookingNumber;
    }
    @InvocableMethod(label='Confirm Re Booking')
    public static List<BookingResponse> confirmReBooking(List<InputFields> inputParam) {
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_CICSBookingChatbot:confirmReBooking() with parameters'); 
        List<Booking__c> listBookingRecord = new List<Booking__C>();
        List<BookingResponse> listResponses = new List<BookingResponse>();
        BookingResponse bookingResponseValue = new BookingResponse();
        bookingResponseValue.sBookingNumber = CustomerCommunity_Constants.EMPTY_STRING;
        
        try{
            listBookingRecord = [SELECT Id, Booking_Number__c, Name FROM Booking__c WHERE Name = :inputParam[0].sBookingRequestNumber LIMIT 1];
            if(!listBookingRecord.isEmpty()){
                listBookingRecord[0] = CustomerCommunity_CreateCICSBooking.CreateCICSBooking(listBookingRecord[0].Id);
                bookingResponseValue.sBookingNumber = bookingResponseValue.sBookingNumber + 'Your Booking is confirmed with Booking Number - ' + listBookingRecord[0].Booking_Number__c + '. Details here - https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/booking/' + listBookingRecord[0].Id; 
            }
            else{
                bookingResponseValue.sBookingNumber = bookingResponseValue.sBookingNumber + 'Booking with Request Number : ' + inputParam[0].sBookingRequestNumber + ' could not be found.';
            }
            listResponses.add(bookingResponseValue);
            System.debug(LoggingLevel.INFO,'Exiting from CustomerCommunity_CICSBookingChatbot:confirmReBooking() returning--'+listResponses);
            return listResponses;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            bookingResponseValue.sBookingNumber = bookingResponseValue.sBookingNumber + 'Booking with Request Number : ' + inputParam[0].sBookingRequestNumber + ' could not be confirmed. Please try again and use the provided Booking Request Number for any further Reference.';
            listResponses.add(bookingResponseValue);
            return listResponses;
        } 
    }*/
    @future(callout=true)
    public static void confirmReBooking(String bookingName){
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_CICSBookingChatbot:confirmReBooking() with parameters'); 
        List<Booking__c> listBookingRecord = new List<Booking__C>();
        
        try{
            listBookingRecord = [SELECT Id, Booking_Number__c, Name FROM Booking__c WHERE Name = :bookingName LIMIT 1];
            if(!listBookingRecord.isEmpty()){
                listBookingRecord[0] = CustomerCommunity_CreateCICSBooking.CreateCICSBooking(listBookingRecord[0].Id, false);
            }
            System.debug(LoggingLevel.INFO,'Exiting from CustomerCommunity_CICSBookingChatbot:confirmReBooking()');
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
        } 
    }
}