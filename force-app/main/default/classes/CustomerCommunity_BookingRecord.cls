public class CustomerCommunity_BookingRecord {
    @AuraEnabled public String receiptTermVal{get; set;}
    @AuraEnabled public String deliveryTermVal{get; set;}
    @AuraEnabled public String bookingRequestNumber{get; set;}
    @AuraEnabled public String customerId{get; set;}
    @AuraEnabled public String customerContactId{get; set;}
    @AuraEnabled public String bookingType{get; set;}
    @AuraEnabled public Contact customerContactRecord{get; set;}
    @AuraEnabled public Address__c customerAddressRecord{get; set;}
    @AuraEnabled public String shipperId{get; set;}
    @AuraEnabled public String shipperContactId{get; set;}
    @AuraEnabled public Contact shipperContactRecord{get; set;}
    @AuraEnabled public Address__c shipperAddressRecord{get; set;}
    @AuraEnabled public String consigneeId{get; set;}
    @AuraEnabled public String consigneeContactId{get; set;}
    @AuraEnabled public Contact consigneeContactRecord{get; set;}
    @AuraEnabled public Address__c consigneeAddressRecord{get; set;}
    @AuraEnabled public String originCode{get; set;}
    @AuraEnabled public String destinationCode{get; set;}
    @AuraEnabled public DateTime estSailingDate{get; set;}
    @AuraEnabled public List<Requirement__c> requirementRecords{get; set;}
    @AuraEnabled public List<Commodity__c> commodityRecords{get; set;}
    @AuraEnabled public List<PackingLine__c> listPackingLineRecords{get; set;}
    @AuraEnabled public Double totalVolume{get; set;}
    @AuraEnabled public Double totalWeight{get; set;}
    @AuraEnabled public String totalVolumeUnit{get; set;}
    @AuraEnabled public String totalWeightUnit{get; set;}
    @AuraEnabled public Contact contactRecord{get; set;}
    @AuraEnabled public List<ContainerCargoDetail> listCargoDetails{get; set;}
    
    public class ContainerCargoDetail{
        @AuraEnabled public Integer sequenceId{get; set;}
        @AuraEnabled public String UniqueId{get; set;}
        @AuraEnabled public Boolean isHazardous{get; set;}
        @AuraEnabled public FreightDetail__c freightRecord{get; set;}
        @AuraEnabled public Requirement__c requirementRecord{get; set;}
        @AuraEnabled public List<Commodity__c> listCommodityRecords{get; set;}
    }
}