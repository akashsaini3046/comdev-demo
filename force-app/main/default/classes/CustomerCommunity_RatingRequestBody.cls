public class CustomerCommunity_RatingRequestBody {
    /*public Booking Booking{get; set;}
    public Login Login{get; set;}
    public String Action{get; set;}
    public String CalcRule{get; set;}
    public class Booking {
        public CodeValue BookingAgency{get; set;}
        public CodeValue FromVal{get; set;}
        public CodeValue To{get; set;}
        public RequestedBookingRoute RequestedBookingRoute{get; set;}
        public SingleContainer SingleContainer{get; set;}
        public CodeValue CurrencyVal{get; set;}
        public NumberData PolSequenceNumber{get; set;}
        public NumberData PodSequenceNumber{get; set;}
    }
    public class Login {
        public String Username{get; set;}
        public String Password{get; set;}
    }
    public class CodeValue {
        public String Code {get; set;}
    }
    public class RequestedBookingRoute {
        public List<Legs> Legs{get; set;}
        public LocationCodeVal StartLocation{get; set;}
        public LocationCodeVal EndLocation{get; set;}
        public Integer RouteType{get; set;}
    }
    public class Legs {
        public LegSequence LegSequence {get; set;}
        public LocationCodeVal StartLocation{get; set;}
        public LocationCodeVal EndLocation{get; set;}
        public String ReceiptTermCode{get; set;}
        public String DeliveryTermCode{get; set;}
        public String ReadyDate{get; set;}
    }
    public class LegSequence {
        public Integer Sequence{get; set;}
    }
    public class LocationCodeVal {
        public CodeValue LocationCode{get; set;}
    }
    public class SingleContainer {
        public Container Container{get; set;}
        public List<Container> Cargo{get; set;}
        public Integer Count{get; set;}
        public CargoItemId CargoItemId{get; set;}
        public Integer PrincipalVoucherPositionId{get; set;}
        public Integer AoIVoucherPositionId{get; set;}
    }
    public class Container {
        public IdVal Id{get; set;}
        public ContainerType ContainerType{get; set;}
        public CodeValue LoadingType{get; set;}
        public Weight ContainerGrossWeight{get; set;}
        public Weight Tare{get; set;}
        public OperationalStatus OperationalStatus{get; set;}
        public CodeValue Commodity{get; set;}
        public Weight CargoGrossWeight{get; set;}
        public KindOfPackage KindOfPackage{get; set;}
        public NumberData NumberOfPackage{get; set;}
    }
    public class IdVal {
        public Integer ContainerRunningNumber{get; set;}
        public CargoItemId CargoItemId{get; set;}
    }
    public class CargoItemId {
        public Integer ItemRunningNumber{get; set;}
    }
    public class ContainerType {
        public CodeValue ContainerTypeCode{get; set;}
        public Boolean IsReefer{get; set;}
    }
    public class Weight {
        public Double Value{get; set;}
        public Integer Unit{get; set;}
    }
    public class OperationalStatus {
        public String Status{get; set;}
    }
    public class KindOfPackage{
        public CodeValue Code{get; set;}
    }
    public class NumberData {
        public Integer NumberVal{get; set;}
    }*/
    public Booking Booking{get; set;}
    public Login Login{get; set;}
    public String Action{get; set;}
    public String CalcRule{get; set;}
    public class Booking {
        public RequestedBookingRoute RequestedBookingRoute{get; set;}
        public SingleContainer BookingCargo{get; set;}
        public CodeValue Currency_PostFix{get; set;}
        public Customer Customer{get; set;}
        public Contract Contract{get; set;}
    }
    public class Login {
        public String Username{get; set;}
        public String Password{get; set;}
    }
    public class CodeValue {
        public String Code {get; set;}
    }
    public class RequestedBookingRoute {
        public Legs Legs{get; set;}
        public Integer RouteType {get; set;}
    }
    public class Legs {
        //public List<legsValues> aads{get; set;}
        public List<legsValues> prefixDollar_values{get; set;}
        public String prefixDollar_type{get; set;}
    }
	
	public class legsValues {
        public LegSequence LegSequence {get; set;}
        public LocationCodeVal StartLocation{get; set;}
        public LocationCodeVal EndLocation{get; set;}
        public String ReceiptTermCode{get; set;}
        public String DeliveryTermCode{get; set;}
        public String ReadyDate{get; set;}
    }
	
    public class LegSequence {
        public Integer Sequence{get; set;}
    }
    public class LocationCodeVal {
        public CodeValue LocationCode{get; set;}
    }
    public class SingleContainer {
		public String prefixDollar_type{get; set;}
        public Integer Count{get; set;}
        public NumberData CargoItemId{get; set;}
        public Container Container{get; set;}
    }
    public class Container {
        public IdData Id_PostFix{get; set;}
        public ContainerType ContainerType{get; set;}        
        public CodeValue Commodity{get; set;}
        public CodeValue LoadingType{get; set;}
        public KindOfPackage KindOfPackage{get; set;}
        public NumberOfPackage NumberOfPackage{get; set;}
        public Weight ContainerGrossWeight{get; set;}
        public OperationalStatus OperationalStatus{get; set;}

    }
    public class ContainerType {
        public CodeValue ContainerTypeCode{get; set;}
        public Boolean IsReefer{get; set;}
        public CodeValue LoadingType{get; set;}
    }
    public class KindOfPackage{
        public String Code{get; set;}
    }
    public class NumberOfPackage{
        public Integer Number_PostFix{get; set;}
    }
    
    public class IdData {
        public Integer ContainerRunningNumber{get; set;}
        public NumberData CargoItemId{get; set;}
        
    }
    public class NumberData {
        public Integer ItemRunningNumber{get; set;}
    }
    
    public class Weight{
        public Integer Value_PostFix {get; set;}
        public Integer Unit {get; set;}
    }
    public class OperationalStatus{
        public String Status {get; set;}
    }
    public class Customer{
        public CodeValue CustomerCode{get; set;}
    }
    public class Contract{
        public String ContractNumber{get; set;}
    }
}