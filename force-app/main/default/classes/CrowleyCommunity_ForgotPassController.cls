global without sharing class CrowleyCommunity_ForgotPassController {
    @AuraEnabled
    public static String usernameExists(String username) {
        List<User> commUser = new List<User>();
        try{
            commUser = [SELECT Id, Username FROM User WHERE (Username = :username) LIMIT 1];
            if(!commUser.isEmpty()) {
                Boolean emailSent = Site.forgotPassword(username);
                System.debug('emailSent-->'+ emailSent);
                if(emailSent)
                    return CustomerCommunity_Constants.TRUE_MESSAGE;
                else
                    return CustomerCommunity_Constants.ERROR_MESSAGE;
            }
            return CustomerCommunity_Constants.FALSE_MESSAGE;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CustomerCommunity_Constants.FALSE_MESSAGE;
        }
    }
    @AuraEnabled 
    public static String setNewPassword(String idContact,String password){
       List<User> commUser = new List<User>();
        try{ 
            if(idContact.contains('003')){
                commUser = [SELECT Id, Username FROM User WHERE (contactId  = :idContact) LIMIT 1];
            }
            else if(idContact.contains('005')){
               commUser = [SELECT Id, Username FROM User WHERE (id  = :idContact) LIMIT 1];
            }
            if(!commUser.isEmpty()){
                System.setPassword(commUser[0].Id,password);
                return commUser[0].Username;
            }
            else{
                return CustomerCommunity_Constants.FALSE_MESSAGE;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CustomerCommunity_Constants.FALSE_MESSAGE; 
        }
    }
    @AuraEnabled 
    public static String Login(String username,String password,String currentURL){
        try{
           String navigateUser = CrowleyCommunity_LoginController.checkPortal(username,password,currentURL);
            if(navigateUser == 'True'){
              return CustomerCommunity_Constants.TRUE_MESSAGE;  
            } 
            else{
              return CustomerCommunity_Constants.FALSE_MESSAGE;  
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CustomerCommunity_Constants.FALSE_MESSAGE;  
        }   
    }
}