@isTest
public with sharing class BookingObjectsDAOTest {
    
    @testSetup static void setup() {
        List<Booking__c> bookings = TestDataUtility.getBookingWithChildren('DP', 'BBULK', 2);
        List<Ports_Information__c> portInformations = TestDataUtility.getPortInformationRecords('Port Terminal', 1, true);
    }
    
    @isTest
    static void getBookingDataTest() {
        Booking__c booking = [SELECT Id, Name FROM Booking__c LIMIT 1];
        Test.startTest();
        Booking__c fetchedBooking = BookingObjectsDAO.getBookingData(booking.Id);
        Test.stopTest();
        System.assertEquals(booking.Name, fetchedBooking.Name, 'Booking Name Check');
    }
    
    @isTest
    static void getStopsListTest() {
        Booking__c booking = [SELECT Id, (SELECT Id FROM Transports__r) FROM Booking__c LIMIT 1];
        Set<Id> tranportIds = (new Map<Id, Transport__c>(booking.Transports__r)).keySet();
        List<Stop__c> stops = [SELECT Id  FROM Stop__c WHERE Transport__c IN :tranportIds];
        
        Test.startTest();
        List < Stop__c > fetchedStops = BookingObjectsDAO.getStopsList(tranportIds);
        Test.stopTest();
        
        System.assertEquals(stops.size(), fetchedStops.size(), 'Stops count Check');
    }
    
    @isTest
    static void getFreightDetailTest() {
        Booking__c booking = [SELECT Id, (SELECT Id FROM Shipments__r) FROM Booking__c LIMIT 1];
        Set<Id> shipmentIds = (new Map<Id, Shipment__c>(booking.Shipments__r)).keySet();
        List<FreightDetail__c> freightDetails = [SELECT Id  FROM FreightDetail__c WHERE Shipment__c IN :shipmentIds];
        
        Test.startTest();
        List < FreightDetail__c > fetchedFreightDetails = BookingObjectsDAO.getFreightDetailList(shipmentIds);
        Test.stopTest();
        
        System.assertEquals(freightDetails.size(), fetchedFreightDetails.size(), 'Freight Details count Check');
    }
    
    @isTest
    static void getVoyageListTest() {
        Booking__c booking = [SELECT Id, (SELECT Id FROM Shipments__r) FROM Booking__c LIMIT 1];
        Set<Id> shipmentIds = (new Map<Id, Shipment__c>(booking.Shipments__r)).keySet();
        List<Voyage__c> voyages = [SELECT Id  FROM Voyage__c WHERE Shipment__c IN :shipmentIds];
        
        Test.startTest();
        List < Voyage__c > fetchedVoyages = BookingObjectsDAO.getVoyageList(shipmentIds);
        Test.stopTest();
        
        System.assertEquals(voyages.size(), fetchedVoyages.size(), 'Voyages count Check');
    }
    
    @isTest
    static void getEquipmentsListTest() {
        List<Requirement__c> requirements = [SELECT Id FROM Requirement__c];
        Set<Id> requirementIds = (new Map<Id, Requirement__c>(requirements)).keySet();
        List<Equipment__c> equipments = [SELECT Id  FROM Equipment__c WHERE Requirement__c IN :requirementIds];
        
        Test.startTest();
        List < Equipment__c > fetchedEquipments = BookingObjectsDAO.getEquipmentsList(requirementIds);
        Test.stopTest();
        System.assertEquals(equipments.size(), fetchedEquipments.size(), 'Equipment Count Check');
    }
    
    @isTest
    static void getPortsTest() {
        Ports_Information__c port = [SELECT Id, PortTerminal__c FROM Ports_Information__c LIMIT 1];
        Test.startTest();
        List<Ports_Information__c> fetchedPorts = BookingObjectsDAO.getPorts(port.PortTerminal__c);
        Test.stopTest();
        System.assertEquals(1, fetchedPorts.size(), 'Port Count Check');
    }
}