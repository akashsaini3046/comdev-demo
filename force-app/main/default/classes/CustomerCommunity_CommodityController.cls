public class CustomerCommunity_CommodityController {
    
    public static String validateHazardousBooking(Id bookingId ,List<List<Commodity__c>> listContainerCommodities, Map<String, String> mapContainerHierarchy){
		System.debug('bookingId---->'+bookingId);
        List<CustomerCommunity_BookingValidationReq.DGBookingItem> listOfDGBookingItem=new List<CustomerCommunity_BookingValidationReq.DGBookingItem>();
        List<CustomerCommunity_BookingValidationReq.DGBookingPackage> listOfDGBookingPackage=new List<CustomerCommunity_BookingValidationReq.DGBookingPackage>();
        List<CustomerCommunity_BookingValidationReq.DGCTU> listOfDGCTU=new List<CustomerCommunity_BookingValidationReq.DGCTU>();
        String hazCheckResponse = '';
        String htmlBody = '';
        String commentsBody='';
        Booking__c bookingRecord = new Booking__c();
        bookingRecord.Id = bookingId;
        System.debug('^^^Booking Id '+bookingRecord.Id);
        System.debug('listContainerCommodities----> '+listContainerCommodities);
        
        try{
            if(listContainerCommodities != Null && !listContainerCommodities.isEmpty()){
                for(List<Commodity__c> listCommodities : listContainerCommodities){
                    if(listCommodities != Null && !listCommodities.isEmpty()){
                        CustomerCommunity_BookingValidationReq.DGCTU dgctu=new CustomerCommunity_BookingValidationReq.DGCTU();
                        dgctu.UnitType='2';
                        CustomerCommunity_BookingValidationReq.BookingItems bookingItems=new CustomerCommunity_BookingValidationReq.BookingItems();
                        CustomerCommunity_BookingValidationReq.DGBookingPackage dgBookingPackage=new CustomerCommunity_BookingValidationReq.DGBookingPackage();
                        CustomerCommunity_BookingValidationReq.Packages dgPackages=new CustomerCommunity_BookingValidationReq.Packages();
                        listOfDGBookingItem=new List<CustomerCommunity_BookingValidationReq.DGBookingItem>();
                        listOfDGBookingPackage=new List<CustomerCommunity_BookingValidationReq.DGBookingPackage>();
                        for(Commodity__c commodityRecord : listCommodities){
                            CustomerCommunity_BookingValidationReq.DGBookingItem dgBookingItem=new CustomerCommunity_BookingValidationReq.DGBookingItem();
                            dgBookingItem.Suffix = commodityRecord.Suffix__c;
                            dgBookingItem.GrossWeightKG='0';
                            System.debug('&&&UN CODE is '+commodityRecord.Number__c);
                            if(commodityRecord.Number__c != Null){
                                String unCodeNumber = String.valueOf(commodityRecord.Number__c);
                                if(unCodeNumber.contains('.'))
                                    unCodeNumber = unCodeNumber.substringBefore('.');
                                if(unCodeNumber.contains('UN'))
                                    unCodeNumber=unCodeNumber.replace('UN','');

                                dgBookingItem.UNNo=unCodeNumber;
                            }  
                            System.debug('&&&Technical Name is '+commodityRecord.Technical_Name__c);
                            if(commodityRecord.Technical_Name__c != Null)
                                dgBookingItem.TechnicalName=commodityRecord.Technical_Name__c;
                            if(commodityRecord.Phone_Number__c != Null)
                                dgBookingItem.EmergencyContactNumber=String.valueOf(commodityRecord.Phone_Number__c);
                            if(commodityRecord.Name__c != Null)
                                dgBookingItem.EmergencyContactName=commodityRecord.Name__c;
                            //if(commodityRecord.Package_Group__c != Null && commodityRecord.Package_Group__c != '')
                            //    dgBookingItem.Suffix = commodityRecord.Package_Group__c;
                            listOfDGBookingItem.add(dgBookingItem);
                        }
                        bookingItems.DGBookingItem=listOfDGBookingItem;
                        dgBookingPackage.BookingItems=bookingItems;
                        listOfDGBookingPackage.add(dgBookingPackage);
                        dgPackages.DGBookingPackage=listOfDGBookingPackage;
                        
                        dgctu.Packages=dgPackages;
                        listOfDGCTU.add(dgctu);
                    }
                }
            }
            
            CustomerCommunity_BookingValidationReq.CTUs ctu=new CustomerCommunity_BookingValidationReq.CTUs();
            ctu.DGCTU=listOfDGCTU;
            
            CustomerCommunity_BookingValidationReq.Booking booking=new CustomerCommunity_BookingValidationReq.Booking();
            booking.IMDGAmdt='39';
            booking.CTUs=ctu;
            booking.BookingRef='';
            
            CustomerCommunity_BookingValidationReq.request bookingRequest=new CustomerCommunity_BookingValidationReq.request();
            bookingRequest.Booking=booking;
            
            CustomerCommunity_BookingValidationReq.ValidateBooking validateBooking=new CustomerCommunity_BookingValidationReq.ValidateBooking();
            validateBooking.request=bookingRequest;
            
            CustomerCommunity_BookingValidationReq.Body bookingBody=new CustomerCommunity_BookingValidationReq.Body();
            bookingBody.ValidateBooking=validateBooking;
            
            CustomerCommunity_BookingValidationReq bookingCreationRequest=new CustomerCommunity_BookingValidationReq();
            bookingCreationRequest.Body=bookingBody;
            
            String requestBody = JSON.serialize(bookingCreationRequest);
            
            HttpRequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();
            Http http = new Http();
            request.setEndpoint('https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/validateBooking');
            request.setMethod('POST');
            request.setHeader('content-type', 'application/json');
            request.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
            request.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
            request.setBody(requestBody);
            request.setTimeout(120000);
			system.debug('requestBody--->'+requestBody);
            response = http.send(request);
            if(response.getBody() != Null && response.getStatusCode() == 200){
                System.debug('response--->'+response.getBody());
                CustomerCommunity_BookingValidationRes bookingResponse = (CustomerCommunity_BookingValidationRes ) JSON.deserialize(response.getBody(), CustomerCommunity_BookingValidationRes.class);
				system.debug('requestBody--->'+requestBody);
                /*CustomerCommunity_HazCheckPDFController objHazCheck = new CustomerCommunity_HazCheckPDFController();
                objHazCheck.bookingResponse = bookingResponse;
                objHazCheck.attachPDF(bookingId);*/
                if(bookingResponse != Null){
                    
                    //Attachment pdf code starts
                    String strCTUWiseRequest = JSON.serialize(mapContainerHierarchy);
                    Attachment attach0 = new Attachment();
                    Blob  body0 = BLOB.valueOf(response.getBody());
					attach0.Body = body0;
                    attach0.Name = 'HazardousResponse'+ bookingId + '.txt';
                    attach0.ParentId = bookingId;
                    insert attach0; 
                    System.enqueueJob(new HazheckResponseQuable(attach0.Id, bookingId, strCTUWiseRequest));
					//Attachment pdf code ends
					
                    System.debug('bookingResponse--->'+bookingResponse);
                    System.debug(' Has Validation Error ???? '+bookingResponse.Body.ValidateBookingResponse.ValidateBookingResult.BookingStatus.HasError);
                    if(bookingResponse.Body.ValidateBookingResponse.ValidateBookingResult.BookingStatus.HasError=='true'){
                        bookingRecord.Status__c = 'HazCheck Rejected';
                    }
                    
                    else{
                        bookingRecord.Status__c = 'HazCheck Accepted';
                        hazCheckResponse = 'True';
                    }
                    
                    for(CustomerCommunity_BookingValidationRes.DGCTUStatus dgctuStatus:bookingResponse.Body.ValidateBookingResponse.ValidateBookingResult.BookingStatus.CTUStatus.DGCTUStatus){
                        System.debug('status--->'+dgctuStatus.Results.DGResult.Description);
                        System.Debug('@@@ - dgctuStatus.HasError - ' + dgctuStatus.HasError);
                        if(bookingRecord.Status__c == 'HazCheck Rejected' && dgctuStatus.HasError == 'true'){
                            if(dgctuStatus.ItemsStatus != Null && dgctuStatus.ItemsStatus.DGBookingItemStatus != Null){
                                for(CustomerCommunity_BookingValidationRes.DGBookingItemStatus bookingItem : dgctuStatus.ItemsStatus.DGBookingItemStatus){
                                    if(bookingItem.HasError == 'true' && bookingItem.Results.DGResult.Description != Null){
                                        if(hazCheckResponse == '')
                                            hazCheckResponse = bookingItem.Results.DGResult.Description;
                                        else
                                            hazCheckResponse = hazCheckResponse + '; ' +bookingItem.Results.DGResult.Description;
                                    }
                                }
                            }
                            if(dgctuStatus.PackagesStatus != Null && dgctuStatus.PackagesStatus.DGBookingPackageStatus != Null){
                                for(CustomerCommunity_BookingValidationRes.DGBookingPackageStatus bookingPackage : dgctuStatus.PackagesStatus.DGBookingPackageStatus){
                                    if(bookingPackage.Results != Null && bookingPackage.Results.DGResult != Null && bookingPackage.Results.DGResult.Description != Null){
                                    System.debug('status--->'+bookingPackage.Results.DGResult.Description);
                                    System.Debug('@@@ - bookingPackage.HasError - ' + bookingPackage.HasError);
                                    if(bookingPackage.HasError == 'true' && bookingPackage.Results.DGResult.Description != Null){
                                        if(hazCheckResponse == '')
                                            hazCheckResponse = bookingPackage.Results.DGResult.Description;
                                        else
                                            hazCheckResponse = hazCheckResponse + '; ' +bookingPackage.Results.DGResult.Description;
                                    }
                                }
                            }
                            }
                            if(hazCheckResponse == '')
                            	hazCheckResponse = dgctuStatus.Results.DGResult.Description;
                            //bookingRecord.Status__c = 'HazCheck Rejected';
                        }
                        /*htmlBody='<html><body><H2><p>'+dgctuStatus.Results.DGResult.Description+'<p></H2><br/><br/>';
                        commentsBody=commentsBody+'<h2>'+' Stowage Segregation Comments'+'</h2><br/>';
                        for(CustomerCommunity_BookingValidationRes.StowageSegregationComments comments:dgctuStatus.StowageSegregation.StowageSegregationComments){
                            commentsBody=commentsBody+'<p>'+'>   '+comments.Comment+'</p><br/>';    
                        }
                        commentsBody = commentsBody + '</body></html>';*/
                    }
                    /*if(hazCheckResponse == ''){
                        bookingRecord.Status__c = 'HazCheck Accepted';
                        hazCheckResponse = 'True';
                    }*/
                }
            }
            else{
                bookingRecord.Status__c = 'HazCheck Required';
                hazCheckResponse = 'Hazardous goods cannot be validated at the moment. Please try validating later.';
            }
            update bookingRecord;
            
            /*if(response.getStatusCode()==200){ 
                String base64Response = response.getBody();
                Attachment attach = new Attachment();
                attach.contentType = 'application/octet-stream';
                attach.name = 'hazardousResponse.pdf';
                attach.parentId = bookingRecord.id;
                attach.body = blob.toPDF(htmlBody+commentsBody); 
                //insert attach;
            }*/
            System.debug('@@@ hazCheckResponse - ' + hazCheckResponse);
            return hazCheckResponse;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
}