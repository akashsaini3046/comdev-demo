public class CustomerCommunity_ArticleController {
    
    @AuraEnabled
    public static List<KnowledgeArticleVersion> getArticles(){
        List<KnowledgeArticleVersion> listArticles = new List<KnowledgeArticleVersion>();
        try {
            listArticles = [SELECT Id, Title, UrlName, Language, KnowledgeArticleId, IsVisibleInCsp, Summary FROM KnowledgeArticleVersion 
                            WHERE IsVisibleInCsp = True AND PublishStatus = 'Online' ORDER BY LastPublishedDate DESC];
            if(!listArticles.isEmpty())
                return listArticles;
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    @AuraEnabled
    public static Knowledge__kav getArticleDetails(String articleId){
        List<Knowledge__kav> articleRecord = new List<Knowledge__kav>();
        try {
            Id knwId = (Id)articleId;
            articleRecord = [SELECT ArticleNumber, Id, KnowledgeArticleId, PublishStatus, Summary, Title, UrlName, Description__c 
                             FROM Knowledge__kav WHERE KnowledgeArticleId = :knwId AND PublishStatus = 'Online' LIMIT 1 UPDATE VIEWSTAT];
            if(!articleRecord.isEmpty())
                return articleRecord[0];
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
}