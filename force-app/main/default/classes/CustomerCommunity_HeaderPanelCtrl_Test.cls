@isTest
public class CustomerCommunity_HeaderPanelCtrl_Test {
static testmethod void getUserName_test()
   {
        Account acc;
        User testuser1;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
       System.runAs ( thisUser ) {
            
            acc = TestDataUtility.createAccount('Nagarro', null,'Customer', 1)[0];
            //acc.OwnerId = m1.Id;
            //update acc;
            Id accId = acc.Id;
            System.debug(accId);
            DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
            List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
            Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = acc.Id)}, 'BL1', 'City1', 
                                                                                    stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                    'US', 1)[0];
            System.assertNotEquals(NULL, businessLocationObj.Country__c);
            
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
            System.assertNotEquals(NULL, recordTypeId);
            Contact con= TestDataUtility.createContact(recordTypeId, accId , new List<Address__c>{businessLocationObj}, null, 
                                                       'Test Community Con1', 'communityexample@gmail.com', '11111111111', false, 
                                                       Null, 1)[0];
           System.debug(con.Id);
           //m1 = new user(id=m1.id,contactId=con.Id);
            //update m1;
            Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus User' Limit 1].Id;
            Id roleId1 = [select id from UserRole where Name ='Walmart Customer User'].Id;
            testuser1 = TestDataUtility.createUser(roleId1, profileId1, Null, 'Mgr1', 'approvalMgr1@test.com', 
                                                 'en_US', 'en_US', 'UTF-8', 'America/Los_Angeles', con.Id,null);
           
        //   System.debug(m1); 
        
       }  
       System.runAs ( testuser1 ) {
        CustomerCommunity_HeaderPanelController.getUserName();
       }
       
}
    static testmethod void getUserName_test2(){
        
        User testuser2;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
        Id profileId2 = [SELECT Id FROM Profile WHERE Name = 'System Administrator' Limit 1].Id;
        Id roleId2 = [select id from UserRole where Name ='Walmart Customer User'].Id;
        testuser2 =  TestDataUtility.createUser(roleId2, profileId2, Null, 'Mgr2', 'approvalMgr2@test.com', 
                                                 'en_US', 'en_US', 'UTF-8', 'America/Los_Angeles', null,null);
        
        }
        System.runAs ( testuser2 ) {
        CustomerCommunity_HeaderPanelController.getUserName();
       }
    }
}