public without sharing class CustomerCommunity_Substances {
    
    @AuraEnabled
    public static Map<Id,Substance__c> getAllSubstancesRecord(){
		Map<Id,Substance__c> mapSubstances = new Map<Id,Substance__c>([SELECT Id, Suffix__c,Name, Primary_Class__c, Substance_Name__c, Variation__c, 
                                              	Packing_Group__c, Secondary_Class__c, Tertiary_Class__c
                                              FROM Substance__c ]);
        return mapSubstances;
    }
}