@isTest
public class CustomerCommunity_ForgotPass_Test {
    
    static testmethod void usernameExists_test(){
        
        User UserComm;
        Account acc;
        acc = TestDataUtility.createAccount('Nagarro', null,'Customer', 1)[0];
        Id accId = acc.Id;
        DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
            List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
            Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = acc.Id)}, 'BL1', 'City1', 
                                                                                    stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                    'US', 1)[0];
            System.assertNotEquals(NULL, businessLocationObj.Country__c);
            
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
            System.assertNotEquals(NULL, recordTypeId);
            Contact con= TestDataUtility.createContact(recordTypeId, accId , new List<Address__c>{businessLocationObj}, null, 
                                                       'Test Community Con1', 'communityexample@gmail.com', '11111111111', false, 
                                                       Null, 1)[0];
           System.debug(con.Id);
        
        Id profileId1 = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus User' Limit 1].Id;
        Id roleId1 = [select id from UserRole where Name ='Walmart Customer User'].Id;
        UserComm = TestDataUtility.createUser(roleId1, profileId1, Null, 'Mgr1', 'dhruv.raisinghani@nagarro.com', 
                                                 'en_US', 'en_US', 'UTF-8', 'America/Los_Angeles', con.id,null);
        

        System.runAs(UserComm){
            
         CustomerCommunity_ForgotPassController.usernameExists(UserComm.Username);
            
        }
    }
    static testmethod void getUserName_test2(){
        
        User testuser2;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
        Id profileId2 = [SELECT Id FROM Profile WHERE Name = 'System Administrator' Limit 1].Id;
        Id roleId2 = [select id from UserRole where Name ='Walmart Customer User'].Id;
        testuser2 =  TestDataUtility.createUser(roleId2, profileId2, Null, 'Mgr2', 'approvalMgr2@test.com', 
                                                 'en_US', 'en_US', 'UTF-8', 'America/Los_Angeles', null,null);
        
        }
        System.runAs ( testuser2 ) {
       	CustomerCommunity_ForgotPassController.usernameExists(testuser2.Username);
       }
    }
       
}