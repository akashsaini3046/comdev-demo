public class CustomerCommunity_PreferencesController {
    @AuraEnabled
    public static String updateDetails(String userEmail,Id idUser,List <String> listSmsNumbers,List<String> listWhatsAppNumbers) {
        try {
            User user = [SELECT id,contactId,Email from User where id =: idUser LIMIT 1];
            Contact userContact=[SELECT id,HomePhone,MobilePhone,Phone,Email FROM Contact where id=:user.contactId LIMIT 1];
            if(userEmail != NULL){
                userContact.Email = userEmail;
            }
            System.debug('listSmsNumbers -->' + listSmsNumbers);
            System.debug('listWhatsAppNumbers -->' + listWhatsAppNumbers);
            System.debug('listSize ---> ' + listSmsNumbers.size());
			if(userContact != null){
                System.debug('listSize ---> ' + listSmsNumbers.size());
                if(listSmsNumbers.size() == 0){
                   userContact.HomePhone= null;
                   userContact.MobilePhone= null;
                   userContact.Phone = null;
                   userContact.Mobile_Phone_1__c = null;
                   userContact.Mobile_Phone_2__c = null;
                   userContact.Mobile_Phone_3__c = null; 
                }  
               else if(listSmsNumbers.size() == 1){
                   userContact.HomePhone=listSmsNumbers[0];
                   userContact.MobilePhone= null;
                   userContact.Phone = null;
                   userContact.Mobile_Phone_1__c = null;
                   userContact.Mobile_Phone_2__c = null;
                   userContact.Mobile_Phone_3__c = null; 
                }
                else if(listSmsNumbers.size() == 2){
                    userContact.HomePhone=listSmsNumbers[0];
                    userContact.MobilePhone=listSmsNumbers[1];
                    userContact.Phone = null;
                    userContact.Mobile_Phone_1__c = null;
                    userContact.Mobile_Phone_2__c = null;
                    userContact.Mobile_Phone_3__c = null;  
                }
                else if(listSmsNumbers.size() == 3){
                    userContact.HomePhone=listSmsNumbers[0];
                    userContact.MobilePhone=listSmsNumbers[1];
                    userContact.Phone=listSmsNumbers[2];
                    userContact.Mobile_Phone_1__c = null;
                    userContact.Mobile_Phone_2__c = null;
                    userContact.Mobile_Phone_3__c = null;  
                }
                else if(listSmsNumbers.size() == 4){
                    userContact.HomePhone=listSmsNumbers[0];
                    userContact.MobilePhone=listSmsNumbers[1];
                    userContact.Phone=listSmsNumbers[2];
                    userContact.Mobile_Phone_1__c = listSmsNumbers[3];
                    userContact.Mobile_Phone_2__c = null;
                    userContact.Mobile_Phone_3__c = null;
                }
                else if(listSmsNumbers.size() == 5){
                    userContact.HomePhone=listSmsNumbers[0];
                    userContact.MobilePhone=listSmsNumbers[1];
                    userContact.Phone=listSmsNumbers[2];
                    userContact.Mobile_Phone_1__c = listSmsNumbers[3];
                    userContact.Mobile_Phone_2__c = listSmsNumbers[4];
                    userContact.Mobile_Phone_3__c = null;
                }
                else if(listSmsNumbers.size() == 6){
                    userContact.HomePhone=listSmsNumbers[0];
                    userContact.MobilePhone=listSmsNumbers[1];
                    userContact.Phone=listSmsNumbers[2];
                    userContact.Mobile_Phone_1__c = listSmsNumbers[3];
                    userContact.Mobile_Phone_2__c = listSmsNumbers[4];
                    userContact.Mobile_Phone_3__c = listSmsNumbers[5];
                }
				update userContact;
				return 'TRUE';
			}
			else {
                return 'FALSE';
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return 'FALSE';
        }
    }
    @AuraEnabled
    public static Contact fetchUserDetails(){
        try{
            Id userId = UserInfo.getUserId();
            User userDetails = [Select Email,contactId,PhoneNumber_SMS__c,PhoneNumber_WhatsApp__c from User where id= :userId LIMIT 1];
            Contact userContact=[SELECT id,HomePhone,MobilePhone,Phone,Email,Mobile_Phone_1__c,Mobile_Phone_2__c,Mobile_Phone_3__c FROM Contact where id=:userDetails.contactId LIMIT 1];
            if(userContact!= NULL){
                return userContact;
            }
            else{
                return NULL;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
}