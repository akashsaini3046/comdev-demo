public without sharing class CrowleyCommunity_ValidateUser {
    @AuraEnabled
    public static String validateUserDetail(String userEmail){
        List<User> listUser = new List<User>();
        List<Contact> listContact = new List<Contact>();
        List<Lead> listLead = new List<Lead>();
        try{ 
            listUser = [Select id from User where Email = :userEmail LIMIT 1]; 
            if(!listUser.isEmpty()){
                return CrowleyCommunity_Constants.TRUE_MESSAGE;
            }       
            listLead = [Select id from Lead where email = :userEmail LIMIT 1];
            if(!listUser.isEmpty()){
                return CrowleyCommunity_Constants.TRUE_MESSAGE;
            }      
            else { 
                 String contactExists = 'False';
                Profile profile1 = [Select Id from Profile where name = 'Crowley Community Plus Test'];
                User portalAccountOwner1 = new User(
                    ProfileId = profile1.Id,
                    Username = System.now().millisecond() + 'test2@test.com',
                    Alias = 'batman',
                    Email= userEmail,
                    Phone = '99999999',
                    EmailEncodingKey='UTF-8',
                    Firstname='Donald',
                    Lastname='Trump',
                    Country = 'USA',
                    CommunityNickname = CrowleyCommunity_ValidateUser.generateRandomString(8),
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago');
                System.debug('idportalAccountOwner1---'+portalAccountOwner1);
                
                String identifier = System.UserManagement.initSelfRegistration(Auth.VerificationMethod.EMAIL, portalAccountOwner1);
                System.debug('identifier@@'+identifier);
                
                listContact = [Select id from Contact where Email = :userEmail LIMIT 1];
                System.debug('listContact'+listContact);
                if(!listContact.isEmpty()){
                    contactExists = 'True';      
                }
                 return contactExists +':'+ identifier;              
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CrowleyCommunity_Constants.ERROR_MESSAGE;  
        } 
    }
    public static String generateRandomString(Integer len) {
        final String chars = CustomerCommunity_Constants.CHARS;
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx + 1);
        }
        return randStr;
    }
}