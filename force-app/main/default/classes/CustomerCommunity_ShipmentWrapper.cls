public  class CustomerCommunity_ShipmentWrapper {

	public cls_UniversalResponse UniversalResponse;
	public class cls_UniversalResponse {
		public String Status;	//PRS
		public cls_Data Data;
		public String version;	//1.1
	}
	public class cls_Data {
		public cls_UniversalShipment UniversalShipment;
	}
	public class cls_UniversalShipment {
		public cls_Shipment Shipment;
		public String version;	//1.1
	}
	public class cls_Shipment {
		public cls_DataContext DataContext;
		public cls_AWBServiceLevel AWBServiceLevel;
		public String BookingConfirmationReference;	//CAT369879
		public String ChargeableRate;	//0.0000
		public String ContainerCount;	//0
		public cls_ContainerMode ContainerMode;
		public String DocumentedChargeable;	//604.73
		public String DocumentedVolume;	//604.73
		public String DocumentedWeight;	//9809
		public String FreightRate;	//0.0000
		public cls_FreightRateCurrency FreightRateCurrency;
		public String IsCFSRegistered;	//false
		public String IsDirectBooking;	//false
		public String IsForwardRegistered;	//true
		public String IsNeutralMaster;	//false
		public String LloydsIMO;	//9395044
		public String ManifestedChargeable;	//604.73
		public String ManifestedVolume;	//604.73
		public String ManifestedWeight;	//9809
		public String NoCopyBills;	//3
		public String NoOriginalBills;	//3
		public String OuterPacks;	//0
		public cls_PaymentMethod PaymentMethod;
		public cls_PlaceOfDelivery PlaceOfDelivery;
		public cls_PlaceOfIssue PlaceOfIssue;
		public cls_PlaceOfReceipt PlaceOfReceipt;
		public cls_PortOfDischarge PortOfDischarge;
		public cls_PortOfLoading PortOfLoading;
		public cls_ScreeningStatus ScreeningStatus;
		public cls_ShipmentType ShipmentType;
		public String TotalNoOfPacks;	//21
		public cls_TotalNoOfPacksPackageType TotalNoOfPacksPackageType;
		public String TotalPreallocatedChargeable;	//0.000
		public String TotalPreallocatedVolume;	//0.000
		public String TotalPreallocatedWeight;	//0.000
		public String TotalVolume;	//604.73
		public cls_TotalVolumeUnit TotalVolumeUnit;
		public String TotalWeight;	//9809
		public cls_TotalWeightUnit TotalWeightUnit;
		public cls_TransportMode TransportMode;
		public String VesselName;	//VARAMO
		public String VoyageFlightNo;	//SZV9030S
		public String WayBillNumber;	//CAT369879
		public cls_WayBillType WayBillType;
		public cls_CustomizedFieldCollection[] CustomizedFieldCollection;
		public cls_DateCollection[] DateCollection;
		public cls_MilestoneCollection[] MilestoneCollection;
		public cls_OrganizationAddressCollection[] OrganizationAddressCollection;
		public cls_SubShipmentCollection SubShipmentCollection;
		public cls_TransportLegCollection TransportLegCollection;
	}
	public class cls_DataContext {
		public cls_DataSourceCollection[] DataSourceCollection;
	}
	public class cls_DataSourceCollection {
		public String Type;	//ForwardingShipment
		public String Key;	//SUCA285995
	}
	public class cls_Company {
		public String Code;	//JAX
		public cls_Country Country;
		public String Name;	//Crowley Logistics, Inc.
	}
	public class cls_Country {
		public String Code;	//US
		public String Name;	//United States
	}
	public class cls_AWBServiceLevel {
		public String Code;	//STD
		public String Description;	//Standard
	}
	public class cls_ContainerMode {
		public String Code;	//LCL
		public String Description;	//Less Container Load
	}
	public class cls_FreightRateCurrency {
		public String Code;	//USD
		public String Description;	//United States Dollar
	}
	public class cls_PaymentMethod {
		public String Code;	//DEF
		public String Description;	//Default
	}
	public class cls_PlaceOfDelivery {
		public String Code;	//CRSJO
		public String Name;	//San Jose
	}
	public class cls_PlaceOfIssue {
		public String Code;	//USJAX
		public String Name;	//Jacksonville
	}
	public class cls_PlaceOfReceipt {
		public String Code;	//USMIA
		public String Name;	//Miami
	}
	public class cls_PortOfDischarge {
		public String Code;	//CRMOB
		public String Name;	//Moin
	}
	public class cls_PortOfLoading {
		public String Code;	//USPEF
		public String Name;	//Port Everglades
	}
	public class cls_ScreeningStatus {
		public String Code;	//UNK
		public String Description;	//Unknown
	}
	public class cls_ShipmentType {
		public String Code;	//STD
		public String Description;	//Standard House
	}
	public class cls_TotalNoOfPacksPackageType {
		public String Code;	//CTN
		public String Description;	//Carton
	}
	public class cls_TotalVolumeUnit {
		public String Code;	//CF
		public String Description;	//Cubic Feet
	}
	public class cls_TotalWeightUnit {
		public String Code;	//LB
		public String Description;	//Pounds
	}
	public class cls_TransportMode {
		public String Code;	//SEA
		public String Description;	//Sea (Non Container, Container) (10, 11)
	}
	public class cls_WayBillType {
		public String Code;	//HWB
		public String Description;	//House Waybill
	}
	public class cls_CustomizedFieldCollection {
		public String DataType;	//String
		public String Key;	//WH RECEIPT NUMBER
		public String Value;	//W00738531
	}
	public class cls_DateCollection {
		public String Type;	//Departure
		public String IsEstimate;	//true
		public String Value;	//2019-07-26T00:00:00
	}
	public class cls_MilestoneCollection {
		public String Description;	//Pickup Cartage Complete/Finalised
		public String EventCode;	//PCF
		public String Sequence;	//2
        public DateTime ActualDate;
        public DateTime EstimatedDate;      
	}
	public class cls_OrganizationAddressCollection {
		public String AddressType;	//Forwarder
		public String Address1;	//10205 NW 108TH AVE STE 1
		public String Address2;	//FIRMS CODE L117
		public String AddressOverride;	//false
		public String AddressShortCode;	//01
		public String City;	//MIAMI
		public String CompanyName;	//CROWLEY LOGISTICS
		public cls_Country Country;
		public String Email;	//kissy.amiama@crowley.com
		public String GovRegNum;	//94-330039900
		public cls_GovRegNumType GovRegNumType;
		public String OrganizationCode;	//COLMIAUS
		public String Phone;	//+13054704000
		public cls_Port Port;
		public String Postcode;	//33178
		public cls_ScreeningStatus ScreeningStatus;
		public String State;	//FL
		public String UniversalOfficeCode;	//496153601
		//public cls_RegistrationNumberCollection[] RegistrationNumberCollection;
	}
	public class cls_GovRegNumType {
		public String Code;	//EIN
		public String Description;	//Employer Identification Number
	}
	public class cls_Port {
		public String Code;	//USJAX
		public String Name;	//Jacksonville
	}
	public class cls_RegistrationNumberCollection {
		public cls_Type Type;
		public cls_CountryOfIssue CountryOfIssue;
		public String Value;	//5448115
	}
	public class cls_Type {
		public String Code;	//LSC
		public String Description;	//Legacy System Code
	}
	public class cls_CountryOfIssue {
		public String Code;	//US
		public String Name;	//United States
	}
	public class cls_SubShipmentCollection {
		public cls_SubShipment SubShipment;
	}
	public class cls_SubShipment {
		public cls_DataContext DataContext;
		public String ActualChargeable;	//272.940
		public cls_Branch Branch;
		public cls_CommercialInfo CommercialInfo;
		public String ContainerCount;	//0
		public cls_ContainerMode ContainerMode;
		public cls_CustomsContainerMode CustomsContainerMode;
		public cls_CustomsProfileIdentifier CustomsProfileIdentifier;
		public String DocumentedChargeable;	//272.940
		public String DocumentedVolume;	//272.940
		public String DocumentedWeight;	//7200.000
		public cls_EntryStatus EntryStatus;
		public String FreightRate;	//0.0000
		public String GoodsValue;	//0.0000
		public cls_GoodsValueCurrency GoodsValueCurrency;
		public cls_HBLAWBChargesDisplay HBLAWBChargesDisplay;
		public String InsuranceValue;	//0.0000
		public cls_InsuranceValueCurrency InsuranceValueCurrency;
		public String IsBooking;	//false
		public String IsCancelled;	//false
		public String IsCFSRegistered;	//false
		public String IsDirectBooking;	//false
		public String IsForwardRegistered;	//true
		public String IsNeutralMaster;	//false
		public String IsPersonalEffects;	//false
		public String IsShipping;	//false
		public String IsSplitShipment;	//false
		public String LloydsIMO;	//9395044
		public String ManifestedChargeable;	//272.940
		public String ManifestedVolume;	//272.940
		public String ManifestedWeight;	//7200.000
		public cls_MergeBy MergeBy;
		public cls_MessageStatus MessageStatus;
		public cls_MessageType MessageType;
		public String NoCopyBills;	//1
		public String NoOriginalBills;	//0
		public String OuterPacks;	//6
		public cls_OuterPacksPackageType OuterPacksPackageType;
		public String PackingOrder;	//0
		public cls_PaymentMethod PaymentMethod;
		public cls_PortOfDestination PortOfDestination;
		public cls_PortOfDischarge PortOfDischarge;
		public cls_PortOfLoading PortOfLoading;
		public cls_PortOfOrigin PortOfOrigin;
		public cls_ReleaseType ReleaseType;
		public cls_ScreeningStatus ScreeningStatus;
		public cls_ServiceLevel ServiceLevel;
		public cls_ShipmentIncoTerm ShipmentIncoTerm;
		public cls_ShipmentType ShipmentType;
		public cls_ShippedOnBoard ShippedOnBoard;
		public String ShipperCODAmount;	//0.0000
		public String TotalNoOfPacks;	//0
		public String TotalNoOfPacksDecimal;	//0.0000
		public cls_TotalNoOfPacksPackageType TotalNoOfPacksPackageType;
		public String TotalNoOfPieces;	//0
		public String TotalVolume;	//272.940
		public cls_TotalVolumeUnit TotalVolumeUnit;
		public String TotalWeight;	//7200.000
		public cls_TotalWeightUnit TotalWeightUnit;
		public String TranshipToOtherCFS;	//false
		public cls_TransportMode TransportMode;
		public String VesselName;	//VARAMO
		public String VoyageFlightNo;	//SZV9030S
		public String WayBillNumber;	//SUCA285995
		public cls_WayBillType WayBillType;
		public cls_LocalProcessing LocalProcessing;
		public cls_AddInfoCollection[] AddInfoCollection;
		public cls_AdditionalBillCollection[] AdditionalBillCollection;
		public cls_CustomizedFieldCollection[] CustomizedFieldCollection;
		public cls_DateCollection[] DateCollection;
		public cls_MilestoneCollection[] MilestoneCollection;
		public cls_OrganizationAddressCollection[] OrganizationAddressCollection;
		public cls_PackingLineCollection[] PackingLineCollection;
		public String Content;	//Complete
		public cls_TransportLegCollection TransportLegCollection;
	}
	public class cls_Branch {
		public String Code;	//UCA
		public String Name;	//US-CA NVO OPS
	}
	public class cls_CommercialInfo {
		public String Name;	//All Invoices
		public cls_CommercialInvoiceCollection[] CommercialInvoiceCollection;
	}
	public class cls_CommercialInvoiceCollection {
		public String InvoiceNumber;	//94291255
		public String AgreedExchangeRate;	//1.000000000
		public cls_Buyer Buyer;
		public cls_IncoTerm IncoTerm;
		public String InvoiceAmount;	//7476.2400
		public cls_InvoiceCurrency InvoiceCurrency;
		public String LandedCostExchangeRate;	//1.000000000
		public String NetWeight;	//0.000
		public cls_NetWeightUQ NetWeightUQ;
		public String NoOfPacks;	//0.000
		public String PaymentAmount;	//0.0000
		public String PaymentExchangeRate;	//1.000000000
		public cls_Supplier Supplier;
		public String Volume;	//0.000
		public String Weight;	//3088.824
		public cls_WeightUnit WeightUnit;
		public cls_AddInfoCollection[] AddInfoCollection;
		public cls_CommercialChargeCollection[] CommercialChargeCollection;
		public cls_CommercialInvoiceLineCollection CommercialInvoiceLineCollection;
	}
	public class cls_Buyer {
		public String AddressType;	//UltimateConsignee
		public String Address1;	//SABANA SUR, 400 MTS OESTE DE LA POPS
		public String AddressOverride;	//false
		public String AddressShortCode;	//02
		public String City;	//SAN JOSE
		public String CompanyName;	//PISCINAS AQUA LUX XXI INC S A
		public cls_Country Country;
		public String Email;	//crlcl@crowley.com
		public String Fax;	//+50622966274
		public String GovRegNum;	//310136631522
		public cls_GovRegNumType GovRegNumType;
		public String OrganizationCode;	//PISAQUCR
		public String Phone;	//+50622966273
		public cls_Port Port;
		public cls_ScreeningStatus ScreeningStatus;
		public String State;	//SJ
		//public cls_RegistrationNumberCollection RegistrationNumberCollection;
	}
	public class cls_RegistrationNumber {
		public cls_Type Type;
		public cls_CountryOfIssue CountryOfIssue;
		public String Value;	//3807738
	}
	public class cls_IncoTerm {
		public String Code;	//DDP
		public String Description;	//Delivered Duty Paid
	}
	public class cls_InvoiceCurrency {
		public String Code;	//USD
		public String Description;	//United States Dollar
	}
	public class cls_NetWeightUQ {
		public String Code;	//KG
		public String Description;	//Kilograms
	}
	public class cls_Supplier {
		public String AddressType;	//USPrincipalPartyInInterest
		public String Address1;	//1400 BLUEGRASS LAKES PKWY
		public String AddressOverride;	//false
		public String AddressShortCode;	//13
		public String City;	//ALPHARETTA
		public String CompanyName;	//ARCH CHEMICALS INC
		public cls_Country Country;
		public String Email;	//DEBBIEROBERTS@ODYSSEYLOGISTICS.COM
		public String GovRegNum;	//06-152631500
		public cls_GovRegNumType GovRegNumType;
		public String OrganizationCode;	//ARCCHEUS
		public String Phone;	//+18437697030
		public cls_Port Port;
		public String Postcode;	//30004
		public cls_ScreeningStatus ScreeningStatus;
		public String State;	//GA
		//public cls_RegistrationNumberCollection RegistrationNumberCollection;
	}
	public class cls_WeightUnit {
		public String Code;	//LB
		public String Description;	//Pounds
	}
	public class cls_AddInfoCollection {
		public String Key;	//7501Agent
		public String Value;	//N
	}
	public class cls_CommercialChargeCollection {
		public cls_ChargeType ChargeType;
		public String AdjustedCharge;	//false
		public String Amount;	//0.0000
		public cls_ApportionmentType ApportionmentType;
		public cls_DistributeBy DistributeBy;
		public String IsApportionedCharge;	//false
		public String IsDutiable;	//false
		public String IsGSTApplicable;	//false
		public String IsIncludedInITOT;	//false
		public String IsNotIncludedInInvoice;	//false
		public String PercentageOfLinePrice;	//0.00000
		public cls_PrepaidCollect PrepaidCollect;
	}
	public class cls_ChargeType {
		public String Code;	//LCH
		public String Description;	//Landing Charges (Local Charges)
	}
	public class cls_ApportionmentType {
		public String Code;	//PAA
		public String Description;	//Partial Apportionment: Apportion this amount to the invoices that don't have thi
	}
	public class cls_DistributeBy {
		public String Code;	//VAL
		public String Description;	//Value
	}
	public class cls_PrepaidCollect {
		public String Code;	//PPD
		public String Description;	//Prepaid
	}
	public class cls_CommercialInvoiceLineCollection {
		public cls_CommercialInvoiceLine CommercialInvoiceLine;
	}
	public class cls_CommercialInvoiceLine {
		public String LineNo;	//1
		public String BondedWarehouseQuantity;	//0.00000
		public String CustomsQuantity;	//1016.00000
		public cls_CustomsQuantityUnit CustomsQuantityUnit;
		public String CustomsSecondQuantity;	//0.00000
		public String CustomsThirdQuantity;	//0.00000
		public String CustomsValue;	//7476.24
		public String Description;	//RODENTICIDES AND SIMILAR PRODUCTS, FOR RETAIL SALE
		public String HarmonisedCode;	//3808990002
		public String InvoiceQuantity;	//0.00000
		public String LinePrice;	//7476.2400
		public String Link;	//1
		public String NetWeight;	//0.000
		public cls_NetWeightUnit NetWeightUnit;
		public String OrderLineLink;	//0
		public String ParentLineNo;	//0
		public String PreviousEntryLineNumber;	//0
		public String UnitPrice;	//0
		public String ValuationMarkup;	//0.000
		public String Volume;	//0.000
		public cls_VolumeUnit VolumeUnit;
		public String Weight;	//1016.000
		public cls_WeightUnit WeightUnit;
		public cls_AddInfoCollection[] AddInfoCollection;
	}
	public class cls_CustomsQuantityUnit {
		public String Code;	//KG
		public String Description;	//Kilograms
	}
	public class cls_NetWeightUnit {
		public String Code;	//LB
		public String Description;	//Pounds
	}
	public class cls_VolumeUnit {
		public String Code;	//CF
		public String Description;	//Cubic Feet
	}
	public class cls_CustomsContainerMode {
		public String Code;	//CNT
		public String Description;	//Containerized (Trans. Mode: 11, 21,
	}
	public class cls_CustomsProfileIdentifier {
		public String Type;	//UserName
	}
	public class cls_EntryStatus {
		public String Description;	//Not Sent
	}
	public class cls_GoodsValueCurrency {
		public String Code;	//USD
		public String Description;	//United States Dollar
	}
	public class cls_HBLAWBChargesDisplay {
		public String Code;	//ALL
		public String Description;	//Show Prepaid & Collect Charges
	}
	public class cls_InsuranceValueCurrency {
		public String Code;	//USD
		public String Description;	//United States Dollar
	}
	public class cls_MergeBy {
		public String Code;	//NON
		public String Description;	//No Merge
	}
	public class cls_MessageStatus {
		public String Description;	//Not Sent
	}
	public class cls_MessageType {
		public String Code;	//EXP
		public String Description;	//Export
	}
	public class cls_OuterPacksPackageType {
		public String Code;	//PAL
		public String Description;	//Pallet
	}
	public class cls_PortOfDestination {
		public String Code;	//CRSJO
		public String Name;	//San Jose
	}
	public class cls_PortOfOrigin {
		public String Code;	//USMIA
		public String Name;	//Miami
	}
	public class cls_ReleaseType {
		public String Code;	//OBR
		public String Description;	//Original Bill Required at Destination
	}
	public class cls_ServiceLevel {
		public String Code;	//STD
		public String Description;	//Standard
	}
	public class cls_ShipmentIncoTerm {
		public String Code;	//EXW
		public String Description;	//Ex Works
	}
	public class cls_ShippedOnBoard {
		public String Code;	//SHP
		public String Description;	//Shipped
	}
	public class cls_LocalProcessing {
		public String DeliveryLabourCharge;	//0.0000
		public String DemurrageOnDeliveryCharge;	//0.0000
		public String DemurrageOnPickupCharge;	//0.0000
		public cls_FCLDeliveryEquipmentNeeded FCLDeliveryEquipmentNeeded;
		public cls_FCLPickupEquipmentNeeded FCLPickupEquipmentNeeded;
		public String HasProhibitedPackaging;	//false
		public String InsuranceRequired;	//false
		public String IsContingencyRelease;	//false
		public String LCLAirStorageCharge;	//0.0000
		public String LCLAirStorageDaysOrHours;	//0
		public String LCLDatesOverrideConsol;	//false
		public String PickupLabourCharge;	//0.0000
		public cls_PrintOptionForPackagesOnAWB PrintOptionForPackagesOnAWB;
	}
	public class cls_FCLDeliveryEquipmentNeeded {
		public String Code;	//ANY
		public String Description;	//Any
	}
	public class cls_FCLPickupEquipmentNeeded {
		public String Code;	//ANY
		public String Description;	//Any
	}
	public class cls_PrintOptionForPackagesOnAWB {
		public String Code;	//DEF
		public String Description;	//Default (Dims, fallback to Vol)
	}
	public class cls_AdditionalBillCollection {
		public String BillNumber;	//CAT369879
		public cls_BillType BillType;
		public cls_MessageStatus MessageStatus;
		public String NoOfPacks;	//0.0000
	}
	public class cls_BillType {
		public String Code;	//MWB
		public String Description;	//Master Waybill
	}
	public class cls_PackingLineCollection {
		public cls_Commodity Commodity;
		public String ContainerPackingOrder;	//0
		public String EndItemNo;	//0
		public String GoodsDescription;	//W00738531
		public String Height;	//26.000
		public String ItemNo;	//0
		public String Length;	//48.000
		public cls_LengthUnit LengthUnit;
		public String LinePrice;	//0.0000
		public String Link;	//1
		public String LoadingMeters;	//0.000
		public String OutturnDamagedQty;	//0
		public String OutturnedHeight;	//0.000
		public String OutturnedLength;	//0.000
		public String OutturnedVolume;	//0.000
		public String OutturnedWeight;	//0.000
		public String OutturnedWidth;	//0.000
		public String OutturnPillagedQty;	//0
		public String OutturnQty;	//0
		public String PackQty;	//1
		public cls_PackType PackType;
		public String ReferenceNumber;	//M0000266077
		public String Volume;	//29.610
		public cls_VolumeUnit VolumeUnit;
		public String Weight;	//1200.000
		public cls_WeightUnit WeightUnit;
		public String Width;	//41.000
		public cls_CustomizedFieldCollection[] CustomizedFieldCollection;
		public cls_UNDGCollection UNDGCollection;
	}
	public class cls_Commodity {
		public String Code;	//GEN
		public String Description;	//General
	}
	public class cls_LengthUnit {
		public String Code;	//IN
		public String Description;	//Inches
	}
	public class cls_PackType {
		public String Code;	//PLT
		public String Description;	//Pallet
	}
	public class cls_UNDGCollection {
		public cls_UNDG UNDG;
	}
	public class cls_UNDG {
		public String FlashPoint;	//0.0
		public String IMOClass;	//8
		public String PackedInLimitedQuantity;	//false
		public String PackingGroup;	//III
		public String PackQty;	//0
		public String ProperShippingName;	//DISINFECTANT, LIQUID, CORROSIVE, N.O.S.
		public String UNDGCode;	//1903c
		public String Volume;	//0.000
		public String Weight;	//0.000
	}
	public class cls_TransportLegCollection {
		public cls_TransportLeg TransportLeg;
		public String Content;	//Complete
	}
	public class cls_TransportLeg {
		public cls_PortOfDischarge PortOfDischarge;
		public cls_PortOfLoading PortOfLoading;
		public String LegOrder;	//1
		public cls_Carrier Carrier;
		public String CarrierBookingReference;	//SZV
		public String EstimatedArrival;	//2019-07-30T00:00:00
		public String EstimatedDeparture;	//2019-07-26T00:00:00
		public String IsCargoOnly;	//true
		public String LegType;	//Main
		public String TransportMode;	//Sea
		public String VesselLloydsIMO;	//9395044
		public String VesselName;	//VARAMO
		public String VoyageFlightNo;	//SZV9030S
	}
	public class cls_Carrier {
		public String AddressType;	//Carrier
		public String Address1;	//SOUTHERN ZONE MOVEMENTS
		public String Address2;	//BETWEEN PEV AND CA   SZV.
		public String AddressOverride;	//false
		public String AddressShortCode;	//22
		public String City;	//JACKSONVILLE
		public String CompanyName;	//CROWLEY LATIN AMERICA SERVICES
		public cls_Country Country;
		public String Email;	//CROWLEY@CROWLEY.COM
		public String GovRegNum;	//98-059007400
		public cls_GovRegNumType GovRegNumType;
		public String OrganizationCode;	//IC1CLSSZV
		public cls_Port Port;
		public String Postcode;	//22
		public cls_ScreeningStatus ScreeningStatus;
		public String State;	//FL
		public String UniversalOfficeCode;	//496153601
		//public cls_RegistrationNumberCollection[] RegistrationNumberCollection;
	}
    
    public class CustomerCommunity_MilestonesInfo{
		@AuraEnabled public String ParentJob{get; set;}
		@AuraEnabled public String Description{get; set;}
		@AuraEnabled public DateTime Shipmentdate{get; set;}
		@AuraEnabled public String Status{get; set;}
	}
}