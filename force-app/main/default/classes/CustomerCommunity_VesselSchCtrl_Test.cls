@isTest
public class CustomerCommunity_VesselSchCtrl_Test {
    private class RestMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '[{"d":"({\r\n\r\n\"HeaderTexts\" : ({\r\n\r\n\"VoyageNumber\" : \"Voyage #\",\r\n\"VesselCode\" : \"Vessel\",\r\n\"VesselName\" : null\r\n,\r\n\"TypeCode\" : \"Type\",\r\n\"TypeDescription\" : null\r\n,\r\n\"StatusCode\" : \"Status\",\r\n\"StatusDescription\" : null\r\n,\r\n\"PortOfLoadingCode\" : \"POL\",\r\n\"PortOfLoadingCallId\" : \"POL Call ID\",\r\n\"PortOfLoadingDescription\" : null\r\n,\r\n\"PortOfDischargeCode\" : \"POD\",\r\n\"PortOfDischargeCallId\" : \"POD Call ID\",\r\n\"PortOfDischargeDescription\" : null\r\n,\r\n\"CentralPortCode\" : \"Centralport\",\r\n\"PortCallId\" : \"Call ID\",\r\n\"CentralPortDescription\" : null\r\n,\r\n\"StartDate\" : \"Departure\",\r\n\"EndDate\" : \"Arrival\",\r\n\"Duration\" : \"Duration\",\r\n\"HasTransshipment\" : \"Direct\",\r\n\"ServiceName\" : \"Service\",\r\n\"PortCode\" : \"Code\",\r\n\"PortName\" : \"Port\",\r\n\"ArrivalDate\" : \"Arrival Date\",\r\n\"SailingDate\" : \"Sailing Date\", \r\n\"VoyageMachineNumber\" : \"2111\"}) \r\n]\r\n})\r\n"}]';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    
    static testMethod void getPortOfLoadingAndDischargeTestMethod(){
        Test.startTest();
        CustomerCommunity_VesselSchController.getPortOfLoadingAndDischarge();
        Test.stopTest();
    }
    
    static testMethod void getVesselSchedulesListTestMethod(){
        Test.setMock(HttpCalloutMock.class, new RestMock());
        
        string year = '2008';
        string month = '10';
        string day = '5';
        string hour = '12';
        string minute = '20';
        string second = '20';
        string stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        
        Test.startTest();
        //Departure date is set as NULL to cover else-if scenario
        CustomerCommunity_VesselSchController.getVesselSchedulesList('Barcadera, Oranjestad, Aruba (AWBAR)', 'Bonaire (BQBON)', NULL, stringDate, '10', 'Test');
        
        //Arrival date is set as NULL to cover else-if scenario
        CustomerCommunity_VesselSchController.getVesselSchedulesList('Barcadera, Oranjestad, Aruba (AWBAR)', 'Bonaire (BQBON)', stringDate, NULL, '10', 'Test');
        
        //Exception case - To cover catch statements, arrival date format is incorrect
        CustomerCommunity_VesselSchController.getVesselSchedulesList('Barcadera, Oranjestad, Aruba (AWBAR)', 'Bonaire (BQBON)', stringDate, '11-March-2019', '10', 'Test');
        Test.stopTest();
    }
}