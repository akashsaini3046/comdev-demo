public class CustomerCommunity_ScheduleController {
    @AuraEnabled
    public static void getSailingSchedule() {
        String str = '{"polcode":"USJAX","podcode":"HNPCR", "departure":"4/12/2019", "arrival":"", "days":"28", "count":"15", "onlyWebPorts":"false", "dateFormat": "dd-MMM-yyyy", "includeTerminals":"false", "useETBandETD":"false", "service":"", "includeTransshipment":"true"}';
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://veloz.crowley.com/Softship.Schedule/(S(l1hm10lagaqktpqdsvzqa3ef))/WebServices/VoyageInfoService.asmx/GetVoyagesByPortOfDischarge/');
        request.setMethod('POST');
        request.setHeader('Content-Type','application/json; charset=UTF-8');
        request.setBody(str);
        System.debug('@@@ request - ' + request);
        Http service = new Http();
        HttpResponse response = service.send(request);
        System.Debug('@@@ - response - ' + response + '  body - '+response.getBody());     
    }
}