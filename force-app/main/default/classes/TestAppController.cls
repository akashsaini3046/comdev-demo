public class TestAppController {    
    @auraEnabled
    public static void savePDFOnAccount(){
        PageReference pdfPage = new PageReference('/apex/testPdf');
		pdfPage.getParameters().put('Id', '0010t00001D222Q');
        Blob pdfContent = pdfPage.getContent();
        
        Attachment attach1= new Attachment();
        attach1.ParentId = '0010t00001D222Q';
        attach1.Name = 'Test Attachment for PDF';
        attach1.Body = pdfContent;
        attach1.contentType = 'application/pdf';
		insert attach1;
        
    }
}