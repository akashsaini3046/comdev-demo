global class CrowleyCom_BatchSyncVesselLocation implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global Iterable<sObject> start(Database.BatchableContext bc) {
       return getDummnyRecord();
    }
    global void execute(Database.BatchableContext bc, List<sObject> records){
        	system.debug('BatchExecuted--->>>');
        	List<Vessel_Detail__c> listVesselDetail = new List<Vessel_Detail__c>();
            Map<String, Vessel_Detail__c> mapVessel = new Map<String, Vessel_Detail__c>();
            for(Vessel_Detail__c vessel : [Select Id, Code__c,VesselMmsi__c from Vessel_Detail__c Where RecordType.DeveloperName ='Vessel']){
                mapVessel.put(vessel.VesselMmsi__c ,vessel);
            }
            RecordType vesselRecordtype = [select Id from RecordType where SobjectType ='Vessel_Detail__c' And DeveloperName ='Vessel' Limit 1];

            HttpRequest requestinside = new HttpRequest();
            //requestinside.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            requestinside.setHeader('Content-Type', 'application/json');
            requestinside.setEndpoint('https://veloz.crowley.com/Softship.SeaMap/api/vessel/AISDATA.asmx/GetAisDataForAllVessels');
            requestinside.setMethod('GET');
            requestinside.setTimeout(120000);        
            String fieldDef = '{"companyCode" : "CRMLS"}';
            requestinside.setBody(fieldDef);
            HTTPResponse res = new http().send(requestinside);
            System.debug(res.getBody());            
            Map<String, Object> m =(Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            List<object> listVesselData = (List<object>)m.get('d');
            for(object vesselData : listVesselData){
                Map<String, Object> vesselDetail = (Map<String, Object>) vesselData;
                Vessel_Detail__c VD= new Vessel_Detail__c();
                if(mapVessel!=null && mapVessel.containsKey((String)vesselDetail.get('VesselMmsi'))){
                    VD.Id = mapVessel.get((String)vesselDetail.get('VesselMmsi')).Id;
                    system.debug(VD.Id+'<<<---->>>');
                }
                VD.Name = (String)vesselDetail.get('VesselName');
                VD.RecordTypeId = vesselRecordtype.Id;
                VD.Code__c = (String)vesselDetail.get('Code');
                VD.VesselMmsi__c = (String)vesselDetail.get('VesselMmsi');
                VD.Status__c = (String)vesselDetail.get('Status');
                VD.NextPortName__c = (String)vesselDetail.get('NextPortName');
                VD.Eta__c = (String)vesselDetail.get('Eta');
                VD.CalcEta__c = (String)vesselDetail.get('CalcEta');
                VD.DestinationMatch__c = String.valueOf(vesselDetail.get('DestinationMatch'));
                VD.DestinationMatchStatus__c = (String)vesselDetail.get('DestinationMatchStatus');
                VD.Latitude__c = Decimal.valueOf((String)vesselDetail.get('LAT'));
                VD.Longitude__c = Decimal.valueOf((String)vesselDetail.get('LON'));
                system.debug(VD.Latitude__c+'<<<---->>>'+VD.Longitude__c);
                VD.Speed__c = (String)vesselDetail.get('Speed');
                VD.Course__c = (String)vesselDetail.get('Course');
                VD.Draught__c = (String)vesselDetail.get('Draught');
                VD.ScheduleVoyageNumber__c = (String)vesselDetail.get('ScheduleVoyageNumber');
                VD.ScheduleNextPortCode__c = (String)vesselDetail.get('ScheduleNextPortCode');
                VD.ScheduleEta__c = (String)vesselDetail.get('ScheduleEta');
                VD.ScheduleETB__c = (String)vesselDetail.get('ScheduleETB');
                VD.NextPortId__c = String.valueOf(vesselDetail.get('NextPortId'));
                VD.VesselReportFWD__c = (String)vesselDetail.get('VesselReportFWD');
                VD.VesselReportAFT__c = (String)vesselDetail.get('VesselReportAFT');
                VD.Source__c = (String)vesselDetail.get('Source');
                VD.LastReportDate__c = (String)vesselDetail.get('LastReportDate');
                listVesselDetail.add(VD);
                
            }	
            upsert listVesselDetail;
    }    
    global void finish(Database.BatchableContext bc){
        
    }
    
    private List<Account> getDummnyRecord(){
        List<Account> dummyAccount = new List<Account>();
        dummyAccount.add(new Account(Name='Test'));        
        return dummyAccount;
    }
        
}