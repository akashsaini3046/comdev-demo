public class CC_Constants {
    public static final String GET_REQUEST_METHOD = 'GET';
    public static final String POST_REQUEST_METHOD = 'POST';
    public static final String PUT_REQUEST_METHOD = 'PUT';
    public static final String DELETE_REQUEST_METHOD = 'DELETE';
    public static final String BLANK_STRING = '';
    public static final String QUESTION_MARK_STRING = '?';
    public static final String EQUAL_STRING = '=';
    public static final String URL_ENCODE_FORMAT = 'UTF-8';
    public static final Integer DEFAULT_TIMEOUT = 30000;
    public static final String CLIENT_ID_STRING = 'client_id';
    public static final String CLIENT_SECRET_STRING = 'client_secret';
    public static final String CONTENT_TYPE = 'Content-Type';
}