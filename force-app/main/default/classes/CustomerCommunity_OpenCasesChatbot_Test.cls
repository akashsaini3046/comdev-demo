@isTest
public class CustomerCommunity_OpenCasesChatbot_Test {
		
    static testmethod void getOpenCases_Test(){
        
        Account test_account;
        List<Case> test_cases = new List<Case>();
        List<String> testContactID = new List<String>();
        User test_user = [Select Id from user where Profile.Name = 'System Administrator' LIMIT 1];
        System.runAs(test_user){
            
          test_account=TestDataUtility.createAccount('Nagarro', null,'Customer', 1)[0];
            DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
            List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
            Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = test_account.Id)}, 'BL1', 'City1', 
                                                                                    stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                    'US', 1)[0];
            System.assertNotEquals(NULL, businessLocationObj.Country__c);
            
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
            System.assertNotEquals(NULL, recordTypeId);
            Contact con= TestDataUtility.createContact(recordTypeId, test_account.Id , new List<Address__c>{businessLocationObj}, null, 
                                                       'Test Community Con1', 'communityexample@gmail.com', '11111111111', false, 
                                                       Null, 1)[0];
            
            test_cases = TestDataUtility.createCase(test_account.Id, 'Email', test_user.Id, 'In Progress', 4);
            
            testContactID.add(con.Id);
            
            Test.startTest();
            
            CustomerCommunity_OpenCasesChatbot.getOpenCases(testContactID);
            
            Test.stopTest();
        }
        
    }
    
    //catch method
    static testmethod void getOpenCases_catchtest(){
        
        List<String> falsecontact = new List<String>();
        
        CustomerCommunity_OpenCasesChatbot.getOpenCases(falseContact);
    }
}