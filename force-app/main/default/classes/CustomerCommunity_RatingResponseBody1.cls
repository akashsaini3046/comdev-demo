public class CustomerCommunity_RatingResponseBody1 {
    @AuraEnabled public Boolean success{get; set;}
    @AuraEnabled public result result{get; set;}
     public class result {
         @AuraEnabled public Integer RouteId{get; set;}
         @AuraEnabled public boolean success{get; set;}
         @AuraEnabled public String Message{get; set;}
         @AuraEnabled public Boolean OverallSuccess{get; set;}
         @AuraEnabled public Boolean SuccessForPurchase{get; set;}
         @AuraEnabled public Boolean SuccessForSales{get; set;}
         @AuraEnabled public Rate Rate{get; set;}
         @AuraEnabled public SelectedRoute SelectedRoute{get; set;}
         @AuraEnabled public Revenue Revenue{get; set;}
    }
    public class Rate {
        @AuraEnabled public Double Contribution{get; set;}
        @AuraEnabled public Double Cost{get; set;}
        //public CodeVal CurrencyVal{get; set;}
        @AuraEnabled public String DebuggerDisplay{get; set;}
        @AuraEnabled public String Errors{get; set;}
        @AuraEnabled public Boolean IsBestFitting{get; set;}
        @AuraEnabled public Double Revenue{get; set;}
        @AuraEnabled public Integer Strategy{get; set;}
        @AuraEnabled public Boolean Success{get; set;}
        @AuraEnabled public Boolean isSaved{get; set;}
    }
    public class CodeVal {
        @AuraEnabled public String Code{get; set;}
    }
    public class SelectedRoute {
        @AuraEnabled public Transport Transport{get; set;}
        @AuraEnabled public Integer RouteId{get; set;}
        @AuraEnabled public Boolean IsSelected{get; set;}
        @AuraEnabled public List<Legs> Legs{get; set;}
        //public String SalesTariffIds{get; set;}
        //public String PurchaseTariffIds{get; set;}
        //public String OptionalServiceGuids{get; set;}
        @AuraEnabled public String DebuggerDisplay{get; set;}
    }
    public class Transport {
        @AuraEnabled public String PortOfLoading{get; set;}
        @AuraEnabled public String PortOfDischarge{get; set;}
        @AuraEnabled public String PortOfLoadingBerth{get; set;}
        @AuraEnabled public String PortOfDischargeBerth{get; set;}
        @AuraEnabled public String DeliveryTermsPol{get; set;}
        public String DeliveryTermsPod{get; set;}
        public String PlaceOfDelivery{get; set;}
        public String PlaceOfDeliveryZip{get; set;}
        public String PlaceOfReceipt{get; set;}
        public String PlaceOfReceiptZip{get; set;}
        public String PortOfDischargeBerthSequenceNumber{get; set;}
        public String PortOfLoadingBerthSequenceNumber{get; set;}
        public Integer PortOfLoadingSequenceNumber{get; set;}
        public Double VoyageMachineNumber{get; set;}
        public String Voyage{get; set;}
        public String TariffReference{get; set;}
        public String ServiceCode{get; set;}
        public String TariffCategory1{get; set;}
        public String TariffCategory2{get; set;}
        public String ShipmentType{get; set;}
        public String VesselCode{get; set;}
        public String ShipmentCondition{get; set;}
    }
    public class Legs {
        public String FromLocation{get; set;}
        public String ToLocation{get; set;}
        public String FromSubLocation{get; set;}
        public String ToSubLocation{get; set;}
        public String VoyageMachineNumber{get; set;}
        public String Voyage{get; set;}
        public String VoyageNumber{get; set;}
        public Integer DistanceKm{get; set;}
        public String DistanceMi{get; set;}
        public String DirectTransportFlag{get; set;}
        public String SupplierNumber{get; set;}
        public String TransportMode{get; set;}
        public String ServiceCode{get; set;}
        public String VesselCode{get; set;}
        public Integer LegSequenceNumber{get; set;}
        public String SplitFlag{get; set;}
        public String FromLocationSequenceNumber{get; set;}
        public String FromSubLocationSequenceNumber{get; set;}
        public String ToLocationSequenceNumber{get; set;}
        public String ToSubLocationSequenceNumber{get; set;}
        public String TransshipmentMatrixDetailId{get; set;}
        public Integer PrimaryKeySequenceNumber{get; set;}
        public String DebuggerDisplay{get; set;}
    }
    
    public class Revenue {
        public String NetFreightCode{get; set;}
        @AuraEnabled public List<Positions> Positions{get; set;}
    }
	public class Positions {
        @AuraEnabled public String ChargeCode{get; set;}
        @AuraEnabled public String ChargeDescription{get; set;}
        @AuraEnabled public String Basis{get; set;}
		@AuraEnabled public String Per{get; set;}
		@AuraEnabled public Decimal Rate{get; set;}
		//public String Currency {get; set;}
    }
}