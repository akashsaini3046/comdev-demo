public with sharing class CustomerCommunity_ReBookingChatbot {
    public class InputFields {
        @InvocableVariable(required=true)
        public String sBookingNumber;
        @InvocableVariable(required=true)
        public Date sReadyDate;
        @InvocableVariable(required=true)
        public String sContactId;
    }  
    public class BookingName {
        @InvocableVariable(required=true)
        public String sBookingName;
    }
    @InvocableMethod(label='Re Create Bookings')
    public static List<BookingName> cloneBooking(List<InputFields> inputParam) {
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_ReBookingChatbot:cloneBooking() with parameters'); 
        List<Booking__c> listBookingRecord = new List<Booking__C>();
        List<BookingName> listResponses = new List<BookingName>();
        BookingName bookingResponse = new BookingName();
        bookingResponse.sBookingName = CustomerCommunity_Constants.EMPTY_STRING;
        
        try{
            if(inputParam[0].sBookingNumber != '' && inputParam[0].sBookingNumber != Null && inputParam[0].sReadyDate != Null){
                listBookingRecord = [SELECT Id, Origin_Type__c, Destination_Type__c, Contact__c, Account__c, OwnerId, Status__c FROM Booking__c WHERE Booking_Number__c = :inputParam[0].sBookingNumber LIMIT 1];
                if(!listBookingRecord.isEmpty()){
                    listBookingRecord[0] = CustomerCommunity_ReBookingController.cloneBookingRecords(listBookingRecord[0].Id, inputParam[0].sReadyDate, Id.valueOf(inputParam[0].sContactId));
                    CustomerCommunity_CICSBookingChatbot.confirmReBooking(listBookingRecord[0].Name);
                    bookingResponse.sBookingName = bookingResponse.sBookingName + 'A Booking record has been created with Booking Number - ' + listBookingRecord[0].Name + '. Booking confirmation will be notified via text message.'; 
                }
                else{
                    bookingResponse.sBookingName = bookingResponse.sBookingName + 'Booking Record not Found';
                }
            }
            if(bookingResponse.sBookingName == Null || bookingResponse.sBookingName == '')
                bookingResponse.sBookingName = bookingResponse.sBookingName + 'INVALID INPUT';
            listResponses.add(bookingResponse);
            System.debug(LoggingLevel.INFO,'Exiting from CustomerCommunity_ReBookingChatbot:cloneBooking() returning--'+listResponses);
            return listResponses;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            bookingResponse.sBookingName = bookingResponse.sBookingName + 'Some error occurred while processing your request. Please try again later.';
            listResponses.add(bookingResponse);
            return listResponses;
        } 
    }
}