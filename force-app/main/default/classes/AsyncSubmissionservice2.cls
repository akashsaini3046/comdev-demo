//Generated by wsdl2apex

public class AsyncSubmissionservice2 {
    public class SubmitResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.SubmissionResult getValue() {
            submissionservice2.SubmitResponse_element response = (submissionservice2.SubmitResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class ExtractNextResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.ExtractionResult getValue() {
            submissionservice2.ExtractNextResponse_element response = (submissionservice2.ExtractNextResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class RegisterResourceResponse_elementFuture extends System.WebServiceCalloutFuture {
        public void getValue() {
            System.WebServiceCallout.endInvoke(this);
        }
    }
    public class UploadFileResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.WSFile getValue() {
            submissionservice2.UploadFileResponse_element response = (submissionservice2.UploadFileResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class RetrieveResourceResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.WSFile getValue() {
            submissionservice2.RetrieveResourceResponse_element response = (submissionservice2.RetrieveResourceResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class DeleteResourceResponse_elementFuture extends System.WebServiceCalloutFuture {
        public void getValue() {
            System.WebServiceCallout.endInvoke(this);
        }
    }
    public class UploadFileAppendResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.WSFile getValue() {
            submissionservice2.UploadFileAppendResponse_element response = (submissionservice2.UploadFileAppendResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class ConvertFileResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.ConversionResult getValue() {
            submissionservice2.ConvertFileResponse_element response = (submissionservice2.ConvertFileResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class SubmitXMLResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.SubmissionResults getValue() {
            submissionservice2.SubmitXMLResponse_element response = (submissionservice2.SubmitXMLResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class DownloadFileResponse_elementFuture extends System.WebServiceCalloutFuture {
        public String getValue() {
            submissionservice2.DownloadFileResponse_element response = (submissionservice2.DownloadFileResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class SubmitTransportResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.SubmissionResult getValue() {
            submissionservice2.SubmitTransportResponse_element response = (submissionservice2.SubmitTransportResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class ExtractFirstResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.ExtractionResult getValue() {
            submissionservice2.ExtractFirstResponse_element response = (submissionservice2.ExtractFirstResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class ListResourcesResponse_elementFuture extends System.WebServiceCalloutFuture {
        public submissionservice2.Resources getValue() {
            submissionservice2.ListResourcesResponse_element response = (submissionservice2.ListResourcesResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.return_x;
        }
    }
    public class AsyncSubmissionServiceSoap {
        public String endpoint_x = 'https://na2.esker.com:443/EDPWS/EDPWS.dll?Handler=Submission2Handler';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        public submissionservice2.ExtractionHeader ExtractionHeaderValue;
        public submissionservice2.SessionHeader SessionHeaderValue;
        private String ExtractionHeaderValue_hns = 'ExtractionHeaderValue=urn:SubmissionService2';
        private String SessionHeaderValue_hns = 'SessionHeaderValue=urn:SubmissionService2';
        private String[] ns_map_type_info = new String[]{'urn:SubmissionService2', 'submissionservice2'};
        public AsyncSubmissionservice2.SubmitResponse_elementFuture beginSubmit(System.Continuation continuation,String subject,submissionservice2.BusinessData document,submissionservice2.BusinessRules rules) {
            submissionservice2.Submit_element request_x = new submissionservice2.Submit_element();
            request_x.subject = subject;
            request_x.document = document;
            request_x.rules = rules;
            return (AsyncSubmissionservice2.SubmitResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.SubmitResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#Submit',
              'urn:SubmissionService2',
              'Submit',
              'urn:SubmissionService2',
              'SubmitResponse',
              'submissionservice2.SubmitResponse_element'}
            );
        }
        public AsyncSubmissionservice2.ExtractNextResponse_elementFuture beginExtractNext(System.Continuation continuation,submissionservice2.ExtractionParameters params) {
            submissionservice2.ExtractNext_element request_x = new submissionservice2.ExtractNext_element();
            request_x.params = params;
            return (AsyncSubmissionservice2.ExtractNextResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.ExtractNextResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#ExtractNext',
              'urn:SubmissionService2',
              'ExtractNext',
              'urn:SubmissionService2',
              'ExtractNextResponse',
              'submissionservice2.ExtractNextResponse_element'}
            );
        }
        public AsyncSubmissionservice2.RegisterResourceResponse_elementFuture beginRegisterResource(System.Continuation continuation,submissionservice2.WSFile resource,String type_x,Boolean published,Boolean overwritePrevious) {
            submissionservice2.RegisterResource_element request_x = new submissionservice2.RegisterResource_element();
            request_x.resource = resource;
            request_x.type_x = type_x;
            request_x.published = published;
            request_x.overwritePrevious = overwritePrevious;
            return (AsyncSubmissionservice2.RegisterResourceResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.RegisterResourceResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#RegisterResource',
              'urn:SubmissionService2',
              'RegisterResource',
              'urn:SubmissionService2',
              'RegisterResourceResponse',
              'submissionservice2.RegisterResourceResponse_element'}
            );
        }
        public AsyncSubmissionservice2.UploadFileResponse_elementFuture beginUploadFile(System.Continuation continuation,String fileContent,String name) {
            submissionservice2.UploadFile_element request_x = new submissionservice2.UploadFile_element();
            request_x.fileContent = fileContent;
            request_x.name = name;
            return (AsyncSubmissionservice2.UploadFileResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.UploadFileResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#UploadFile',
              'urn:SubmissionService2',
              'UploadFile',
              'urn:SubmissionService2',
              'UploadFileResponse',
              'submissionservice2.UploadFileResponse_element'}
            );
        }
        public AsyncSubmissionservice2.RetrieveResourceResponse_elementFuture beginRetrieveResource(System.Continuation continuation,String resourceName,String type_x,Boolean published,String eMode) {
            submissionservice2.RetrieveResource_element request_x = new submissionservice2.RetrieveResource_element();
            request_x.resourceName = resourceName;
            request_x.type_x = type_x;
            request_x.published = published;
            request_x.eMode = eMode;
            return (AsyncSubmissionservice2.RetrieveResourceResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.RetrieveResourceResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#RetrieveResource',
              'urn:SubmissionService2',
              'RetrieveResource',
              'urn:SubmissionService2',
              'RetrieveResourceResponse',
              'submissionservice2.RetrieveResourceResponse_element'}
            );
        }
        public AsyncSubmissionservice2.DeleteResourceResponse_elementFuture beginDeleteResource(System.Continuation continuation,String resourceName,String type_x,Boolean published) {
            submissionservice2.DeleteResource_element request_x = new submissionservice2.DeleteResource_element();
            request_x.resourceName = resourceName;
            request_x.type_x = type_x;
            request_x.published = published;
            return (AsyncSubmissionservice2.DeleteResourceResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.DeleteResourceResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#DeleteResource',
              'urn:SubmissionService2',
              'DeleteResource',
              'urn:SubmissionService2',
              'DeleteResourceResponse',
              'submissionservice2.DeleteResourceResponse_element'}
            );
        }
        public AsyncSubmissionservice2.UploadFileAppendResponse_elementFuture beginUploadFileAppend(System.Continuation continuation,String fileContent,submissionservice2.WSFile destWSFile) {
            submissionservice2.UploadFileAppend_element request_x = new submissionservice2.UploadFileAppend_element();
            request_x.fileContent = fileContent;
            request_x.destWSFile = destWSFile;
            return (AsyncSubmissionservice2.UploadFileAppendResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.UploadFileAppendResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#UploadFileAppend',
              'urn:SubmissionService2',
              'UploadFileAppend',
              'urn:SubmissionService2',
              'UploadFileAppendResponse',
              'submissionservice2.UploadFileAppendResponse_element'}
            );
        }
        public AsyncSubmissionservice2.ConvertFileResponse_elementFuture beginConvertFile(System.Continuation continuation,submissionservice2.WSFile inputFile,submissionservice2.ConversionParameters params) {
            submissionservice2.ConvertFile_element request_x = new submissionservice2.ConvertFile_element();
            request_x.inputFile = inputFile;
            request_x.params = params;
            return (AsyncSubmissionservice2.ConvertFileResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.ConvertFileResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#ConvertFile',
              'urn:SubmissionService2',
              'ConvertFile',
              'urn:SubmissionService2',
              'ConvertFileResponse',
              'submissionservice2.ConvertFileResponse_element'}
            );
        }
        public AsyncSubmissionservice2.SubmitXMLResponse_elementFuture beginSubmitXML(System.Continuation continuation,submissionservice2.XMLDescription xml) {
            submissionservice2.SubmitXML_element request_x = new submissionservice2.SubmitXML_element();
            request_x.xml = xml;
            return (AsyncSubmissionservice2.SubmitXMLResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.SubmitXMLResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#SubmitXML',
              'urn:SubmissionService2',
              'SubmitXML',
              'urn:SubmissionService2',
              'SubmitXMLResponse',
              'submissionservice2.SubmitXMLResponse_element'}
            );
        }
        public AsyncSubmissionservice2.DownloadFileResponse_elementFuture beginDownloadFile(System.Continuation continuation,submissionservice2.WSFile wsFile) {
            submissionservice2.DownloadFile_element request_x = new submissionservice2.DownloadFile_element();
            request_x.wsFile = wsFile;
            return (AsyncSubmissionservice2.DownloadFileResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.DownloadFileResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#DownloadFile',
              'urn:SubmissionService2',
              'DownloadFile',
              'urn:SubmissionService2',
              'DownloadFileResponse',
              'submissionservice2.DownloadFileResponse_element'}
            );
        }
        public AsyncSubmissionservice2.SubmitTransportResponse_elementFuture beginSubmitTransport(System.Continuation continuation,submissionservice2.Transport transport) {
            submissionservice2.SubmitTransport_element request_x = new submissionservice2.SubmitTransport_element();
            request_x.transport = transport;
            return (AsyncSubmissionservice2.SubmitTransportResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.SubmitTransportResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#SubmitTransport',
              'urn:SubmissionService2',
              'SubmitTransport',
              'urn:SubmissionService2',
              'SubmitTransportResponse',
              'submissionservice2.SubmitTransportResponse_element'}
            );
        }
        public AsyncSubmissionservice2.ExtractFirstResponse_elementFuture beginExtractFirst(System.Continuation continuation,submissionservice2.BusinessData document,submissionservice2.BusinessRules rules,submissionservice2.ExtractionParameters params) {
            submissionservice2.ExtractFirst_element request_x = new submissionservice2.ExtractFirst_element();
            request_x.document = document;
            request_x.rules = rules;
            request_x.params = params;
            return (AsyncSubmissionservice2.ExtractFirstResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.ExtractFirstResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#ExtractFirst',
              'urn:SubmissionService2',
              'ExtractFirst',
              'urn:SubmissionService2',
              'ExtractFirstResponse',
              'submissionservice2.ExtractFirstResponse_element'}
            );
        }
        public AsyncSubmissionservice2.ListResourcesResponse_elementFuture beginListResources(System.Continuation continuation,String type_x,Boolean published) {
            submissionservice2.ListResources_element request_x = new submissionservice2.ListResources_element();
            request_x.type_x = type_x;
            request_x.published = published;
            return (AsyncSubmissionservice2.ListResourcesResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSubmissionservice2.ListResourcesResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              '#ListResources',
              'urn:SubmissionService2',
              'ListResources',
              'urn:SubmissionService2',
              'ListResourcesResponse',
              'submissionservice2.ListResourcesResponse_element'}
            );
        }
    }
}