public class CustomerCommunity_QuotationResponseInfo{
		@AuraEnabled public Body Body{get;set;}
	public class serviceTypes{
		@AuraEnabled public String name{get;set;}
		@AuraEnabled public String type{get;set;}
		@AuraEnabled public String description{get;set;}
		@AuraEnabled public String code{get;set;}
	}
	public class services{
		@AuraEnabled public list<serviceTypes> serviceTypes{get;set;}
		@AuraEnabled public String serviceMethod{get;set;}
	}
	public class messageStatus{
		@AuraEnabled public String status{get;set;}
	}
	public class LTLResponseDetail{
		@AuraEnabled public String error{get;set;}
		@AuraEnabled public String rate{get;set;}
		@AuraEnabled public String nmfcClass{get;set;}
		@AuraEnabled public String weight{get;set;}
		@AuraEnabled public String charge{get;set;}
	}
	public class LTLRateShipmentSimpleResponse{
		@AuraEnabled public String errorCode{get;set;}
		@AuraEnabled public String shipmentDateCCYYMMDD{get;set;}
		@AuraEnabled public String originPostalCode{get;set;}
		@AuraEnabled public String shipmentID{get;set;}
		@AuraEnabled public String originCountry{get;set;}
		@AuraEnabled public String tariffName{get;set;}
		@AuraEnabled public String effectiveDate{get;set;}
		@AuraEnabled public String totalCharge{get;set;}
		@AuraEnabled public details details{get;set;}
		@AuraEnabled public LTLRateShipmentSimpleResponse LTLRateShipmentSimpleResponse{get;set;}
		@AuraEnabled public String destinationPostalCode{get;set;}
		@AuraEnabled public String destinationCountry{get;set;}
	}
	public class details{
		@AuraEnabled public LTLResponseDetail LTLResponseDetail{get;set;}
	}
	public class CarrierServicesResponse{
		@AuraEnabled public messageStatus messageStatus{get;set;}
		@AuraEnabled public carrierServices carrierServices{get;set;}
		@AuraEnabled public CarrierServicesResponse CarrierServicesResponse{get;set;}
	}
	public class carrierServices{
		@AuraEnabled public services services{get;set;}
		@AuraEnabled public String SCAC{get;set;}
		@AuraEnabled public String carrierName{get;set;}
	}
	public class Body{
		@AuraEnabled public CarrierServicesResponse CarrierServicesResponse{get;set;}
		@AuraEnabled public LTLRateShipmentSimpleResponse LTLRateShipmentSimpleResponse{get;set;}
	}
}