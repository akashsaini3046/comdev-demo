public class CrowleyCommunity_AccountDetail {
    @AuraEnabled public String companyName {get;set;}
    @AuraEnabled public String userEmail {get;set;}
    @AuraEnabled public String userName {get;set;}
    @AuraEnabled public String firstName {get;set;}
    @AuraEnabled public String lastName {get;set;}
    @AuraEnabled public String phoneNumber {get;set;}
    @AuraEnabled public String businessLocationName {get;set;}
    @AuraEnabled public String addressLine1 {get;set;}
    @AuraEnabled public String addressLine2 {get;set;}
    @AuraEnabled public String countryName {get;set;}
    @AuraEnabled public String stateName {get;set;}
    @AuraEnabled public String cityName {get;set;}
    @AuraEnabled public String zipCode {get;set;}
    @AuraEnabled public Boolean newContact {get;set;}
    @AuraEnabled public Boolean addNewAccount {get;set;}
    @AuraEnabled public Boolean addNewLocation {get;set;}
    @AuraEnabled public String accountRecordId {get;set;}
    @AuraEnabled public String contactRecordId {get;set;}
    @AuraEnabled public String locationRecordId {get;set;}
}