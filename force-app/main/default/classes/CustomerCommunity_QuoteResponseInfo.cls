public class CustomerCommunity_QuoteResponseInfo{
		@AuraEnabled public Body Body{get;set;}
	public class TransitPlanningResponse{
		@AuraEnabled public list<carriers> carriers{get;set;}
		@AuraEnabled public messageStatus messageStatus{get;set;}
		@AuraEnabled public origin origin{get;set;}
		@AuraEnabled public destination destination{get;set;}
		@AuraEnabled public String shipmentId{get;set;}
		@AuraEnabled public pickUpWindow pickUpWindow{get;set;}
	}
	public class transitCalendar{
		@AuraEnabled public String pickupDate{get;set;}
		@AuraEnabled public String estimatedDeliveryDate{get;set;}
		@AuraEnabled public Integer calendarDays{get;set;}
	}
	public class terminalCodes{
		@AuraEnabled public String origin{get;set;}
		@AuraEnabled public String destination{get;set;}
	}
	public class serviceDetail{
		@AuraEnabled public String origin{get;set;}
		@AuraEnabled public String service{get;set;}
		@AuraEnabled public String destination{get;set;}
	}
	public class pickUpWindow{
		@AuraEnabled public String earliestDate{get;set;}
		@AuraEnabled public String latestDate{get;set;}
	}
	public class origin{
		@AuraEnabled public String postalCode{get;set;}
		@AuraEnabled public String countryCode{get;set;}
	}
	public class messageStatus{
		@AuraEnabled public String status{get;set;}
	}
	public class LTLResponseDetail{
		@AuraEnabled public String rate{get;set;}
		@AuraEnabled public String nmfcClass{get;set;}
		@AuraEnabled public String error{get;set;}
		@AuraEnabled public String weight{get;set;}
		@AuraEnabled public String charge{get;set;}
	}
	public class LTLRateShipmentSimpleResponse{
		@AuraEnabled public String tariffName{get;set;}
		@AuraEnabled public String shipmentDateCCYYMMDD{get;set;}
		@AuraEnabled public String shipmentID{get;set;}
		@AuraEnabled public String originPostalCode{get;set;}
		@AuraEnabled public String totalCharge{get;set;}
		@AuraEnabled public String originCountry{get;set;}
		@AuraEnabled public details details{get;set;}
		@AuraEnabled public String effectiveDate{get;set;}
		@AuraEnabled public String errorCode{get;set;}
		@AuraEnabled public String destinationPostalCode{get;set;}
		@AuraEnabled public String destinationCountry{get;set;}
	}
	public class details{
		@AuraEnabled public LTLResponseDetail LTLResponseDetail{get;set;}
	}
	public class destination{
		@AuraEnabled public String address1{get;set;}
		@AuraEnabled public String postalCode{get;set;}
		@AuraEnabled public String countryCode{get;set;}
		@AuraEnabled public String stateProvince{get;set;}
		@AuraEnabled public String city{get;set;}
	}
	public class dataGroup{
		@AuraEnabled public String expirationDate{get;set;}
		@AuraEnabled public String name{get;set;}
		@AuraEnabled public String supportId{get;set;}
		@AuraEnabled public String releaseDate{get;set;}
	}
	public class carrierServiceDetail{
		@AuraEnabled public String serviceMethod{get;set;}
		@AuraEnabled public String serviceType{get;set;}
		@AuraEnabled public String serviceCode{get;set;}
		@AuraEnabled public String SCAC{get;set;}
		@AuraEnabled public String carrierName{get;set;}
	}
	public class carriers{
		@AuraEnabled public dataGroup dataGroup{get;set;}
		@AuraEnabled public transitCalendar transitCalendar{get;set;}
		@AuraEnabled public Integer transitDays{get;set;}
		@AuraEnabled public terminalCodes terminalCodes{get;set;}
		@AuraEnabled public serviceDetail serviceDetail{get;set;}
		@AuraEnabled public messageStatus messageStatus{get;set;}
		@AuraEnabled public carrierServiceDetail carrierServiceDetail{get;set;}
	}
	public class Body{
		@AuraEnabled public TransitPlanningResponse TransitPlanningResponse{get;set;}
		@AuraEnabled public LTLRateShipmentSimpleResponse LTLRateShipmentSimpleResponse{get;set;}
        @AuraEnabled public String OceanRate{get;set;}
        @AuraEnabled public String TotalRate{get;set;}
	}
}