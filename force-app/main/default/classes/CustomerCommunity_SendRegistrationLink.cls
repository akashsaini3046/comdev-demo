global without sharing class CustomerCommunity_SendRegistrationLink {
    @AuraEnabled
    public static String sentRegistrationEmail(String username){
        Set<ID> setUserIDs = new Set<ID>();
        List<Messaging.SingleEmailMessage> listmails =  new List<Messaging.SingleEmailMessage>();  
        try { 
            List<User> listUser = [Select id from User where userName = :username];
            System.debug('listUser@@'+listUser);
            EmailTemplate emailTemplate = [Select id from EmailTemplate where DeveloperName= : 'CustomerCommunity_WelcomeEmail'];
            System.debug('emailTemplate@@'+emailTemplate);
            for(User recordUser : listUser) {
                Messaging.SingleEmailMessage emailMessage =  new Messaging.SingleEmailMessage();
                emailMessage.setTemplateId(emailTemplate.id);	
                emailMessage.setTargetObjectId(recordUser.Id);
                emailMessage.setSenderDisplayName('Salesforce Administrator');
                emailMessage.saveAsActivity = false;
                listmails.add(emailMessage);
                System.debug('email Message :'+ emailMessage);
            }  
            if(!listmails.isEmpty()){
                System.debug('listmails :'+ listmails);
                Messaging.sendEmail(listmails);
                return CustomerCommunity_Constants.TRUE_MESSAGE;
            }
            else{
                return CustomerCommunity_Constants.FALSE_MESSAGE;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CustomerCommunity_Constants.ERROR_MESSAGE;
        }
    }
}