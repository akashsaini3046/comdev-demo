public with sharing class CustomerCommunity_ChatbotIdentifyPorts{
    public static void identifyPorts(String userInput){
        String originPort = ''; String destinationPort = '';
        List<String> listOriginKeywords = new List<String>{'from', 'leave', 'depart'};
        List<String> listDestinationKeywords = new List<String>{'to', 'reach', 'arrive'};
        Schema.DescribeFieldResult portFieldResult = Find_Route__c.Origin_Port__c.getDescribe();
        List<Schema.PicklistEntry> portList = portFieldResult.getPicklistValues();
        for (Schema.PicklistEntry port: portList) {
            if(userInput.toLowerCase().contains(port.getLabel().substring(4).toLowerCase())){
                String currentPort = port.getLabel().substring(4).toLowerCase();
                Integer currentPortIndex = userInput.toLowerCase().indexOf(currentPort);
                if(originPort == ''){
                    for(String keyword : listOriginKeywords){
                        if(userInput.toLowerCase().contains(keyword) && userInput.toLowerCase().indexOf(keyword) < currentPortIndex && originPort == ''){
                            originPort = port.getLabel().substring(4);
                            System.debug('@@@ Origin Port -> ' + originPort);
                            break;
                        }
                    }
                }
                else if(destinationPort == ''){
                    for(String keyword : listDestinationKeywords){
                        if(userInput.toLowerCase().contains(keyword) && userInput.toLowerCase().indexOf(keyword) < currentPortIndex && destinationPort == ''){
                            destinationPort = port.getLabel().substring(4);
                            System.debug('@@@ Destination Port -> ' + destinationPort);
                            break;
                        }
                    }
                }
            }
        }
    }
}