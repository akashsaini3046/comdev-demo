@isTest
public class CustomerCommunity_ControlCenterCtrlTest {
    public static testmethod void testfetchBookingNegative(){
        //exception
        Header__c h = CustomerCommunity_ControlCenterCtrl.fetchBooking();
    }
    public static testmethod void testfetchBookingNegative2(){
        //null case
        User u;
         System.runAs(new User(Id = UserInfo.getUserId())){             
            Account portalAccount = new Account(name = 'portalAccount');//create a portal account first
            insert portalAccount;
            Contact portalContact = new contact(LastName = 'portalContact', AccountId = portalAccount.Id,Email='standardcontact@testorg.com'); //create a portal contact
            insert portalContact;            
            Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus Login User' LIMIT 1];
            u = new User( email='test.test@gmail.com',
                         profileid = p.Id, 
                         UserName='test.test@gmail.com', 
                         Alias = 'GDS',
                         TimeZoneSidKey='America/New_York',
                         EmailEncodingKey='ISO-8859-1',
                         LocaleSidKey='en_US', 
                         LanguageLocaleKey='en_US',
                         ContactId = portalContact.Id,
                         PortalRole = 'Manager',
                         FirstName = 'Genelia',
                         LastName = 'Dsouza');
            insert u;
        }
        System.runAs(u) {
            List<Header__c> headers = new List<Header__c>();
            Header__c header = new Header__c(Status__c = 'Uploaded');
            headers.add(header);
            insert headers;            
            System.debug(header);
            Header__c h = CustomerCommunity_ControlCenterCtrl.fetchBooking();
        }
    }
    public static testmethod void testfetchBooking(){
        //positive test cases
        User u;
        Booking__c b1 ,b2;
        System.runAs(new User(Id = UserInfo.getUserId())){ 
            
            Account portalAccount = new Account(name = 'portalAccount');//create a portal account first
            insert portalAccount;
            Contact portalContact = new contact(LastName = 'portalContact', AccountId = portalAccount.Id,Email='standardcontact@testorg.com'); //create a portal contact
            insert portalContact;
            List<Booking__c> bookingList = new List<Booking__c>();
            b1 = new Booking__c(Origin__c = 'AW, ARUBA', Account__c = portalAccount.id);
            b2 = new Booking__c(Origin__c = 'AW, ARUBA', Account__c = portalAccount.id);
            
            bookingList.add(b1);
            bookingList.add(b2);
            
            insert bookingList;
            
            Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus Login User' LIMIT 1];
            u = new User( email='test.test@gmail.com',
                         profileid = p.Id, 
                         UserName='test.test@gmail.com', 
                         Alias = 'GDS',
                         TimeZoneSidKey='America/New_York',
                         EmailEncodingKey='ISO-8859-1',
                         LocaleSidKey='en_US', 
                         LanguageLocaleKey='en_US',
                         ContactId = portalContact.Id,
                         PortalRole = 'Manager',
                         FirstName = 'Genelia',
                         LastName = 'Dsouza');
            insert u;
        }
        System.runAs(u) {
            List<Header__c> headers = new List<Header__c>();
            Header__c header = new Header__c(Booking__c = b1.id,Status__c = 'Available for Review');
            headers.add(header);
            insert headers;            
            System.debug(header);
            Header__c h = CustomerCommunity_ControlCenterCtrl.fetchBooking();
        }
    }
}