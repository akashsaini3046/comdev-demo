public class ShippingInstructionsTriggerHandler { 
    public static void updateShortenURL(Set<Id> setHeaderId){
       CustomerCommunity_ShipmentController.updateShortenURL(setHeaderId);
    }
    public static void updateBookingFields(List<Header__c> listHeader){
       CustomerCommunity_ShipmentController.updateBookingFields(listHeader);  
    }
}