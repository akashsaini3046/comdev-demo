public class CustomerCommunity_BookingCreationRequest{
    public String originType{get; set;}
    public String destinationType{get; set;}
    public Boolean BookAndRate{get; set;}
    public String ContainerMode{get; set;}
    //public String AWBServiceLevel{get; set;}
    public String TotalVolume{get; set;}
    public String TotalWeight{get; set;}
    public String TotalWeightUnit{get; set;}
    public String TotalVolumeUnit{get; set;}
    public party party{get; set;}
    public List<shipments> shipments{get; set;}
    
    public class party{
        public partyInfo shipper{get; set;}
        public partyInfo consignee{get; set;}
        public partyInfo customer{get; set;}
    }
    public class partyInfo{
        public String type{get; set;}
        public String cvif{get; set;}
        public String refNumber{get; set;}
        public String name{get; set;}
        public String contactName{get; set;}
        public String contactPhoneNumber{get; set;}
        public String faxNumber{get; set;}
        public String addressLine1{get; set;}
        public String addressLine2{get; set;}
        public String city{get; set;}
        public String state{get; set;}
        public String country{get; set;}
        public String zip{get; set;}
    }
    public class shipments{
        public String numberVal{get; set;}
        public String originCode{get; set;}
        public String destinationCode{get; set;}
        public List<voyages> voyages{get; set;}
        public List<freightDetails> freightDetails{get; set;}
        public PackingLineCollection PackingLineCollection{get; set;}
    }
    public class voyages{
        public String estimateSailDate{get; set;}
    }
    public class freightDetails{
        public List<commodities> commodities{get; set;}
        public List<requirements> requirements{get; set;}
    }
    public class commodities{
        public String numberVal{get; set;}
        public String packageGroup{get; set;}
        public String dotName{get; set;}
        public String emergencyContactName{get; set;}
        public String emergencyContactPhoneNumber{get; set;}
        public String imoClass{get; set;}
        public String technicalName{get; set;}
        public String imoDGPageClass{get; set;}
        public Boolean isHazardous{get; set;}
    }
    public class requirements{
        public String quantity{get; set;}
        public String category{get; set;}
        public String length{get; set;}
        public String rrInd{get; set;}
    }
    public class PackingLineCollection{
        public List<PackingLine> PackingLine{get; set;}
    }
    public class PackingLine{
        public String CommodityCode{get; set;}
        public String Height{get; set;}
        public String Width{get; set;}
        public String Length{get; set;}
        public String LengthUnit{get; set;}
        public String Volume{get; set;}
        public String VolumeUnit{get; set;}
        public String Weight{get; set;}
        public String WeightUnit{get; set;}
        public String PackQty{get; set;}
        public String PackTypeCode{get; set;}
        public String PackTypeDesc{get; set;}
    }
}