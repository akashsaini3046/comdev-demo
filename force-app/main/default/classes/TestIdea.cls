@isTest
public with sharing class TestIdea {
    public static Idea createIdea(Id communityId){
        Map<string, Schema.SObjectField> fdMap = Schema.getGlobalDescribe().get('idea').getDescribe().fields.getMap();
        Idea ideaRec = new Idea();
        ideaRec.CommunityId = communityId;
    	ideaRec.Title = 'Test 1';
        ideaRec.Body = 'Test idea description';
        ideaRec.Status = 'Open';
        ideaRec.Critical_Requirement__c = false;
        ideaRec.Benefits__c = fdMap.get('benefits__c').getDescribe().getPicklistValues()[0].getValue();
        insert ideaRec;
        return ideaRec;
    }
    
    public static User getUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser.crowley7991@testorg.com');
		return u;
    }
    
    public static testMethod void saveIdeaRecord(){
        List<Community> zones = [Select Id, Name From Community Limit 1];
        if(!zones.isEmpty()){
            Map<string, Schema.SObjectField> fdMap = Schema.getGlobalDescribe().get('idea').getDescribe().fields.getMap();
            string status = fdMap.get('status').getDescribe().getPicklistValues()[0].getValue();
            string benefits = fdMap.get('benefits__c').getDescribe().getPicklistValues()[0].getValue();
            string categories = fdMap.get('categories').getDescribe().getPicklistValues()[0].getValue();
            
            Idea ideaRec = getIdea('Test',null);
            try{
                ideaRec = IdeaDetailLightningController.saveIdeaRecord(ideaRec);
            }catch(Exception ex){
                System.assert(ex.getMessage().contains('Please fill all required fields'));
            }
            ideaRec.CommunityId = zones[0].Id;
            ideaRec = IdeaDetailLightningController.saveIdeaRecord(ideaRec);
            System.assert(ideaRec.Id!=null);
            
            System.runAs(getUser()){
            	IdeaDetailLightningController.upvoteIdea(ideaRec.Id);      
            }
            
            IdeaDetailLightningController.getIdeaDescribe();
            IdeaDetailLightningController.getIdeaFieldDescribe();
            IdeaDetailLightningController.findSimilarIdeas(zones[0].Id, 'Test');
            IdeaDetailLightningController.getIdeasList(zones[0].Id, status, 'Test', categories);
            ideaRec.Status = 'Implemented and Closed'; 
            ideaRec = IdeaDetailLightningController.saveIdeaRecord(ideaRec);
            Idea ideaRecResult = IdeaDetailLightningController.getIdeaDetails(ideaRec.Id);
            System.assert(ideaRec.Status == ideaRecResult.Status);
            
            System.assert(IdeaDetailLightningController.getUser().Id == UserInfo.getUserId());
            
            IdeaDetailLightningController.getZonesList();
            IdeaDetailLightningController.getBenefits();
            IdeaDetailLightningController.getIdeaStatuses();
            IdeaDetailLightningController.getIdeaCategories();
            IdeaDetailLightningController.deleteIdeaRecord(ideaRec.Id);
        }
    }
    
    public static Idea getIdea(string title, string communityId){
        Map<string, Schema.SObjectField> fdMap = Schema.getGlobalDescribe().get('idea').getDescribe().fields.getMap();
        string status = fdMap.get('status').getDescribe().getPicklistValues()[0].getValue();
        string benefits = fdMap.get('benefits__c').getDescribe().getPicklistValues()[0].getValue();
        string categories = fdMap.get('categories').getDescribe().getPicklistValues()[0].getValue();
        
        Idea ideaRec = new Idea();
        ideaRec.Title = title;
        ideaRec.Body = 'Test idea description';
        ideaRec.Status = status;
        ideaRec.Critical_Requirement__c = false;
        ideaRec.Benefits__c = benefits;
        ideaRec.Categories = categories;
        ideaRec.CommunityId = communityId;
        return ideaRec;
    }
    
    public static testMethod void testIdeaComment(){
        List<Community> zones = [Select Id, Name From Community Limit 1];
        if(!zones.isEmpty()){
            Idea ideaRec = getIdea('Test',zones[0].Id);
            
            ideaRec = IdeaDetailLightningController.saveIdeaRecord(ideaRec);
            System.assert(ideaRec.Id!=null); 
            
            System.runAs(getUser()){
            	IdeaDetailLightningController.downvoteIdea(ideaRec.Id);      
            }          
            
            IdeaComment comm = new IdeaComment();
            comm.IdeaId = ideaRec.Id;
            comm.CommentBody = 'Test Comment 1';
            IdeaComment commResult = IdeaDetailLightningController.addComment(JSON.serialize(comm));
            
            commResult = IdeaDetailLightningController.getIdeaCommentRecord(commResult.Id);
            commResult.CommentBody = 'Test Comment 2';
            commResult = IdeaDetailLightningController.saveIdeaCommentRecord(commResult);
            commResult = IdeaDetailLightningController.getIdeaCommentRecord(commResult.Id);
            System.assert(commResult.CommentBody == 'Test Comment 2');
            
            IdeaDetailLightningController.likeComment(commResult.Id);
            IdeaDetailLightningController.getIdeaComments(ideaRec.Id);
            IdeaDetailLightningController.unlikeComment(commResult.Id);
            IdeaDetailLightningController.deleteComment(commResult.Id);
            
            List<IdeaComment> comms = [Select Id From IdeaComment Where Id=:commResult.Id];
        	System.assert(comms.isEmpty());
        }
    }
    
    public static testMethod void testFileUpload(){
        List<Community> zones = [Select Id, Name From Community Limit 1];
        if(!zones.isEmpty()){
            Idea ideaRec = getIdea('Test',zones[0].Id);
            insert ideaRec;
            String s = EncodingUtil.base64Encode(Blob.valueOf('Sample file for testing file upload on idea'));
            FileUploadController.saveChunk(ideaRec.Id, 'Test File 1', EncodingUtil.base64Encode(Blob.valueOf('Test 1')), 'text/plain', '');
            FileUploadController.saveChunk(ideaRec.Id, 'Test File 1', EncodingUtil.base64Encode(Blob.valueOf('Test 1')), 'text/plain', ideaRec.Id);
        }   
    }
}