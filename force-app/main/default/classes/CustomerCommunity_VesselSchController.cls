public class CustomerCommunity_VesselSchController {
    @AuraEnabled
    public static List < String > getPortOfLoadingAndDischarge() {
        List < Port_Of_Loading_And_Discharge__mdt > listOfPorts = new List < Port_Of_Loading_And_Discharge__mdt > ();
        listOfPorts = [SELECT Id, Label, DeveloperName, Code__c, Country__c
                            FROM Port_Of_Loading_And_Discharge__mdt ORDER By Label ASC];
        List < String > portOptions = new List < String > ();
        Schema.DescribeFieldResult fieldResult = Find_Route__c.Origin_Port__c.getDescribe();
        List < Schema.PicklistEntry > pList = fieldResult.getPicklistValues();
        for (Port_Of_Loading_And_Discharge__mdt p: listOfPorts) {
            portOptions.add(p.Label);
        }
        return portOptions;
    }
    @AuraEnabled
    public static List < CustomerCommunity_VesselScheduleInfo > getVesselSchedulesList(String polcode, String podcode, String departure, String arrival, String range, String includeTransshipment) {
		try{
            System.debug('polcode--->'+polcode+'podcode--->'+podcode);
			String ServiceName = CustomerCommunity_Constants.VESSELSCHEDULES;
			List < CustomerCommunity_VesselScheduleInfo > listVesselSchedules = new List < CustomerCommunity_VesselScheduleInfo > ();
			CustomerCommunity_VesselScheduleInfo ins = new CustomerCommunity_VesselScheduleInfo();
			Map < String, String > mapOfPortAndPortCode = new Map < String, String > ();
			for (Port_Of_Loading_And_Discharge__mdt port: [Select id, Code__c, Country__c, MasterLabel from Port_Of_Loading_And_Discharge__mdt]) {
				mapOfPortAndPortCode.put(port.MasterLabel, port.Code__c);
			}
			Integer DAYS_INT = Integer.valueOf(range.substring(0, 1)) * 7;
			Boolean onlyWebPorts = false;
			String dateFormat = CustomerCommunity_Constants.DATE_FORMAT_DD_MM;
			Integer COUNT = 15;
			String POLCOD = mapOfPortAndPortCode.get(polcode);
			String PODCOD = mapOfPortAndPortCode.get(podcode);
			String arrivalDate;
			String departureDate;
			if (departure != null && !String.isBlank(departure)) {
				departureDate = String.valueOf(date.valueOf(departure).format());
			} else {
				departureDate = CustomerCommunity_Constants.EMPTY_STRING;
			}
			if (arrival != null && !String.isBlank(arrival)) {
				arrivalDate = String.valueOf(date.valueOf(arrival).format());
			} else {
				arrivalDate = CustomerCommunity_Constants.EMPTY_STRING;
			}
			JSONGenerator gen = JSON.createGenerator(true);    
			gen.writeStartObject();      
			gen.writeStringField('polcode', POLCOD);
			gen.writeStringField('podcode',PODCOD);
			gen.writeStringField('departure',departureDate);
			gen.writeStringField('arrival',arrivalDate);
			gen.writeNumberField('days',DAYS_INT);
			gen.writeNumberField('count',COUNT);
			gen.writeBooleanField('onlyWebPorts',onlyWebPorts);
			gen.writeStringField('dateFormat',dateFormat);
			gen.writeBooleanField('includeTerminals',onlyWebPorts);
			gen.writeBooleanField('useETBandETD',onlyWebPorts);
			gen.writeStringField('service','');
			gen.writeStringField('includeTransshipment',includeTransshipment);
			gen.writeEndObject();
            
			String requestbody = gen.getAsString();
            System.debug('requestbody-->'+requestbody);
			String response = CustomerCommunity_APICallouts.getAPIResponse(ServiceName, requestbody, Null);
            System.debug('response-->'+response);
			String decodedJson;
			String jsonPretty;
			if (response != Null && response != '') {
				String encoded = EncodingUtil.urlEncode(response, CustomerCommunity_Constants.URL_ENCODING_UTF_8);
				String responsePretty = encoded.replaceAll('%5Cr%5Cn', '').replaceAll('%5C', '').replaceAll('%22%28', CustomerCommunity_Constants.EMPTY_STRING).replaceAll('%28', CustomerCommunity_Constants.EMPTY_STRING).replaceAll('%29',CustomerCommunity_Constants.EMPTY_STRING);
                decodedJson = EncodingUtil.urlDecode(responsePretty, CustomerCommunity_Constants.URL_ENCODING_UTF_8).removeEnd('"}') + '}';
                decodedJson = '[{' + decodedJson.substring(decodedJson.indexOf(CustomerCommunity_Constants.VOYAGEMACHINENUMBER), decodedJson.length() - 2);
            }
			listVesselSchedules = (List < CustomerCommunity_VesselScheduleInfo > ) JSON.deserialize(decodedJson, List < CustomerCommunity_VesselScheduleInfo > .class);
			for (CustomerCommunity_VesselScheduleInfo comInfo: listVesselSchedules) {
				if (comInfo.Duration == CustomerCommunity_Constants.ZERO_STRING) {
					comInfo.Duration = CustomerCommunity_Constants.ZERO_HOURS_STRING;
				} else if (comInfo.Duration == CustomerCommunity_Constants.ONE_STRING) {
					comInfo.Duration = comInfo.Duration + CustomerCommunity_Constants.DAY;
				} else if (comInfo.Duration != CustomerCommunity_Constants.ONE_STRING) {
					comInfo.Duration = comInfo.Duration + CustomerCommunity_Constants.DAYS;
				}
				if (departure != null && !String.isBlank(departure)) {
					DateTime DateTimeFrom = (DateTime) Date.valueOf(departure).AddDays(1);
					DateTime myDateTimeTo = (DateTime) Date.valueOf(departure).AddDays(DAYS_INT + 1);
					comInfo.DatesBetween = DateTimeFrom.format(CustomerCommunity_Constants.DATE_FORMAT_DAY_WEEK) + CustomerCommunity_Constants.AND_STRING + 
						myDateTimeTo.format(CustomerCommunity_Constants.DATE_FORMAT_DAY_WEEK);
				} else if (arrival != null && !String.isBlank(arrival)) {
					DateTime DateTimeFrom = (DateTime) Date.valueOf(arrival).AddDays(1);
					DateTime myDateTimeTo = (DateTime) Date.valueOf(arrival).AddDays(DAYS_INT + 1);
					comInfo.DatesBetween = DateTimeFrom.format(CustomerCommunity_Constants.DATE_FORMAT_DAY_WEEK) + CustomerCommunity_Constants.AND_STRING + 
						myDateTimeTo.format(CustomerCommunity_Constants.DATE_FORMAT_DAY_WEEK);
				}
			}
			return listVesselSchedules;
		}
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
}