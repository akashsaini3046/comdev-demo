public class CustomerCommunity_SoftshipRating {
    public static CustomerCommunity_RatingResponseBody getSoftshipRatings(CustomerCommunity_RatingRequestBody requestBody){
        CustomerCommunity_RatingResponseBody ratingResponse = new CustomerCommunity_RatingResponseBody();
        
        // Make HTTP callout to create Booking
        String httpRequestBody = JSON.serialize(requestBody);
        System.debug('@@@ requestbeforevalll - ' + httpRequestBody);
        httpRequestBody = httpRequestBody.replaceAll('_PostFix', '');
        System.debug('@@@ requestafterval - ' + httpRequestBody);
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint('https://veloz-dev.crowley.com/Veloz.Commercial/api/booking/BookingExternal/Execute');
        request.setClientCertificateName('le_190f2dba_9644_4265_816d_7e15c76dccc6');
        request.setTimeout(120000);
        request.setMethod('GET');
        request.setHeader('content-type', 'application/json');
        request.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
        request.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
        System.debug('@@@ request - ' + httpRequestBody);
        request.setBody(httpRequestBody);
        //request.setCompressed(true);
        response = http.send(request);
        System.debug('@@@ Response - ' + response);
        System.debug('@@@ Response Body - ' + response.getBody());

        if(response.getStatus() == 'OK'){
            String responseBody = response.getBody().replaceAll('Currency', 'CurrencyVal');
            ratingResponse = (CustomerCommunity_RatingResponseBody) JSON.deserialize(responseBody, CustomerCommunity_RatingResponseBody.class);
            System.debug('@@@ - ' + ratingResponse);
            return ratingResponse;
        }
        else{
            System.debug('System Ran into error. Please try again later. ' + response);
            return Null;
        }
    }
    
    public static CustomerCommunity_RatingResponseBody getSoftshipRatings(String requestBody){
        // public static List<CustomerCommunity_RatingResponseBody> getSoftshipRatings(String requestBody){
        
        CustomerCommunity_RatingResponseBody ratingResponse = new CustomerCommunity_RatingResponseBody();
          //   List<CustomerCommunity_RatingResponseBody> ratingResponseTest = new List<CustomerCommunity_RatingResponseBody>();
        
       
        
        // Make HTTP callout to create Booking
        //String httpRequestBody = JSON.serialize(requestBody);
        String httpRequestBody = requestBody;
        httpRequestBody = httpRequestBody.replaceAll('_PostFix', '');
        System.debug('@@@ requestafter - ' + httpRequestBody);
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint('https://veloz-dev.crowley.com/Veloz.Commercial/api/booking/BookingExternal/Execute');
        request.setClientCertificateName('le_190f2dba_9644_4265_816d_7e15c76dccc6');
        request.setTimeout(120000);
        request.setMethod('GET');
        request.setHeader('content-type', 'application/json');
        request.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
        request.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
        System.debug('@@@ request - ' + httpRequestBody);
        request.setBody(httpRequestBody);
        //request.setCompressed(true);
        response = http.send(request);
      
        System.debug('@@@ Response - ' + response);
        System.debug('@@@ Response Body - ' + response.getBody());

        if(response.getStatus() == 'OK'){
            String responseBody = response.getBody().replaceAll('Currency', 'CurrencyVal');
            System.debug('Before deserialize');
            System.debug('@@@ - ' + response.getBody());
            ratingResponse.resultOld = (List<CustomerCommunity_RatingResponseBody.resultOld>) JSON.deserialize(responseBody, List<CustomerCommunity_RatingResponseBody.resultOld>.class);
            //ratingResponseTest = (List<CustomerCommunity_RatingResponseBody>) JSON.deserialize(responseBody,List<CustomerCommunity_RatingResponseBody>.class);
             System.debug('Before deserialize');
            System.debug('@@@ - ' + ratingResponse);
           
            return ratingResponse;
           
        }
        else{
            System.debug('System Ran into error. Please try again later. ' + response);
            return Null;
        }
    }
}