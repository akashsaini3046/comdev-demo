/*
 * 11-06-2020 – Richa Sharma – Created this class for fetching routes information using SailingsScheduleAPI.
 *
 */

/*
 * @company		: Nagarro Inc.
 * @date		: 11-06-2020
 * @author		: Nagarro
 * @description	: To fetch routes information using SailingsScheduleAPI
 * @history		: Version 1.0
 * @test class	: CustomerCommunity_FindARouteCtrl_Test
 */
public class CustomerCommunity_FindARouteController {
    
   /*
     * @purpose		: Method to get the list of origin Port records 
     * @return 		: List<String> of Origin Ports
     */
    @AuraEnabled
    public static List<String> getOriginPorts(){
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_FindARouteController:getOriginPorts() with parameters'); 
        List<String> originOptions = new List<String>();
        try {
            Schema.DescribeFieldResult fieldResult = Find_Route__c.Origin_Port__c.getDescribe();
            List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: pList) {
                originOptions.add(p.getLabel());
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        System.debug(LoggingLevel.INFO,'Exiting to CustomerCommunity_FindARouteController:getOriginPorts() returning--'+originOptions);
        return originOptions;
    } 
    
    @AuraEnabled
    public static Boolean handleTest11(){
        Boolean isSuccess = false;
        Lead lead = new Lead();
        try {
            lead.firstname = 'Test First';
            lead.lastname = 'Test Last';
            lead.Company = 'ABC';
            insert lead;
            isSuccess = true;
        }
        catch(Exception ex){
            return isSuccess;
        }
        return isSuccess;
    }
    
    @AuraEnabled
    public static Lead handleTest22(){
        Lead lead = new Lead();
        try {
            lead = [SELECT Id, firstname, lastname from Lead WHERE FirstName = 'Test First' LIMIT 1];
        }
        catch(Exception ex){
            return null;
        }
        return lead;
    }
    
    /*
     * @purpose		: Method to get the list of FCL Ports from the custom metadata 
     * @return 		: List<String> of FCL Ports
     */
    
    @AuraEnabled
    public static List<FCL_Ports__mdt> getFCLPorts(){   
        try{
            String fields = 'Id, MasterLabel, Abbreviation__c' ;
            List<FCL_Ports__mdt> list_FCLOptions = CustomMetaDataDAO.getCustomMetaData(fields,'FCL_Ports__mdt','MasterLabel');
            return list_FCLOptions; 
        }
        catch(Exception e){
           return null;
        }
    }
  
    
    /*
     * @purpose		: Method to get the list of LCL Ports from the custom metadata 
     * @return 		: List<String> of LCL Ports
     */
    @AuraEnabled
    public static List<LCL_Ports__mdt> getLCLPorts(){   
        try{
            String fields = 'Id, MasterLabel, Abbreviation__c' ;
            List<LCL_Ports__mdt> list_LCLOptions = CustomMetaDataDAO.getCustomMetaData(fields,'LCL_Ports__mdt','MasterLabel');
            return list_LCLOptions; 
        }
        catch(Exception e){
           return null;
        }       
    }
    
      
     /*
     * @purpose		: Method to get the map of FCL ports name and their abbreviation from the custom metadata 
     * @return 		: Map<String,String>, of FCL Ports and their abbreviation
     */
    @AuraEnabled
    public static Map<String,String> getFCLPortsMap(){   
        try{
            Map<String,String> portNameVsAbbreviationMap = new Map<String,String>();
            String fields = 'Id, MasterLabel, Abbreviation__c' ;
            List<FCL_Ports__mdt> list_FCLOptions = CustomMetaDataDAO.getCustomMetaData(fields,'FCL_Ports__mdt','MasterLabel');
            for(FCL_Ports__mdt fclPort : list_FCLOptions)
                portNameVsAbbreviationMap.put(fclPort.MasterLabel,fclPort.Abbreviation__c);
            return portNameVsAbbreviationMap; 
        }
        catch(Exception e){
           return null;
        }
    }
    
    /*
     * @purpose		: Method to get the map of LCL ports name and their abbreviation from the custom metadata 
     * @return 		: Map<String,String>, of LCL Ports and their abbreviation
     */
    @AuraEnabled
    public static Map<String,String> getLCLPortsMap(){   
        try{
            Map<String,String> portNameVsAbbreviationMap = new Map<String,String>();
            String fields = 'Id, MasterLabel, Abbreviation__c' ;
            List<LCL_Ports__mdt> list_LCLOptions = CustomMetaDataDAO.getCustomMetaData(fields,'LCL_Ports__mdt','MasterLabel');
            for(LCL_Ports__mdt lclPort : list_LCLOptions)
                portNameVsAbbreviationMap.put(lclPort.MasterLabel,lclPort.Abbreviation__c);
            return portNameVsAbbreviationMap; 
        }
        catch(Exception e){
           return null;
        }
    }
    
    /*
     * @purpose		: Method to get the list of Sailing Weeks from the custom metadata 
     * @return 		: List<String> of Sailing Weeks
     */
    @AuraEnabled
    public static List<Sailing_Weeks__mdt> getSailingWeeks(){
        try{
            String fields = 'Id, MasterLabel' ;
            List<Sailing_Weeks__mdt> list_SailingWeeks = CustomMetaDataDAO.getCustomMetaData(fields,'Sailing_Weeks__mdt','MasterLabel');
            return list_SailingWeeks; 
        }
        catch(Exception e){
           return null;
        }
    }
    
    /*
     * @purpose		: Method to get the list of destination Port records 
     * @return 		: List<String> of Destination Ports
     */
    @AuraEnabled
    public static List<String> getDestinationPorts(){
        List<String> destinationOptions = new List<String>();
        try{
            Schema.DescribeFieldResult fieldResult = Find_Route__c.Destination_Port__c.getDescribe();
            List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: pList) {
                destinationOptions.add(p.getLabel());
            }
            return destinationOptions;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }  
    
    @AuraEnabled
    public static List<CustomerCommunity_RouteInformation> getRoutes(String originPort, String destinationPort, String shipmentType, String sailingWeeks){
        System.debug('Entered getRoutes() in CustomerCommunity_FindARouteController apex ');
        System.debug('Parameter values : '+originPort+' '+destinationPort+' '+shipmentType+' '+sailingWeeks);
        String dateText=date.today().format();
        String ServiceName = CustomerCommunity_Constants.FAR_SERVICE_NAME;
        try {
            Schema.DescribeFieldResult OriginFieldResult = Find_Route__c.Origin_Port__c.getDescribe();
            List<Schema.PicklistEntry> originList = OriginFieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: originList) {
                if(p.getLabel() == originPort) {
                    originPort = p.getValue();
                    break;
                }
            }
            Schema.DescribeFieldResult destinationFieldResult = Find_Route__c.Destination_Port__c.getDescribe();
            List<Schema.PicklistEntry> destinationList = destinationFieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: destinationList) {
                if(p.getLabel() == destinationPort) {
                    destinationPort = p.getValue();
                    break;
                }
            }
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();      
            gen.writeStringField('originPort', originPort);
            gen.writeStringField('destPort',destinationPort);
            gen.writeDateField('cargoReadyDate',System.Today());
            gen.writeStringField('FclLclData',shipmentType);
            gen.writeStringField('displayRoutes',sailingWeeks);
            gen.writeEndObject();    
            String requestbody = gen.getAsString();
            System.debug('request body : '+requestbody);
            String response = CustomerCommunity_APICallouts.getAPIResponse(ServiceName, requestbody, Null);
            if(response != Null) {
                System.debug('Response size : --> '+response.length());
                List<CustomerCommunity_RouteInformation> routeInformationList = (List<CustomerCommunity_RouteInformation>)JSON.deserialize(response, List<CustomerCommunity_RouteInformation>.class);
                Integer count = 1;
                if(routeInformationList.size()>0) {
                    for(CustomerCommunity_RouteInformation ri : routeInformationList){
                        ri.stopCount = String.valueOf(ri.details.size()-1);
                        ri.routeCount = String.valueOf(routeInformationList.size());
                        ri.transitTime = ri.transitTime.substring(0,ri.transitTime.indexOf(CustomerCommunity_Constants.SPACE_STRING));
                        ri.sequenceCount = String.valueOf(count++);
                        for(CustomerCommunity_DetailInfo di : ri.details) {
                            di.transitTime = di.transitTime.substring(0,di.transitTime.indexOf(CustomerCommunity_Constants.SPACE_STRING));
                            System.debug('di.transitTime->>>'+ di.transitTime);
                            Datetime startDt = Datetime.newInstance(Integer.valueOf(di.startDate.substring(6, di.startDate.length())), Integer.valueOf(di.startDate.substring(0, 1)), Integer.valueOf(di.startDate.substring(3, 4)));
                            System.debug('startDt -->' + startDt);
                            di.startDay = startDt.format(CustomerCommunity_Constants.DATE_FORMAT);
                            Datetime arrivalDt = Datetime.newInstance(Integer.valueOf(di.arrivalDate.substring(6, di.arrivalDate.length())), Integer.valueOf(di.arrivalDate.substring(0, 1)), Integer.valueOf(di.arrivalDate.substring(3, 4)));
                            di.arrivalDay = arrivalDt.format(CustomerCommunity_Constants.DATE_FORMAT);
                        }
                    }
                    System.debug('Length  --->  '+routeInformationList.size());
                    System.debug('routeInformationList --> '+routeInformationList);
                    return routeInformationList;
                    
                }
            }
            System.debug('response is --> '+response);
            return Null;
        }
        catch(Exception ex){
            LogFactory.error('CustomerCommunity_FindARouteController', 'getRoutes', 'Error in Find a Route', 'Line Number : ' + ex.getLineNumber() +' Message : '+ ex.getMessage());
            LogFactory.saveLog();
            //System.debug('exception-> '+ex.getCause() + '**********'+ex.getMessage());
            //ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    } 
	
    
    /*
     * @purpose		: Method to get the route information after hitting the SailingScheduleAPI
     * @parameter   : Instance of SailingScheduleAPIWrapper
     * @return 		: List<CustomerCommunity_RouteInformation> , list of response Wrapper
     */
    @AuraEnabled
    public static List<CustomerCommunity_RouteInformation> getRoutesTest(CustomerCommunity_RouteRequest routeRequestWrapper){
        try {
            String requestbody=JSON.serialize(routeRequestWrapper, true);
            String response = APICalloutUtility.callAPIResponseService('FindARoutePOST', requestbody, Null, null);
            LogFactory.info('CustomerCommunity_FindARouteController', 'getRoutesTest', 'Response in Find a Route', response);
            LogFactory.saveLog();
            List<CustomerCommunity_RouteInformation> routeInformationList = null;
            if(response != null) {
                routeInformationList = (List<CustomerCommunity_RouteInformation>)JSON.deserialize(response, List<CustomerCommunity_RouteInformation>.class);
                Integer count = 1;
                for(CustomerCommunity_RouteInformation routeInfo : routeInformationList){
                    routeInfo.stopCount = String.valueOf(routeInfo.details.size()-1);
                    routeInfo.routeCount = String.valueOf(routeInformationList.size());
                    routeInfo.transitTime = routeInfo.transitTime.substring(0,routeInfo.transitTime.indexOf(CustomerCommunity_Constants.SPACE_STRING));
                    routeInfo.sequenceCount = String.valueOf(count++);
                    for(CustomerCommunity_DetailInfo detailInfo : routeInfo.details) {
                        detailInfo.transitTime = detailInfo.transitTime.substring(0,detailInfo.transitTime.indexOf(CustomerCommunity_Constants.SPACE_STRING));
                        Datetime startDt = Datetime.newInstance(Integer.valueOf(detailInfo.startDate.substring(6, detailInfo.startDate.length())), Integer.valueOf(detailInfo.startDate.substring(0, 2)), Integer.valueOf(detailInfo.startDate.substring(3, 5)));
                        detailInfo.startDay = startDt.format(CustomerCommunity_Constants.DATE_FORMAT);
                        Datetime arrivalDt = Datetime.newInstance(Integer.valueOf(detailInfo.arrivalDate.substring(6, detailInfo.arrivalDate.length())), Integer.valueOf(detailInfo.arrivalDate.substring(0, 2)), Integer.valueOf(detailInfo.arrivalDate.substring(3, 5)));
                        detailInfo.arrivalDay = arrivalDt.format(CustomerCommunity_Constants.DATE_FORMAT);
                    }
                }
            }            
            return routeInformationList;
        }
        catch(Exception ex){
            LogFactory.error('CustomerCommunity_FindARouteController', 'getRoutesTest', 'Error in Find a Route', ex.getMessage());
            LogFactory.saveLog();
            return Null;
        }
    }  
}