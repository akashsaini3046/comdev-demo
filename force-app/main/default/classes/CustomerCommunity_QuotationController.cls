public without sharing class  CustomerCommunity_QuotationController {
    @AuraEnabled
    public static List<Destination_ports__mdt> getDestinationPorts(){   
        List<Destination_ports__mdt> list_MenuOptions = new List<Destination_ports__mdt>();
            list_MenuOptions = [SELECT Id, MasterLabel
                      
					  FROM Destination_ports__mdt ];
            
            return list_MenuOptions;
    }
    
	@AuraEnabled
    public static CustomerCommunity_QuoteResponseInfo getQuotationRates(String destinationCountry ,String weight, String volume,String originCountry, String originPostalCode,String currentUnit) {
        try{	
			System.debug('originPostalCode-->'+originPostalCode+'destinationCountry-->'+destinationCountry);
			Ports_Information__c portInformation=[Select id,AssignedCFS_City__c,Zip_Code__c,Assigned_CFS_Zipcode__c from Ports_Information__c where Zip_Code__c=:originPostalCode];
			System.debug('portInformation-->'+portInformation);
			Quotation_Rates__c quoteRate=[Select id,Origin_CFS__c,Rate_per_100_lbs__c,Rate_per_Cu_Ft__c,Transfer_CFS__c,Destination_CFS__c ,Additionals__c
										  FROM Quotation_Rates__c
										  WHERE Origin_CFS__c=:portInformation.AssignedCFS_City__c and Destination_CFS__c=:destinationCountry];
			System.debug('quoteRate-->'+quoteRate+'currentUnit--->'+currentUnit);	
			Decimal oceanRate=calculateOceanRate(quoteRate,weight,volume,currentUnit);
			oceanRate=oceanRate.setScale(2);
			System.debug('oceanRate-->'+oceanRate);
			Double containerWeight;
			if(weight!=null && !String.isBlank(weight)){
				containerWeight=Double.valueOf(weight);
			}
			else{
				containerWeight=Double.valueOf(volume);
			}
			System.debug('containerWeight--->'+containerWeight);
			/*String requestBody='{"Body":{"LTLRateShipmentSimple": {"LTLRateShipmentSimpleRequest": {"destinationaddress1":"2751 Patterson St","destinationaddress2":"","destinationcity":"Greensboro","destinationstateProvince":"NC","destinationCountry": "USA","destinationPostalCode": "27407","details": {"LTLRequestDetail": {"nmfcClass": "70","weight": "'+containerWeight+'"}},"originCountry": "USA","originPostalCode": "30080","shipmentDateCCYYMMDD": "20180924","shipmentID": "Test","tariffName": "LITECZ02"}}}}';*/
			
			
			CustomerCommunity_QuoteRequest.LTLRequestDetail ltlRequestDetail=new CustomerCommunity_QuoteRequest.LTLRequestDetail();
			ltlRequestDetail.weight=String.valueOf(containerWeight);
			ltlRequestDetail.nmfcClass='70';
			
			CustomerCommunity_QuoteRequest.details  details=new CustomerCommunity_QuoteRequest.details();
			details.LTLRequestDetail=ltlRequestDetail;
			
			CustomerCommunity_QuoteRequest.LTLRateShipmentSimpleRequest simpleRequest=new CustomerCommunity_QuoteRequest.LTLRateShipmentSimpleRequest();
			simpleRequest.details=details;
			simpleRequest.originCountry=originCountry;
			simpleRequest.originPostalCode=originPostalCode;
			simpleRequest.destinationPostalCode=portInformation.Assigned_CFS_Zipcode__c;
			simpleRequest.shipmentDateCCYYMMDD='20180924';
			simpleRequest.destinationCountry=originCountry;
			simpleRequest.destinationstateProvince='NC';
			simpleRequest.tariffName='LITECZ02';
			simpleRequest.destinationcity='Greensboro';
			simpleRequest.destinationaddress2='';
			simpleRequest.destinationaddress1='2751 Patterson St';
			
			CustomerCommunity_QuoteRequest.LTLRateShipmentSimple ltlRateShipmentSimple=new CustomerCommunity_QuoteRequest.LTLRateShipmentSimple();
			ltlRateShipmentSimple.LTLRateShipmentSimpleRequest=simpleRequest;
			
			CustomerCommunity_QuoteRequest.Body body=new CustomerCommunity_QuoteRequest.Body();
			body.LTLRateShipmentSimple=ltlRateShipmentSimple;
			
			CustomerCommunity_QuoteRequest quoteRequest =new CustomerCommunity_QuoteRequest();
			quoteRequest.Body=body;
			
			String requestBody = JSON.serialize(quoteRequest);
			System.debug('requestBody-->'+requestBody);
			
            HttpRequest request = new HttpRequest();
			HttpResponse response = new HttpResponse();
			Http http = new Http();
			request.setEndpoint(' https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/quote');
			request.setMethod('POST');
			request.setHeader('content-type', 'application/json');
			request.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
			request.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
			request.setBody(requestBody);
			response = http.send(request);
			System.debug('responseBody-->'+response.getBody());
			CustomerCommunity_QuoteResponseInfo quoteInfo = (CustomerCommunity_QuoteResponseInfo ) JSON.deserialize(response.getBody(), CustomerCommunity_QuoteResponseInfo.class);
			System.debug('rate111-->'+quoteInfo.Body.LTLRateShipmentSimpleResponse.details.LTLResponseDetail.rate);
			quoteInfo.Body.OceanRate=String.valueOf(oceanRate);
       
			quoteInfo.Body.TotalRate=String.valueOf(Double.valueOf(quoteInfo.Body.LTLRateShipmentSimpleResponse.details.LTLResponseDetail.rate)+oceanRate);
            System.debug('quoteInfo-->'+quoteInfo);
            return quoteInfo;
		}
        catch(Exception ex){
            System.debug('catch-->'+ex);
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
	@AuraEnabled
    public static Decimal calculateOceanRate(Quotation_Rates__c quoteRate,String weight,String volume,String currentUnit) {
		System.debug('weight111-->'+weight+'volume111-->'+volume+'currentUnit--->'+currentUnit);
			Decimal oceanRate=0;
			
			if(weight!=null && !String.isBlank(weight)){
				System.debug('weight1112222-->'+weight);
				oceanRate=Decimal.valueOf(weight);
				if(currentUnit=='true'){
					System.debug('currentUnit-->'+currentUnit);
					oceanRate=Decimal.valueOf(weight)*2.20462;
					System.debug('oceanRate-->'+oceanRate);
				}
			    oceanRate=oceanRate/100*quoteRate.Rate_per_100_lbs__c+quoteRate.Additionals__c;
				System.debug('oceanRate11111-->'+oceanRate);
			}
			
			if(volume!=null && !String.isBlank(volume)){
				System.debug('volume-->'+volume);
				Decimal oceanVolume=Decimal.valueOf(volume);
				System.debug('volume44444-->'+volume);
				if(currentUnit=='true'){
					oceanVolume=oceanVolume*35.3147;
					System.debug('oceanVolume5555666-->'+oceanVolume);
				}
			    oceanVolume=oceanVolume*quoteRate.Rate_per_Cu_Ft__c+quoteRate.Additionals__c;
				System.debug('oceanVolume77777-->'+oceanVolume);
				if(oceanRate< oceanVolume){
					System.debug('oceanRate123-->'+oceanRate+'oceanVolume123-->'+oceanVolume);
					oceanRate=oceanVolume;
				}
			}
			return oceanRate;
	}
	
	
    @AuraEnabled
    public static Ports_Information__c getPorts(String zipCode){
        Ports_Information__c portInformationList=new Ports_Information__c();
      	portInformationList=[Select id,Place_Name__c,State_Code__c, Zip_Code__c from Ports_Information__c where Zip_Code__c=:zipCode limit 1];
        if(portInformationList!=null){
        	return portInformationList;    
        }
        	return null;  
    }
}