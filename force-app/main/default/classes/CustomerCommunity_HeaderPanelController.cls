public class CustomerCommunity_HeaderPanelController {
    @AuraEnabled
    public static String getUserName() {
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        Map<String, Object> mapUserDetails = new Map<String, Object>();
        try{
            userDetail = [select Id, contactId, LanguageLocaleKey from User where Id = : userId];
            if(userDetail.contactId != Null) {
                conDetail = [SELECT Id, FirstName FROM Contact WHERE Id = :userDetail.contactId];
                mapUserDetails.put(CustomerCommunity_Constants.HEADERPANELRESPONSE_USERNAME,conDetail.FirstName);
                mapUserDetails.put('UserId',userDetail.Id);
                if(userDetail.LanguageLocaleKey == CustomerCommunity_Constants.SPANISH_LANGUAGECODE)
                	mapUserDetails.put(CustomerCommunity_Constants.HEADERPANELRESPONSE_LANGUAGE,CustomerCommunity_Constants.SPANISH_LANGUAGE);
                else
                    mapUserDetails.put(CustomerCommunity_Constants.HEADERPANELRESPONSE_LANGUAGE,CustomerCommunity_Constants.ENGLISH_LANGUAGE);
                return JSON.serialize(mapUserDetails);
            }
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        } 
    }
}