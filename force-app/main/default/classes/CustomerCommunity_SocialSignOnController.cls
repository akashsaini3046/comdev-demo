public without sharing class CustomerCommunity_SocialSignOnController {
    
	@AuraEnabled
    public static List<ThirdPartyAccountLink> fetchUserSocialDetails(){
        try{
            Id userId = UserInfo.getUserId();
            List<ThirdPartyAccountLink> userSocialDetails = [SELECT Id, ssoproviderid, handle, ssoprovidername, remoteidentifier, provider
                                FROM ThirdPartyAccountLink 
                                WHERE userId = :userId];
            if(userSocialDetails != NULL && !userSocialDetails.isEmpty()){
                return userSocialDetails;
            }
            else{
                return NULL;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    
    @AuraEnabled
    public static String disconnectSocialAccount(Id socialId){
        try{
            ThirdPartyAccountLink accountRecord = [SELECT Id, ssoproviderid, handle, ssoprovidername, remoteidentifier, provider
                                FROM ThirdPartyAccountLink 
                                WHERE Id = :socialId LIMIT 1];
            String userId = UserInfo.getUserId();
            Auth.AuthToken.revokeAccess(String.valueOf(accountRecord.SsoProviderId).left(15), accountRecord.Provider, userId.left(15), String.valueOf(accountRecord.remoteidentifier));
            return 'SUCCESS';
        }catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
}