public class CustomerCommunity_LFSQuotationRequest {
		public Body Body{get;set;}
		public String Header{get;set;}
    public class PcsDims{
		public list<Item> Item{get;set;}
	}
	public class Item{
		public String Height{get;set;}
		public String Volume{get;set;}
		public String Width{get;set;}
		public String PClass{get;set;}
		public String Length{get;set;}
		public String Commodity{get;set;}
		public String Weight{get;set;}
		public String Hazmat{get;set;}
		public String DimType{get;set;}
		public list<String> UNNmbr{get;set;}
		public String Qty{get;set;}
		public list<String> NMFC{get;set;}
		public list<String> StackAmount{get;set;}
		public String Stack{get;set;}
	}
	public class GetRatesRequest{
		public String DestinationCountry{get;set;}
		public String DestinationState{get;set;}
		public String DestinationCity{get;set;}
		public PcsDims PcsDims{get;set;}
		public String DestinationZipCode{get;set;}
		public String WeightEach{get;set;}
		public String OriginCountry{get;set;}
		public String UOM{get;set;}
		public String OriginState{get;set;}
		public String OriginCity{get;set;}
		public Accessorials Accessorials{get;set;}
		public String OriginZipCode{get;set;}
		public String Carrier{get;set;}
	}
	public class Body{
		public GetRatesRequest GetRatesRequest{get;set;}
        public String CarrierPath{get;set;}
	}
	public class Accessorials{
		public list<String> Item{get;set;}
	}
}