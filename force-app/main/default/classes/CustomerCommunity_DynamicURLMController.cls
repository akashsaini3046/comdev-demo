public class CustomerCommunity_DynamicURLMController {
    @AuraEnabled
    public static List < String > getPortOfLoadingAndDischarge() {
        List < Port_Of_Loading_And_Discharge__mdt > listOfPorts = new List < Port_Of_Loading_And_Discharge__mdt > ();
        listOfPorts = [SELECT Id, Label, DeveloperName, Code__c, Country__c
                            FROM Port_Of_Loading_And_Discharge__mdt ORDER By Label ASC];
        List < String > portOptions = new List < String > ();
        Schema.DescribeFieldResult fieldResult = Find_Route__c.Origin_Port__c.getDescribe();
        List < Schema.PicklistEntry > pList = fieldResult.getPicklistValues();
        for (Port_Of_Loading_And_Discharge__mdt p: listOfPorts) {
            portOptions.add(p.Label);
        }
        return portOptions;
    }
    @AuraEnabled
    public static List < CustomerCommunity_DynamicURLResponse > getVesselSchedulesList(String polcode, String podcode, String departure, String arrival, String range, String includeTransshipment) {
        try{
            System.debug('polcode--->'+polcode+'podcode--->'+podcode);
            String ServiceName = CustomerCommunity_Constants.VESSELSCHEDULES;
            //List < CustomerCommunity_VesselScheduleInfo > listVesselSchedules = new List < CustomerCommunity_VesselScheduleInfo > ();
            CustomerCommunity_DynamicURLResponse ins = new CustomerCommunity_DynamicURLResponse();
            Map < String, String > mapOfPortAndPortCode = new Map < String, String > ();
            for (Port_Of_Loading_And_Discharge__mdt port: [Select id, Code__c, Country__c, MasterLabel from Port_Of_Loading_And_Discharge__mdt]) {
                mapOfPortAndPortCode.put(port.MasterLabel, port.Code__c);
            }
            Integer DAYS_INT = Integer.valueOf(range.substring(0, 1)) * 7;
            Boolean onlyWebPorts = false;
            String dateFormat = CustomerCommunity_Constants.DATE_FORMAT_DD_MM;
            Integer COUNT = 15;
            String POLCOD = mapOfPortAndPortCode.get(polcode);
            String PODCOD = mapOfPortAndPortCode.get(podcode);
            String arrivalDate;
            String departureDate;
            if (departure != null && !String.isBlank(departure)) {
                departureDate = String.valueOf(date.valueOf(departure).format());
            } else {
                departureDate = String.valueOf(date.valueOf(arrival).format());
            }
            if (arrival != null && !String.isBlank(arrival)) {
                arrivalDate = String.valueOf(date.valueOf(arrival).format());
            } else {
                arrivalDate = String.valueOf(date.valueOf(departure).format());
            }
            string endpoint='https://stage.api.crowley.com/v1/voyages'+'?polcode='+POLCOD+'&podcode='+PODCOD+'&arrival='+arrivalDate
                            +'&departure='+departureDate+'&days='+DAYS_INT+'&count='+COUNT+'&onlyWebPorts='+onlyWebPorts+'&dateFormat='+dateFormat
                            +'&includeTerminals=TRUE&useETBandETD=TRUE&service=SAL&includeTransshipment='+includeTransshipment;
            //String temp = 'https://stage.api.crowley.com/v1/voyages?polcode=USJAX&podcode=PRSJU&arrival=10/15/2019&departure=10/15/2019&days=14&count=15&onlyWebPorts=false&dateFormat=dd-MMM-yyyy&includeTerminals=TRUE&useETBandETD=TRUE&service=SAL&includeTransshipment=TRUE';
            System.debug('endpoint ---->'+endpoint);                
            /*Http http = new Http();
            HttpRequest ratesRequest = new HttpRequest();
            HttpResponse ratesResponse = new HttpResponse();
            ratesRequest.setEndpoint(endpoint);
            ratesRequest.setMethod('GET');
            //ratesRequest.setTimeout(120000);
            ratesRequest.setHeader('content-type', 'application/json');
            ratesRequest.setHeader('client_id', '2f065d7451c749bd8cb1ce08c45f721e');
            ratesRequest.setHeader('client_secret', 'd1BbC072682c4185956d4a461156870e');*/
            //ratesResponse = http.send(ratesRequest);
            //ratesResponse.setBody();
            system.debug('response time');
            String jresponse='[{"HeaderTexts": {"VoyageNumber": "Voyage #", "VesselCode": "Vessel", "VesselName": null,    "TypeCode": "Type","TypeDescription": null,"StatusCode": "Status","StatusDescription": null,"PortOfLoadingCode": "POL","PortOfLoadingCallId": "POL Call ID",    "PortOfLoadingDescription": null,"PortOfDischargeCode": "POD","PortOfDischargeCallId": "POD Call ID","PortOfDischargeDescription": null, "CentralPortCode": "Centralport","PortCallId": "Call ID","CentralPortDescription": null,"StartDate": "Departure","EndDate": "Arrival",    "Duration": "Duration","HasTransshipment": "Direct","ServiceName": "Service","PortCode": "Code","PortName": "Port","ArrivalDate": "Arrival Date",    "SailingDate": "Sailing Date" }, "Voyages": [{"VoyageMachineNumber": "2963","VoyageNumber": "SAL9082","VesselCode": "TAO", "VesselName": "Taino", "TypeCode": "export",      "TypeDescription": "",  "StatusCode": "Active",      "StatusDescription": "",  "CentralPortCode": "USJAX",      "CentralPortDescription": "Jacksonville, FL", "PortOfLoadingCode": "USJAX", "PortOfLoadingDescription": "Jacksonville, FL",      "PortOfDischargeCode": "PRSJU", "PortOfDischargeDescription": "San Juan, PR",      "StartDate": "15-Oct-2019",  "EndDate": "18-Oct-2019", "Duration": "2", "DurationHours": "7",  "HasTransshipment": "False", "ServiceCode": null, "ServiceName": null, "Route": null, "Ports": [ { "PortCode": "USJAX", "PortName": "Jacksonville, FL", "PortCallId": "9172", "ArrivalDate": "14-Oct-2019",  "ArrivalTime": "08:18", "SailingDate": "15-Oct-2019", "SailingTime": "18:00",  "TypeDescription": "Load","Terminals": [{"Code": "JAXT001", "Name": "Talleyrand Term, Jacksonville", "ArrivalDate": "14-Oct-2019","ArrivalTime": "08:18","DepartureDate": "15-Oct-2019", "DepartureTime": "18:00"}],"PortStatus": "PP"},{"PortCode": "PRSJU","PortName": "San Juan, PR","PortCallId": "9173","ArrivalDate": "18-Oct-2019","ArrivalTime": "05:00","SailingDate": "19-Oct-2019","SailingTime": "05:00","TypeDescription": "Load/Discharge","Terminals": [{ "Code": "SJUT001","Name": "Isla Grande Term, San Juan, PR", "ArrivalDate": "18-Oct-2019","ArrivalTime": "05:00","DepartureDate": "19-Oct-2019","DepartureTime": "05:00"}          ],"PortStatus": "PP"}      ]    },    {      "VoyageMachineNumber": "2622",      "VoyageNumber": "SAL9942",      "VesselCode": "EC",      "VesselName": "El Conquistador",      "TypeCode": "export",      "TypeDescription": "",      "StatusCode": "Active",      "StatusDescription": "",      "CentralPortCode": "USJAX",      "CentralPortDescription": "Jacksonville, FL",      "PortOfLoadingCode": "USJAX",      "PortOfLoadingDescription": "Jacksonville, FL",      "PortOfDischargeCode": "PRSJU",      "PortOfDischargeDescription": "San Juan, PR",      "StartDate": "17-Oct-2019",      "EndDate": "23-Oct-2019",      "Duration": "6",      "DurationHours": "3",      "HasTransshipment": "False",      "ServiceCode": null,      "ServiceName": null,      "Route": null,      "Ports": [        {          "PortCode": "USJAX",          "PortName": "Jacksonville, FL",          "PortCallId": "8510",          "ArrivalDate": "14-Oct-2019",          "ArrivalTime": "13:54",          "SailingDate": "16-Oct-2019",          "SailingTime": "20:30",          "TypeDescription": "Load",          "Terminals": [            {              "Code": "JAXT001",              "Name": "Talleyrand Term, Jacksonville",              "ArrivalDate": "14-Oct-2019",              "ArrivalTime": "13:54",              "DepartureDate": "16-Oct-2019",              "DepartureTime": "20:30"            }          ],          "PortStatus": "PP"        },        {          "PortCode": "PRSJU",          "PortName": "San Juan, PR",          "PortCallId": "8511",          "ArrivalDate": "23-Oct-2019",          "ArrivalTime": "07:00",          "SailingDate": "23-Oct-2019",          "SailingTime": "20:30",          "TypeDescription": "Load/Discharge",          "Terminals": [            {             "Code": "SJUT001",              "Name": "Isla Grande Term, San Juan, PR",              "ArrivalDate": "23-Oct-2019",              "ArrivalTime": "07:00",              "DepartureDate": "23-Oct-2019",              "DepartureTime": "20:30"            }          ],          "PortStatus": "PP"        }      ]    },    {      "VoyageMachineNumber": "2801",      "VoyageNumber": "SAL9083",      "VesselCode": "ECO",      "VesselName": "El Coqui",      "TypeCode": "export",      "TypeDescription": "",      "StatusCode": "Active",      "StatusDescription": "",      "CentralPortCode": "USJAX",      "CentralPortDescription": "Jacksonville, FL",      "PortOfLoadingCode": "USJAX",      "PortOfLoadingDescription": "Jacksonville, FL",      "PortOfDischargeCode": "PRSJU",      "PortOfDischargeDescription": "San Juan, PR",      "StartDate": "18-Oct-2019",      "EndDate": "21-Oct-2019",      "Duration": "2",      "DurationHours": "7",      "HasTransshipment": "False",      "ServiceCode": null,      "ServiceName": null,      "Route": null,      "Ports": [        {          "PortCode": "USJAX",          "PortName": "Jacksonville, FL",          "PortCallId": "8821",          "ArrivalDate": "17-Oct-2019",          "ArrivalTime": "14:00",          "SailingDate": "18-Oct-2019",          "SailingTime": "18:00",          "TypeDescription": "Load",          "Terminals": [            {              "Code": "JAXT000",              "Name": "JPA Term, Jacksonville, FL",              "ArrivalDate": "17-Oct-2019",              "ArrivalTime": "14:00",              "DepartureDate": "18-Oct-2019",              "DepartureTime": "18:00"            }          ],          "PortStatus": "PP"        },{"PortCode": "PRSJU","PortName": "San Juan, PR","PortCallId": "8822", "ArrivalDate": "21-Oct-2019","ArrivalTime": "05:00","SailingDate": "22-Oct-2019","SailingTime": "05:00","TypeDescription": "Load/Discharge", "Terminals": [{ "Code": "SJUT001","Name": "Isla Grande Term, San Juan, PR",              "ArrivalDate": "21-Oct-2019",              "ArrivalTime": "05:00",              "DepartureDate": "22-Oct-2019",              "DepartureTime": "05:00"            } ], "PortStatus": "PP"        }      ]    },    {      "VoyageMachineNumber": "2964",      "VoyageNumber": "SAL9084",      "VesselCode": "TAO",      "VesselName": "Taino",      "TypeCode": "export",      "TypeDescription": "",      "StatusCode": "Active",      "StatusDescription": "",      "CentralPortCode": "USJAX",      "CentralPortDescription": "Jacksonville, FL",      "PortOfLoadingCode": "USJAX",      "PortOfLoadingDescription": "Jacksonville, FL",      "PortOfDischargeCode": "PRSJU",      "PortOfDischargeDescription": "San Juan, PR",      "StartDate": "22-Oct-2019",      "EndDate": "25-Oct-2019", "Duration": "2","DurationHours": "7", "HasTransshipment": "False","ServiceCode": null,"ServiceName": null, "Route": null,      "Ports": [        {          "PortCode": "USJAX",          "PortName": "Jacksonville, FL",         "PortCallId": "9175",          "ArrivalDate": "21-Oct-2019",          "ArrivalTime": "14:00",          "SailingDate": "22-Oct-2019",          "SailingTime": "18:00",          "TypeDescription": "Load",          "Terminals": [            {              "Code": "JAXT001",              "Name": "Talleyrand Term, Jacksonville",              "ArrivalDate": "21-Oct-2019",              "ArrivalTime": "14:00",              "DepartureDate": "22-Oct-2019",              "DepartureTime": "18:00"            }          ],          "PortStatus": "PP"        },        {          "PortCode": "PRSJU",          "PortName": "San Juan, PR",          "PortCallId": "9176",          "ArrivalDate": "25-Oct-2019",          "ArrivalTime": "05:00",          "SailingDate": "26-Oct-2019",          "SailingTime": "05:00",          "TypeDescription": "Load/Discharge",          "Terminals": [            {              "Code": "SJUT001",              "Name": "Isla Grande Term, San Juan, PR",              "ArrivalDate": "25-Oct-2019",              "ArrivalTime": "05:00",              "DepartureDate": "26-Oct-2019",              "DepartureTime": "05:00"            }          ],          "PortStatus": "PP"        }      ]    },    {      "VoyageMachineNumber": "2996",      "VoyageNumber": "SAL9085",      "VesselCode": "ECO",      "VesselName": "El Coqui",      "TypeCode": "export",      "TypeDescription": "",      "StatusCode": "Active",      "StatusDescription": "",      "CentralPortCode": "USJAX",      "CentralPortDescription": "Jacksonville, FL",      "PortOfLoadingCode": "USJAX",      "PortOfLoadingDescription": "Jacksonville, FL",      "PortOfDischargeCode": "PRSJU",      "PortOfDischargeDescription": "San Juan, PR",      "StartDate": "25-Oct-2019",      "EndDate": "28-Oct-2019",      "Duration": "2",      "DurationHours": "7",      "HasTransshipment": "False",      "ServiceCode": null,      "ServiceName": null,      "Route": null,      "Ports": [   {          "PortCode": "USJAX",          "PortName": "Jacksonville, FL",          "PortCallId": "9208",          "ArrivalDate": "24-Oct-2019",          "ArrivalTime": "14:00",          "SailingDate": "25-Oct-2019",          "SailingTime": "18:00",          "TypeDescription": "Load",          "Terminals": [{"Code": "JAXT000","Name": "JPA Term, Jacksonville, FL",              "ArrivalDate": "24-Oct-2019",              "ArrivalTime": "14:00",              "DepartureDate": "25-Oct-2019",              "DepartureTime": "18:00"            }          ],          "PortStatus": "PP"        },        {          "PortCode": "PRSJU",          "PortName": "San Juan, PR",          "PortCallId": "9209",          "ArrivalDate": "28-Oct-2019","ArrivalTime": "05:00","SailingDate": "29-Oct-2019","SailingTime": "05:00","TypeDescription": "Load/Discharge","Terminals": [{"Code": "SJUT001","Name": "Isla Grande Term, San Juan, PR","ArrivalDate": "28-Oct-2019","ArrivalTime": "05:00",              "DepartureDate": "29-Oct-2019",              "DepartureTime": "05:00"}],"PortStatus": "PP"}]}  ]}]';
            List< CustomerCommunity_DynamicURLResponse > listVesselSchedules = (List< CustomerCommunity_DynamicURLResponse >)JSON.deserialize(
               jresponse, List< CustomerCommunity_DynamicURLResponse >.class);
            system.debug('deserializing--->'+listVesselSchedules);
            //system.debug('ratesResponse--->'+ratesResponse.getStatusCode());
            for (CustomerCommunity_DynamicURLResponse comInfo: listVesselSchedules) {
                for(CustomerCommunity_DynamicURlResponse.Voyages voy:comInfo.Voyages) {
                    if (voy.Duration == CustomerCommunity_Constants.ZERO_STRING) {
                        voy.duration = CustomerCommunity_Constants.ZERO_HOURS_STRING;
                } else if (voy.Duration == CustomerCommunity_Constants.ONE_STRING) {
                    voy.Duration = voy.Duration + CustomerCommunity_Constants.DAY;
                } else if (voy.Duration != CustomerCommunity_Constants.ONE_STRING) {
                    voy.Duration = voy.Duration + CustomerCommunity_Constants.DAYS;
                }
            } 
            }
            system.debug('listvesselSchedules---->'+listVesselSchedules);
            return listVesselSchedules;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
}