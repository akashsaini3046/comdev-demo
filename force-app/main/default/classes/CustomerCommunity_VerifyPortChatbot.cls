public class CustomerCommunity_VerifyPortChatbot {
        public class PortData {
            @InvocableVariable(required=true)
            public String portName;
        }
        public class ResponseData {
            @InvocableVariable(required=true)
            public String sResponse;
        }
        @InvocableMethod(label='Verify Port')
        public static List<ResponseData> verifyPort(List<PortData> listPorts) {
            List<ResponseData> listResponses = new List<ResponseData>();
            ResponseData validityResponse = new ResponseData();
            String response = CustomerCommunity_Constants.EMPTY_STRING;
            Boolean portFound = False;
            try{
                Schema.DescribeFieldResult portFieldResult = Find_Route__c.Origin_Port__c.getDescribe();
                List<Schema.PicklistEntry> portList = portFieldResult.getPicklistValues();
                for (Schema.PicklistEntry port: portList) {
                    if(port.getLabel().substring(4).equalsIgnoreCase(listPorts[0].portName) || port.getValue().equalsIgnoreCase(listPorts[0].portName)) {
                        portFound = True;
                        validityResponse.sResponse = CustomerCommunity_Constants.MSG_PORTFOUND;
                        break;
                    }
                }
                if(!portFound){
                    validityResponse.sResponse = CustomerCommunity_Constants.MSG_PORTNOTFOUND;
                }
                listResponses.add(validityResponse);
                return listResponses;
            }
            catch(Exception ex){
                ExceptionHandler.logApexCalloutError(ex);
                validityResponse.sResponse = CustomerCommunity_Constants.MSG_PORTNOTFOUND;
                listResponses.add(validityResponse);
                return listResponses;
            }
        }
    }