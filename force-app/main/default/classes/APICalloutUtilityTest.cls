@isTest
public class APICalloutUtilityTest {

    @isTest
    static void testGetCallout() {

        Test.startTest();
        
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        APICalloutUtility.serviceParamsMdtMap = TestDataUtility.getServiceParamsTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        
        String response = APICalloutUtility.callAPIResponseService('GetService', '{"test":"Get"}', null, null);
        Test.stopTest();

        System.assertEquals('{"success":true}', response, 'Response Body check');
    }
    
    @isTest
    static void testPostCalloutWithQueryParams() {
        Test.startTest();
        
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        APICalloutUtility.serviceParamsMdtMap = TestDataUtility.getServiceParamsTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        String response = APICalloutUtility.callAPIResponseService('GetService', '{"test":"Post"}', new Map<String, String>{ 'test' => 'parameter'}, null);
        Test.stopTest();

        System.assertEquals('{"success":true}', response, 'Response Body check');
    }
    
    @isTest
    static void testPostCallout() {

        Test.startTest();
        
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        APICalloutUtility.serviceParamsMdtMap = TestDataUtility.getServiceParamsTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        
        String response = APICalloutUtility.callAPIResponseService('PostService', '{"test":"Post"}', null, null);
        Test.stopTest();

        System.assertEquals('{"success":true}', response, 'Response Body check');
    }
    
    @isTest
    static void testPutCallout() {

        Test.startTest();
        
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        APICalloutUtility.serviceParamsMdtMap = TestDataUtility.getServiceParamsTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        
        String response = APICalloutUtility.callAPIResponseService('PutService', '{"test":"Put"}', null, null);
        Test.stopTest();

        System.assertEquals('{"success":true}', response, 'Response Body check');
    }
    
    @isTest
    static void testDeleteCallout() {

        Test.startTest();
        
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        APICalloutUtility.serviceParamsMdtMap = TestDataUtility.getServiceParamsTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        
        String response = APICalloutUtility.callAPIResponseService('DeleteService', '{"test":"Delete"}', null, null);
        Test.stopTest();

        System.assertEquals('{"success":true}', response, 'Response Body check');
    }
    
    @isTest
    static void testForIntegrationServiceVar() {

        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        String response = APICalloutUtility.callAPIResponseService('GetService', '{"test":"Get"}', null, null);
        
        Test.stopTest();

        System.assertEquals('', response, 'Response Body check');
    }
    
    @isTest
    static void testForServiceEnvironmentVar() {

        Test.startTest();
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        String response = APICalloutUtility.callAPIResponseService('GetService', '{"test":"Get"}', null, null);
        
        Test.stopTest();

        System.assertEquals('', response, 'Response Body check');
    }
    
    @isTest
    static void testForServiceEnvParamVar() {

        Test.startTest();
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        String response = APICalloutUtility.callAPIResponseService('GetService', '{"test":"Get"}', null, null);
        
        Test.stopTest();

        System.assertEquals('', response, 'Response Body check');
    }
    
    @isTest
    static void testErrorCallout() {

        Test.startTest();
        
        APICalloutUtility.integrationServiceMdtMap = TestDataUtility.getIntegrationServiceTestMap();
        APICalloutUtility.serviceEnvironCSMap = TestDataUtility.getServiceEnvironmentTestMap();
        APICalloutUtility.serviceParamsMdtMap = TestDataUtility.getServiceParamsTestMap();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        Boolean isErrorThrown = false;
        try{
            String response = APICalloutUtility.callAPIResponseService('ErrorService', '{"test":"Error"}', null, null);
        }catch(Exception e){
            isErrorThrown = true;
        }
        Test.stopTest();

        System.assertEquals(true, isErrorThrown, 'Error Thrown check');
    }
    
    public class MockHttpResponse implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"success":true}');
            res.setStatusCode(200);
            
            if (req.getEndpoint().contains('getservice')) {
                System.assertEquals('GET', req.getMethod());
            } else if (req.getEndpoint().contains('postservice')) {
                System.assertEquals('POST', req.getMethod());
            } else if (req.getEndpoint().contains('putservice')) {
                System.assertEquals('PUT', req.getMethod());
            } else if (req.getEndpoint().contains('deleteservice')) {
                System.assertEquals('DELETE', req.getMethod());
            } else {
                res.setStatusCode(500);
            }
            return res;
        }
    }
}