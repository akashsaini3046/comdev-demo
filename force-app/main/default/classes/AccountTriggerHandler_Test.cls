@IsTest
public class AccountTriggerHandler_Test {
    
    @testSetup static void dataSetup(){
        
        List<Account> accList = new List<Account>();
        
        for(integer i = 1; i<3; i++){
            Account accObj = new Account();
            accObj.Name                         = 'Test Account'+i;
            accObj.Phone                        = '999999999';
            accObj.Website                      = 'www.google.com';
           /* accObj.Classification_Category__c   = 'Test Category'+i;
            accObj.Classification_Code__c       = 'Test Code'+i;
            accObj.Social_Network__c            = 'Facebook';
            accObj.Primary_User_ID__c           = 'Test User Id'+i;*/
            accList.add(accObj);
        }
        insert accList;
        
    }
    
    static testMethod void TestMethod2()
    {
        ProfileNames__c p1 = new ProfileNames__c();
        p1.Name = 'Sales Leader';
        insert p1;
        
        ProfileNames__c p2 = new ProfileNames__c();
        p2.Name = 'System Administrator';
        insert p2;
        
        Account accObj1 = new Account();
        accObj1.Name = 'Account 123';
        insert accObj1;
        
        DescribeFieldResult describe = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
        
        Address__c businessLocationObj = new Address__c();
        businessLocationObj.Account__c = accObj1.Id;
        businessLocationObj.Address_Line_1__c = 'address1';
        businessLocationObj.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj.Country__c = availableValues[0].getValue();
        businessLocationObj.Postal_Code__c = '1111111';
        businessLocationObj.Phone__c = '88888888888';
        businessLocationObj.State__c = 'State1';
        businessLocationObj.Name ='BL1';
        insert businessLocationObj;
        
        Contact con= new Contact();
        con.LastName='Test con1';
        con.Phone = '99999999999';
        con.Email = 'contact@email.com';
        con.Address__c = businessLocationObj.Id;
        insert con;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Update standardPricebook;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = accObj1.Id;
        opp.Name = 'Opp1';
        opp.Service_Type__c = 'Air';
        opp.CloseDate = System.today();
        opp.StageName='Prospecting';
        opp.Contact__c = con.Id;
        insert opp;
        
        Account accObj2 = new Account();
        accObj2.Name = 'Acc 1234';
        insert accObj2;
        
        List<Account> accListToDelete = new List<Account>();
        accListToDelete.add(accObj1);
        accListToDelete.add(accObj2);
        
        test.startTest();
        System.assertNotEquals(opp.AccountId, NULL);
        System.assertEquals(businessLocationObj.Account__c, opp.AccountId);
        
        try
        {
            delete accListToDelete;
        }
        catch(Exception e)
        {
            System.debug('Exception in Account Test Class'+e.getMessage());
        }
        test.stopTest();
    }
}