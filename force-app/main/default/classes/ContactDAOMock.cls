public class ContactDAOMock implements ContactDAO.IContactDAO {
 
    public List<Contact> contactsToReturn = new List<Contact>();
 
    public List<Contact> getAllContacts()
    {
        return contactsToReturn;
    }
    
    public List<Contact> getContactsByRecordType(String recordType)
    {
        return contactsToReturn;
    }
    
    public List<Contact> getContactByQueryParam(String queryCondition,String queryParam)
    {
        return contactsToReturn;
    }
}