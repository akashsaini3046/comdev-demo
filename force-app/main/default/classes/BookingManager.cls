@RestResource(urlMapping = '/getBookingData/*')
global with sharing class BookingManager {
    @HttpGet
    global static String doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        Map < String, String > paramMap = req.params;
        System.debug('req --->' + req + 'paramMap--->' + paramMap);
        String bookingNum = paramMap.get('bookingNumber');
        System.debug('bookingNum --->' + bookingNum);

        String bookingNumber = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        System.debug('bookingNumberget--->' + bookingNumber);
        if (bookingNumber != null) {
            return handleGetRates(bookingNumber);
        } else {
            return 'Booking Number does not exist';
        }

    }

    @AuraEnabled
    public static String handleGetRates(String bookingNumber) {
        try {
            System.debug('bookingNumber--------> ' + bookingNumber);

            Booking__c bookingRec = new Booking__c();
            Shipment__c shipmentRec = new Shipment__c();
            Ports_Information__c portInfoDest = new Ports_Information__c();
            Map < String, Container__c > mapContainer = new Map < String, Container__c > ();
            List < FreightDetail__c > freightDetailRec = new List < FreightDetail__c > ();

            bookingRec = [SELECT Customer_Origin_Country__c, Customer_Origin_City__c, Customer_Origin_Zip__c, Customer_Origin_Code__c,
                Customer_Destination_Country__c, Customer_Destination_City__c, Customer_Destination_Zip__c, Customer_Destination_Code__c,
                Description__c, Contract_Number__c, Origin_Type__c, Destination_Type__c, Account__c, Ready_Date__c,From_Sub_Location__c, To_Sub_Location__c,
                Transportation_Management_System_Origin__c,Transportation_Management_System_Destina__c,Pickup_Location__c,Delivery_Location__c,Payment_Terms__c               
                FROM Booking__c WHERE Booking_Number__c =: bookingNumber
            ];

            shipmentRec = [Select Origin_Code__c, Destination_Code__c, Origin_Country__c, Origin_Port__c, Destination_Port__c, Destination_Country__c, (Select Id, Color__c, Item_Weight__c, Make__c, Model__c, Unit_Quantity__c, Vin__c, Year__c, Description__c, Quantity_unit_of_Measure__c, Weight_unit_of_Measure__c, Max_Dimension_Height__c, Max_Dimension_Length__c, Max_Dimension_Width__c from Dock_Receipts__r where Description__c='AUTO') from Shipment__c where Booking__c =: bookingRec.Id Limit 1];
            freightDetailRec = [Select Manufacturer__c,Model__c,Type__c,Freight_Quantity__c, Cargo_Type__c, Length_Major__c, Width_Major__c, Height_Major__c, Length__c, Width__c, Height__c, Declared_Weight_Value__c, Privately_Owned_Dealer_Owned_Vehicle__c, (Select Container_Type__c, Category__c, Is_Empty__c, Quantity__c,Running_Reefer__c,IsShippersOwn__c,IsHeavy__c,IsNonOperativeReefer__c,OutOfGauge__c from Requirements__r), (Select Name from Commodities__r) from FreightDetail__c where Shipment__c =: shipmentRec.Id];

            for (Container__c container: [Select Id, Name, CICS_ISO_Code__c, Description__c from Container__c]) {
                mapContainer.put(container.CICS_ISO_Code__c, container);
            }


            BookingWrapper bookingWrapper = BookingWrapper.getBookingWrapper();
            System.debug('bookingWrapper ' + bookingWrapper);

            /************ Fetching Leg Information START*****************/
            bookingWrapper.booking = bookingRec;
            
            if(bookingWrapper.booking.Payment_Terms__c.equalsIgnoreCase('Collect'))
                bookingWrapper.booking.Payment_Terms__c = 'CO';
            if(bookingWrapper.booking.Payment_Terms__c.equalsIgnoreCase('Prepaid'))
                bookingWrapper.booking.Payment_Terms__c = 'PP';
            bookingWrapper.booking.Transportation_Management_System_Origin__c='';
            bookingWrapper.booking.Transportation_Management_System_Destina__c='';
            bookingWrapper.booking.Origin_Type__c = bookingRec.Description__c.substring(0, 1);
            bookingWrapper.booking.Destination_Type__c = bookingRec.Description__c.substring(1);
            if (bookingWrapper.booking.Origin_Type__c == 'D') {
                if (bookingRec.Customer_Origin_Zip__c != null && bookingRec.Customer_Origin_Zip__c != '') {
                    //bookingWrapper.booking.Customer_Origin_Code__c = bookingRec.Customer_Origin_Zip__c;
                     portInfoDest = [SELECT Softship_Zipcodes__c FROM Ports_Information__c WHERE Name =: bookingRec.Customer_Origin_Zip__c
                        AND recordType.DeveloperName = 'ZipCode'
                        LIMIT 1
                    ];
                    bookingWrapper.booking.Customer_Origin_Code__c = portInfoDest.Softship_Zipcodes__c;

                } else {
					System.debug('else-->'+bookingRec.Customer_Origin_City__c+'cccccc--->'+bookingRec.Customer_Origin_Country__c);
                    portInfoDest = [SELECT Name,Softship_Zipcodes__c FROM Ports_Information__c WHERE Zip_Code__c != null AND city__c =: bookingRec.Customer_Origin_City__c AND Country__c =: bookingRec.Customer_Origin_Country__c
                        AND recordType.DeveloperName = 'ZipCode'
                        LIMIT 1
                    ];
                    bookingWrapper.booking.Customer_Origin_Code__c = portInfoDest.Softship_Zipcodes__c;
                }
            } else {
                if(shipmentRec.Origin_Code__c!=null && shipmentRec.Origin_Code__c!='' )
                    bookingWrapper.booking.Customer_Origin_Code__c = shipmentRec.Origin_Code__c;
                else
                	bookingWrapper.booking.Customer_Origin_Code__c = shipmentRec.Origin_Country__c + shipmentRec.Origin_Port__c;
            }
            if (bookingWrapper.booking.Destination_Type__c == 'D') {
                if (bookingRec.Customer_Destination_Zip__c != null && bookingRec.Customer_Destination_Zip__c != '') {
                    //bookingWrapper.booking.Customer_Destination_Code__c = bookingRec.Customer_Destination_Zip__c;
                     portInfoDest = [SELECT Softship_Zipcodes__c FROM Ports_Information__c WHERE Name =: bookingRec.Customer_Destination_Zip__c	 AND recordType.DeveloperName = 'ZipCode'
                        LIMIT 1
                    ];
                    bookingWrapper.booking.Customer_Destination_Code__c = portInfoDest.Softship_Zipcodes__c;
                } else {
                    portInfoDest = [SELECT Softship_Zipcodes__c FROM Ports_Information__c WHERE Zip_Code__c != null AND city__c =: bookingRec.Customer_Destination_City__c AND Country__c =: bookingRec.Customer_Destination_Country__c AND recordType.DeveloperName = 'ZipCode'
                        LIMIT 1
                    ];
                    bookingWrapper.booking.Customer_Destination_Code__c = portInfoDest.Softship_Zipcodes__c;
                }

            } else {
                if(shipmentRec.Destination_Code__c!=null && shipmentRec.Destination_Code__c!='' )
                    bookingWrapper.booking.Customer_Destination_Code__c = shipmentRec.Destination_Code__c;
                else
                	bookingWrapper.booking.Customer_Destination_Code__c = shipmentRec.Destination_Country__c + shipmentRec.Destination_Port__c;
            }

            /**************** Fetching Leg Information END********************/


            /**************** Fetching Cargo Information START****************/

            //bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.clear();
            if (freightDetailRec.size() > 0) {
                System.debug('bookingWrapper--->>>' + json.serialize(freightDetailRec));
                for (FreightDetail__c freightDetail: freightDetailRec) {
                    
                    BookingWrapper.FreightDetailWrapper freightWrapper = new BookingWrapper.FreightDetailWrapper();
                    if (freightDetail.Cargo_Type__c.equalsIgnoreCase('EQUIP')) {
                        bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper.clear();
                        bookingWrapper.shipment.listCargo[0].cargoType = 'Container';
                        for (Requirement__c requirement: freightDetail.Requirements__r) {
                            BookingWrapper.RequirementWrapper reqWrapper = new BookingWrapper.RequirementWrapper();
                            reqWrapper.commodityDesc = 'Cargo, NOS';
                            reqWrapper.containerDesc = (mapContainer.get(requirement.Container_Type__c) != null) ? mapContainer.get(requirement.Container_Type__c).Description__c : '';
                            reqWrapper.containerType = (mapContainer.get(requirement.Container_Type__c) != null) ? mapContainer.get(requirement.Container_Type__c).Name : '';
                            reqWrapper.requirement = requirement;
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper.add(reqWrapper);
                        }
                        System.debug('---->>>>>>>>>'+bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper);
                    }
                    if (freightDetail.Cargo_Type__c.equalsIgnoreCase('AUTO')) {
                        bookingWrapper.shipment.listCargo[0].cargoType = 'RoRo';

                        //freightWrapper.commodityDesc = '8700000000';
                        /*if (freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c == 'P' || freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c == 'PVEH' || freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c=='Private') {
                            freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c='PVEH';
                            freightWrapper.typeOfPackage = 'PVEH';
                        } else {
                            freightWrapper.typeOfPackage = 'CVEH';
                            freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c='CVEH';
                        }
                        freightWrapper.freightDetail = freightDetail;
                        if (bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.size() == 1) {
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0] = freightWrapper;
                        } else {
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.add(freightWrapper);
                        }*/
                    }
                
                    if (freightDetail.Cargo_Type__c.equalsIgnoreCase('BBULK')) {
                        bookingWrapper.shipment.listCargo[0].cargoType = 'BreakBulk';
                        freightWrapper.commodityDesc = '8700000000';
                        freightWrapper.typeOfPackage = 'NIT';
                        freightWrapper.freightDetail = freightDetail;
                        if (bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.size() == 1) {
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0] = freightWrapper;
                        } else {
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.add(freightWrapper);
                        }

                    }
                }
                if(bookingWrapper.shipment.listCargo[0].cargoType == 'RoRo'){
                    for(Dock_Receipt__c dockReceipt : shipmentRec.Dock_Receipts__r){
                        BookingWrapper.FreightDetailWrapper freightWrapper1 = new BookingWrapper.FreightDetailWrapper();
                        freightWrapper1.commodityDesc = '8700000000';
                        freightWrapper1.typeOfPackage = 'PVEH';
                        FreightDetail__c freight = new FreightDetail__c();
                        freight.Manufacturer__c = dockReceipt.Make__c;
                        freight.Model__c = dockReceipt.Model__c;
                        //freight.Type__c = dockReceipt.
                        freight.Freight_Quantity__c = dockReceipt.Unit_Quantity__c;
                        freight.Cargo_Type__c = 'AUTO';
                        freight.Length_Major__c = dockReceipt.Max_Dimension_Length__c;
                        freight.Width_Major__c = dockReceipt.Max_Dimension_Width__c;
                        freight.Height_Major__c = dockReceipt.Max_Dimension_Height__c;
                        //freight.Length__c = dockReceipt.Max_Dimension_Length__c;
                        //freight.Width__c = dockReceipt.Max_Dimension_Width__c;
                        //freight.Height__c = dockReceipt.Max_Dimension_Height__c;
                        freight.Declared_Weight_Value__c = dockReceipt.Item_Weight__c;
                        freight.Privately_Owned_Dealer_Owned_Vehicle__c ='PVEH';     
                        freightWrapper1.freightDetail = freight;
                        if (bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.size() == 1) {
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0] = freightWrapper1;
                        } else {
                            bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.add(freightWrapper1);
                        }                   
                        
                    }

                }
            }

            /**************** Fetching Cargo Information END******************/

            System.debug('bookingWrapper--->>>' + json.serialize(bookingWrapper));

            CustomerCommunity_RatingAPIRequest requestWrapper = BookingFCLRatesService.createRequestWrapper(bookingWrapper);
            System.debug('@@@@@@ Rating API Request : ' + JSON.serialize(requestWrapper, true));
            return JSON.serialize(requestWrapper, true);

        } catch (Exception e) {
            throw e;
        }
        //return 'null';
    }

    @HttpPost
    global static Booking__c doPost(String bookingNumber) {
        System.debug('bookingNumberpost--->' + bookingNumber);
        Booking__c bookingRec = [SELECT Id, Booking_Number__c FROM Booking__c WHERE Booking_Number__c =: bookingNumber];
        return bookingRec;
    }
}