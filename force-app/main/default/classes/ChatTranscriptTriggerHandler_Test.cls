@isTest
public class ChatTranscriptTriggerHandler_Test {
    static testmethod void populateContact(){
        User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
        
        Account accObj = new Account();
        accObj.Name = 'TestAccount'; 
        insert accObj;
                
        DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
        List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
        Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = accObj.Id)}, 'BL1', 'City1', 
                                                                                stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                'US', 1)[0];
        System.assertNotEquals(NULL, businessLocationObj.Country__c);
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        System.assertNotEquals(NULL, recordTypeId);
        Contact conObj = TestDataUtility.createContact(recordTypeId, accObj.Id, new List<Address__c>{businessLocationObj}, 'Community Test', 'Contact', System.now().millisecond() + 'test@test.com', 'NULL', FALSE, NULL, 1)[0];
        System.assertNotEquals(NULL, conObj);
        
        LiveChatVisitor liveChatVisitorObj = new LiveChatVisitor();
        insert liveChatVisitorObj;
        
        LiveChatTranscript liveChatTranscriptObj = new LiveChatTranscript();
        liveChatTranscriptObj.ContactId = conObj.Id;
        liveChatTranscriptObj.Username__c = 'testcommunity@username.com';
        liveChatTranscriptObj.LiveChatVisitorId = liveChatVisitorObj.Id;
        insert liveChatTranscriptObj;
        System.assertNotEquals(NULL, liveChatTranscriptObj);
        
        System.runAs ( currentUser )
        {
            test.startTest();
            User communityUser = TestDataUtility.createCommunityUser('testcommunity@username.com','TestCommunity@email.com');
            System.assertEquals(liveChatTranscriptObj.Username__c, communityUser.Username);
            ChatTranscriptTriggerHandler.populateContact(new List<LiveChatTranscript> {liveChatTranscriptObj});
            test.stopTest();
        }
    }
}