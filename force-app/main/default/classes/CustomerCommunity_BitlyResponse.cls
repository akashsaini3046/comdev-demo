public class CustomerCommunity_BitlyResponse 
{
    @AuraEnabled
    public String status_txt{get;set;}
    @AuraEnabled
    public Data data{get;set;}
    @AuraEnabled
    public Integer status_code{get;set;}
    public class Data{
        @AuraEnabled
        public String global_hash{get;set;}
        @AuraEnabled
        public String long_url{get;set;}
        @AuraEnabled
        public String hash{get;set;}
        @AuraEnabled
        public Integer new_hash{get;set;}
        @AuraEnabled
        public String url{get;set;}
    }
}