global without sharing class CustomerCommunity_RegisterController {
	@AuraEnabled(cacheable = true)
	public static List <String> getUserFields() {
		Map <String,Schema.SObjectField> fieldMap = Schema.SObjectType.User_Register__mdt.fields.getMap();
		List <String> fieldNames = new List <String> ();
		try {
			if (fieldMap != null) {
				for (Schema.SObjectField sfield: fieldMap.Values()) {
					Schema.describefieldresult dfield = sfield.getDescribe();
					String fieldkey = dfield.getname();
					if (fieldkey.contains('__c')) {
						fieldNames.add(fieldkey);
					}
				}
				return fieldNames;
			} else {
				return null;
			}
		} catch (Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return null;
		}
	}
	@AuraEnabled(cacheable = true)
	public static List <String> getCountries() {
		List <String> picklistEntries = new List <String> ();
		Schema.DescribeFieldResult PicklistCountriesGlobal = Schema.SObjectType.Address__c.fields.Country__c;
		List <PicklistEntry> entries = PicklistCountriesGlobal.getSObjectField().getDescribe().getPicklistValues();
		try {
			for (PicklistEntry entry: entries) {
				picklistEntries.add(entry.getLabel());
			}
			if (entries != null) {
				return picklistEntries;
			} else {
				return null;
			}
		} catch (Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return null;
		}
	}
	@AuraEnabled
	public static String checkIfAccountExists(String companyName, String firstName, String lastName, String email, String title, String country) {

    
		List <Account> accList = [Select id from Account where name = : companyName LIMIT 1];
		if (accList.isEmpty()) {
			try {
				
				List<Contact> contactList =  [Select id from Contact where email = :email LIMIT 1];
                System.debug('contactList ----->>>>>>>>'+contactList);
				if(!contactList.isEmpty()){
				return 'EXCEPTION';
		       }
                List<Lead> leadList = [Select id from Lead where email= :email LIMIT 1];
                System.debug('leadList ---->>>>>>'+leadList);
                if(!leadList.isEmpty()){
                    return 'EXCEPTION1';
                }
				Account newAccount = new Account(Name = companyName, OwnerId = CustomerCommunity_Constants.OWNER_ID);
				insert newAccount;
				Contact newContact = new Contact(FirstName = firstName, LastName = lastName, Email = email, Title = title, accountId = newAccount.Id, MailingCountry = country);
				insert newContact;
				return newContact.Id;
			} catch (Exception ex) {
				ExceptionHandler.logApexCalloutError(ex);
				return null;
			}
		} else {
			List <Contact> conList = [Select id from Contact where email = : email LIMIT 1];
			System.debug('@@@@'+conList);
			if (conList.isEmpty()) {
				try {
					Contact newContact = new Contact(FirstName = firstName, LastName = lastName, Email = email, Title = title, accountId = accList.get(0).Id, MailingCountry = country);
					insert newContact;
					return newContact.Id;
				} catch (Exception ex) {
					ExceptionHandler.logApexCalloutError(ex);
					return null;
				}
			} else return conList.get(0).Id;
		}
	}
	public static String generateRandomString(Integer len) {
		final String chars = CustomerCommunity_Constants.CHARS;
		String randStr = '';
		while (randStr.length() < len) {
			Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			randStr += chars.substring(idx, idx + 1);
		}
		return randStr;
	}
	@AuraEnabled
	public static String registerUser(CustomerCommunity_UserRegWrapper objUserFilter, String contactId) {
		List <User> duplicateNameList = [SELECT id FROM User WHERE UserName = : objUserFilter.UserName];
		if (!duplicateNameList.isEmpty()) {
			return CustomerCommunity_Constants.EXISTS_MESSAGE;
		}
		List<String> profiles = new List<String>{CustomerCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME,
		CustomerCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME_USER_CUSTOM,CustomerCommunity_Constants.CUSTOMER_PROFILE_NAME_USER_CUSTOM};
		
		List <User> duplicateEmailList = [SELECT Id, ContactId FROM User WHERE Email = : objUserFilter.Email AND Profile.Name IN :profiles];
		try {
			if (duplicateEmailList.isEmpty()) {
				User newuser = new User();
				newuser.ProfileId = [SELECT Id, ContactId, Profile.Name FROM User WHERE Profile.Name = : CustomerCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME
					LIMIT 1
				].ProfileId;
				newuser.firstName = objUserFilter.FirstName;
				newuser.lastName = objUserFilter.LastName;
				newuser.Phone = objUserFilter.PhoneNumber;
				newuser.Title = objUserFilter.Title;
				newuser.Alias = CustomerCommunity_Constants.USER_ALIAS;
				newuser.Country = objUserFilter.Country;
				newuser.ContactId = contactId;
				newuser.Username = objUserFilter.UserName;
				newuser.Email = objUserFilter.Email;
				newuser.CommunityNickname = generateRandomString(8);
				newuser.TimeZoneSidKey = CustomerCommunity_Constants.TIMEZONE_SID_KEY;
				newuser.LocaleSidKey = CustomerCommunity_Constants.LOCALE_SID_KEY;
				newuser.EmailEncodingKey = CustomerCommunity_Constants.EMAIL_ENCODING_KEY;
				newuser.LanguageLocaleKey = CustomerCommunity_Constants.LANG_LOCALE_KEY;
				Database.DMLOptions dlo = new Database.DMLOptions();
				dlo.EmailHeader.triggerUserEmail = true;
				Database.SaveResult srList = Database.insert(newuser, dlo);
				if (srList != NULL) {
                    
					{
						if (srList.isSuccess()) {
							List <PermissionSet> permissionList = [SELECT id, Label FROM PermissionSet where Label = : CustomerCommunity_Constants.PERMISSION_SET_NAME
								LIMIT 1
							];
							if (!permissionList.isEmpty()) {
								PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionList.get(0).Id, AssigneeId = newuser.Id);
								insert psa;
							}
						}
					}
				}
				return CustomerCommunity_Constants.TRUE_MESSAGE;
			} else {
				return CustomerCommunity_Constants.FALSE_MESSAGE;
			}
		} catch (Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return CustomerCommunity_Constants.ERROR_MESSAGE;
		}
	}
}