public class CustomerCommunity_RouteInformation {
    @AuraEnabled public String transitTime{get; set;}
    @AuraEnabled public String startDay{get; set;}
    @AuraEnabled public String startDate{get; set;}
    @AuraEnabled public String arrivalDay{get; set;}
    @AuraEnabled public String arrivalDate{get; set;}
    @AuraEnabled public String stopCount{get; set;}
    @AuraEnabled public List<CustomerCommunity_DetailInfo> details {get; set;}
    @AuraEnabled public List<String> cutOffNotes{get; set;}
    @AuraEnabled public String originPort {get; set;}
    @AuraEnabled public String originPortAbbr {get; set;}
    @AuraEnabled public String initialVoyage {get; set;}
    @AuraEnabled public String initialVessel {get; set;}
    @AuraEnabled public String destination {get; set;}
    @AuraEnabled public String destinationPortAbbr {get; set;}
    @AuraEnabled public String finalVoyage {get; set;}
    @AuraEnabled public String finalVessel {get; set;}
    @AuraEnabled public String sequenceCount {get; set;}
    @AuraEnabled public String routeCount {get; set;}
    @AuraEnabled public String startDateExpectedFormat {get;set;}
    @AuraEnabled public String arrivalDateExpectedFormat {get;set;}
}