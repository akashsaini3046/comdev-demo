public class CustomerCommunity_CargoShipmentWrapper{
		@AuraEnabled public DataSourceCollection dataSourceCollection{get;set;}
	public class MilestoneCollection{
		@AuraEnabled public DateTime EstimatedDate{get;set;}
		@AuraEnabled public String Description{get;set;}
		@AuraEnabled public DateTime ActualDate{get;set;}
		@AuraEnabled public String EventCode{get;set;}
		@AuraEnabled public String Sequence{get;set;}
	}
	public class DataSourceCollection{
		@AuraEnabled public list<MilestoneCollection> MilestoneCollection{get;set;}
		@AuraEnabled public String Key{get;set;}
		@AuraEnabled public DataSource DataSource{get;set;}
		@AuraEnabled public String Type{get;set;}
	}
	public class DataSource{
		@AuraEnabled public String Type{get;set;}
		@AuraEnabled public list<MilestoneCollection> MilestoneCollection{get;set;}
		@AuraEnabled public String Key{get;set;}
	}
    
    public class CustomerCommunity_MilestonesInfo{
		@AuraEnabled  public String ParentJob{get; set;}
		@AuraEnabled  public String Description{get; set;}
		@AuraEnabled  public DateTime Shipmentdate{get; set;}
		@AuraEnabled  public String Status{get; set;}
	}
}