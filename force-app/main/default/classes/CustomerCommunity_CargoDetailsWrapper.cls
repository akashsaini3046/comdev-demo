public class CustomerCommunity_CargoDetailsWrapper {
    @AuraEnabled public Integer sequenceId{get; set;}
    @AuraEnabled public Integer quantity{get; set;}
    @AuraEnabled public String type{get; set;}
    @AuraEnabled public Integer totalWeight{get; set;}
    @AuraEnabled public Integer length{get; set;}
    @AuraEnabled public Integer width{get; set;}
    @AuraEnabled public Integer height{get; set;}
    @AuraEnabled public Integer totalVolume{get; set;}
    @AuraEnabled public Boolean isHazardous{get; set;}
    @AuraEnabled public Integer unNumber{get; set;}
    @AuraEnabled public FreightDetail__c freightRecord{get; set;}
    @AuraEnabled public Requirement__c requirementRecord{get; set;}
    @AuraEnabled public List<Commodity__c> listCommodityRecords{get; set;}
    
    
}