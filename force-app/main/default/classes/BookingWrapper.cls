public class BookingWrapper {  

    @AuraEnabled public Booking__c booking {get;set;}
    @AuraEnabled public Map<String, List<Party__c>> mapParty {get;set;}
    @AuraEnabled public TransportWrapper transportOrigin {get;set;}
    @AuraEnabled public TransportWrapper transportDestination {get;set;}
    @AuraEnabled public ShipmentWrapper shipment {get;set;}
    @AuraEnabled public Map<String, ShipmentWrapper> shipmentMap {get;set;}
    @AuraEnabled public Boolean isError {get;set;} 
    @AuraEnabled public String originLocation {get;set;}
    @AuraEnabled public String destinationLocation {get;set;}
    @AuraEnabled public String originLocationCode {get;set;}
    @AuraEnabled public String destinationLocationCode {get;set;}
    @AuraEnabled public Integer selectedRouteId {get;set;}
    @AuraEnabled public String errorMessage {get;set;}
    
    public BookingWrapper(){
        this.booking= new Booking__c();
        this.mapParty = new Map<String,List<Party__c>>();
        mapParty.put('Customer',new List<Party__c>{new Party__c(Type__c='CUST')});
        mapParty.put('shiper',new List<Party__c>{new Party__c(Type__c='SHP')});
        mapParty.put('consignee',new List<Party__c>{new Party__c(Type__c='CON')});
        mapParty.put('notify',new List<Party__c>());
        mapParty.put('forwarder',new List<Party__c>());        
        this.shipment = new ShipmentWrapper();
        this.shipmentMap = new Map<String,ShipmentWrapper>();
        shipmentMap.put('RORO', new ShipmentWrapper('roro'));
        shipmentMap.put('BREAKBULK', new ShipmentWrapper('breakbulk'));
        shipmentMap.put('CONTAINER', new ShipmentWrapper('container'));
        this.transportOrigin = new TransportWrapper();
        this.transportDestination = new TransportWrapper();
        this.isError = false;
        this.selectedRouteId = 1;
    }

    public class TransportWrapper{
        @AuraEnabled public Transport__c transport {get;set;}
        @AuraEnabled public List<Stop__c> listStop {get;set;}
        public TransportWrapper(){
            this.transport = new Transport__c();
            this.listStop = new List<Stop__c>{new Stop__c()};
        }
    }
    
    public class ShipmentWrapper{
        @AuraEnabled public Shipment__c shipment {get;set;}
        @AuraEnabled public List<Voyage__c> listVogage {get;set;}
        @AuraEnabled public List<CargoWrapper> listCargo {get;set;}
        @AuraEnabled public String cargoType {get;set;}
        @AuraEnabled public boolean isSelected {get;set;}
        public ShipmentWrapper(){
            this.shipment = new Shipment__c();
            this.listVogage = new List<Voyage__c>();
            this.listCargo = new List<CargoWrapper>();
        }

        public ShipmentWrapper(String cargoType){
            this.cargoType =cargoType;
            this.isSelected = (cargoType=='container');
            this.shipment = new Shipment__c();
            this.listVogage = new List<Voyage__c>();
            this.listCargo = new List<CargoWrapper>{new CargoWrapper()};
        }
    }
    public class CargoWrapper{
        @AuraEnabled public String cargoType {get;set;}
        @AuraEnabled public List<FreightDetailWrapper> listFreightDetailWrapper {get;set;}
        public CargoWrapper(){
            this.cargoType = '';            
            this.listFreightDetailWrapper = new List<FreightDetailWrapper>{new FreightDetailWrapper()};
            
        }
    }
    public class FreightDetailWrapper{
        @AuraEnabled public String commodityCode {get;set;}        
        @AuraEnabled public String commodityDesc {get;set;}          
        @AuraEnabled public String typeOfPackage {get;set;}
        @AuraEnabled public String packageDesc {get;set;}
        @AuraEnabled public FreightDetail__c freightDetail {get;set;}
        @AuraEnabled public List<CommodityWrapper> listCommodityWrapper {get;set;}
        @AuraEnabled public List<RequirementWrapper> listRequirementWrapper {get;set;}

        public FreightDetailWrapper(){
            this.commodityDesc = '';
            this.commodityCode = '';    
            this.typeOfPackage = '';
            this.packageDesc = '';
            this.freightDetail = new FreightDetail__c();
            this.listCommodityWrapper = new List<CommodityWrapper>{};
            this.listRequirementWrapper = new List<RequirementWrapper>{new RequirementWrapper()};
        }
    }

    public class CommodityWrapper{
        @AuraEnabled public Commodity__c commodity {get;set;}
        public CommodityWrapper(){
            this.commodity = new Commodity__c();
        }
    }

    public class RequirementWrapper{
        @AuraEnabled public String commodityDesc {get;set;}
        @AuraEnabled public String containerType {get;set;}
        @AuraEnabled public String containerDesc {get;set;}
         @AuraEnabled public String commodityCode {get;set;}
        @AuraEnabled public Requirement__c requirement {get;set;}
        public RequirementWrapper(){
            this.containerType ='';
            this.containerDesc ='';
            this.commodityDesc = '';
            this.requirement = new Requirement__c();
        }
    }

    @AuraEnabled
    public static BookingWrapper addNewCargo( BookingWrapper listCargoWrapper){
        //listCargoWrapper.add(new BookingWrapper.CargoWrapper());
        BookingWrapper bookWrapper = new BookingWrapper();
        bookWrapper = listCargoWrapper;
        System.debug(listCargoWrapper);
        System.debug(json.serialize(bookWrapper));
        return bookWrapper;
    }

    @AuraEnabled
    public static BookingWrapper getBookingWrapper(){		
        CargoWrapper cargoWrap= new CargoWrapper();
        cargoWrap.cargoType = 'container';
        ShipmentWrapper shipmentWrap = new ShipmentWrapper();
        shipmentWrap.listCargo.add(cargoWrap);
        BookingWrapper bookWrapper = new BookingWrapper();
        bookWrapper.shipment = shipmentWrap;
        return bookWrapper;

    }

    @AuraEnabled
    public static BookingWrapper getQuoteBookingWrapper(){		
        CargoWrapper cargoWrap= new CargoWrapper();
        cargoWrap.cargoType = 'container';
        ShipmentWrapper shipmentWrap = new ShipmentWrapper();
        shipmentWrap.listCargo.add(cargoWrap);
        BookingWrapper bookWrapper = new BookingWrapper();
        bookWrapper.booking.RecordTypeId = getRecordTypeId('Booking__c:Quote_Record');
        bookWrapper.shipment = shipmentWrap;
        return bookWrapper;

    }

    private static Map<String, ID> recordTypesMap{
        get{
            if (recordTypesMap == null ){
                recordTypesMap = new Map<String, Id>();
                for (RecordType aRecordType : [SELECT SobjectType, DeveloperName FROM RecordType WHERE isActive = true]){
                    recordTypesMap.put(aRecordType.SobjectType + ':' + aRecordType.DeveloperName, aRecordType.Id);
                }
            }
            return recordTypesMap;
        }
        set;
    }
    
    public static Id getRecordTypeId(String recTypeName){
        if (recordTypesMap!= null && recordTypesMap.containsKey(recTypeName)){
            return recordTypesMap.get(recTypeName);
        }
        return null;
    }

}