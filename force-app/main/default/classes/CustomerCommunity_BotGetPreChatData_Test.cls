@isTest
public class CustomerCommunity_BotGetPreChatData_Test {
    static testmethod void getSnapInsPrechatDataTestMethod(){
        User currentUser = [Select Id from User where Id = :UserInfo.getUserId()];
        
        Account accObj = new Account();
        accObj.Name = 'TestAccount'; 
        insert accObj;
        
        Case caseObj = TestDataUtility.createCase(accObj.Id, 'Web', currentUser.Id, 'New', 1)[0];
        
        DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
        
        List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
        Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = accObj.Id)}, 'BL1', 'City1', 
                                                                                stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                'US', 1)[0];
        System.assertNotEquals(NULL, businessLocationObj.Country__c);
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        System.assertNotEquals(NULL, recordTypeId);
        Contact conObj = TestDataUtility.createContact(recordTypeId, accObj.Id, new List<Address__c>{businessLocationObj}, 'Community Test', 'Contact', System.now().millisecond() + 'test@test.com', 'NULL', FALSE, NULL, 1)[0];
        
        LiveChatVisitor liveChatVisitorObj = new LiveChatVisitor();
        insert liveChatVisitorObj;
        
        LiveChatTranscript liveChatTranscriptObj = new LiveChatTranscript();
        liveChatTranscriptObj.ContactId = conObj.Id;
        liveChatTranscriptObj.CaseId = caseObj.Id;
        liveChatTranscriptObj.Username__c = 'TestCommunity@username.com';
        liveChatTranscriptObj.LiveChatVisitorId = liveChatVisitorObj.Id;
        liveChatTranscriptObj.ChatKey = 'ChatKeyTest';
        insert liveChatTranscriptObj;
        System.debug('liveChatTranscriptObj @@ @@ '+liveChatTranscriptObj);
        
        CustomerCommunity_BotGetPreChatData.PrechatInput pi = new CustomerCommunity_BotGetPreChatData.PrechatInput();
        pi.sChatKey = 'ChatKeyTest';        
        List<CustomerCommunity_BotGetPreChatData.PrechatInput> p = new List<CustomerCommunity_BotGetPreChatData.PrechatInput>();
        p.add(pi);
        
        CustomerCommunity_BotGetPreChatData.getSnapInsPrechatData(p);
        
        /*      CustomerCommunity_BotGetPreChatData.PrechatInput pi1 = new CustomerCommunity_BotGetPreChatData.PrechatInput();
pi1.sChatKey = 'ChatKeyT@@#$%^&*(est';        
List<CustomerCommunity_BotGetPreChatData.PrechatInput> p1 = new List<CustomerCommunity_BotGetPreChatData.PrechatInput>();
p1.add(pi);
*/    
        //   CustomerCommunity_BotGetPreChatData.getSnapInsPrechatData(p1);
    } 
}