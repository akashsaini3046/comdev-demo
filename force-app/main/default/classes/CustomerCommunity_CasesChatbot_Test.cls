@isTest
public class CustomerCommunity_CasesChatbot_Test {
    
    static testmethod void GetCaseStatus_test(){
    
        Account test_account;
        Case testcase1,testcase2;
        User test_user = [Select Id from user where Profile.Name = 'System Administrator' LIMIT 1];
        System.runAs(test_user){
            
        test_account=TestDataUtility.createAccount('Nagarro', null,'Customer', 1)[0];  
            
            DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
            List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
            Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = test_account.Id)}, 'BL1', 'City1', 
                                                                                    stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                    'US', 1)[0];
            System.assertNotEquals(NULL, businessLocationObj.Country__c);
            
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
            System.assertNotEquals(NULL, recordTypeId);
            Contact con= TestDataUtility.createContact(recordTypeId, test_account.Id , new List<Address__c>{businessLocationObj}, null, 
                                                       'Test Community Con1', 'communityexample@gmail.com', '11111111111', false, 
                                                       Null, 1)[0];
    
        testcase1 = TestDataUtility.createCase(test_account.Id,con.Id, 'Email', test_user.Id, 'In Progress','TestSub', 1)[0];
        testcase2 = TestDataUtility.createCase(test_account.Id,con.Id, 'Web', test_user.Id, 'Closed','TestSub2', 1)[0];
 
        testcase1 = [select CaseNumber from Case where Id = :testcase1.id];
        testcase2 = [select CaseNumber from Case where Id = :testcase2.id];
            
        CustomerCommunity_CasesChatbot.CaseParameters input1 = new CustomerCommunity_CasesChatbot.CaseParameters();
        input1.sCaseNumber = testcase1.CaseNumber;
           
        CustomerCommunity_CasesChatbot.CaseParameters input2 = new CustomerCommunity_CasesChatbot.CaseParameters();
        input2.sCaseNumber = testcase2.CaseNumber;
            
        CustomerCommunity_CasesChatbot.CaseParameters input3 = new CustomerCommunity_CasesChatbot.CaseParameters();
        input3.sCaseNumber = null;
            
        List<CustomerCommunity_CasesChatbot.CaseParameters> list_input1 = new List<CustomerCommunity_CasesChatbot.CaseParameters>();
        List<CustomerCommunity_CasesChatbot.CaseParameters> list_input2 = new List<CustomerCommunity_CasesChatbot.CaseParameters>();
        List<CustomerCommunity_CasesChatbot.CaseParameters> list_input3 = new List<CustomerCommunity_CasesChatbot.CaseParameters>();
        List<CustomerCommunity_CasesChatbot.CaseParameters> list_input4 = new List<CustomerCommunity_CasesChatbot.CaseParameters>();
            
            list_input1.add(input1);
            list_input2.add(input2);
            list_input3.add(input3);
            
            Test.startTest();
            
            CustomerCommunity_CasesChatbot.GetCaseStatus(list_input1);
            CustomerCommunity_CasesChatbot.GetCaseStatus(list_input2);
            CustomerCommunity_CasesChatbot.GetCaseStatus(list_input3);
            //for catch
            CustomerCommunity_CasesChatbot.GetCaseStatus(list_input4);
            
            Test.stopTest();
        }   
    }
}