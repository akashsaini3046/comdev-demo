public class PartyDataController {
    @AuraEnabled
    public static void getPartyData(Id bookingId){
       
    }
    
    
     @AuraEnabled
    public static String getStringQuery(Map<String,String> mapFieldSet,String type,String objectAPIName){
        System.debug('Entered getStringQuery()');
        String strQuery='select id ,';
        for(String fields:mapFieldSet.keySet() ){
            strQuery+=fields+',' ;
        }
        strQuery=strQuery.substring(0,strQuery.length()-1);
        
        strQuery+=' from '+objectAPIName;
        return strQuery;
    }
    
    @AuraEnabled
    public static Map<String,String> getFieldsDefinition(){
        try{
            
            Map<String,String> partyFieldNameVsFieldLabel = new Map<String,String>();
            //Map<String,Map<String,String>> sectionNameVsFieldsList =  new Map<String,Map<String,String>>();
            List<String> partyFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Party__c.FieldSets.Party_Detail_Internal.getFields()){
                partyFieldNameVsFieldLabel.put(fieldMember.getFieldPath(),fieldMember.getLabel());
               // partyFields.add(fieldMember.getFieldPath()); 
            }
            return partyFieldNameVsFieldLabel;
        }
        catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
	
}