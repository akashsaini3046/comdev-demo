public class CustomerCommunity_QuotationsControllerKa {
    @AuraEnabled
    public static List < CustomerCommunity_BookingParty > fetchContactsAndAddresses(String partyCode) {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:fetchContactsAndAddresses() with parameters');
        List < Contact > listContacts = new List < Contact > ();
        Set < Id > setAddressId = new Set < Id > ();
        Map < Id, Address__c > mapAddressRecords;
        List < CustomerCommunity_BookingParty > listPartyRecords = new List < CustomerCommunity_BookingParty > ();
        try {
            if (partyCode != Null && partyCode != '') {
                listContacts = [SELECT Id, Name, Phone, MobilePhone, Fax, Address__c FROM Contact WHERE Account.CVIF__c = : partyCode];
                if (!listContacts.isEmpty()) {
                    for (Contact contactRecord: listContacts)
                        if (contactRecord.Address__c != Null) setAddressId.add(contactRecord.Address__c);
                    mapAddressRecords = new Map < Id, Address__c > ([SELECT Id, Name, Address_Line_2__c, Address_Line_3__c, City__c, State_Picklist__c, Postal_Code__c, Country__c
                                                                 , CVIF_Location_Id__c FROM Address__c WHERE Id IN: setAddressId]);
                    for (Contact contactRecord: listContacts) {
                        CustomerCommunity_BookingParty newParty = new CustomerCommunity_BookingParty();
                        newParty.code = partyCode;
                        newParty.ContactRecord = contactRecord;
                        if (contactRecord.Address__c != Null && !mapAddressRecords.isEmpty() && mapAddressRecords.containsKey(contactRecord.Address__c)) newParty.AddressRecord = mapAddressRecords.get(contactRecord.Address__c);
                        listPartyRecords.add(newParty);
                    }
                }
                if (!listPartyRecords.isEmpty()) return listPartyRecords;
            }
            return Null;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    public static void createCargoWiseRequest(CustomerCommunity_QuotationRequest quoteWrapper, List < CustomerCommunity_CargoDetailsWrapper > cargodetailList){
   
      System.debug('quoteWrapper --->' + quoteWrapper); 
      System.debug('cargodetailList --->' + cargodetailList);   
     
        
      CustomerCommunity_QuotationRequestKa.shipper shipper = new CustomerCommunity_QuotationRequestKa.shipper();
      shipper.name = quoteWrapper.shipperAddressRecord.Name; 
      shipper.city = quoteWrapper.shipperAddressRecord.City__c;
      shipper.country = quoteWrapper.shipperAddressRecord.Country__c;
      shipper.zip = quoteWrapper.shipperAddressRecord.Postal_Code__c;
      shipper.contactName = quoteWrapper.shipperContactRecord.Name;
      shipper.contactPhoneNumber = quoteWrapper.shipperContactRecord.MobilePhone;  
         
        
      CustomerCommunity_QuotationRequestKa.consignee consignee = new CustomerCommunity_QuotationRequestKa.consignee();
      consignee.name = quoteWrapper.consigneeAddressRecord.Name;
      consignee.city = quoteWrapper.consigneeAddressRecord.City__c;  
      consignee.country = quoteWrapper.consigneeAddressRecord.Country__c;
      consignee.zip = quoteWrapper.consigneeAddressRecord.Postal_Code__c; 
      consignee.contactName = quoteWrapper.consigneeContactRecord.Name;
      consignee.contactPhoneNumber = quoteWrapper.consigneeContactRecord.MobilePhone;
         
        
      CustomerCommunity_QuotationRequestKa.customer customer =  new CustomerCommunity_QuotationRequestKa.customer();
      customer.name = quoteWrapper.customerAddressRecord.Name;
      customer.city = quoteWrapper.customerAddressRecord.City__c;
      customer.country = quoteWrapper.customerAddressRecord.Country__c;
      customer.zip = quoteWrapper.customerAddressRecord.Postal_Code__c;
      customer.contactName = quoteWrapper.customerContactRecord.Name;
      customer.contactPhoneNumber = quoteWrapper.customerContactRecord.MobilePhone;
        
        
      CustomerCommunity_QuotationRequestKa.party party = new CustomerCommunity_QuotationRequestKa.party();
      
      party.shipper =  shipper;
      party.consignee = consignee;  
      party.customer = customer;

      List<CustomerCommunity_QuotationRequestKa.voyages> listVoyages = new List<CustomerCommunity_QuotationRequestKa.voyages>();  
      CustomerCommunity_QuotationRequestKa.voyages voyages = new CustomerCommunity_QuotationRequestKa.voyages();
      voyages.estimateSailDate = String.valueOf(quoteWrapper.estSailingDate);
      listVoyages.add(voyages);
        
        
      List<CustomerCommunity_QuotationRequestKa.PackingLine> listPackageLineItem = new List<CustomerCommunity_QuotationRequestKa.PackingLine>();
      
 
      for(CustomerCommunity_CargoDetailsWrapper objCargoDetail : cargodetailList){
          CustomerCommunity_QuotationRequestKa.PackingLine packageLineItem = new CustomerCommunity_QuotationRequestKa.PackingLine(); 
          packageLineItem.Height = String.valueOf(objCargoDetail.height);
          packageLineItem.Length = String.valueOf(objCargoDetail.length);
          packageLineItem.PackQty = String.valueOf(objCargoDetail.quantity);
          packageLineItem.Volume = String.valueOf(objCargoDetail.totalVolume);
          packageLineItem.Width = String.valueOf(objCargoDetail.totalWeight);
          packageLineItem.PackTypeCode = objCargoDetail.type;
          packageLineItem.CommodityCode = String.valueOf(objCargoDetail.unNumber);  
          listPackageLineItem.add(packageLineItem);
      }  
      
      CustomerCommunity_QuotationRequestKa.PackingLineCollection packageLineCollection = new CustomerCommunity_QuotationRequestKa.PackingLineCollection();
      packageLineCollection.PackingLine = listPackageLineItem; 
      
      CustomerCommunity_QuotationRequestKa.freightDetails freightDetails = new CustomerCommunity_QuotationRequestKa.freightDetails();
      List<CustomerCommunity_QuotationRequestKa.freightDetails> listFreightDetails = new List<CustomerCommunity_QuotationRequestKa.freightDetails>();
      List<CustomerCommunity_QuotationRequestKa.commodities> listCommodities = new List<CustomerCommunity_QuotationRequestKa.commodities>();
     
      CustomerCommunity_QuotationRequestKa.commodities commodity = new CustomerCommunity_QuotationRequestKa.commodities();
      commodity.numberCommodities = '3166';
      commodity.packageGroup = 'N';
      commodity.dotName = 'ENGINES, INTERNAL COMBUSTION, INCLUDING WHEN FITTED IN MACHINERY OR VEHICLES.';
      commodity.emergencyContactName = 'CHEMTREC';
      commodity.emergencyContactPhoneNumber = '800-424-9300';
      commodity.technicalName = '';
      commodity.imoClass = '9';
      commodity.imoDGPageClass  = '';
      commodity.isHazardous  = true;
      listCommodities.add(commodity);
        
      CustomerCommunity_QuotationRequestKa.requirements requirements = new CustomerCommunity_QuotationRequestKa.requirements();
      List<CustomerCommunity_QuotationRequestKa.requirements> listRequirements = new List<CustomerCommunity_QuotationRequestKa.requirements>();
      requirements.quantity = 1;
      requirements.category = 'DRY';
      requirements.length = 45;
      requirements.rrInd = 'N';  
      listRequirements.add(requirements);
      
      freightDetails.commodities = listCommodities; 
      freightDetails.requirements = listRequirements;
        
      listFreightDetails.add(freightDetails);  
        
      List<CustomerCommunity_QuotationRequestKa.shipments> listShipments = new List<CustomerCommunity_QuotationRequestKa.shipments>();  
      CustomerCommunity_QuotationRequestKa.shipments shipment = new CustomerCommunity_QuotationRequestKa.shipments();
      shipment.destinationCode = quoteWrapper.destinationCode;
      shipment.originCode = quoteWrapper.originCode; 
      shipment.voyages = listVoyages; 
      shipment.PackingLineCollection = packageLineCollection; 
      shipment.freightDetails = listFreightDetails;  
      listShipments.add(shipment);   
          
      CustomerCommunity_QuotationRequestKa quotation = new CustomerCommunity_QuotationRequestKa();
      quotation.originType = 'T';
      quotation.destinationType = 'T';
      quotation.ContainerMode =  quoteWrapper.ContainerMode;  
      quotation.AWBServiceLevel = 'STD';  
      quotation.party = party;
      quotation.shipments = listShipments;
      
    } 
    @AuraEnabled
    public static Contact getContactDetails() { 
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:getContactDetails() with parameters');
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        try {
            userDetail = [select Id, contactId, LanguageLocaleKey from User where Id = : userId];
            if (userDetail.contactId != Null) {
                conDetail = [SELECT Id, Name, AccountId, Address__c, Account.OwnerId FROM Contact WHERE Id = : userDetail.contactId];
                System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:getContactDetails() returning--' + conDetail);
                return conDetail;
            }
            return Null;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    @AuraEnabled
    public static String getCustomerCVIF(Contact contactRecord) {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:getCustomerCVIF() with parameters');
        List < Account > listAccountRecords = new List < Account > ();
        try {
            if (contactRecord.AccountId != Null) {
                listAccountRecords = [SELECT CVIF__c, Name, Id FROM Account WHERE Id = : contactRecord.AccountId];
                if (!listAccountRecords.isEmpty() && listAccountRecords[0].CVIF__c != Null && listAccountRecords[0].CVIF__c != '') {
                    System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:getCustomerCVIF() returning--' + listAccountRecords[0].CVIF__c);
                    return listAccountRecords[0].CVIF__c;
                }
            }
            return Null;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    @AuraEnabled
    public static List < CustomerCommunity_KeyValueRecord > fetchIncoTermList() {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:fetchIncoTermList() with parameters');
        List < CustomerCommunity_KeyValueRecord > incoTermsList = new List < CustomerCommunity_KeyValueRecord > ();
        try {
            Schema.DescribeFieldResult fieldResult = New_Booking_Fields__mdt.Inco_Terms__c.getDescribe();
            List < Schema.PicklistEntry > picklistFields = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry option: picklistFields) {
                CustomerCommunity_KeyValueRecord newOption = new CustomerCommunity_KeyValueRecord();
                newOption.key = option.getValue();
                newOption.value = option.getLabel();
                incoTermsList.add(newOption);
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:fetchIncoTermList() returning--' + incoTermsList);
        return incoTermsList;
    }
    @AuraEnabled
    public static List < CustomerCommunity_KeyValueRecord > fetchReceiptDeliveryTermsList() {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:fetchReceiptDeliveryTermsList() with parameters');
        List < CustomerCommunity_KeyValueRecord > receiptDeliveryTermsList = new List < CustomerCommunity_KeyValueRecord > ();
        try {
            Schema.DescribeFieldResult fieldResult = New_Booking_Fields__mdt.Receipt_Delivery_Terms__c.getDescribe();
            List < Schema.PicklistEntry > picklistFields = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry option: picklistFields) {
                CustomerCommunity_KeyValueRecord newOption = new CustomerCommunity_KeyValueRecord();
                newOption.key = option.getValue();
                newOption.value = option.getLabel();
                receiptDeliveryTermsList.add(newOption);
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:fetchReceiptDeliveryTermsList() returning--' + receiptDeliveryTermsList);
        return receiptDeliveryTermsList;
    }
    @AuraEnabled
    public static List < String > fetchCommodityList() {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:fetchCommodityList() with parameters');
        List < String > listCommodities = new List < String > ();
        List < New_Booking_Commodities__mdt > listCommoditiesRecords = new List < New_Booking_Commodities__mdt > ();
        try {
            listCommoditiesRecords = [SELECT MasterLabel, Commodity_Name__c, Category__c FROM New_Booking_Commodities__mdt WHERE Category__c != Null AND Category__c != ''];
            if (!listCommoditiesRecords.isEmpty()) {
                for (New_Booking_Commodities__mdt currentCommodity: listCommoditiesRecords) {
                    if (currentCommodity.Commodity_Name__c != Null && currentCommodity.Commodity_Name__c != '') listCommodities.add(currentCommodity.Commodity_Name__c);
                }
            }
            System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:fetchCommodityList() returning--' + listCommodities);
            if (!listCommodities.isEmpty()) return listCommodities;
            else return null;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    @AuraEnabled
    public static List < String > fetchContainerTypeList() {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:fetchContainerTypeList() without parameters');
        List < String > listContainers = new List < String > ();
        List < New_Booking_Container_Type__mdt > listContainerTypes = new List < New_Booking_Container_Type__mdt > ();
        try {
            listContainerTypes = [SELECT Id, MasterLabel, Category__c, Length__c, Type__c FROM New_Booking_Container_Type__mdt];
            if (!listContainerTypes.isEmpty()) {
                for (New_Booking_Container_Type__mdt bookingCommodity: listContainerTypes) {
                    String containerValue = '';
                    if (bookingCommodity.Length__c != Null && bookingCommodity.Length__c != '') containerValue = containerValue + bookingCommodity.Length__c + ' ';
                    containerValue = containerValue + bookingCommodity.MasterLabel;
                    listContainers.add(containerValue);
                }
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:fetchContainerTypeList() returning--' + listContainers);
        return listContainers;
    }
    @AuraEnabled
    public static String getLocationDetails(String termCode, String locCode) {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:getLocationDetails() with parameters');
        String locDetail = '';
        List < Ports_Information__c > listPortsInfo = new List < Ports_Information__c > ();
        try {
            if (termCode == 'D') {
                listPortsInfo = [SELECT Place_Name__c, State__c, Country__c, Zip_Code__c FROM Ports_Information__c WHERE Zip_Code__c = : locCode LIMIT 1];
                if (!listPortsInfo.isEmpty()) {
                    if (listPortsInfo[0].Place_Name__c != Null && listPortsInfo[0].Place_Name__c != '') locDetail = locDetail + listPortsInfo[0].Place_Name__c;
                    if (listPortsInfo[0].State__c != Null && listPortsInfo[0].State__c != '' && listPortsInfo[0].Place_Name__c != listPortsInfo[0].State__c) locDetail = locDetail + ' ' + listPortsInfo[0].State__c;
                    if (listPortsInfo[0].Country__c != Null && listPortsInfo[0].Country__c != '') locDetail = locDetail + ' ' + listPortsInfo[0].Country__c;
                }
            }
            if (termCode == 'P') {
                listPortsInfo = [SELECT Place_Name__c, State__c, Country__c, Location_Code__c FROM Ports_Information__c WHERE Location_Code__c = : locCode LIMIT 1];
                if (!listPortsInfo.isEmpty()) {
                    if (listPortsInfo[0].Place_Name__c != Null && listPortsInfo[0].Place_Name__c != '') locDetail = locDetail + listPortsInfo[0].Place_Name__c;
                    if (listPortsInfo[0].Country__c != Null && listPortsInfo[0].Country__c != '') locDetail = locDetail + ' ' + listPortsInfo[0].Country__c;
                }
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        System.debug(LoggingLevel.INFO, 'Exiting from CustomerCommunity_NewBookingsController:getLocationDetails() returning--' + locDetail);
        if (locDetail != '') return locDetail;
        else return null;
    }
    @AuraEnabled
    public static List < CustomerCommunity_CargoDetailsWrapper > createBlankCargoRecord(List<CustomerCommunity_CargoDetailsWrapper>cargoRecordList) {
        List < CustomerCommunity_CargoDetailsWrapper > listCargoDetails = new List < CustomerCommunity_CargoDetailsWrapper > ();
        System.debug('cccccccc--->'+cargoRecordList.size());
        if(!cargoRecordList.isEmpty()){
          listCargoDetails=cargoRecordList;    
        }
        CustomerCommunity_CargoDetailsWrapper containerCargoRecord = new CustomerCommunity_CargoDetailsWrapper();
        containerCargoRecord.sequenceId = listCargoDetails.size() + 1;
        containerCargoRecord.totalVolume = null;
        containerCargoRecord.unNumber = null;
        containerCargoRecord.isHazardous = False;
        containerCargoRecord.quantity = null;
        containerCargoRecord.totalWeight = null;
        containerCargoRecord.length = null;
        containerCargoRecord.width = null;
        containerCargoRecord.height = null;
        listCargoDetails.add(containerCargoRecord);
        System.debug('size--->' + listCargoDetails.size());
        return listCargoDetails;
    }
    @AuraEnabled
    public static List < String > getCarriers() {
        List < String > listOfCarrierCodes = new List < String > ();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint('https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/getLFSCarriers');
        request.setMethod('GET');
        request.setHeader('content-type', 'application/json');
        request.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
        request.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
        response = http.send(request);
        System.debug('responseBody-->' + response.getBody());
        List < CustomerCommunity_LfsCarrierResponse > careerInfo = (List < CustomerCommunity_LfsCarrierResponse > ) JSON.deserialize(response.getBody(), List < CustomerCommunity_LfsCarrierResponse > .class);
        System.debug('careerInfo-->' + careerInfo);
        for (CustomerCommunity_LfsCarrierResponse carrierAccount: careerInfo) {
            listOfCarrierCodes.add(carrierAccount.CarrierCode);
        }
        System.debug('listOfCarrierCodes-->' + listOfCarrierCodes);
        return listOfCarrierCodes;
    }
    @AuraEnabled
    public static List < CustomerCommunity_LfsRatesResponse > getCarrierRates(List < String > listOfAllCarrierCodes, List < CustomerCommunity_LfsRatesResponse > listOfDisplayedQuotes, CustomerCommunity_QuotationRequest quoteWrapper, List < CustomerCommunity_CargoDetailsWrapper > cargodetailList) {
        System.debug('listOfAllCarrierCodes--->' + listOfAllCarrierCodes + 'size-->' + listOfAllCarrierCodes.size() + 'listOfDisplayedQuotes-->' + listOfDisplayedQuotes + 'sizequotes-->' + listOfDisplayedQuotes.size());
        System.debug('quoteWrapper-->' + quoteWrapper + 'cargodetailList-->' + cargodetailList);
        createCargoWiseRequest(quoteWrapper,cargodetailList);
        List < String > listOfPreviousCodes = new List < String > ();
        for (CustomerCommunity_LfsRatesResponse displayedCode: listOfDisplayedQuotes) {
            System.debug('bodycode--->' + displayedCode.Body.CarrierCode);
            listOfPreviousCodes.add(displayedCode.Body.CarrierCode);
        }
        System.debug('listOfPreviousCodes-----' + listOfPreviousCodes + 'size--->' + listOfPreviousCodes.size());
        List < CustomerCommunity_LfsRatesResponse > listOfQuoteRatesResponse = new List < CustomerCommunity_LfsRatesResponse > ();
        try {
            List < String > newCodeList = new List < String > ();
            if (listOfPreviousCodes != null && !listOfPreviousCodes.isEmpty()) {
                for (String carCode: listOfAllCarrierCodes) {
                    if (newCodeList.size() < 6) {
                        System.debug('while-->');
                        System.debug('containscar-->' + carCode);
                        if (!listOfPreviousCodes.contains(carCode)) {
                            System.debug('contains-->' + carCode);
                            newCodeList.add(carCode);
                        }
                    }
                }
            } else {
                for (Integer i = 0; i < 6; i++) {
                    System.debug('contains111-->');
                    newCodeList.add(listOfAllCarrierCodes[i]);
                }
            }
            if(!newCodeList.isEmpty()){
                listOfQuoteRatesResponse=createRequest(newCodeList,quoteWrapper,cargodetailList);
            }
            System.debug('newCodeList--->' + newCodeList);
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        listOfQuoteRatesResponse.addAll(listOfDisplayedQuotes);
        return listOfQuoteRatesResponse;
    }
    @AuraEnabled
    public static List < CustomerCommunity_LfsRatesResponse > createRequest(List < String > newCodeList,CustomerCommunity_QuotationRequest quotationWrapper, 
        List < CustomerCommunity_CargoDetailsWrapper > cargoWrapperList) {
        List < CustomerCommunity_LfsRatesResponse > listOfRatesResponse = new List < CustomerCommunity_LfsRatesResponse > ();
        List<Ports_Information__c> listPortsInfo = new List<Ports_Information__c>();
        String[] arrayOfaccessorials = new List < String > ();
        String[] arrayOfNMFC = new List < String > ();
        String[] arrayOfStackAmount = new List < String > ();
        String[] arrayOfUNNmbr = new List < String > ();
        Set<String> setLocationCodes = new Set<String>();
        if(quotationWrapper!=null && quotationWrapper.destinationCode!=null && quotationWrapper.originCode!=null){
            setLocationCodes.add(quotationWrapper.originCode); 
            setLocationCodes.add(quotationWrapper.destinationCode);
        }
        if(!setLocationCodes.isEmpty()){
            listPortsInfo = [SELECT Place_Name__c,State_Abbreviation__c, Zip_Code__c, Location_Code__c, Name,Country_Code__c FROM Ports_Information__c 
                                 WHERE Zip_Code__c IN :setLocationCodes OR Location_Code__c IN :setLocationCodes OR Name IN :setLocationCodes];
        }
        
        System.debug('setLocationCodes-->'+setLocationCodes+'listPortsInfo-->'+listPortsInfo);
        for (String carriedCode: newCodeList) {
            CustomerCommunity_LfsRatesResponse rateInfo=new CustomerCommunity_LfsRatesResponse();
            List < CustomerCommunity_QuotationRequest.Item > itemList = new List < CustomerCommunity_QuotationRequest.Item > ();
            for(CustomerCommunity_CargoDetailsWrapper cargoWrapper:cargoWrapperList){
                CustomerCommunity_QuotationRequest.Item item = new CustomerCommunity_QuotationRequest.Item();
                item.Volume = String.valueOf(cargoWrapper.totalVolume);
                item.Height = String.valueOf(cargoWrapper.height);
                item.Width = String.valueOf(cargoWrapper.width);
                item.ItemClass = '100';
                item.Length = String.valueOf(cargoWrapper.length);
                item.Commodity = 'Tables';
                item.Weight = String.valueOf(cargoWrapper.totalWeight);
                item.Hazmat = cargoWrapper.isHazardous;
                item.DimType = cargoWrapper.type;
                item.Qty = String.valueOf(cargoWrapper.quantity);
                item.NMFC = arrayOfNMFC;
                item.StackAmount = arrayOfStackAmount;
                item.Stack = 'false';
                item.UNNmbr = arrayOfUNNmbr;
                itemList.add(item);
            }
                
            CustomerCommunity_QuotationRequest.PcsDims pcdms = new CustomerCommunity_QuotationRequest.PcsDims();
            pcdms.Item = itemList;
            
            CustomerCommunity_QuotationRequest.Accessorials accessorials = new CustomerCommunity_QuotationRequest.Accessorials();
            accessorials.Item = arrayOfaccessorials;
            
            CustomerCommunity_QuotationRequest.GetRatesRequest ratesRequest = new CustomerCommunity_QuotationRequest.GetRatesRequest();
            for(Ports_Information__c port : listPortsInfo){
                if(port.Zip_Code__c == quotationWrapper.originCode || port.Location_Code__c == quotationWrapper.originCode || port.Name == quotationWrapper.originCode){
                    ratesRequest.OriginCountry = port.Country_Code__c;
                    ratesRequest.OriginCity = port.Place_Name__c;
                    ratesRequest.OriginState = port.State_Abbreviation__c;
                    ratesRequest.OriginZipCode = port.Zip_Code__c;
                }
                if(port.Zip_Code__c ==quotationWrapper.destinationCode || port.Location_Code__c ==quotationWrapper.destinationCode || port.Name == quotationWrapper.destinationCode){
                    ratesRequest.DestinationCountry = port.Country_Code__c;
                    ratesRequest.DestinationState = port.State_Abbreviation__c;
                    ratesRequest.DestinationCity = port.Place_Name__c;
                    ratesRequest.DestinationZipCode = port.Zip_Code__c;
                }
            }
            ratesRequest.PcsDims = pcdms;
            ratesRequest.WeightEach = 'false';
            ratesRequest.UOM = 'US';
            ratesRequest.Accessorials = accessorials;
            ratesRequest.Carrier = carriedCode;
            
            CustomerCommunity_QuotationRequest.Body body = new CustomerCommunity_QuotationRequest.Body();
            body.GetRatesRequest = ratesRequest;
            
            CustomerCommunity_QuotationRequest quoteRequest = new CustomerCommunity_QuotationRequest();
            quoteRequest.Body = body;
            rateInfo=sendRateRequest(quoteRequest,carriedCode);
            
            listOfRatesResponse.add(rateInfo);
            System.debug('rateInfo-->'+rateInfo+'listOfRatesResponse--->'+listOfRatesResponse);
            
        }
        return listOfRatesResponse; 
    }
    
    @AuraEnabled
    public static CustomerCommunity_LfsRatesResponse sendRateRequest(CustomerCommunity_QuotationRequest quoteRequest,String carriedCode){
        List<CustomerCommunity_LfsRatesResponse> listOfRatesResponse=new List<CustomerCommunity_LfsRatesResponse>();
        CustomerCommunity_LfsRatesResponse ratesInfo= new CustomerCommunity_LfsRatesResponse();
        String requestBody = JSON.serialize(quoteRequest);
            System.debug('requestBody-->' + requestBody);
            Http http = new Http();
            HttpRequest ratesRequest = new HttpRequest();
            HttpResponse ratesResponse = new HttpResponse();
            ratesRequest.setEndpoint('https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/getLFSCarrierRates');
            ratesRequest.setMethod('POST');
            ratesRequest.setBody(requestBody);
            ratesRequest.setHeader('content-type', 'application/json');
            ratesRequest.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
            ratesRequest.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
            ratesRequest.setTimeout(120000);
            ratesResponse = http.send(ratesRequest);
            System.debug('ratesResponse-->' + ratesResponse + 'ratesResponse--->' + ratesResponse.getBody());
            if (!ratesResponse.getBody().contains('Error')) {
                System.debug('errror-->');
                ratesInfo = (CustomerCommunity_LfsRatesResponse) JSON.deserialize(ratesResponse.getBody(), CustomerCommunity_LfsRatesResponse.class);
                System.debug('ratesInfo1111-->' + ratesInfo);
                if (ratesInfo.Body != null) {
                    for (CustomerCommunity_LfsRatesResponse.Carrier carrierInfo: ratesInfo.Body.GetRatesData.Results.Carrier) {
                        ratesInfo.Body.CarrierName = carrierInfo.CarrierName;
                        ratesInfo.Body.ServiceLevel = carrierInfo.ServiceLevel;
                        ratesInfo.Body.TransitDays = carrierInfo.TransitDays;
                        if (carrierInfo.Total != null) {
                            ratesInfo.Body.Rate = Decimal.valueOf(carrierInfo.Total);
                        }
                        ratesInfo.Body.CarrierCode = carriedCode;
                    }
                    //listOfRatesResponse.add(ratesInfo);
                } else if (ratesInfo.Body == null) {
                    CustomerCommunity_LfsRatesResponse ratesnullInfo = new CustomerCommunity_LfsRatesResponse();
                    CustomerCommunity_LfsRatesResponse.Body nullBody = new CustomerCommunity_LfsRatesResponse.Body();
                    System.debug('ratesnullInfo-->' + ratesnullInfo);
                    nullBody.CarrierName = 'Error';
                    nullBody.ServiceLevel = 'Error';
                    nullBody.TransitDays = 'Error';
                    nullBody.Rate = 10;
                    nullBody.CarrierCode = carriedCode;
                    ratesnullInfo.Body = nullBody;
                    ratesInfo=ratesnullInfo;
                    //listOfRatesResponse.add(ratesnullInfo);
                }
                //System.debug('listOfRatesResponse-->' + listOfRatesResponse);
            }
            else if (ratesResponse.getBody().contains('Error')) {
                System.debug('errror-->');
                CustomerCommunity_LfsRatesResponse ratesErrorInfo = new CustomerCommunity_LfsRatesResponse();
                CustomerCommunity_LfsRatesResponse.Body errorBody = new CustomerCommunity_LfsRatesResponse.Body();
                System.debug('ratesErrorInfo-->' + ratesErrorInfo);
                errorBody.CarrierName = 'Error';
                errorBody.ServiceLevel = 'Error';
                errorBody.TransitDays = 'Error';
                errorBody.Rate = 10;
                errorBody.CarrierCode = carriedCode;
                ratesErrorInfo.Body = errorBody;
                //listOfRatesResponse.add(ratesErrorInfo);
                ratesInfo=ratesErrorInfo;
                System.debug('listOfQuoteRateserrrorResponse-->' + listOfRatesResponse.size());
            }
            //System.debug('finallistttt-->' + listOfRatesResponse.size());
            //listOfRatesResponse.addAll(listOfDisplayedQuotes);
            return ratesInfo;
    }
        
    @AuraEnabled
    public static List < Ports_Information__c > getCFSLocations() {
        List < Ports_Information__c > listPortInformation = new List < Ports_Information__c > ();
        listPortInformation = [SELECT Id, Name, Place_Name__c, Street__c, Location_Description__c, State__c, State_Abbreviation__c
                               , Country__c, Zip_Code__c FROM Ports_Information__c WHERE RecordType.DeveloperName = 'CFS_Locations'];
        if (!listPortInformation.isEmpty()) return listPortInformation;
        else return Null;
    }
    @AuraEnabled
    public static CustomerCommunity_QuotationRequest createBlankQuotationRecord() {
        System.debug(LoggingLevel.INFO, 'Entering to CustomerCommunity_NewBookingsController:createBlankBookingRecord() without parameters');
        CustomerCommunity_QuotationRequest quotationRecord = new CustomerCommunity_QuotationRequest();
        quotationRecord.receiptTermVal = '--None--';
        quotationRecord.deliveryTermVal = '--None--';
        quotationRecord.originCode = '';
        quotationRecord.destinationCode = '';
        quotationRecord.ContainerMode= 'LCL';
        return quotationRecord;
    }
    
    @AuraEnabled
    public static CustomerCommunity_LfsRatesResponse getCarrierRatesDuplicate(String carrierCode){
        System.debug('carrierCode--->'+carrierCode);
        CustomerCommunity_LfsRatesResponse listOfQuoteRatesResponse=new CustomerCommunity_LfsRatesResponse();
        try{
            String [] arrayOfaccessorials = new List<String>();
            String [] arrayOfNMFC = new List<String>();
            String [] arrayOfStackAmount = new List<String>();
            String [] arrayOfUNNmbr = new List<String>();
            
                    List<CustomerCommunity_QuotationRequest.Item> itemList=new List<CustomerCommunity_QuotationRequest.Item>();
                    CustomerCommunity_QuotationRequest.Item item=new CustomerCommunity_QuotationRequest.Item();
                    item.Volume='10';
                    item.Height='10';
                    item.Width='10';
                    item.ItemClass='100';
                    item.Length='10';
                    item.Commodity='Tables';
                    item.Weight='150';
                    item.Hazmat=false;
                    item.DimType='PLT';
                    item.Qty='1';
                    item.NMFC=arrayOfNMFC;
                    item.StackAmount=arrayOfStackAmount;
                    item.Stack='false';
                    item.UNNmbr=arrayOfUNNmbr;
                    itemList.add(item);
                    
                    CustomerCommunity_QuotationRequest.PcsDims  pcdms=new CustomerCommunity_QuotationRequest.PcsDims();
                    pcdms.Item=itemList;
                    
                    CustomerCommunity_QuotationRequest.Accessorials accessorials=new CustomerCommunity_QuotationRequest.Accessorials();
                    accessorials.Item= arrayOfaccessorials ;
                    
                    CustomerCommunity_QuotationRequest.GetRatesRequest ratesRequest=new CustomerCommunity_QuotationRequest.GetRatesRequest();
                    ratesRequest.DestinationCountry='US';
                    ratesRequest.DestinationState='IL';
                    ratesRequest.DestinationCity='Chicago';
                    ratesRequest.PcsDims=pcdms;
                    ratesRequest.DestinationZipCode='60606';
                    ratesRequest.WeightEach='false';
                    ratesRequest.OriginCountry='US';
                    ratesRequest.UOM='US';
                    ratesRequest.OriginState='FL';
                    ratesRequest.OriginCity='Miami';
                    ratesRequest.Accessorials=accessorials;
                    ratesRequest.OriginZipCode='33133';
                    ratesRequest.Carrier=carrierCode;
                    
                    CustomerCommunity_QuotationRequest.Body body=new CustomerCommunity_QuotationRequest.Body();
                    body.GetRatesRequest=ratesRequest;
                    
                    CustomerCommunity_QuotationRequest quoteRequest =new CustomerCommunity_QuotationRequest();
                    quoteRequest.Body=body;
                    
                    String requestBody = JSON.serialize(quoteRequest);
                    System.debug('requestBody-->'+requestBody);  
                    Http http1 = new Http();
                    HttpRequest ratesRequest1 = new HttpRequest();
                    HttpResponse ratesResponse = new HttpResponse();
                              
                    ratesRequest1.setEndpoint('https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/getLFSCarrierRates');
                    ratesRequest1.setMethod('POST');
                    ratesRequest1.setBody(requestBody);
                    ratesRequest1.setHeader('content-type', 'application/json');
                    ratesRequest1.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
                    ratesRequest1.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
                    ratesResponse = http1.send(ratesRequest1);
                    System.debug('ratesResponse-->'+ratesResponse+'ratesResponse--->'+ratesResponse.getBody());


                    if(!ratesResponse.getBody().contains('Error')){
                        System.debug('errror-->');
                        CustomerCommunity_LfsRatesResponse ratesInfo = (CustomerCommunity_LfsRatesResponse ) JSON.deserialize(ratesResponse.getBody(), 
                        CustomerCommunity_LfsRatesResponse.class);
                        System.debug('ratesInfo1111-->'+ratesInfo);
                        
                        for(CustomerCommunity_LfsRatesResponse.Carrier carrierInfo :ratesInfo.Body.GetRatesData.Results.Carrier){
                                ratesInfo.Body.CarrierName=carrierInfo.CarrierName;
                                ratesInfo.Body.ServiceLevel=carrierInfo.ServiceLevel;
                                ratesInfo.Body.TransitDays=carrierInfo.TransitDays;
                                ratesInfo.Body.Rate=Decimal.valueOf(carrierInfo.Total);
                                ratesInfo.Body.CarrierCode=carrierCode;
                        }
                        listOfQuoteRatesResponse=ratesInfo;
                        
                    }
                    if(ratesResponse.getBody().contains('Error')){
                        System.debug('errror-->');
                        CustomerCommunity_LfsRatesResponse ratesErrorInfo = new CustomerCommunity_LfsRatesResponse();
                        CustomerCommunity_LfsRatesResponse.Body errorBody=new CustomerCommunity_LfsRatesResponse.Body();
                        System.debug('ratesErrorInfo-->'+ratesErrorInfo);
                        errorBody.CarrierName='Error';
                        errorBody.ServiceLevel='Error';
                        errorBody.TransitDays='Error';
                        errorBody.Rate=10;
                        errorBody.CarrierCode=carrierCode;
                        ratesErrorInfo.Body=errorBody;
                        listOfQuoteRatesResponse=ratesErrorInfo;
                        System.debug('listOfQuoteRateserrrorResponse-->'+listOfQuoteRatesResponse);
                
                    }
                } 
        
            
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        return listOfQuoteRatesResponse;
    }
}