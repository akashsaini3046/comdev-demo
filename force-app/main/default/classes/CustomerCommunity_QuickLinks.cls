public class CustomerCommunity_QuickLinks {
    @AuraEnabled public String linkTitle {get; set;}
    @AuraEnabled public String linkRef {get; set;}
}