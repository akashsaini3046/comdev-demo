public class CustomerCommunity_QuotationRequestKa {
    public party party{get;set;}
    public String TotalVolume{get;set;}
    public String TotalWeight{get;set;}
    public String AWBServiceLevel{get;set;}
    public String TotalWeightUnit{get;set;}
    public String ContainerMode{get;set;}
    public String TotalVolumeUnit{get;set;}
    public String destinationType{get;set;}
    public list<shipments> shipments{get;set;}
    public String originType{get;set;}
    public class voyages{
        public String estimateSailDate{get;set;}
    }
    public class shipper{
        public String faxNumber{get;set;}
        public String contactPhoneNumber{get;set;}
        public String addressLine1{get;set;}
        public String contactName{get;set;}
        public String addressLine2{get;set;}
        public String name{get;set;}
        public String city{get;set;}
        public String refNumber{get;set;}
        public String state{get;set;}
        public String cvifLocationCode{get;set;}
        public String country{get;set;}
        public String cvif{get;set;}
        public String zip{get;set;}
        public String typeShipper{get;set;}
    }
    public class shipments{
        public String numberShipments{get;set;}
        public list<voyages> voyages{get;set;}
        public list<freightDetails> freightDetails{get;set;}
        public String destinationCode{get;set;}
        public PackingLineCollection PackingLineCollection{get;set;}
        public String originCode{get;set;}
    }
    public class requirements{
        public Integer length{get;set;}
        public String rrInd{get;set;}
        public Integer quantity{get;set;}
        public String category{get;set;}
    }
    public class party{
        public customer customer{get;set;}
        public consignee consignee{get;set;}
        public shipper shipper{get;set;}
    }
    public class PackingLineCollection{
        public list<PackingLine> PackingLine{get;set;}
    }
    public class PackingLine{
        public String CommodityCode{get;set;}
        public String Length{get;set;}
        public String Width{get;set;}
        public String Height{get;set;}
        public String LengthUnit{get;set;}
        public String Volume{get;set;}
        public String VolumeUnit{get;set;}
        public String PackTypeDesc{get;set;}
        public String Weight{get;set;}
        public String PackTypeCode{get;set;}
        public String WeightUnit{get;set;}
        public String PackQty{get;set;}
    }
    public class freightDetails{
        public list<requirements> requirements{get;set;}
        public list<commodities> commodities{get;set;}
    }
    public class customer{
        public String faxNumber{get;set;}
        public String name{get;set;}
        public String refNumber{get;set;}
        public String contactName{get;set;}
        public String cvifLocationCode{get;set;}
        public String contactPhoneNumber{get;set;}
        public String cvif{get;set;}
        public String addressLine1{get;set;}
        public String typeCustomer{get;set;}
        public String addressLine2{get;set;}
        public String country{get;set;}
        public String state{get;set;}
        public String zip{get;set;}
        public String city{get;set;}
    }
    public class consignee{
        public String zip{get;set;}
        public String state{get;set;}
        public String city{get;set;}
        public String country{get;set;}
        public String addressLine2{get;set;}
        public String name{get;set;}
        public String contactName{get;set;}
        public String refNumber{get;set;}
        public String contactPhoneNumber{get;set;}
        public String cvifLocationCode{get;set;}
        public String faxNumber{get;set;}
        public String cvif{get;set;}
        public String addressLine1{get;set;}
        public String typeConsignee{get;set;}
    }
    public class commodities{
        public String numberCommodities{get;set;}
        public String technicalName{get;set;}
        public String emergencyContactPhoneNumber{get;set;}
        public String imoClass{get;set;}
        public String emergencyContactName{get;set;}
        public String imoDGPageClass{get;set;}
        public String dotName{get;set;}
        public Boolean isHazardous{get;set;}
        public String packageGroup{get;set;}
    }
}