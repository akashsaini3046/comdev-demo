public class CustomerCommunity_CreateCICSBooking {
    
    public static Booking__c CreateCICSBooking(Id bookingRecordId, Boolean bookAndRate){
        List<Booking__c> listBookingsRecord = new List<Booking__c>();
        List<Party__c> listParties = new List<Party__c>();
        List<Shipment__c> listShipments = new List<Shipment__c>();
        List<Voyage__c> listVoyages = new List<Voyage__c>();
        List<FreightDetail__c> listFreightDetails = new List<FreightDetail__c>();
        List<Requirement__c> listRequirements = new List<Requirement__c>();
        List<Commodity__c> listCommodities = new List<Commodity__c>();
        List<PackingLine__c> listPackingLine = new List<PackingLine__c>();
        
        Set<Id> setShipmentIds = new Set<Id>();
        Set<Id> setFreightIds = new Set<Id>();
        Map<Id, List<Voyage__c>> mapShipmentVoyages = new Map<Id, List<Voyage__c>>();
        Map<Id, List<PackingLine__c>> mapShipmentPackingLines = new Map<Id, List<PackingLine__c>>();
        Map<Id, List<FreightDetail__c>> mapShipmentFreights = new Map<Id, List<FreightDetail__c>>();
        Map<Id, List<Commodity__c>> mapFreightCommodities = new Map<Id, List<Commodity__c>>();
        Map<Id, List<Requirement__c>> mapFreightRequirements = new Map<Id, List<Requirement__c>>();
        Map<String, String> mapPackageType = new Map<String, String>();
        CustomerCommunity_BookingCreationRequest bookingRequestBody = new CustomerCommunity_BookingCreationRequest();
        CustomerCommunity_BookingCreationRequest.party bookingRequestParty = new CustomerCommunity_BookingCreationRequest.party(); 
        bookingRequestBody.originType = '';
        bookingRequestBody.destinationType = '';
        bookingRequestBody.BookAndRate = bookAndRate;
        //bookingRequestBody.AWBServiceLevel = '';
        bookingRequestBody.ContainerMode = '';
        bookingRequestBody.TotalVolume = '';
        bookingRequestBody.TotalVolumeUnit = '';
        bookingRequestBody.TotalWeight = '';
        bookingRequestBody.TotalWeightUnit = '';
        
        try{
            listBookingsRecord = [SELECT Name, Booking_Number__c, Origin_Type__c, Destination_Type__c, Status__c, Container_Mode__c, Total_Volume__c, 
                                  Total_Volume_Unit__c, Total_Weight__c, Total_Weight_Unit__c FROM Booking__c WHERE Id = :bookingRecordId LIMIT 1];
            if(!listBookingsRecord.isEmpty()){
                listParties = [SELECT Name, Address_Line1__c, Address_Line2__c, Booking__c, CHB_Number__c, City__c, Code__c, Country__c, CVIF__c, CVIF_Location_Code__c, Fax_Number__c, Phone_Number__c,
                               REF_Number__c, State__c, Type__c, Zip__c  FROM Party__c WHERE Booking__c = :listBookingsRecord[0].Id];
                listShipments = [SELECT Booking__c, Destination_Code__c, Origin_Code__c FROM Shipment__c WHERE Booking__c = :listBookingsRecord[0].Id];
            }
            
            if(!listShipments.isEmpty()){
                for(Shipment__c shipment : listShipments)
                    setShipmentIds.add(shipment.Id);
                listVoyages = [SELECT Shipment__c,Estimate_Sail_Date__c FROM Voyage__c WHERE Shipment__c IN :setShipmentIds];
                listPackingLine = [SELECT Name, Commodity__c, Height__c, Length__c, Length_Unit__c, Package_Quantity__c, Package_Type__c, Shipment__c, Volume__c,
                                  Volume_Unit__c, Weight__c, Weight_Unit__c, Width__c FROM PackingLine__c WHERE Shipment__c IN :setShipmentIds];
                listFreightDetails = [SELECT Shipment__c, Cargo_Type__c FROM FreightDetail__c WHERE Shipment__c IN :setShipmentIds];
                if(!listFreightDetails.isEmpty()){
                    for(FreightDetail__c freight : listFreightDetails)
                        setFreightIds.add(freight.Id);
                    listRequirements = [SELECT Category__c, Freight__c, Length__c, Quantity__c, rrInd__c FROM Requirement__c WHERE Freight__c IN :setFreightIds];
                    listCommodities = [SELECT Freight__c, IMO_Class__c, IMO_DG_Page_Class__c, Is_Hazardous__c, Name__c, Number__c, Package_Group__c, Phone_Number__c, Technical_Name__c, Dot_Name__c
                                       FROM Commodity__c WHERE Freight__c IN :setFreightIds];
                }
            }
            
            if(!listShipments.isEmpty()){
                for(Shipment__c shipmentRecord : listShipments){
                    List<Voyage__c> listTempVoyages = new List<Voyage__c>();
                    List<PackingLine__c> listTempPackingLines = new List<PackingLine__c>(); 
                    List<FreightDetail__c> listTempFreights = new List<FreightDetail__c>();
                    if(!listVoyages.isEmpty()){
                        for(Voyage__c voyageRecord : listVoyages){
                            if(voyageRecord.Shipment__c == shipmentRecord.Id)
                                listTempVoyages.add(voyageRecord);
                        }
                    }
                    if(!listTempVoyages.isEmpty())
                        mapShipmentVoyages.put(shipmentRecord.Id, listTempVoyages);
                    if(!listPackingLine.isEmpty()){
                        for(PackingLine__c packingLineRecord : listPackingLine){
                            if(packingLineRecord.Shipment__c == shipmentRecord.Id)
                                listTempPackingLines.add(packingLineRecord);
                        }
                    }
                    if(!listTempPackingLines.isEmpty())
                        mapShipmentPackingLines.put(shipmentRecord.Id, listTempPackingLines);
                    if(!listFreightDetails.isEmpty()){
                        for(FreightDetail__c freightRecord : listFreightDetails){
                            if(freightRecord.Shipment__c == shipmentRecord.Id)
                                listTempFreights.add(freightRecord);
                        }
                    }
                    if(!listTempFreights.isEmpty())
                        mapShipmentFreights.put(shipmentRecord.Id, listTempFreights);
                }
                if(!listFreightDetails.isEmpty()){
                    for(FreightDetail__c freightRecord : listFreightDetails){
                        List<Requirement__c> listTempRequirements = new List<Requirement__c>();
                        List<Commodity__c> listTempCommodities = new List<Commodity__c>();
                        if(!listRequirements.isEmpty()){
                            for(Requirement__c requirementRecord : listRequirements){
                                if(requirementRecord.Freight__c == freightRecord.Id)
                                    listTempRequirements.add(requirementRecord);
                            }
                        }
                        if(!listTempRequirements.isEmpty())
                            mapFreightRequirements.put(freightRecord.Id, listTempRequirements);
                        if(!listCommodities.isEmpty()){
                            for(Commodity__c commodityRecord : listCommodities){
                                if(commodityRecord.Freight__c == freightRecord.Id)
                                    listTempCommodities.add(commodityRecord);
                            }
                        }
                        if(!listTempCommodities.isEmpty())
                            mapFreightCommodities.put(freightRecord.Id, listTempCommodities);
                    }
                }
            }
            
            // Create a map of Packing Type Code and Description
            Schema.DescribeFieldResult fieldResult = PackingLine__c.Package_Type__c.getDescribe();
            List<Schema.PicklistEntry> picklistFields = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry option: picklistFields)
                mapPackageType.put(option.getValue(), option.getLabel());
            
            if(listBookingsRecord[0].Origin_Type__c != Null && listBookingsRecord[0].Origin_Type__c != ''){
                if(listBookingsRecord[0].Origin_Type__c.contains(' '))
                    bookingRequestBody.originType = listBookingsRecord[0].Origin_Type__c.Substring(0,1);
                else
                    bookingRequestBody.originType = listBookingsRecord[0].Origin_Type__c;
            }
            if(listBookingsRecord[0].Destination_Type__c != Null && listBookingsRecord[0].Destination_Type__c != ''){
                if(listBookingsRecord[0].Destination_Type__c.contains(' '))
                    bookingRequestBody.destinationType = listBookingsRecord[0].Destination_Type__c.Substring(0,1);
                else
                    bookingRequestBody.destinationType = listBookingsRecord[0].Destination_Type__c;
            }
            
            //if(listBookingsRecord[0].Container_Mode__c != Null && listBookingsRecord[0].Container_Mode__c != '')
            //    bookingRequestBody.ContainerMode = listBookingsRecord[0].Container_Mode__c;
            if(listBookingsRecord[0].Total_Volume__c != Null)
                bookingRequestBody.TotalVolume = String.valueOf(listBookingsRecord[0].Total_Volume__c);
            if(listBookingsRecord[0].Total_Volume_Unit__c != Null)
                bookingRequestBody.TotalVolumeUnit = listBookingsRecord[0].Total_Volume_Unit__c;
            if(listBookingsRecord[0].Total_Weight__c != Null)
                bookingRequestBody.TotalWeight = String.valueOf(listBookingsRecord[0].Total_Weight__c);
            if(listBookingsRecord[0].Total_Weight_Unit__c != Null)
                bookingRequestBody.TotalWeightUnit = listBookingsRecord[0].Total_Weight_Unit__c;
            
            if(!listParties.isEmpty()){
                for(Party__c partyRecord : listParties){
                    if(partyRecord.Type__c != '' && partyRecord.Type__c != Null){
                        CustomerCommunity_BookingCreationRequest.partyInfo bookingRequestPartyInfo = new CustomerCommunity_BookingCreationRequest.partyInfo(); 
                        bookingRequestPartyInfo.type = partyRecord.Type__c;
                        if(partyRecord.CVIF__c != Null)
                            bookingRequestPartyInfo.cvif = String.valueof(partyRecord.CVIF__c);
                        if(partyRecord.REF_Number__c != Null)
                            bookingRequestPartyInfo.refNumber = partyRecord.REF_Number__c;
                        if(partyRecord.Address_Line1__c != Null){
                            bookingRequestPartyInfo.name = partyRecord.Address_Line1__c;
                            bookingRequestPartyInfo.addressLine1 = partyRecord.Address_Line1__c;
                        }
                        if(partyRecord.Address_Line2__c != Null)
                            bookingRequestPartyInfo.addressLine2 = partyRecord.Address_Line2__c;
                        if(partyRecord.Name != Null)
                            bookingRequestPartyInfo.contactName = partyRecord.Name;
                        if(partyRecord.Phone_Number__c != Null)
                            bookingRequestPartyInfo.contactPhoneNumber = String.valueof(partyRecord.Phone_Number__c);
                        if(partyRecord.Fax_Number__c != Null)
                            bookingRequestPartyInfo.faxNumber = String.valueOf(partyRecord.Fax_Number__c);
                        if(partyRecord.City__c != Null)
                            bookingRequestPartyInfo.city = partyRecord.City__c;
                        if(partyRecord.State__c != Null)
                            bookingRequestPartyInfo.state = partyRecord.State__c;
                        if(partyRecord.Country__c != Null)
                            bookingRequestPartyInfo.country = partyRecord.Country__c;
                        if(partyRecord.Zip__c != Null)
                            bookingRequestPartyInfo.zip = partyRecord.Zip__c;
                        if(partyRecord.Type__c == 'SHP')
                            bookingRequestParty.shipper = bookingRequestPartyInfo;
                        if(partyRecord.Type__c == 'CON')
                            bookingRequestParty.consignee = bookingRequestPartyInfo;
                        if(partyRecord.Type__c == 'CUST')
                            bookingRequestParty.customer = bookingRequestPartyInfo;
                    }
                }
                bookingRequestBody.party = bookingRequestParty;
            }
            if(!listShipments.isEmpty()){
                List<CustomerCommunity_BookingCreationRequest.shipments> listBookingRequestShipments = new List<CustomerCommunity_BookingCreationRequest.shipments>();
                List<CustomerCommunity_BookingCreationRequest.PackingLine> listBookingRequestPackingLine = new List<CustomerCommunity_BookingCreationRequest.PackingLine>();
                CustomerCommunity_BookingCreationRequest.PackingLineCollection bookingRequestPackingLineCollection = new CustomerCommunity_BookingCreationRequest.PackingLineCollection();
                Integer count = 1;
                for(Shipment__c shipmentRecord : listShipments){
                    CustomerCommunity_BookingCreationRequest.shipments bookingRequestShipment = new CustomerCommunity_BookingCreationRequest.shipments();
                    bookingRequestShipment.numberVal = '00' + count;
                    if(shipmentRecord.Origin_Code__c != Null)
                        bookingRequestShipment.originCode = shipmentRecord.Origin_Code__c;
                    if(shipmentRecord.Destination_Code__c != Null)
                        bookingRequestShipment.destinationCode = shipmentRecord.Destination_Code__c;
                    if(!mapShipmentVoyages.isEmpty() && mapShipmentVoyages.containsKey(shipmentRecord.Id)){
                        List<CustomerCommunity_BookingCreationRequest.voyages> listBookingRequestVoyages = new List<CustomerCommunity_BookingCreationRequest.voyages>();
                        for(Voyage__c voyageRecord : mapShipmentVoyages.get(shipmentRecord.Id)){
                            CustomerCommunity_BookingCreationRequest.voyages bookingRequestVoyage = new CustomerCommunity_BookingCreationRequest.voyages();
                            if(voyageRecord.Estimate_Sail_Date__c != Null){
                                bookingRequestVoyage.estimateSailDate = String.valueOf(voyageRecord.Estimate_Sail_Date__c);
                                listBookingRequestVoyages.add(bookingRequestVoyage);
                           }
                        }
                        if(!listBookingRequestVoyages.isEmpty())
                            bookingRequestShipment.voyages = listBookingRequestVoyages;
                    }
                    if(!mapShipmentPackingLines.isEmpty() && mapShipmentPackingLines.containsKey(shipmentRecord.Id)){
                        for(PackingLine__c packingLineRecord : mapShipmentPackingLines.get(shipmentRecord.Id)){
                            CustomerCommunity_BookingCreationRequest.PackingLine bookingRequestPackingLine = new CustomerCommunity_BookingCreationRequest.PackingLine();
                            bookingRequestPackingLine.CommodityCode = '';
                            bookingRequestPackingLine.Height = '';
                            bookingRequestPackingLine.Width = '';
                            bookingRequestPackingLine.Length = '';
                            bookingRequestPackingLine.LengthUnit = '';
                            bookingRequestPackingLine.Volume = '';
                            bookingRequestPackingLine.VolumeUnit = '';
                            bookingRequestPackingLine.Weight = '';
                            bookingRequestPackingLine.WeightUnit = '';
                            bookingRequestPackingLine.PackQty = '';
                            bookingRequestPackingLine.PackTypeCode = '';
                            bookingRequestPackingLine.PackTypeDesc = '';
                            if(packingLineRecord.Commodity__c != Null)
                                bookingRequestPackingLine.CommodityCode = packingLineRecord.Commodity__c;
                            if(packingLineRecord.Height__c != Null)
                                bookingRequestPackingLine.Height = String.valueOf(packingLineRecord.Height__c);
                            if(packingLineRecord.Width__c != Null)
                                bookingRequestPackingLine.Width = String.valueOf(packingLineRecord.Width__c);   
                            if(packingLineRecord.Length__c != Null)
                                bookingRequestPackingLine.Length = String.valueOf(packingLineRecord.Length__c);
                            if(packingLineRecord.Length_Unit__c != Null)
                                bookingRequestPackingLine.LengthUnit = packingLineRecord.Length_Unit__c;
                            if(packingLineRecord.Volume__c != Null)
                                bookingRequestPackingLine.Volume = String.valueOf(packingLineRecord.Volume__c);
                            if(packingLineRecord.Volume_Unit__c != Null)
                                bookingRequestPackingLine.VolumeUnit = packingLineRecord.Volume_Unit__c;
                            if(packingLineRecord.Weight__c != Null)
                                bookingRequestPackingLine.Weight = String.valueOf(packingLineRecord.Weight__c);
                            if(packingLineRecord.Weight_Unit__c != Null)
                                bookingRequestPackingLine.WeightUnit = packingLineRecord.Weight_Unit__c;
                            if(packingLineRecord.Package_Quantity__c != Null) 
                                bookingRequestPackingLine.PackQty = String.valueOf(packingLineRecord.Package_Quantity__c);
                            if(packingLineRecord.Package_Type__c != Null)
                                bookingRequestPackingLine.PackTypeCode = packingLineRecord.Package_Type__c;
                            if(packingLineRecord.Package_Type__c != Null && mapPackageType.containsKey(packingLineRecord.Package_Type__c))
                                bookingRequestPackingLine.PackTypeDesc = mapPackageType.get(packingLineRecord.Package_Type__c);
                            listBookingRequestPackingLine.add(bookingRequestPackingLine);
                        }
                    }
                    if(!mapShipmentFreights.isEmpty() && mapShipmentFreights.containsKey(shipmentRecord.Id)){
                        List<CustomerCommunity_BookingCreationRequest.freightDetails> listBookingRequestFreights = new List<CustomerCommunity_BookingCreationRequest.freightDetails>();
                        for(FreightDetail__c freightRecord : mapShipmentFreights.get(shipmentRecord.Id)){
                            CustomerCommunity_BookingCreationRequest.freightDetails bookingRequestFreight = new CustomerCommunity_BookingCreationRequest.freightDetails();
                            if(!mapFreightRequirements.isEmpty() && mapFreightRequirements.containsKey(freightRecord.Id)){
                                List<CustomerCommunity_BookingCreationRequest.requirements> listBookingRequestRequirements = new List<CustomerCommunity_BookingCreationRequest.requirements>();
                                for(Requirement__c requirementRecord : mapFreightRequirements.get(freightRecord.Id)){
                                    CustomerCommunity_BookingCreationRequest.requirements bookingRequestRequirement = new CustomerCommunity_BookingCreationRequest.requirements();
                                    if(requirementRecord.Category__c != Null)
                                        bookingRequestRequirement.category = requirementRecord.Category__c;
                                    if(requirementRecord.Length__c != Null)
                                        bookingRequestRequirement.length = String.valueOf(requirementRecord.Length__c);
                                    if(requirementRecord.Quantity__c != Null)
                                        bookingRequestRequirement.quantity = String.valueOf(requirementRecord.Quantity__c);
                                    if(requirementRecord.rrInd__c != Null)
                                        bookingRequestRequirement.rrInd = requirementRecord.rrInd__c;
                                    listBookingRequestRequirements.add(bookingRequestRequirement);
                                }
                                if(!listBookingRequestRequirements.isEmpty()){
                                    bookingRequestFreight.requirements = listBookingRequestRequirements;
                                }  
                            }
                            if(!mapFreightCommodities.isEmpty() && mapFreightCommodities.containsKey(freightRecord.Id)){
                                List<CustomerCommunity_BookingCreationRequest.commodities> listBookingRequestCommodities = new List<CustomerCommunity_BookingCreationRequest.commodities>();
                                for(Commodity__c commodityRecord : mapFreightCommodities.get(freightRecord.Id)){
                                    CustomerCommunity_BookingCreationRequest.commodities bookingRequestCommodity = new CustomerCommunity_BookingCreationRequest.commodities();
                                    if(commodityRecord.Number__c != Null)
                                        bookingRequestCommodity.numberVal = String.valueOf(commodityRecord.Number__c);
                                    if(commodityRecord.Package_Group__c != Null)
                                        bookingRequestCommodity.packageGroup = commodityRecord.Package_Group__c;
                                    if(commodityRecord.Dot_Name__c != Null)
                                        bookingRequestCommodity.dotName = commodityRecord.Dot_Name__c;
                                    if(commodityRecord.Name__c != Null)
                                        bookingRequestCommodity.emergencyContactName = commodityRecord.Name__c;
                                    if(commodityRecord.Phone_Number__c != Null)
                                        bookingRequestCommodity.emergencyContactPhoneNumber = String.valueOf(commodityRecord.Phone_Number__c);
                                    if(commodityRecord.Technical_Name__c != Null)
                                        bookingRequestCommodity.technicalName = commodityRecord.Technical_Name__c;
                                    if(commodityRecord.IMO_Class__c != Null)
                                        bookingRequestCommodity.imoClass = String.valueOf(commodityRecord.IMO_Class__c);
                                    if(commodityRecord.IMO_DG_Page_Class__c != Null)
                                        bookingRequestCommodity.imoDGPageClass = commodityRecord.IMO_DG_Page_Class__c;
                                    bookingRequestCommodity.isHazardous = true;
                                    listBookingRequestCommodities.add(bookingRequestCommodity);
                                }
                                if(!listBookingRequestCommodities.isEmpty()){
                                    bookingRequestFreight.commodities = listBookingRequestCommodities;
                                }  
                            }
                            listBookingRequestFreights.add(bookingRequestFreight);
                        }
                        if(!listBookingRequestFreights.isEmpty())
                            bookingRequestShipment.freightDetails = listBookingRequestFreights;
                    }
                    bookingRequestPackingLineCollection.PackingLine = listBookingRequestPackingLine;
                    bookingRequestShipment.PackingLineCollection = bookingRequestPackingLineCollection;
                    listBookingRequestShipments.add(bookingRequestShipment);
                }
                bookingRequestBody.shipments = listBookingRequestShipments;
            }
            
            // Make HTTP callout to create Booking
            String requestBody = JSON.serialize(bookingRequestBody);
            requestBody = requestBody.replaceAll('numberVal', 'number');
            System.debug('@@@ requestBody - ' + requestBody);
            HttpRequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();
            Http http = new Http();
            request.setEndpoint('https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/createBooking');
            request.setMethod('POST');
            request.setHeader('content-type', 'application/json');
            request.setTimeout(120000);
            request.setHeader('client_id', 'f1bd172b39184ab2bbb92c89d6e78d17');
            request.setHeader('client_secret', '7a09ca2CE3cC473BAaB07d90ec29D902');
            request.setBody(requestBody);
            response = http.send(request);
            System.debug('response--->'+response.getBody());
            //String bookingName = response.getBody();
            String bookingNumber;
            CustomerCommunity_CICSBookingResponse bookingInfo;
            bookingInfo = (CustomerCommunity_CICSBookingResponse) JSON.deserialize(response.getBody(), CustomerCommunity_CICSBookingResponse.class);
            if(response.getStatus() == 'OK'){
                //listBookingsRecord[0].Booking_Number__c = bookingName.substringBetween('"', '"');
                listBookingsRecord[0].Booking_Number__c = bookingInfo.BookingNumber; 
                listBookingsRecord[0].Status__c = 'Booking Confirmed';
                if(bookingInfo.CWTransitDays != Null)
                    listBookingsRecord[0].Transit_Days__c = bookingInfo.CWTransitDays;
                if(bookingInfo.TotalAmount != Null && bookingInfo.TotalAmount != '')
                    listBookingsRecord[0].Total_Amount__c = bookingInfo.TotalAmount;
                update listBookingsRecord[0];
                return listBookingsRecord[0];
            }
            else
                return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
}