@isTest
public class CustCommunity_PreferencesControllerTest {
    public static testmethod void testUpdateDetails(){
        String userName = UserInfo.getUserName();        
        User activeUser = [Select Email From User where Username = : userName limit 1];
        
        ID userID = UserInfo.getUserId();
        List <String> listSmsNumbers = new List <String>();
        List <String> listWhatsAppNumbers = new List <String>();
        
        listSmsNumbers.add('1234567890');
        listWhatsAppNumbers.add('1234567890');
        //positive test
        CustomerCommunity_PreferencesController.updateDetails(activeUser.Email,userID,listSmsNumbers,listWhatsAppNumbers);
        //negative test
        CustomerCommunity_PreferencesController.updateDetails(activeUser.Email,null,listSmsNumbers,listWhatsAppNumbers);
        
    }
    public static testmethod void testFetchUserDetails(){
        String userName = UserInfo.getUserName(); 
        User activeUser = [Select Email From User where Username = : userName limit 1];
        
        User testUser = CustomerCommunity_PreferencesController.fetchUserDetails();
        
        System.assertEquals(activeUser.Id,testUser.Id);
    }
}