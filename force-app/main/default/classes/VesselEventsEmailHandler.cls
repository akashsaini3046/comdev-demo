global class VesselEventsEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelop){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        List<Attachment> listAttachment = new List<Attachment>();
        List<BAPLIE_Line_Item__c> listBaplieRec = new List<BAPLIE_Line_Item__c>();
        List<VesselEvents__c> listVessel = new List<VesselEvents__c>();
        Id VesselEventId;
        System.debug('@@@ subject - ' + email.subject);
        String emailSubject = email.subject;
        
        if(emailSubject != Null && emailSubject != ''){
            List<String> listKeywords = emailSubject.split(' ');
            listVessel = [SELECT Id, Name, Voyage__c, ReportType__c, PortName__c, PortId__c FROM VesselEvents__c WHERE PortId__c IN :listKeywords AND ReportType__c = 'AB'];
            System.debug('@@@ List - ' + listVessel);
            if(!listVessel.isEmpty()){
                for(VesselEvents__c vessel : listVessel){
                    if(vessel.Name != Null && vessel.PortName__c != Null && emailSubject.contains(vessel.Name) && emailSubject.contains(vessel.PortName__c)){
                        VesselEventId = vessel.Id;
                        break;
                    }
                }
            }
        }
        
        if(email.binaryAttachments != Null && VesselEventId != Null){
            System.debug('@@@ binary Attachment');
            for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                System.debug('@@@ Attachment type - ' + bAttachment.headers + ' MIME - ' + bAttachment.mimeTypeSubType);
                String fileName = bAttachment.fileName;
                if(fileName.contains('.csv')){
                    Attachment attachment = new Attachment();
                    attachment.Name = bAttachment.fileName;
                    attachment.Body = bAttachment.body;
                    attachment.ParentId = VesselEventId;
                    listAttachment.add(attachment);
                    listBaplieRec.addAll(readCSVFile(bAttachment.body.toString(), attachment.ParentId));
                }
            }
        }
        if(!listAttachment.isEmpty())
            insert listAttachment;
        if(!listBaplieRec.isEmpty())
            insert listBaplieRec;
        return result;
    }
    
    public List<BAPLIE_Line_Item__c> readCSVFile(String contactDataAsString, Id vesselId) {
        List<String> contactDataLines = contactDataAsString.split('\n');
        List<string> csvFieldNames = contactDataLines[0].split(',');
        Map<String, Integer> fieldNumberMap = new Map<String, Integer>();
        Map<String, String> mapFieldNames = new Map<String, String>();
        List<BAPLIE_Line_Item__c> listBaplie = new List<BAPLIE_Line_Item__c>();
        
        mapFieldNames.put('Equipment Nbr','Equipment_Number__c'); mapFieldNames.put('Eqp Type','Equipment_Type__c'); mapFieldNames.put('Type','Type__c');
        mapFieldNames.put('Status','Status__c'); mapFieldNames.put('Weight','Weight__c'); mapFieldNames.put('Hazardous','Hazardous__c');
        mapFieldNames.put('Reefer','Reefer__c'); mapFieldNames.put('Temp','Temperature__c'); mapFieldNames.put('POL','POL__c');
        mapFieldNames.put('POD','POD__c'); mapFieldNames.put('Booking Nbr','Booking_Number__c'); mapFieldNames.put('Voyage Nbr','Voyage_Number__c'); mapFieldNames.put('Vessel Name','Vessel_Name__c');
        
        for(Integer i=0; i<csvFieldNames.size(); i++)
            fieldNumberMap.put(csvFieldNames[i].trim(), i);
        
        for(Integer i=1; i<contactDataLines.size(); i++){
            BAPLIE_Line_Item__c baplieRec = new BAPLIE_Line_Item__c();
            baplieRec.VesselEvents__c = vesselId;
            List<String> csvRecordData = contactDataLines[i].split(',');
            for (String fieldName: csvFieldNames) {
                if(!mapFieldNames.isEmpty() && mapFieldNames.containsKey(fieldName.trim())){
                    Integer fieldNumber = fieldNumberMap.get(fieldName);
                    String fieldValue = csvRecordData[fieldNumber];
                    baplieRec.put(mapFieldNames.get(fieldName.trim()), fieldValue.trim());
                }
            }
            listBaplie.add(baplieRec);                
        }
        return listBaplie;         
    }
}