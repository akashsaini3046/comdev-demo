public with sharing class CC_BillofLadingDetailController {
    @AuraEnabled
    public static Map<String,List<Map<String, String>>> getBolDetails(Id bolId){
        try {
            Map<String,List<Map<String,String>>> sectionNameVsFieldLabelValue = new Map<String,List<Map<String,String>>>();
            Map<String,Map<String,String>> sectionNameVsFieldsList = getFieldsDefinition();
            Map<List<Id>,List<Map<String,String>>> billItemsIdsDataMap = new Map<List<Id>,List<Map<String,String>>>();
            List<Id> bolIds = new List<Id>();
            List<Id> billItemIds = new List<Id> ();
            bolIds.add(bolId);
            
            sectionNameVsFieldLabelValue.put('Bol',((getSectionWiseData(sectionNameVsFieldsList,'bolFields','BOL_Detail_Internal','Bill_Of_Lading__c',bolIds,'NA')).values())[0]);            
            sectionNameVsFieldLabelValue.put('FreeText',((getSectionWiseData(sectionNameVsFieldsList,'bolFieldsFreeText','BOL_Free_Text_Internal','Bill_Of_Lading__c',bolIds,'NA')).values())[0]);      
            sectionNameVsFieldLabelValue.put('Locations',((getSectionWiseData(sectionNameVsFieldsList,'bolFieldsLocations','BOL_Locations_Internal','Bill_Of_Lading__c',bolIds,'NA')).values())[0]);            
            sectionNameVsFieldLabelValue.put('Routing',((getSectionWiseData(sectionNameVsFieldsList,'bolFieldsRouting','BOL_Routing_Internal','Bill_Of_Lading__c',bolIds,'NA')).values())[0]);
            
            sectionNameVsFieldLabelValue.put('Voyage', ((getSectionWiseData(sectionNameVsFieldsList,'voyageFields','Voyage_BOL_Internal','Voyage__c',bolIds,'Bill_Of_Lading_Number__c')).values())[0]);
            sectionNameVsFieldLabelValue.put('Equipment',((getSectionWiseData(sectionNameVsFieldsList,'equipmentFields','Equipment_BOL_Internal','Equipment__c',bolIds,'Bill_Of_Lading_Number__c')).values())[0]); 

            billItemsIdsDataMap = getSectionWiseData(sectionNameVsFieldsList,'billItemFields','Bill_Item_BOL_Internal','Bill_Item__c',bolIds,'Bill_Of_Lading_Number__c');
            sectionNameVsFieldLabelValue.put('BillItems', (billItemsIdsDataMap.values())[0]);
            sectionNameVsFieldLabelValue.put('ItemTexts',((getSectionWiseData(sectionNameVsFieldsList,'itemTextFields','Bill_Item_BOL_Item_Text_Internal','Bill_Item__c',bolIds,'Bill_Of_Lading_Number__c')).values())[0]);
            
            for(List<id> ids : billItemsIdsDataMap.keySet()){
                billItemIds.addAll(ids);
            }
            sectionNameVsFieldLabelValue.put('SubItems', ((getSectionWiseData(sectionNameVsFieldsList,'subItemFields','Sub_Items_BOL_Internal','Sub_Item__c',billItemIds,'Bill_Item__c')).values())[0]);
            sectionNameVsFieldLabelValue.put('Commodity',((getSectionWiseData(sectionNameVsFieldsList,'commodityFields','Commodity_BOL_Internal','Commodity__c',billItemIds,'Bill_Item__c')).values())[0]);
            return sectionNameVsFieldLabelValue;
        } catch (Exception ex) {
            LogFactory.error('CC_BillofLadingDetailController', 'getBolDetails', 'Bol Detail Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            ExceptionHandler.logApexCalloutError(ex);
        }
        LogFactory.saveLog();
        return Null;
    }

    public static Map<String,Map<String,String>> getFieldsDefinition(){   
        try {
            Map<String, Map<String,String>> sectionNameVsFieldsList = new Map<String, Map<String,String>>();
            sectionNameVsFieldsList.putAll(getFieldNameLabel('BOL_Detail_Internal','Bill_Of_Lading__c','bolFields'));
            sectionNameVsFieldsList.putAll(getFieldNameLabel('BOL_Free_Text_Internal','Bill_Of_Lading__c','bolFieldsFreeText'));
            sectionNameVsFieldsList.putAll(getFieldNameLabel('BOL_Locations_Internal','Bill_Of_Lading__c','bolFieldsLocations'));
			sectionNameVsFieldsList.putAll(getFieldNameLabel('BOL_Routing_Internal','Bill_Of_Lading__c','bolFieldsRouting'));
			sectionNameVsFieldsList.putAll(getFieldNameLabel('Bill_Item_BOL_Internal','Bill_Item__c','billItemFields'));
			sectionNameVsFieldsList.putAll(getFieldNameLabel('Bill_Item_BOL_Item_Text_Internal','Bill_Item__c','itemTextFields'));
			sectionNameVsFieldsList.putAll(getFieldNameLabel('Sub_Items_BOL_Internal','Sub_Item__c','subItemFields'));
            sectionNameVsFieldsList.putAll(getFieldNameLabel('Voyage_BOL_Internal','Voyage__c','voyageFields'));
            sectionNameVsFieldsList.putAll(getFieldNameLabel('Commodity_BOL_Internal','Commodity__c','commodityFields'));
            sectionNameVsFieldsList.putAll(getFieldNameLabel('Equipment_BOL_Internal','Equipment__c','equipmentFields'));
            return sectionNameVsFieldsList ;  
        }
        catch (Exception ex) {
            LogFactory.error('CC_BillofLadingDetailController', 'getFieldsDefinition', 'Field Definition Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            return Null;
        }
    }

    public static Map<List<Id>,List<Map<String,String>>> getSectionWiseData(Map<String,Map<String,String>> sectionNameVsFieldsList,String sectionName,String fieldSetName,String objectAPIName,List<Id> parentIdsList,String parentObjectRelationName){
        
        try{
            Map<List<Id>,List<Map<String,String>>> idListRecordDataMap = new Map<List<Id>,List<Map<String,String>>>();
            Map<String,String> fieldNameVsFieldValue = new Map<String,String>();
            List<Map<String,String>> objectRecordsLabelValueList = new List<Map<String,String>>();
            List<Id> sectionIds = new List<Id>();
            String queryString;
            String commaSeperatedFields = getStringQuery(readFieldSet(fieldSetName, objectAPIName));
           /* if(parentObjectRelationName=='NA'){
                queryString = getStringQuery(fieldSetData,objectAPIName)+ ' where id IN : parentIdsList';
            } else{
                queryString = getStringQuery(readFieldSet(fieldSetName,objectAPIName),objectAPIName) +' where '+ parentObjectRelationName +' IN  : parentIdsList';
            }*/
            
            System.debug(queryString + queryString);
            //List<sObject> queryResult=(Database.query(queryString));
            List<sObject> queryResult = BillOfLadingDAO.getGenericObjectsResult(commaSeperatedFields, objectAPIName,  parentObjectRelationName, parentIdsList);
            System.debug(queryResult.size());
            for(sObject objRec : queryResult){
                sectionIds.add(objRec.id);
                fieldNameVsFieldValue = new Map<String,String>();
                for(String field : sectionNameVsFieldsList.get(sectionName).keySet()){
                    Object value = objRec.get(field);
                    if(value!=null) {
                        fieldNameVsFieldValue.put(sectionNameVsFieldsList.get(sectionName).get(field), getDisplayValue(value,objectAPIName,field));
                    }
                    if(value==null){
                        fieldNameVsFieldValue.put(sectionNameVsFieldsList.get(sectionName).get(field),'');
                    }
                }
                objectRecordsLabelValueList.add(fieldNameVsFieldValue); 
            }
            idListRecordDataMap.put(sectionIds, objectRecordsLabelValueList);
            return idListRecordDataMap ;
        }
        catch(Exception ex){
            LogFactory.error('CC_BillofLadingDetailController', 'getSectionWiseData', 'Section Wise Data Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            return null;
        }
    } 
    
    private static String getDisplayValue(Object value , String objectAPIName, String field){ 
        if(String.valueOf(value)=='false'){
            return 'No';
        }  
        else if(String.valueOf(value)=='true'){
            return 'Yes';
        }
        Map<String,Schema.SObjectField> mFields = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap();
        Schema.SObjectField fieldDesc = mFields.get(field);
        if(fieldDesc.getDescribe().getType()==Schema.DisplayType.Date){
            Date d= Date.valueOf(value);
            String stringDate = d.day()+'/'+d.month()+'/'+d.year();
            return stringDate;
        }
        else if(fieldDesc.getDescribe().getType()==Schema.DisplayType.DateTime){
            String timeZone = String.valueOf(UserInfo.getTimeZone());
            DateTime dt = DateTime.valueOf(value);
            String stringDateTime = dt.format('yyyy/MM/dd HH:mm:ss',timeZone);
            return stringDateTime;
        }
        else{
            return String.valueOf(value);
        }          
     }

    public static Map<String,Map<String,String>> getFieldNameLabel(String fieldSetName, String objectName,String sectionName){
        Map<String,Map<String,String>> secNameVsField = new Map<String,Map<String,String>>();
        List<Schema.FieldSetMember> fieldMemberList = readFieldSet(fieldSetName,ObjectName);
        Map<String,String> fieldNameLabelMap = new Map<String,String>();
        for(Schema.FieldSetMember fieldMember : fieldMemberList){
            fieldNameLabelMap.put(fieldMember.getFieldPath(), fieldMember.getLabel());
        }
        if(fieldNameLabelMap != null && !fieldNameLabelMap.isEmpty()){
            secNameVsField.put(sectionName, fieldNameLabelMap);
        }
        return secNameVsField;
    }

    public static String getStringQuery(List<Schema.FieldSetMember> fieldMemberList){
        String strQuery = '';
        //String strQuery='select id ,';
        for(Schema.FieldSetMember fieldMember : fieldMemberList){
            if(fieldMember.getType()==Schema.DisplayType.Picklist){
                strQuery+='toLabel('+fieldMember.getFieldPath()+'),' ;
            } else{
                strQuery+=fieldMember.getFieldPath()+',' ;
            }                
        }
        strQuery=strQuery.substring(0,strQuery.length()-1);
        //strQuery+=' from '+objectAPIName;
        return strQuery;
    }
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String objectName){     
        Schema.DescribeSObjectResult describeSObjectResultObj = Schema.getGlobalDescribe().get(objectName).getDescribe();
        Schema.FieldSet fieldSetObj = describeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    } 

}