public class CustomerCommunity_ExcelParserController {
    @AuraEnabled
    public static List<BoL_Excel_Parser__mdt> getFieldLabels(){
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_ExcelParserController:getFieldLabels()'); 
        List<BoL_Excel_Parser__mdt> listExcelParser = [SELECT MasterLabel, Ending_Label__c, Traverse_Path__c, Active__c, Object_API_Name__c, Parent_Object_API_Name__c,
                                                       Field_Data__c, Sequence__c, Field_API_Name__c, Field_Data_Type__c, Type__c 
                                                       FROM BoL_Excel_Parser__mdt WHERE Active__c = True Order By Sequence__c ASC];
        if(listExcelParser != Null && !listExcelParser.isEmpty()){
            System.debug(LoggingLevel.INFO,'Exiting from CustomerCommunity_ExcelParserController:getFieldLabels() returning--'+listExcelParser);
            return listExcelParser;
        }
        else
            return Null;
    }
    @AuraEnabled
    public static void createBLRecords(List<BoL_Excel_Parser__mdt> listFormData){
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_ExcelParserController:createBLRecords() with parameters ' + listFormData);
        Map<String, List<BoL_Excel_Parser__mdt>> mapObjectFieldDetails = new Map<String, List<BoL_Excel_Parser__mdt>>();
        Map<String, List<BoL_Excel_Parser__mdt>> mapTypeFieldDetails = new Map<String, List<BoL_Excel_Parser__mdt>>();
        Map<String, Id> mapParentRecordIds = new Map<String, Id>();
        List<sObject> listObject = new List<sObject>();
        for(BoL_Excel_Parser__mdt fieldData : listFormData){
            List<BoL_Excel_Parser__mdt> listfieldDetails = new List<BoL_Excel_Parser__mdt>();
            List<BoL_Excel_Parser__mdt> listTypeFieldDetails = new List<BoL_Excel_Parser__mdt>();
            if(fieldData.Type__c != Null && fieldData.Type__c != '' && !mapTypeFieldDetails.isEmpty() && mapTypeFieldDetails.containsKey(fieldData.Type__c)){
                listTypeFieldDetails = mapTypeFieldDetails.get(fieldData.Type__c);
                listTypeFieldDetails.add(fieldData);
            }
            else if(fieldData.Type__c != Null && fieldData.Type__c != '')
                listTypeFieldDetails.add(fieldData);
            else if(!mapObjectFieldDetails.isEmpty() && mapObjectFieldDetails.containsKey(fieldData.Object_API_Name__c)){
                listfieldDetails = mapObjectFieldDetails.get(fieldData.Object_API_Name__c);
                listfieldDetails.add(fieldData);
            }
            else
                listfieldDetails.add(fieldData);
            if(!listTypeFieldDetails.isEmpty())
                mapTypeFieldDetails.put(fieldData.Type__c, listTypeFieldDetails);
            if(!listfieldDetails.isEmpty())
                mapObjectFieldDetails.put(fieldData.Object_API_Name__c, listfieldDetails);
        }
        if(!mapObjectFieldDetails.isEmpty()){
            listObject = new List<sObject>();
            for(String objectAPIName : mapObjectFieldDetails.keySet()){
                sObject objectRecord = Schema.getGlobalDescribe().get(objectAPIName).newSObject();
                for(BoL_Excel_Parser__mdt fieldDetail : mapObjectFieldDetails.get(objectAPIName)){
                    if(fieldDetail.Field_Data__c.contains('\n'))
                        fieldDetail.Field_Data__c = fieldDetail.Field_Data__c.replaceAll('\n',' ');
                    if(fieldDetail.Field_Data_Type__c == 'Text')
                        objectRecord.put(fieldDetail.Field_API_Name__c, String.valueOf(fieldDetail.Field_Data__c));
                    if(fieldDetail.Field_Data_Type__c == 'Number')
                        objectRecord.put(fieldDetail.Field_API_Name__c, Integer.valueOf(fieldDetail.Field_Data__c));
                    if(fieldDetail.Parent_Object_API_Name__c != Null && fieldDetail.Parent_Object_API_Name__c != '' && !mapParentRecordIds.isEmpty() && mapParentRecordIds.containsKey(fieldDetail.Parent_Object_API_Name__c))
                        objectRecord.put(fieldDetail.Parent_Object_API_Name__c, mapParentRecordIds.get(fieldDetail.Parent_Object_API_Name__c));
                }
                listObject.add(objectRecord);
            }
            if(!listObject.isEmpty())
                insert listObject;
            mapParentRecordIds = addParentIdInMap(listObject, mapParentRecordIds);
        }
        //insert address records
        if(!mapTypeFieldDetails.isEmpty()){
            listObject = new List<sObject>();
            for(String addressType : mapTypeFieldDetails.keyset()){
                sObject objectRecord = Schema.getGlobalDescribe().get(mapTypeFieldDetails.get(addressType)[0].Object_API_Name__c).newSObject();
                for(BoL_Excel_Parser__mdt fieldDetail : mapTypeFieldDetails.get(addressType)){
                    objectRecord.put('Type__c', String.valueOf(fieldDetail.Type__c));
                    String fieldValue = fieldDetail.Field_Data__c;
                    if(fieldValue.contains('\n') && fieldValue.indexOf('\n',0) == 0)
                        fieldValue = fieldValue.substring(1, fieldValue.length());
                    if(fieldDetail.Field_API_Name__c == 'Address__c'){
                        if(fieldValue.contains('\n')){
                            objectRecord.put('Name', fieldValue.substring(0,fieldValue.indexOf('\n', 0)));   
                            objectRecord.put(fieldDetail.Field_API_Name__c, fieldValue.substring(fieldValue.indexOf('\n', 0)));
                        }
                        else
                            objectRecord.put(fieldDetail.Field_API_Name__c, fieldValue);
                    }
                    else if(fieldDetail.Field_Data_Type__c == 'Text')
                        objectRecord.put(fieldDetail.Field_API_Name__c, String.valueOf(fieldValue));
                    else if(fieldDetail.Field_Data_Type__c == 'Number')
                        objectRecord.put(fieldDetail.Field_API_Name__c, Integer.valueOf(fieldValue));
                    if(fieldDetail.Parent_Object_API_Name__c != Null && fieldDetail.Parent_Object_API_Name__c != '' && !mapParentRecordIds.isEmpty() && mapParentRecordIds.containsKey(fieldDetail.Parent_Object_API_Name__c))
                        objectRecord.put(fieldDetail.Parent_Object_API_Name__c, mapParentRecordIds.get(fieldDetail.Parent_Object_API_Name__c));
                }
                listObject.add(objectRecord);
            }
            if(!listObject.isEmpty())
                insert listObject;
            mapParentRecordIds = addParentIdInMap(listObject, mapParentRecordIds);
        }
        System.debug(LoggingLevel.INFO,'Exiting from CustomerCommunity_ExcelParserController:createBLRecords()');
    }
    public static Map<String, Id> addParentIdInMap(List<sObject> listsObjects, Map<String, Id> mapParentRecordIds){
        if(!listsObjects.isEmpty()){
            for(sObject objRecord : listsObjects){
                String sObjName = objRecord.Id.getSObjectType().getDescribe().getName();
                if(mapParentRecordIds != Null && !mapParentRecordIds.containsKey(sObjName))
                    mapParentRecordIds.put(sObjName, objRecord.Id);
            }
        }
        return mapParentRecordIds;
    }
    @AuraEnabled
    public static void createFileBooking(String fileName, String base64Data, String contentType){
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        ContentVersion cv = new ContentVersion();
        cv.Title = fileName;
        cv.VersionData = EncodingUtil.base64Decode(base64Data);
        cv.PathOnClient = fileName;
        insert cv;
        
        List<ContentVersion> listcv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ID = :cv.Id];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = listcv[0].ContentDocumentId;
        cdl.LinkedEntityId = 'a060t000002lETw';
        cdl.ShareType = 'V';
        insert cdl;
        System.debug('@@@ cdl Id - ' + listcv[0].ContentDocumentId);
    }
}