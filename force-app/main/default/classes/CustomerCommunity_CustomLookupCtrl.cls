public without sharing class  CustomerCommunity_CustomLookupCtrl {
    
	// Used in Mobile community
    @AuraEnabled
    public static List<String> fetchPorts(String searchKeyWord, String shipmentType){ 
        List<String> portList = new List<String>(); 
        String searchKey = '%' +searchKeyWord.toUpperCase() + '%';
        if(shipmentType.equalsIgnoreCase('FCL')){
            List<FCL_Ports__mdt> list_FCLOptions = new List<FCL_Ports__mdt>();
            list_FCLOptions = [SELECT Id, MasterLabel FROM FCL_Ports__mdt WHERE MasterLabel LIKE :searchKey ORDER BY MasterLabel LIMIT 5];
            for(FCL_Ports__mdt port : list_FCLOptions){
                portList.add(port.MasterLabel);
            }
            return portList;
        } else {
            List<LCL_Ports__mdt> list_LCLOptions = new List<LCL_Ports__mdt>();
            list_LCLOptions = [SELECT Id, MasterLabel FROM LCL_Ports__mdt WHERE MasterLabel LIKE :searchKey ORDER BY MasterLabel LIMIT 5];
            for(LCL_Ports__mdt port : list_LCLOptions){
                portList.add(port.MasterLabel);
            }
            return portList;
        }
    }
    // Used in Desktop Community
    @AuraEnabled
    public static List<Address__c> fetchBusinessLocation(String searchKeyWord,String companyName){
        String searchKey = searchKeyWord.toUpperCase();
        List<Address__c> listBusinessLoction = new List<Address__c>();
        List<Address__c> listFilterBusinessLocations = new List<Address__c>();
     
        try{
            listFilterBusinessLocations = [SELECT id,Name,Account_Name__c,Country__c,State_Picklist__c,City__c,Postal_Code__c FROM Address__c Where Account_Name__c LIKE :companyName];
            
            System.debug('listAddress@@@'+listFilterBusinessLocations);
            for(Address__c addressRecord : listFilterBusinessLocations){   
                if(String.valueOf(addressRecord.Name).toUpperCase().contains(searchKey) || String.valueOf(addressRecord.City__c).toUpperCase().contains(searchKey)){
                   listBusinessLoction.add(addressRecord);
                }
            }
            System.debug('listBusinessLoction@@@'+listBusinessLoction);
            return listBusinessLoction;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;    
        }  
    }
    // Used in Desktop Community
    @AuraEnabled
    public static List<Account> fetchAccounts(String searchKeyWord){
        List<Account> listAccount = new List<Account>();
        String searchKey = '' +searchKeyWord.toUpperCase() + '%';
        try{
            List<Account> listAccounts = [Select id,Name FROM Account WHERE Name LIKE :searchKey ORDER BY NAME LIMIT 25];
            System.debug('listAccounts@@@'+listAccounts);
            for(Account AccountRecord : listAccounts){
                listAccount.add(AccountRecord);
            }
            System.debug('listAccount@@@'+listAccount);
            return listAccount; 
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;    
        }  
    }
    @AuraEnabled
    public static List<Account> fetchAccountsBooking(String searchKeyWord){
        List<Account> listAccount = new List<Account>();
        String searchKey = '' +searchKeyWord.toUpperCase() + '%';
        try{
            List<Account> listAccounts = [Select id,Name, CVIF__c FROM Account WHERE Name LIKE :searchKey ORDER BY NAME LIMIT 25];
            System.debug('listAccounts@@@'+listAccounts);
            for(Account AccountRecord : listAccounts){
                listAccount.add(AccountRecord);
            }
            System.debug('listAccount@@@'+listAccount);
            return listAccount; 
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;    
        }  
    }
    @AuraEnabled
    public static Address__c fetchLocation(String contactId){
        Address__c addressRecord = new Address__c();
        try{
            Contact contactRecord = new Contact();
            contactRecord = [Select id,Address__c from Contact where id = :contactId];
            if(contactRecord != NULL){
                addressRecord = [Select id,Name,Address_Line_2__c,toLabel(Country__c),toLabel(State_Picklist__c),City__c,Postal_Code__c FROM Address__c where id = :contactRecord.Address__c];
            }
            return addressRecord;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;    
        }           
    }
    @AuraEnabled
    public static Address__c fetchLocationDetails(String searchKeyWord){
        Address__c addressRecord = new Address__c();
        try{
            addressRecord = [Select id,Name,Address_Line_2__c,toLabel(Country__c),toLabel(State_Picklist__c),City__c,Postal_Code__c FROM Address__c where Name LIKE :searchKeyWord];
            return addressRecord;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;    
        }    
    }
     // Used in Desktop Community
    @AuraEnabled
    public static List<String> fetchCountries(String searchKeyWord){
        String searchKey = searchKeyWord.toUpperCase();
        List<String> listCountries = new List<String>(); 
        try{  
            listCountries = CrowleyCommunity_RegisterController.getCountries(searchKey);
            return listCountries;
        }
        catch(Exception ex){
           ExceptionHandler.logApexCalloutError(ex);
           return null; 
        }   
    }
    @AuraEnabled
     public static List<String> fetchStates(String searchKeyWord){
        String searchKey = searchKeyWord.toUpperCase();
        List<String> listStates = new List<String>(); 
        try{  
            listStates = CrowleyCommunity_RegisterController.getStates(searchKey);
            return listStates;
        }
        catch(Exception ex){
           ExceptionHandler.logApexCalloutError(ex);
           return null; 
        }   
    }
    
    @AuraEnabled 
    public static List<Ports_Information__c> getLocationDetails(String termCode, String locCode){
        System.debug(LoggingLevel.INFO,'Entering to getLocationDetails() with parameters'); 
        String searchKey = '%' +locCode.toUpperCase() + '%';
        List<Ports_Information__c> listPortsInfo = new List<Ports_Information__c>();
        try {
            if(termCode == 'D'){
                listPortsInfo = [SELECT Id, Place_Name__c, State__c, Country__c, Zip_Code__c, Softship_Zipcodes__c FROM Ports_Information__c WHERE Zip_Code__c LIKE :searchKey LIMIT 5];
            }
            if(termCode == 'P'){
                listPortsInfo = [SELECT Id, Place_Name__c, State__c, Country__c, Location_Code__c FROM Ports_Information__c WHERE Location_Code__c LIKE :searchKey AND RecordType.DeveloperName ='Location_Code' LIMIT 5];
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        return listPortsInfo;
    }
}