public with sharing class CustomerCommunity_FAQChatbot {
    public class FAQSearchInput {
        @InvocableVariable(required=true)
        public String sKeyword;
    }
    
    public class FAQSearchOutput {
        @InvocableVariable(required=true)
        public String sFAQSearchResult;
    }
    
    @InvocableMethod(label='Search FAQ')
    public static List<FAQSearchOutput> searchFAQ(List<FAQSearchInput> faqSearchInput) {
        String sKeyword = faqSearchInput[0].sKeyword;
        List<FAQSearchOutput> faqSearchOutputs = new List<FAQSearchOutput>();
        String sFAQSearchResult = CustomerCommunity_Constants.EMPTY_STRING;
        String sArticleBaseUrl = Label.CustomerCommunity_BaseURL;
        String sQuery = CustomerCommunity_Constants.EMPTY_STRING;
        FAQSearchOutput faqSearchOutput = new FAQSearchOutput();
        try{
            if(sKeyword.contains(CustomerCommunity_Constants.SINGLEQUOTE_CHARACTER)){
                sKeyword = sKeyword.replaceAll(CustomerCommunity_Constants.SINGLEQUOTE_CHARACTER,CustomerCommunity_Constants.EMPTY_STRING);
            }
            sQuery = CustomerCommunity_Constants.FIND_SOSL_STRING + sKeyword + CustomerCommunity_Constants.KNOWLEDGE_SOSL_STRING;
            Search.SearchResults searchResults = Search.find(sQuery);
            List<Search.SearchResult> articlelist = searchResults.get(CustomerCommunity_Constants.KNOWLEDGE_STRING);
            for (Search.SearchResult searchResult : articlelist) {
                KnowledgeArticleVersion article = (KnowledgeArticleVersion)searchResult.getSObject();
                sFAQSearchResult = sFAQSearchResult + summarizeArticleForBot(sArticleBaseUrl, article);
            }
            if (sFAQSearchResult == CustomerCommunity_Constants.EMPTY_STRING)
                sFAQSearchResult = CustomerCommunity_Constants.NO_RESULT_STRING;
            faqSearchOutput.sFAQSearchResult = sFAQSearchResult;
            faqSearchOutputs.add(faqSearchOutput);
            return faqSearchOutputs;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            faqSearchOutput.sFAQSearchResult = CustomerCommunity_Constants.NO_RESULT_STRING;
            faqSearchOutputs.add(faqSearchOutput);
            return faqSearchOutputs;
        }
    }
    public static String summarizeArticleForBot(String sArticleBaseUrl, KnowledgeArticleVersion article) {
        String sSummary, sURL,knowledgeURL;
        knowledgeURL = sArticleBaseUrl+Label.CustomerCommunity_ArticleDetailUrl+article.KnowledgeArticleId;
        sSummary = CustomerCommunity_Constants.ARTICLE_TITLE + article.Title + CustomerCommunity_Constants.NEWLINE_CHARACTER + '...' +CustomerCommunity_Constants.NEWLINE_CHARACTER + knowledgeURL + CustomerCommunity_Constants.NEWLINE_CHARACTER + CustomerCommunity_Constants.NEWLINE_CHARACTER;
        return sSummary;
    }
}