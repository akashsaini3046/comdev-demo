@IsTest
public class ContactTriggerHandlerTest {
	
    @isTest private static void ContactTriggerHandler_TestMethod(){    
        
        
        ProfileNames__c p1 = new ProfileNames__c();
        p1.Name = 'Sales Leader';
        insert p1;
        
        ProfileNames__c p2 = new ProfileNames__c();
        p2.Name = 'System Administrator';
        insert p2;
        
        Account accObj1 = new Account();
        accObj1.Name = 'Account 123';
        accObj1.Created_From_Lead_Conversion__c = TRUE;
        insert accObj1;
        
        GovernanceTeamEmail__c govEmail = new GovernanceTeamEmail__c();
        govEmail.Name = 'test@test.com';
        insert govEmail;
        
        DescribeFieldResult describe = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
                
        Address__c businessLocationObj = new Address__c();
        businessLocationObj.Account__c = accObj1.Id;
        businessLocationObj.Address_Line_1__c = 'address1';
        businessLocationObj.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj.Country__c = availableValues[0].getValue();
        businessLocationObj.Postal_Code__c = '1111111';
        businessLocationObj.Phone__c = '88888888888';
        businessLocationObj.State__c = 'State1';
        businessLocationObj.Name ='BL1';
        insert businessLocationObj;
        
        System.assertNotEquals(NULL, businessLocationObj.Country__c);
        
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        System.assertNotEquals(NULL, recordTypeId);
        
        Contact con= new Contact();
        con.LastName='Test con1';
        con.Phone = '99999999999';
        con.Email = 'contact@email.com';
        con.RecordTypeId = recordTypeId;
        con.Address__c = businessLocationObj.Id;
        con.Contact_Created_From_Lead__c = TRUE;
        insert con;
        
        System.assertEquals(businessLocationObj.Id, con.Address__c);
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Update standardPricebook;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = accObj1.Id;
        opp.Name = 'Opp1';
        opp.Service_Type__c = 'Air';
        opp.CloseDate = System.today();
        opp.StageName='Prospecting';
        opp.Contact__c = con.Id;
        insert opp;
        
        List<Contact> conListToDelete = new List<Contact>();
        conListToDelete.add(con);
        
        test.startTest();
        System.assertNotEquals(NULL, opp);
        try
        {
            delete conListToDelete;
        }
        catch(Exception e)
        {
            System.debug('Exception in Contact Test Class'+e.getMessage());
        }
        test.stopTest();
        
    }
}