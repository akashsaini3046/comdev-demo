/*
* Name: NeglectedAccountReport
* Purpose: For deleting account active records.
* Author: Nagarro
* Created Date: 30/04/2020
* 
*  Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*
*/
public without Sharing class NeglectedAccountReport {
	
    public static void deleteNeglectedAccountRecord(Set<Id> sObjectIds){
        List<Neglected_Account_Data__c> negDatatoDelList	=	[Select Id from Neglected_Account_Data__c where AccountId__c IN: sObjectIds];
        
        if(negDatatoDelList.size()>0){
            try{
                delete negDatatoDelList;
            }catch(Exception e){
                System.debug('Error: '+ e.getMessage() );
                System.debug('Error line number: ' + e.getLineNumber());
            } 
            
        }

    }
    
}