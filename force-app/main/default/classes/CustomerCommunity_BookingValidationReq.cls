public class CustomerCommunity_BookingValidationReq {
		@AuraEnabled public Body Body{get;set;}
	public class ValidateBooking{
		@AuraEnabled public request request{get;set;}
	}
	public class request{
		@AuraEnabled public Booking Booking{get;set;}
	}
	public class Packages{
		@AuraEnabled public list<DGBookingPackage> DGBookingPackage{get;set;}
	}
	public class DGCTU{
		@AuraEnabled public String UnitType{get;set;}
		@AuraEnabled public Packages Packages{get;set;}
	}
	public class DGBookingPackage{
		@AuraEnabled public BookingItems BookingItems{get;set;}
	}
	public class DGBookingItem{
		@AuraEnabled public String UNNo{get;set;}
		@AuraEnabled public String TechnicalName{get;set;}
		@AuraEnabled public String GrossWeightKG{get;set;}
		@AuraEnabled public String Suffix{get;set;}
		@AuraEnabled public String EmergencyContactNumber{get;set;}
		@AuraEnabled public String EmergencyContactName{get;set;}
	}
	public class CTUs{
		@AuraEnabled public list<DGCTU> DGCTU{get;set;}
	}
	public class BookingItems{
		@AuraEnabled public list<DGBookingItem> DGBookingItem{get;set;}
	}
	public class Booking{
		@AuraEnabled public String IMDGAmdt{get;set;}
		@AuraEnabled public CTUs CTUs{get;set;}
		@AuraEnabled public String BookingRef{get;set;}
	}
	public class Body{
		@AuraEnabled public ValidateBooking ValidateBooking{get;set;}
	}
}