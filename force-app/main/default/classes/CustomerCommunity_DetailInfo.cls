public class CustomerCommunity_DetailInfo {
    @AuraEnabled public String voyageNumber{get; set;}
    @AuraEnabled public String vesselName {get; set;}
    @AuraEnabled public String transitTime {get; set;}
    @AuraEnabled public String transitPeriod {get; set;}
    @AuraEnabled public String originPort {get; set;}
    @AuraEnabled public String destinationPort {get; set;}
    @AuraEnabled public String startDate {get; set;}
    @AuraEnabled public String arrivalDate {get; set;}
    @AuraEnabled public String startDay {get; set;}
    @AuraEnabled public String arrivalDay {get; set;}
}