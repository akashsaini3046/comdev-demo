public class CustomerCommunity_ShipmentVoyageInfo {
    @AuraEnabled public String numberInfo{get; set;}
    @AuraEnabled public String vessel{get; set;}
}