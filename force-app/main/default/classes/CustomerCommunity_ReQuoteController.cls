public with sharing class CustomerCommunity_ReQuoteController {
    
    @AuraEnabled
    public static List<QuoteWrapper> fetchQuoteData(){
        List<QuoteWrapper> allQuotes = new List<QuoteWrapper>();
        Id quoteRecordTypeId = getRecordTypeId('Booking__c:Quote_Record');
        List<Quote__c> quotes = [SELECT Id, Name, Container_Mode__c, Origin_Type__c, Destination_Type__c, Is_Booked__c, Status__c, Destination_Code__c, Origin_Code__c FROM Quote__c]; 
        for(Quote__c quote : quotes){
            QuoteWrapper quoteWrapper = new QuoteWrapper();
            quoteWrapper.quoteNumber = quote.Name;
            quoteWrapper.conatinerMode = quote.Container_Mode__c;
            quoteWrapper.originType = quote.Origin_Type__c;
            quoteWrapper.origin = quote.Origin_Code__c;
            quoteWrapper.destinationType = quote.Destination_Type__c;
            quoteWrapper.destination = quote.Destination_Code__c;
            quoteWrapper.quoteStatus = quote.Status__c;
            allQuotes.add(quoteWrapper);
        }
        
        List<Booking__c> quotesInBooking = [SELECT Quote_Number__c, Customer_Origin_Code__c, Customer_Destination_Code__c, 
                                            Container_Mode__c, Status__c
                                            FROM Booking__c WHERE RecordTypeId = :quoteRecordTypeId];
        for(Booking__c quote : quotesInBooking){
            QuoteWrapper quoteWrapper = new QuoteWrapper();
            quoteWrapper.quoteNumber = quote.Quote_Number__c;
            quoteWrapper.conatinerMode = 'FCL';
            quoteWrapper.originType = '';
            quoteWrapper.origin = quote.Customer_Origin_Code__c;
            quoteWrapper.destinationType = '';
            quoteWrapper.destination = quote.Customer_Destination_Code__c;
            quoteWrapper.quoteStatus = quote.Status__c;
            allQuotes.add(quoteWrapper);
        }
        return allQuotes;
    } 
    
    @AuraEnabled
    public static List<QuoteWrapper> fetchQuoteDataSorted(String fieldName, String order){
        List<QuoteWrapper> quoteWrapperList = fetchQuoteData();
        return quoteWrapperList;
    }
    
    public static Id getRecordTypeId(String recTypeName){
        if (recordTypesMap!= null && recordTypesMap.containsKey(recTypeName)){
            return recordTypesMap.get(recTypeName);
        }
        return null;
    }
    
    private static Map<String, ID> recordTypesMap{
        get{
            if (recordTypesMap == null ){
                recordTypesMap = new Map<String, Id>();
                for (RecordType aRecordType : [SELECT SobjectType, DeveloperName FROM RecordType WHERE isActive = true]){
                    recordTypesMap.put(aRecordType.SobjectType + ':' + aRecordType.DeveloperName, aRecordType.Id);
                }
            }
            return recordTypesMap;
        }
        set;
    }
    
    public class QuoteWrapper{
        @AuraEnabled public String quoteNumber;
        @AuraEnabled public String conatinerMode;
        @AuraEnabled public String originType;
        @AuraEnabled public String origin;
        @AuraEnabled public String destinationType;
        @AuraEnabled public String destination;
        @AuraEnabled public String quoteStatus;
    }
}