/*
* Name: ADTasksController
* Purpose: Controller class for Task component
* Author: Nagarro
* Created Date: 08-Jan-2019
* Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*/
public class ADTasksController {
    
    @AuraEnabled
    public Integer totalTaskCount {get;set;}
    @AuraEnabled
    public Integer openTaskCount {get;set;}
    @AuraEnabled
    public Integer closedTaskCount {get;set;}
    @AuraEnabled
    public Integer overdueTaskCount {get;set;}
    @AuraEnabled
    public List<Task> taskList {get;set;}
    @AuraEnabled
    public List<Id> ownerIdList {get;set;}
    
    //Task list to display on task summary section based on the open and completed tasks having activity date.
    @AuraEnabled
    public List<Task> taskListTemp {get;set;}
    
    /*
* Method Name: fetchTasks
* Input Parameters:
* Id accountId: This holds parent/current account Id.
* ADMasterFilterWrapper objMasterFilter: This is filter warapper which holds filter parameters.
* Return value: ADTasksController
* Purpose: This method fetches all the tasks related to account hierarchy or opportunities.
*/    
    @AuraEnabled
    public static ADTasksController fetchTasks(Id accountId, ADMasterFilterWrapper objMasterFilter)
    {
        ADTasksController taskControllerObj = new ADTasksController();
        
        // Set<Id> ownerIdSet = new Set<Id>();
        taskControllerObj.ownerIdList = new List<Id>();
        
        //Filter accounts
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(ADAccountDashboardUtil.getAllRelatedAccount(accountId), objMasterFilter);
        
        //Get related opportunities and apply filter on those
        ADMasterFilterWrapper objMasterFilterTemp = new ADMasterFilterWrapper();
        objMasterFilterTemp.lstSelectedAccounts = objMasterFilter.lstSelectedAccounts;
        objMasterFilterTemp.lstSelectedOwners = objMasterFilter.lstSelectedOwners;
        objMasterFilterTemp.strSelectedTopFilter = objMasterFilter.strSelectedTopFilter;
        objMasterFilterTemp.strSelectedOppSizeFilterOperator = objMasterFilter.strSelectedOppSizeFilterOperator;
        objMasterFilterTemp.strOppSizeVal = objMasterFilter.strOppSizeVal;
        objMasterFilterTemp.strSelectedYear = null;
        List<Opportunity> opportunityList = ADAccountDashboardUtil.applyOpportunityFilter(ADAccountDashboardUtil.getAllRelatedOpportunitiesWithoutFilter(lstFilteredAccounts), objMasterFilterTemp);
        
        //Get tasks related to account or opportunity
        if((lstFilteredAccounts != NULL && !lstFilteredAccounts.isEmpty()) || (opportunityList != NULL && !opportunityList.isEmpty()))
        {
            if(objMasterFilter.strSelectedYear == Label.AD_PICKLIST_VAL_ALL_YEAR) 
                taskControllerObj.taskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where (WhatId in: opportunityList OR WhatId in: lstFilteredAccounts) ORDER BY ActivityDate DESC NULLS LAST];
            else
                taskControllerObj.taskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where (WhatId in: opportunityList OR WhatId in: lstFilteredAccounts ) AND (ActivityDateYear__c = :objMasterFilter.strSelectedYear  OR ActivityDate = null)ORDER BY ActivityDate DESC NULLS LAST];
        }
        
        //Initializing the variables
        taskControllerObj.openTaskCount = 0;
        taskControllerObj.closedTaskCount = 0;
        taskControllerObj.overdueTaskCount = 0;
        taskControllerObj.totalTaskCount = 0;
        
        if(taskControllerObj.taskList == NULL || taskControllerObj.taskList.isEmpty())
            taskControllerObj.totalTaskCount = 0;
        else
            taskControllerObj.totalTaskCount = taskControllerObj.taskList.size(); //Total number of tasks
        
        if(taskControllerObj.taskList != NULL && !taskControllerObj.taskList.isEmpty())
        {
            for(Task t: taskControllerObj.taskList)
            {     
                if(t.Status == ConstantClass.TASK_STATUS_OPEN)
                {
                    taskControllerObj.openTaskCount ++; //Total number of open tasks
                    if(t.ActivityDate < System.today())
                        taskControllerObj.overdueTaskCount ++; //Total number of overdue tasks
                }
                if(t.Status == ConstantClass.TASK_STATUS_COMPLETED)
                {
                    taskControllerObj.closedTaskCount ++; //Total number of closed tasks
                }
                //Used to fetch only the related users in the multiselect lookup filter.
                if(t.OwnerId != NULL)
                {
                    taskControllerObj.ownerIdList.add(t.OwnerId);
                }
            }
        }
        
        Integer openTaskHavingDueDateCount = 0;
        List<Task> openTaskListHavingDueDate = new List<Task>();
        List<Task> openTaskListHavingNoDueDate = new List<Task>();
        List<Task> completedTaskListHavingDueDate = new List<Task>();
        List<Task> completedTaskListHavingNoDueDate = new List<Task>();
        taskControllerObj.taskListTemp = new List<Task>();
        
        if(taskControllerObj.taskList != NULL && !taskControllerObj.taskList.isEmpty())
        {
            for(Task t: taskControllerObj.taskList)
            {
                if(t.Status == ConstantClass.TASK_STATUS_OPEN && t.ActivityDate !=NULL)
                {
                    openTaskHavingDueDateCount++;
                    openTaskListHavingDueDate.add(t);
                    if(openTaskHavingDueDateCount>= 3)
                        break;
                }
                else if(t.Status == ConstantClass.TASK_STATUS_OPEN && t.ActivityDate ==NULL)
                {
                    openTaskListHavingNoDueDate.add(t);
                }
                else if(t.Status == ConstantClass.TASK_STATUS_COMPLETED && t.ActivityDate ==NULL)
                {
                    completedTaskListHavingDueDate.add(t);
                }
                else if(t.Status == ConstantClass.TASK_STATUS_COMPLETED && t.ActivityDate !=NULL)
                {
                    completedTaskListHavingNoDueDate.add(t);
                }
            }
        }
        
        if(openTaskListHavingDueDate.size() >= 3)
        {
            taskControllerObj.taskListTemp.addAll(openTaskListHavingDueDate);
        }
        else
        {
            taskControllerObj.taskListTemp.addAll(openTaskListHavingDueDate);
            taskControllerObj.taskListTemp.addAll(openTaskListHavingNoDueDate);
            taskControllerObj.taskListTemp.addAll(completedTaskListHavingDueDate);
            taskControllerObj.taskListTemp.addAll(completedTaskListHavingNoDueDate);
        }
        
        return taskControllerObj;
    }
    
    /*
* Method Name: applyFilter
* Input Parameters:
* Id accountId: This holds parent/current account Id.
* Date startDate: This holds the start date.
* Date endDate: This holds the end date.
* String dateFilter: This holds the selected date type (Due Date or Created Date).
* List<sObject> selectedOwners: This holds the list of owners selected in the multi select lookup component on the AllTasks component.
* List<Task> taskList: List of tasks related to account hierarchy or opportunities.
* Return value: List<Task>
* Purpose: 	There are 3 filter options on the component: Assigned To, Created Date and Due Date.
This method is called when Apply button is clicked on the component. Based on the users and date filter selected, filtered task list is returned.
*/    
    @AuraEnabled
    public static List<Task> applyFilter(Id accountId, Date startDate, Date endDate, String dateFilter, List<sObject> selectedOwners, List<Task> taskList) {
        Set<Id> selectedOwnerIds = new Set<Id>();
        if(selectedOwners != NULL && !selectedOwners.isEmpty())
        {
            for(sObject s: selectedOwners)
            {
                selectedOwnerIds.add(s.Id);
            }
        }
        Set<Id> taskIdSet = new Set<Id>();
        for(Task t: taskList)
        {
            taskIdSet.add(t.Id);
        }
        List<Task> filteredTaskList = new List<Task>();
        if(taskIdSet != NULL && !taskIdSet.isEmpty())
        {
            if(dateFilter == ConstantClass.DATE_FILTER_CREATED_DATE)
            {
                filteredTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet AND DAY_ONLY(CreatedDate)>=:startDate AND DAY_ONLY(CreatedDate)<=:endDate ORDER BY ActivityDate DESC NULLS LAST];
                if(selectedOwnerIds!= NULL && !selectedOwnerIds.isEmpty())
                {
                    filteredTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet AND DAY_ONLY(CreatedDate)>=:startDate AND DAY_ONLY(CreatedDate)<=:endDate AND OwnerId in: selectedOwnerIds ORDER BY ActivityDate DESC NULLS LAST];
                }
            }
            else if(dateFilter == ConstantClass.DATE_FILTER_DUE_DATE)
            {
                filteredTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet AND ActivityDate>=:startDate AND ActivityDate<=:endDate ORDER BY ActivityDate DESC NULLS LAST];
                if(selectedOwnerIds!= NULL && !selectedOwnerIds.isEmpty())
                {
                    filteredTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet AND ActivityDate>=:startDate AND ActivityDate<=:endDate AND OwnerId in: selectedOwnerIds ORDER BY ActivityDate DESC NULLS LAST];
                }
            }
            else if(selectedOwnerIds!=NULL && !selectedOwnerIds.isEmpty() && (dateFilter != '' || dateFilter != null))
            {
                filteredTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet AND OwnerId in: selectedOwnerIds ORDER BY ActivityDate DESC NULLS LAST];
            }
            else
            {
                return taskList;
            }
        }
        return filteredTaskList;
    }
    
    /*
* Method Name: applySorting
* Input Parameters: 
* String sortedDateOptionSelected: This holds the sorting option selected.
* List<Task> taskList: This holds the list of tasks currently displayed on the component.
* Return value: List<Task>
* Purpose: 	There are 2 sorting links on the component: Sort by Due Date, Sort by Created Date.
This method is called when either of the sorting link is clicked on the component. Based on the sorting link the sorted task list is returned.	
*/    
    @AuraEnabled
    public static List<Task> applySorting(String sortedDateOptionSelected, List<Task> taskList)
    {
        Set<Id> taskIdSet = new Set<Id>();
        if(taskList !=NULL && !taskList.isEmpty())
        {
            for(Task t: taskList)
            {
                taskIdSet.add(t.Id);
            }
        }
        List<Task> sortedTaskList = new List<Task>();
        
        if(taskIdSet != NULL && !taskIdSet.isEmpty())
        {
            if(sortedDateOptionSelected == ConstantClass.SORTING_DUE_DATE_ASC)
                sortedTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet ORDER BY ActivityDate ASC NULLS LAST];
            else if(sortedDateOptionSelected == ConstantClass.SORTING_CREATED_DATE_ASC)
                sortedTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet ORDER BY CreatedDate ASC NULLS LAST];
            else if(sortedDateOptionSelected == ConstantClass.SORTING_DUE_DATE_DESC)
                sortedTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet ORDER BY ActivityDate DESC NULLS LAST];
            else if(sortedDateOptionSelected == ConstantClass.SORTING_CREATED_DATE_DESC)
                sortedTaskList = [Select Id, Status, Description, Owner.Name, Subject, What.Type, What.Name, ActivityDate, CreatedDate from Task where Id in: taskIdSet ORDER BY CreatedDate DESC NULLS LAST];
        }
        return sortedTaskList;
    }
}