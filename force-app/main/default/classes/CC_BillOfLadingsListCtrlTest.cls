@isTest
public class CC_BillOfLadingsListCtrlTest {
    @testSetup static void setup() {
        List<Bill_Of_Lading__c> bolList = TestDataUtility.getBillOfLadingRecords('RR',5);
    }
    @isTest
    static void fetchBillOfLadingDataTest(){
        
        Test.startTest();
        List<Bill_Of_Lading__c> billOfLadings= CC_BillOfLadingsListController.fetchBillOfLadingData(5,0,'Name','asc');
        System.debug('billOfLadings @@@ '+billOfLadings);
        Test.stopTest();
        
        System.assertEquals(5, billOfLadings.size(), 'Size check');
    }
    
    @isTest
    static void getTotalNumberOfBillOfLadingsTest(){
        
        Test.startTest();
        Integer totalCount = CC_BillOfLadingsListController.getTotalNumberOfBillOfLadings();
        Test.stopTest();
        
        System.assertEquals(5, totalCount, 'Value Check');
    }
    
     @isTest
    static void getCommunityUrlPathPrefixTest(){
        
        Test.startTest();
        String pathPrefix = CC_BillOfLadingsListController.getCommunityUrlPathPrefix();
        Test.stopTest();
        
        System.assertEquals(null, pathPrefix, 'Value Check');
    }
    
}