public class ChatTranscriptTriggerHandler {
    public static void populateContact(List<LiveChatTranscript> listChatTranscript) {
        Set<String> setUsernames = new Set<String>();
        List<User> listUsers = new List<User>();
        Map<String, Id> mapUsernameContact = new Map<String, Id>();
        try{
            System.debug(listChatTranscript);
            for(LiveChatTranscript transcript : listChatTranscript){
                if(transcript.Username__c != Null && transcript.Username__c != CustomerCommunity_Constants.EMPTY_STRING)
                    setUsernames.add(transcript.Username__c);
            }
            System.debug(setUsernames);
            if(!setUsernames.isEmpty()){
                listUsers = [SELECT Id, Username, ContactId FROM User WHERE Username IN :setUsernames AND ContactId != Null];
                if(!listUsers.isEmpty()) {
                    for(User currentUser : listUsers){
                        mapUsernameContact.put(currentUser.Username, currentUser.ContactId);
                    }
                    if(!mapUsernameContact.isEmpty()){
                        for(LiveChatTranscript transcript : listChatTranscript){
                            if(transcript.Username__c != Null && transcript.Username__c != CustomerCommunity_Constants.EMPTY_STRING && 
                               mapUsernameContact.containsKey(transcript.Username__c) && mapUsernameContact.get(transcript.Username__c) != Null){
                                transcript.ContactId = mapUsernameContact.get(transcript.Username__c);
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
        }
    }
}