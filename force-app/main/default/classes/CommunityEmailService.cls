public without sharing class CommunityEmailService {
	public static void sendNotification_IdeaPost(List<Idea> ideasList){
        List<Messaging.SingleEmailMessage> emailMsgs = new List<Messaging.SingleEmailMessage>();
        Group grp = [Select Id, Name From Group Where Name='Idea Moderator Group' Limit 1];
        List<GroupMember> members = [Select Id, UserorGroupId From GroupMember Where GroupId=:grp.Id];
        System.debug('members -> '+members);
        for(Idea idea: ideasList){
            for(GroupMember member: members){
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                msg.setTemplateId(System.Label.New_Idea_Post_Template_Id);
                msg.setWhatId(idea.Id);
                msg.setTargetObjectId(member.UserorGroupId);
                msg.setSaveAsActivity(false);
                emailMsgs.add(msg);
            }
        }
        
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(emailMsgs);
        for(Messaging.SendEmailResult res: results){
            if(!res.isSuccess()){
                for(Messaging.SendEmailError err: res.getErrors()){
                    System.debug(err.getStatusCode() +' : '+ err.getMessage());
                }
            }
        } 
    }
    
    public static void sendNotification_IdeaImplemented(List<Idea> ideasList){
        
        List<Idea> implementedIdeas = new List<Idea>();
        List<Id> ideaIds = new List<Id>();
        for(Idea idea : ideasList){
            if(idea.Status == 'Implemented and Closed'){
                ideaIds.add(idea.Id);
                implementedIdeas.add(idea);
            }
        }
        
        if(!implementedIdeas.isEmpty()){
            System.debug('implemented ideas -> '+implementedIdeas);
            Id ideaId = ideaIds[0];
            Set<Id> userids = new Set<Id>();
            for(Vote voteObj : [Select Id, ParentId, LastModifiedById From Vote Where ParentId = :ideaId]){
                userids.add(voteObj.LastModifiedById);
            }
            List<Messaging.SingleEmailMessage> emailMsgs = new List<Messaging.SingleEmailMessage>();
            for(Idea idea: implementedIdeas){
                for(Id userId: userids){
                    Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                    msg.setTemplateId(System.Label.Idea_Delivered_Notification_Template_Id);
                    msg.setWhatId(idea.Id);
                    msg.setTargetObjectId(userId);
                    msg.setSaveAsActivity(false);
                    emailMsgs.add(msg);
                    System.debug(msg);
                }
                
            }
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(emailMsgs);
            for(Messaging.SendEmailResult res: results){
                if(!res.isSuccess()){
                    for(Messaging.SendEmailError err: res.getErrors()){
                        System.debug(err.getStatusCode() +' : '+ err.getMessage());
                    }
                }
            }
        }
    }
}