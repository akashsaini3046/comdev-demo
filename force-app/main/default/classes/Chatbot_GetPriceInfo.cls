public class Chatbot_GetPriceInfo {
    
    public class InputData{
        @InvocableVariable
        public String contactId;
        @InvocableVariable
        public String custPhonenumber;
        @InvocableVariable
        public String custEmailId;
        @InvocableVariable
        public String username;
        @InvocableVariable
        public String origin = 'Jacksonville';
        @InvocableVariable
        public String destination='San Juan';
        @InvocableVariable
        public String totalWeight;
        @InvocableVariable
        public String totalVolume;
        
        
    }
    @InvocableMethod
    public static void getPriceDetails(){
        String totalWeight='12.0';
        String totalVolume = '1.0';
        String origin = 'Jacksonville';
        String destination='San Juan';
        
        String originCode;
        String destinationCode;
        
        String estimateSailDate = (Date.today()+21).format();
        
        System.debug('origin: ' + origin);
        
        try{
            
            originCode = [Select Location_Code__c from Ports_Information__c Where Name=:origin].Location_Code__c ;
            destinationCode = [Select Location_Code__c from Ports_Information__c Where Name=:destination].Location_Code__c;
            
            System.debug('origincode: ' + originCode);
            System.debug('destinationCode: ' + destinationCode);
        }catch(Exception e){
            System.debug('error message: ' + e.getMessage() );
        }
        
        String body = '{"TotalWeightUnit":"KG"' + ','
            			+'"TotalWeight":' + '"' + totalWeight+ '"' + ','
        				+'"TotalVolumeUnit":"M3"' + ','
            			+'"TotalVolume":'+'"'+totalVolume+'"'+','
            			+'"shipments":'+'"{"'
            				+'"voyages":['+
            						'{'+
            							'"estimateSailDate":' + '"' +estimateSailDate+'"'+
            						'}'+'],'+
            				+'"PackingLineCollection":{'+
            							'"PackingLine":[]'+'},'
            				+'"originCode":'+'"' + originCode +'"' +','
            				+'"numberShipments":null'+','
            				+'"freightDetails":['+
         								'{'+
            								'"requirements":['+
               									'{'+
               									'}'+
            									'],'+
            								'"commodities":['+
               									'{'+
               									'}'+
            								']'+
         								'}'+
      								'],'+
            				'"destinationCode":'+'"'+destinationCode+'"'
            			+'},'
            			+'"party":{'
      							+'"shipper":{'
         						+'zip":"34669",'
         						+'"type":"SHP",'
         						+'"state":"FL",'
         						+'"refNumber":"",'
         						+'"name":"GALLERY SHIPPING LLC",'
         						+'"faxNumber":"",'
         						+'"cvifLocationCode":"01",'
         						+'"cvif":"12341234",'
         						+'"country":"US",'
         						+'"contactPhoneNumber":null,'
         						+'"contactName":"Roopal Kumar",'
         						+'"city":"HUDSON",'
         						+'"addressLine2":"",'
         						+'"addressLine1":"GALLERY SHIPPING LLC"'
      						+'},'
            
           					+'"customer":{'
         					+'"zip":"34669",'
         					+'"type":"CUST",'
         					+'"state":"FL",'
         					+'"refNumber":"",'
         					+'"name":"GALLERY SHIPPING LLC",'
         					+'"faxNumber":"",'
         					+'"cvifLocationCode":"01",'
         					+'"cvif":"12341234",'
         					+'"country":"US",'
         					+'"contactPhoneNumber":"17577791889",'
         					+'"contactName":"Canjura Rodrigo",'
         					+'"city":"HUDSON",'
         					+'"addressLine2":"",'
         					+'"addressLine1":"GALLERY SHIPPING LLC"'
      						+'},'
            			+'"consignee":{'
         				+'"zip":"34669",'
         				+'"type":"CON",'
         				+'"state":"FL",'
         				+'"refNumber":"",'
         				+'"name":"GALLERY SHIPPING LLC",'
         				+'"faxNumber":"",'
         				+'"cvifLocationCode":"01",'
         				+'"cvif":"12341234",'
         				+'"country":"US",'
         				+'"contactPhoneNumber":null,'
         				+'"contactName":"Sunil Dhamankar",'
         				+'"city":"HUDSON",'
         				+'"addressLine2":"",'
         				+'"addressLine1":"GALLERY SHIPPING LLC"'
            			+'}'
   						+'},'
            +'"originType":"T",'
   			+'"destinationType":"T",'
   			+'"ContainerMode":"LCL",'
   			+'"CarrierPath":null,'
   			+'"BookAndRate":true,'
            +'"AWBServiceLevel":"STD"'
            +'}';
        
        
        String apiUrl = 'https://sailingscheduleapi.azurewebsites.net/v2/sailingschedule/';

        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setMethod('POST');
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setHeader('Content-Length', '1538');
        httpRequest.setBody(body);
        httpRequest.setEndpoint(apiUrl);  
        try {
            Http http = new Http();   
            HttpResponse httpResponse = http.send(httpRequest);  
            if (httpResponse.getStatusCode() == 200 ) {
                System.debug('respone 200');
                System.debug(JSON.serializePretty(JSON.deserializeUntyped(httpResponse.getBody()) ));
                //responseList =(List<JSON2Apex>)JSON.deserialize(httpResponse.getBody(), List<JSON2Apex>.class);
                //System.debug('response: ' + responseList);
            } else {  
                System.debug(' httpResponse ' + httpResponse.getBody() );  
                throw new CalloutException( httpResponse.getBody() );  
            }   
        }catch( System.Exception e) {  
            System.debug('ERROR: '+ e);  
            throw e;  
        }
        
            }

}