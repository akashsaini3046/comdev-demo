public class CrowleyCommunity_UserDetail {
    @AuraEnabled public String userName {get;set;}
    @AuraEnabled public String userEmail {get;set;}
    @AuraEnabled public String firstName {get;set;}
    @AuraEnabled public String lastName {get;set;}
    @AuraEnabled public String phoneNumber {get;set;}
    @AuraEnabled public String countryName {get;set;}
}