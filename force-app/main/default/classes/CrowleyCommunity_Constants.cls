public class CrowleyCommunity_Constants {
    public static final String COMMUNITY_URL = '/CustomerCommunity/s/';
    public static final String TRUE_MESSAGE = 'True';
    public static final String FALSE_MESSAGE = 'False';
    public static final String ERROR_MESSAGE = 'Error'; 
    public static final String EXISTS_MESSAGE = 'Exists';
    public static final String CUSTOMER_LOGIN_PROFILE_NAME = 'Crowley Community Plus Login User Custom';
}