public class CustomerCommunity_VesselHeader {
		 @AuraEnabled public String VoyageNumber{get; set;}	//Voyage #
		 @AuraEnabled public String VesselCode{get; set;}	//Vessel
		 
		 @AuraEnabled public String TypeCode{get; set;}	//Type
		
		 @AuraEnabled public String StatusCode{get; set;}	//Status
		 
		 @AuraEnabled public String PortOfLoadingCode{get; set;}	//POL
		 @AuraEnabled public String PortOfLoadingCallId{get; set;}	//POL Call ID
		 
		 @AuraEnabled public String PortOfDischargeCode{get; set;}	//POD
		 @AuraEnabled public String PortOfDischargeCallId{get; set;}	//POD Call ID
		 @AuraEnabled public String CentralPortCode{get; set;}	//Centralport
		 @AuraEnabled public String PortCallId{get; set;}	//Call ID
		
		 @AuraEnabled public String StartDate{get; set;}	//Departure
		 @AuraEnabled public String EndDate{get; set;}	//Arrival
		 @AuraEnabled public String Duration{get; set;}	//Duration
		 @AuraEnabled public String HasTransshipment{get; set;}	//Direct
		 @AuraEnabled public String ServiceName{get; set;}	//Service
		 @AuraEnabled public String PortCode{get; set;}	//Code
		 @AuraEnabled public String PortName{get; set;}	//Port
		 @AuraEnabled public String ArrivalDate{get; set;}	//Arrival Date
		 @AuraEnabled public String SailingDate{get; set;}	//Sailing Date
	}