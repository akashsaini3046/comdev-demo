public class CustomerCommunity_QuoteRequest {
		@AuraEnabled public Body Body{get;set;}
	public class LTLRequestDetail{
		@AuraEnabled public String weight{get;set;}
		@AuraEnabled public String nmfcClass{get;set;}
	}
	public class LTLRateShipmentSimpleRequest{
		@AuraEnabled public details details{get;set;}
		@AuraEnabled public String originCountry{get;set;}
		@AuraEnabled public String originPostalCode{get;set;}
		@AuraEnabled public String destinationPostalCode{get;set;}
		@AuraEnabled public String shipmentDateCCYYMMDD{get;set;}
		@AuraEnabled public String destinationCountry{get;set;}
		@AuraEnabled public String shipmentID{get;set;}
		@AuraEnabled public String destinationstateProvince{get;set;}
		@AuraEnabled public String tariffName{get;set;}
		@AuraEnabled public String destinationcity{get;set;}
		@AuraEnabled public String destinationaddress2{get;set;}
		@AuraEnabled public String destinationaddress1{get;set;}
	}
	public class LTLRateShipmentSimple{
		@AuraEnabled public LTLRateShipmentSimpleRequest LTLRateShipmentSimpleRequest{get;set;}
	}
	public class details{
		@AuraEnabled public LTLRequestDetail LTLRequestDetail{get;set;}
	}
	public class Body{
		@AuraEnabled public LTLRateShipmentSimple LTLRateShipmentSimple{get;set;}
	}
}