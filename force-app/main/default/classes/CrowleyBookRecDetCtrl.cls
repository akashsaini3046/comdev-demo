public without sharing class CrowleyBookRecDetCtrl {
    
    @AuraEnabled
    public static Map<String,List<Map<String, String>>> getBookingDetail(Id bookingId, String selectedMenu){
        System.debug('bookingId  -----> '+bookingId);
        try {
            Map<String,List<Map<String,String>>> sectionNameVsFieldLabelValue = new Map<String,List<Map<String,String>>>();
            Map<String,Map<String,String>> sectionNameVsFieldsList =getFieldsDefinition(selectedMenu);
            Map<List<Id>,List<Map<String,String>>> shipmentIdsDataMap = new Map<List<Id>,List<Map<String,String>>>();
            Map<List<Id>,List<Map<String,String>>> freightDetailIdsDataMap = new Map<List<Id>,List<Map<String,String>>>();
            Map<List<Id>,List<Map<String,String>>> requirementIdsDataMap = new Map<List<Id>,List<Map<String,String>>>();
            Map<List<Id>,List<Map<String,String>>> transportIdsDataMap = new Map<List<Id>,List<Map<String,String>>>();
            List<Id> bookingIds = new List<Id>();
            List<Id> shippingIds = new List<Id> ();
            List<Id> freightDetailIds = new List<Id> ();
            List<Id> requirementIds = new List<Id> ();
            List<Id> transportIds = new List<Id>();
            bookingIds.add(bookingId);
            if(selectedMenu.equalsIgnoreCase('details')){
                sectionNameVsFieldLabelValue.put('Booking',((getSectionWiseData(sectionNameVsFieldsList,'bookingFields','Booking_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingServiceType',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsServiceType','Service_Type_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);      
                sectionNameVsFieldLabelValue.put('BookingIMType',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsIMType','IM_Type_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingTMSType',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsTMSType','TMS_Type_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingCustomerOrigin',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsCustomerOrigin','Customer_Origin_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingCustomerDestination',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsCustomerDestination','Customer_Destination_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingConnectingCarrier',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsConnectingCarrier','Connecting_Carrier_Detail_Internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingConnectAtLoc',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsConnectAtLoc','Connect_At_Loc_Detail_internal','Booking__c',bookingIds,'NA')).values())[0]);            
                sectionNameVsFieldLabelValue.put('BookingConnectToLoc',((getSectionWiseData(sectionNameVsFieldsList,'bookingFieldsConnectToLoc','Connect_To_Loc_Detail_internal','Booking__c',bookingIds,'NA')).values())[0]);   
            }
            if(selectedMenu.equalsIgnoreCase('parties')){
                sectionNameVsFieldLabelValue.put('Party',
                                                 ((getSectionWiseData
                                                   (sectionNameVsFieldsList,'partyFields','Party_Detail_Internal','Party__c',bookingIds,'Booking__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('transports') || selectedMenu.equalsIgnoreCase('stops')){
                //INSIDE TRANSPORT(inclusive)
                transportIdsDataMap = getSectionWiseData(sectionNameVsFieldsList,'transportFields','Transport_Detail_Internal','Transport__c',bookingIds,'Booking__c');
                sectionNameVsFieldLabelValue.put('Transport',(transportIdsDataMap.values())[0]);
                
                for(List<id> ids : transportIdsDataMap.keySet()){
                    transportIds.addAll(ids);
                }
            }
            if(selectedMenu.equalsIgnoreCase('stops')){
                sectionNameVsFieldLabelValue.put('Stop',((getSectionWiseData(sectionNameVsFieldsList,'stopFields','Stop_Detail_Internal','Stop__c',transportIds,'Transport__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('bookingRemarks')){
                sectionNameVsFieldLabelValue.put('BookingRemark',
                                                 ((getSectionWiseData
                                                   (sectionNameVsFieldsList,'bookingRemarkFields','Booking_Remarks_Detail_Internal','Booking_Remark__c',bookingIds,'Booking__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('shipment') || selectedMenu.equalsIgnoreCase('customerNotifications') || 
               selectedMenu.equalsIgnoreCase('voyages') || selectedMenu.equalsIgnoreCase('dockReceipts') || 
               selectedMenu.equalsIgnoreCase('freightDetails') || selectedMenu.equalsIgnoreCase('commodities') ||
               selectedMenu.equalsIgnoreCase('requirements') || selectedMenu.equalsIgnoreCase('equipments')){
                   shipmentIdsDataMap = getSectionWiseData(sectionNameVsFieldsList,'shipmentFields','Shipment_Detail_Internal','Shipment__c',bookingIds,'Booking__c');
                   sectionNameVsFieldLabelValue.put('Shipment',(shipmentIdsDataMap.values())[0]);
                   for(List<id> ids : shipmentIdsDataMap.keySet()){
                       shippingIds.addAll(ids);
                   }
               }
            if(selectedMenu.equalsIgnoreCase('customerNotifications')){
                sectionNameVsFieldLabelValue.put('CustomerNotification',
                                                 ((getSectionWiseData
                                                   (sectionNameVsFieldsList,'customerNotificationFields','Customer_Notification_Detail_Internal','Customer_Notification__c',shippingIds,'Shipment__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('voyages')){
                sectionNameVsFieldLabelValue.put('Voyage',
                                                 ((getSectionWiseData
                                                   (sectionNameVsFieldsList,'voyageFields','Voyage_Detail_Internal','Voyage__c',shippingIds,'Shipment__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('dockReceipts')){
                sectionNameVsFieldLabelValue.put('DockReceipt',
                                                 ((getSectionWiseData
                                                   (sectionNameVsFieldsList,'dockReceiptFields','Dock_Receipt_Detail_Internal','Dock_Receipt__c',shippingIds,'Shipment__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('freightDetails') || selectedMenu.equalsIgnoreCase('commodities') ||
               selectedMenu.equalsIgnoreCase('requirements') || selectedMenu.equalsIgnoreCase('equipments')){
                   freightDetailIdsDataMap = getSectionWiseData(sectionNameVsFieldsList,'freightDetailFields','Freight_Detail_Internal','FreightDetail__c',shippingIds,'Shipment__c');          
                   sectionNameVsFieldLabelValue.put('FreightDetail',(freightDetailIdsDataMap.values())[0]);
                   for(List<id> ids : freightDetailIdsDataMap.keySet()){
                       freightDetailIds.addAll(ids);
                   }
               }
            if(selectedMenu.equalsIgnoreCase('commodities')){
                sectionNameVsFieldLabelValue.put('Commodity',((getSectionWiseData(sectionNameVsFieldsList,'commodityFields','Commodity_Detail_Internal','Commodity__c',freightDetailIds,'Freight__c')).values())[0]);
            }
            if(selectedMenu.equalsIgnoreCase('requirements') || selectedMenu.equalsIgnoreCase('equipments')){
                requirementIdsDataMap = getSectionWiseData(sectionNameVsFieldsList,'requirementFields','Requirement_Detail_Internal','Requirement__c',freightDetailIds,'Freight__c');
                sectionNameVsFieldLabelValue.put('Requirement',(requirementIdsDataMap.values())[0]);
                for(List<id> ids : requirementIdsDataMap.keySet()){
                    requirementIds.addAll(ids);
                }
            }
            if(selectedMenu.equalsIgnoreCase('equipments')){
                sectionNameVsFieldLabelValue.put('Equipment',((getSectionWiseData(sectionNameVsFieldsList,'equipmentFields','Equipment_Detail_Internal','Equipment__c',requirementIds,'Requirement__c')).values())[0]);
            }
            return sectionNameVsFieldLabelValue;
            
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    
    public static String getStringQuery(List<Schema.FieldSetMember> fieldMemberList,String objectAPIName){
        
        String strQuery='select id ,';
        for(Schema.FieldSetMember fieldMember : fieldMemberList){
            if(fieldMember.getType()==Schema.DisplayType.Picklist){
                strQuery+='toLabel('+fieldMember.getFieldPath()+'),' ;
            }
            
            else{
                strQuery+=fieldMember.getFieldPath()+',' ;
            }                
        }
        strQuery=strQuery.substring(0,strQuery.length()-1);
        strQuery+=' from '+objectAPIName;
        return strQuery;
    }
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName){     
        Schema.DescribeSObjectResult DescribeSObjectResultObj = Schema.getGlobalDescribe().get(ObjectName).getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    } 
    
    @AuraEnabled
    public static Map<List<Id>,List<Map<String,String>>> getSectionWiseData(Map<String,Map<String,String>> sectionNameVsFieldsList,String sectionName,String fieldSetName,String objectAPIName,List<Id> parentIdsList,String parentObjectRelationName){
        
        try{
            Map<List<Id>,List<Map<String,String>>> idListRecordDataMap = new Map<List<Id>,List<Map<String,String>>>();
            Map<String,String> fieldNameVsFieldValue = new Map<String,String>();
            List<Map<String,String>> objectRecordsLabelValueList = new List<Map<String,String>>();
            List<Id> sectionIds = new List<Id>();
            String queryString;
            system.debug('----->>>>'+parentIdsList);
            if(parentObjectRelationName=='NA')
                queryString = getStringQuery(readFieldSet(fieldSetName,objectAPIName),objectAPIName) + ' where id IN : parentIdsList';
            else
                queryString = getStringQuery(readFieldSet(fieldSetName,objectAPIName),objectAPIName) +' where '+ parentObjectRelationName +' IN  : parentIdsList';
            System.debug('queryString '+queryString);
            List<sObject> queryResult=(Database.query(queryString));
            Map<String,Schema.SObjectField> mFields = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap();
            System.debug(queryResult);
            for(sObject objRec : queryResult){
                System.debug(sectionName + ' - '+sectionNameVsFieldsList);
                sectionIds.add(objRec.id);
                fieldNameVsFieldValue = new Map<String,String>();
                for(String field : sectionNameVsFieldsList.get(sectionName).keySet()){
                    System.debug(field);
                    Object value = objRec.get(field);
                    if(value!=null)                       
                        fieldNameVsFieldValue.put(sectionNameVsFieldsList.get(sectionName).get(field),getDisplayValue(value,mFields,field));
                    if(value==null)
                        fieldNameVsFieldValue.put(sectionNameVsFieldsList.get(sectionName).get(field),'');
                }
                objectRecordsLabelValueList.add(fieldNameVsFieldValue); 
            }
            idListRecordDataMap.put(sectionIds,objectRecordsLabelValueList);
            return idListRecordDataMap ;
        }
        catch(Exception e){
            return null;
        }
    }    
    
    
    private static String getDisplayValue(Object value , Map<String,Schema.SObjectField> mFields, String field){ 
        if(String.valueOf(value)=='false')
            return 'No';
        else if(String.valueOf(value)=='true')
            return 'Yes';
        Schema.SObjectField fieldDesc = mFields.get(field);
        if(fieldDesc.getDescribe().getType()==Schema.DisplayType.Date){
            Date d= Date.valueOf(value);
            String stringDate = d.day()+'/'+d.month()+'/'+d.year();
            return stringDate;
        }
        else if(fieldDesc.getDescribe().getType()==Schema.DisplayType.DateTime){
            String timeZone = String.valueOf(UserInfo.getTimeZone());
            DateTime dt = DateTime.valueOf(value);
            String stringDateTime = dt.format('yyyy/MM/dd HH:mm:ss',timeZone);
            return stringDateTime;
        }
        else{
            return String.valueOf(value);
        }          
    }
    
    @AuraEnabled
    public static Map<String,Map<String,String>> getFieldNameLabel(String fieldSetName, String ObjectName,String sectionName){
        Map<String,Map<String,String>> secNameVsField = new Map<String,Map<String,String>>();
        List<Schema.FieldSetMember> fieldMemberList = readFieldSet(fieldSetName,ObjectName);
        Map<String,String> fieldNameLabelMap = new Map<String,String>();
        for(Schema.FieldSetMember fieldMember : fieldMemberList){
            // System.debug(fieldMember.getFieldPath()+' : '+fieldMember.getLabel());
            fieldNameLabelMap.put(fieldMember.getFieldPath(),fieldMember.getLabel());
        }
        //  System.debug(fieldNameLabelMap);
        if(fieldNameLabelMap != null && !fieldNameLabelMap.isEmpty()){
            secNameVsField.put(sectionName, fieldNameLabelMap);
        }
        return secNameVsField;
    }
    
    @AuraEnabled
    //Return Map of sectionName and (Map of Field Name and Field Label)
    public static Map<String,Map<String,String>> getFieldsDefinition(String selectedMenu){   
        try {
            Map<String, Map<String,String>> sectionNameVsFieldsList = new Map<String, Map<String,String>>();
            if(selectedMenu.equalsIgnoreCase('details')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Booking_Detail_Internal','Booking__c','bookingFields'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Service_Type_Detail_Internal','Booking__c','bookingFieldsServiceType'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('IM_Type_Detail_Internal','Booking__c','bookingFieldsIMType'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('TMS_Type_Detail_Internal','Booking__c','bookingFieldsTMSType'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Customer_Origin_Detail_Internal','Booking__c','bookingFieldsCustomerOrigin'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Customer_Destination_Detail_Internal','Booking__c','bookingFieldsCustomerDestination'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Connecting_Carrier_Detail_Internal','Booking__c','bookingFieldsConnectingCarrier'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Connect_At_Loc_Detail_internal','Booking__c','bookingFieldsConnectAtLoc'));
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Connect_To_Loc_Detail_internal','Booking__c','bookingFieldsConnectToLoc'));
            }
            if(selectedMenu.equalsIgnoreCase('parties')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Party_Detail_Internal','Party__c','partyFields'));
            }
            if(selectedMenu.equalsIgnoreCase('transports') || selectedMenu.equalsIgnoreCase('stops')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Transport_Detail_Internal','Transport__c','transportFields'));
            }
            if(selectedMenu.equalsIgnoreCase('stops')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Stop_Detail_Internal','Stop__c','stopFields'));
            }
            if(selectedMenu.equalsIgnoreCase('bookingRemarks')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Booking_Remarks_Detail_Internal','Booking_Remark__c','bookingRemarkFields'));
            }
            if(selectedMenu.equalsIgnoreCase('shipment') || selectedMenu.equalsIgnoreCase('customerNotifications') || 
               selectedMenu.equalsIgnoreCase('voyages') || selectedMenu.equalsIgnoreCase('dockReceipts') || 
               selectedMenu.equalsIgnoreCase('freightDetails') || selectedMenu.equalsIgnoreCase('commodities') ||
               selectedMenu.equalsIgnoreCase('equipments') || selectedMenu.equalsIgnoreCase('requirements')){
                   sectionNameVsFieldsList.putAll(getFieldNameLabel('Shipment_Detail_Internal','Shipment__c','shipmentFields'));
               }
            if(selectedMenu.equalsIgnoreCase('customerNotifications')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Customer_Notification_Detail_Internal','Customer_Notification__c','customerNotificationFields'));
            }
            if(selectedMenu.equalsIgnoreCase('voyages')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Voyage_Detail_Internal','Voyage__c','voyageFields'));
            }
            if(selectedMenu.equalsIgnoreCase('dockReceipts')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Dock_Receipt_Detail_Internal','Dock_Receipt__c','dockReceiptFields'));
            }
            if(selectedMenu.equalsIgnoreCase('freightDetails') || selectedMenu.equalsIgnoreCase('commodities') ||
               selectedMenu.equalsIgnoreCase('requirements') || selectedMenu.equalsIgnoreCase('equipments')){
                   sectionNameVsFieldsList.putAll(getFieldNameLabel('Freight_Detail_Internal','FreightDetail__c','freightDetailFields'));
               }
            if(selectedMenu.equalsIgnoreCase('commodities')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Commodity_Detail_Internal','Commodity__c','commodityFields'));
            }
            if(selectedMenu.equalsIgnoreCase('requirements') || selectedMenu.equalsIgnoreCase('equipments')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Requirement_Detail_Internal','Requirement__c','requirementFields'));
            }
            if(selectedMenu.equalsIgnoreCase('equipments')){
                sectionNameVsFieldsList.putAll(getFieldNameLabel('Equipment_Detail_Internal','Equipment__c','equipmentFields'));
            }
            return sectionNameVsFieldsList ;  
        }
        catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    public static Map<String,String> fetchReceiptDeliveryTermsPickListValues(String fieldName){
        Map<String,String> pickLabelValueMap = new Map<String,String>();
        Map<String,List<Schema.PicklistEntry>> picklistSchema= new Map<String,List<Schema.PicklistEntry>>();
        Schema.sObjectType objType = New_Booking_Fields__mdt.getSObjectType();
        //Schema.sObjectType objType = objectName.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        //List<String> fieldsToGetValues=new List<String>();
        //String fieldName ='Receipt_Delivery_Terms__c' ;
        //fieldsToGetValues.add('Receipt_Delivery_Terms__c');
        if(fieldMap.containsKey(fieldName)){
            list<Schema.PicklistEntry> values =
                fieldMap.get(fieldName).getDescribe().getPickListValues();
            for(Schema.PicklistEntry p : values){
                pickLabelValueMap.put(p.getLabel(),p.getValue());
            }
        }
        System.debug(fieldName+' values :');
        System.debug(pickLabelValueMap);
        return pickLabelValueMap; 
    }
    
    @AuraEnabled
    public static Map<String, Object> getRatingDetail(Id bookingId){
        Map<String, Object> result = new Map<String, Object>();
        Booking__c booking = [SELECT Id, ChargeLineItem__c, Selected_Route_Id__c FROM Booking__c WHERE Id =: bookingId];
        if(booking.ChargeLineItem__c != null){
            result.put('chargeLineItems', (CustomerCommunity_RatingAPIResponse.ValuesGroup) JSON.deserialize(booking.ChargeLineItem__c, CustomerCommunity_RatingAPIResponse.ValuesGroup.class));
            result.put('selectedRouteId', booking.Selected_Route_Id__c);
        }
        return result;
    }
}