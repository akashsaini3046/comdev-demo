public class KnowledgeTriggerHandler {
    public static void sentEmailToRecordCreator(List<Knowledge__kav> listKnowledge){
        Set<ID> setUserIDs = new Set<ID>();
        List<Messaging.SingleEmailMessage> listmails =  new List<Messaging.SingleEmailMessage>();
        for(Knowledge__kav recordKnowledge: listKnowledge){
            System.debug('recordKnowledge :'+ recordKnowledge.PublishStatus);
            if(recordKnowledge.PublishStatus == 'Online'){
                System.debug('recordKnowledge.PublishStatus :'+ recordKnowledge.PublishStatus);
                setUserIDs.add(recordKnowledge.CreatedById); 
            }
        }
        System.debug('setUserIDs :'+ setUserIDs);
        List<User> listUser = [Select id from User where id in : setUserIDs]; 
        for(User recordUser : listUser){
            Messaging.SingleEmailMessage emailMessage =  new Messaging.SingleEmailMessage();
            EmailTemplate emailTemplate = [Select id from EmailTemplate where DeveloperName= : 'Knowledge_Article_Publish_Template'];
            emailMessage.setTemplateId(emailTemplate.id);	
            emailMessage.setTargetObjectId(recordUser.Id);
            emailMessage.setSenderDisplayName('Salesforce Support');
            listmails.add(emailMessage);
            System.debug('email Message :'+ emailMessage);
        }  
        if(!listmails.isEmpty()){
            System.debug('listmails :'+ listmails);
            Messaging.sendEmail(listmails);
        }
    }
}