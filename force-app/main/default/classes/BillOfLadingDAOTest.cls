@isTest
public class BillOfLadingDAOTest {
	@testSetup
    static void setup(){
        List<Bill_Of_Lading__c> bolList = TestDataUtility.getBillOfLadingRecords('RR',5);
        TestDataUtility.getBillItemRecords(bolList[0].id,1);
        TestDataUtility.getBOLPartyRecordsForBOL(bolList[0].id,1);
        TestDataUtility.getEquipmentRecordsForBOL(bolList[0].id,1);
        TestDataUtility.getChargeLineItemRecordsForBOL(bolList[0].id,1);
    }
    @isTest
    static void getBolDetailsTest(){
        Test.startTest();
        List<Bill_Of_Lading__c> bol = BillOfLadingDAO.getBolRecords(5,0,'Name','asc','RR');
        Test.stopTest();
        
        System.assertEquals(5, bol.size(), 'Size check');
    }
    
    @isTest
    static void getBolDetailsTestNegative(){
        Boolean errorThrown = false ;
        
        Test.startTest();
        try{
        List<Bill_Of_Lading__c> bol = BillOfLadingDAO.getBolRecords(5,0,'','','');
        }
        catch(Exception ex){
            errorThrown = true ;
        }
        Test.stopTest();
        
        System.assertEquals(true, errorThrown , 'Should have thrown error');
    }
    
    @isTest
    static void getTotalBillOfLadingsRecordsTest(){
        Test.startTest();
        Integer bolCount = BillOfLadingDAO.getTotalBillOfLadingsRecords();
        Test.stopTest();
        
        System.assertEquals(5, bolCount, 'Count check');
    }
    
    @isTest
    static void getGenericObjectsResultTestCase1(){
        Id bolId = [SELECT id from Bill_Of_Lading__c LIMIT 1].Id;
        List<Id> listBolId = new List<Id>();
        listBolId.add(bolId);
        
        Test.startTest();
        List<sObject> sObjectQueryResult = BillOfLadingDAO.getGenericObjectsResult('Name, Bill_of_lading_Status__c','Bill_Of_Lading__c','NA',listBolId);
        Test.stopTest();
        
        System.assertNotEquals(null,sObjectQueryResult, 'Error in query');
    }
    
    @isTest
    static void getGenericObjectsResultTestCase2(){
        Id bolId = [SELECT id from Bill_Of_Lading__c LIMIT 1].Id;
        List<Id> listBolId = new List<Id>();
        listBolId.add(bolId);
        
        Test.startTest();
        List<sObject> sObjectQueryResult = BillOfLadingDAO.getGenericObjectsResult('Name, Bill_Of_Lading_Number__c','Bill_Item__c','Bill_Of_Lading_Number__c',listBolId);
        Test.stopTest();
        
        System.assertNotEquals(null,sObjectQueryResult, 'Error in query');
    }
    
    @isTest 
    static void getBolRecordByIdTest(){
        Bill_Of_Lading__c bolRec = [SELECT id,Name from Bill_Of_Lading__c LIMIT 1];
        
        Test.startTest();
        Bill_Of_Lading__c bol = BillOfLadingDAO.getBolRecordById(bolRec.id);
        Test.stopTest();
        
        System.assertEquals(bolRec.Name, bol.Name, 'BOL Record error');
    }
    
    @isTest 
    static void getBolRecordByIdTestNegative(){
        Boolean errorThrown = false;
        
        Test.startTest();
        try{
            Bill_Of_Lading__c bol = BillOfLadingDAO.getBolRecordById(null);
        }
        catch(Exception ex){
            errorThrown = true;
        }
        Test.stopTest();
        
        System.assertEquals(true, errorThrown, 'Should have thrown error');
    }
    
     @isTest 
    static void getBOLPartyReordsTest(){
        Bill_Of_Lading__c bolRec = [SELECT id,Name from Bill_Of_Lading__c LIMIT 1];
        
        Test.startTest();
         List<Party__c> partyList = BillOfLadingDAO.getBOLPartyReords(bolRec.id);
        Test.stopTest();
        
        System.assertNotEquals(null, partyList, 'Parties Record error');
    }
    
     @isTest 
    static void getBOLPartyReordsTestNegative(){
        Boolean errorThrown = false;
        Bill_Of_Lading__c bolRec = [SELECT id,Name from Bill_Of_Lading__c LIMIT 1];
        
        Test.startTest();
        try{
            List<Party__c> partyList = BillOfLadingDAO.getBOLPartyReords(null);
        }
        catch(Exception ex){
            errorThrown = true;
        }
        Test.stopTest();
        
       // System.assertEquals(true, errorThrown, 'Should have thrown error');
    }
    @isTest 
    static void getBillItemRecordsTest(){
        Bill_Of_Lading__c bolRec = [SELECT id,Name from Bill_Of_Lading__c LIMIT 1];
        
        Test.startTest();
        List<Bill_Item__c> billItemList = BillOfLadingDAO.getBillItemRecords(bolRec.id);
        Test.stopTest();
        
        System.assertNotEquals(null, billItemList, 'Parties Record error');
    }
     @isTest 
    static void getEquipmentRecordsTest(){
        Bill_Of_Lading__c bolRec = [SELECT id,Name from Bill_Of_Lading__c LIMIT 1];
        
        Test.startTest();
         List<Equipment__c> equipmentList = BillOfLadingDAO.getEquipmentRecords(bolRec.id);
        Test.stopTest();
        
        System.assertNotEquals(null, equipmentList, 'Equipments Record error');
    }
    @isTest 
    static void getChargeLineItemsTest(){
        Bill_Of_Lading__c bolRec = [SELECT id,Name from Bill_Of_Lading__c LIMIT 1];
        
        Test.startTest();
         List<Charge_Line__c> chargeLineItems = BillOfLadingDAO.getChargeLineItems(bolRec.id);
        Test.stopTest();
        
        System.assertNotEquals(null, chargeLineItems, 'chargeLineItems Record error');
    }
}