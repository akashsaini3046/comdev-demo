public class CustomerCommunity_SoftShipRateRequest{
    public Booking Booking{get; set;}
    public Login Login{get; set;}
    public String Action{get; set;}
    public String CalcRule{get; set;}
    public class Booking {
        public RequestedBookingRoute RequestedBookingRoute{get; set;}
        public SingleContainer SingleContainer{get; set;}
        public CodeValue Currency_PostFix{get; set;}
    }
    public class Login {
        public String Username{get; set;}
        public String Password{get; set;}
    }
    public class CodeValue {
        public String Code {get; set;}
    }
    public class RequestedBookingRoute {
        public List<Legs> Legs{get; set;}
    }
    public class Legs {
        public LegSequence LegSequence {get; set;}
        public LocationCodeVal StartLocation{get; set;}
        public LocationCodeVal EndLocation{get; set;}
        public String ReceiptTermCode{get; set;}
        public String DeliveryTermCode{get; set;}
        public String ReadyDate{get; set;}
    }
    public class LegSequence {
        public Integer Sequence{get; set;}
    }
    public class LocationCodeVal {
        public CodeValue LocationCode{get; set;}
    }
    public class SingleContainer {
        public Container Container{get; set;}
    }
    public class Container {
        public ContainerType ContainerType{get; set;}
        public CodeValue LoadingType{get; set;}
        public CodeValue Commodity{get; set;}
        public KindOfPackage KindOfPackage{get; set;}

    }
    public class ContainerType {
        public CodeValue ContainerTypeCode{get; set;}
        public Boolean IsReefer{get; set;}
    }
    public class KindOfPackage{
        public CodeValue Code{get; set;}
    }
    public class NumberData {
        public Integer NumberVal{get; set;}
    }
}