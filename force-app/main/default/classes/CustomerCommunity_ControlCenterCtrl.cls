public class CustomerCommunity_ControlCenterCtrl {
    @AuraEnabled
    public static Header__c fetchBooking(){
        try{
            Id userId = UserInfo.getUserId();
            User userDetail = new User();
            Contact conDetail = new Contact();
            Set <ID> bookingSet = new Set <ID> ();
            userDetail = [SELECT id, contactId FROM User WHERE Id = : userId];
            if (userDetail.contactId != NULL) {
                conDetail = [SELECT Id, Name, AccountId FROM Contact WHERE Id = : userDetail.contactId];
                List <Booking__c> bookingList = [SELECT ID, Name FROM Booking__c WHERE Account__c = : conDetail.AccountId];
                for (Booking__c booking: bookingList) {
                    bookingSet.add(booking.ID);
                }
            }
            Header__c objHeader = [Select Booking__r.Name,Name,CreatedName__c,CreatedById,CreatedDate,Status__c from Header__c WHERE Booking__c in : bookingSet AND Created_By_Portal_User__c = TRUE AND Status__c = 'Available for Review' ORDER BY CreatedDate DESC][0];
            if(objHeader == NULL){
                return NULL;
            }
            return objHeader;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            System.debug(ex);
            return NULL;
        }
    }
    
     @AuraEnabled 
    public static user fetchUser(){
       User currentUser = [select id,Name,email from User where id =: userInfo.getUserId()];
       return currentUser;
    }
    @AuraEnabled(cacheable=true)
    public static String fetchAccountName(){
        User currentUser = [Select Id,contactId from User where Id = :userInfo.getUserId()];
        Contact conDetail = [SELECT Id, Name, AccountId FROM Contact WHERE Id = :currentUser.contactId];
        String accountName = [Select Id,Name from Account where Id = :conDetail.AccountId].Name;
        return accountName;
    }
    @AuraEnabled 
    public static Integer countOfCases(){
       User userDetail = [SELECT id, contactId FROM User WHERE Id = : UserInfo.getUserId()];
       Contact conDetail = [SELECT Id, Name, AccountId FROM Contact WHERE Id = : userDetail.contactId];
       Integer caseCount = [SELECT count() FROM Case WHERE AccountId = :conDetail.AccountId];
       return caseCount;
    }
}