public class CC_RatingAPISchedule {
    @AuraEnabled public UTCDate StartSailingDate;
    @AuraEnabled public UTCDate EndSailingDate;
    @AuraEnabled public List<Segments> Segments;
    
    public class UTCDate {
		@AuraEnabled public String UtcDateTime;
	}
	
	public class NumberX {
		@AuraEnabled public String NumberX;
	}	
    
    public class LocationCode {
		@AuraEnabled public String Code;
	}
    
    public class Duration {
		@AuraEnabled public Decimal Amount;
		@AuraEnabled public String TimeUnit;
		@AuraEnabled public Decimal AmountInDays;
		@AuraEnabled public Decimal AmountinMinuteRounded;
		@AuraEnabled public Decimal AmountinHours;
	}
    
    public class Legs {
		@AuraEnabled public Integer LegSeqNumber;
		@AuraEnabled public StartPosition StartPosition;
		@AuraEnabled public EndPosition EndPosition;
		@AuraEnabled public TransportMode TransportMode;
		@AuraEnabled public Duration Duration;
		@AuraEnabled public StayTimePod StayTimePod;
		@AuraEnabled public StayTimePol StayTimePol;
		@AuraEnabled public Integer TransshipmentMatrixDetailId;
	}
    public class StartPosition {
		@AuraEnabled public LocationCode LocationCode;
		@AuraEnabled public SubLocationCode SubLocationCode;
		@AuraEnabled public String DisplayName;
	}
    public class SubLocationCode {
		@AuraEnabled public String Code;
	}
	public class EndPosition {
		@AuraEnabled public LocationCode LocationCode;
		@AuraEnabled public SubLocationCode SubLocationCode;
		@AuraEnabled public String DisplayName;
	}
	public class TransportMode {
		@AuraEnabled public String Code;
		@AuraEnabled public String Type;
	}
	public class StayTimePod {
		@AuraEnabled public Decimal Amount;
		@AuraEnabled public String TimeUnit;
		@AuraEnabled public Decimal AmountInDays;
	}
	public class StayTimePol {
		@AuraEnabled public Decimal Amount;
		@AuraEnabled public String TimeUnit;
		@AuraEnabled public Decimal AmountInDays;
	}

    public class Segments {	
		@AuraEnabled public String VesselName;
		@AuraEnabled public NumberX VoyageNumber;	
		@AuraEnabled public LocationCode FromLocation;
		@AuraEnabled public LocationCode ToLocation;
		@AuraEnabled public LocationCode VesselCode;		
		@AuraEnabled public Boolean IsTBN;
		@AuraEnabled public Boolean IsOcean;
		@AuraEnabled public Legs Leg;
        @AuraEnabled public String FromSubDisplayName;
        @AuraEnabled public String ToSubDisplayName;
        @AuraEnabled public UTCDate DepartureTime;
        @AuraEnabled public Integer NumberOfStops;
	}
}