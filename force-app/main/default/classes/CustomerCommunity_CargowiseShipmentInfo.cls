public Class CustomerCommunity_CargowiseShipmentInfo{
	@AuraEnabled  public UniversalResponse UniversalResponse{get; set;}
	public Class UniversalResponse {
		@AuraEnabled public String Status{get; set;}	//PRS
		@AuraEnabled public Data Data{get; set;}
		@AuraEnabled public String version{get; set;}	//1.1
	}
	public Class Data {
		@AuraEnabled public UniversalShipment UniversalShipment{get; set;}
	}
	public Class UniversalShipment {
		@AuraEnabled public Shipment Shipment{get; set;}
		@AuraEnabled public String version{get; set;}	//1.1
	}
	public Class Shipment {
		@AuraEnabled public DataContext DataContext{get; set;}
		@AuraEnabled public AWBServiceLevel AWBServiceLevel{get; set;}
		@AuraEnabled public String BookingConfirmationReference{get; set;}	//CAT428317
		@AuraEnabled public String ChargeableRate{get; set;}	//0.0000
		@AuraEnabled public String ContainerCount{get; set;}	//1
		@AuraEnabled public ContainerMode ContainerMode{get; set;}
		@AuraEnabled public String DocumentedChargeable{get; set;}	//12.33
		@AuraEnabled public String DocumentedVolume{get; set;}	//0
		@AuraEnabled public String DocumentedWeight{get; set;}	//12330
		@AuraEnabled public String FreightRate{get; set;}	//0.0000
		@AuraEnabled public FreightRateCurrency FreightRateCurrency{get; set;}
		@AuraEnabled public String IsCFSRegistered{get; set;}	//false
		@AuraEnabled public String IsDirectBooking{get; set;}	//true
		@AuraEnabled public String IsForwardRegistered{get; set;}	//true
		@AuraEnabled public String IsNeutralMaster{get; set;}	//false
		@AuraEnabled public String LloydsIMO{get; set;}	//9355472
		@AuraEnabled public String ManifestedChargeable{get; set;}	//12.33
		@AuraEnabled public String ManifestedVolume{get; set;}	//0
		@AuraEnabled public String ManifestedWeight{get; set;}	//12330
		@AuraEnabled public String NoCopyBills{get; set;}	//3
		@AuraEnabled public String NoOriginalBills{get; set;}	//3
		@AuraEnabled public String OuterPacks{get; set;}	//0
		@AuraEnabled public PaymentMethod PaymentMethod{get; set;}
		@AuraEnabled public PlaceOfDelivery PlaceOfDelivery{get; set;}
		@AuraEnabled public PlaceOfIssue PlaceOfIssue{get; set;}
		@AuraEnabled public PlaceOfReceipt PlaceOfReceipt{get; set;}
		@AuraEnabled public PortOfDischarge PortOfDischarge{get; set;}
		@AuraEnabled public PortOfLoading PortOfLoading{get; set;}
		@AuraEnabled public ScreeningStatus ScreeningStatus{get; set;}
		@AuraEnabled public ShipmentType ShipmentType{get; set;}
		@AuraEnabled public String TotalNoOfPacks{get; set;}	//78
		@AuraEnabled public TotalNoOfPacksPackageType TotalNoOfPacksPackageType{get; set;}
		@AuraEnabled public String TotalPreallocatedChargeable{get; set;}	//0.000
		@AuraEnabled public String TotalPreallocatedVolume{get; set;}	//0.000
		@AuraEnabled public String TotalPreallocatedWeight{get; set;}	//0.000
		@AuraEnabled public String TotalVolume{get; set;}	//0
		@AuraEnabled public TotalVolumeUnit TotalVolumeUnit{get; set;}
		@AuraEnabled public String TotalWeight{get; set;}	//12330
		@AuraEnabled public TotalWeightUnit TotalWeightUnit{get; set;}
		@AuraEnabled public TransportMode TransportMode{get; set;}
		@AuraEnabled public String VesselName{get; set;}	//TUCANA J
		@AuraEnabled public String VoyageFlightNo{get; set;}	//NZV9074S
		@AuraEnabled public String WayBillNumber{get; set;}	//COSS9M376766
		@AuraEnabled public WayBillType WayBillType{get; set;}
		@AuraEnabled public ContainerCollection ContainerCollection{get; set;}
		@AuraEnabled public CustomizedFieldCollection[] CustomizedFieldCollection{get; set;}
		@AuraEnabled public DateCollection[] DateCollection{get; set;}
		@AuraEnabled public MilestoneCollection[] MilestoneCollection{get; set;}
		@AuraEnabled public OrganizationAddressCollection[] OrganizationAddressCollection{get; set;}
		@AuraEnabled public SubShipmentCollection SubShipmentCollection{get; set;}
		@AuraEnabled public TransportLegCollection TransportLegCollection{get; set;}
	}
	public Class DataContext {
		@AuraEnabled public DataSourceCollection DataSourceCollection{get; set;}
	}
	public Class DataSourceCollection {
		@AuraEnabled public DataSource[] DataSource{get; set;}
	}
	public Class Company {
		@AuraEnabled public String Code{get; set;}	//JAX
		@AuraEnabled public Country Country{get; set;}
		@AuraEnabled public String Name{get; set;}	//Crowley Logistics, Inc.
	}
	public Class Country {
		@AuraEnabled public String Code{get; set;}	//US
		@AuraEnabled public String Name{get; set;}	//United States
	}
	public Class AWBServiceLevel {
		@AuraEnabled public String Code{get; set;}	//STD
		@AuraEnabled public String Description{get; set;}	//Standard
	}
	public Class ContainerMode {
		@AuraEnabled public String Code{get; set;}	//FCL
		@AuraEnabled public String Description{get; set;}	//Full Container Load
	}
	public Class FreightRateCurrency {
		@AuraEnabled public String Code{get; set;}	//USD
		@AuraEnabled public String Description{get; set;}	//United States Dollar
	}
	public Class PaymentMethod {
		@AuraEnabled public String Code{get; set;}	//PPD
		@AuraEnabled public String Description{get; set;}	//Prepaid
	}
	public Class PlaceOfDelivery {
		@AuraEnabled public String Code{get; set;}	//HNPCR
		@AuraEnabled public String Name{get; set;}	//Puerto Cortes
	}
	public Class PlaceOfIssue {
		@AuraEnabled public String Code{get; set;}	//USJAX
		@AuraEnabled public String Name{get; set;}	//Jacksonville
	}
	public Class PlaceOfReceipt {
		@AuraEnabled public String Code{get; set;}	//USMIA
		@AuraEnabled public String Name{get; set;}	//Miami
	}
	public Class PortOfDischarge {
		@AuraEnabled public String Code{get; set;}	//HNPCR
		@AuraEnabled public String Name{get; set;}	//Puerto Cortes
	}
	public Class PortOfLoading {
		@AuraEnabled public String Code{get; set;}	//USPEF
		@AuraEnabled public String Name{get; set;}	//Port Everglades
	}
	public Class ScreeningStatus {
		@AuraEnabled public String Code{get; set;}	//UNK
		@AuraEnabled public String Description{get; set;}	//Unknown
	}
	public Class ShipmentType {
		@AuraEnabled public String Code{get; set;}	//STD
		@AuraEnabled public String Description{get; set;}	//Standard House
	}
	public Class TotalNoOfPacksPackageType {
		@AuraEnabled public String Code{get; set;}	//CTN
		@AuraEnabled public String Description{get; set;}	//Carton
	}
	public Class TotalVolumeUnit {
		@AuraEnabled public String Code{get; set;}	//CF
		@AuraEnabled public String Description{get; set;}	//Cubic Feet
	}
	public Class TotalWeightUnit {
		@AuraEnabled public String Code{get; set;}	//LB
		@AuraEnabled public String Description{get; set;}	//Pounds
	}
	public Class TransportMode {
		@AuraEnabled public String Code{get; set;}	//SEA
		@AuraEnabled public String Description{get; set;}	//Sea Freight
	}
	public Class WayBillType {
		@AuraEnabled public String Code{get; set;}	//HWB
		@AuraEnabled public String Description{get; set;}	//House Waybill
	}
	public Class ContainerCollection {
		@AuraEnabled public Container Container{get; set;}
		@AuraEnabled public String Content{get; set;}	//Complete
	}
	public Class Container {
		@AuraEnabled public String AirVentFlow{get; set;}	//0.0
		@AuraEnabled public String ArrivalCartageDemurrageCharge{get; set;}	//0.0000
		@AuraEnabled public String ArrivalPickupByRail{get; set;}	//false
		@AuraEnabled public String ContainerCount{get; set;}	//1
		@AuraEnabled public String ContainerDetentionCharge{get; set;}	//0.0000
		@AuraEnabled public String ContainerDetentionDays{get; set;}	//0
		@AuraEnabled public String ContainerNumber{get; set;}	//CMCU4505090
		@AuraEnabled public String ContainerParkEmptyPickupGateOut{get; set;}	//2019-06-18T00:00:00
		@AuraEnabled public ContainerType ContainerType{get; set;}
		@AuraEnabled public String DeliveryMode{get; set;}	//CY/CY
		@AuraEnabled public String DeliverySequence{get; set;}	//0
		@AuraEnabled public String DepartureCartageDemurrageCharge{get; set;}	//0.0000
		@AuraEnabled public String DepartureDeliveryByRail{get; set;}	//false
		@AuraEnabled public String DunnageWeight{get; set;}	//0.000
		@AuraEnabled public FCL_LCL_AIR FCL_LCL_AIR{get; set;}
		@AuraEnabled public String FCLHeldInTransitStaging{get; set;}	//false
		@AuraEnabled public String FCLOnBoardVessel{get; set;}	//2019-06-24T00:00:00
		@AuraEnabled public String FCLStorageArrivedUnderbond{get; set;}	//false
		@AuraEnabled public String FCLStorageCharge{get; set;}	//0.0000
		@AuraEnabled public String FCLStorageDays{get; set;}	//0
		@AuraEnabled public String FCLUnloadFromVessel{get; set;}	//2019-06-27T00:00:00
		@AuraEnabled public String FCLWharfGateIn{get; set;}	//2019-06-21T00:00:00
		@AuraEnabled public String FCLWharfGateOut{get; set;}	//2019-07-01T00:00:00
		@AuraEnabled public String GoodsValue{get; set;}	//0.0000
		@AuraEnabled public String GoodsWeight{get; set;}	//12330.00
		@AuraEnabled public String GrossWeight{get; set;}	//12330.000
		@AuraEnabled public String GrossWeightVerificationDateTime{get; set;}	//2019-06-24T11:41:00
		@AuraEnabled public GrossWeightVerificationType GrossWeightVerificationType{get; set;}
		@AuraEnabled public String HumidityPercent{get; set;}	//0
		@AuraEnabled public String IsCFSRegistered{get; set;}	//false
		@AuraEnabled public String IsControlledAtmosphere{get; set;}	//false
		@AuraEnabled public String IsDamaged{get; set;}	//false
		@AuraEnabled public String IsEmptyContainer{get; set;}	//false
		@AuraEnabled public String IsSealOk{get; set;}	//true
		@AuraEnabled public String IsShipperOwned{get; set;}	//false
		@AuraEnabled public LengthUnit LengthUnit{get; set;}
		@AuraEnabled public String Link{get; set;}	//1
		@AuraEnabled public String OverhangBack{get; set;}	//0.000
		@AuraEnabled public String OverhangFront{get; set;}	//0
		@AuraEnabled public String OverhangHeight{get; set;}	//0
		@AuraEnabled public String OverhangLeft{get; set;}	//0
		@AuraEnabled public String OverhangRight{get; set;}	//0.000
		@AuraEnabled public String OverrideFCLAvailableStorage{get; set;}	//false
		@AuraEnabled public String OverrideLCLAvailableStorage{get; set;}	//false
		@AuraEnabled public String Seal{get; set;}	//0050824
		@AuraEnabled public String SetPointTemp{get; set;}	//0.000
		@AuraEnabled public String SetPointTempUnit{get; set;}	//C
		@AuraEnabled public String TareWeight{get; set;}	//4717.000
		@AuraEnabled public String TotalHeight{get; set;}	//0.000
		@AuraEnabled public String TotalLength{get; set;}	//0.000
		@AuraEnabled public String TotalWidth{get; set;}	//0.000
		@AuraEnabled public String VolumeCapacity{get; set;}	//0.000
		@AuraEnabled public VolumeUnit VolumeUnit{get; set;}
		@AuraEnabled public String WeightCapacity{get; set;}	//0.000
		@AuraEnabled public WeightUnit WeightUnit{get; set;}
		@AuraEnabled public OrganizationAddressCollection OrganizationAddressCollection{get; set;}
	}
	public Class ContainerType {
		@AuraEnabled public String Code{get; set;}	//45GP
		@AuraEnabled public Category Category{get; set;}
		@AuraEnabled public String Description{get; set;}	//45' general purpose
	}
	public Class Category {
		@AuraEnabled public String Code{get; set;}	//DRY
		@AuraEnabled public String Description{get; set;}	//Dry Storage
	}
	public Class FCL_LCL_AIR {
		@AuraEnabled public String Code{get; set;}	//FCL
		@AuraEnabled public String Description{get; set;}	//Full Container Load
	}
	public Class GrossWeightVerificationType {
		@AuraEnabled public String Code{get; set;}	//PKG
		@AuraEnabled public String Description{get; set;}	//Method 2 - Packages
	}
	public Class LengthUnit {
		@AuraEnabled public String Code{get; set;}	//FT
		@AuraEnabled public String Description{get; set;}	//Feet
	}
	public Class VolumeUnit {
		@AuraEnabled public String Code{get; set;}	//M3
		@AuraEnabled public String Description{get; set;}	//Cubic Meters
	}
	public Class WeightUnit {
		@AuraEnabled public String Code{get; set;}	//LB
		@AuraEnabled public String Description{get; set;}	//Pounds
	}
	public Class OrganizationAddressCollection {
		@AuraEnabled public String AddressType{get; set;}	//ConsigneeDocumentaryAddress
		@AuraEnabled public String Address1{get; set;}	//400 N ELM ST
		@AuraEnabled public String AddressOverride{get; set;}	//false
		@AuraEnabled public String AddressShortCode{get; set;}	//444 N ELM ST
		@AuraEnabled public String City{get; set;}	//GREENSBORO
		@AuraEnabled public String CompanyName{get; set;}	//VF JEANSWEAR LTD PARTNERSHIP SOUTHBOUND
		@AuraEnabled public Country Country{get; set;}
		@AuraEnabled public String Fax{get; set;}	//+13363324259
		@AuraEnabled public String OrganizationCode{get; set;}	//VFJEANUS2
		@AuraEnabled public String Phone{get; set;}	//+13363323400
		@AuraEnabled public Port Port{get; set;}
		@AuraEnabled public String Postcode{get; set;}	//27401
		@AuraEnabled public ScreeningStatus ScreeningStatus{get; set;}
		@AuraEnabled public String State{get; set;}	//NC
		@AuraEnabled public RegistrationNumberCollection RegistrationNumberCollection{get; set;}
	}
	public Class OrganizationAddress {
		@AuraEnabled public String AddressType{get; set;}	//GrossWeightVerifiedBy
		@AuraEnabled public String Address1{get; set;}	//91 FOURTH ST
		@AuraEnabled public String AddressOverride{get; set;}	//false
		@AuraEnabled public String AddressShortCode{get; set;}	//91 FOURTH ST
		@AuraEnabled public String City{get; set;}	//TRION
		@AuraEnabled public String CompanyName{get; set;}	//MOUNT VERNON MILLS
		@AuraEnabled public Country Country{get; set;}
		@AuraEnabled public String GovRegNum{get; set;}	//52-042276000
		@AuraEnabled public GovRegNumType GovRegNumType{get; set;}
		@AuraEnabled public String OrganizationCode{get; set;}	//MOUVERUS
		@AuraEnabled public String Phone{get; set;}	//+17067344719
		@AuraEnabled public Port Port{get; set;}
		@AuraEnabled public String Postcode{get; set;}	//30753
		@AuraEnabled public ScreeningStatus ScreeningStatus{get; set;}
		@AuraEnabled public String State{get; set;}	//GA
		@AuraEnabled public RegistrationNumberCollection RegistrationNumberCollection{get; set;}
	}
	public Class GovRegNumType {
		@AuraEnabled public String Code{get; set;}	//EIN
		@AuraEnabled public String Description{get; set;}	//Employer Identification Number
	}
	public Class Port {
		@AuraEnabled public String Code{get; set;}	//USJAX
		@AuraEnabled public String Name{get; set;}	//Jacksonville
	}
	public Class RegistrationNumberCollection {
		@AuraEnabled public Type Type{get; set;}
		@AuraEnabled public CountryOfIssue CountryOfIssue{get; set;}
		@AuraEnabled public String Value{get; set;}	//5448115
	}
	public Class RegistrationNumber {
		@AuraEnabled public Type Type{get; set;}
		@AuraEnabled public CountryOfIssue CountryOfIssue{get; set;}
		@AuraEnabled public String Value{get; set;}	//3562387
	}
	public Class Type {
		@AuraEnabled public String Code{get; set;}	//LSC
		@AuraEnabled public String Description{get; set;}	//Legacy System Code
	}
	public Class CountryOfIssue {
		@AuraEnabled public String Code{get; set;}	//US
		@AuraEnabled public String Name{get; set;}	//United States
	}
	public Class CustomizedFieldCollection {
		@AuraEnabled public String DataType{get; set;}	//String
		@AuraEnabled public String Key{get; set;}	//CLASS
		@AuraEnabled public String Value{get; set;}	//PC
	}
	public Class DateCollection {
		@AuraEnabled public String Type{get; set;}	//BookingConfirmed
		@AuraEnabled public String IsEstimate{get; set;}	//false
		@AuraEnabled public String Value{get; set;}	//2019-06-24T09:30:00
	}
	public Class MilestoneCollection {
		@AuraEnabled public String Description{get; set;}	//Received at Warehouse
		@AuraEnabled public String EventCode{get; set;}	//OCF
		@AuraEnabled public String Sequence{get; set;}	//1
		@AuraEnabled public String ActualDate{get; set;}	//2019-06-24T09:30:00
        @AuraEnabled public String EstimatedDate{get; set;}
	}
	public Class SubShipmentCollection {
		@AuraEnabled public SubShipment SubShipment{get; set;}
	}
	public Class SubShipment {
		@AuraEnabled public DataContext DataContext{get; set;}
		@AuraEnabled public String ActualChargeable{get; set;}	//12.330
		@AuraEnabled public String ContainerCount{get; set;}	//1
		@AuraEnabled public ContainerMode ContainerMode{get; set;}
		@AuraEnabled public String DocumentedChargeable{get; set;}	//12.330
		@AuraEnabled public String DocumentedVolume{get; set;}	//0.000
		@AuraEnabled public String DocumentedWeight{get; set;}	//12330.000
		@AuraEnabled public String FreightRate{get; set;}	//0.0000
		@AuraEnabled public String GoodsValue{get; set;}	//0.0000
		@AuraEnabled public GoodsValueCurrency GoodsValueCurrency{get; set;}
		@AuraEnabled public HBLAWBChargesDisplay HBLAWBChargesDisplay{get; set;}
		@AuraEnabled public String InsuranceValue{get; set;}	//0.0000
		@AuraEnabled public InsuranceValueCurrency InsuranceValueCurrency{get; set;}
		@AuraEnabled public String IsBooking{get; set;}	//false
		@AuraEnabled public String IsCancelled{get; set;}	//false
		@AuraEnabled public String IsCFSRegistered{get; set;}	//false
		@AuraEnabled public String IsDirectBooking{get; set;}	//false
		@AuraEnabled public String IsForwardRegistered{get; set;}	//true
		@AuraEnabled public String IsNeutralMaster{get; set;}	//false
		@AuraEnabled public String IsShipping{get; set;}	//false
		@AuraEnabled public String IsSplitShipment{get; set;}	//false
		@AuraEnabled public String LloydsIMO{get; set;}	//9355472
		@AuraEnabled public String ManifestedChargeable{get; set;}	//12.330
		@AuraEnabled public String ManifestedVolume{get; set;}	//0.000
		@AuraEnabled public String ManifestedWeight{get; set;}	//12330.000
		@AuraEnabled public String NoCopyBills{get; set;}	//1
		@AuraEnabled public String NoOriginalBills{get; set;}	//0
		@AuraEnabled public String OuterPacks{get; set;}	//78
		@AuraEnabled public OuterPacksPackageType OuterPacksPackageType{get; set;}
		@AuraEnabled public String PackingOrder{get; set;}	//0
		@AuraEnabled public PortOfDestination PortOfDestination{get; set;}
		@AuraEnabled public PortOfDischarge PortOfDischarge{get; set;}
		@AuraEnabled public PortOfLoading PortOfLoading{get; set;}
		@AuraEnabled public PortOfOrigin PortOfOrigin{get; set;}
		@AuraEnabled public ReleaseType ReleaseType{get; set;}
		@AuraEnabled public ScreeningStatus ScreeningStatus{get; set;}
		@AuraEnabled public ServiceLevel ServiceLevel{get; set;}
		@AuraEnabled public ShipmentIncoTerm ShipmentIncoTerm{get; set;}
		@AuraEnabled public ShipmentType ShipmentType{get; set;}
		@AuraEnabled public ShippedOnBoard ShippedOnBoard{get; set;}
		@AuraEnabled public String ShipperCODAmount{get; set;}	//0.0000
		@AuraEnabled public String TotalNoOfPacks{get; set;}	//0
		@AuraEnabled public TotalNoOfPacksPackageType TotalNoOfPacksPackageType{get; set;}
		@AuraEnabled public String TotalVolume{get; set;}	//0.000
		@AuraEnabled public TotalVolumeUnit TotalVolumeUnit{get; set;}
		@AuraEnabled public String TotalWeight{get; set;}	//12330.000
		@AuraEnabled public TotalWeightUnit TotalWeightUnit{get; set;}
		@AuraEnabled public String TranshipToOtherCFS{get; set;}	//false
		@AuraEnabled public TransportMode TransportMode{get; set;}
		@AuraEnabled public String VesselName{get; set;}	//TUCANA J
		@AuraEnabled public String VoyageFlightNo{get; set;}	//NZV9074S
		@AuraEnabled public String WayBillNumber{get; set;}	//SUCA278342
		@AuraEnabled public WayBillType WayBillType{get; set;}
		@AuraEnabled public LocalProcessing LocalProcessing{get; set;}
		@AuraEnabled public ContainerCollection ContainerCollection{get; set;}
		@AuraEnabled public CustomizedFieldCollection[] CustomizedFieldCollection{get; set;}
		@AuraEnabled public DateCollection[] DateCollection{get; set;}
		@AuraEnabled public MilestoneCollection[] MilestoneCollection{get; set;}
		//@AuraEnabled public NoteCollection NoteCollection{get; set;}
		@AuraEnabled public OrganizationAddressCollection[] OrganizationAddressCollection{get; set;}
		@AuraEnabled public PackingLineCollection[] PackingLineCollection{get; set;}
		@AuraEnabled public String Content{get; set;}	//Complete
		@AuraEnabled public RelatedShipmentCollection[] RelatedShipmentCollection{get; set;}
		@AuraEnabled public TransportLegCollection TransportLegCollection{get; set;}
	}
	public Class DataSource {
		@AuraEnabled public String Type{get; set;}	//OrderManagerOrder
		@AuraEnabled public String Key{get; set;}	//4501703654~0~VFJEANUS2
	}
	public Class GoodsValueCurrency {
		@AuraEnabled public String Code{get; set;}	//USD
		@AuraEnabled public String Description{get; set;}	//United States Dollar
	}
	public Class HBLAWBChargesDisplay {
		@AuraEnabled public String Code{get; set;}	//AGR
		@AuraEnabled public String Description{get; set;}	//Show "As Agreed" in the charges section
	}
	public Class InsuranceValueCurrency {
		@AuraEnabled public String Code{get; set;}	//USD
		@AuraEnabled public String Description{get; set;}	//United States Dollar
	}
	public Class OuterPacksPackageType {
		@AuraEnabled public String Code{get; set;}	//ROL
		@AuraEnabled public String Description{get; set;}	//Roll
	}
	public Class PortOfDestination {
		@AuraEnabled public String Code{get; set;}	//USGBO
		@AuraEnabled public String Name{get; set;}	//Greensboro
	}
	public Class PortOfOrigin {
		@AuraEnabled public String Code{get; set;}	//USPHL
		@AuraEnabled public String Name{get; set;}	//Philadelphia
	}
	public Class ReleaseType {
		@AuraEnabled public String Code{get; set;}	//EBL
		@AuraEnabled public String Description{get; set;}	//Express Bill of Lading
	}
	public Class ServiceLevel {
		@AuraEnabled public String Code{get; set;}	//STD
		@AuraEnabled public String Description{get; set;}	//Standard
	}
	public Class ShipmentIncoTerm {
		@AuraEnabled public String Code{get; set;}	//FOB
		@AuraEnabled public String Description{get; set;}	//Free On Board
	}
	public Class ShippedOnBoard {
		@AuraEnabled public String Code{get; set;}	//SHP
		@AuraEnabled public String Description{get; set;}	//Shipped
	}
	public Class LocalProcessing {
		@AuraEnabled public String DeliveryLabourCharge{get; set;}	//0.0000
		@AuraEnabled public String DemurrageOnDeliveryCharge{get; set;}	//0.0000
		@AuraEnabled public String DemurrageOnPickupCharge{get; set;}	//0.0000
		@AuraEnabled public FCLDeliveryEquipmentNeeded FCLDeliveryEquipmentNeeded{get; set;}
		@AuraEnabled public FCLPickupEquipmentNeeded FCLPickupEquipmentNeeded{get; set;}
		@AuraEnabled public String HasProhibitedPackaging{get; set;}	//false
		@AuraEnabled public String InsuranceRequired{get; set;}	//false
		@AuraEnabled public String IsContingencyRelease{get; set;}	//false
		@AuraEnabled public String LCLAirStorageCharge{get; set;}	//0.0000
		@AuraEnabled public String LCLAirStorageDaysOrHours{get; set;}	//0
		@AuraEnabled public String LCLDatesOverrideConsol{get; set;}	//false
		@AuraEnabled public String PickupLabourCharge{get; set;}	//0.0000
		@AuraEnabled public PrintOptionForPackagesOnAWB PrintOptionForPackagesOnAWB{get; set;}
		@AuraEnabled public OrderNumberCollection[] OrderNumberCollection{get; set;}
	}
	public Class FCLDeliveryEquipmentNeeded {
		@AuraEnabled public String Code{get; set;}	//WUP
		@AuraEnabled public String Description{get; set;}	//Wait for Pack/Unpack
	}
	public Class FCLPickupEquipmentNeeded {
		@AuraEnabled public String Code{get; set;}	//ANY
		@AuraEnabled public String Description{get; set;}	//Any
	}
	public Class PrintOptionForPackagesOnAWB {
		@AuraEnabled public String Code{get; set;}	//DEF
		@AuraEnabled public String Description{get; set;}	//Default (Dims, fallback to Vol)
	}
	public Class OrderNumberCollection {
		@AuraEnabled public String OrderReference{get; set;}	//4501703654
		@AuraEnabled public String Sequence{get; set;}	//1
	}
	public Class NoteCollection {
		@AuraEnabled public Note Note{get; set;}
		@AuraEnabled public String Content{get; set;}	//Partial
	}
	public Class Note {
		@AuraEnabled public String Description{get; set;}	//Detailed Goods Description
		@AuraEnabled public String IsCustomDescription{get; set;}	//false
		@AuraEnabled public String NoteText{get; set;}	//78 rollos de tela78 rolls of fabricITN: X20190620032632
		@AuraEnabled public NoteContext NoteContext{get; set;}
		@AuraEnabled public Visibility Visibility{get; set;}
	}
	public Class NoteContext {
		@AuraEnabled public String Code{get; set;}	//AAA
		@AuraEnabled public String Description{get; set;}	//Module: A - All, Direction: A - All, Freight: A - All
	}
	public Class Visibility {
		@AuraEnabled public String Code{get; set;}	//PUB
		@AuraEnabled public String Description{get; set;}	//CLIENT-VISIBLE
	}
	public Class PackingLineCollection {
		@AuraEnabled public Commodity Commodity{get; set;}
		@AuraEnabled public String ContainerLink{get; set;}	//1
		@AuraEnabled public String ContainerNumber{get; set;}	//CMCU4505090
		@AuraEnabled public String ContainerPackingOrder{get; set;}	//2
		@AuraEnabled public CountryOfOrigin CountryOfOrigin{get; set;}
		@AuraEnabled public String EndItemNo{get; set;}	//0
		@AuraEnabled public String GoodsDescription{get; set;}	//63(.5)APP 7.5 3X1L 100C FR 63729 BLU BLK
		@AuraEnabled public String Height{get; set;}	//0.000
		@AuraEnabled public String ItemNo{get; set;}	//0
		@AuraEnabled public String Length{get; set;}	//0.000
		@AuraEnabled public String LinePrice{get; set;}	//14763.3200
		@AuraEnabled public String Link{get; set;}	//1
		@AuraEnabled public String LoadingMeters{get; set;}	//0.000
		@AuraEnabled public String OutturnDamagedQty{get; set;}	//0
		@AuraEnabled public String OutturnedHeight{get; set;}	//0.000
		@AuraEnabled public String OutturnedLength{get; set;}	//0.000
		@AuraEnabled public String OutturnedVolume{get; set;}	//0.000
		@AuraEnabled public String OutturnedWeight{get; set;}	//0.000
		@AuraEnabled public String OutturnedWidth{get; set;}	//0.000
		@AuraEnabled public String OutturnPillagedQty{get; set;}	//0
		@AuraEnabled public String OutturnQty{get; set;}	//0
		@AuraEnabled public String PackQty{get; set;}	//5
		@AuraEnabled public PackType PackType{get; set;}
		@AuraEnabled public String Volume{get; set;}	//0.000
		@AuraEnabled public String Weight{get; set;}	//1587.000
		@AuraEnabled public WeightUnit WeightUnit{get; set;}
		@AuraEnabled public String Width{get; set;}	//0.000
		@AuraEnabled public CustomizedFieldCollection[] CustomizedFieldCollection{get; set;}
		@AuraEnabled public PackedItemCollection PackedItemCollection{get; set;}
	}
	public Class Commodity {
		@AuraEnabled public String Code{get; set;}	//GEN
		@AuraEnabled public String Description{get; set;}	//General
	}
	public Class CountryOfOrigin {
		@AuraEnabled public String Code{get; set;}	//US
		@AuraEnabled public String Name{get; set;}	//United States
	}
	public Class PackType {
		@AuraEnabled public String Code{get; set;}	//ROL
		@AuraEnabled public String Description{get; set;}	//Roll
	}
	public Class PackedItemCollection {
		@AuraEnabled public PackedItem PackedItem{get; set;}
	}
	public Class PackedItem {
		@AuraEnabled public String OrderLineLink{get; set;}	//1
		@AuraEnabled public String PackedQuantity{get; set;}	//1459.00000
		@AuraEnabled public Product Product{get; set;}
		@AuraEnabled public UnitOfQuantity UnitOfQuantity{get; set;}
	}
	public Class Product {
		@AuraEnabled public String Code{get; set;}	//62164
		@AuraEnabled public String Description{get; set;}	//60(.5)FSI 6.5 88C12N FR 63828 PLAID
	}
	public Class UnitOfQuantity {
		@AuraEnabled public String Code{get; set;}	//YD
		@AuraEnabled public String Description{get; set;}	//Yards
	}
	public Class RelatedShipmentCollection {
		@AuraEnabled public DataContext DataContext{get; set;}
		@AuraEnabled public ContainerMode ContainerMode{get; set;}
		@AuraEnabled public CountryOfSupply CountryOfSupply{get; set;}
		@AuraEnabled public String FreightRate{get; set;}	//1.000000000
		@AuraEnabled public FreightRateCurrency FreightRateCurrency{get; set;}
		@AuraEnabled public String OuterPacks{get; set;}	//4
		@AuraEnabled public OuterPacksPackageType OuterPacksPackageType{get; set;}
		@AuraEnabled public PortOfDestination PortOfDestination{get; set;}
		@AuraEnabled public PortOfDischarge PortOfDischarge{get; set;}
		@AuraEnabled public PortOfLoading PortOfLoading{get; set;}
		@AuraEnabled public PortOfOrigin PortOfOrigin{get; set;}
		@AuraEnabled public ShipmentIncoTerm ShipmentIncoTerm{get; set;}
		@AuraEnabled public String TotalVolume{get; set;}	//0.000
		@AuraEnabled public TotalVolumeUnit TotalVolumeUnit{get; set;}
		@AuraEnabled public String TotalWeight{get; set;}	//1868.000
		@AuraEnabled public TotalWeightUnit TotalWeightUnit{get; set;}
		@AuraEnabled public TransportMode TransportMode{get; set;}
		@AuraEnabled public WayBillType WayBillType{get; set;}
		@AuraEnabled public Order Order{get; set;}
		@AuraEnabled public AdditionalBillCollection AdditionalBillCollection{get; set;}
		@AuraEnabled public DateCollection[] DateCollection{get; set;}
		@AuraEnabled public MilestoneCollection[] MilestoneCollection{get; set;}
		@AuraEnabled public OrganizationAddressCollection[] OrganizationAddressCollection{get; set;}
	}
	public Class CountryOfSupply {
		@AuraEnabled public String Code{get; set;}	//US
		@AuraEnabled public String Name{get; set;}	//United States
	}
	public Class Order {
		@AuraEnabled public String OrderNumber{get; set;}	//4501703654
		@AuraEnabled public String OrderNumberSplit{get; set;}	//0
		@AuraEnabled public Status Status{get; set;}
		@AuraEnabled public OrderLineCollection OrderLineCollection{get; set;}
	}
	public Class Status {
		@AuraEnabled public String Code{get; set;}	//PLC
		@AuraEnabled public String Description{get; set;}	//Order Placed / finalized
	}
	public Class OrderLineCollection {
		@AuraEnabled public OrderLine OrderLine{get; set;}
		@AuraEnabled public String Content{get; set;}	//Complete
	}
	public Class OrderLine {
		@AuraEnabled public String ContainerPackingOrder{get; set;}	//0
		@AuraEnabled public CustomsData CustomsData{get; set;}
		@AuraEnabled public String ExpectedQuantity{get; set;}	//2532.00000
		@AuraEnabled public String ExtendedLinePrice{get; set;}	//21395.4000
		@AuraEnabled public String InnerPacksQty{get; set;}	//0.000
		@AuraEnabled public InnerPacksQtyUnit InnerPacksQtyUnit{get; set;}
		@AuraEnabled public String LineNumber{get; set;}	//1
		@AuraEnabled public String LineSplitNumber{get; set;}	//0
		@AuraEnabled public String Link{get; set;}	//5
		@AuraEnabled public String OrderedQty{get; set;}	//1977.00000
		@AuraEnabled public OrderedQtyUnit OrderedQtyUnit{get; set;}
		@AuraEnabled public String PackageHeight{get; set;}	//0.000
		@AuraEnabled public String PackageLength{get; set;}	//0.000
		@AuraEnabled public String PackageQty{get; set;}	//0.000
		@AuraEnabled public String PackageWidth{get; set;}	//0.000
		@AuraEnabled public Product Product{get; set;}
		@AuraEnabled public String QuantityMet{get; set;}	//2532.00000
		@AuraEnabled public Status Status{get; set;}
		@AuraEnabled public String SubLineNumber{get; set;}	//1
		@AuraEnabled public String UnitPriceRecommended{get; set;}	//10.8222
		@AuraEnabled public String Volume{get; set;}	//0.000
		@AuraEnabled public String Weight{get; set;}	//1868.000
		@AuraEnabled public WeightUnit WeightUnit{get; set;}
		@AuraEnabled public CustomizedFieldCollection[] CustomizedFieldCollection{get; set;}
	}
	public Class CustomsData {
		@AuraEnabled public CountryOfOrigin CountryOfOrigin{get; set;}
	}
	public Class InnerPacksQtyUnit {
		@AuraEnabled public String Code{get; set;}	//ROL
		@AuraEnabled public String Description{get; set;}	//Roll
	}
	public Class OrderedQtyUnit {
		@AuraEnabled public String Code{get; set;}	//YD
		@AuraEnabled public String Description{get; set;}	//Yards
	}
	public Class AdditionalBillCollection {
		@AuraEnabled public AdditionalBill AdditionalBill{get; set;}
	}
	public Class AdditionalBill {
		@AuraEnabled public BillType BillType{get; set;}
	}
	public Class BillType {
		@AuraEnabled public String Code{get; set;}	//MWB
		@AuraEnabled public String Description{get; set;}	//Master Waybill
	}
	public Class TransportLegCollection {
		@AuraEnabled public TransportLeg TransportLeg{get; set;}
		@AuraEnabled public String Content{get; set;}	//Complete
	}
	public Class TransportLeg {
		@AuraEnabled public PortOfDischarge PortOfDischarge{get; set;}
		@AuraEnabled public PortOfLoading PortOfLoading{get; set;}
		@AuraEnabled public String LegOrder{get; set;}	//1
		@AuraEnabled public String ActualArrival{get; set;}	//2019-06-27T00:00:00
		@AuraEnabled public String ActualDeparture{get; set;}	//2019-06-25T00:00:00
		@AuraEnabled public Carrier Carrier{get; set;}
		@AuraEnabled public String CarrierBookingReference{get; set;}	//NZV
		@AuraEnabled public String EstimatedArrival{get; set;}	//2019-06-27T06:30:00
		@AuraEnabled public String EstimatedDeparture{get; set;}	//2019-06-22T09:00:00
		@AuraEnabled public String IsCargoOnly{get; set;}	//true
		@AuraEnabled public String LegType{get; set;}	//Main
		@AuraEnabled public String TransportMode{get; set;}	//Sea
		@AuraEnabled public String VesselLloydsIMO{get; set;}	//9355472
		@AuraEnabled public String VesselName{get; set;}	//TUCANA J
		@AuraEnabled public String VoyageFlightNo{get; set;}	//NZV9074S
	}
	public Class Carrier {
		@AuraEnabled public String AddressType{get; set;}	//Carrier
		@AuraEnabled public String Address1{get; set;}	//NORTHERN ZONE
		@AuraEnabled public String Address2{get; set;}	//MOVEMENTS BETWEEN PEV AND CA NZV
		@AuraEnabled public String AddressOverride{get; set;}	//false
		@AuraEnabled public String AddressShortCode{get; set;}	//21
		@AuraEnabled public String City{get; set;}	//JACKSONVILLE
		@AuraEnabled public String CompanyName{get; set;}	//CROWLEY LATIN AMERICA SERVICES
		@AuraEnabled public Country Country{get; set;}
		@AuraEnabled public String Email{get; set;}	//CROWLEY@CROWLEY.COM
		@AuraEnabled public String GovRegNum{get; set;}	//98-059007400
		@AuraEnabled public GovRegNumType GovRegNumType{get; set;}
		@AuraEnabled public String OrganizationCode{get; set;}	//IC1CLSNZV
		@AuraEnabled public Port Port{get; set;}
		@AuraEnabled public String Postcode{get; set;}	//32225
		@AuraEnabled public ScreeningStatus ScreeningStatus{get; set;}
		@AuraEnabled public String State{get; set;}	//FL
		@AuraEnabled public String UniversalOfficeCode{get; set;}	//496153601
		@AuraEnabled public RegistrationNumberCollection[] RegistrationNumberCollection{get; set;}
	}
    
    public Class CustomerCommunity_MilestonesInfo{
		@AuraEnabled public String ParentJob{get; set;}
		@AuraEnabled public String Description{get; set;}
		@AuraEnabled public String Shipmentdate{get; set;}
		@AuraEnabled public String Status{get; set;}
	}
	@AuraEnabled public static CustomerCommunity_CargowiseShipmentInfo parse(String json){
		return (CustomerCommunity_CargowiseShipmentInfo) System.JSON.deserialize(json, CustomerCommunity_CargowiseShipmentInfo.class);
	}
}