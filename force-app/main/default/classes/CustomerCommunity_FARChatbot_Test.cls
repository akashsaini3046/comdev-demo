@isTest
public class CustomerCommunity_FARChatbot_Test {
    private class RestMock1 implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '[{"originPort":"AW, ARUBA", "destinationPort":"BQ, BONAIRE", "shipmentType":"LCL", "sailingWeeks":"2 Week", "transitTime":"2015-07-27 00:00:00.000", "details":[{"voyageNumber":"12", "vesselName":"vName", "startDate":"10-11-2019", "arrivalDate":"11-11-2019", "transitTime":"2015-07-27 00:00:00.000"}]}]';

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    
     private class RestMock2 implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '[{"d":"({\r\n\r\n\"HeaderTexts\" : ({\r\n\r\n\"VoyageNumber\" : \"Voyage #\",\r\n\"VesselCode\" : \"Vessel\",\r\n\"VesselName\" : null\r\n,\r\n\"TypeCode\" : \"Type\",\r\n\"TypeDescription\" : null\r\n,\r\n\"StatusCode\" : \"Status\",\r\n\"StatusDescription\" : null\r\n,\r\n\"PortOfLoadingCode\" : \"POL\",\r\n\"PortOfLoadingCallId\" : \"POL Call ID\",\r\n\"PortOfLoadingDescription\" : null\r\n,\r\n\"PortOfDischargeCode\" : \"POD\",\r\n\"PortOfDischargeCallId\" : \"POD Call ID\",\r\n\"PortOfDischargeDescription\" : null\r\n,\r\n\"CentralPortCode\" : \"Centralport\",\r\n\"PortCallId\" : \"Call ID\",\r\n\"CentralPortDescription\" : null\r\n,\r\n\"StartDate\" : \"Departure\",\r\n\"EndDate\" : \"Arrival\",\r\n\"Duration\" : \"Duration\",\r\n\"HasTransshipment\" : \"Direct\",\r\n\"ServiceName\" : \"Service\",\r\n\"PortCode\" : \"Code\",\r\n\"PortName\" : \"Port\",\r\n\"ArrivalDate\" : \"Arrival Date\",\r\n\"SailingDate\" : \"Sailing Date\", \r\n\"VoyageMachineNumber\" : \"2111\"}) \r\n]\r\n})\r\n"}]';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    static TestMethod void RouteParameter_test1(){
        Test.setMock(HttpCalloutMock.class, new RestMock1());
        CustomerCommunity_FARChatbot.RouteParameters input = new CustomerCommunity_FARChatbot.RouteParameters();
        input.sShipment = 'LCL';
        input.sOrigin = 'ARUBA';
        input.sDestination = 'BONAIRE';
        input.sWeeks = '2 week';
        
        List<CustomerCommunity_FARChatbot.RouteParameters> listinput = new list<CustomerCommunity_FARChatbot.RouteParameters>();	
        listinput.add(input);
        Test.startTest();
        CustomerCommunity_FARChatbot.FindARoute(listinput);
        Test.stopTest();
               
    }
    
    static TestMethod void RouteParameter_test2(){
        Test.setMock(HttpCalloutMock.class, new RestMock2());
        CustomerCommunity_FARChatbot.RouteParameters input2 = new CustomerCommunity_FARChatbot.RouteParameters();
        input2.sShipment = 'test';
        input2.sOrigin = 'test';
        input2.sDestination = 'test';
        input2.sWeeks = '2 test';
        
        List<CustomerCommunity_FARChatbot.RouteParameters> listinput2 = new list<CustomerCommunity_FARChatbot.RouteParameters>();	
        listinput2.add(input2);
        Test.startTest();
        CustomerCommunity_FARChatbot.FindARoute(listinput2);
        Test.stopTest();
               
    }

}