@isTest
public class EmailMessageTriggerHandler_Test {
    static testMethod void EmailMessageTriggerHandler_TestMethod()
    {
        Account accObj = new Account();
        accObj.Name = 'Account 123';
        insert accObj;
        
        DescribeFieldResult describe = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
        
        Address__c businessLocationObj = new Address__c();
        businessLocationObj.Account__c = accObj.Id;
        businessLocationObj.Address_Line_1__c = 'address1';
        businessLocationObj.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj.Country__c = availableValues[0].getValue();
        businessLocationObj.Postal_Code__c = '1111111';
        businessLocationObj.Phone__c = '88888888888';
        businessLocationObj.State__c = 'State1';
        businessLocationObj.Name ='BL1';
        insert businessLocationObj;
        
        Contact con= new Contact();
        con.LastName='Test con1';
        con.Phone = '99999999999';
        con.Email = 'contact@email.com';
        con.Address__c = businessLocationObj.Id;
        insert con;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Update standardPricebook;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = accObj.Id;
        opp.Name = 'Opp1';
        opp.CloseDate = System.today();
        opp.StageName = ConstantClass.PROPOSAL_PRICE_QUOTE_OPP_STAGE;
        opp.Proposal_Submission_Date__c = System.today();
        opp.Est_Business_Start_Date__c = System.today();
        opp.Service_Type__c = 'CrowleyFresh';
        opp.Contact__c = con.Id;
        insert opp;
        
        System.assertEquals(opp.StageName, ConstantClass.PROPOSAL_PRICE_QUOTE_OPP_STAGE);
        
        EmailMessage outGoingMail= new EmailMessage();
        outGoingMail.fromaddress='test@test.com';
        outGoingMail.toAddress = con.Email;
        outGoingMail.subject = 'This is the message subject.';
        outGoingMail.TextBody= 'This is the message body. Send Quote '+opp.Id;
        outGoingMail.RelatedToId=opp.id;
        insert outGoingMail;   

        Test.startTest();
        System.assertEquals(opp.Id, outGoingMail.RelatedToId);
        System.assertEquals(outGoingMail.TextBody.contains('Send Quote '+opp.Id), true);
        Test.stopTest();
    }
}