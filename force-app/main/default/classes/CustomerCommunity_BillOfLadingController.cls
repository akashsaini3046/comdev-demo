public without sharing class CustomerCommunity_BillOfLadingController {
    
    @AuraEnabled
    public static List<Bill_Of_Lading__c> fetchBillOfLadingData(Integer recordLimit, Integer recordOffset, String fieldName, String order){
        Integer intLimit = Integer.valueof(recordLimit);
        Integer intOffset = Integer.valueof(recordOffset);
        String status='RR';
        //Id bookingRecordTypeId = getRecordTypeId('Booking__c:Booking_Record');
        String query = 'SELECT Id, Name, Booking_Number__r.Booking_Number__c,  CreatedDate FROM Bill_Of_Lading__c WHERE Bill_of_lading_Status__c=\''+status+ '\' ORDER BY '+fieldName+' '+ order + ' LIMIT ' +intLimit + ' OFFSET '+ intOffset ;
        System.debug('query : '+query);
        List<Bill_Of_Lading__c> billsOfLading = Database.query(query);
        System.debug('billsOfLading size : '+billsOfLading.size());
        return billsOfLading;
    }
   
    
    @AuraEnabled
    public static Integer getTotalNumberOfBillOfLadings(){
        AggregateResult results = [SELECT Count(Id) Total From Bill_Of_Lading__c WHERE Bill_of_lading_Status__c='RR'];
        Integer totalRecords = (Integer)results.get('Total') ; 
        System.debug('totalRecords : '+totalRecords);
        return totalRecords;
    }
    
    @AuraEnabled
    public static String fetchCommunityName(){
        String nwid = Network.getNetworkId();
        if (!String.isBlank(nwid)) {
            String communityName = [SELECT name from Network where id =: nwid][0].name ;
            System.debug('COMMUNITY NAME '+ communityName);
            return communityName;
    	}
        else{
            return null;
        }
        
    }
   
}