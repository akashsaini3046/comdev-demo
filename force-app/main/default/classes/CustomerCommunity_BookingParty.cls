public class CustomerCommunity_BookingParty {
    @AuraEnabled public String code{get; set;}
    @AuraEnabled public Contact ContactRecord{get; set;}
    @AuraEnabled public Address__c AddressRecord{get; set;}
}