@IsTest
public class TBD_FindARouteCtrl_Test {
 static testMethod void getRoutesTestMethodPositive(){
        CustomerCommunity_MockResponses.FindARouteMockResponseStatic mock=new CustomerCommunity_MockResponses.FindARouteMockResponseStatic();
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();    
        TBD_FindARouteController.getRoutes('DO, CAUCEDO', 'US, CHICAGO-CFS', 'FCL', '1 Week');      
        Test.StopTest(); 
 }
   static testMethod void getRoutesTestMethodNegative(){
        CustomerCommunity_MockResponses.FindARouteMockResponseStatic mock=new CustomerCommunity_MockResponses.FindARouteMockResponseStatic();
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();    
        TBD_FindARouteController.getRoutes('DO, CAUCEDO', null, null, null);      
        Test.StopTest(); 
 }
}