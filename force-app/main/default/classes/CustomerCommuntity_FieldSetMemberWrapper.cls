public class CustomerCommuntity_FieldSetMemberWrapper implements Comparable {
    
    @AuraEnabled
    public String objectLabelName {get;set;} 
    
    @AuraEnabled
    public String objectApiName {get;set;}
    
    @AuraEnabled
    public Decimal sequence {get;set;}
    
    @AuraEnabled
    public String parentName {get;set;}
    
    @AuraEnabled
    public String fieldNamesconcat {get;set;}    
    
    @AuraEnabled
    public List<CustomerCommunity_FieldLabelWrapper> listFieldLabelWrapper { get; set; }
    
    @AuraEnabled
    public Boolean multipleRecord {get;set;}
    
    public Integer compareTo(Object objToCompare){
        CustomerCommuntity_FieldSetMemberWrapper currentDocument = (CustomerCommuntity_FieldSetMemberWrapper)(objToCompare);
        
        if (this.sequence < currentDocument.sequence) {
            return -1;
        }
        if (this.sequence == currentDocument.sequence) {
            return 0;
        }
        return 1;
    }
}