global with sharing class CustomAuthProvider extends Auth.AuthProviderPluginClass{
    public String redirectURL;
    private String key;
    private String secret;
    private String type;

    global String getCustomMetadataType(){
        return 'CustomAuthProvider__mdt';
    }

    global PageReference initiate(Map<String, String> authProviderConfiguration, String stateToPropagate) {
        return null;
    }



    global Auth.AuthProviderTokenResponse handleCallback(Map<String, String> authProviderConfiguration, Auth.AuthProviderCallbackState state) {

        key = authProviderConfiguration.get('client_id__c');
        secret = authProviderConfiguration.get('client_secret__c');
        type = authProviderConfiguration.get('ContentType__c');
        //redirectURL = authProviderConfiguration.get('EndURL__c');
       
        
        Map<String,String> queryParams = state.queryParameters;
        String code = queryParams.get('code');
        System.debug('Code value : '+code);
        String sfdcState = queryParams.get('state');
        System.debug('sfdcState value : '+sfdcState);
        HttpRequest req = new HttpRequest();
        req.setEndpoint(redirectURL);
        req.setHeader('client_id',key);
       req.setHeader('client_secret',secret);
        //req.setHeader('redirect_uri', redirectURL);
        req.setHeader('Content-Type	','application/json');
        req.setMethod('POST');
       // req.setHeader('client_id='+key+'&client_secret='+secret+'&redirect_uri='+redirectURL);
 
        Http http = new Http();
        HTTPResponse res = http.send(req);
        String responseBody = res.getBody();
       // String token = getTokenValueFromResponse(responseBody, 'Token', null);

        return new Auth.AuthProviderTokenResponse('CustomAuthProvider',null,null,sfdcState);
       // return new Auth.AuthProviderTokenResponse('CustomAuthProvider',token, null, sfdcState);
     }


     global Auth.UserData getUserInfo(Map<String, String> authProviderConfiguration, Auth.AuthProviderTokenResponse response) {
         return null;

     }


   /*  private String getTokenValueFromResponse(String response, String token, String ns)
        {
        Dom.Document docx = new Dom.Document();
        docx.load(response);
        String ret = null;
        dom.XmlNode xroot = docx.getrootelement() ;
        if(xroot != null){ ret = xroot.getChildElement(token, ns).getText();

        }

    return ret;

    }  */
}