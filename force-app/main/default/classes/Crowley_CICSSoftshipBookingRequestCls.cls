public class Crowley_CICSSoftshipBookingRequestCls {
	public String number_replace;
	public HeadersWrap headers;
	public ServiceTypeWrap serviceType;
	public CustomerLocationWrap customerOrigin;
	public CustomerLocationWrap customerDestination;
	public List <PartyWrapperCls> parties = new List <PartyWrapperCls> ();
	public List <TransportWrapperCls> transports = new List <TransportWrapperCls> ();
	public List <ShipmentWrapperCls> shipments = new List <ShipmentWrapperCls> ();


	public class CustomerLocationWrap {
		public String transId;
		public String stopId;
		public String code;
		public String city;
		public String state;
		public String country;
		public String zip;
		public String referenceNumber;
	}

	public class ServiceTypeWrap {
		public String description;
	}

	public class HeadersWrap {
		public String label;
		public String paymentTerms;
		public String status;
		public String bookDate;
		public String cmcPickupLoc;
		public String cmcDeliverLoc;
	}
	
	/*********************-------*********************/
	public class PartyWrapperCls {
		public String id;
		public String type;
		public String cvif;
		public String cvifLocationCode;
		public String refNumber;
		public String partyName;
		public String billToInd;
		public String bookbyInd;
		public String notifyInd;
		public String contactName;
		public String phoneNumber;
		public String emailAddr;
		public String faxNumber;
		public ParyLocationWrap location;
		
	}

	public class ParyLocationWrap {
		public String addressLine1;
		public String addressLine2;
		public String city;
		public String state;
		public String country;
		public String zip;
	}
	
	/*********************-------*********************/

	public class TransportWrapperCls {
		public String id;
		public String originDestCd;
		public String ctlLocAbbr;
		public List<StopWrap> stops = new List<StopWrap>();
	}

	public class StopWrap {
		public String stopId;
		public String cvif;
		public String cvifLocationCode;
		public String stopName;
		public String name;
		public String phoneNumber;
		public String pickupDate;
		public String address1;
		public String address2;
		public String city;
		public String state;
		public String zip;
		public String country;
	}
	
	/*********************-------*********************/

	public class ShipmentWrapperCls {
		public String number_replace;
		public OriginWrap origin;
		public DestinationWrap destination;
		public String status;
		public List<VoyagesWrapperCls> voyages = new List<VoyagesWrapperCls>();
		public List<FreightDetailsWrapperCls> freightDetails = new List<FreightDetailsWrapperCls>();
	}
	
	public class DestinationWrap {
		public String port;
		public LocationWrap location;
	}
	public class OriginWrap {
		public String port;
		public LocationWrap location;
	}
	public class LocationWrap {
		public String locCode;
		public String code;
		public String city;
		public String state;
		public String country;
	}
	
	/*********************-------*********************/

	public class VoyagesWrapperCls {
		public String shipmentNumber;
		public String voyageNumber;
		public String estimateSailDate;
		public String estimateArrivalDate;
		public String vesselAbbreviation;
		public String vesselName;
		public String vesselLloydsCode;
		public String roldReason;
		public LoadDischargeWrap load;
		public LoadDischargeWrap discharge;
	}
	public class LoadDischargeWrap {
		public String port;
		public Decimal sequence;
		public LocationWrap location;
	}
	
	/*********************-------*********************/

	public class FreightDetailsWrapperCls {
		public String freightId;
		public String cargoType;
		public ValueUnitFreightWrap declaredWeight;
		public ValueUnitFreightWrap volume;
		public Integer declaredValue;
		public MoveType moveType;
		public ValueUnitFreightWrap freightQty;
		public String overDimInd;
		public String roroInd;
		public String fleetUsedCd;
		public String povDealerCd;
		public MajorMinorWrap length;
		public MajorMinorWrap width;
		public MajorMinorWrap height;
		public LumberWrap lumberQty;
		public List<CommodityWrapperCls> commodities = new List<CommodityWrapperCls>();
		public List<RequirementWrapperCls> requirements = new List<RequirementWrapperCls>();
	}
	
	public class LumberWrap {
		public String linearUom;
		public Integer value;
		public String unitofMeasure;
	}
	public class MajorMinorWrap {
		public Integer major;
		public Integer minor;
	}
	public class MoveType {
		public String moveTypeDesc;
	}
	public class ValueUnitWrap {
		public String value;
		public String unitofMeasure;
	}

	public class ValueUnitFreightWrap {
		public Integer value;
		public String unitofMeasure;
	}
	
	/*********************-------*********************/

	public class CommodityWrapperCls{
	
		public String imoClass;
		public String prefix;
		public String description;
		public String secondaryClass;
		public String imdgPage;
		public String number_replace;
		public String subRiskClass;
		public String dotName;
		public String packageGroup;
		public String isMarinePollutant;
		public String primaryClass;
		public String requiredQtyInd;
		public Boolean isHazardous;
		public String contractNbr;
		public ValueUnitWrap quantity;
		public EmergencyContact emergencyContact;
		public ValueUnitWrap weight;
		public String hazIMOLock;
		public String commodityId;
		public String technicalName;
		public String tariffNbr;
		public ValueUnitWrap volume;
		public String permitNumber;
		public ValueUnitWrap flashTemperature;
		public String isLimitedQuantity;
		public String reportedQty;	
	}
	
	public class EmergencyContact{
		public String phoneNumber;
		public String name;
	}
	
	/*********************-------*********************/

	public class RequirementWrapperCls{
		public Decimal quantity;
		public String specEquipCd;
		public String rrInd;
		public String tarpInd;
		public Decimal length;
		public ValueUnitWrap weight;
		public String type;
		public String termsCd;
		public String reqId;
		public String highCubeInd;
		public String ventilated;
		public ValueUnitWrap temperature;
		public Decimal width;
		public String category;
			
	}
}