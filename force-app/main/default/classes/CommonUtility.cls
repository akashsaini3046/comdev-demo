public with sharing class CommonUtility {

    public static String getCommunityName(){
        
        String nwid = Network.getNetworkId();
        if (!String.isBlank(nwid)) {
            String communityName = [SELECT name from Network where id =: nwid][0].name ;
            return communityName;
    	}
        else{
            return null;
        }
    }

    public static String getCommunityUrlPathPrefix(){
        
        String nwid = Network.getNetworkId();
        if (!String.isBlank(nwid)) {
            String communityUrlPathPrefix = [SELECT UrlPathPrefix from Network where id =: nwid][0].UrlPathPrefix ;
            return communityUrlPathPrefix;
    	}
        else{
            return null;
        }
    }
}