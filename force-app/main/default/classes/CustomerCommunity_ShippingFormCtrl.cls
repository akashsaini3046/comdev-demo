public with sharing class CustomerCommunity_ShippingFormCtrl {
    
    @AuraEnabled 
    public static List<CustomerCommuntity_FieldSetMemberWrapper> fetchObjects(String shippingInstructionId){
        
        Map<String, String> mapObjectRecordId = new Map<String, String>();
        List<ShippingInstructionFormFields__mdt> listShippingFormFields = [Select Label,DeveloperName,ShippingFieldSet__c,SequenceNumber__c,Parent__c,customLabel__c 
                                                                           FROM ShippingInstructionFormFields__mdt ORDER BY SequenceNumber__c ASC];
        System.debug('listShippingFormFields--->'+listShippingFormFields);
        List<CustomerCommuntity_FieldSetMemberWrapper> wrapperList = new List<CustomerCommuntity_FieldSetMemberWrapper>();
        List<CustomerCommuntity_FieldSetMemberWrapper> listMultipleRecords = new List<CustomerCommuntity_FieldSetMemberWrapper>();
        
        try {
            if (listShippingFormFields != null) {
                for(ShippingInstructionFormFields__mdt objApiName : listShippingFormFields){
                    CustomerCommuntity_FieldSetMemberWrapper objWrapper = new CustomerCommuntity_FieldSetMemberWrapper();
                    objWrapper.objectLabelName = String.valueOf(objApiName.customLabel__c);
                    objWrapper.objectApiName = String.valueOf(objApiName.Label);
                    objWrapper.sequence = objApiName.SequenceNumber__c;
                    objWrapper.multipleRecord = False;
                    objWrapper.parentName = objApiName.Parent__c;
                    objWrapper.listFieldLabelWrapper = MapObjectsWithFields(objApiName);
                    String fieldNames = '';
                    for(CustomerCommunity_FieldLabelWrapper objFieldLabelWrapper : objWrapper.listFieldLabelWrapper){
                        fieldNames += objFieldLabelWrapper.fieldApiName + ', ';
                    }
                    fieldNames = fieldNames.removeEnd(', ');
                    objWrapper.fieldNamesconcat = fieldNames;
                    wrapperList.add(objWrapper);
                }
                System.debug('wrapperList --->' +wrapperList);
                for(CustomerCommuntity_FieldSetMemberWrapper objFieldSetMember: wrapperList){
                    if(!objFieldSetMember.multipleRecord){
                        List<Sobject> records = getRecordData(objFieldSetMember,mapObjectRecordId,shippingInstructionId);
                        System.debug('records to be send in frontend --->'+records);
                        if(records == NULL){
                            for(CustomerCommunity_FieldLabelWrapper objFieldLabel : objFieldSetMember.listFieldLabelWrapper){
                                objFieldLabel.record = NULL;
                                objFieldLabel.recordId = NULL;
                            } 
                        } 
                        if(records != NULL){
                            Integer iterationCount = 0;
                            String recIds = '(\'';
                            for(sObject obj : records){
                                recIds = recIds + obj.Id + '\', ';
                                if(iterationCount != 0){
                                    CustomerCommuntity_FieldSetMemberWrapper objWrapper = new CustomerCommuntity_FieldSetMemberWrapper();
                                    objWrapper.objectLabelName = objFieldSetMember.objectLabelName;
                                    objWrapper.objectApiName = objFieldSetMember.objectApiName;
                                    objWrapper.sequence = objFieldSetMember.sequence + Decimal.valueOf(iterationCount)/10;
                                    System.debug('objWrapper.sequence  -->'+objWrapper.sequence);
                                    objWrapper.multipleRecord = True;
                                   
                                    objWrapper.listFieldLabelWrapper = new List<CustomerCommunity_FieldLabelWrapper>();
                                    
                                    for(CustomerCommunity_FieldLabelWrapper objFieldData : objFieldSetMember.listFieldLabelWrapper){
                                        CustomerCommunity_FieldLabelWrapper objFieldLabel = new CustomerCommunity_FieldLabelWrapper();
                                        objFieldLabel.fieldApiName = objFieldData.fieldApiName;
                                        objFieldLabel.fieldLabel = objFieldData.fieldLabel;
                                        objFieldLabel.record = String.valueOf(obj.get(objFieldData.fieldApiName));
                                        objFieldLabel.recordId = String.valueOf(obj.Id);
                                        objFieldLabel.fieldType = objFieldData.fieldType;
                                        objFieldLabel.fieldVisibility = objFieldData.fieldVisibility; 
                                        
                                        objWrapper.listFieldLabelWrapper.add(objFieldLabel);
                                        System.debug('for multiple records --->' + objFieldData);
                                    }
                                    listMultipleRecords.add(objWrapper);
                                }
                                else{
                                    for(CustomerCommunity_FieldLabelWrapper objFieldData : objFieldSetMember.listFieldLabelWrapper){
                                        objFieldData.record = String.valueOf(obj.get(objFieldData.fieldApiName));
                                        objFieldData.recordId = String.valueOf(obj.Id);
                                    }
                                }
                                iterationCount++;
                            }
                            recIds = recIds.removeEnd(', ') + ')';
                            mapObjectRecordId.put(objFieldSetMember.objectApiName, recIds); 
                        }   
                    }
                }
                wrapperList.addAll(listMultipleRecords);
                System.debug('wrapper List --->'+wrapperList);
                wrapperList.sort();
                return wrapperList;
            }
            else {
                return null;
            }   
        } 
        catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }     
    }
    public static List<Sobject> getRecordData(CustomerCommuntity_FieldSetMemberWrapper objFieldSetMember,Map<String, String> mapObjectRecordId,String shippingInstructionId){
        
        String query = '';
        String fieldNames = '';
        String objectApiName = '';
        //Map<String, List<String>> mapFieldNameValue = new Map<String, List<String>>();
        List<Sobject> records = new List<SObject>();
        fieldNames = objFieldSetMember.fieldNamesconcat;
        System.debug('fieldNames --->'+fieldNames);
        query = 'select '+fieldNames+' from '+ objFieldSetMember.objectApiName;
        if(objFieldSetMember.sequence == 1){
            query = query + ' WHERE Id = \'' + shippingInstructionId + '\'';
            System.debug('query --->'+query);
            records = Database.query(query);
        }
        else if(!mapObjectRecordId.isEmpty() && mapObjectRecordId.containsKey(objFieldSetMember.parentName)) {
            query = query + ' WHERE ' + objFieldSetMember.parentName + ' IN ' + mapObjectRecordId.get(objFieldSetMember.parentName);
            System.debug('query --->'+query);
            records = Database.query(query);
        }            
        System.debug('records --->'+records.size()+records);
        if(records.size() >0)
        return records;
        else
        return NULL;
    }
    public static List<CustomerCommunity_FieldLabelWrapper> MapObjectsWithFields(ShippingInstructionFormFields__mdt objApiName){
        
        try{
            
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objApiName.Label);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(objApiName.ShippingFieldSet__c);
            List<CustomerCommunity_FieldLabelWrapper> listFieldLabelWrapper = new List<CustomerCommunity_FieldLabelWrapper>();
            List<Schema.FieldSetMember> listSchemaField = fieldSetObj.getFields();
            if(listSchemaField != null){
                for(Schema.FieldSetMember objFieldSetMember : listSchemaField){
                    CustomerCommunity_FieldLabelWrapper objFieldWrapper = new CustomerCommunity_FieldLabelWrapper();
                    objFieldWrapper.fieldApiName = objFieldSetMember.getFieldPath();
                    objFieldWrapper.fieldLabel = objFieldSetMember.getLabel();
                    objFieldWrapper.fieldType = String.valueOf(objFieldSetMember.getType());
                    if(objFieldSetMember.getType() == Schema.DisplayType.REFERENCE){
                        objFieldWrapper.fieldVisibility = false;
                    }
                    else{
                        objFieldWrapper.fieldVisibility = true;
                    }
                    listFieldLabelWrapper.add(objFieldWrapper);
                }
            }
            if(listFieldLabelWrapper != null){
                System.debug('listFieldLabelWrapper -->'+listFieldLabelWrapper);
                return listFieldLabelWrapper;
            }
            else{
                return null;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;  
        }
    }
    @AuraEnabled
    public static String saveRecordData(List<CustomerCommuntity_FieldSetMemberWrapper> listWrapper){
        List<Sobject> objects = new List<Sobject>();
        boolean result;
        System.debug('listWrapper --->'+listWrapper);
        Integer indexCounter = 0;
        for(CustomerCommuntity_FieldSetMemberWrapper objFieldMember : listWrapper){
            List<CustomerCommunity_FieldLabelWrapper> listFieldLabel = objFieldMember.listFieldLabelWrapper;
            sObject currentObject =  Schema.getGlobalDescribe().get(objFieldMember.objectApiName).newSObject(listFieldLabel.get(indexCounter).recordId) ;
            for(CustomerCommunity_FieldLabelWrapper objFieldLabel : listFieldLabel){
                Schema.DisplayType fieldDataType = Schema.getGlobalDescribe().get(objFieldMember.objectApiName).getDescribe().fields.getMap().
                    get(objFieldLabel.fieldApiName).getDescribe().getType();
                
                if(fieldDataType == Schema.DisplayType.String) {
                    String StringValue = String.valueOf(objFieldLabel.record);
                    currentObject.put(objFieldLabel.fieldApiName, StringValue);
                }
                else if(fieldDataType == Schema.DisplayType.Integer) {
                    Integer IntValue = Integer.valueOf(objFieldLabel.record);
                    currentObject.put(objFieldLabel.fieldApiName, IntValue);
                }  
                else if(fieldDataType == Schema.DisplayType.DATETIME) {     
                    Datetime dateValue = Datetime.valueOf(objFieldLabel.record);
                    currentObject.put(objFieldLabel.fieldApiName, dateValue);
                }    
                /*else if(fieldDataType == Schema.DisplayType.TEXTAREA) {
                    TextArea dateValue = TextArea.valueOf(objFieldLabel.record);
                    currentObject.put(objFieldLabel.fieldApiName, dateValue);
                } */     
            }
            objects.add(currentObject);
            indexCounter++;
        }
           
            System.debug('objects list --->'+objects);
            Database.SaveResult[] srList = Database.update(objects, false);
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    System.debug('Successfully inserted Sobject : ' + sr.getId());
                    result = true;
                }
                else {             
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Sobjects fields that affected this error: ' + err.getFields());
                        result = false;
                    }
                }
            }
        if(result){
            return 'TRUE';
        }
        else{
            return 'FALSE';
        }
        }
    }