public class CustomerCommunity_DocumentsWrapper implements Comparable {
   @AuraEnabled public Id DocumentId {get;private set;}
   @AuraEnabled public String BookingName {get;private set;}
   @AuraEnabled public String BookingNumber {get;private set;} 
   @AuraEnabled public String Name {get;private set;}
   @AuraEnabled public String DocumentType {get; private set;}
   @AuraEnabled public String CreatedName {get;private set;}
   @AuraEnabled public String CreatedById {get;private set;}
   @AuraEnabled public Datetime CreatedDate {get;private set;}
   @AuraEnabled public String Status {get;private set;}
   public CustomerCommunity_DocumentsWrapper(Id documentId,String bookingNumber,String bookingName,String name,String documentType,String createdName,String createdById,Datetime createdDate,String status){
       this.DocumentId = documentId;
       this.BookingNumber = bookingNumber;
       this.BookingName = bookingName;
       this.Name = name;
       this.DocumentType = documentType;
       this.CreatedName = createdName;
       this.CreatedById = createdById;
       this.CreatedDate = createdDate;
       this.Status = status;
   }
    public Integer compareTo(Object objToCompare){
      CustomerCommunity_DocumentsWrapper currentDocument = (CustomerCommunity_DocumentsWrapper)(objToCompare);

    if (this.CreatedDate < currentDocument.CreatedDate) {
        return 1;
    }
    if (this.CreatedDate == currentDocument.CreatedDate) {
        return 0;
    }
    return -1;
   }
}