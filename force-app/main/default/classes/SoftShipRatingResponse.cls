public class SoftShipRatingResponse{
    
    public String Action{get;set;}
    public String maxRoutes{get;set;}
    public String CalcRule{get;set;}
    public Login Login{get;set;}
    public Booking Booking{get;set;}
    public class WidthCentimeter{
        public Decimal Amount{get;set;}
        public Integer Unit{get;set;}
    }
    public class WeightKilogram{
        public Integer Value{get;set;}
        public Integer Unit{get;set;}
    }
    public class StartLocation{
        public LocationCode LocationCode{get;set;}
    }
    public class RequestedBookingRoute{
        public Legs Legs{get;set;}
    }
    public class Login{
        public String Username{get;set;}
        public String Password{get;set;}
    }
    public class LocationCode{
        public String Code{get;set;}
    }
    public class LengthCentimeter{
        public Integer Unit{get;set;}
        public Decimal Amount{get;set;}
    }
    public class LegSequence{
        public Integer Sequence{get;set;}
    }
    public class Legs{
        public list<cargoValue> cargoValue{get;set;}
        public String type{get;set;}
    }
    public class KindOfPackage{
        public String Code{get;set;}
        public String Description{get;set;}
    }
    public class HeightCentimeter{
        public Decimal Amount{get;set;}
        public Integer Unit{get;set;}
    }
    public class EndLocation{
        public LocationCode LocationCode{get;set;}
    }
    public class CustomerCode{
        public String Code{get;set;}
    }
    public class Customer{
        public CustomerCode CustomerCode{get;set;}
    }
    public class currencyValue{
        public String Code{get;set;}
    }
    public class Contract{
        public String ContractNumber{get;set;}
    }
    public class Commodity{
        public String Code{get;set;}
    }
    public class CarModelId{
        public String Type{get;set;}
        public String Model{get;set;}
        public String Manufacturer{get;set;}
    }
    public class CargoItemId{
        public Integer ItemRunningNumber{get;set;}
    }
    public class Cargo{
        public WidthCentimeter WidthCentimeter{get;set;}
        public HeightCentimeter HeightCentimeter{get;set;}
        public WeightKilogram WeightKilogram{get;set;}
        public LengthCentimeter LengthCentimeter{get;set;}
        public String DescriptionOfGoods{get;set;}
        public CarModelId CarModelId{get;set;}
        public KindOfPackage KindOfPackage{get;set;}
        public String Vin{get;set;}
        public Commodity Commodity{get;set;}
        public String type{get;set;}
    }
    public class BookingCargo{
        public list<cargoValue> cargoValue{get;set;}
        public String type{get;set;}
    }
    public class Booking{
        public BookingCargo BookingCargo{get;set;}
        public currencyValue currencyValue{get;set;}
        public String ShipmentType{get;set;}
        public RequestedBookingRoute RequestedBookingRoute{get;set;}
        public Contract Contract{get;set;}
        public Customer Customer{get;set;}
    }
    public class cargoValue{
        public list<Cargo> Cargo{get;set;}
        public String DeliveryTermCode{get;set;}
        public String type{get;set;}
        public String ReadyDate{get;set;}
        public Commodity Commodity{get;set;}
        public String ReceiptTermCode{get;set;}
        public String DescriptionOfGoods{get;set;}
        public EndLocation EndLocation{get;set;}
        public KindOfPackage KindOfPackage{get;set;}
        public StartLocation StartLocation{get;set;}
        public CargoItemId CargoItemId{get;set;}
        public LegSequence LegSequence{get;set;}
    }
}