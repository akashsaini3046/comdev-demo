public class UserDAO implements IUserDAO{
    Integer queryLimitRows = Limits.getLimitQueryRows();
	public interface IUserDAO
    {
        List<User> getAllUsers();
        List<User> getUsersByQueryParam(String queryCondition,String queryParam);
    }
 
    public List<User> getAllUsers() {
        return Database.query(
            'SELECT Id, FirstName, LastName, Email, IsActive, Contact, Profile, UserRole, Name, Username ' +
            'FROM User ' +
            'LIMIT: '+ queryLimitRows 
        );
    }
    
    public List<User> getUsersByQueryParam(String queryCondition,String queryParam) {
        
        return Database.query(
            'SELECT IId, FirstName, LastName, Email, IsActive, Contact, Profile, UserRole, Name, Username  ' +
            'FROM User ' +
            'WHERE ' + queryCondition + '='+'\''+queryParam+'\''+
            ' LIMIT  '+ queryLimitRows
        );
    }
}