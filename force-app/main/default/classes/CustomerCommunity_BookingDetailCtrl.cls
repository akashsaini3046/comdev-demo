public without sharing class CustomerCommunity_BookingDetailCtrl {
    @AuraEnabled
    public static BookingDetailWrapper getBookingDetail(Id bookingId){   
        try {
            BookingDetailWrapper bookingDetailsResponse = new BookingDetailWrapper();
            
            // Query : Transport Details 
            String queryTransport =  'SELECT Id';
            for(Schema.FieldSetMember fieldMember : SObjectType.Transport__c.FieldSets.Transport_Details.getFields()){
                queryTransport += ', ' + fieldMember.getFieldPath(); 
            }
            queryTransport += ' FROM Transports__r';
            
            // Query : Booking Remark Details 
            String queryBookingRemarks =  'SELECT Id';
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking_Remark__c.FieldSets.Booking_Remarks_Details.getFields()){
                queryBookingRemarks += ', ' + fieldMember.getFieldPath(); 
            }
            queryBookingRemarks += ' FROM Booking_Remarks__r';
            
            // Query : Shipment Details 
            String queryShipments =  'SELECT Id';
            for(Schema.FieldSetMember fieldMember : SObjectType.Shipment__c.FieldSets.Shipping_Details.getFields()){
                queryShipments += ', ' + fieldMember.getFieldPath(); 
            }
            queryShipments += ' FROM Shipments__r';
            
            // Query : Parties Details 
            String queryParties =  'SELECT Id';
            for(Schema.FieldSetMember fieldMember : SObjectType.Party__c.FieldSets.Party_Details.getFields()){
                queryParties += ', ' + fieldMember.getFieldPath(); 
            }
            queryParties += ' FROM Parties__r';
            
            // Query : Booking Details (Combined)
            String queryBooking = 'SELECT Id';
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Booking_Detail.getFields()){
                queryBooking += ', ' + fieldMember.getFieldPath(); 
            }
            queryBooking += ', ('+ queryParties + ')';
            queryBooking += ', ('+ queryShipments + ')';
            queryBooking += ', ('+ queryTransport + ')';
            queryBooking += ', ('+ queryBookingRemarks + ')';
            queryBooking += ' FROM Booking__c WHERE Id = :bookingId LIMIT 1';
            System.debug(queryBooking);
            Booking__c booking = Database.query(queryBooking);
            
            // Fetching details of the Shipment and related objects
            List<ShipmentDetailsWrapper> shipmentRecords = new List<ShipmentDetailsWrapper>();
            List<FreightDetailsWrapper> freightRecords = new List<FreightDetailsWrapper>();
            Map<Id, List<RequirementWrapper>> freightIdVsRequirementWrapperList = new Map<Id, List<RequirementWrapper>>();
            Map<Id, List<FreightDetailsWrapper>> shipmentIdVsFreightWrapperList = new Map<Id, List<FreightDetailsWrapper>>();
            
            List<Shipment__c> shipmentRecordsList = new List<Shipment__c>();
            Set<Id> shipmentIds = new Set<Id>();
            for(Shipment__c shipment : booking.Shipments__r){
                shipmentIds.add(shipment.Id);
            }
            if(shipmentIds != null && !shipmentIds.isEmpty()){
                // Query : Voyage Details 
                String queryVoyage =  'SELECT Id';
                for(Schema.FieldSetMember fieldMember : SObjectType.Voyage__c.FieldSets.Voyage_Details.getFields()){
                    queryVoyage += ', ' + fieldMember.getFieldPath(); 
                }
                queryVoyage += ' FROM Voyages__r';
                
                // Query : Freight Details 
                String queryfreightDetail =  'SELECT Id';
                for(Schema.FieldSetMember fieldMember : SObjectType.FreightDetail__c.FieldSets.Freight_Details.getFields()){
                    queryfreightDetail += ', ' + fieldMember.getFieldPath(); 
                }
                queryfreightDetail += ' FROM Freight_Details__r';
                
                // Query : Dock Receipt Details
                String queryDockReceipt =  'SELECT Id';
                for(Schema.FieldSetMember fieldMember : SObjectType.Dock_Receipt__c.FieldSets.Dock_Receipt_Details.getFields()){
                    queryDockReceipt += ', ' + fieldMember.getFieldPath(); 
                }
                queryDockReceipt += ' FROM Dock_Receipts__r';
                
                String queryShipmentRelated = 'SELECT Id';
                queryShipmentRelated += ', ('+ queryVoyage + ')';
                queryShipmentRelated += ', ('+ queryfreightDetail + ')';
                queryShipmentRelated += ', ('+ queryDockReceipt + ')';
                queryShipmentRelated += ' FROM Shipment__c WHERE Id IN :shipmentIds';
                
                shipmentRecordsList = Database.query(queryShipmentRelated);
				
                for(Shipment__c shipment : shipmentRecordsList){
                    ShipmentDetailsWrapper shipmentWrapper = new ShipmentDetailsWrapper();
                    shipmentWrapper.shipment = shipment;
                    shipmentRecords.add(shipmentWrapper);
                }
                
                // Fetching details of the Freight Details and related objects
                List<FreightDetail__c> freightDetailRecordAllShipments = new List<FreightDetail__c>();
                List<FreightDetail__c> freightDetailRecordsList = new List<FreightDetail__c>();
                for(Shipment__c shipment : shipmentRecordsList){
                    if(shipment.Freight_Details__r != null){
                        freightDetailRecordAllShipments.addAll(shipment.Freight_Details__r);
                    }
                }
                
                if(freightDetailRecordAllShipments != null && !freightDetailRecordAllShipments.isEmpty()){
                    Set<Id> freightDetailIds = (new Map<Id, FreightDetail__c>(freightDetailRecordAllShipments)).keySet();
                    
                    // Query : Commodity Details
                    String queryCommodity =  'SELECT Id';
                    for(Schema.FieldSetMember fieldMember : SObjectType.Commodity__c.FieldSets.Commodity_Details.getFields()){
                        queryCommodity += ', ' + fieldMember.getFieldPath(); 
                    }
                    queryCommodity += ' FROM Commodities__r';
                    
                    // Query : Requirement Details
                    String queryRequirement =  'SELECT Id';
                    for(Schema.FieldSetMember fieldMember : SObjectType.Requirement__c.FieldSets.Requirement_Details.getFields()){
                        queryRequirement += ', ' + fieldMember.getFieldPath(); 
                    }
                    queryRequirement += ' FROM Requirements__r';
                    
                    String queryFreightRelated = 'SELECT Id, Shipment__c';
                    queryFreightRelated += ', ('+ queryCommodity + ')';
                    queryFreightRelated += ', ('+ queryRequirement + ')';
                    queryFreightRelated += ' FROM FreightDetail__c WHERE Id IN :freightDetailIds';
                    
                    freightDetailRecordsList = Database.query(queryFreightRelated);
                    
                    for(FreightDetail__c freightDetail : freightDetailRecordsList){
                        FreightDetailsWrapper freightWrapper = new FreightDetailsWrapper();
                        freightWrapper.freightDetail = freightDetail;
                        freightRecords.add(freightWrapper);
                        if(shipmentIdVsFreightWrapperList != null && shipmentIdVsFreightWrapperList.get(freightDetail.Shipment__c) != null){
                            shipmentIdVsFreightWrapperList.get(freightDetail.Shipment__c).add(freightWrapper);
                        }else{
                            List<FreightDetailsWrapper> freiWrapper = new List<FreightDetailsWrapper>();
                            freiWrapper.add(freightWrapper);
                            shipmentIdVsFreightWrapperList.put(freightDetail.Shipment__c, freiWrapper);
                        }
                    }
                    
                    List<Requirement__c> requirementRecordAllFreight = new List<Requirement__c>();
                    List<Requirement__c> requirementRecordsList = new List<Requirement__c>();
                    for(FreightDetail__c freightDetail : freightDetailRecordsList){
                        if(freightDetail.Requirements__r != null){
                            requirementRecordAllFreight.addAll(freightDetail.Requirements__r);
                        }
                    }
                    
                    if(requirementRecordAllFreight != null && !requirementRecordAllFreight.isEmpty()){
                        Set<Id> requirementIds = (new Map<Id, Requirement__c>(requirementRecordAllFreight)).keySet();
                        
                        // Query : Equipment Details
                        String queryEquipment =  'SELECT Id';
                        for(Schema.FieldSetMember fieldMember : SObjectType.Equipment__c.FieldSets.Equipment_Details.getFields()){
                            queryEquipment += ', ' + fieldMember.getFieldPath(); 
                        }
                        queryEquipment += ' FROM Equipments__r';
                        
                        String queryRequirementRelated = 'SELECT Id, Freight__c';
                        queryRequirementRelated += ', ('+ queryEquipment + ')';
                        queryRequirementRelated += ' FROM Requirement__c WHERE Id IN :requirementIds';
                        System.debug('queryRequirementRelated' + queryRequirementRelated);
                        requirementRecordsList = Database.query(queryRequirementRelated);
                        
                        for(Requirement__c requirement : requirementRecordsList){
                            RequirementWrapper requirementWrapper = new RequirementWrapper();
                            requirementWrapper.requirement = requirement;
                            requirementWrapper.equipementRecords = requirement.Equipments__r;
                            if(freightIdVsRequirementWrapperList != null && freightIdVsRequirementWrapperList.get(requirement.Freight__c) != null){
                                freightIdVsRequirementWrapperList.get(requirement.Freight__c).add(requirementWrapper);
                            }else{
                                List<RequirementWrapper> reqWrapper = new List<RequirementWrapper>();
                                reqWrapper.add(requirementWrapper);
                                freightIdVsRequirementWrapperList.put(requirement.Freight__c, reqWrapper);
                            }
                        }
                    }
                }
                
                if(freightRecords != null){
                    for(FreightDetailsWrapper freightWrapperRecord : freightRecords){
                        freightWrapperRecord.requirementRecords = freightIdVsRequirementWrapperList.get(freightWrapperRecord.freightDetail.Id);
                    }
                }
                
                if(shipmentRecords != null){
                    for(ShipmentDetailsWrapper shipmentWrapperRecord : shipmentRecords){
                        shipmentWrapperRecord.freightDetailRecords = shipmentIdVsFreightWrapperList.get(shipmentWrapperRecord.shipment.Id);
                    }
                }
                
                bookingDetailsResponse.shipmentRecords = shipmentRecords;
            }
            
            // Fetching details of the Transport and related objects
            List<Transport__c> transportRecords = new List<Transport__c>();
            Set<Id> transportIds = new Set<Id>();
            for(Transport__c transport : booking.Transports__r){
                transportIds.add(transport.Id);
            }
            System.debug(transportIds);
            if(transportIds != null && !transportIds.isEmpty()){
                // Query : Stop Details 
                String queryStops =  'SELECT Id';
                for(Schema.FieldSetMember fieldMember : SObjectType.Stop__c.FieldSets.Stop_Details.getFields()){
                    queryStops += ', ' + fieldMember.getFieldPath(); 
                }
                queryStops += ' FROM Stops__r';
                transportRecords = Database.query('SELECT Id, ('+ queryStops +') FROM Transport__c WHERE Id IN :transportIds');
                bookingDetailsResponse.transportRecords = transportRecords;
            }
            
            List<ContentDocumentLink> listContentDocumentLink = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :bookingId];
            System.debug('listContentDocumentLink@@'+listContentDocumentLink);
            List<DocumentWrapper> documentsWrapper = new List<DocumentWrapper>();
            Set<Id> contentDocumentIds = new Set<Id>(); 
            for(ContentDocumentLink contentDocument : listContentDocumentLink){
                contentDocumentIds.add(contentDocument.ContentDocumentId);
            }
            System.debug('contentDocumentIds@@'+contentDocumentIds);
            List<ContentDocument> listContentDocument = [SELECT Id, FileType, Title FROM ContentDocument WHERE Id IN :contentDocumentIds];
            for(ContentDocument contentDocument : listContentDocument){
                DocumentWrapper documentWrpapper = new DocumentWrapper();
                documentWrpapper.documentId = contentDocument.Id;
                documentWrpapper.documentName = contentDocument.Title;
                documentWrpapper.documentType = contentDocument.FileType;
                documentsWrapper.add(documentWrpapper);
            }
            
            if(documentsWrapper != null && !documentsWrapper.isEmpty()){
                bookingDetailsResponse.documentsWrapper = documentsWrapper;
            }
            
            bookingDetailsResponse.bookingRecord = booking;
            System.debug('bookingDetailsResponse@@@'+bookingDetailsResponse);
            return bookingDetailsResponse;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    public class BookingDetailWrapper {
        @AuraEnabled public Booking__c bookingRecord;
        @AuraEnabled public List<Transport__c> transportRecords;
        @AuraEnabled public List<ShipmentDetailsWrapper> shipmentRecords;
        @AuraEnabled public List<DocumentWrapper> documentsWrapper;
    }
    
    public class ShipmentDetailsWrapper {
        @AuraEnabled public Shipment__c shipment;
        @AuraEnabled public List<FreightDetailsWrapper> freightDetailRecords;
    }
    
    public class FreightDetailsWrapper {
        @AuraEnabled public FreightDetail__c freightDetail;
        @AuraEnabled public List<RequirementWrapper> requirementRecords;
    }
    
    public class RequirementWrapper {
        @AuraEnabled public Requirement__c requirement;
        @AuraEnabled public List<Equipment__c> equipementRecords;
    }
    
    public class DocumentWrapper {
        @AuraEnabled public Id documentId;
        @AuraEnabled public String documentName;
        @AuraEnabled public String documentType;
    }
}