global without sharing class CustomerCommunity_ShippingRelated {
    @AuraEnabled
    public static List<Charge_Line__c> fetchChargeLine(Id recordId){
        try{
            List<TLIEquipType__c> listTliEqipType = [Select id FROM TLIEquipType__c WHERE Header__c = :recordId];
            Set<ID> setTliEqipType = new Set<ID>();
            if(!listTliEqipType.isEmpty()){
                for(TLIEquipType__c objTliEqipType : listTliEqipType){
                    setTliEqipType.add(objTliEqipType.ID);
                }
                List<Charge_Line__c> listChargeLine = [Select Basis__c,Bill_To__c,Charge_Code__c,Line_Total__c,Prepaid_Collect__c,Quantity__c,Rate__c,TLI_Equip_Type__c FROM Charge_Line__c WHERE TLI_Equip_Type__c in :setTliEqipType];
                return listChargeLine;    
            }
            else{
                return NULL;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL; 
        } 
    }
    @AuraEnabled
    public static List<Equipment_BOL__c> fetchEquipments(Id recordId){
        try{
            List<TLIEquipType__c> listTliEqipType = [Select id FROM TLIEquipType__c WHERE Header__c = :recordId];
            Set<ID> setTliEqipType = new Set<ID>();
            if(!listTliEqipType.isEmpty()){
                for(TLIEquipType__c objTliEqipType : listTliEqipType){
                    setTliEqipType.add(objTliEqipType.ID);
                } 
                List<Equipment_BOL__c> listEquipments = [Select Name,Seal_2__c,TLIEquipType__c,VGMCompanyName__c,VGMWeight__c,Volume__c,Weight__c FROM Equipment_BOL__c WHERE TLIEquipType__c in :setTliEqipType];
                return listEquipments;
            }
            else{
                return NULL;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;  
        }
    }
    @AuraEnabled
    public static List<Line_Item__c> fetchLineItems(Id recordId){
        try{
            List<TLIEquipType__c> listTliEqipType = [Select id FROM TLIEquipType__c WHERE Header__c = :recordId];
            Set<ID> setTliEqipType = new Set<ID>();
            Set<ID> setEquipment = new Set<ID>();
            if(!listTliEqipType.isEmpty()){
                for(TLIEquipType__c objTliEqipType : listTliEqipType){
                    setTliEqipType.add(objTliEqipType.ID);
                } 
                List<Equipment_BOL__c> listEquipments = [Select Name,Prefix__c,	RunningReeferInd__c,Seal__c,Seal_2__c FROM Equipment_BOL__c WHERE TLIEquipType__c in :setTliEqipType];
                for(Equipment_BOL__c objEquipmentType : listEquipments){
                    setEquipment.add(objEquipmentType.ID);
                }
                System.debug('listEquipments ---->' + listEquipments);
                List<Line_Item__c> listLineItem = [Select Bill_Freight_Type__c,Declared_Value_USD__c,Haz_Indicator__c,Lading_Quantity__c,Package_Type_Code__c,Volume__c,Weight__c,Equipment__c FROM Line_Item__c WHERE Equipment__c in :setEquipment];
                System.debug('listLineItem ----->'+listLineItem);
                return listLineItem;
            }
            else{
                return NULL;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;  
        }
    }
}