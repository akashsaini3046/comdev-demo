public class CustomerCommunity_SetVariableChatbot {
    public class InputVariable {
            @InvocableVariable(required=true)
            public String sinputVariable;
        }
        public class OutputVariable {
            @InvocableVariable(required=true)
            public String soutputVariable;
        }
        @InvocableMethod(label='Set Variable')
        public static List<OutputVariable> setVariable(List<InputVariable> listInput) {
            List<OutputVariable> listResponses = new List<OutputVariable>();
            OutputVariable outputResponse = new OutputVariable();
            outputResponse.soutputVariable = CustomerCommunity_Constants.EMPTY_STRING;
            try{
                if(listInput[0].sinputVariable != Null){
                	outputResponse.soutputVariable = listInput[0].sinputVariable;
                }
                listResponses.add(outputResponse);
                return listResponses;
            }
            catch(Exception ex){
                ExceptionHandler.logApexCalloutError(ex);
                listResponses.add(outputResponse);
                return listResponses;
            }
        }
}