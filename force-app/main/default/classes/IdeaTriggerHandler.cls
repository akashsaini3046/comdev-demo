/*Purpose:  This class is to handle Idea realted actions
===========================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL 
1.0         Nagarro   3/12/2019   Handler Class
===========================================================================================================================================
*/
public with sharing class IdeaTriggerHandler extends TriggerHandler {
    private static Boolean recursionVarBeforeInsert = true;
    private static Boolean recursionVarAfterInsert = true;
    private static Boolean recursionVarBeforeUpdate = true;
    private static Boolean recursionVarAfterUpdate = true;
    private static Boolean recursionVarBeforeDelete = true;
    private static Boolean recursionVarAfterDelete = true;
    private static Boolean Flag = true;
    private static Boolean recflag;
    List<Idea> newIdeaList;
    List<Idea> oldIdeaList;
    Map<Id, Idea> newIdeaMap;
    Map<Id, Idea> oldIdeaMap;
    
    /*Create the constructor*/
    public IdeaTriggerHandler(){
        this.newIdeaList     = (List<Idea>) Trigger.new; 
        this.oldIdeaList     = (List<Idea>) Trigger.old; 
        this.newIdeaMap      = (Map<Id, Idea>) Trigger.newMap;
        this.oldIdeaMap      = (Map<Id, Idea>) Trigger.oldMap;
    }
    
    
    /*Override the before insert method from the parent class*/
    public override void beforeInsert(){
        if(recursionVarBeforeInsert){
            recursionVarBeforeInsert = false;
        }
    }
    
    /*Override the after insert method from the parent class*/
    public override void afterInsert(){
        if(recursionVarAfterInsert){
            recursionVarAfterInsert = false;
            CommunityEmailService.sendNotification_IdeaPost(this.newIdeaList);
        }
    }
    
    public override void beforeUpdate(){
        if(recursionVarBeforeUpdate){
            recursionVarBeforeUpdate = false;
        }
    }
    
    public override void afterUpdate(){
        if(recursionVarAfterUpdate){
            recursionVarAfterUpdate = false;
        
            List<Idea> implementedIdeas = new List<Idea>();
            for(Idea ideaObj: newIdeaList){
                System.debug('new status -> '+ideaObj.Status);
                System.debug('old status -> '+oldIdeaMap.get(ideaObj.id).Status);
                if(ideaObj.Status=='Implemented and Closed' && ideaObj.Status!=oldIdeaMap.get(ideaObj.id).Status){
                    implementedIdeas.add(ideaObj);
                }
            }
                
            CommunityEmailService.sendNotification_IdeaImplemented(implementedIdeas);
        }
    }
    
    /*Override before delete method*/
    public override void beforeDelete(){
        if(recursionVarBeforeDelete){
            recursionVarBeforeDelete = false;
        }
    }
    
    /*Override after delete method*/
    public override void afterDelete() {
        if(recursionVarAfterDelete){
            recursionVarAfterDelete = false;
        }
    }
    
    
    
}