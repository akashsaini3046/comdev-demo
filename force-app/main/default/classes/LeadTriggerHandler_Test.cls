@isTest
public class LeadTriggerHandler_Test {
    
    @testSetup static void setup() {
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' Limit 1].Id;
        User u = new User();
        u.LastName = 'Admin';
        u.Email = 'adminemail@test.com';
        u.Username = 'adminusertest@test.com';
        u.CompanyName = 'TEST';
        u.Title = 'title';
        u.Alias = 'aliasa';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.ProfileId = profileId;
        insert u;  
        
        CountryRegionMapping__c crm = new CountryRegionMapping__c();
        crm.Name='1';
        crm.Country_Name__c = 'United States';
        crm.Region__c = 'USA';
        insert crm;
        
        DescribeFieldResult describe = Lead.Origin_Country1__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
        
        Lead leadObj1 = new Lead();
        leadObj1.FirstName = 'Test';
        leadObj1.LastName = 'Test';
        leadObj1.Email = 'test@test.com';
        leadObj1.Phone = '9999999999';
        leadObj1.Company = 'ABC Ltd.';
        leadObj1.LeadSource = ConstantClass.LEADSOURCE;
        leadObj1.Industry='Carriers';
        leadObj1.Service__c='Air';
        leadObj1.title ='ASSOCIATE';
        leadObj1.Followup__c = TRUE;
        leadObj1.Origin_Country1__c = availableValues[0].getValue();
        leadObj1.OwnerId = u.Id;
        leadObj1.Country = 'TEST';
        leadObj1.City = 'TEST';
        leadObj1.State = 'TEST';
        leadObj1.Street = 'TEST';
        insert leadObj1;
        
        Account accObj1 = new Account();
        accObj1.Name = 'Account 123';
        accObj1.Created_From_Lead_Conversion__c = TRUE;
        insert accObj1;
        
        GovernanceTeamEmail__c govEmail = new GovernanceTeamEmail__c();
        govEmail.Name = 'test@test.com';
        insert govEmail;
        
        DescribeFieldResult describe1 = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValues1 = describe1.getPicklistValues();
        
        Address__c businessLocationObj = new Address__c();
        businessLocationObj.Account__c = accObj1.Id;
        businessLocationObj.Address_Line_1__c = 'address1';
        businessLocationObj.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj.Country__c = availableValues1[0].getValue();
        businessLocationObj.Postal_Code__c = '1111111';
        businessLocationObj.Phone__c = '88888888888';
        businessLocationObj.State__c = 'State1';
        businessLocationObj.Name ='BL1';
        insert businessLocationObj;
        
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        System.assertNotEquals(NULL, recordTypeId);
        
        Contact con= new Contact();
        con.LastName='Test con1';
        con.Phone = '99999999999';
        con.Email = 'contact@email.com';
        con.RecordTypeId = recordTypeId;
        con.Address__c = businessLocationObj.Id;
        con.Contact_Created_From_Lead__c = TRUE;
        insert con;
    }
    
    static testMethod void InsertTestMethod()
    {
        Lead leadObj1 = [Select Id, OwnerId, Company,IsConverted from lead];
        Contact con = [Select Id from Contact];
        Account acc = [Select Id from Account];
        
        System.assertNotEquals(NULL, leadObj1);
        System.assertNotEquals(NULL, con);
        System.assertNotEquals(NULL, acc);
        
        Task t = new Task();
        t.Subject = 'Lead Follow Up- '+ leadObj1.Company;
        t.Priority = 'Normal';
        t.Status = 'Open';
        t.TaskSubtype = 'Task';
        t.WhoId = leadObj1.Id;
        t.OwnerId = leadObj1.OwnerId;
        t.ActivityDate = System.today();
        insert t;
        System.assertNotEquals(NULL, t);
        t.Status  = 'Completed';
        update t;
        
        Test.startTest();
        
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadObj1.id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Qualified');
        lc.setAccountId(acc.Id);
        lc.setContactId(con.Id);
        
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        System.assert(lcr.isSuccess());
        System.assertEquals(leadObj1.OwnerId, t.OwnerId);
        
        Test.stopTest();        
        
        
    }
    
    static testMethod void UpdateTestMethod()
    {
        Lead leadObj1 = [SELECT Id, Followup__c FROM Lead LIMIT 1];
        System.assertNotEquals(NULL, leadObj1);
        
        Test.startTest();
        leadObj1.Followup__c = FALSE;
        update leadObj1;
        Test.stopTest();
    }
    
    static testMethod void DeleteTestMethod()
    {
        Lead leadObj1 = [SELECT Id FROM Lead LIMIT 1];
        
        Test.startTest();
        List<Lead> leadListToDelete = new List<Lead>();
        leadListToDelete.add(leadObj1);
        
        try
        {
            delete leadListToDelete;
        }
        catch(Exception e)
        {
            System.debug('Exception in Lead Test Class'+e.getMessage());
        }
        Test.stopTest();
    }
    
}