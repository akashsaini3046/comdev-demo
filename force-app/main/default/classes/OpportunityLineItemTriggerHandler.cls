public with sharing class OpportunityLineItemTriggerHandler extends TriggerHandler{
    List<OpportunityLineItem> newOpportunityLineItemList;
    List<OpportunityLineItem> oldOpportunityLineItemList;
    
    public static Boolean isRecursion = FALSE;
    
    //Constructor
    public OpportunityLineItemTriggerHandler(){
        this.newOpportunityLineItemList = (List<OpportunityLineItem>) Trigger.new;
        this.oldOpportunityLineItemList = (List<OpportunityLineItem>) Trigger.old;
    }
      
    public override void afterInsert(){
        if(isRecursion == FALSE && OpportunityTriggerHandler.isRecursion==FALSE)
        {
            updateServiceOnOpportunity(newOpportunityLineItemList);
        }
    }
    
    public override void afterDelete(){
     
     if(isRecursion == FALSE && OpportunityTriggerHandler.isRecursion==FALSE){
            updateServiceOnOpportunity(oldOpportunityLineItemList);
        }
    }
    
    
    public static void updateServiceOnOpportunity(List<OpportunityLineItem> olt){
       
        List<Id> listIds = new List<Id>();
        Map<Id,opportunity> mapOpportunityToUpdate = new Map<Id,opportunity>();
        Map<Id,OpportunityLineItem> mapToShowError = new Map<Id,OpportunityLineItem>();
        
        for (OpportunityLineItem childItem : olt) {
            listIds.add(childItem.OpportunityId);
            mapToShowError.put(childItem.OpportunityId, childItem);
        }
        
          list<Opportunity> parentOpp = new List<Opportunity>([SELECT id, Service_Type__c, Name,(SELECT ID, product2.name FROM OpportunityLineItems) FROM Opportunity WHERE ID IN :listIds]);
          
        for(Opportunity opp : parentOpp){
            
            if(!opp.OpportunityLineItems.isEmpty()){
    
                opp.Service_Type__c = '';
                
                for(OpportunityLineItem item:opp.OpportunityLineItems){
                    String service = String.valueOf((item.product2.name != null) ? item.product2.name : '') ;   
                    
                    if(!opp.Service_Type__c.contains(service)){
                        opp.Service_Type__c = opp.Service_Type__c+';'+service;
                        mapOpportunityToUpdate.put(opp.id, opp); 
                    }
                   
                   }
            }else{
                
                if(opp.opportunityLineItems.isEmpty()){
                    
                    if(mapToShowError.containsKey(opp.id)){
                        mapToShowError.get(opp.id).addError('There should be atleast one service related to an Opportunity.');
                    }
                }
            
            }
            
        }
        
         
        If(mapOpportunityToUpdate.values().size()>0){
        
            try{
                Database.update(mapOpportunityToUpdate.values(),true);  
            }
            catch(Exception ex){
                if(ex.getMessage().contains('Destination Port')){
                    for(opportunitylineitem oltt: olt){
                        oltt.addError('"Destination Port" is mandatory for Opportunity with customs service');
                    }
                }
            }    
        
        }    
    }

}