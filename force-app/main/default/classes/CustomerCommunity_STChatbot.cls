public class CustomerCommunity_STChatbot {
    
    public class TrackingIDs {
        @InvocableVariable(required=true)
        public String sTrackingId;
        @InvocableVariable(required=false)
        public String sContactId;
    }
    
    public class ResponseData {
        @InvocableVariable(required=true)
        public String sTrackingResponse;
    }
    
    @InvocableMethod(label='Shipment Tracking')
    public static List<ResponseData> TrackShipments(List<TrackingIDs> trackIds) {
        User currentUser ;
        String userLanguage = CustomerCommunity_Constants.CHATBOT_USERLANGUAGE_ENGLISH;
        String response, URL = CustomerCommunity_Constants.EMPTY_STRING, ServiceName = CustomerCommunity_Constants.ST_SERVICE_NAME, diffChar = CustomerCommunity_Constants.DIFF_CHAR;
        List<String> listTrackcingIds = new List<String>();
        List<ResponseData> listResponses = new List<ResponseData>();
        List<CustomerCommunity_ShipmentInformation> listTrackingInfo = new List<CustomerCommunity_ShipmentInformation>();
        List<Language_Translations__mdt> listTranslations = new List<Language_Translations__mdt>();
        Map<String, Language_Translations__mdt> mapLabelTranslations = new Map<String, Language_Translations__mdt>();
        try{
            if(trackIds[0].sContactId != Null && trackIds[0].sContactId != ''){
                currentUser = [SELECT Id, LanguageLocaleKey FROM User WHERE ContactId = :trackIds[0].sContactId];
                if(currentUser.LanguageLocaleKey == CustomerCommunity_Constants.SPANISH_LANGUAGECODE)
                    userLanguage = CustomerCommunity_Constants.CHATBOT_USERLANGUAGE_SPANISH;
            }
            listTranslations = [Select DeveloperName, Id, English__c, Spanish__c From Language_Translations__mdt];
            if(!listTranslations.isEmpty()){
                for(Language_Translations__mdt translation : listTranslations) {
                    mapLabelTranslations.put(translation.DeveloperName, translation);
                }
            }
            if(!trackIds.isEmpty()){
                URL = URL + trackIds[0].sTrackingId.trim();
                response = CustomerCommunity_APICallouts.getAPIResponse(ServiceName, Null, URL);
                if(response != Null && response != CustomerCommunity_Constants.EMPTY_STRING) {
                    response = response.substring(1, response.length()-1).replaceAll(CustomerCommunity_Constants.BACKSLASH_STRING,CustomerCommunity_Constants.EMPTY_STRING).replaceAll(CustomerCommunity_Constants.DATE_STRING,CustomerCommunity_Constants.DATEINFO_STRING).replaceAll(CustomerCommunity_Constants.NUMBER_STRING,CustomerCommunity_Constants.NUMBERINFO_STRING);
                }
                listTrackingInfo = (List<CustomerCommunity_ShipmentInformation>)JSON.deserialize(response, List<CustomerCommunity_ShipmentInformation>.class);
                if(listTrackingInfo != null && !mapLabelTranslations.isEmpty()){
                    ResponseData rd = new ResponseData();
                    rd.sTrackingResponse = CustomerCommunity_Constants.EMPTY_STRING;
                    for(CustomerCommunity_ShipmentInformation track : listTrackingInfo) {
                        if(mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_SHIPMENTID) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_SHIPMENTID).get(userLanguage) != Null && mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_STATUS) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_STATUS).get(userLanguage) != Null)
                            rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_SHIPMENTID).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.shipmentID + CustomerCommunity_Constants.NEWLINE_CHARACTER + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_STATUS).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.currentStatus + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                        if(!track.currentStatus.containsIgnoreCase(CustomerCommunity_Constants.ST_RESPONSE_NOTFOUND) && !track.currentStatus.containsIgnoreCase(CustomerCommunity_Constants.ST_RESPONSE_INVALIDFORMAT) && !track.currentStatus.containsIgnoreCase(CustomerCommunity_Constants.ST_RESPONSE_NOTBOOKED) && mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_STATUSDATE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_STATUSDATE).get(userLanguage) != Null) {
                            rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_STATUSDATE).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.statusDate + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                            if(track.estSailDate != CustomerCommunity_Constants.EMPTY_STRING && mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_ESTSAILINGDATE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_ESTSAILINGDATE).get(userLanguage) != Null)
                                rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_ESTSAILINGDATE).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.estSailDate + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                            if(track.shipmentType != CustomerCommunity_Constants.EMPTY_STRING && mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_SHIPMENTTYPE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_SHIPMENTTYPE).get(userLanguage) != Null)
                                rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_SHIPMENTTYPE).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.shipmentType + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                            if(track.voyage.vessel != CustomerCommunity_Constants.EMPTY_STRING && track.voyage.numberInfo != CustomerCommunity_Constants.EMPTY_STRING && mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_VESSELVOYAGE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_VESSELVOYAGE).get(userLanguage) != Null)
                                rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_VESSELVOYAGE).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.voyage.vessel + CustomerCommunity_Constants.SPACE_STRING + track.voyage.numberInfo + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                            if(track.discharge.dateInfo != CustomerCommunity_Constants.EMPTY_STRING && track.discharge.location != CustomerCommunity_Constants.EMPTY_STRING && mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_RESPONSE_DISCHARGEDATE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_DISCHARGEDATE).get(userLanguage) != Null)
                                rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_RESPONSE_DISCHARGEDATE).get(userLanguage) + CustomerCommunity_Constants.SPACE_STRING + track.discharge.dateInfo + CustomerCommunity_Constants.BACKSLASH_CHARACTER + track.discharge.location + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                        }
                        rd.sTrackingResponse = rd.sTrackingResponse + CustomerCommunity_Constants.NEWLINE_CHARACTER;
                    }
                    listResponses.add(rd);
                }
            }
            if(listResponses.isEmpty()) { 
                ResponseData rd = new ResponseData();
                rd.sTrackingResponse = CustomerCommunity_Constants.EMPTY_STRING;
                if(mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_CHATBOT_ERRORMESSAGE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_CHATBOT_ERRORMESSAGE).get(userLanguage) != Null)
                    rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_CHATBOT_ERRORMESSAGE).get(userLanguage);
                listResponses.add(rd);
            }
            return listResponses;  
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            ResponseData rd = new ResponseData();
            rd.sTrackingResponse = CustomerCommunity_Constants.EMPTY_STRING;
            if(mapLabelTranslations.containsKey(CustomerCommunity_Constants.ST_CHATBOT_ERRORMESSAGE) && mapLabelTranslations.get(CustomerCommunity_Constants.ST_CHATBOT_ERRORMESSAGE).get(userLanguage) != Null)
                rd.sTrackingResponse = rd.sTrackingResponse + mapLabelTranslations.get(CustomerCommunity_Constants.ST_CHATBOT_ERRORMESSAGE).get(userLanguage);
            listResponses.add(rd);
            return listResponses;
        }
    }
}