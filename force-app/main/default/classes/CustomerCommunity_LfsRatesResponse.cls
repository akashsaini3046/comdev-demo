global class CustomerCommunity_LfsRatesResponse{
	@AuraEnabled public Body Body{get;set;}
	public class Results{
		@AuraEnabled public list<Carrier> Carrier{get;set;}
	}
	public class RateRemarks{
		@AuraEnabled public Item Item{get;set;}
	}
	public class Item{
		@AuraEnabled public String Amount{get;set;}
		@AuraEnabled public String Description{get;set;}
	}
	public class GetRatesData{
		@AuraEnabled public Results Results{get;set;}
        @AuraEnabled public Error Error{get;set;}   
	}
    
    public class Error{
		@AuraEnabled public String Message{get;set;}
	}
	public class Carrier{
		@AuraEnabled public String QuoteNumber{get;set;}
		@AuraEnabled public String Total{get;set;}
		@AuraEnabled public String TransitDays{get;set;}
		@AuraEnabled public String ServiceLevel{get;set;}
		//@AuraEnabled public Breakdown Breakdown{get;set;}
		@AuraEnabled public String CarrierCode{get;set;}
		//@AuraEnabled public RateRemarks RateRemarks{get;set;}
		@AuraEnabled public String CarrierSCAC{get;set;}
		@AuraEnabled public String Guaranteed{get;set;}
		@AuraEnabled public String CarrierName{get;set;}
	}
	public class Breakdown{
		@AuraEnabled public List<Item> Item{get;set;}
	}
	global class Body{
        @AuraEnabled public Integer uniqueId{get; set;}
		@AuraEnabled public GetRatesData GetRatesData{get;set;}
        @AuraEnabled public String CarrierName{get;set;}
        @AuraEnabled public String ServiceLevel{get;set;}
        @AuraEnabled public String TransitDays{get;set;}
        @AuraEnabled public Decimal Rate{get;set;}
        @AuraEnabled public String CarrierCode{get;set;}
        @AuraEnabled public List<ChargeLine> ChargeLine{get;set;}
        @AuraEnabled public String isCFS{get;set;}
        @AuraEnabled public String TotalAmount{get;set;}  
        @AuraEnabled public Integer CWTransitDays {get;set;}
        @AuraEnabled public Decimal TotalRate {get;set;}
        @AuraEnabled public Integer TotalDays {get;set;}
        @AuraEnabled public String FromToLocation{get;set;}
        @AuraEnabled public Boolean isSaved{get;set;}
	}
    global class ChargeLine{
        @AuraEnabled public String ChargeCode {get;set;}
        @AuraEnabled public String ChargeCodeDesc {get;set;}
        @AuraEnabled public String SellOSAmount {get;set;}
    }
}