public class CrowleyCommunity_VerticalMenuController {
    @AuraEnabled
    public static List<Vertical_Menu_List_Mobile__mdt> getMenuItems(){
        List<Vertical_Menu_List_Mobile__mdt> list_MenuOptions = new List<Vertical_Menu_List_Mobile__mdt>();
        Integer newSequence = 1;
        try{
            list_MenuOptions = [SELECT Id, Description__c, MasterLabel, IsActive__c, Sequence__c, Style_Classes__c, Lightning_Component_Name__c
                                FROM Vertical_Menu_List_Mobile__mdt ORDER By Sequence__c ASC];
            for(Vertical_Menu_List_Mobile__mdt menu : list_MenuOptions) {
                menu.Sequence__c = newSequence++;
            }
            System.debug('list_MenuOptions'+list_MenuOptions);
            return list_MenuOptions;
            
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return list_MenuOptions;
        } 
    }
}