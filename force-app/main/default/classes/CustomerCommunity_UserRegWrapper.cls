public class CustomerCommunity_UserRegWrapper {
  @AuraEnabled public String FirstName {get;set;}
  @AuraEnabled public String Email {get;set;}
  @AuraEnabled public String CompanyName {get;set;}
  @AuraEnabled public String LastName {get;set;}
  @AuraEnabled public String UserName {get;set;}
  @AuraEnabled public String Title {get;set;}
  @AuraEnabled public String PhoneNumber {get;set;}   
  @AuraEnabled public String Country {get;set;}
}