/*
 * Name: ContactDAO
 * Purpose: This is the DAO for Contact.
 * Author: Nagarro
 * Created Date: 01-April-2020
 * Modification History
 * Modification #   Story/Defect#      Modified By     Date   Description
 */
public inherited sharing class ContactDAO implements IContactDAO {
    public Integer queryLimitRows = Limits.getLimitQueryRows();
    public interface IContactDAO {
        List < Contact > getContactByQueryParam(String queryCondition, String queryParam);
        List < Contact > getContactsByRecordType(String recordType);
    }

    public static List < Contact > getContactByQueryParam(String queryCondition, String queryParam) {

        return Database.query(
            'SELECT Id, FirstName, LastName, Title, Email ' +
            'FROM Contact ' +
            'WHERE ' + queryCondition + '=' + '\'' + queryParam + '\'' +
            'LIMIT ' + 10
        );
    }

    public List < Contact > getContactsByRecordType(String recordType) {

        return Database.query(
            'SELECT Id, FirstName, LastName, Title, Email ' +
            'FROM Contact ' +
            'where RecordType.DeveloperName = :' + recordType +
            'LIMIT ' + queryLimitRows
        );
    }
}