/*
 * @company     : Nagarro Inc.
 * @date        : 10-08-2020
 * @author      : Nagarro
 * @description : DAO for Bill Of Lading. and Related Objects
 * @history     : Version 1.0
 * @test class  : BillOfLadingDAOTest
 */
public class BillOfLadingDAO implements IBillOfLadingDAO{
      /*
     * @company     : Nagarro Inc.
     * @date        : 10-08-2020
     * @author      : Nagarro
     * @description : Interface for the DAO - Contains methods which must be implemented
     * @history     : Version 1.0
     */
	public interface IBillOfLadingDAO {
        List<Bill_Of_Lading__c> getBolRecords(Integer intLimit, Integer intOffset, String fieldName, String order, String status);
        Integer getTotalBillOfLadingsRecords();
        List<sObject> getGenericObjectsResult(String commaSeperatedFields,String objectAPIName,String parentObjectRelationName,List<Id>parentIdsList); 
        Bill_Of_Lading__c getBolRecordById(Id billOfLadingId);
        List<Party__c> getBOLPartyReords(Id billOfLadingId);
        List<Bill_Item__c> getBillItemRecords(Id billOfLadingId);
        List<Charge_Line__c> getChargeLineItems(id billOfLadingId);
        List<Equipment__c> getEquipmentRecords(id billOfLadingId);
    }
    
    /*
     * @purpose     : Method to get Bill Of Lading Records.
     * @parameter   : intLimit - Number of records to be fetched
     * @parameter   : intOffset - Starting position of the records to be fetched
     * @parameter   : fieldName - Fields name for sorting basis
     * @parameter   : order - Sorting Direction
     * @parameter   : status - Bill Of Lading Status for query condition
     * @return      : List<Bill_Of_Lading__c> - List of BillsOfLading records
     */
    public static List<Bill_Of_Lading__c> getBolRecords(Integer intLimit, Integer intOffset, String fieldName, String order,String status){
        try{
            String query = 'SELECT Id, Name, Bill_of_lading_Status__c,Booking_Reference_Number__c,Initiate_Date__c, Issue_Date__c,Booking_Number__c, Booking_Number__r.Booking_Number__c,  CreatedDate FROM Bill_Of_Lading__c WHERE Bill_of_lading_Status__c= :status ORDER BY '+fieldName+' '+ order + ' LIMIT ' +intLimit + ' OFFSET '+ intOffset ;
            List<Bill_Of_Lading__c> billsOfLading = Database.query(query);
            return billsOfLading;
        }
        catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getBolRecords', 'Bol Record Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
    
    /*
     * @purpose     : Method to get total number of Bill Of Lading Records with status 'RR'.
     * @return      : Integer - Total number of records.
     */
    public static Integer getTotalBillOfLadingsRecords(){
        try{
            AggregateResult results = [SELECT Count(Id) Total From Bill_Of_Lading__c WHERE Bill_of_lading_Status__c='RR'];
            Integer totalRecords = (Integer)results.get('Total') ; 
            return totalRecords;
        }
        catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getTotalBillOfLadingsRecords', ' Total Bol Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
    
     /*
     * @purpose     : Method to get BOL and related objects records based on the conditions and Object name specified.
     * @parameter   : commaSeperatedFields - Fields data to be fetched
     * @parameter   : objectAPIName - Object on which query has to be fired
     * @parameter   : parentObjectRelationName - 'NA' incase of BillOfLading , relationAPIName in case of child objects
     * @parameter   : parentIdsList - BillOfLadingId incase of BillOfLading and parentsIds in case of child Object
     * @return      : List<sObject> - Query Result records for sObject.
     */
    public static  List<sObject> getGenericObjectsResult(String commaSeperatedFields,String objectAPIName, String parentObjectRelationName,List<Id>parentIdsList){
        String queryString ='';
        if(parentObjectRelationName=='NA'){
            queryString = 'SELECT id, '+commaSeperatedFields +' FROM '+objectAPIName+ ' where id IN : parentIdsList';
        } else{
            queryString = 'SELECT id, '+commaSeperatedFields +' FROM '+objectAPIName+' where '+ parentObjectRelationName +' IN  : parentIdsList';
        }
        List<sObject> queryResult=(Database.query(queryString));
        return queryResult;
    }
    
    /*
     * @purpose     : Method to get BillOfLading Record.
     * @parameter   : billOfLadingId - BillOfLading Id
     * @return      : Bill_Of_Lading__c - BillOfLading Record.
     */
    public static Bill_Of_Lading__c getBolRecordById(Id billOfLadingId){
        try{
             Bill_Of_Lading__c bol = [SELECT id, name, Bill_Of_Lading_Number__c,Booking_Number__r.Booking_Number__c,Booking_Number__r.id,Booking_Reference_Number__c,Consignee_Reference_Number__c,Customer_Billing_Reference_Number__c,Numeric_Reference_Number__c,
                                Export_Identification_Number__c,Export_Reference_Number__c,Forwarder_Reference_Number__c,Inbound_Reference_Number__c,Option_4_Reference_Number__c,
                                Special_Reference_Number__c,Shipper_Reference_Number__c,Supplier_Reference_Number__c,Tax_Reference_Number__c,Value_Reference_Number__c,Mode_Type__c,Move_Type__c,
                                Initiate_Date__c,Issue_Date__c,SCAC_Code__c,Load_Port_Description__c,Control_Load_Port_Description__c,Discharge_Port_Description__c,Control_Discharge_Port_Description__c,
                                Point_Of_Origin_Description__c,Final_Destination_Description__c,Relay_Point_Description__c,Final_Destination__c,Final_Destination_State__c,Final_Destination_Country__c,
                                Origin_Voyage__c,Hazardous__c,Hazardous_Emergency_Contact__c,Hazardous_Emergency_Phone_Number_1__c,Hazardous_Emergency_Phone_Number_2__c,Prepaid_Collect__c,
                                Route__c,Move_Type_Description__c,BOL_Release_Description__c,Free_Text_Lines_DSC__c,
                                (Select Vessel_Name__c,Vessel_Country_Code__c from Voyages__r)
                                from Bill_Of_Lading__c where id=:billOfLadingId][0];
            return bol;
        }
        catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getBolRecordById', 'Bol Record Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
    
     /*
     * @purpose     : Method to get Party Records for given BillOfLading Id.
     * @parameter   : billOfLadingId - BillOfLading Id
     * @return      : List<Party__c> - Parties list related to given billOfLading.
     */
    public static List<Party__c> getBOLPartyReords(Id billOfLadingId){
        try{
            List<Party__c> partyList= [Select id,Name,Address_Line1__c,Address_Line2__c,Address_Line3__c,Address_Line4__c,CVIF__c,CVIF_Location_Code__c,Type__c,CHB_Number__c,City__c,Country__c,State__c From Party__c where Bill_Of_Lading_Number__c=:billOfLadingId];
            return partyList;
        }
         catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getBOLPartyReords', 'Bol Party Record Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
   
     /*
     * @purpose     : Method to get Bill Item Records for given BillOfLading Id.
     * @parameter   : billOfLadingId - BillOfLading Id
     * @return      : List<Bill_Item__c> - Bill Items list related to given billOfLading.
     */
    public static List<Bill_Item__c> getBillItemRecords(Id billOfLadingId){
        try{
            List<Bill_Item__c> billItemList=[Select id,Name,Loading_Port__c,Discharge_Port__c,Commodity_Type__c,Rate_Authority_Type__c,Authority_Number__c,Unit_Quantity__c,Unit_of_measure__c,Weight__c,Weight_Unit_of_measure__c,Cube__c,Cube_Feet__c,Cube_Meters__c,
                                             Schedule_B_Number__c,Shipper_Declare_value__c,Equipment_container__c,Sequence_Number__c,Items_Text_DSC__c
                                             From Bill_Item__c where Bill_Of_Lading_Number__c=:billOfLadingId];
            return billItemList;
        }
        catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getBillItemRecords', 'Bill Item Record Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
     /*
     * @purpose     : Method to get Charge Line Records for given BillOfLading Id.
     * @parameter   : billOfLadingId - BillOfLading Id
     * @return      : List<Charge_Line__c> - Charge Lines list related to given billOfLading.
     */
     public static List<Charge_Line__c> getChargeLineItems(id billOfLadingId){
        try{
            List<Charge_Line__c> chargeLineItems=[Select id,ChargeDescription__c,Quantity__c,Rate__c,Prepaid_Collect__c,Currency__c,Basis__c,Amount__c From Charge_Line__c where Bill_Of_Lading_Number__c=:billOfLadingId];
            return chargeLineItems;
        }
        catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getChargeLineItems', 'Charge Line Records Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
    
    /*
     * @purpose     : Method to get Equipment Records for given BillOfLading Id.
     * @parameter   : billOfLadingId - BillOfLading Id
     * @return      : List<Equipment__c> - Equipment list related to given billOfLading.
     */
     public static List<Equipment__c> getEquipmentRecords(id billOfLadingId){
        try{
            List<Equipment__c> equipList=[Select id,Prefix__c,Number__c,Seal_Numbers__c From Equipment__c where Bill_Of_Lading_Number__c=:billOfLadingId];
            return equipList;
        }
        catch(Exception ex){
            LogFactory.error('BillOfLadingDAO', 'getEquipmentRecords', 'Equipment Records Error', ex.getLineNumber() + ' - ' + ex.getMessage() + ' - ' + ex.getStackTraceString());
            throw ex;
        }
    }
}