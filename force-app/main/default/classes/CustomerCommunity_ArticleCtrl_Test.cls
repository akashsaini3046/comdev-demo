@isTest
public class CustomerCommunity_ArticleCtrl_Test {
    
    static testmethod void getArticle_Test(){
        
        User thisUser = [ select Id from User where username = 'ayush.mishra@nagarro.com.crowley.comdev'];
        Knowledge__kav test;
        System.runAs(thisUser){
            
            Knowledge__kav test_article = new Knowledge__kav(Title = 'test apex',
                                                             Summary = 'test from apex',
                                                             URLName = 'test',
                                                             Language = 'en_US',
                                                             Description__c = 'test test',
                                                             IsVisibleInCsp = True);
            insert test_article;             
            
            test_article = [SELECT KnowledgeArticleId,PublishStatus,Articlenumber FROM Knowledge__kav WHERE Id = :test_article.Id];
            
            //publish it
            KbManagement.PublishingService.publishArticle(test_article.KnowledgeArticleId, true);
            
            CustomerCommunity_ArticleController.getArticles();
            CustomerCommunity_ArticleController.getArticleDetails(test_article.KnowledgeArticleId); 

            //To cover catch statement
            CustomerCommunity_ArticleController.getArticleDetails('Test Article Id');
        }                
    }
  	   static testmethod void getArticle_test2(){
        User thisUser = [ select Id from User where username = 'ayush.mishra@nagarro.com.crowley.comdev'];
        Knowledge__kav test;
        System.runAs(thisUser){
            
            Knowledge__kav test_article = new Knowledge__kav(Title = 'test apex1',
                                                             Summary = 'test from apex1',
                                                             URLName = 'test1',
                                                             Language = 'en_US',
                                                             IsVisibleInCsp = false);
            insert test_article;
            
            CustomerCommunity_ArticleController.getArticles();
            CustomerCommunity_ArticleController.getArticleDetails(test_article.KnowledgeArticleId);
        }
    }
}