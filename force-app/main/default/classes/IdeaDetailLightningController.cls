public with sharing class IdeaDetailLightningController {
	
    public class SObjectDescribe{
        @AuraEnabled public boolean updateable{get;set;}
        @AuraEnabled public boolean accessible{get;set;}
        @AuraEnabled public boolean deletable{get;set;}
        @AuraEnabled public boolean createable{get;set;}
    }    
    public class SObjectFieldDescribe{
        @AuraEnabled public boolean updateable{get;set;}
        @AuraEnabled public boolean accessible{get;set;}
    }    
	@AuraEnabled    
    public static Idea getIdeaDetails(Id ideaId){
        try{
            if(string.isBlank(ideaId))
                return new Idea();
            Idea ideaRecord = new Idea();
            List<string> allFields = new List<String>();
            List<string> customFields = new List<String>();
            Map<string, Schema.SObjectField> ideafdMap = Idea.SobjectType.getDescribe().fields.getMap();
            Set<string> excludedFields = new Set<string>{'attachmentlength','attachmentcontenttype','attachmentbody'};
            for(String fd: ideafdMap.keySet()){
                if(ideafdMap.get(fd).getDescribe().isAccessible()){
                    if(ideafdMap.get(fd).getDescribe().isCustom()){
                        customFields.add(fd);
                    }
                    if(!excludedFields.contains(fd))
                    	allFields.add(fd);
                }
            }
            System.debug('All fields : '+allFields);
        	Id userId = UserInfo.getUserId();
            String query = 'Select '+String.join(allFields, ',')+', Community.Name, (Select Id, Type, CreatedById From Votes Where CreatedById=:userId) From Idea Where Id=:ideaId Limit 50000';
            System.debug('query -> '+query);
            ideaRecord = (Idea)Database.Query(query);
            System.debug('ideaRecord -> '+ideaRecord);
            return ideaRecord;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled    
    public static User getUser(){
        try{
            Id userId = UserInfo.getUserId();
            return [Select Id, Name, Username, FirstName, LastName,FullPhotoURL,SmallPhotoURL From User Where Id=:userId Limit 1];
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    @AuraEnabled    
    public static String getZonesList() {
        try{
            List<Community> communityList = [Select Id, Name, IsActive From Community Where IsActive=true Order By Name asc Limit 10000];
            return JSON.serialize(communityList);
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    @AuraEnabled    
    public static String getBenefits(){
        List<Schema.PicklistEntry> benefits = new List<Schema.PicklistEntry>();
        for(Schema.PicklistEntry p : Idea.SObjectType.getDescribe().fields.getMap().get('benefits__c').getDescribe().getPicklistValues()){
            if(p.isActive()){
                benefits.add(p);
            }            
        }
        return JSON.serialize(benefits);
    }
    
    @AuraEnabled    
    public static String getIdeaStatuses(){
        List<Schema.PicklistEntry> statuses = new List<Schema.PicklistEntry>();
        for(Schema.PicklistEntry p : Idea.SObjectType.getDescribe().fields.getMap().get('status').getDescribe().getPicklistValues()){
            if(p.isActive()){
                statuses.add(p);
            }            
        }
        return JSON.serialize(statuses);
    }
    
    @AuraEnabled    
    public static String getIdeaCategories(){       
        List<Schema.PicklistEntry> categories = new List<Schema.PicklistEntry>();
        for(Schema.PicklistEntry p : Idea.SObjectType.getDescribe().fields.getMap().get('categories').getDescribe().getPicklistValues()){
            if(p.isActive()){
                categories.add(p);
            }            
        }
        return JSON.serialize(categories);
    }
    
    @AuraEnabled    
    public static Idea saveIdeaRecord(Idea ideaRecord){
        try{
            
            system.debug('ideaRecord -> '+ideaRecord);
            System.debug('saving idea');
            if(String.isBlank(ideaRecord.Id) && String.isBlank(ideaRecord.Status)){
                for(Schema.PicklistEntry entry : Idea.getSobjectType().getDescribe().fields.getMap().get('status').getDescribe().getPicklistValues()){
                    if(entry.isDefaultValue()){
                        ideaRecord.Status = entry.getValue();
                    }    
                }
            }
            //return [Select Id, Status, Categories, Title, Body, CommunityId, Critical_Requirement__c, Benefits__c From Idea Where Id='087M00000000LiF' Limit 1];    
            upsert ideaRecord;
            return ideaRecord;
        }catch(Exception ex){
            if(ex.getMessage().contains('REQUIRED_FIELD_MISSING')){
                throw new IdeaCustomException('Please fill all required fields');
            }else{
                throw new IdeaCustomException('Something went wrong!! '+ex.getMessage());
            }
        }
    }
    
    @AuraEnabled    
    public static Idea deleteIdeaRecord(Id ideaId){
        try{
            System.debug('deleting idea');
            Idea ideaRecord = new Idea();
            ideaRecord.Id = ideaId;
            delete ideaRecord;
            system.debug('ideaRecord -> '+ideaRecord);
            return ideaRecord;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<Idea> findSimilarIdeas(string communityId, string title){
        try{
            Idea ideaRecord = new Idea();
            ideaRecord.Title = title;
            ideaRecord.CommunityId = communityId;
            List<Id> similarIdeasIds = Ideas.findSimilar(ideaRecord);
            System.debug('similar -> '+Ideas.findSimilar(ideaRecord));
            System.debug('record -> '+ideaRecord);    
            Id userId = UserInfo.getUserId();
            List<Idea> similarIdeas = [Select Id, Title, Status, Community.Name, Body, VoteTotal From Idea Where Id IN : similarIdeasIds];
            return similarIdeas;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    @AuraEnabled
    public static string getIdeaFieldDescribe(){
        try{
            Map<string, SobjectFieldDescribe> result = new Map<string, SobjectFieldDescribe>();
            Map<string, Schema.SObjectField> fdMap = Idea.SobjectType.getDescribe().fields.getMap();
            for(string fd: fdMap.keyset()){
                SObjectFieldDescribe fdDesc = new SObjectFieldDescribe();
                fdDesc.updateable = fdMap.get(fd).getDescribe().isUpdateable();
                fdDesc.accessible = fdMap.get(fd).getDescribe().isAccessible();
                result.put(fd, fdDesc);
            }
            System.debug('JSON.serialize(resfdMap) -> '+JSON.serialize(result));
            return JSON.serialize(result);
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static string getIdeaDescribe(){
        try{
            Map<string, SobjectDescribe> result = new Map<string, SobjectDescribe>();
            SObjectDescribe ideaDesc = new SObjectDescribe();
            ideaDesc.updateable = Idea.SobjectType.getDescribe().isUpdateable();
            ideaDesc.accessible = Idea.SobjectType.getDescribe().isAccessible();
            ideaDesc.deletable = Idea.SobjectType.getDescribe().isDeletable();
            ideaDesc.createable = Idea.SobjectType.getDescribe().isCreateable();
            result.put('idea', ideaDesc);
            System.debug('JSON.serialize(resfdMap) -> '+JSON.serialize(result));
            return JSON.serialize(result);
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    public class Comment{
        @AuraEnabled public IdeaComment ideaComment;
        @AuraEnabled public List<Vote> votes;
        @AuraEnabled public boolean isUpvoted;
    }
    @AuraEnabled
    public static string getIdeaComments(string ideaId){
        try{
            Map<string, Schema.SobjectField> cfdmap = IdeaComment.SobjectType.getDescribe().fields.getMap();
            Set<String> commentFdsSet = cfdmap.keyset(); 
            for(string key: commentFdsSet){
                System.debug(key+' -> '+cfdmap.get(key).getDescribe());
            }
            List<string> commentFields = new List<String>(commentFdsSet);
            String commQuery = 'Select '+String.join(commentFields, ',')+', CreatedBy.Alias, CreatedBy.Name, (Select Id, Type, LastModifiedById, CreatedById From Votes) From IdeaComment Where IdeaId=:ideaId Order By CreatedDate Asc';
            List<IdeaComment> ideaComments = (List<IdeaComment>)Database.query(commQuery);
            List<Comment> resComments = new List<Comment>();
            for(IdeaComment comm: ideaComments){
                Comment c = new Comment();
                c.ideaComment = comm;
                c.isUpvoted = false;
                c.votes = comm.votes;
                if(comm.votes.size()>0){
                    for(Vote vt : comm.votes){
                        if( vt.CreatedById == UserInfo.getUserId() ){
                            if(vt.Type == 'Up'){
                                c.isUpvoted = true;
                            }    
                        }
                    }
                }
                resComments.add(c);
            System.debug('comment -> '+c);
            }
            System.debug(JSON.serialize(resComments));
            return JSON.serialize(resComments);
        }catch(Exception ex){
            SYstem.debug('Exception -> '+ex.getMessage());
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static void upvoteIdea(string ideaId){
        try{
            Id userId = UserInfo.getUserId();
            List<Vote> upvotes = [Select Id, ParentId, CreatedById, Type From Vote Where CreatedById=:userId and ParentId=:ideaId];
            System.debug('existing upvotes -> '+upvotes);
            if(upvotes.isEmpty()){
                Vote vot = new Vote();
                vot.ParentId = ideaId;
                vot.type = 'Up';
                insert vot;
            }
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
        
    }
    @AuraEnabled
    public static void downvoteIdea(string ideaId){
        try{
            Id userId = UserInfo.getUserId();
            List<Vote> upvotes = [Select Id, ParentId, CreatedById, Type From Vote Where CreatedById=:userId and ParentId=:ideaId];
            if(upvotes.isEmpty()){
                Vote vot = new Vote();
                vot.ParentId = ideaId;
                vot.type = 'Down';
                insert vot;
            }
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
        
    }
    @AuraEnabled
    public static void likeComment(string ideaCommentId){
        try{
            System.debug('Liking comment -> '+ideaCommentId);
            Id userId = UserInfo.getUserId();
            List<Vote> votes = [Select Id, ParentId, Type, LastModifiedById From Vote Where ParentId=:ideaCommentId and LastModifiedById=:userId];
            if(votes.isEmpty()){
                Vote vt = new Vote();
                vt.ParentId = ideaCommentId;
                vt.Type = 'Up';
                insert vt;
            }
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static void unlikeComment(string ideaCommentId){
        try{
            System.debug('Disliking comment -> '+ideaCommentId);
            Id userId = UserInfo.getUserId();
            List<Vote> votes = [Select Id, ParentId, Type, LastModifiedById From Vote Where ParentId=:ideaCommentId and LastModifiedById=:userId];
            delete votes;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static void deleteComment(string ideaCommentId){
        try{
            IdeaComment comment = new IdeaComment();
            comment.Id = ideaCommentId;
            delete comment;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static IdeaComment addComment(string param){
        try{
            IdeaComment comm = (IdeaComment)JSON.deserialize(param, IdeaComment.class);
            IdeaComment newComm = new IdeaComment();
            newComm.IdeaId = comm.IdeaId;
            newComm.CommentBody = comm.CommentBody;
            insert newComm;
            return newComm;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static IdeaComment getIdeaCommentRecord(string ideaCommentId){
        try{
            IdeaComment comment = [Select Id, CommentBody, Idea.Title, Idea.CommunityId, IdeaId  From IdeaComment Where Id=:ideaCommentId];
            return comment;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static IdeaComment saveIdeaCommentRecord(IdeaComment comment){
        try{
            upsert comment;
            return comment;
        }catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<Idea> getIdeasList(string communityId, string statuses, string searchText, string categories){
        try{
            Id userId = UserInfo.getUserId();
            Map<string, Schema.SObjectField> fdMap = Idea.SobjectType.getDescribe().fields.getMap();
            List<string> fields = new List<string>();
            for(string fd : fdMap.keyset()){
                if(fdMap.get(fd).getDescribe().isAccessible()){
                    fields.add(fd);
                }
            }       
            fields.add('Community.Name');
            fields.add('CreatedBy.Alias');
            fields.add('CreatedBy.Name');
            
            String q = 'Select '+String.join(fields, ',')+',(Select CreatedById, Type From Votes Where CreatedById=:userId) From Idea';
            List<string> whereconds = new List<String>();
            if(String.isNotBlank(communityId)){
                whereconds.add('CommunityId=:communityId');
            }
            if(String.isNotBlank(statuses)){
                whereconds.add('Status = :statuses');
            }
            if(String.isNotBlank(categories)){
                whereconds.add('Categories INCLUDES (:categories)');
            }
            if(String.isNotBlank( searchText )){
                string tempSearchText = '%'+searchText +'%';
                whereconds.add('Title like :tempSearchText ');
            }
            
            if(!whereconds.isEmpty()){
                q = q + ' Where '+String.Join(whereconds, ' and ')+' Order By CreatedDate Desc';
            }
            System.debug('query -> '+q);
            return (List<Idea>)Database.Query(q);
        }
        catch(Exception ex){
            throw new IdeaCustomException(ex.getMessage());
        }
    }
    
    @AuraEnabled
    public static  Map<String, Map<String ,String>> fetchRecordTypeSpecificPickListvalues(String recordTypeId){
        HTTPRequest req = new HTTPRequest();
        //req.setEndpoint('callout:UI_API_Named_Credentials'+'/services/data/v48.0/sobjects/Account/describe/layouts');
        req.setEndpoint('callout:UI_API_Named_Credentials'+'/services/data/v48.0/sobjects/Idea/describe/layouts/'+recordTypeId);
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);       
        System.debug(' response'+res.getBody());
        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        List<Object> detailLayoutSections = (List<Object>)result.get('detailLayoutSections');
        Map<String, Object> detailFirstSection = (Map<String, Object> )detailLayoutSections[0];
        List<Object> layoutRows = (List<Object>)detailFirstSection.get('layoutRows');
        Map<String, Map<String ,String>> picklistVsKeyValueMap = new Map<String, Map<String, String>>();
        Map<String, List<Object>> fieldVsPicklistObjectList = new Map<String, List<Object>>();
        List<Object> allLayoutItems = new List<Object>();
        for(Object layoutRow : layoutRows){
            Map<String, Object> layRow = (Map<String, Object> ) layoutRow;
            List<Object> layoutItems = (List<Object>)layRow.get('layoutItems');
            allLayoutItems.addAll(layoutItems);
        }
        for(Object layoutItem : allLayoutItems){
            Map<String, Object> layItem = (Map<String, Object> ) layoutItem;
            String fieldLabel = (String)layItem.get('label');
            List<Object> layoutComponents = (List<Object>)layItem.get('layoutComponents');
            Map<String, Object> layComponent = (Map<String, Object>)layoutComponents[0];
            Map<String, Object> details = (Map<String, Object>) layComponent.get('details');
            List<Object> picklistValues = (List<Object>) details.get('picklistValues');
            fieldVsPicklistObjectList.put(fieldLabel, picklistValues);
        }
        
        for(String fieldLabel : fieldVsPicklistObjectList.keySet()){           
            Map<String,String> valueLabelMap = new Map<String,String>();
            List<Object> pickListValues = fieldVsPicklistObjectList.get(fieldLabel);
            for(Object pickListValue :pickListValues){
                Map<String,Object> pickListObjectMap = (Map<String,Object>)pickListValue;
                if((Boolean)pickListObjectMap.get('active')==true)
                    valueLabelMap.put((String)pickListObjectMap.get('value'),(String)pickListObjectMap.get('label'));
        	}   
            picklistVsKeyValueMap.put(fieldLabel,valueLabelMap);
        }       
        System.debug('PickListValues : '+picklistVsKeyValueMap);  
        return picklistVsKeyValueMap;

    }
    
    @AuraEnabled
    public static String getRecordTypeId(Id zoneId){
        String recordTypeId=null;
        try{
            System.debug('print zone id '+zoneId);
        Community communityZone = [Select name from Community where id =: zoneId][0];
            System.debug('communityZone '+communityZone);
        Schema.DescribeSObjectResult objRes = Idea.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> recTypeInfo = objRes.getRecordTypeInfos();
        for(Schema.RecordTypeInfo r : recTypeInfo){
            System.debug('compare '+communityZone.name+' --> '+r.getName()+'  '+r.getRecordTypeId());
            System.debug(communityZone.name+': Idea');
            System.debug(r.getName());
            System.debug(r.getRecordTypeId());
            if(communityZone.name+': Idea Record Type' == r.getName()){
                System.debug('Value Matched...');
            	recordTypeId=r.getRecordTypeId();
                break;
            }
        }
        return recordTypeId;
        }
        catch(Exception e){
            System.debug('Inside catch');
            return null;
        }
    }
    
    @AuraEnabled 
    public Static List<Id> getExpertsGroupMembers(Id ideaId){
        List<id> userIds= new List<id>();
        Idea idea = [Select communityId, community.Name from Idea where id =: ideaId][0];
        String zoneName = idea.community.Name;
        Idea_Community_Zone__mdt publicGroupVsZoneName = [SELECT Public_Group_Name__c from Idea_Community_Zone__mdt where MasterLabel=: zoneName][0];
        String grpName=publicGroupVsZoneName.Public_Group_Name__c;
        List<GroupMember> groupMembers = new List<GroupMember>();
        groupMembers=[SELECT id, UserOrGroupId, GroupId from GroupMember where Group.Name=:grpName];
        for(GroupMember gm : groupMembers){
            userIds.add(gm.id);
        }
        return userIds;
    }
}