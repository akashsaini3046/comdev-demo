@isTest
public class CC_BillofLadingDetailCtrlTest {
	@testSetup
    static void setup(){
        TestDataUtility.getBillOfLadingRecords('RR',1);       
    }
    @isTest
    static void getBolDetailsTest(){
        Id bolId = [SELECT id from Bill_Of_Lading__c].Id;
        
        Test.startTest();
        Map<String, List< Map<String, String>>> result = CC_BillofLadingDetailController.getBolDetails(bolId);
        Test.stopTest();
        
        System.assertNotEquals(null, result);
    }
    
     @isTest
    static void getBolDetailsTestNegative(){
        Id bolId = [SELECT id from Bill_Of_Lading__c].Id;
        
        Test.startTest();
        
        Map<String, List< Map<String, String>>> result = CC_BillofLadingDetailController.getBolDetails(null);
        Test.stopTest();
        
        //System.assertNotEquals(null, result);
    }
}