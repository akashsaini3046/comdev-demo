@isTest
public class CustomerCommunity_STChatbot_Test {

    private class RestMock1 implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fJson = '[[{\"currentStatus\":\"Loadlisted\",\"discharge\":{\"date\":\"2019-06-14\",\"flag\":\"E\",\"location\":\"Port Everglades, FL\"},\"equipment\":{\"Bookings\":[],\"equipment\":[{\"Routing\":[{\"date\":\"\",\"status\":\"For more detailed information,\"},{\"date\":\"\",\"status\":\"please inquire by booking or\"},{\"date\":\"\",\"status\":\"bill of lading number.\"}],\"id\":\"CMCU4578693\"}],\"equipmentBooked\":[]},\"estSailDate\":\"2019-06-12\",\"shipmentID\":\"CMCU4578693         \",\"shipmentType\":\"Equipment\",\"statusDate\":\"2019-06-12 10:31\",\"voyage\":{\"number\":\"024S\",\"vessel\":\"VARAMO\"}}]]';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fJson);
            res.setStatusCode(200);
            return res;
        }
    }
    
    private class RestMock2 implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '[{"currentStatus":"[{Not booked}]", "discharge":[{"dateInfo":"","flag":"","location":""}], "estSailDate":"", "shipmentID":"CMCU4554958", "shipmentType":"Equipment", "statusDate":"", "voyage":[{"numberInfo":"", "vessel":""}]}]';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    
    static testmethod void TrackShipment_test1(){
        
        Test.setMock(HttpCalloutMock.class, new RestMock1());
        CustomerCommunity_STChatbot.TrackingIDs input = new CustomerCommunity_STChatbot.TrackingIDs();
        input.sTrackingId = 'T230';
        List<CustomerCommunity_STChatbot.TrackingIDs> listtrackingID = new List<CustomerCommunity_STChatbot.TrackingIDs>();
        listtrackingID.add(input);
        Test.startTest();
        CustomerCommunity_STChatbot.TrackShipments(listtrackingID);
        Test.stopTest();
        
         }
    static testmethod void TrackShipment_test2(){
        Test.setMock(HttpCalloutMock.class, new RestMock2());
        CustomerCommunity_STChatbot.TrackingIDs input = new CustomerCommunity_STChatbot.TrackingIDs();
        input.sTrackingId = 'T233';
        List<CustomerCommunity_STChatbot.TrackingIDs> listtrackingID = new List<CustomerCommunity_STChatbot.TrackingIDs>();
        listtrackingID.add(input);
        Test.startTest();
        CustomerCommunity_STChatbot.TrackShipments(listtrackingID);
        Test.stopTest();        
    }
}