public class CustomerCommunity_APICallouts {
    public Static String getAPIResponse(String ServiceName, String body, String URLsuffix) {
        List<Integration_Parameters__mdt> list_APIServices = new List<Integration_Parameters__mdt>();
        if(ServiceName != '' && ServiceName != Null){
            list_APIServices = [SELECT Id, MasterLabel, Endpoint_URL__c, Request_Method__c, Request_Timeout__c, Header_Type__c, Header_Value__c
                                FROM Integration_Parameters__mdt WHERE MasterLabel = :ServiceName LIMIT 1];
            System.debug('list_APIServices--> '+list_APIServices);
            if(!list_APIServices.isEmpty()) {
                HttpRequest request = new HttpRequest();
                if(list_APIServices[0].Endpoint_URL__c != Null && list_APIServices[0].Endpoint_URL__c != '') {
                    if(URLsuffix != Null && URLsuffix != '')
                        list_APIServices[0].Endpoint_URL__c = list_APIServices[0].Endpoint_URL__c + URLsuffix;
                    request.setEndpoint(list_APIServices[0].Endpoint_URL__c);
                }
                if(list_APIServices[0].Request_Method__c != Null && list_APIServices[0].Request_Method__c != '')
                    request.setMethod(list_APIServices[0].Request_Method__c);
                if(list_APIServices[0].Request_Timeout__c != Null)
                    request.setTimeout(Integer.valueOf(list_APIServices[0].Request_Timeout__c));
                if(list_APIServices[0].Header_Type__c != Null && list_APIServices[0].Header_Type__c != '' && list_APIServices[0].Header_Value__c != Null && list_APIServices[0].Header_Value__c != '')
                    request.setHeader(list_APIServices[0].Header_Type__c, list_APIServices[0].Header_Value__c);
                if(body != Null && body != '')
                    request.setBody(body);
                system.debug('----->>>>>>'+request.getBody());
                try {
                    Http service = new Http();
                    
                    HttpResponse response = service.send(request);
                    if(response.getStatusCode() == 301 || response.getStatusCode() == 302)
                    {    
                        request.setEndpoint(response.getHeader('Location'));
                        response = new Http().send(request);
                    }
                    if(response.getBody() != Null && response.getBody() != ''){
                        System.debug('response.getBody()-> '+response.getBody());
                        return response.getBody();
                    }
                }
                catch (Exception err) {
                    System.debug('Exception Occured in API Callout - ' + 'ServiceName - ' + err.getMessage());
                    return null;
                }
            }
        }
        return Null;
    }
    
    /// Create By Drupad  
    public static String getAPIResponse(String serviceName, String body) {
        List<Integration_Parameters__mdt> apiServicesList = new List<Integration_Parameters__mdt>();
        apiServicesList = [SELECT Id, MasterLabel, Endpoint_URL__c, Request_Method__c, Request_Timeout__c, Client_Certificate_Name__c
                           FROM Integration_Parameters__mdt 
                           WHERE MasterLabel = :ServiceName LIMIT 1];
        if(!apiServicesList.isEmpty()){
            Integration_Parameters__mdt apiService = apiServicesList.get(0);
            List<Integration_Header__mdt> apiServiceHeadersList = [SELECT MasterLabel, Header_Key__c, Header_Value__c 
                                                                   FROM Integration_Header__mdt 
                                                                   WHERE Integration_Parameters__c = :apiService.Id];
            if(!apiServiceHeadersList.isEmpty()){
                Map<String, String> headers = new Map<String, String>();
                for(Integration_Header__mdt header : apiServiceHeadersList){
                    headers.put(header.Header_Key__c, header.Header_Value__c);
                }
                if(apiService.Request_Method__c.equalsIgnoreCase('GET')){
                    return sendGet(apiService.Endpoint_URL__c, headers, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('POST')){
                    return sendPost(apiService.Endpoint_URL__c, headers, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('PUT')){
                    return sendPut(apiService.Endpoint_URL__c, headers, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('DELETE')){
                    return sendDelete(apiService.Endpoint_URL__c, headers, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }
            }else{
                if(apiService.Request_Method__c.equalsIgnoreCase('GET')){
                    return sendGet(apiService.Endpoint_URL__c, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('POST')){
                    return sendPost(apiService.Endpoint_URL__c, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }
            }
        }
        return '';
    }
    public static String getAPIResponseService(String serviceName, String body, Map<String, String> queryParams) {
        List<Integration_Parameters__mdt> apiServicesList = new List<Integration_Parameters__mdt>();
        if(serviceName == null && serviceName == ''){
            return '';
        }
        apiServicesList = [SELECT Id, MasterLabel, Endpoint_URL__c, Request_Method__c, Request_Timeout__c, Client_Certificate_Name__c
                           FROM Integration_Parameters__mdt 
                           WHERE MasterLabel = :ServiceName LIMIT 1];
        if(apiServicesList != null && !apiServicesList.isEmpty()){
            Integration_Parameters__mdt apiService = apiServicesList.get(0);
            List<Integration_Header__mdt> apiServiceHeadersList = [SELECT MasterLabel, Header_Key__c, Header_Value__c 
                                                                   FROM Integration_Header__mdt 
                                                                   WHERE Integration_Parameters__c = :apiService.Id];
            String url = apiService.Endpoint_URL__c;
            if(queryParams != null && !queryParams.isEmpty()){
                url += '?';
                for(String key : queryParams.keySet()){
                    url += key + '=' + EncodingUtil.urlEncode(queryParams.get(key), 'UTF-8');
                }
            }
            if(!apiServiceHeadersList.isEmpty()){
                Map<String, String> headers = new Map<String, String>();
                for(Integration_Header__mdt header : apiServiceHeadersList){
                    headers.put(header.Header_Key__c, header.Header_Value__c);
                }
                if(apiService.Request_Method__c.equalsIgnoreCase('GET')){
                    return sendGet(url, headers, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('POST')){
                    return sendPost(url, headers, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('PUT')){
                    return sendPut(url, headers, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('DELETE')){
                    return sendDelete(url, headers, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }
            }else{
                if(apiService.Request_Method__c.equalsIgnoreCase('GET')){
                    return sendGet(url, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('POST')){
                    return sendPost(url, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('PUT')){
                    return sendPut(url, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('DELETE')){
                    return sendDelete(url, body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }
            }
        }
        return '';
    }
    public static String sendGet(String url, Integer timeOut, String cert){
        return sendGet(url, null, timeOut, cert);
    }
    public static String sendGet(String url, Map<String, String> headers, Integer timeOut, String cert){
        return sendRequestCheckResult(url, headers, 'GET', null, timeOut, cert);
    }
    public static String sendPost(String url, String body, Integer timeOut, String cert){
        return sendPost(url, null, body, timeOut, cert);
    }
    public static String sendPost(String url, Map<String, String> headers, String body, Integer timeOut, String cert){
        return sendRequestCheckResult(url, headers, 'POST', body, timeOut, cert);
    }
    public static String sendPut(String url, String body, Integer timeOut, String cert){
        return sendPost(url, null, body, timeOut, cert);
    }
    public static String sendPut(String url, Map<String, String> headers, String body, Integer timeOut, String cert){
        return sendRequestCheckResult(url, headers, 'PUT', body, timeOut, cert);
    }
    public static String sendDelete(String url, String body, Integer timeOut, String cert){
        return sendPost(url, null, body, timeOut, cert);
    }
    public static String sendDelete(String url, Map<String, String> headers, String body, Integer timeOut, String cert){
        return sendRequestCheckResult(url, headers, 'DELETE', body, timeOut, cert);
    }
    public static String sendRequestCheckResult(String url, Map<String, String> headers, String method, String body, Integer timeOut, String cert){
        HttpResponse response;
        response = sendRequest(url, headers, method, body, timeOut, cert);
        if (response == null){
            return null;
        }
        if (response.getStatusCode() < 200 || response.getStatusCode() >= 400){
            System.debug(LoggingLevel.ERROR, 'Error in sending callout to: ' + url + ' with body: ' + body
                         + '. Response from server: ' + response.getBody());
            throw new CustomerCommunity_APICalloutsException('Error in sending callout to: ' + url + ' with body: ' + body
                                                             + '. Response from server: ' + response.getBody());
        }
        return response.getBody();
    }
    public static HttpResponse sendRequest(String url, Map<String, String> headers, String method, String body, Integer timeOut, String cert){
        if (url == null || url == ''){
            return null;
        }
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(url);
        request.setMethod(method);
        if(timeOut != null)
            request.setTimeout(timeOut);
        else
            request.setTimeout(30000);
        if(cert != null && cert != '')
            request.setClientCertificateName(cert);
        if (body != null)
            request.setBody(body);
        if (headers != null){
            for (String key : headers.keySet()){
                request.setHeader(key, headers.get(key));
            }
        }
        System.debug('*** Sending request to: ' + url + '. Content header: ' + request.getHeader('content-type') + '. Body: ' + body);
        System.debug('***' + request);
        HttpResponse result = http.send(request);
        
        System.debug('*** Response: ' + result.getStatus() + '. Body: ' + result.getBody());
        
        return result;
    }

    public static String getCustomAuthRes(String url,String body){
        if (url == null || url == ''){
            return null;
        }
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(url);
        request.setTimeout(30000);
        request.setMethod('POST');
     /*   request.setHeader('client_id','9567932436aa4c55b872eeb92960a799');
        request.setHeader('client_secret','e21F083673dA45e0814Bc57e16C97Bb0');
        request.setHeader('Content-Type','application/json');  */
    
        if (body != null){
            request.setBody(body);
        }
       
        System.debug('*** Sending request to: ' + url + '. Content header: ' + '. Body: ' + body);
        System.debug('***' + request);
        HttpResponse result = http.send(request);
        
        System.debug('*** Response: ' + result.getStatus() + '. Body: ' + result.getBody());
        HttpResponse response;
        response = result;
        if (response == null){
            return null;
        }
        if (response.getStatusCode() < 200 || response.getStatusCode() >= 400){
            System.debug(LoggingLevel.ERROR, 'Error in sending callout to: ' + url + ' with body: ' + body
                         + '. Response from server: ' + response.getBody());
            throw new CustomerCommunity_APICalloutsException('Error in sending callout to: ' + url + ' with body: ' + body
                                                             + '. Response from server: ' + response.getBody());
        }
        return response.getBody();
    }

    public static HttpRequest getAPIRequest(String serviceName, String body) {
        HttpRequest request = new HttpRequest();
        List<Integration_Parameters__mdt> apiServicesList = new List<Integration_Parameters__mdt>();
        apiServicesList = [SELECT Id, MasterLabel, Endpoint_URL__c, Request_Method__c, Request_Timeout__c, Client_Certificate_Name__c
                           FROM Integration_Parameters__mdt 
                           WHERE MasterLabel = :ServiceName LIMIT 1];
        if(!apiServicesList.isEmpty()){
            Integration_Parameters__mdt apiService = apiServicesList.get(0);
            List<Integration_Header__mdt> apiServiceHeadersList = [SELECT MasterLabel, Header_Key__c, Header_Value__c 
                                                                   FROM Integration_Header__mdt 
                                                                   WHERE Integration_Parameters__c = :apiService.Id];
            if(!apiServiceHeadersList.isEmpty()){
                Map<String, String> headers = new Map<String, String>();
                for(Integration_Header__mdt header : apiServiceHeadersList){
                    headers.put(header.Header_Key__c, header.Header_Value__c);
                }
                if(apiService.Request_Method__c.equalsIgnoreCase('GET')){
                    
                }else if(apiService.Request_Method__c.equalsIgnoreCase('POST')){
                    request = getRequest(apiService.Endpoint_URL__c, headers, 'POST', body, Integer.valueOf(apiService.Request_Timeout__c), apiService.Client_Certificate_Name__c);
                }else if(apiService.Request_Method__c.equalsIgnoreCase('PUT')){
                    
                }else if(apiService.Request_Method__c.equalsIgnoreCase('DELETE')){
                    
                }
            }
        }
        return request;
    }
    
    public static HttpRequest getRequest(String url, Map<String, String> headers, String method, String body, Integer timeOut, String cert){
        if (url == null || url == ''){
            return null;
        }
        HttpRequest request = new HttpRequest();
        request.setEndpoint(url);
        request.setMethod(method);
        //if(timeOut != null)
            //request.setTimeout(timeOut);
       // else
        //    request.setTimeout(120000);
        if(cert != null && cert != '')
            request.setClientCertificateName(cert);
        if (body != null)
            request.setBody(body);
        if (headers != null){
            for (String key : headers.keySet()){
                request.setHeader(key, headers.get(key));
            }
        }
        return request;
        
    }

    
    public class CustomerCommunity_APICalloutsException extends Exception {}
}