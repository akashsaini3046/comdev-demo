public class CustomerCommunity_ReBookingController {
    @AuraEnabled
    public static Booking__c cloneBookingRecords(Id bookingRecordId, Date readyDate, Id contactRecordId){
        System.debug(LoggingLevel.INFO,'Entering to CustomerCommunity_ReBookingController:cloneBookingRecords() with parameters'); 
        List<Booking__c> listBookingRecord = new List<Booking__c>();
        List<Party__c> listPartyRecords = new List<Party__c>();
        Map<Id, Shipment__c> mapShipmentRecords;
        List<Voyage__c> listVoyageRecords = new List<Voyage__c>();
        Map<Id, FreightDetail__c> mapFreightDetailRecords;
        List<Requirement__c> listRequirementRecords = new List<Requirement__c>();
        List<Commodity__c> listCommodityRecords = new List<Commodity__c>();
        Set<Id> setShipmentIds = new Set<Id>();
        Set<Id> setFreightIds = new Set<Id>();
        Integer count = 1;
        
        try{
            listBookingRecord = [SELECT Id, Origin_Type__c, Destination_Type__c, Contact__c, Account__c, OwnerId, Status__c FROM Booking__c WHERE Id = :bookingRecordId LIMIT 1];
            if(!listBookingRecord.isEmpty()){
                listPartyRecords = [SELECT Name, Address_Line1__c, Address_Line2__c, Booking__c, CHB_Number__c, City__c, Code__c, Country__c, CVIF__c, CVIF_Location_Code__c, Fax_Number__c, Phone_Number__c,
                                    REF_Number__c, State__c, Type__c, Zip__c  FROM Party__c WHERE Booking__c = :listBookingRecord[0].Id];
                mapShipmentRecords = new Map<Id, Shipment__c>([SELECT Booking__c, Destination_Code__c, Destination_City__c, Destination_Country__c, Destination_Port__c, Destination_Sequence__c, Destination_State__c, 
                                                               Destination_Zip__c, Origin_Code__c, Origin_City__c, Origin_Country__c, Origin_Port__c, Origin_Sequence__c, Origin_State__c, Origin_Zip__c 
                                                               FROM Shipment__c WHERE Booking__c = :listBookingRecord[0].Id]);
            }
            if(!mapShipmentRecords.isEmpty()){
                for(Shipment__c shipmentRecord : mapShipmentRecords.values())
                    setShipmentIds.add(shipmentRecord.Id);
                listVoyageRecords = [SELECT Shipment__c,Estimate_Sail_Date__c  FROM Voyage__c WHERE Shipment__c IN :setShipmentIds];
                mapFreightDetailRecords = new Map<Id, FreightDetail__c>([SELECT Shipment__c, Cargo_Type__c FROM FreightDetail__c WHERE Shipment__c IN :setShipmentIds]);
                if(!mapFreightDetailRecords.isEmpty()){
                    for(FreightDetail__c freightRecord : mapFreightDetailRecords.values())
                        setFreightIds.add(freightRecord.Id);
                    listRequirementRecords = [SELECT Category__c, Freight__c, Length__c, Quantity__c, rrInd__c FROM Requirement__c WHERE Freight__c IN :setFreightIds];
                    listCommodityRecords = [SELECT Freight__c, IMO_Class__c, IMO_DG_Page_Class__c, Is_Hazardous__c, Name__c, Number__c, Package_Group__c, Phone_Number__c, Technical_Name__c, Dot_Name__c
                                            FROM Commodity__c WHERE Freight__c IN :setFreightIds];
                }
            }
            
            //Create Booking Record
            listBookingRecord[0].Id = Null;
            if(contactRecordId != Null)
                listBookingRecord[0].Contact__c = contactRecordId;
            if(!listCommodityRecords.isEmpty())
                listBookingRecord[0].Status__c = 'HazCheck Required';
            else
                listBookingRecord[0].Status__c = 'HazCheck Not Required';
            insert listBookingRecord;
            listBookingRecord = [SELECT Id, Origin_Type__c, Destination_Type__c, Contact__c, Account__c, OwnerId, Status__c, Name FROM Booking__c WHERE Id = :listBookingRecord[0].Id LIMIT 1];
            
            //Create Party Records
            if(!listPartyRecords.isEmpty()){
                for(Party__C partyRecord : listPartyRecords){
                    partyRecord.Id = Null;
                    partyRecord.Booking__c = listBookingRecord[0].Id;
                }
                insert listPartyRecords;
            }
            
            //Create Shipment Records
            if(!mapShipmentRecords.isEmpty()){
                for(Shipment__c shipmentRecord : mapShipmentRecords.values()){
                    shipmentRecord.Id = Null;
                    shipmentRecord.Name = listBookingRecord[0].Name + ' - Shipment';
                    shipmentRecord.Booking__c = listBookingRecord[0].Id;
                }
                insert mapShipmentRecords.values();
                
                //Create Voyage Records
                if(!listVoyageRecords.isEmpty()){
                    for(Voyage__c voyageRecord : listVoyageRecords){
                        voyageRecord.Id = Null;
                        voyageRecord.Name = listBookingRecord[0].Name + ' - Voyage';
                        voyageRecord.Estimate_Sail_Date__c = readyDate;
                        if(mapShipmentRecords.containsKey(voyageRecord.Shipment__c))
                            voyageRecord.Shipment__c = mapShipmentRecords.get(voyageRecord.Shipment__c).Id;
                    }
                    insert listVoyageRecords;
                }
                
                //Create Freight Detail Records
                if(!mapFreightDetailRecords.isEmpty()){
                    for(FreightDetail__c freightRecord : mapFreightDetailRecords.values()){
                        freightRecord.Id = Null;
                        freightRecord.Name = listBookingRecord[0].Name + ' - Freight ' + count++;
                        if(mapShipmentRecords.containsKey(freightRecord.Shipment__c))
                            freightRecord.Shipment__c = mapShipmentRecords.get(freightRecord.Shipment__c).Id;
                    }
                    insert mapFreightDetailRecords.values();
                    
                    //Create Requirement Records
                    if(!listRequirementRecords.isEmpty()){
                        count = 1;
                        for(Requirement__c requirementRecord : listRequirementRecords){
                            requirementRecord.Id = Null;
                            requirementRecord.Name = listBookingRecord[0].Name + ' - Requirement ' + count++;
                            if(mapFreightDetailRecords.containsKey(requirementRecord.Freight__c))
                                requirementRecord.Freight__c = mapFreightDetailRecords.get(requirementRecord.Freight__c).Id;
                        }
                        insert listRequirementRecords;
                    }
                    
                    //Create Commodity Records
                    if(!listCommodityRecords.isEmpty()){
                        count = 1;
                        for(Commodity__c commodityRecord : listCommodityRecords){
                            commodityRecord.Id = Null;
                            commodityRecord.Name = listBookingRecord[0].Name + ' - Commodity ' + count++;
                            if(mapFreightDetailRecords.containsKey(commodityRecord.Freight__c))
                                commodityRecord.Freight__c = mapFreightDetailRecords.get(commodityRecord.Freight__c).Id;
                        }
                        insert listCommodityRecords;
                    }
                }
            }
            System.debug(LoggingLevel.INFO,'Exiting from CustomerCommunity_NewBookingsController:createBookingRecord() returning--'+listBookingRecord);
            return listBookingRecord[0];
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }    
    }
    @AuraEnabled
    public static String validateHazardousBooking(Id bookingId){
        List<Shipment__c> listShipments = new List<Shipment__c>();
        Set<Id> setShipmentId = new Set<Id>();
        List<FreightDetail__c> listFreightDetails = new List<FreightDetail__c>();
        Set<Id> setFreightDetailId = new Set<Id>();
        List<Commodity__c> listCommodities = new List<Commodity__c>();
        String hazCheckerResponse;
        
        try{
            listShipments = [SELECT Booking__c FROM Shipment__c WHERE Booking__c = :bookingId];  
            if(!listShipments.isEmpty()){
                for(Shipment__c shipment : listShipments)
                    setShipmentId.add(shipment.Id);
                listFreightDetails = [SELECT Shipment__c FROM FreightDetail__c WHERE Shipment__c IN :setShipmentId];
                if(!listFreightDetails.isEmpty()){
                    for(FreightDetail__c freight : listFreightDetails)
                        setFreightDetailId.add(freight.Id);
                    listCommodities = [SELECT Id, Name, Freight__c, IMO_Class__c, Name__c, Number__c, Package_Group__c, Phone_Number__c, Technical_Name__c 
                                       FROM Commodity__c WHERE Freight__c IN :setFreightDetailId];
                }
            }
            /*if(listCommodities != Null && !listCommodities.isEmpty())
                hazCheckerResponse = CustomerCommunity_CommodityController.validateHazardousBooking(bookingId, listCommodities);*/
            return hazCheckerResponse;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    @AuraEnabled
    public static Booking__c getCICSBookingNumber(Id bookingId){
        List<Booking__c> listBookingRecord = new List<Booking__c>();
        try{
            listBookingRecord[0] = CustomerCommunity_CreateCICSBooking.CreateCICSBooking(bookingId, false);
            return listBookingRecord[0];
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
}