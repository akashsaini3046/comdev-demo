@isTest
public class CustomerCommunity_MtchngPortChatbt_Test {

    static testmethod void getMatchingPorts_Test(){
        
        List<String> listports_Test1 = new List<String>();
        listports_Test1.add('BQ, BONAIRE');
        listports_Test1.add('BB, BRIDGETOWN');
        listports_Test1.add('VI, CHRISTENSTED-CFS');
        
        CustomerCommunity_MatchingPortsChatbot.getMatchingPorts(listports_Test1);
       
    }
    
    static testmethod void getMatchingPorts_CatchTest(){
        
        List<String> listports_Test2 = new List<String>();
        
        CustomerCommunity_MatchingPortsChatbot.getMatchingPorts(listports_Test2);
    }
}