public class CC_FindARouteController {
    @AuraEnabled 
    public static List<Location__c> getLocations(String type){
        List<Location__c> locations = new List<Location__c>();
        if(type == 'D')
        	locations = [SELECT Name, LcCode__c, ZipCode__c, UN_Location_Code__c,Location_Name__c, Country_Name__c FROM Location__c WHERE Location_Type__c = 'ZIPCODE' LIMIT 50000 ];
        if(type == 'P')
            locations = [SELECT Name, Country_Name__c FROM Location__c WHERE Location_Type__c ='CITY' Or Location_Type__c ='PORT'];
        if(type == 'R')
            locations = [SELECT Name, Country_Name__c FROM Location__c WHERE (Location_Type__c ='CITY' Or Location_Type__c ='PORT') AND Country_Code__c ='US' ];
        return locations;
    }
    
    @AuraEnabled
    public static List<Location__c> getSublocations(String type,String loctionCode){
        List<Location__c> subLocations = new List<Location__c>();
        if(type == 'P')
            subLocations = [SELECT Name FROM Location__c WHERE UN_Location_Code__c =: loctionCode AND Sub_Location_Type__c =: 'BERTH'];
        if(type == 'R')
        	subLocations = [SELECT Name FROM Location__c WHERE UN_Location_Code__c =: loctionCode AND Sub_Location_Type__c =: 'RAIL'];
        if(subLocations.size()>0)
        	return subLocations;
        else
            return null;
    }
    @AuraEnabled
    public static CustomerCommunity_RatingAPIResponse getRoutesList(String originCode, String destinationCode, String originType, String destinationType, Date readyDate,String receiptTypeCode, String deliveryTypeCode, String toSubLocation, String fromSubLocation,String description){
        try{
            System.debug('getRoutesList');
            BookingWrapper bwrap = createBookingWrapper(originCode,destinationCode,originType,destinationType,readyDate,receiptTypeCode,deliveryTypeCode,toSubLocation,fromSubLocation,description);
            System.debug('bwrap '+bwrap);
            CustomerCommunity_RatingAPIRequest reqString = BookingFCLRatesService.createRequestWrapper(bwrap);
            System.debug('reqString '+reqString);
            CustomerCommunity_RatingAPIResponse resString = BookingFCLRatesService.sendRatingRequest(reqString);
            return resString;
        }
        catch(Exception ex){
            throw ex;
        }
        
        
    }
    
    public static BookingWrapper createBookingWrapper(String sCityCode,String dCityCode, String originType, String destinationType,Date readyDate,String receiptTypeCode, String deliveryTypeCode, String toSubLocation, String fromSubLocation,String description){
        
        BookingWrapper bookingWrapper = BookingWrapper.getBookingWrapper();
        
        bookingWrapper.booking.Origin_Type__c = originType;
        bookingWrapper.booking.Destination_Type__c = destinationType;
        bookingWrapper.booking.Customer_Origin_Code__c = sCityCode;
        bookingWrapper.booking.Customer_Destination_Code__c = dCityCode;
       	bookingWrapper.booking.Transportation_Management_System_Origin__c=receiptTypeCode;
        bookingWrapper.booking.Transportation_Management_System_Destina__c=deliveryTypeCode;
        bookingWrapper.booking.To_Sub_Location__c=toSubLocation;
        bookingWrapper.booking.From_Sub_Location__c=fromSubLocation;
        bookingWrapper.booking.Ready_Date__c =  readyDate;
        bookingWrapper.booking.Description__c = description;
        
        bookingWrapper.shipment.listCargo[0].cargoType = 'Container';
        bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper[0].requirement.Is_Empty__c = false;
        bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper[0].containerType = '40DS';
        bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper[0].containerDesc = '40\' Dry Container';
        bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper[0].requirement.Quantity__c = 1;
        bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper[0].commodityDesc =  'Cargo, NOS';
        
    
        
        
        return bookingWrapper;
    }
}