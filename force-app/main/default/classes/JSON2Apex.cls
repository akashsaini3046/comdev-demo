public class JSON2Apex {
    public List<Colors> colors{get;set;}
    
	public class Colors {
		public String color {get;set;} 
		public String context {get;set;} 
		public String theme {get;set;} 
	}
	
	public class Details {
		public Boolean aggregatable {get;set;} 
		public Boolean aiPredictionField {get;set;} 
		public Boolean autoNumber {get;set;} 
		public Integer byteLength {get;set;} 
		public Boolean calculated {get;set;} 
		public Object calculatedFormula {get;set;} 
		public Boolean cascadeDelete {get;set;} 
		public Boolean caseSensitive {get;set;} 
		public Object compoundFieldName {get;set;} 
		public Object controllerName {get;set;} 
		public Boolean createable {get;set;} 
		public Boolean custom {get;set;} 
		public Object defaultValue {get;set;} 
		public Object defaultValueFormula {get;set;} 
		public Boolean defaultedOnCreate {get;set;} 
		public Boolean dependentPicklist {get;set;} 
		public Boolean deprecatedAndHidden {get;set;} 
		public Integer digits {get;set;} 
		public Boolean displayLocationInDecimal {get;set;} 
		public Boolean encrypted {get;set;} 
		public Boolean externalId {get;set;} 
		public Object extraTypeInfo {get;set;} 
		public Boolean filterable {get;set;} 
		public Object filteredLookupInfo {get;set;} 
		public Boolean formulaTreatNullNumberAsZero {get;set;} 
		public Boolean groupable {get;set;} 
		public Boolean highScaleNumber {get;set;} 
		public Boolean htmlFormatted {get;set;} 
		public Boolean idLookup {get;set;} 
		public Object inlineHelpText {get;set;} 
		public String label {get;set;} 
		public Integer length {get;set;} 
		public Object mask {get;set;} 
		public Object maskType {get;set;} 
		public String name {get;set;} 
		public Boolean nameField {get;set;} 
		public Boolean namePointing {get;set;} 
		public Boolean nillable {get;set;} 
		public Boolean permissionable {get;set;} 
		public List<PicklistValues> picklistValues {get;set;} 
		public Boolean polymorphicForeignKey {get;set;} 
		public Integer precision {get;set;} 
		public Boolean queryByDistance {get;set;} 
		public Object referenceTargetField {get;set;} 
		public List<ReferenceTo> referenceTo {get;set;} 
		public Object relationshipName {get;set;} 
		public Object relationshipOrder {get;set;} 
		public Boolean restrictedDelete {get;set;} 
		public Boolean restrictedPicklist {get;set;} 
		public Integer scale {get;set;} 
		public Boolean searchPrefilterable {get;set;} 
		public String soapType {get;set;} 
		public Boolean sortable {get;set;} 
		public String type_Z {get;set;} // in json: type
		public Boolean unique {get;set;} 
		public Boolean updateable {get;set;} 
		public Boolean writeRequiresMasterRead {get;set;} 

		public Details(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'aggregatable') {
							aggregatable = parser.getBooleanValue();
						} else if (text == 'aiPredictionField') {
							aiPredictionField = parser.getBooleanValue();
						} else if (text == 'autoNumber') {
							autoNumber = parser.getBooleanValue();
						} else if (text == 'byteLength') {
							byteLength = parser.getIntegerValue();
						} else if (text == 'calculated') {
							calculated = parser.getBooleanValue();
						} else if (text == 'calculatedFormula') {
							calculatedFormula = parser.readValueAs(Object.class);
						} else if (text == 'cascadeDelete') {
							cascadeDelete = parser.getBooleanValue();
						} else if (text == 'caseSensitive') {
							caseSensitive = parser.getBooleanValue();
						} else if (text == 'compoundFieldName') {
							compoundFieldName = parser.readValueAs(Object.class);
						} else if (text == 'controllerName') {
							controllerName = parser.readValueAs(Object.class);
						} else if (text == 'createable') {
							createable = parser.getBooleanValue();
						} else if (text == 'custom') {
							custom = parser.getBooleanValue();
						} else if (text == 'defaultValue') {
							defaultValue = parser.readValueAs(Object.class);
						} else if (text == 'defaultValueFormula') {
							defaultValueFormula = parser.readValueAs(Object.class);
						} else if (text == 'defaultedOnCreate') {
							defaultedOnCreate = parser.getBooleanValue();
						} else if (text == 'dependentPicklist') {
							dependentPicklist = parser.getBooleanValue();
						} else if (text == 'deprecatedAndHidden') {
							deprecatedAndHidden = parser.getBooleanValue();
						} else if (text == 'digits') {
							digits = parser.getIntegerValue();
						} else if (text == 'displayLocationInDecimal') {
							displayLocationInDecimal = parser.getBooleanValue();
						} else if (text == 'encrypted') {
							encrypted = parser.getBooleanValue();
						} else if (text == 'externalId') {
							externalId = parser.getBooleanValue();
						} else if (text == 'extraTypeInfo') {
							extraTypeInfo = parser.readValueAs(Object.class);
						} else if (text == 'filterable') {
							filterable = parser.getBooleanValue();
						} else if (text == 'filteredLookupInfo') {
							filteredLookupInfo = parser.readValueAs(Object.class);
						} else if (text == 'formulaTreatNullNumberAsZero') {
							formulaTreatNullNumberAsZero = parser.getBooleanValue();
						} else if (text == 'groupable') {
							groupable = parser.getBooleanValue();
						} else if (text == 'highScaleNumber') {
							highScaleNumber = parser.getBooleanValue();
						} else if (text == 'htmlFormatted') {
							htmlFormatted = parser.getBooleanValue();
						} else if (text == 'idLookup') {
							idLookup = parser.getBooleanValue();
						} else if (text == 'inlineHelpText') {
							inlineHelpText = parser.readValueAs(Object.class);
						} else if (text == 'label') {
							label = parser.getText();
						} else if (text == 'length') {
							length = parser.getIntegerValue();
						} else if (text == 'mask') {
							mask = parser.readValueAs(Object.class);
						} else if (text == 'maskType') {
							maskType = parser.readValueAs(Object.class);
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'nameField') {
							nameField = parser.getBooleanValue();
						} else if (text == 'namePointing') {
							namePointing = parser.getBooleanValue();
						} else if (text == 'nillable') {
							nillable = parser.getBooleanValue();
						} else if (text == 'permissionable') {
							permissionable = parser.getBooleanValue();
						} else if (text == 'picklistValues') {
							picklistValues = arrayOfPicklistValues(parser);
						} else if (text == 'polymorphicForeignKey') {
							polymorphicForeignKey = parser.getBooleanValue();
						} else if (text == 'precision') {
							precision = parser.getIntegerValue();
						} else if (text == 'queryByDistance') {
							queryByDistance = parser.getBooleanValue();
						} else if (text == 'referenceTargetField') {
							referenceTargetField = parser.readValueAs(Object.class);
						} else if (text == 'referenceTo') {
							referenceTo = arrayOfReferenceTo(parser);
						} else if (text == 'relationshipName') {
							relationshipName = parser.readValueAs(Object.class);
						} else if (text == 'relationshipOrder') {
							relationshipOrder = parser.readValueAs(Object.class);
						} else if (text == 'restrictedDelete') {
							restrictedDelete = parser.getBooleanValue();
						} else if (text == 'restrictedPicklist') {
							restrictedPicklist = parser.getBooleanValue();
						} else if (text == 'scale') {
							scale = parser.getIntegerValue();
						} else if (text == 'searchPrefilterable') {
							searchPrefilterable = parser.getBooleanValue();
						} else if (text == 'soapType') {
							soapType = parser.getText();
						} else if (text == 'sortable') {
							sortable = parser.getBooleanValue();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'unique') {
							unique = parser.getBooleanValue();
						} else if (text == 'updateable') {
							updateable = parser.getBooleanValue();
						} else if (text == 'writeRequiresMasterRead') {
							writeRequiresMasterRead = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Details consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public ButtonLayoutSection buttonLayoutSection {get;set;} 
	public List<DetailLayoutSections> detailLayoutSections {get;set;} 
	public List<DetailLayoutSections> editLayoutSections {get;set;} 
	public Object feedView {get;set;} 
	public Object highlightsPanelLayoutSection {get;set;} 
	public String id {get;set;} 
	public List<ReferenceTo> multirowEditLayoutSections {get;set;} 
	public List<ReferenceTo> offlineLinks {get;set;} 
	public Object quickActionList {get;set;} 
	public Object relatedContent {get;set;} 
	public List<ReferenceTo> relatedLists {get;set;} 
	public List<ReferenceTo> saveOptions {get;set;} 
	
	public class DetailButtons {
		public Object behavior {get;set;} 
		public List<Colors> colors {get;set;} 
		public Object content {get;set;} 
		public Object contentSource {get;set;} 
		public Boolean custom {get;set;} 
		public Object encoding {get;set;} 
		public Object height {get;set;} 
		public List<Icons> icons {get;set;} 
		public String label {get;set;} 
		public Boolean menubar {get;set;} 
		public String name {get;set;} 
		public Boolean overridden {get;set;} 
		public Boolean resizeable {get;set;} 
		public Boolean scrollbars {get;set;} 
		public Boolean showsLocation {get;set;} 
		public Boolean showsStatus {get;set;} 
		public Boolean toolbar {get;set;} 
		public Object url {get;set;} 
		public Object width {get;set;} 
		public Object windowPosition {get;set;} 

		public DetailButtons(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'behavior') {
							behavior = parser.readValueAs(Object.class);
						} else if (text == 'content') {
							content = parser.readValueAs(Object.class);
						} else if (text == 'contentSource') {
							contentSource = parser.readValueAs(Object.class);
						} else if (text == 'custom') {
							custom = parser.getBooleanValue();
						} else if (text == 'encoding') {
							encoding = parser.readValueAs(Object.class);
						} else if (text == 'height') {
							height = parser.readValueAs(Object.class);
						} else if (text == 'icons') {
							icons = arrayOfIcons(parser);
						} else if (text == 'label') {
							label = parser.getText();
						} else if (text == 'menubar') {
							menubar = parser.getBooleanValue();
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'overridden') {
							overridden = parser.getBooleanValue();
						} else if (text == 'resizeable') {
							resizeable = parser.getBooleanValue();
						} else if (text == 'scrollbars') {
							scrollbars = parser.getBooleanValue();
						} else if (text == 'showsLocation') {
							showsLocation = parser.getBooleanValue();
						} else if (text == 'showsStatus') {
							showsStatus = parser.getBooleanValue();
						} else if (text == 'toolbar') {
							toolbar = parser.getBooleanValue();
						} else if (text == 'url') {
							url = parser.readValueAs(Object.class);
						} else if (text == 'width') {
							width = parser.readValueAs(Object.class);
						} else if (text == 'windowPosition') {
							windowPosition = parser.readValueAs(Object.class);
						} else {
							System.debug(LoggingLevel.WARN, 'DetailButtons consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class ReferenceTo {

		public ReferenceTo(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'ReferenceTo consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class DetailLayoutSections {
		public Boolean collapsed {get;set;} 
		public Integer columns {get;set;} 
		public String heading {get;set;} 
		public List<LayoutRows> layoutRows {get;set;} 
		public String layoutSectionId {get;set;} 
		public String parentLayoutId {get;set;} 
		public Integer rows {get;set;} 
		public String tabOrder {get;set;} 
		public Boolean useCollapsibleSection {get;set;} 
		public Boolean useHeading {get;set;} 

		public DetailLayoutSections(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'collapsed') {
							collapsed = parser.getBooleanValue();
						} else if (text == 'columns') {
							columns = parser.getIntegerValue();
						} else if (text == 'heading') {
							heading = parser.getText();
						} else if (text == 'layoutRows') {
							layoutRows = arrayOfLayoutRows(parser);
						} else if (text == 'layoutSectionId') {
							layoutSectionId = parser.getText();
						} else if (text == 'parentLayoutId') {
							parentLayoutId = parser.getText();
						} else if (text == 'rows') {
							rows = parser.getIntegerValue();
						} else if (text == 'tabOrder') {
							tabOrder = parser.getText();
						} else if (text == 'useCollapsibleSection') {
							useCollapsibleSection = parser.getBooleanValue();
						} else if (text == 'useHeading') {
							useHeading = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'DetailLayoutSections consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Icons {
		public String contentType {get;set;} 
		public Integer height {get;set;} 
		public String theme {get;set;} 
		public String url {get;set;} 
		public Integer width {get;set;} 

		public Icons(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'contentType') {
							contentType = parser.getText();
						} else if (text == 'height') {
							height = parser.getIntegerValue();
						} else if (text == 'theme') {
							theme = parser.getText();
						} else if (text == 'url') {
							url = parser.getText();
						} else if (text == 'width') {
							width = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Icons consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class PicklistValues {
		public Boolean active {get;set;} 
		public Boolean defaultValue {get;set;} 
		public String label {get;set;} 
		public Object validFor {get;set;} 
		public String value {get;set;} 

		public PicklistValues(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'active') {
							active = parser.getBooleanValue();
						} else if (text == 'defaultValue') {
							defaultValue = parser.getBooleanValue();
						} else if (text == 'label') {
							label = parser.getText();
						} else if (text == 'validFor') {
							validFor = parser.readValueAs(Object.class);
						} else if (text == 'value') {
							value = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'PicklistValues consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class LayoutItems {
		public Boolean editableForNew {get;set;} 
		public Boolean editableForUpdate {get;set;} 
		public String label {get;set;} 
		public List<LayoutComponents> layoutComponents {get;set;} 
		public Boolean placeholder {get;set;} 
		public Boolean required {get;set;} 

		public LayoutItems(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'editableForNew') {
							editableForNew = parser.getBooleanValue();
						} else if (text == 'editableForUpdate') {
							editableForUpdate = parser.getBooleanValue();
						} else if (text == 'label') {
							label = parser.getText();
						} else if (text == 'layoutComponents') {
							layoutComponents = arrayOfLayoutComponents(parser);
						} else if (text == 'placeholder') {
							placeholder = parser.getBooleanValue();
						} else if (text == 'required') {
							required = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'LayoutItems consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class LayoutComponents {
		public Details details {get;set;} 
		public Integer displayLines {get;set;} 
		public Integer tabOrder {get;set;} 
		public String type_Z {get;set;} // in json: type
		public String value {get;set;} 

		public LayoutComponents(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'details') {
							details = new Details(parser);
						} else if (text == 'displayLines') {
							displayLines = parser.getIntegerValue();
						} else if (text == 'tabOrder') {
							tabOrder = parser.getIntegerValue();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'value') {
							value = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'LayoutComponents consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class LayoutRows {
		public List<LayoutItems> layoutItems {get;set;} 
		public Integer numItems {get;set;} 

		public LayoutRows(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'layoutItems') {
							layoutItems = arrayOfLayoutItems(parser);
						} else if (text == 'numItems') {
							numItems = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'LayoutRows consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class ButtonLayoutSection {
		public List<DetailButtons> detailButtons {get;set;} 

		public ButtonLayoutSection(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'detailButtons') {
							detailButtons = arrayOfDetailButtons(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'ButtonLayoutSection consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	



    private static List<PicklistValues> arrayOfPicklistValues(System.JSONParser p) {
        List<PicklistValues> res = new List<PicklistValues>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new PicklistValues(p));
        }
        return res;
    }


    private static List<DetailLayoutSections> arrayOfDetailLayoutSections(System.JSONParser p) {
        List<DetailLayoutSections> res = new List<DetailLayoutSections>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new DetailLayoutSections(p));
        }
        return res;
    }


    private static List<ReferenceTo> arrayOfReferenceTo(System.JSONParser p) {
        List<ReferenceTo> res = new List<ReferenceTo>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new ReferenceTo(p));
        }
        return res;
    }

    private static List<LayoutItems> arrayOfLayoutItems(System.JSONParser p) {
        List<LayoutItems> res = new List<LayoutItems>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new LayoutItems(p));
        }
        return res;
    }


    private static List<LayoutComponents> arrayOfLayoutComponents(System.JSONParser p) {
        List<LayoutComponents> res = new List<LayoutComponents>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new LayoutComponents(p));
        }
        return res;
    }




    private static List<DetailButtons> arrayOfDetailButtons(System.JSONParser p) {
        List<DetailButtons> res = new List<DetailButtons>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new DetailButtons(p));
        }
        return res;
    }










    private static List<Icons> arrayOfIcons(System.JSONParser p) {
        List<Icons> res = new List<Icons>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Icons(p));
        }
        return res;
    }




    private static List<LayoutRows> arrayOfLayoutRows(System.JSONParser p) {
        List<LayoutRows> res = new List<LayoutRows>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new LayoutRows(p));
        }
        return res;
    }


}