public class contactListController {
    public Account acct{get; set;}
    public List<Contact> listCon{get; set;}
    public String accName{get; set;}
    
    public pageReference showAccount(){
        if(accName != '' && accName != Null){
            this.acct = [SELECT Id, Name FROM Account WHERE Name = :accName LIMIT 1];
            listCon = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE AccountId = :acct.Id];
        }
        return Null;
    } 
    
    public pageReference editContact(){
        String contactId = Apexpages.currentpage().getParameters().get('contactId');  
        pageReference pageRef = new pageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + contactId + '/e?retURL=' + acct.Id);  
        return pageRef; 
    }
    
    public pageReference deleteContact(){  
        String contactId = Apexpages.currentpage().getParameters().get('contactId');  
        contact contactList = [SELECT Id FROM contact WHERE id = : contactId LIMIT 1]; 
        delete contactList; 
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm(); 
        PageReference redirectPage = new PageReference(baseUrl+'/'+acct.id); 
        return redirectPage;  
    }  
}