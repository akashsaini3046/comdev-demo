global without sharing class CrowleyCommunity_RegisterController {
	public static List < String > getCountries(String searchKey) {
		List < String > picklistEntries = new List < String > ();
		List < String > listCountries = new List < String > ();
		try {
			Schema.DescribeFieldResult PicklistCountriesGlobal = Schema.SObjectType.Address__c.fields.Country__c;
			List < PicklistEntry > entries = PicklistCountriesGlobal.getSObjectField().getDescribe().getPicklistValues();

			for (PicklistEntry entry: entries) {
				picklistEntries.add(entry.getLabel().toUpperCase());
			}
			for (String entry: picklistEntries) {
				if (entry.contains(searchKey)) {
					listCountries.add(entry);
				}
			}
			if (listCountries != null) {
				return listCountries;
			} else {
				return null;
			}
		} catch(Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return null;
		}
	}
	public static List < String > getStates(String searchKey) {

		List < String > picklistEntries = new List < String > ();
		List < String > listStates = new List < String > ();
		try {
			Schema.DescribeFieldResult PicklistCountriesGlobal = Schema.SObjectType.Address__c.fields.State_Picklist__c;
			List < PicklistEntry > entries = PicklistCountriesGlobal.getSObjectField().getDescribe().getPicklistValues();
			for (PicklistEntry entry: entries) {
				picklistEntries.add(entry.getLabel());
			}
			for (String entry: picklistEntries) {
				if (entry.contains(searchKey)) {
					listStates.add(entry);
				}
			}
			if (listStates != null) {
				return listStates;
			} else {
				return null;
			}
		}
		catch(Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return null;
		}
	}@AuraEnabled
	public static String checkAccountExists(CrowleyCommunity_AccountDetail accountDetails) {

		
		List < Contact > listContact = new List < Contact > ();
		Account accountRecord = new Account();
		Contact contactRecord = new Contact();
        Address__c newAddressRecord = new Address__c();
        System.debug('accountDetails@@@'+ accountDetails);
        
		if (accountDetails.addNewAccount) {
			try {
				listContact = [Select id from Contact where email = :accountDetails.userEmail LIMIT 1];
				System.debug('listContact@@@' + listContact);
				if (!listContact.isEmpty()) {
					return 'EXCEPTION';
				}
				List < Lead > listLead = [Select id from Lead where email = :accountDetails.userEmail LIMIT 1];
				System.debug('listLead@@@' + listLead);
				if (!listLead.isEmpty()) {
					return 'EXCEPTION1';
				}
                accountRecord = CrowleyCommunity_RegisterController.createAccount(accountDetails);
				insert accountRecord;

                newAddressRecord = CrowleyCommunity_RegisterController.createBusinessLocation(accountDetails);
                newAddressRecord.Account__c = accountRecord.Id;
                insert newAddressRecord;
                 
                contactRecord = CrowleyCommunity_RegisterController.createContact(accountDetails);
				contactRecord.accountId = accountRecord.Id;		
                contactRecord.Address__c = newAddressRecord.Id;
				insert contactRecord;
                System.debug('Creating all New Account Location and Contact @@@'+contactRecord);
				return contactRecord.Id;
			}
			catch(Exception ex) {
				ExceptionHandler.logApexCalloutError(ex);
				return null;
			}
		}
		else {   
           try{
            if(accountDetails.addNewLocation){
                newAddressRecord = CrowleyCommunity_RegisterController.createBusinessLocation(accountDetails);
                System.debug('@@@'+newAddressRecord);
                newAddressRecord.Account__c = accountDetails.accountRecordId;
                insert newAddressRecord; 
                
                listContact = [Select id,Address__c from Contact where email = :accountDetails.userEmail LIMIT 1]; 
                System.debug('listContact@@@'+listContact);
                
                if (accountDetails.newContact) {       
                    contactRecord = CrowleyCommunity_RegisterController.createContact(accountDetails);                    
                    contactRecord.accountId =  accountDetails.accountRecordId;   
                    contactRecord.Address__c = newAddressRecord.Id;
                    insert contactRecord;
                    System.debug('creating New contact and New Location @@@'+contactRecord);
                    return contactRecord.Id;
                }
                else{   
                   listContact.get(0).Address__c = newAddressRecord.Id;
                   update listContact.get(0);
                   System.debug('Updating contact and Creating New Location @@@'+ listContact.get(0)); 
                   return listContact.get(0).Id;
                }
            }
            else{
                listContact = [Select id,Address__c from Contact where email = :accountDetails.userEmail LIMIT 1]; 
                if (listContact.isEmpty()) {     
                    contactRecord = CrowleyCommunity_RegisterController.createContact(accountDetails);    
                    contactRecord.accountId = accountDetails.accountRecordId;
                    contactRecord.Address__c = accountDetails.locationRecordId;
                    insert contactRecord;
                    System.debug('Creating the contact and Linking to Old Location' + contactRecord);
                    return contactRecord.Id;
                }
                else{
                   listContact.get(0).Address__c = accountDetails.locationRecordId;
                   update listContact.get(0);
                   System.debug('Updating contact and Linking it to Old Location' +listContact.get(0));
                   return listContact.get(0).Id;
                }   
            }   
         }
            catch(Exception ex){
                ExceptionHandler.logApexCalloutError(ex);
                return null; 
            }
        } 
	}
    public static String mapCountryToCode(String countryName){
        Map<String,String> mapCountryCode = new Map<String,String>();
        try{
            Schema.DescribeFieldResult PicklistCountriesGlobal = Schema.SObjectType.Address__c.fields.Country__c;
            List <PicklistEntry> entries = PicklistCountriesGlobal.getSObjectField().getDescribe().getPicklistValues();
            for (PicklistEntry entry: entries) {
                mapCountryCode.put(entry.getLabel().toUpperCase(),entry.getValue().toUpperCase());
            }
            System.debug('mapCountryCode@@'+mapCountryCode);
            if(mapCountryCode != NULL && !mapCountryCode.isEmpty()){
                if(mapCountryCode.containsKey(countryName)){
                    return mapCountryCode.get(countryName);
                }
            }
            return 'NULL';
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return 'NULL';
        }      
    }
    public static String mapStateToCode(String stateName){
        
        Map<String,String> mapStateCode = new Map<String,String>(); 
        try{
            Schema.DescribeFieldResult PicklistCountriesGlobal = Schema.SObjectType.Address__c.fields.State_Picklist__c;
            List <PicklistEntry> entries = PicklistCountriesGlobal.getSObjectField().getDescribe().getPicklistValues();
            for (PicklistEntry entry: entries) {
                mapStateCode.put(entry.getLabel().toUpperCase(),entry.getValue().toUpperCase());
            }
            System.debug('mapStateCode@@'+mapStateCode);     
            if(mapStateCode != NULL && !mapStateCode.isEmpty()){
                if(mapStateCode.containsKey(stateName)){
                    return mapStateCode.get(stateName);
                }
            }
              return 'NULL';
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex); 
            return 'NULL';
        }    
    }
    public static Account createAccount(CrowleyCommunity_AccountDetail accountDetails){
        
        Account accountRecord = new Account();
        try{
            if(accountDetails.companyName != NULL){
                accountRecord.Name = accountDetails.companyName;
            }
            accountRecord.OwnerId = CustomerCommunity_Constants.OWNER_ID;
            return accountRecord;
        }
        catch(Exception ex){
          ExceptionHandler.logApexCalloutError(ex);
          return NULL; 
        }        
    }
    public static Contact createContact(CrowleyCommunity_AccountDetail accountDetails){
        
        Contact contactRecord = new Contact();
        try{
            if(accountDetails.firstName != NULL){
                contactRecord.FirstName = accountDetails.firstName;
            }
            if(accountDetails.phoneNumber != NULL){
                contactRecord.Phone = accountDetails.phoneNumber;
            }
            if(accountDetails.lastName != NULL){
                contactRecord.LastName = accountDetails.lastName;
            }
            if(accountDetails.userEmail != NULL){
                contactRecord.Email = accountDetails.userEmail;
            }
            if( contactRecord.MailingCountry != NULL){
            contactRecord.MailingCountry = accountDetails.countryName;
            }
            contactRecord.Locations_Served__c = 'United States';
            return contactRecord;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;  
        }  
    }
    public static Address__c createBusinessLocation(CrowleyCommunity_AccountDetail accountDetails){
     
        Address__c addressRecord = new Address__c();    
        try{
            addressRecord = new Address__c();
            if(accountDetails.addressLine1 != NULL){
                addressRecord.Name = accountDetails.addressLine1;
            }
            addressRecord.Address_Line_2__c = accountDetails.addressLine2;
            if(CrowleyCommunity_RegisterController.mapCountryToCode(accountDetails.countryName) != NULL){
                addressRecord.Country__c = CrowleyCommunity_RegisterController.mapCountryToCode(accountDetails.countryName);
            }
            if( CrowleyCommunity_RegisterController.mapStateToCode(accountDetails.stateName) != NULL){
                addressRecord.State_Picklist__c = CrowleyCommunity_RegisterController.mapStateToCode(accountDetails.stateName);
            }
            if(accountDetails.cityName != NULL){
                addressRecord.City__c = accountDetails.cityName;
            }
            if(accountDetails.zipCode != NULL){
                addressRecord.Postal_Code__c = accountDetails.zipCode;
            }
            System.debug('addressRecord@@@'+addressRecord+'@@'+addressRecord.Id);
            return addressRecord;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return NULL; 
        }  
    }
	public static String generateRandomString(Integer len) {
		final String chars = CustomerCommunity_Constants.CHARS;
		String randStr = '';
		while (randStr.length() < len) {
			Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			randStr += chars.substring(idx, idx + 1);
		}
		return randStr;
	}@AuraEnabled
	public static String registerUser(CrowleyCommunity_UserDetail userDetails, String contactId) {
		List < User > listUser = new List < User > ();

		List < User > duplicateNameList = [SELECT id FROM User WHERE UserName = :userDetails.userName];
		if (!duplicateNameList.isEmpty()) {
			return CustomerCommunity_Constants.EXISTS_MESSAGE;
		}
		List < String > profiles = new List < String > {
			CrowleyCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME
		};

		List < User > duplicateEmailList = [SELECT Id, ContactId FROM User WHERE Email = :userDetails.userEmail AND Profile.Name IN: profiles];
		try {
			if (duplicateEmailList.isEmpty()) {
				User newuser = new User();
				newuser.ProfileId = [SELECT Id, Profile.Name FROM Profile WHERE Profile.Name = :CrowleyCommunity_Constants.CUSTOMER_LOGIN_PROFILE_NAME
				LIMIT 1].ID;
				newuser.firstName = userDetails.firstName;
				newuser.lastName = userDetails.lastName;
				newuser.Phone = userDetails.phoneNumber;
				newuser.Title = userDetails.lastName + generateRandomString(8);
				newuser.Alias = CustomerCommunity_Constants.USER_ALIAS;
				newuser.Country = userDetails.countryName;
				newuser.ContactId = contactId;
				newuser.Username = userDetails.userName;
				newuser.Email = userDetails.userEmail;
				newuser.CommunityNickname = generateRandomString(8);
				newuser.TimeZoneSidKey = CustomerCommunity_Constants.TIMEZONE_SID_KEY;
				newuser.LocaleSidKey = CustomerCommunity_Constants.LOCALE_SID_KEY;
				newuser.EmailEncodingKey = CustomerCommunity_Constants.EMAIL_ENCODING_KEY;
				newuser.LanguageLocaleKey = CustomerCommunity_Constants.LANG_LOCALE_KEY;
				Database.DMLOptions dlo = new Database.DMLOptions();
				dlo.EmailHeader.triggerUserEmail = true;
				Database.SaveResult srList = Database.insert(newuser, dlo);
				System.debug('srList@@@' + srList);
				if (srList != NULL) {

					{
						if (srList.isSuccess()) {
							List < PermissionSet > permissionList = [SELECT id, Label FROM PermissionSet where Label = :CustomerCommunity_Constants.PERMISSION_SET_NAME
							LIMIT 1];
							if (!permissionList.isEmpty()) {
								PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionList.get(0).Id, AssigneeId = newuser.Id);
								insert psa;
							}
						}
					}
				}
				return CustomerCommunity_Constants.TRUE_MESSAGE;
			} else {
				return CustomerCommunity_Constants.FALSE_MESSAGE;
			}
		} catch(Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
			return CustomerCommunity_Constants.ERROR_MESSAGE;
		}
	}
}