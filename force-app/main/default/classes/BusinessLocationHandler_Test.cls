@isTest
public class BusinessLocationHandler_Test {

    static testMethod void BusinessLocationHandler_TestMethod()
    {
        Account accObj = new Account();
        accObj.Name = 'Account 123';
        insert accObj;
        
        DescribeFieldResult describe = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
        
        Address__c businessLocationObj1 = new Address__c();
        businessLocationObj1.Account__c = accObj.Id;
        businessLocationObj1.Address_Line_1__c = 'address1';
        businessLocationObj1.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj1.Country__c = availableValues[0].getValue();
        businessLocationObj1.Postal_Code__c = '1111111';
        businessLocationObj1.Phone__c = '88888888888';
        businessLocationObj1.State__c = 'State1';
        businessLocationObj1.Name ='BL1';
        insert businessLocationObj1;
        
        Address__c businessLocationObj2 = new Address__c();
        businessLocationObj2.Account__c = accObj.Id;
        businessLocationObj2.Address_Line_1__c = 'address1';
        businessLocationObj2.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj2.Country__c = availableValues[0].getValue();
        businessLocationObj2.Postal_Code__c = '1111111';
        businessLocationObj2.Phone__c = '88888888888';
        businessLocationObj2.State__c = 'State1';
        businessLocationObj2.Name ='BL1';
        insert businessLocationObj2;
        
        Contact con= new Contact();
        con.LastName='Test con1';
        con.Phone = '99999999999';
        con.Email='con@email.com';
        con.Address__c = businessLocationObj1.Id;
        insert con;
        
        List<Address__c> businessLocationListToDelete = new List<Address__c>();
        businessLocationListToDelete.add(businessLocationObj1);
        businessLocationListToDelete.add(businessLocationObj2);
        
        test.startTest();
        System.assertNotEquals(businessLocationObj1.Country__c, NULL);
        System.assertNotEquals(businessLocationObj2.Country__c, NULL);
        System.assertNotEquals(businessLocationObj1.Account__c, NULL);
        System.assertEquals(businessLocationObj1.Id, con.Address__c);
        
        try
        {
            delete businessLocationListToDelete;
        }
        catch(Exception e)
        {
            System.debug('Exception in Business Location Test Class'+e.getMessage());
        }
        test.stopTest();
    }
}