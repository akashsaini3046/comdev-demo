public with sharing class WrapperForDev {
    
    public static void displayBookingWrapper(){
        BookingWrapper bookingWrapper = WrapperForDev.getBookingWrapper('a060t000003D8ljAAC');
        System.debug(JSON.serializePretty(bookingWrapper));
    }

    public static BookingWrapper getBookingWrapper(Id bookingId){
        BookingWrapper finalWrapper = new BookingWrapper();
        
        Booking__c booking = new Booking__c();
        Shipment__c shipment = new Shipment__c();
        List<Party__c> parties = new List<Party__c>();
        List<FreightDetail__c> freightDetails = new List<FreightDetail__c>();
        List<Commodity__c> commodities = new List<Commodity__c>();
        List<Requirement__c> requirements = new List<Requirement__c>();
        
		String bookingFields = commaSeperatedFieldsValues('Booking__c');
        String partyFields = commaSeperatedFieldsValues('Party__c');
        String shipmentFields = commaSeperatedFieldsValues('Shipment__c');
        String freightDetailFields = commaSeperatedFieldsValues('FreightDetail__c');
        String commodityFields = commaSeperatedFieldsValues('Commodity__c');
        String requirementFields = commaSeperatedFieldsValues('Requirement__c');
            
        booking = Database.query('SELECT ' + bookingFields + ' FROM Booking__c WHERE Id =:bookingId LIMIT 1');
        parties = Database.query('SELECT ' + partyFields + ' FROM Party__c WHERE Booking__c =:bookingId');
        shipment = Database.query('SELECT ' + shipmentFields + ' FROM Shipment__c WHERE Booking__c =:bookingId LIMIT 1');
        if(shipment != null){
            Id shipmentId = shipment.Id;
            freightDetails = Database.query('SELECT ' + freightDetailFields + ' FROM FreightDetail__c WHERE Shipment__c =:shipmentId');
            if(freightDetails != null && !freightDetails.isEmpty()){
                Set<Id> freightDetailsIds = new Set<Id>();
                for(FreightDetail__c freightDetail : freightDetails){
                    freightDetailsIds.add(freightDetail.Id);
                }
                commodities = Database.query('SELECT ' + commodityFields + ' FROM Commodity__c WHERE Freight__c =:freightDetailsIds');
                requirements = Database.query('SELECT ' + requirementFields + ' FROM Requirement__c WHERE Freight__c =:freightDetailsIds');
            }
        }
        
        finalWrapper.booking = booking;
        finalWrapper.mapParty = getMapParty(parties);
        finalWrapper.shipment = getShipmentWrapper(shipment, freightDetails, commodities, requirements);
        
        return finalWrapper;
    }
    
    private static Map<String, List<Party__c>> getMapParty(List<Party__c> parties){
        Map<String, List<Party__c>> mapParty = new Map<String, List<Party__c>>();
        for(Party__c party : parties){
            if(party.Type__c.equals('CUST')){
                if(mapParty.get('Customer') != null && !mapParty.get('Customer').isEmpty()){
                    List<Party__c> customerList = mapParty.get('Customer');
                    customerList.add(party);
                    mapParty.put('Customer', customerList);
                }else{
                    mapParty.put('Customer', new List<Party__c>{party});
                }
            }
            if(party.Type__c.equals('SHP')){
                if(mapParty.get('shiper') != null && !mapParty.get('shiper').isEmpty()){
                    List<Party__c> customerList = mapParty.get('shiper');
                    customerList.add(party);
                    mapParty.put('shiper', customerList);
                }else{
                    mapParty.put('shiper', new List<Party__c>{party});
                }
            }
            if(party.Type__c.equals('CON')){
                if(mapParty.get('consignee') != null && !mapParty.get('consignee').isEmpty()){
                    List<Party__c> customerList = mapParty.get('consignee');
                    customerList.add(party);
                    mapParty.put('consignee', customerList);
                }else{
                    mapParty.put('consignee', new List<Party__c>{party});
                }
            }
        }
        return mapParty;
    }
    
    private static BookingWrapper.ShipmentWrapper getShipmentWrapper(Shipment__c shipment, List<FreightDetail__c> freightDetails, 
                                                                     List<Commodity__c> commodities, List<Requirement__c> requirements)
    {
        BookingWrapper.ShipmentWrapper shipmentWrapper = new BookingWrapper.ShipmentWrapper();
        shipmentWrapper.shipment = shipment;
        shipmentWrapper.listCargo = getCargoWrapperList(freightDetails, commodities, requirements);
        return shipmentWrapper;
    }
    
    private static List<BookingWrapper.CargoWrapper> getCargoWrapperList(List<FreightDetail__c> freightDetails, List<Commodity__c> commodities, List<Requirement__c> requirements){
        List<BookingWrapper.CargoWrapper> cargoWrapperList = new List<BookingWrapper.CargoWrapper>();      
        
        Map<Id, List<BookingWrapper.CommodityWrapper>> freightDetailIdVsCommodityWrapperList = new Map<Id, List<BookingWrapper.CommodityWrapper>>();
        for(Commodity__c commodity : commodities){
            if(freightDetailIdVsCommodityWrapperList.get(commodity.Freight__c) != null){
                List<BookingWrapper.CommodityWrapper> commodityWrapperList = freightDetailIdVsCommodityWrapperList.get(commodity.Freight__c);
                BookingWrapper.CommodityWrapper commodityWrapper = new BookingWrapper.CommodityWrapper();
                commodityWrapper.commodity = commodity;
                commodityWrapperList.add(commodityWrapper);
                freightDetailIdVsCommodityWrapperList.put(commodity.Freight__c, commodityWrapperList);
            }else{
                BookingWrapper.CommodityWrapper commodityWrapper = new BookingWrapper.CommodityWrapper();
                commodityWrapper.commodity = commodity;
                freightDetailIdVsCommodityWrapperList.put(commodity.Freight__c, new List<BookingWrapper.CommodityWrapper>{commodityWrapper});
            }
        }
        
        Map<Id, List<BookingWrapper.RequirementWrapper>> freightDetailIdVsRequirementWrapperList = new Map<Id, List<BookingWrapper.RequirementWrapper>>();
        for(Requirement__c requirement : requirements){
            if(freightDetailIdVsRequirementWrapperList.get(requirement.Freight__c) != null){
                List<BookingWrapper.RequirementWrapper> requirementWrapperList = freightDetailIdVsRequirementWrapperList.get(requirement.Freight__c);
                BookingWrapper.RequirementWrapper requirementWrapper = new BookingWrapper.RequirementWrapper();
                requirementWrapper.requirement = requirement;
                requirementWrapperList.add(requirementWrapper);
                freightDetailIdVsRequirementWrapperList.put(requirement.Freight__c, requirementWrapperList);
            }else{
                BookingWrapper.RequirementWrapper requirementWrapper = new BookingWrapper.RequirementWrapper();
                requirementWrapper.requirement = requirement;
                freightDetailIdVsRequirementWrapperList.put(requirement.Freight__c, new List<BookingWrapper.RequirementWrapper>{requirementWrapper});
            }
        }
        
        Map<String, List<BookingWrapper.FreightDetailWrapper>> cargoTypeVsFreightList = new Map<String, List<BookingWrapper.FreightDetailWrapper>>();
        for(FreightDetail__c freightDetail : freightDetails){
            if(cargoTypeVsFreightList != null && !cargoTypeVsFreightList.isEmpty() && cargoTypeVsFreightList.get(freightDetail.Cargo_Type__c) != null){
                List<BookingWrapper.FreightDetailWrapper> freightDetailWrapperList = cargoTypeVsFreightList.get(freightDetail.Cargo_Type__c);
                BookingWrapper.FreightDetailWrapper freightDetailWrapper = new BookingWrapper.FreightDetailWrapper();
                freightDetailWrapper.commodityDesc = '';
                freightDetailWrapper.freightDetail = freightDetail;
                if(freightDetailIdVsCommodityWrapperList != null && !freightDetailIdVsCommodityWrapperList.isEmpty() && freightDetailIdVsCommodityWrapperList.get(freightDetail.Id) != null){
                    freightDetailWrapper.listCommodityWrapper = freightDetailIdVsCommodityWrapperList.get(freightDetail.Id);
                }
                if(freightDetailIdVsRequirementWrapperList != null && !freightDetailIdVsRequirementWrapperList.isEmpty() && freightDetailIdVsRequirementWrapperList.get(freightDetail.Id) != null){
                    freightDetailWrapper.listRequirementWrapper = freightDetailIdVsRequirementWrapperList.get(freightDetail.Id);
                }
                freightDetailWrapperList.add(freightDetailWrapper);
                cargoTypeVsFreightList.put(freightDetail.Cargo_Type__c, freightDetailWrapperList);
            }else{
                BookingWrapper.FreightDetailWrapper freightDetailWrapper = new BookingWrapper.FreightDetailWrapper();
                freightDetailWrapper.commodityDesc = '';
                freightDetailWrapper.freightDetail = freightDetail;
                if(freightDetailIdVsCommodityWrapperList != null && !freightDetailIdVsCommodityWrapperList.isEmpty() && freightDetailIdVsCommodityWrapperList.get(freightDetail.Id) != null){
                    freightDetailWrapper.listCommodityWrapper = freightDetailIdVsCommodityWrapperList.get(freightDetail.Id);
                }
                if(freightDetailIdVsRequirementWrapperList != null && !freightDetailIdVsRequirementWrapperList.isEmpty() && freightDetailIdVsRequirementWrapperList.get(freightDetail.Id) != null){
                    freightDetailWrapper.listRequirementWrapper = freightDetailIdVsRequirementWrapperList.get(freightDetail.Id);
                }
                cargoTypeVsFreightList.put(freightDetail.Cargo_Type__c, new List<BookingWrapper.FreightDetailWrapper>{freightDetailWrapper});
            }
        }
        
        for(String cargoType : cargoTypeVsFreightList.keySet()){
            BookingWrapper.CargoWrapper cargoWrapper = new BookingWrapper.CargoWrapper();
            cargoWrapper.cargoType = cargoType;
            cargoWrapper.listFreightDetailWrapper = cargoTypeVsFreightList.get(cargoType);
            cargoWrapperList.add(cargoWrapper);
        }
        
        return cargoWrapperList;
    }
    
    private static String commaSeperatedFieldsValues(String sObjectName){
        String commaSeperatedFieldsValues = '';
        Map<String,Schema.SObjectField> fieldsMap = fetchAllFields(sObjectName);
        for(Schema.SObjectField fieldApiName : fieldsMap.values() ){
            commaSeperatedFieldsValues += (commaSeperatedFieldsValues != '' ? ', ' : '') + fieldApiName;
        }
        return commaSeperatedFieldsValues;
    }
    
    private static Map<String,Schema.SObjectField> fetchAllFields(String sObjectName){
        SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        Map<String,Schema.SObjectField> mfields = sObjectType.getDescribe().fields.getMap();
        return mfields;
    }
}