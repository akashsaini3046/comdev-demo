public class CustomerCommunity_BadgesController {
    @AuraEnabled
	public static void checkBadges(String username) {
        System.debug('checkbadges'+username);
		try {
			User currentUser=[Select id from User where username=:username];
			System.debug('currentUser--->'+currentUser.id);
			WorkBadgeDefinition workDef=[SELECT id,Name FROM WorkBadgeDefinition where Name='Great Work'];
            System.debug('workDef--->'+workDef);
            integer count= [SELECT count() FROM LoginHistory WHERE UserId = :currentUser.id];
            System.debug('Count--->'+Count);
			List<WorkBadge> receivedBadge=[SELECT id,RecipientId from WorkBadge where RecipientId = :currentUser.id and DefinitionId = :workDef.id];
			System.debug('Count--->'+Count+'receivedBadge-->'+receivedBadge);
            if(count>=25 && receivedBadge.size()==0){
				ID networkID = Network.getNetworkId();
				ID badgeDefintionID = workDef.id;
				ID giverID = '0050t00000209KE';
				ID recipientID = currentUser.id;
				String message = 'Welcome to the community';

				WorkThanks thanks = new WorkThanks (
				networkID=networkID,
				GiverID=giverID,
				Message=message
				);
				insert thanks;
                
				WorkBadge badge = new WorkBadge(
				DefinitionId=badgeDefintionID,
				NetworkID=networkID,
				RecipientId=recipientID,
				SourceId=thanks.id
				);
				insert badge;
				
				/*FeedItem feedItem = new FeedItem(
				NetworkScope=networkID,
				ParentId=recipientID,
				RelatedRecordId=thanks.id,
				Body=message,
				Type='RypplePost',
				Visibility='AllUsers'
				);
				insert feedItem;*/
                System.debug('badge--->'+badge);
			}
			
			
		} catch (Exception ex) {
			ExceptionHandler.logApexCalloutError(ex);
		}
	}

}