public class HazheckResponseQuable implements system.Queueable, Database.AllowsCallouts {
    private String strResponseAttachmentId;
    private String strBookingId;
    private String bookingName;
    private String strCTUWiseRequest;
    
    public HazheckResponseQuable(String strResponseAttachmentId, String strBookingId, String strCTUWiseRequest) {
    	this.strResponseAttachmentId =  strResponseAttachmentId;  
        this.strBookingId = strBookingId;
        this.strCTUWiseRequest = strCTUWiseRequest;
        bookingName = [SELECT Name FROM Booking__c WHERE Id =: strBookingId][0].Name;
    }
    
    public void execute(System.QueueableContext objCnxt) {
    				PageReference pdf = Page.HazcheckResponsePDF;
                    pdf.getParameters().put('hazardousResponse',strResponseAttachmentId);
        			pdf.getParameters().put('CTUWiseRequestJson',strCTUWiseRequest);
                    Attachment attach = new Attachment();
                    Blob  body = pdf.getContent();
					attach.Body = body;
                    attach.Name = 'HazardousResponse '+ bookingName + '.pdf';
                    attach.IsPrivate = false;
                    attach.ParentId = strBookingId;
                    insert attach;
                    delete new Attachment(Id = strResponseAttachmentId);		    
    }
}