public class CustomerCommunity_CICSBookingResponse {

    @AuraEnabled public String TotalAmount{get;set;}
    @AuraEnabled public String BookingNumber{get;set;}
    @AuraEnabled public Integer CWTransitDays{get; set;}

}