/*
 * 11-06-2020 – Lovesh Tomar – Created this class for handling Custom Mata Data Queries.
 *
 */

/*
 * @company    : Nagarro Inc.
 * @date    : 23-06-2020
 * @author    : Nagarro
 * @description  : DAO for CustomMetaData
 * @history    : Version 1.0
 * @test class  : CustomMetaDataDAOTest
 */
public class CustomMetaDataDAO implements ICustomMetaDataDAO {
    public interface ICustomMetaDataDAO {
        List < sObject > getCustomMetaData(String fields, String objectSOQL, String orderBy);
        List < sObject > getCustomMetaDataWithCondition(String fields, String objectSOQL, String whereCondition, String orderBy);
    }

    /*
     * @purpose    : Method to query custom meta data  records
     * @parameter  : fields - fields to be queried
     * @parameter  : objectSOQL - custom meta data to be queried
     * @parameter  : orderBy - Order By field
     * @return     : List<sObject> 
     */
    public static List < sObject > getCustomMetaData(String fields, String objectSOQL, String orderBy) {
        return Database.query('SELECT ' + fields + ' FROM ' + objectSOQL + ' ORDER BY ' + orderBy);
    }

    /*
     * @purpose    : Method to query custom meta data records
     * @parameter  : fields - fields to be queried
     * @parameter  : objectSOQL - custom meta data to be queried
     * @parameter  : whereCondition - where condition
     * @parameter  : orderBy - Order By field
     * @return     : List<sObject> 
     */
    public static List < sObject > getCustomMetaDataWithCondition(String fields, String objectSOQL, String whereCondition, String orderBy) {
        String query = 'SELECT ' + fields + ' FROM ' + objectSOQL;
        if (String.isNotBlank(whereCondition)) {
            query += ' WHERE ' + whereCondition;
        }
        if (String.isNotBlank(orderBy)) {
            query += ' ORDER BY ' + orderBy;
        }
        return Database.query(query);
    }
}