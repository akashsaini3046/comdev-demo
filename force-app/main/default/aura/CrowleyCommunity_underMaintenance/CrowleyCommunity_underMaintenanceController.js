({
	handleHomeButtonOnclick : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://comdev-crowleydev.cs77.force.com/CustomerCommunity/s/"
        });
        urlEvent.fire();
	},
    doInit : function(component,event,helper){
       helper.fireHighlightEvent(component, event);
    }
})