({
    fetchBookingAndRelatedData : function(component, event, helper, selectedMenu) {
        console.log(selectedMenu);
        this.showSpinner(component);
        // this.fetchFieldsDefinition(component, event, helper);
        var bookingId = component.get("v.recordId");
        // var bookingId = 'a060t000003BasOAAS'
        var action = component.get("c.getBookingDetail");
        action.setParams({
            bookingId : bookingId,
            selectedMenu : selectedMenu
            // bookingId : 'a060t000003BasOAAS'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log('### ---> Stringify Response');
                console.log(JSON.stringify(responseData));
                if(selectedMenu === 'details'){
                    this.setBookingRelatedData(component,responseData,"v.bookingFieldNameValue","Booking");
                    console.log('@@@BookingDetails : '+JSON.stringify(component.get("v.bookingFieldNameValue")));
                    this.setBookingRelatedData(component,responseData,"v.bookingServiceTypeFieldNameValue","BookingServiceType");
                    this.setBookingRelatedData(component,responseData,"v.bookingIMTypeFieldNameValue","BookingIMType");
                    this.setBookingRelatedData(component,responseData,"v.bookingTMSTypeFieldNameValue","BookingTMSType");
                    this.setBookingRelatedData(component,responseData,"v.bookingCustomerOriginFieldNameValue","BookingCustomerOrigin");
                    this.setBookingRelatedData(component,responseData,"v.bookingCustomerDestinationFieldNameValue","BookingCustomerDestination");
                    this.setBookingRelatedData(component,responseData,"v.bookingConnectingCarrierFieldNameValue","BookingConnectingCarrier");
                    this.setBookingRelatedData(component,responseData,"v.bookingConnectAtLocFieldNameValue","BookingConnectAtLoc");
                    this.setBookingRelatedData(component,responseData,"v.bookingConnectToLocFieldNameValue","BookingConnectToLoc");
                }
                if(selectedMenu === 'parties'){
                    this.setSectionData(component,responseData,"v.partyList","Party");
                }
                if(selectedMenu === 'transports'){
                    this.setSectionData(component,responseData,"v.transportList","Transport");
                }
                if(selectedMenu === 'stops'){    
                    this.setSectionData(component,responseData,"v.stopList","Stop");
                }
                if(selectedMenu === 'bookingRemarks'){
                    this.setSectionData(component,responseData,"v.bookingRemarkList","BookingRemark");
                }
                if(selectedMenu === 'shipment'){
                    this.setSectionData(component,responseData,"v.shipmentList","Shipment");
                }
                if(selectedMenu === 'customerNotifications'){
                    this.setSectionData(component,responseData,"v.customerNotificationList","CustomerNotification");
                }
                if(selectedMenu === 'voyages'){
                    this.setSectionData(component,responseData,"v.voyageList","Voyage");
                }
                if(selectedMenu === 'dockReceipts'){
                    this.setSectionData(component,responseData,"v.dockReceiptList","DockReceipt");
                }
                if(selectedMenu === 'freightDetails'){
                    this.setSectionData(component,responseData,"v.freightDetailList","FreightDetail");
                }
                if(selectedMenu === 'commodities'){
                    this.setSectionData(component,responseData,"v.commodityList","Commodity");
                }
                if(selectedMenu === 'requirements'){
                    this.setSectionData(component,responseData,"v.requirementList","Requirement");                                                           
                }
                if(selectedMenu === 'equipments'){
                    this.setSectionData(component,responseData,"v.equipmentList","Equipment");
                }
                component.set('v.currentContent', selectedMenu);
                component.set('v.selectedItem', selectedMenu);
                this.hideSpinner(component);
            } else if (state === "ERROR"){
                console.log("Error in controller");
            } else {
                console.log("Error Unknown !");
            }
        });
        $A.enqueueAction(action);
        
        
    },
    setRatingValues:function(component,event, helper, selectedMenu){
        this.showSpinner(component);
        var bookingId = component.get("v.recordId");
        var action = component.get("c.getRatingDetail");
        action.setParams({
            bookingId : bookingId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log('### ---> Stringify getRatingDetail Response');
                console.log(JSON.stringify(responseData));
                component.set('v.ratingList', responseData["chargeLineItems"]);
                component.set('v.selectedRouteId', parseInt(responseData["selectedRouteId"]));
                this.hideSpinner(component);
                component.set('v.currentContent', selectedMenu);
                component.set('v.selectedItem', selectedMenu);
            } else if (state === "ERROR"){
                console.log("Error in controller");
            } else {
                console.log("Error Unknown !");
            }
        });
        $A.enqueueAction(action);
    },
    setSectionData: function(component,responseData,attribute,sectionName){
        var sectionRecords=responseData[sectionName];
        var sectionList=[];
        for(var s in sectionRecords){
            var sectionFieldNameValue=[];
            var sectionRec=sectionRecords[s];
            for(var key in sectionRec){
                sectionFieldNameValue.push({label:key,value:sectionRec[key]});
            }
            sectionList.push(sectionFieldNameValue);
        }
        component.set(attribute,sectionList);
    },
    setBookingRelatedData: function(component,responseData,attribute,sectionName){
        
        var record=(responseData[sectionName])[0];
        var varFieldNameValue=[];
        for(var key in record)
            varFieldNameValue.push({label:key,value:record[key]});
        component.set(attribute,varFieldNameValue);
    } ,  
    showSpinner: function(component) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component){
        component.set("v.spinner", false);
    },
    
})