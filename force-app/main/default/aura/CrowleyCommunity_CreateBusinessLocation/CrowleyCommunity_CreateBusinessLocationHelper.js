({
    navigateToSendLink : function(component,event) {
        var isValid = this.checkValidations(component,event);
        if(isValid){
            var homeScreenEvent = component.getEvent("HomeScreenEvent");
            component.set("v.locationValidity",false);
            component.set("v.countryValidity",false);
            component.set("v.stateValidity",false);
            homeScreenEvent.setParams({
                "HomeScreenComponent": "c:CrowleyCommunity_SendLink",
                "businessLocationName" : component.get("v.businessLocationName"),
                "addressLine1" : component.get("v.addressLine1"),
                "addressLine2" : component.get("v.addressLine2"),
                "selectedCountry" : component.get("v.selectedCountry"),
                "selectedState" : component.get("v.selectedState"),
                "city" : component.get("v.city"),
                "zipCode" : component.get("v.zipCode"),
                "addNewLocation" : component.get("v.addNewLocation"),
                "selectedLocationId" : component.get("v.selectedLocationId"),
                "sendData" : true
            });
            homeScreenEvent.fire();
        }
    },
    checkValidations : function(component,event){
        component.set("v.locationValidity",true);
        component.set("v.countryValidity",true);
        component.set("v.stateValidity",true);
        var fieldValidity = component.find('FormVal').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        if(fieldValidity)
            return true;
        else
            return false;
    },
    setLocationDetails : function(component,event){
        var params = event.getParam('arguments');
        var action = component.get("c.fetchLocation");
        var contactId = params.param1;
        action.setParams({
            "contactId" : contactId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var addressRecord = response.getReturnValue();
                if(addressRecord != null){
                    component.set("v.selectedLocationId",addressRecord['Id'])
                    component.set("v.addressLine1",addressRecord['Name']);
                    component.set("v.addressLine2",addressRecord['Address_Line_2__c']);
                    component.set("v.searchCountry",addressRecord['Country__c']);
                    component.set("v.selectedCountry",addressRecord['Country__c']);
                    component.set("v.city",addressRecord['City__c']);
                    component.set("v.searchState",addressRecord['State_Picklist__c']);
                    component.set("v.selectedState",addressRecord['State_Picklist__c']);
                    component.set("v.zipCode",addressRecord['Postal_Code__c']);
                    component.set("v.addNewLocation",false);
                    component.set("v.showLocationFields",true);
                    component.set("v.isFieldReadonly",true);
                    component.set("v.showLookUp",false); 
                    component.set("v.statesVisible",true);
                }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    }
})