({
    doInit : function(component,event,helper){
      if(component.get('v.selectedCountry') != null){
        component.set("v.searchCountry",component.get('v.selectedCountry'));
      }
      if(component.get('v.selectedState') != null){
        component.set("v.searchState",component.get('v.selectedState'));
      }   
    },
    doAction : function(component,event,helper){
      if(component.get('v.newContact') == false){  
         helper.setLocationDetails(component,event);   
      }
    },
    navigateToSendLink : function(component, event, helper) {
        helper.navigateToSendLink(component,event);
    },
    showStateField : function(component,event,helper){
      var stateField = event.getParam("showStates");
      if(stateField == true){
         component.set("v.statesVisible",true);    
      }
      else{
         component.set("v.statesVisible",false);   
      }
    },
    backToAccountScreen : function(component,event){
        component.set("v.searchLocation","");
        component.set("v.selectedLocationId","");
        component.set("v.addressLine1","");
        component.set("v.addressLine2","");
        component.set("v.searchCountry","");
        component.set("v.city","");
        component.set("v.searchState","");
        component.set("v.zipCode","");
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_CreateAccount",  
            "sendData" : false
        });
        homeScreenEvent.fire(); 
    },
    makeLocationFieldsVisible : function(component,event){
        var fieldsVisible = event.getParam("makeLocationFieldsVisible");
        var fieldsReadOnly = event.getParam("makeLocationFieldReadOnly");
        var addressRecord = event.getParam("addressRecord");
        var addNewLocation = event.getParam("addNewLocation");
        
        if(addNewLocation != null || addNewLocation != undefined)
        component.set("v.addNewLocation",addNewLocation);
        
        if(fieldsVisible == true){
            component.set("v.showLocationFields",true);  
        }
        else{
            component.set("v.showLocationFields",false); 
        }         
        if(fieldsReadOnly == true){
            component.set("v.isFieldReadonly",true);
            if(addressRecord != null){
                component.set("v.statesVisible",true);
                component.set("v.selectedLocationId",addressRecord['Id']);
                component.set("v.addressLine1",addressRecord['Name']);
                component.set("v.addressLine2",addressRecord['Address_Line_2__c']);
                component.set("v.selectedCountry",addressRecord['Country__c']);
                component.set("v.searchCountry",addressRecord['Country__c'])
                component.get("v.selectedState",addressRecord['State_Picklist__c']),
                component.set("v.city",addressRecord['City__c']);
                component.set("v.searchState",addressRecord['State_Picklist__c']);
                component.set("v.zipCode",addressRecord['Postal_Code__c']);
            }
        }
        if(fieldsReadOnly == false){
           component.set("v.isFieldReadonly",false); 
            component.set("v.selectedLocationId","");
            component.set("v.addressLine1","");
            component.set("v.addressLine2","");
            component.set("v.searchCountry","");
            component.set("v.city","");
            component.set("v.searchState","");
            component.set("v.zipCode","");
        }
    },
    handleOnRender :  function(component,event,helper){
         var elem= component.find("getDeviceHeight").getElement();
        var winHeight = window.innerHeight;
        elem.style.height = winHeight + "px";
    }
})