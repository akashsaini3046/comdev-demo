({
    //Custom labels to be preloaded
    // $Label.c.CustomerCommunity_DashboardHeading
    // $Label.c.CustomerCommunity_FARHeading
    // $Label.c.CustomerCommunity_RequestAQuote
    // $Label.c.CustomerCommunity_Bookings
    // $Label.c.CustomerCommunity_MyDocuments
    // $Label.c.CustomerCommunity_STHeading
    // $Label.c.CustomerCommunity_VesselSchedule
    // $Label.c.CustomerCommunity_FAQs
    // $Label.c.CustomerCommunity_VehicleShipping
    // $Label.c.CustomerCommunity_Reports
    // $Label.c.CustomerCommunity_ContactUs
    // $Label.c.CustomerCommunity_Cases
    // $Label.c.CustomerCommunity_Contracts
    // $Label.c.CustomerCommunity_FindMyShipment
    // $Label.c.CustomerCommunity_VesselLocation
    // $Label.c.CrowleyCommunity_DashboardHeading
    // $Label.c.CrowleyCommunity_FARHeading
    // $Label.c.CrowleyCommunity_Bookings
    // $Label.c.CrowleyCommunity_MyDocuments
    // $Label.c.CrowleyCommunity_FAQs
    // $Label.c.CrowleyCommunity_ContactUs
    // $Label.c.CrowleyCommunity_FindMyShipment
    // $Label.c.CrowleyCommunity_VesselLocation
    // $Label.c.CrowleyCommunity_RequestAQuote
    // $Label.c.CrowleyCommunity_STHeading
    // $Label.c.CrowleyCommunity_VesselSchedule
    // $Label.c.CustomerCommunity_BillOfLading
    // $Label.c.CrowleyCommunity_Ideas
    // $Label.c.CustomerCommunity_Ideaa
    // $Label.c.CC_RouteFinder
    highlightListItems: function (component, event) {
        var menuItem = event.getParam("selectedMenu");
        var action = component.get("c.getMenuItems");
        action.setCallback(this, function (a) {
            var menuOptions = a.getReturnValue();
            var i = 0;
            var highlightSequence = 0;
            for (i = 0; i < menuOptions.length; i++) {
                if (menuOptions[i]['Lightning_Component_Name__c'] == menuItem) {
                    highlightSequence = i + 1;
                }
                menuOptions[i]['MasterLabel'] = $A.get('$Label.c.' + menuOptions[i]['MasterLabel']);
            }
            component.set("v.listitems", menuOptions);
            if (highlightSequence != 0) {
                component.set("v.highlightedItem", highlightSequence);
            }
            
            if (($A.get("$Browser.isTablet") || $A.get("$Browser.isPhone")) && $A.util.hasClass(document.getElementById("verticalMenuDiv"), "fliph")) {
                $A.util.removeClass(document.getElementById("verticalMenuDiv"), "fliph");
            } 
        });          
        $A.enqueueAction(action);
    },
    getCommunityName: function(component){
        var action =  component.get("c.getCommunityName");        
        action.setCallback(this, function (res){    
            var resData = res.getReturnValue();
        	component.set("v.communityName",resData);
        });
        
        $A.enqueueAction(action);
    }
})