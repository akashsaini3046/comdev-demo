({
    doInit : function(component, event, helper) {
        helper.getCommunityName(component);
        helper.highlightListItems(component, event);
        var pageReference = component.get("v.pageReference");
        var selectedTab= decodeURIComponent(window.location.search.substring(1));
        console.log('Selected tab --> '+selectedTab);
		component.set("v.selectedSequence", selectedTab);
    },
    changeHighlightSection : function(component, event, helper) {
        helper.highlightListItems(component, event);
    }
})