({

    doInit: function (component, event, helper) {
        var selectedCustomer = component.get("v.selectedCustomer");
        console.log(selectedCustomer);
        helper.customerAccount(component, event);
        
        helper.partyAssignment(component, event);
        console.log("selectedItemID", component.get("v.selectedItemID"));
        console.log(component.get("v.BookingWrapper").mapParty.shiper[0]);

    },
    handleComponentEventAccount: function (component, event, helper) {
        var accountId = event.getParam('selectedItemID');
        var cvifId = event.getParam('cvifId');
        var type = event.getParam('accountType');
        var index = event.getParam('index');
        if (type == 'Shipper') {
            type = 'shipper';
            if (typeof cvifId !== "undefined" && cvifId != '') {
                console.log("cvifId", cvifId);
                helper.getPartyRecords(component, event, cvifId, type, '');
            } else {
                component.set("v.shipperContactRecords", []);
                component.set("v.shipper", {});
            }
        }
        if (type == 'Consignee') {
            type = 'consignee';
            if (typeof cvifId !== "undefined" && cvifId != '') {
                helper.getPartyRecords(component, event, cvifId, type, '');
            } else {
                component.set("v.consigneeContactRecords", []);
                component.set("v.consignee", {});
            }
        }
        if (type == 'Notify') {
            if (typeof cvifId !== "undefined" && cvifId != '') {
                helper.getNotifyForwarderRecords(component, event, cvifId, 'notify', index);
            } else {
                var notifyMap = component.get("v.notifyMap");
                notifyMap[index] = [];
                component.set("v.notifyMap", notifyMap);
            }
        }
        if (type == 'Forwarder') {
            if (typeof cvifId !== "undefined" && cvifId != '') {
                helper.getNotifyForwarderRecords(component, event, cvifId, 'forwarder', index);
            } else {
                var forwarderMap = component.get("v.forwarderMap");
                forwarderMap[index] = [];
                component.set("v.forwarderMap", forwarderMap);
            }
        }
    },
    populateCustomer: function (component, event, helper) {
        var selectedId = component.find("customerId").get("v.value");
        helper.setCustomerData(component, event, selectedId);
    },
    populateShipper: function (component, event, helper) {
        var selectedId = component.find("shipperId").get("v.value");
        helper.populateShipperData(component, event, selectedId);
    },
    populateConsignee: function (component, event, helper) {
        var selectedId = component.find("consigneeId").get("v.value");
        helper.populateConsigneeData(component, event, selectedId);
    },

    populateNotify: function (component, event, helper) {

        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.populateNotifyData(component, event, selectedId, index);

    },
    populateForwarder: function (component, event, helper) {
        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.populateForwarderData(component, event, selectedId, index);
    },

    getNotifyRecords: function (component, event, helper) {
        var index = event.getSource().get("v.name");
        var cvifId = event.getSource().get("v.value");
        if (cvifId != '') {
            helper.getNotifyForwarderRecords(component, event, cvifId, 'notify', index);
        }
    },

    getforwarderRecords: function (component, event, helper) {
        var index = event.getSource().get("v.name");
        var cvifId = event.getSource().get("v.value");
        if (cvifId != '') {
            helper.getNotifyForwarderRecords(component, event, cvifId, 'forwarder', index);
        }
    },

    getPartyRecords: function (component, event, helper) {
        var type = event.getSource().get("v.name");
        var cvifId = event.getSource().get("v.value");
        console.log(type);
        if (type == 'shipper') {
            if (cvifId != '') {
                helper.getPartyRecords(component, event, cvifId, type, '');
            } else {
                component.set("v.shipperContactRecords", []);
                component.set("v.shipper", {});
            }
        }
        if (type == 'consignee') {
            if (cvifId != '') {
                helper.getPartyRecords(component, event, cvifId, type, '');
            } else {
                component.set("v.consigneeContactRecords", []);
                component.set("v.consignee", {});
            }
        }

    },
    next: function (component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "next"
        });
        bookingRecordIdEvent.fire();
    },
    completeLater: function (component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "completeLater"
        });
        bookingRecordIdEvent.fire();

    },
    addNotify: function (component, event, helper) {
        var bookingWrapper = component.get("v.BookingWrapper");
        bookingWrapper.mapParty.notify.push({ Type__c: "NOT" });
        component.set("v.BookingWrapper", bookingWrapper);
    },
    removeNotifier: function (component, event, helper) {
        //var indexvar = event.getSource().get("v.id");
        //console.log(Name);
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.id;
        console.log(index);
        var bookingWrapper = component.get("v.BookingWrapper");
        bookingWrapper.mapParty.notify.splice(index, 1);
        component.set("v.BookingWrapper", bookingWrapper);

    },
    addForwarder: function (component, event, helper) {
        var bookingWrapper = component.get("v.BookingWrapper");
        bookingWrapper.mapParty.forwarder.push({ Type__c: "FOR" });
        component.set("v.BookingWrapper", bookingWrapper);
    },
    removeForwarder: function (component, event, helper) {
        //var indexvar = event.getSource().get("v.id");
        //console.log(Name);
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.id;
        var bookingWrapper = component.get("v.BookingWrapper");
        bookingWrapper.mapParty.forwarder.splice(index, 1);
        component.set("v.BookingWrapper", bookingWrapper);

    },

})