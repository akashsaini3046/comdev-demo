({
	fetchBookingData : function(component, event, helper) {
		var action = component.get("c.getBookingDetail");
        if(typeof component.get("v.recordId") !== "undefined" && component.get("v.recordId") !== null){
            var bookingId = component.get("v.recordId");
        }else{
            var bookingId = component.get("v.bookingId");
        }
        action.setParams({
            bookingId : bookingId
        });
        console.log(bookingId);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log(JSON.stringify(responseData));
                
                this.setParties(component, responseData);
                this.setGeneralBookingInfo(component, responseData);
                this.setPickupInfo(component, responseData);
                this.setReferenceNumber(component, responseData);
                this.setRatings(component, responseData);
                this.setDeliveryInfo(component, responseData);
                this.setSegmentInfo(component, responseData);
                this.setEquipments(component,responseData);
                this.setDocuments(component, responseData);
                this.setBOLDocuments(component, responseData);
                this.setCommodities(component,responseData);
                
            } else if (state === "ERROR"){
                console.log("Error in controller");
            } else {
                console.log("Error Unknown !");
            }
        });
        $A.enqueueAction(action);
    },
    setEquipments : function(component,responseData){
       
        var equipments = [];
        if(typeof responseData["shipmentRecords"] !== "undefined" && responseData["shipmentRecords"] !== null){
            if(typeof responseData["shipmentRecords"][0]["freightDetailRecords"] !== "undefined" && responseData["shipmentRecords"][0]["freightDetailRecords"] !== null)
                if(typeof responseData["shipmentRecords"][0]["freightDetailRecords"][0]["requirementRecords"][0]["equipementRecords"] !== "undefined" && responseData["shipmentRecords"][0]["freightDetailRecords"][0]["requirementRecords"][0]["equipementRecords"] !== null)
                    var equip = responseData["shipmentRecords"][0]["freightDetailRecords"][0]["requirementRecords"][0]["equipementRecords"];
            for(var index = 0; index < equip.length ; index++){
                var equipment = {};
                equipment.Name = equip[index].Name;
                equipment.Type__c=equip[index].Type__c;
                equipment.VGM_Weight__c=equip[index].VGM_Weight__c;
                equipment.Unit_of_Measure_for_VGM__c=equip[index].Unit_of_Measure_for_VGM__c;
               //equipment.Requirement__r. = equip[index].Requirement__c;
                equipments.push(equipment); 
            }
            component.set("v.equipments",equipments);
        }
    },
    setCommodities : function(component,responseData){
       
        var commodities = [];
        if(typeof responseData["shipmentRecords"] !== "undefined" && responseData["shipmentRecords"] !== null){
            if(typeof responseData["shipmentRecords"][0]["freightDetailRecords"] !== "undefined" && responseData["shipmentRecords"][0]["freightDetailRecords"] !== null){
                if(typeof responseData["shipmentRecords"][0]["freightDetailRecords"][0]["freightDetail"] !== "undefined" && responseData["shipmentRecords"][0]["freightDetailRecords"][0]["freightDetail"] !== null){
                   var comm = responseData["shipmentRecords"][0]["freightDetailRecords"][0]["freightDetail"]["Commodities__r"];
                    for(var index = 0; index < comm.length ; index++){
                        var commodity = {};
                        commodity.Name = comm[index].Name;
                        commodity.IMO_Class__c = comm[index].IMO_Class__c;
                        commodity.Package_Group__c = comm[index].Package_Group__c;
                        commodity.Quantity_value__c = comm[index].Quantity_value__c;
                        commodity.Quantity_Unit_of_Measure__c = comm[index].Quantity_Unit_of_Measure__c;
                        commodity.Weight_value__c = comm[index].Weight_value__c;
                        commodity.Weight_Unit_of_Measure__c = comm[index].Weight_Unit_of_Measure__c; 
                        commodity.Volume_value__c = comm[index].Volume_value__c;
                        commodity.Volume_Unit_of_Measure__c = comm[index].Volume_Unit_of_Measure__c;
                        commodity.Emergency_Contact_Name__c = comm[index].Emergency_Contact_Name__c;
                        commodity.Emergency_Contact_Number__c = comm[index].Emergency_Contact_Number__c;
                        commodities.push(commodity);
                    } 
                }
            }
        }
        component.set("v.commodities",commodities);
    },
    setPickupInfo : function(component, responseData){
        var pickupInfo = {};
        if(typeof responseData["transportRecords"] !== "undefined" && responseData["transportRecords"] !== null){
            if(typeof responseData["transportRecords"][0]["Stops__r"] !== "undefined" && responseData["transportRecords"][0]["Stops__r"] != null){
                var stops = responseData["transportRecords"][0]["Stops__r"][0];
                pickupInfo.StopName = stops["Name"];
                pickupInfo.Name = stops["Contact_Name__c"];
                pickupInfo.PhoneNumber = stops["Phone_Number__c"];
                pickupInfo.PickupTime = stops["Pick_up_Time_From__c"] + " To " + stops["Pick_up_TimeTo__c"];
                pickupInfo.PickupDate = stops["Pick_up_Date__c"];
                component.set("v.pickupInfo", pickupInfo);
            }
        }
    },
    
    setDeliveryInfo : function(component, responseData){
        var deliveryInfo = {};
        if(typeof responseData["transportRecords"] !== "undefined" && responseData["transportRecords"] !== null){
            if(typeof responseData["transportRecords"][0]["Stops__r"] !== "undefined" && responseData["transportRecords"][0]["Stops__r"] != null){
                var stops = responseData["transportRecords"][0]["Stops__r"][0];
                deliveryInfo.Address = stops["Address__c"] + ", " + stops["City__c"] + ", " + stops["State__c"] + ", " + stops["Country__c"] + ", " + stops["Zip_Code_Postal_Code__c"];
                deliveryInfo.DropTime = stops["Drop_Time_From__c"] + " To " + stops["Drop_Time_To__c"];
                deliveryInfo.DropDate = stops["Drop_Date__c"];
                component.set("v.deliveryInfo", deliveryInfo);
            }
        }
    },
    
    setSegmentInfo : function(component, responseData){
        var segmentInfo = [];
        if(typeof responseData["shipmentRecords"] !== "undefined" && typeof responseData["shipmentRecords"][0] !== null){
            if(typeof responseData["shipmentRecords"][0]["shipment"]["Voyages__r"] !== "undefined" ){
                var voyageList = responseData["shipmentRecords"][0]["shipment"]["Voyages__r"];
                for(var i = 0; i < voyageList.length ; i++){
                    var segment = {};
                    segment.seqId = voyageList[i].Loading_Sequence__c;
                    segment.Port = voyageList[i].Loading_Port__c;
                    segment.DischargePort = voyageList[i].Discharge_Port__c;
                    segment.VoyageNumber = voyageList[i].Loading_Port__c;
                    segment.VesselName = voyageList[i].Vessel_Name__c;
                    segment.eSailingDate = voyageList[i].Estimate_Sail_Date__c;
                    segment.eArrivalDate = voyageList[i].Estimate_Arrival_Date__c;
                    segment.isLoading = false;
                    if(i === 0){
                        segment.isLoading = true;
                    }
                    segmentInfo.push(segment);
                }
                component.set("v.segmentInfo", segmentInfo);
            }
        }
    },
    
    setRatings : function(component, responseData){
        var ratings = [];
        var rating = {};
        var ratingTotal = {};
        ratingTotal.ToBePrepaid = 0.00;
        ratingTotal.ToBeCollected = 0.00;
        for(var i = 0 ; i < 5 ; i++ ){
            rating.FreightChanges = 'Ocean Freight';
            rating.RatedAs = '1.00';
            rating.Per = 'PTT';
            rating.Rate = 55.00;
            rating.ToBePrepaid = 55.00;
            rating.ToBeCollected = null;
            rating.ForeignCurrency = null;
            ratings.push(rating);
            ratingTotal.ToBePrepaid += (rating.ToBePrepaid != null ? rating.ToBePrepaid : 0.00);
            ratingTotal.ToBeCollected += (rating.ToBeCollected != null ? rating.ToBeCollected : 0.00);
        }
        component.set('v.ratings', ratings);
        component.set('v.ratingsTotal', ratingTotal);
    },
    
    setReferenceNumber : function(component, responseData){
        var referenceNumber = {};
        referenceNumber.CustomerOriginRN = null;
        referenceNumber.CustomerDestRN = null;
        referenceNumber.FreightRN = null;
        referenceNumber.StopRN = null;
        referenceNumber.ForwarderRN = null;
        referenceNumber.shipperRN = null;
        referenceNumber.NotifierRN = null;
        referenceNumber.ConsigneeRN = null;
        if(typeof responseData["bookingRecord"]["Parties__r"] !== "undefined"){
            var parties = responseData["bookingRecord"]["Parties__r"];
            for(var i = 0; i < parties.length; i++){
                if(parties[i]["Type__c"] === "FOR" && typeof parties[i]["REF_Number__c"] !== "undefined"){
                    referenceNumber.ForwarderRN = parties[i]["REF_Number__c"];
                }
                if(parties[i]["Type__c"] === "SHP" && typeof parties[i]["REF_Number__c"] !== "undefined"){
                    referenceNumber.shipperRN = parties[i]["REF_Number__c"];
                }
                if(parties[i]["Type__c"] === "NOT" && typeof parties[i]["REF_Number__c"] !== "undefined"){
                    referenceNumber.NotifierRN = parties[i]["REF_Number__c"];
                }
                if(parties[i]["Type__c"] === "CON" && typeof parties[i]["REF_Number__c"] !== "undefined"){
                    referenceNumber.ConsigneeRN = parties[i]["REF_Number__c"];
                }
            }
        }
        component.set("v.referenceNumbers", referenceNumber);
    },
    
    setBOLDocuments : function(component, responseData){
        var documents = [];
        for(var i = 1 ; i <= 4; i++){
            var document = {};
            document.Name = 'Bol Document '+ i;
            document.DocumentId = '0690t000001F97AAAS';
            if(i < 3){
                document.isPDF = true;
            }else{
                document.isPDF = false;
            }
            documents.push(document);
        }
        component.set("v.bolDocuments", documents);
    },
    
    setDocuments : function(component, responseData){
        var documents = [];
        /*if(typeof responseData["documentsWrapper"] !== "undefined"){
            for(var doc in responseData["documentsWrapper"]){
                document.Name = doc.documentName;
                document.DocumentId = doc.documentId;
                document.isPDF = false;
                if(doc.documentId.includes('PDF')){
                    document.isPDF = true;
                }
                documents.push(document);
            }
        }*/
        
        for(var i = 1 ; i <= 5; i++){
            var document = {};
            document.Name = 'Document '+ i;
            document.DocumentId = '0690t000001F97AAAS';
            document.isPDF = true;
            documents.push(document);
        }
        component.set("v.documents", documents);
    },
    
    setGeneralBookingInfo : function(component, responseData){
        var generalBookingInfo = {};
        var bookingRecord = responseData["bookingRecord"];
        var shipment = responseData["bookingRecord"]["Shipments__r"];
        var parties = responseData["bookingRecord"]["Parties__r"];
        for (var i = 0; i < parties.length; i++) {
            if(parties[i].Type__c === 'SHP') {
                generalBookingInfo.Shipper = parties[i].Name;
            } else if(parties[i].Type__c === 'CON') {
                generalBookingInfo.Consignee = parties[i].Name;
            }
        }
        generalBookingInfo.BookingNumber = bookingRecord.Booking_Number__c;
        generalBookingInfo.Status = bookingRecord.Status__c;
        generalBookingInfo.Origin = shipment[0].Origin_Code__c;
        generalBookingInfo.Destination = shipment[0].Destination_Code__c;
        generalBookingInfo.BookingDate = bookingRecord.Booked_Date__c;
        component.set("v.generalBookingInfo", generalBookingInfo);
    },
    
    setParties : function(component, responseData){
        var parties = responseData["bookingRecord"]["Parties__r"];
        component.set("v.parties", parties);
    },
})