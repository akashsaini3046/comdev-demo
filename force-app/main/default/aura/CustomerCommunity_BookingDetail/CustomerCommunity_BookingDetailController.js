({
    doInit: function (component, event, helper) {
        component.set('v.selectedItem', 'routing');
        component.set('v.currentContent', 'routing');
        helper.fetchBookingData(component, event, helper);
    },
    
    handleSelect: function(component, event, helper) {
        var selected = event.getParam('name');
        component.set('v.currentContent', selected);
        component.set('v.selectedItem', selected);
    },
    
    toggleSection : function(component, event, helper) {
        var sectionAuraId = event.target.getAttribute("data-auraId");
        var hasClassCollapse = $A.util.hasClass(component.find(sectionAuraId), "slds-is-open");
        if(!hasClassCollapse){
            $A.util.addClass(component.find(sectionAuraId), 'slds-section slds-is-open');
            $A.util.removeClass(component.find(sectionAuraId), 'slds-is-close');
        }else{
            $A.util.addClass(component.find(sectionAuraId), 'slds-section slds-is-close');
            $A.util.removeClass(component.find(sectionAuraId), 'slds-is-open');
        }
    },
    
    viewDocument: function (component, event, helper) {
        var indexId = event.currentTarget.getAttribute("data-Id");
        component.set("v.contentId", indexId);
        component.set("v.hasModalOpen", true);
    },
    
    closeModel: function (component, event, helper) {
        component.set("v.hasModalOpen", false);
        component.set("v.contentId", null);
    },
    closeCommodityModalBox : function(component,event,helper){
        component.set("v.isCommodityModelOpen", false); 
    },
    openCommodityModalBox : function(component,event,helper){
        component.set("v.isCommodityModelOpen", true); 
    }
})