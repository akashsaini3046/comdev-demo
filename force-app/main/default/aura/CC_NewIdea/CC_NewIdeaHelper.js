({
    getZonesList : function(cmp, helper){
        var action = cmp.get("c.getZonesList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getZonesList', response.getReturnValue());
                cmp.set("v.zonesOptions", JSON.parse(response.getReturnValue()) );
                // Let DOM state catch up.
                window.setTimeout(
                    $A.getCallback( function() {
                        // Now set our preferred value
                        if(cmp.get("v.ideaRecord")){
                            cmp.find("zones").set("v.value", cmp.get("v.ideaRecord").CommunityId);
                        }
                    }));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    saveIdea: function (cmp, event, helper) {
        console.log('ideaRecord -> ', cmp.get("v.ideaRecord"));
        var action = cmp.get("c.saveIdeaRecord");
        var param = { ideaRecord: cmp.get("v.ideaRecord") };
        console.log(param);
        action.setParams(param);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result -> ',result);
                if(result.Id){
                    cmp.set("v.ideaId", result.Id);
                    
                    // calling method of the child component "fileUploadCmp"
                    // for saving an uploaded file on Idea
                    
                    
                    var childComponent = cmp.find("fileUploadCmp");
                    var message = childComponent.saveFile(cmp.get("v.ideaId"));
                   
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'sticky',
            "type": type,
            "title": title,
            "message": message
        });
        toastEvent.fire();
    },
    
    findSimilarIdeasAction : function(cmp, helper, communityId, title){
        var action = cmp.get("c.findSimilarIdeas");
        var param = { communityId : communityId, title : title };
        console.log('findSimilarIdeas param -> ', param);
        action.setParams(param);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('similar ideas -> ', response.getReturnValue());
                cmp.set("v.similarIdeas", response.getReturnValue());
                helper.showToast("Success", "The record has been updated successfully");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    getIdeaFieldDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaFieldDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('getIdeaFieldDescribe -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaFieldDescribe", JSON.parse(response.getReturnValue()));
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaDescribeResult -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaDescribe", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
     getIdeaDetails : function(cmp, helper){
         console.log('Inside getIdeaDetails ')
        var action = cmp.get("c.getIdeaDetails");
        action.setParams({ideaId: cmp.get("v.ideaId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('ideaRecord -> ',response.getReturnValue());
                cmp.set("v.ideaRecord", response.getReturnValue());
                cmp.set("v.displayNewIdea",false);
                cmp.set("v.displayIdeaDetail",true);
                cmp.set("v.displayIdeasList",false);           
                cmp.set("v.displayEditIdea",false);
                       
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
   
    getRelevantPickListValues: function(cmp,helper,communityZoneId){
        console.log('getRelevantPickListValues  '+communityZoneId);
        var action=cmp.get("c.fetchRecordTypeSpecificPickListvalues");
        action.setParams({
            zoneId : communityZoneId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PicklistValues -> ',response.getReturnValue());
                var statusData=response.getReturnValue()['Status'];
                var categoriesData=response.getReturnValue()['Categories'];
                var benefitsData=response.getReturnValue()['Benefits'];
                var status=[];
                var categories=[];
                var benefits=[];
                console.log('statusData '+JSON.stringify(statusData));
                console.log('categoriesData '+JSON.stringify(categoriesData));
                for(var key in statusData)
                    status.push({label:key,value:statusData[key]});
                for(var key in categoriesData)
                    categories.push({label:key,value:categoriesData[key]});
                for(var key in benefitsData)
                    benefits.push({label:key,value:benefitsData[key]});
                cmp.set("v.benefits",benefits);
                cmp.set("v.ideaStatuses",status);
                cmp.set("v.categoriesOptions",categories);                
            }
            else {
                console.log(" getRelevantPickListValues failed with state: " + state);
            }
        });
        $A.enqueueAction(action);       
    },
    getCommunityNetworkName : function(cmp){
        console.log('getCommunityNetworkName');
        var action = cmp.get("c.getCommunityNetworkName");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state : '+state);
            if(state === "SUCCESS"){
                cmp.set("v.communityNetworkName",response.getReturnValue());
            }
            else{
                 console.log(" getCommunityNetworkName failed with state: " + state);
            }
        });
      $A.enqueueAction(action);                                               
    }
})