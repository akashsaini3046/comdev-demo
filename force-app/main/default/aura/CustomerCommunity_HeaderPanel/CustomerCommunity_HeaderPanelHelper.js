({
    fetchUserDetails: function (component, event) {
        var action = component.get("c.getUserName");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var userDetail = JSON.parse(response.getReturnValue());
                component.set("v.UserName", userDetail.userName);
                component.set("v.Language", userDetail.language);
                component.set("v.UserId", userDetail.UserId);
            }
        });
        $A.enqueueAction(action);
    },
    goToDashboardHelper: function (component, event) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/",
        });
        urlEvent.fire();
    },
    userOptionSelect: function (component, event) {
        var selectedMenuItemValue = event.getParam("value");
        var MenuItem = component.get("v.MenuItemOne");
        if (selectedMenuItemValue === MenuItem) {
            window.location.replace("/CrowleyCustomerCommunity/secur/logout.jsp");
        }
        if (selectedMenuItemValue === "My Settings") {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/settings/" + component.get("v.UserId"),
            });
            urlEvent.fire();
        }
        if (selectedMenuItemValue === "My Profile") {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/profile/" + component.get("v.UserId"),
            });
            urlEvent.fire();
        }
        if(selectedMenuItemValue === "My Preferences"){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/preferences"
            });
            urlEvent.fire();
        }
    },
    handleExpandCollapse: function (component, event) {
        var isCollapsed = $A.util.hasClass(document.getElementById("verticalMenuDiv"), "fliph");
        if (!isCollapsed) {
            $A.util.addClass(document.getElementById("verticalMenuDiv"), "fliph");
            $A.util.addClass(document.getElementById("DashboardContentDiv"), "dasboard-content-lg");
        } else {
            $A.util.removeClass(document.getElementById("verticalMenuDiv"), "fliph");
            if ($A.util.hasClass(document.getElementById("DashboardContentDiv"), "dasboard-content-lg")) {
                $A.util.removeClass(document.getElementById("DashboardContentDiv"), "dasboard-content-lg");
            }
        }
    }
})