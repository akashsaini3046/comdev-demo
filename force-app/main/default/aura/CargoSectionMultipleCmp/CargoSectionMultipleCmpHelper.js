({
    addCargo: function (component, event, helper) {
        var action = component.get("c.addNewCargo");
        action.setParams({
            "listCargoWrapper": component.get("v.BookingWrapper")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cargo = response.getReturnValue();
                console.log(cargo);
                //component.set("v.BookingWrapperObj",cargo);
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);

    },
   
    checkCargoType : function(component){
        var iscontainer = component.find("containerbtn").get("v.checked");
        var isroro = component.find("rorobtn").get("v.checked");
        var isbreakbulk = component.find("breakbulkbtn").get("v.checked");
        if(!iscontainer && !isroro && !isbreakbulk){
            alert('Please select atleast one cargo for this booking');
            return false;        
        }
        return true;
    },

    selectTab : function(component, currentTab, bookingWrapper){
        if(currentTab=="1"){            
            if(bookingWrapper.shipmentMap.BREAKBULK.isSelected){
                component.find("tabs").set("v.selectedTabId", "3");
            }
            if(bookingWrapper.shipmentMap.RORO.isSelected){
                component.find("tabs").set("v.selectedTabId", "2");
            }          
        }
        if(currentTab=="2"){
            if(bookingWrapper.shipmentMap.CONTAINER.isSelected){
                component.find("tabs").set("v.selectedTabId", "1");
            }
            if(bookingWrapper.shipmentMap.BREAKBULK.isSelected){
                component.find("tabs").set("v.selectedTabId", "3");
            }
        }
        if(currentTab=="3"){            
            if(bookingWrapper.shipmentMap.RORO.isSelected){
                component.find("tabs").set("v.selectedTabId", "2");
            }
            if(bookingWrapper.shipmentMap.CONTAINER.isSelected){
                component.find("tabs").set("v.selectedTabId", "1");
            }
        }
    },

    editComodity: function (component, event) {

    },
    deleteComodity: function (component, event) {

    },    
    fetchRates: function (component, event, helper) {
        /*var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "next"
        });
        bookingRecordIdEvent.fire();
        component.set("v.showSpinner", true);*/
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Fetching Rates",
            "message": "Fetching Freight Rates !",
            "type": "info"
        });
        toastEvent.fire();
       
        var action = component.get("c.createSoftshipRateAPIWRequest");
        action.setParams({
            "bookingWrapper": component.get("v.BookingWrapper")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.rateMap",storeResponse);
                component.set("v.RatingApiResponseWrapper", storeResponse);
                var bookingRecordIdEvent = component.getEvent("bookingEventData");
                bookingRecordIdEvent.setParams({
                    "selectedAction": "next"
                });
                bookingRecordIdEvent.fire();
               /* if (storeResponse) {
                    component.set("v.RatingApiResponseWrapper", storeResponse);
                    var bookingRecordIdEvent = component.getEvent("bookingEventData");
                    bookingRecordIdEvent.setParams({
                        "selectedAction": "next"
                    });
                    bookingRecordIdEvent.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error fetching Rates!",
                        "message": "Please try after sometime !",
                        "type": "error"
                    });
                    toastEvent.fire();
                }*/
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error fetching Rates!",
                    "message": "Please try after sometime !",
                    "type": "error"
                });
                toastEvent.fire();
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

})