({
    addCargo: function (component, event, helper) {
        var action = component.get("c.addNewCargo");
        action.setParams({
            "listCargoWrapper": component.get("v.BookingWrapper")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cargo = response.getReturnValue();
                console.log(cargo);
                //component.set("v.BookingWrapperObj",cargo);
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);

    },
    addCargoToContainer: function (component, event, helper) {
        var containerType = component.get("v.containerType");
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        objcargo["cargoType"] = containerType;
        objcargo["listFreightDetailWrapper"] = [{ commodityDesc: '', freightDetail: {}, listCommodityWrapper: [], listRequirementWrapper: [{ requirement: {}, commodityDesc: '' }] }];

        component.set("v.BookingWrapperObj", objcargo);
        console.log(component.get("v.BookingWrapperObj"));
    },

    editCargoToContainer: function (component, event, helper) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        console.log(BookingWrapperObj);
        var bookingWrapper = component.get("v.BookingWrapper");        
        component.set("v.IsHazardous", bookingWrapper.booking.Is_Hazardous__c);

        component.set("v.containerType", BookingWrapperObj[0].cargoType);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    addItem: function (component, event, helper) {
        var containerType = component.get("v.containerType");
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        if (containerType == 'container') {
            BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper.push({ requirement: {}, commodityDesc: '' });
        } else {
            BookingWrapperObj[0].listFreightDetailWrapper.push({ freightDetail: {}, commodityDesc: '' });
        }
        console.log(BookingWrapperObj);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    removeItem: function (component, event, helper) {
        var containerType = component.get("v.containerType");
        var index = event.target.id;
        console.log(index);
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        if (containerType == 'container') {
            BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper.splice(index, 1);
        } else {
            BookingWrapperObj[0].listFreightDetailWrapper.splice(index, 1);
        }
        console.log(BookingWrapperObj);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    editComodity: function (component, event) {

    },
    deleteComodity: function (component, event) {

    },
    fetchContainerTypeRecords: function (component) {
        var action = component.get("c.fetchContainerList");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.ContainerList", responseData);
            }
        });
        $A.enqueueAction(action);
    },
    searchUNSubstances: function (component, event) {
        console.log("searchSubsString", component.get("v.searchSubsString"));
        var code = component.get("v.searchSubsString");
        var action = component.get("c.getSubstancesRecord");
        action.setParams({
            searchKey: code
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", 'Search Result...');
                }
                component.set("v.substanceRecords", storeResponse);
            }
            component.set("v.imdgSpinner", false);
        });
        $A.enqueueAction(action);
    },
    columnSubstances: function (component) {
        component.set('v.substanceColumns', [
            { label: 'UN Code', fieldName: 'Name', type: 'text' },
            { label: 'Name', fieldName: 'Substance_Name__c', type: 'text' },
            { label: 'Primary Class', fieldName: 'Primary_Class__c', type: 'text' },
            { label: 'Variation', fieldName: 'Variation__c', type: 'text' },
            { label: 'Packaging Group', fieldName: 'Packing_Group__c', type: 'text' },
            { label: 'Secondary Class', fieldName: 'Secondary_Class__c', type: 'text' },
            { label: 'Marine Pollutant', fieldName: 'Marine_Pollutant__c', type: 'text' }
        ]);
    },
    fetchRates: function (component, event, helper) {
        /*var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "next"
        });
        bookingRecordIdEvent.fire();
        component.set("v.showSpinner", true);*/
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Fetching Rates",
            "message": "Fetching Freight Rates !",
            "type": "info"
        });
        toastEvent.fire();
        var action = component.get("c.fetchFclRates");
        console.log(component.get("v.BookingWrapper"));
        var bookingWrap = component.get("v.BookingWrapper");
        action.setParams({
            "bookingWrapperData": JSON.stringify(bookingWrap)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse) {
                    component.set("v.RatingApiResponseWrapper", storeResponse);
                    var bookingRecordIdEvent = component.getEvent("bookingEventData");
                    bookingRecordIdEvent.setParams({
                        "selectedAction": "next"
                    });
                    bookingRecordIdEvent.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error fetching Rates!",
                        "message": "Please try after sometime !",
                        "type": "error"
                    });
                    toastEvent.fire();
                }
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error fetching Rates!",
                    "message": "Please try after sometime !",
                    "type": "error"
                });
                toastEvent.fire();
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    getVehiclesData: function (component, event, helper) {
        let manufacture = new Set()
        var action = component.get("c.getVehicles");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var vehicles = response.getReturnValue();
                console.log(vehicles);
                component.set("v.listVehicles",vehicles);
                for (var i = 0; i < vehicles.length; i++) {
                    manufacture.add(vehicles[i].Name);
                 }
                 const manufactureVal= manufacture.values();
                 var listManufacture= component.get("v.listManufacture");
                 for (var i = 0; i < manufacture.size; i++) {
                    listManufacture.push((manufactureVal.next().value));
                 }
                component.set("v.listManufacture",listManufacture);
                
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },

    getModel: function (component, event, selectedId, index) {
        var listVehicles = component.get("v.listVehicles");
        var model = [];
        for (var i = 0; i < listVehicles.length; i++) {
            if(listVehicles[i].Name===selectedId){
                model.push(listVehicles[i]);
            }            
        }
        console.log(model);
        var modelData = component.get("v.modelData");
        modelData[index] = model;
        component.set("v.modelData", modelData);
    },
    populateVehicleDetail: function (component, event, selectedId, index) {
        var modelData =  component.get("v.modelData")[index];
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var freightDetail = BookingWrapperObj[0].listFreightDetailWrapper[index].freightDetail;
        for (var i = 0; i < modelData.length; i++) {
            if(modelData[i].Model__c.toString()==selectedId){
                freightDetail.Length_Major__c =  modelData[i].Length__c;
                freightDetail.Width_Major__c = modelData[i].Width__c;
                freightDetail.Height_Major__c = modelData[i].Height__c;
                freightDetail.Declared_Weight_Value__c = modelData[i].Weight__c;
                freightDetail.Type__c = modelData[i].Type__c;
                break;
            }   
            
        }
        component.set("v.BookingWrapperObj",BookingWrapperObj);
    },

    populateContainerData: function (component, event, selectedId, index) {
        var modelData =  component.get("v.ContainerList");
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var RequirementWrapper = BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper[index];
        for (var i = 0; i < modelData.length; i++) {
            if(modelData[i].Name.toString()==selectedId){
                console.log(modelData[i]);
                if(modelData[i].Reefer__c=='N'){
                    RequirementWrapper.requirement.Category__c ='DRY';
                }else{
                    RequirementWrapper.requirement.Category__c ='REEFER';
                }
                RequirementWrapper.requirement.Length__c =modelData[i].Size__c;
                RequirementWrapper.requirement.Container_Type__c = modelData[i].CICS_ISO_Code__c;
                RequirementWrapper.containerDesc = modelData[i].Description__c;
                //RequirementWrapper.containerDesc = modelData[i].Description__c;
                RequirementWrapper.requirement.Tare_Weight__c = modelData[i].Tare__c;
                break;
            }   
            
        }
        component.set("v.BookingWrapperObj",BookingWrapperObj);
    },

})