({
	fetchUserDetails : function(component) {
		var action = component.get("c.getUserData");
        action.setCallback(this, function(response){
            var state = response.getState();
			if(response.getReturnValue()!=null){
				var conDetail = response.getReturnValue();
                console.log(conDetail);
                component.set("v.CustomerName",conDetail.Name);
                component.set("v.CustomerTitle",conDetail.Title);
                component.set("v.AccountName",conDetail.Account.Name); 
                component.set("v.CustomerCountry",conDetail.MailingCountry);
                
                if(conDetail.Account_Type_Level__c > 1) {
                    var appEvent = $A.get("e.c:CustomerCommunity_VisibilityLevel");
                    appEvent.setParams({"Visibility" : true });
                    appEvent.fire();
                }
			}
            
        });
        $A.enqueueAction(action);
	}
})