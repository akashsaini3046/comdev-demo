({
	getBookingRecords : function(component) {
		var action = component.get("c.fetchBookingRecords");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.bookingRecords", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
	}
})