({
    addCargoToContainer: function (component, event) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        objcargo["cargoType"] = 'container';
        objcargo["listFreightDetailWrapper"] = [{ commodityDesc: '', freightDetail: {}, listCommodityWrapper: [], listRequirementWrapper: [{ requirement: {}, commodityDesc: '' }] }];
        component.set("v.BookingWrapperObj", objcargo);
        console.log(component.get("v.BookingWrapperObj"));
    },

    editCargoToContainer: function (component, event) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        BookingWrapperObj[0].cargoType = 'container';
        console.log(BookingWrapperObj);
        if (BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper.length > 0) {
            component.set("v.IsHazardous", true);
        }
        component.set("v.containerType", BookingWrapperObj[0].cargoType);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },

    addItem: function (component, event, helper) {

        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper.push({ requirement: {}, commodityDesc: '' });
        console.log(BookingWrapperObj);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    removeItem: function (component, event, helper) {
        var index = event.target.id;
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper.splice(index, 1);
        console.log(BookingWrapperObj);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },

    fetchContainerTypeRecords: function (component) {
        var action = component.get("c.fetchContainerList");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.ContainerList", responseData);
            }
        });
        $A.enqueueAction(action);
    },
    searchUNSubstances: function (component, event) {
        console.log("searchSubsString", component.get("v.searchSubsString"));
        var code = component.get("v.searchSubsString");
        var action = component.get("c.getSubstancesRecord");
        action.setParams({
            searchKey: code
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", 'Search Result...');
                }
                component.set("v.substanceRecords", storeResponse);
            }
            component.set("v.imdgSpinner", false);
        });
        $A.enqueueAction(action);
    },
    columnSubstances: function (component) {
        component.set('v.substanceColumns', [
            { label: 'UN Code', fieldName: 'Name', type: 'text' },
            { label: 'Name', fieldName: 'Substance_Name__c', type: 'text' },
            { label: 'Primary Class', fieldName: 'Primary_Class__c', type: 'text' },
            { label: 'Variation', fieldName: 'Variation__c', type: 'text' },
            { label: 'Packaging Group', fieldName: 'Packing_Group__c', type: 'text' },
            { label: 'Secondary Class', fieldName: 'Secondary_Class__c', type: 'text' },
            { label: 'Marine Pollutant', fieldName: 'Marine_Pollutant__c', type: 'text' }
        ]);
    },
    populateContainerData: function (component, event, selectedId, index) {
        var modelData =  component.get("v.ContainerList");
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var RequirementWrapper = BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper[index];
        for (var i = 0; i < modelData.length; i++) {
            if(modelData[i].Name.toString()==selectedId){
                console.log(modelData[i]);
                if(modelData[i].Reefer__c=='N'){
                    RequirementWrapper.requirement.Category__c ='DRY';
                }else{
                    RequirementWrapper.requirement.Category__c ='REEFER';
                }
                RequirementWrapper.requirement.Length__c =modelData[i].Size__c;
                RequirementWrapper.requirement.Container_Type__c = modelData[i].CICS_ISO_Code__c;
                RequirementWrapper.containerDesc = modelData[i].Description__c;
                //RequirementWrapper.containerDesc = modelData[i].Description__c;
                RequirementWrapper.requirement.Tare_Weight__c = modelData[i].Tare__c;
                break;
            }   
            
        }
        component.set("v.BookingWrapperObj",BookingWrapperObj);
    },
})