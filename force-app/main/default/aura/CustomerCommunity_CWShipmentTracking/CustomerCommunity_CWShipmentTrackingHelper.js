({
	fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	}
})