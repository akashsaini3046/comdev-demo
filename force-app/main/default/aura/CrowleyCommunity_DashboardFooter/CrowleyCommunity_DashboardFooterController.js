({
    navigateToFindRoute : function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {
            type: "comm__namedPage",
            attributes: {
                name: "findroute__c"
            }
        };
        navService.navigate(pageReference);
	},
    
    navigateToHome : function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {
            type: "comm__namedPage",
            attributes: {
                name: "Home"
            }
        };
        navService.navigate(pageReference);
	}
})