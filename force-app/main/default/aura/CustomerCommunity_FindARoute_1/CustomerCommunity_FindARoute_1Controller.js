({
	doInit : function(component, event, helper) {
        helper.fireHighlightEvent(component, event);
        helper.fetchOriginPicklist(component);
        helper.fetchDestinationPicklist(component);
        helper.fetchSailingWeeksPicklist(component);
	},
    
    findRoutes : function(component, event,helper) {
        var shipmentTypeValue = component.get("v.shipmentType");
        var originValue = component.find("originId").get("v.value");
        var destinationValue = component.find("destinationId").get("v.value");
        var sailingWeeksValue = component.find("sailingWeeksId").get("v.value");
        var action = component.get("c.getRoutes");
        
        action.setParams({ originPort :originValue,destinationPort:destinationValue,
                          shipmentType:shipmentTypeValue,sailingWeeks:sailingWeeksValue});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.showErrorMessage", false);
            component.set("v.originSelected", originValue);
            component.set("v.destinationSelected", destinationValue);
            
			if(response.getReturnValue()!=null){
				component.set("v.routeList",response.getReturnValue());
                component.set("v.showErrorMessage", false);
                
                var responseRoutes = component.get("v.routeList");
                component.set("v.originCode", responseRoutes[0].originPortAbbr);
                component.set("v.destinationCode", responseRoutes[0].destinationPortAbbr); 
                component.set("v.NoOfRoutes", responseRoutes[0].routeCount);
			}
			else{
                component.set("v.showErrorMessage", true);
                component.set("v.routeList",null);
			}
            
        });
        $A.enqueueAction(action);	
    },
    handleFCLClick : function(component, event) {
        component.set("v.shipmentType", "FCL");
    },
    handleLCLClick : function(component, event) {
        component.set("v.shipmentType", "LCL");
    },
    ExpandCollapseSection : function(component, event, helper) {
        var noOfRoutes = component.get("v.NoOfRoutes");
        var fucnRequired = component.get("v.ExpandCollapseButton");
        var i = 1;
        var collapseLabel = 'Collapse All'; var expandLabel = 'Expand All';
        if(fucnRequired == collapseLabel) {
            while (i <= noOfRoutes ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", expandLabel);
        }
        else if(fucnRequired == expandLabel) {
            while (i <= noOfRoutes ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);  
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", collapseLabel);
        }
    },
	
    clearForm : function(component, event) {
        var originList = component.get("v.originPort");
        var destinationList = component.get("v.destinationPort");
        var weekList = component.get("v.sailingWeeks");
        component.set("v.originPort","");
        component.set("v.originPort",originList);
        component.set("v.destinationPort","");
        component.set("v.destinationPort",destinationList);
        component.set("v.sailingWeeks","");
        component.set("v.sailingWeeks",weekList);
        component.set("v.shipmentType", "FCL"); 
        component.set("v.routeList","");  
        component.set("v.showErrorMessage",false);
    },
    
    PrintFullSchedule : function(component, event) {
        var noOfRoutes = component.get("v.NoOfRoutes");
        var i = 1;
        var collapseLabel = 'Collapse All';
        while (i <= noOfRoutes ) {
            var bodyId = "collapseOne" + i;
            var sectionId = "collapseSection" + i;
            var bodyElement = document.getElementById(bodyId);
            var sectionElement = document.getElementById(sectionId);
            $A.util.addClass(bodyElement, 'show');
            $A.util.removeClass(sectionElement, 'collapsed');
            
            sectionElement.setAttribute('aria-expanded', true);
            console.log('attribute - ' + sectionElement.getAttribute('aria-expanded'));
            i++;
        }
        component.set("v.ExpandCollapseButton", collapseLabel);
       	
        var cmpEvent = component.getEvent("printForm"); 
        cmpEvent.setParams({"PrintPreview" : true}); 
        cmpEvent.fire();
    },
    PrintSection : function (component, event, helper) {
        helper.printIndividualSection(component, event);
    }
})