({
  fetchShipmentTypePicklist : function(component){
        var action = component.get("c.getShipmentType");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.shipmentType", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchOriginPicklist : function(component){
        var action = component.get("c.getOriginPorts");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.originPort", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchDestinationPicklist : function(component){
        var action = component.get("c.getDestinationPorts");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.destinationPort", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchSailingWeeksPicklist : function(component){
        var action = component.get("c.getSailingWeeks");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.sailingWeeks", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
    printIndividualSection : function(component, event) {
    	var sectionId = event.currentTarget.id;
        var IntSectionId = parseInt(sectionId, 10);
    	var noOfRoutes = component.get("v.NoOfRoutes");
        var i = 1;
        var expandLabel = 'Expand All';
        while (i <= noOfRoutes ) {
            var bodyId = "collapseOne" + i;
            var sectionId = "collapseSection" + i;
            var bodyElement = document.getElementById(bodyId);
            var sectionElement = document.getElementById(sectionId);
            if(i != IntSectionId && $A.util.hasClass(bodyElement, 'show')) {
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
            }
            if(i == IntSectionId && !($A.util.hasClass(bodyElement, 'show'))) {
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
            }
            i++;
        }
        component.set("v.ExpandCollapseButton", expandLabel);
       	
        var cmpEvent = component.getEvent("printForm"); 
        cmpEvent.setParams({"PrintPreview" : true}); 
        cmpEvent.fire();
	}
})