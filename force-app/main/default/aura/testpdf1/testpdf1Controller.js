({
    savePDF : function(cmp, event, helper) {
        var action = cmp.get("c.savePDFOnAccount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Attachment saved successfully');   
            }
        });
        $A.enqueueAction(action);
    }
})