({
	 highlightListItems: function (component, event) {
        var menuItem = event.getParam("selectedMenu");
        var action = component.get("c.getMenuItems");
        action.setCallback(this, function (a) {
            var menuOptions = a.getReturnValue();
            var i = 0;
            var highlightSequence = 0;
            for (i = 0; i < menuOptions.length; i++) {
                if (menuOptions[i]['Lightning_Component_Name__c'] == menuItem) {
                    highlightSequence = i + 1;
                }
                menuOptions[i]['MasterLabel'] = $A.get('$Label.c.' + menuOptions[i]['MasterLabel']);
            }
            component.set("v.listitems", menuOptions);
            if (highlightSequence != 0) {
                component.set("v.highlightedItem", highlightSequence);
            }
            
            /*if (($A.get("$Browser.isTablet") || $A.get("$Browser.isPhone")) && $A.util.hasClass(document.getElementById("verticalMenuDiv"), "fliph")) {
                $A.util.removeClass(document.getElementById("verticalMenuDiv"), "fliph");
            } */
        });          
        $A.enqueueAction(action);
    }
})