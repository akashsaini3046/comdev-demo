({
    getDataInDataTable: function (component, fieldName, order) {
        console.log('Inside getDataInDataTable helper');
        var action = component.get("c.fetchBillOfLadingData");
        action.setParams({
            "recordLimit": component.get("v.initialRows"),
            "recordOffset": component.get("v.rowNumberOffset"),
            "fieldName": fieldName,
            "order": order
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('suucces');
                var billOfLadingDetails = response.getReturnValue();
                console.log('billOfLadingDetails  --> '+billOfLadingDetails);
                billOfLadingDetails.forEach(function(record){
                    //record.linkName = '/CrowleyCustomerCommunity/'+record.Id;
                    record.linkName ='/'+component.get("v.communityName")+'/'+record.Id;
                });
                if (billOfLadingDetails != null) {
                    for (var i = 0; i < billOfLadingDetails.length; i++) {
                        var billOfLadingRow = billOfLadingDetails[i];
                        if (billOfLadingRow.Booking_Number__c) billOfLadingRow.BookingNumber = billOfLadingRow.Booking_Number__r.Booking_Number__c;
                    }
                    component.set('v.data', billOfLadingDetails);
                    component.set("v.currentCount", component.get("v.initialRows"));
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
   
    getTotalNumberOfBillOfLadings : function(component) {
        var action = component.get("c.getTotalNumberOfBillOfLadings");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" ) {
                var resultData = response.getReturnValue();
                component.set("v.totalNumberOfRows", resultData);
            }
        });
        $A.enqueueAction(action);
    },
    
    getMoreBillOfLadings: function(component, fieldName, order){
        var rows = component.get('v.rowsToLoad');
        var action = component.get("c.fetchBillOfLadingData");
        var recordOffset = component.get("v.currentCount");
        var recordLimit = component.get("v.rowsToLoad");
        console.log(recordLimit + " : " + recordOffset + " : " + fieldName + " : " + order);
        action.setParams({
            "recordLimit": recordLimit,
            "recordOffset": recordOffset,
            "fieldName": fieldName,
            "order": order
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bookingDetails = response.getReturnValue();
                bookingDetails.forEach(function(record){
                    //record.linkName = '/CrowleyCustomerCommunity/'+record.Id;
                     record.linkName ='/'+component.get("v.communityName")+'/'+record.Id;
                });
                if (bookingDetails != null) {
                    if (component.get('v.data').length !== component.get('v.totalNumberOfRows')) {
                        var currentData = component.get('v.data');
                        var newData = currentData.concat(bookingDetails);
                        component.set('v.data', newData);
                        recordOffset = recordOffset + recordLimit;
                        component.set("v.currentCount", recordOffset);
                    }
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
            console.log("finish");
        });
        $A.enqueueAction(action);
    },
    
    getColumnDefinitions: function () {
        typeAttributes: {label: { fieldName: 'linkLabel' }}
        var columns = [
            {label: 'Name', fieldName:'Name',type:'text'},
            {label: 'Bill Of Lading Status', fieldName: 'Bill_of_lading_Status__c', type: 'text', sortable: true},
            {label: 'Booking Number', fieldName: 'BookingNumber', type: 'text', sortable: true},
            {label: 'Booking Reference Number', fieldName: 'Booking_Reference_Number__c', type: 'text', sortable: true},
            {label: 'Initiate Date', fieldName: 'Initiate_Date__c', type: 'date', sortable: true},
            {label: 'Issue Date', fieldName: 'Issue_Date__c', type: 'date', sortable: true},
            {label: 'Created Date', fieldName: 'CreatedDate', type: 'date', sortable: true},
            {label: 'Created By', fieldName: 'CreatedBy', type: 'text', sortable: true},
            //{label: 'Record Id', fieldName: 'Id', type: 'text', sortable: true},
            {label: 'View Bill Of Ladings', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View  Details'}},
        ];
        return columns;
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var reverse = sortDirection !== 'asc';
        var action = component.get("c.fetchBookingData");
        action.setParams({
            "recordLimit": component.get("v.initialRows"),
            "recordOffset": component.get("v.rowNumberOffset"),
            fieldName : fieldName,
            order : sortDirection
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bookingDetails = response.getReturnValue();
                bookingDetails.forEach(function(record){
                    //record.linkName = '/CrowleyCustomerCommunity/'+record.Id;
            		record.linkName ='/'+component.get("v.communityName")+'/'+record.Id;
                });
                if (bookingDetails != null) {
                    for (var i = 0; i < bookingDetails.length; i++) {
                        var bookingRow = bookingDetails[i];
                        if (bookingRow.Account__c) bookingRow.AccountName = bookingRow.Account__r.Name;
                    }
                    component.set('v.data', bookingDetails);
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
    
    showBillOfLAdingInformation : function(component,row,event){        
            //var urlEvent = $A.get("e.force:navigateToURL");
            /* urlEvent.setParams({
        	"url": 'https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/bill-of-lading/' + row.Id,
    	});
        urlEvent.fire();   */
        // window.location.href='https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/bill-of-lading/'+ row.Id;
        window.location.href='/'+component.get("v.communityName")+'/s/bill-of-lading/'+ row.Id;
    },
    showSpinner : function(component){
        component.set("v.spinner",true);
    },
    hideSpinner : function(component){
        component.set("v.spinner",false);
    },
    getCommunityName : function(component){
        var action = component.get("fetchCommunityName");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.comuunityName",response.getReturnValue());
            }
        });
        
    }
})