({
	doInit : function(component, event, helper) {
		helper.fireHighlightEvent(component, event);
        helper.createTrackingIDArray(component);
	},
    addTrackingId : function(component, event, helper) {
        helper.createTrackingElement(component);
    },
    removeElement : function(component, event, helper) {
        helper.removeTrackingElement(component, event);
    },
    TrackShipments : function(component, event, helper) {
        helper.getTrackingResponse(component);
    },
    ClearForm : function(component, event, helper) {
        helper.clearFormInputs(component);
    },
    ExpandCollapseSection : function(component, event, helper) {
        helper.changeSectionState(component);
    },
    ClearFieldValues : function(component, event, helper) {
        helper.createTrackingIDArray(component);
        helper.clearFormInputs(component);
    },
    ClearCargowiseFieldValues : function(component,event,helper){
      component.set("v.trackingIdValue","");
      component.find("shipmentType").set("v.value", 'ForwardingShipment');
    },
    keyCheck: function (component, event, helper) {
        if (event.which == 13)
            helper.getTrackingResponse(component);
    },
    TrackCargowiseShipments : function(component, event, helper) {
        helper.getCargowiseTrackingResponse(component);
    }, 
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },

    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    }  
})