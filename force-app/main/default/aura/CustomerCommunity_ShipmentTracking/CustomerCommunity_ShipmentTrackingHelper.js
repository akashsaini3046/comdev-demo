({
	fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
    createTrackingIDArray : function(component) {
        var TrackingArr = [{"id":0, "value":""},
                           {"id":1, "value":""},
                           {"id":2, "value":""},
                           {"id":3, "value":""},
                           {"id":4, "value":""}];
        component.set("v.TrackingIds",TrackingArr);
    },
    createTrackingElement : function(component) {
        var trackingArr = component.get("v.TrackingIds");
        if(trackingArr.length < 9) {
            trackingArr.push({"id":trackingArr.length, "value":""});
            component.set("v.TrackingIds",trackingArr);
        }
    },
    removeTrackingElement : function(component, event) {
        var elementId = event.currentTarget.id;
        var trackingArr = component.get("v.TrackingIds");
        var newTrackingArr = []; var count = 0;
        for(var i=0; i<trackingArr.length; i++) {
            if(trackingArr[i].id != elementId) {
                newTrackingArr.push({"id":count++, "value":trackingArr[i].value});
            }
        }
        component.set("v.TrackingIds",newTrackingArr);
    },
    getTrackingResponse : function(component) {
        var trackingArr = component.get("v.TrackingIds");
        var trackingIds = [];
        for(var i=0; i<trackingArr.length; i++) {
            if(trackingArr[i].value != '') {
                trackingIds.push(trackingArr[i].value);
            }
        }
        if(trackingIds.length == 0){
            component.set("v.ErrorMessage",$A.get("$Label.c.CustomerCommunity_STErrorMessage1"));
            component.set("v.ShowErrorMessage",true);
            component.set("v.ShowForm",true);
            component.set("v.TrackingInfoList",Null);
        }
        else {
            var action = component.get("c.getShipmentInfo");
            action.setParams({listTrackingIDs :trackingIds});
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    component.set("v.TrackingInfoList",response.getReturnValue());
                    component.set("v.ShowErrorMessage",false);
                    component.set("v.ShowForm",false); 
                    component.set("v.TrackingListLoaded",true); 
                }
                else{
                    component.set("v.ErrorMessage",$A.get("$Label.c.CustomerCommunity_STErrorMessage2"));
                    component.set("v.ShowErrorMessage",true);
                    component.set("v.ShowForm",true);
                    component.set("v.TrackingInfoList",Null);
                } 
            });
            $A.enqueueAction(action);	
        }
    },
    clearFormInputs : function(component) {
        component.set("v.ShowForm",true);
        component.set("v.TrackingInfoList", "");
        component.set("v.milestoneList", "");
        component.set("v.ShowErrorMessage",false);
    },
    changeSectionState : function (component) {
        var noOfResponses = component.get("v.TrackingInfoList").length;
        var funcRequired = component.get("v.ExpandCollapseButton");
        var collapseLabel = 'Collapse All'; var expandLabel = 'Expand All'; var i = 1;
        if(funcRequired == collapseLabel) {
            while (i <= noOfResponses ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", expandLabel);
        }
        else if(funcRequired == expandLabel) {
            while (i <= noOfResponses ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);  
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", collapseLabel);
        }
    },
    getCargowiseTrackingResponse : function(component) {
        var shipmentType = component.find("shipmentType").get("v.value");
        var trackingId = component.find("trackingId").get("v.value");
        console.log('shipmentType-->'+shipmentType+' trackingId-->'+trackingId);
        if(trackingId==''){
        	component.set("v.ShowCargowiseErrorMessage",true);
            component.set("v.Cargowise",'Please Enter Shipment Id');
        }
        else{
        var action = component.get("c.getCWShipmentInfo");
        action.setParams({
            shipmentType: shipmentType,
            trackingId: trackingId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('here--->'+response.getReturnValue());
                component.set("v.milestoneList", response.getReturnValue());
                component.set("v.ShowForm",false);
                //component.set("v.showCargoResponse",true);
                
            }
        });
        $A.enqueueAction(action);
        } } 
})