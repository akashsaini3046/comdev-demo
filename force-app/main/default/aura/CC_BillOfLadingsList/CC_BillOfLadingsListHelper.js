({
    getDataInDataTable: function (component, fieldName, order) {
        var action = component.get("c.fetchBillOfLadingData");
        action.setParams({
            "recordLimit": component.get("v.initialRows"),
            "recordOffset": component.get("v.rowNumberOffset"),
            "fieldName": fieldName,
            "order": order
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var billOfLadingDetails = response.getReturnValue();
                if (billOfLadingDetails != null) {
                    for (var i = 0; i < billOfLadingDetails.length; i++) {
                        var bolRow = billOfLadingDetails[i];
                        if (bolRow.Booking_Number__c) {
                            bolRow.BookingNumber = bolRow.Booking_Number__r.Booking_Number__c
                        }
                    }
                    component.set('v.data', billOfLadingDetails);
                    component.set("v.currentCount", component.get("v.initialRows"));
                } else {
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },

    getTotalNumberOfBillOfLadings: function (component) {
        var action = component.get("c.getTotalNumberOfBillOfLadings");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
                component.set("v.totalNumberOfRows", resultData);
            }
        });
        $A.enqueueAction(action);
    },

    getMoreBillOfLadings: function (component, fieldName, order) {
        var rows = component.get('v.rowsToLoad');
        var action = component.get("c.fetchBillOfLadingData");
        var recordOffset = component.get("v.currentCount");
        var recordLimit = component.get("v.rowsToLoad");
        if (fieldName === 'BookingNumber') {
            fieldName = 'Booking_Number__r.Booking_Number__c';
        }
        action.setParams({
            "recordLimit": recordLimit,
            "recordOffset": recordOffset,
            "fieldName": fieldName,
            "order": order
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bolDetails = response.getReturnValue();
                if (bolDetails != null) {
                    if (component.get('v.data').length !== component.get('v.totalNumberOfRows')) {
                        var currentData = component.get('v.data');
                        for (var i = 0; i < bolDetails.length; i++) {
                            var bolRow = bolDetails[i];
                            if (bolRow.Booking_Number__c) {
                                bolRow.BookingNumber = bolRow.Booking_Number__r.Booking_Number__c
                            }
                        }
                        var newData = currentData.concat(bolDetails);
                        component.set('v.data', newData);
                        recordOffset = recordOffset + recordLimit;
                        component.set("v.currentCount", recordOffset);
                    }
                } else {
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
            console.log("finish");
        });
        $A.enqueueAction(action);
    },

    getColumnDefinitions: function () {
        typeAttributes: { label: { fieldName: 'linkLabel' } }
        var columns = [
            { label: 'Name', fieldName: 'Name', type: 'text', sortable: true },
            { label: 'Bill Of Lading Status', fieldName: 'Bill_of_lading_Status__c', type: 'text', sortable: true },
            { label: 'Booking Number', fieldName: 'BookingNumber', type: 'text', sortable: false },
            { label: 'Booking Reference Number', fieldName: 'Booking_Reference_Number__c', type: 'text', sortable: true },
            { label: 'Initiate Date', fieldName: 'Initiate_Date__c', type: 'date', sortable: true },
            { label: 'Issue Date', fieldName: 'Issue_Date__c', type: 'date', sortable: true },
            { label: 'Created Date', fieldName: 'CreatedDate', type: 'date', sortable: true },
            { label: 'View Bill Of Ladings', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View  Details' } },
        ];
        return columns;
    },

    sortData: function (component, fieldName, sortDirection) {
        var action = component.get("c.fetchBillOfLadingData");
        if (fieldName === 'BookingNumber') {
            fieldName = 'Booking_Number__r.Booking_Number__c';
        }
        console.log(component.get("v.initialRows") + " : " + component.get("v.rowNumberOffset") + " : " + fieldName + " : " + sortDirection);
        action.setParams({
            "recordLimit": component.get("v.initialRows"),
            "recordOffset": component.get("v.rowNumberOffset"),
            fieldName: fieldName,
            order: sortDirection
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bolDetails = response.getReturnValue();
                if (bolDetails != null) {
                    for (var i = 0; i < bolDetails.length; i++) {
                        var bolRow = bolDetails[i];
                        if (bolRow.Booking_Number__c) {
                            bolRow.BookingNumber = bolRow.Booking_Number__r.Booking_Number__c
                        }
                    }
                    component.set('v.data', bolDetails);
                } else {
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },

    showBillOfLAdingInformation: function (component, row, event) {
        window.location.href = '/' +component.get("v.communityName") +'/s/bill-of-lading/' + row.Id;
    },
    showSpinner: function (component) {
        component.set("v.spinner", true);
    },

    hideSpinner: function (component) {
        component.set("v.spinner", false);
    },

    getCommunityUrlPathPrefix: function (component, row, event) {
        var action = component.get("c.getCommunityUrlPathPrefix");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('999 '+response.getReturnValue());
                component.set("v.communityName", response.getReturnValue() );
            } else if (state === "ERROR") {
                console.log("error");
            }
        });
        $A.enqueueAction(action);
    },
})