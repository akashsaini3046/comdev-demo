<aura:application extends="force:slds">
    <lightning:progressIndicator currentStep="4" type="base" variant="base">
        <lightning:progressStep  value="1"/>
        <lightning:progressStep  value="2"/>
        <lightning:progressStep  value="3"/>
        <lightning:progressStep  value="4"/>
    </lightning:progressIndicator>
</aura:application>