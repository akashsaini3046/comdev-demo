({
	handlePrintEvent: function(component, event, helper) {
        var updatedExp = event.getParam("PrintPreview");
        component.set("v.PrintPreview",updatedExp);
        var bodyElement = document.getElementById('DashboardContentDiv');
        $A.util.addClass(bodyElement, 'row-content');  
        $A.util.removeClass(bodyElement, 'dasboard-content'); 
    },
    printRoutes : function(component, event, helper) {
        var btnDivTop = document.getElementById('printCancelbtnTop');
        btnDivTop.setAttribute('style', 'display:none');
        var btnDivBottom = document.getElementById('printCancelbtnBottom');
        btnDivBottom.setAttribute('style', 'display:none');
        window.print();
        component.set("v.PrintPreview",false);
        var bodyElement = document.getElementById('DashboardContentDiv');
        $A.util.addClass(bodyElement, 'dasboard-content');  
        $A.util.removeClass(bodyElement, 'row-content'); 
        // fire event to highlight vertical menu option
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = "CustomerCommunity_FindARoute";
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
    },
    cancelPrint : function(component, event, helper) {
        component.set("v.PrintPreview",false);
        var bodyElement = document.getElementById('DashboardContentDiv');
        $A.util.addClass(bodyElement, 'dasboard-content');  
        $A.util.removeClass(bodyElement, 'row-content'); 
        // fire event to highlight vertical menu option
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = "CustomerCommunity_FindARoute";
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
    }
})