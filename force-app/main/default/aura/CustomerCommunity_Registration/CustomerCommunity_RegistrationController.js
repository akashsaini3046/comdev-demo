({
    handleClose: function (component, event, helper) {
        var closeModalEvent = $A.get("e.c:CustomerCommunity_CloseModalEvent");
        closeModalEvent.setParams({
            "isModalVisible": true
        });
        closeModalEvent.fire();
    },
    getUserFieldsInit: function (component, event, helper) {
        helper.getUserFieldsInit(component, event);
        helper.getPicklistValues(component, event);
    },
    userSignUp: function (component, event, helper) {
        helper.userSignUp(component, event);
    },
    createNewUser: function (component, event, helper) {
        helper.signupNewUser(component, event);
    },
    showConfirmationModal: function (component) {
        var UserSuccessMsg = component.get("v.UserSuccessMsg");
        var isError = component.get("v.isError");
        var openConfirmationModal = $A.get("e.c:CustomerCommunity_OpenConfirmationEvent");
        openConfirmationModal.setParams({
            "isConfirmationVisible": true,
            "userMessage": UserSuccessMsg,
            "isError": isError
        });
        openConfirmationModal.fire();
    }
});