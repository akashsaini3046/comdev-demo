({
	doInit : function(cmp, event, helper) {
		var ideaCommentId = helper.getURLParameter('id');
        cmp.set("v.ideaCommentId", ideaCommentId);
        helper.getIdeaCommentRecordAction(cmp, helper);
	},
    saveCommentHandler : function(cmp, event, helper) {
		helper.saveIdeaCommentRecordAction(cmp, helper);
	},
    cancel : function(cmp, event, helper) {
        var comment = cmp.get("v.ideaCommentRecord");
        var ideaId = comment.IdeaId;
		window.location.href = $A.get("$Label.c.Idea_Detail_Page")+'?id='+ideaId;
	}
})