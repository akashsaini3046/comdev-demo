({
    showConfirmationModal: function (component, event, helper) {
        var isConfirmationVisible = event.getParam("isConfirmationVisible");
        var userStatus = event.getParam("userMessage");
        var isError = event.getParam("isError");
        if (isConfirmationVisible == true) {
            component.set("v.isVisible", true);
            component.set("v.userStatus", userStatus);
            component.set("v.isError", isError);
        }
        window.setTimeout($A.getCallback(function () {
            if (component.isValid()) {
                component.set("v.isVisible", false);
            } else {
                console.log('Component is Invalid');
            }
        }), 10000);
    }
})