({
	getURLParameter : function(key) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
			console.log('sParameterName -> ',sParameterName);
            if (sParameterName[0] == key) {
                return sParameterName[1];
            }
        }
        return '';
        //return '087M00000008klqIAA';
	},
    
    getIdeaDetails : function(cmp, helper, ideaId){
        var action = cmp.get("c.getIdeaDetails");
        action.setParams({ideaId: cmp.get("v.ideaId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('ideaRecord -> ',response.getReturnValue());
                cmp.set("v.ideaRecord", response.getReturnValue());
                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getZonesList : function(cmp, helper){
        var action = cmp.get("c.getZonesList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getZonesList', response.getReturnValue());
                cmp.set("v.zonesOptions", JSON.parse(response.getReturnValue()) );
                // Let DOM state catch up.
                window.setTimeout(
                    $A.getCallback( function() {
                        // Now set our preferred value
                        if(cmp.get("v.ideaRecord")){
                            cmp.find("zones").set("v.value", cmp.get("v.ideaRecord").CommunityId);
                            helper.getRelevantRecordTypeId(cmp,helper,cmp.get("v.ideaRecord").CommunityId);
                        }
                    }));
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaStatuses : function(cmp, helper){
        var action = cmp.get("c.getIdeaStatuses");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaStatuses', JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaStatuses", JSON.parse(response.getReturnValue()));
                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCategories : function(cmp, helper){
        var action = cmp.get("c.getIdeaCategories");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaCategories', JSON.parse(response.getReturnValue()));
                cmp.set("v.categories", JSON.parse(response.getReturnValue()));
                helper.setCategoriesOptions(cmp);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getBenefits : function(cmp, helper){
        var action = cmp.get("c.getBenefits");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getBenefits', JSON.parse(response.getReturnValue()));
                cmp.set("v.benefits", JSON.parse(response.getReturnValue()));
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    setCategoriesOptions: function (cmp) {
        cmp.set("v.categoriesOptions", cmp.get("v.categories"));
        var categories = cmp.get("v.ideaRecord.Categories");
        console.log('categories', categories);
        var categoriesValues = [];
        if(categories){
            categoriesValues = categories.split(';');
        }
        console.log('categoriesValues', categoriesValues);
        // "values" must be a subset of values from "options"
        cmp.set("v.categoriesValues", categoriesValues);
    },
    
    saveIdea: function (cmp, event, helper) {
        console.log('ideaRecord -> ', cmp.get("v.ideaRecord"));
        var ideaRecord = cmp.get("v.ideaRecord");
        var ifRemoveAttachment = cmp.get("v.ifRemoveAttachment");
        if(ifRemoveAttachment){
            ideaRecord.AttachmentName = null;
            ideaRecord.AttachmentBody = null;
            ideaRecord.AttachmentContentType = null;
            ideaRecord.AttachmentLength = null;
        }
        var ideaId = cmp.get("v.ideaId");
        var action = cmp.get("c.saveIdeaRecord");
        var param = { ideaRecord:  ideaRecord};
        console.log(param);
        action.setParams(param);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                var childComponent = cmp.find("fileUploadCmp");
                
                //If save is done without uploading or replacing attachment
                if(!childComponent){
                    helper.showToast("success", "Success", "The record has been updated successfully");
                    window.location.href = $A.get("$Label.c.Idea_Detail_Page")+'?id='+cmp.get("v.ideaId");
                }else{
                	var message = childComponent.saveFile(result.Id);    
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getIdeaFieldDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaFieldDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log('getIdeaFieldDescribe -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaFieldDescribe", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaDescribeResult -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaDescribe", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message
        });
        toastEvent.fire();
    },
    getRelevantRecordTypeId: function(cmp, helper, selectedZoneId){
        console.log('SELECTED ZONE ID '+selectedZoneId);
    	var action=cmp.get("c.getRecordTypeId");
        action.setParams({
    		zoneId : selectedZoneId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('getRelevantRecordTypeId state '+state);
            if (state === "SUCCESS") {
                console.log('ínside success');
                console.log('selectedZoneRecordTypeId -> ',response.getReturnValue());
                cmp.set("v.selectedZoneRecordTypeId", response.getReturnValue());
                helper.getRelevantPickListValues(cmp,helper,cmp.get("v.selectedZoneRecordTypeId"));
            }
             else {
                console.log(" getRelevantRecordTypeId failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    getRelevantPickListValues: function(cmp,helper,recordTypeId){
        console.log('getRelevantPickListValues  '+recordTypeId);
        var action=cmp.get("c.fetchRecordTypeSpecificPickListvalues");
        action.setParams({
    		recordTypeId : recordTypeId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PicklistValues -> ',response.getReturnValue());
                var statusData=response.getReturnValue()['Status'];
                var categoriesData=response.getReturnValue()['Categories'];
                var benefitsData=response.getReturnValue()['Benefits'];
                var status=[];
                var categories=[];
                var benefits=[];
                console.log('statusData '+JSON.stringify(statusData));
                console.log('categoriesData '+JSON.stringify(categoriesData));
                for(var key in statusData)
                    status.push({label:key,value:statusData[key]});
                for(var key in categoriesData)
                    categories.push({label:key,value:categoriesData[key]});
                for(var key in benefitsData)
                    benefits.push({label:key,value:benefitsData[key]});
                cmp.set("v.benefits",benefits);
                cmp.set("v.ideaStatuses",status);
                cmp.set("v.categoriesOptions",categories);
                
                categoriesVal = cmp.get("v.ideaRecord.Categories");
                var categoriesValues = [];
                if(categoriesVal){
                    categoriesValues = categoriesVal.split(';');
                }
                // "values" must be a subset of values from "options"
                cmp.set("v.categoriesValues", categoriesValues);
                
                helper.getIdeasListAction(cmp, helper);
                
                    
            }
            else {
                console.log(" getRelevantPickListValues failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
    }
})