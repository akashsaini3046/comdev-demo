({
    verifyOTP : function(component,event) {
        var userIdentifier = component.get("v.userIdentifier");
        var verDig1 = component.get("v.verDigit1").toString();
        var verDig2 = component.get("v.verDigit2").toString();
        var verDig3 = component.get("v.verDigit3").toString();
        var verDig4 = component.get("v.verDigit4").toString();
        var verDig5 = component.get("v.verDigit5").toString();
        var verCode = null;
        
        if(verDig1 == "" || verDig2 == "" || verDig3 == "" || verDig4 == "" || verDig5 == ""){
            component.set("v.errorMessage", "Fill in Verification Code and Continue");
            component.set("v.showErrorMessage", true); 
            return;
        }
        else {
            var userIdentifier = component.get("v.userIdentifier");
            verCode = verDig1 + verDig2 + verDig3 + verDig4 + verDig5 ; 
            var action = component.get("c.OTPVerification");
            action.setParams({
                "Identifier": userIdentifier,
                "verifyCode" : verCode 
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var value = response.getReturnValue(); 
                    if (value == "True") {
                        console.log('navigateToAccountScreen @@');
                        this.navigateToAccountScreen(component,event);
                    } else if (value == "False") {  
                        component.set("v.errorMessage", "Please check the OTP. If you still can't log in, contact your Crowley administrator");
                        component.set("v.showErrorMessage", true);
                        component.set("v.showOTPMessage",false);
                    } else if (value == "Error") {
                        component.set("v.errorMessage", "Something went wrong. Please contact Crowley Administrator.");
                        component.set("v.showErrorMessage", true);
                        component.set("v.showOTPMessage",false);
                    }
                } else {
                    component.set("v.errorMessage", "Something went wrong. Please contact Crowley Administrator.");
                    component.set("v.showErrorMessage", true);
                    component.set("v.showOTPMessage",false);
                 }
              
            });
            $A.enqueueAction(action);
        }
    },
    resendOTP : function(component,event){
        var action = component.get("c.OTPResend");
        action.setParams({
            "userEmail": component.get("v.userEmail"),
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                if(response == 'False'){
                    component.set("v.errorMessage",'Something went wrong. Please try Again.');
                    component.set("v.showErrorMessage",true);
                    component.set("v.showOTPMessage", false); 
                }
                else{
                    component.set("v.OTPMessage",'An OTP has been successfully sent to your Email.');
                    component.set("v.showOTPMessage",true);
                    component.set("v.showErrorMessage", false); 
                }  
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    navigateToAccountScreen : function(component,event){
        component.set("v.showOTPMessage",false);
        component.set("v.showErrorMessage",false);
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_CreateAccount"      
        });
        homeScreenEvent.fire();
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    }
});