({  
    verifyOTP: function (component,event,helper) {
        helper.verifyOTP(component,event);
    },
    resendOTP : function(component,event,helper){
      helper.resendOTP(component,event);
    },
    setkeycode1 : function(component,event,helper){
        var keycode = event.keyCode;
        if(keycode == 8 || keycode == 46){
           component.find("digit1").focus();  
        }
        else{
            component.find("digit2").focus();
        }
    },
    setkeycode2 : function(component,event,helper){
        var keycode = event.keyCode;
        if(keycode == 8 || keycode == 46){
            component.find("digit1").focus();
        }
        else{
            component.find("digit3").focus();
        }
    },
    setkeycode3 : function(component,event,helper){
        var keycode = event.keyCode;
        if(keycode == 8 || keycode == 46){
            component.find("digit2").focus();  
        }
        else{
            component.find("digit4").focus(); 
        }
    },
    setkeycode4 : function(component,event,helper){
        var keycode = event.keyCode;
        if(keycode == 8 || keycode == 46){
            component.find("digit3").focus();  
        }
        else{
            component.find("digit5").focus(); 
        }
    },
    setkeycode5 : function(component,event,helper){
        var keycode = event.keyCode;
        if(keycode == 8 || keycode == 46){
            component.find("digit4").focus();  
        }
    },
    navigateToRegisterScreen : function(component,event,helper){
        component.set("v.showOTPMessage",false);
        component.set("v.showErrorMessage",false);
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_Register"
        });
        homeScreenEvent.fire(); 
    },
    handleOnRender :  function(component,event,helper){
         var elem= component.find("getDeviceHeight").getElement();
        var winHeight = window.innerHeight;
        elem.style.height = winHeight + "px";
    }
})