({
    onChangeSearchText : function(component){
        var inputValue = component.find("inputSearch").get("v.value");
        component.set("v.selectedItem", inputValue);
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    checkFieldValidity : function(component){
        var inputField = component.find("inputSearch");
        var inputFieldValue = inputField.get('v.value');
        if(inputFieldValue == "" || typeof(inputFieldValue) == "undefined"){
            inputField.showHelpMessageIfInvalid();
       }
    },
    clearInputValue : function(component,event){
       component.find("inputSearch").set("v.value", ''); 
        if(component.get("v.buttonText") == "Location"){
            var locationEvent = component.getEvent("DisplayLocationFields");
            locationEvent.setParams({
                "makeLocationFieldsVisible" : false     
            });
            locationEvent.fire();
        }
    },
    removeAddDialog : function(component,event,helper){
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        if(component.get("v.buttonText") == "Location"){
            helper.showLocationFields(component,event,false,null,true);
        }
        else if(component.get("v.buttonText") == 'Company'){
            helper.addAccountEvent(component,event,true,null);
        }
    }, 
    keyPressController : function(component, event, helper) {
        component.set("v.loading", true);
        var getInputkeyWord = component.get("v.SearchKeyWord");
        if( getInputkeyWord !== "" ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component, event, getInputkeyWord);
        } else {  
            component.set("v.listOfSearchRecords", null ); 
            if(component.get("v.buttonText") == "Location"){
                helper.hideLocationFields(component,event);
            }
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    }, 
    handleComponentEvent : function(component, event, helper) {	 
        var selectedItemGotFromEvent = event.getParam("selectedItem");
        var selectedItemId = event.getParam('selectedItemID');
        component.set("v.selectedItem" , selectedItemGotFromEvent); 
        component.find("inputSearch").set("v.value", selectedItemGotFromEvent)
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open'); 
        if(component.get("v.buttonText") == 'Location'){
            helper.setLocationDetails(component,event,selectedItemGotFromEvent,false);  
        }else if(component.get("v.buttonText") == 'Company'){
            helper.addAccountEvent(component,event,false,selectedItemId);
        }else if(component.get("v.buttonText") == 'CompanyBooking'){
            var cvifId = event.getParam('cvifId');
            component.set("v.cvifId", cvifId);
            helper.addAccountEvent(component, event, false, selectedItemId, cvifId);
        }else if(component.get("v.buttonText") == 'Country'){
            if(selectedItemGotFromEvent == 'UNITED STATES' || selectedItemGotFromEvent == 'United States'){
                helper.toggleStateField(component,event,true);    
            }
            else{
               helper.toggleStateField(component,event,false);  
            }
        }else if(component.get("v.buttonText") == 'TermLocation'){
            var location = selectedItemId;
            var termType = event.getParam('termType');
            var code = event.getParam('code');
            helper.setTermLocationEvent(component, event, location, termType, selectedItemGotFromEvent, code);
        }
    }, 
    onBlurInputSearch : function(component, event, helper) {
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
})