({
    getFieldLabelList : function(component){
        var action = component.get("c.getFieldLabels");
        action.setCallback(this, function(response){
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.listFieldLabels",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    disableExcelInput: function(cmp) {
        cmp.set("v.disabled", true);
        cmp.set("v.isLoading", true);
    },

    enableExcelInput: function(cmp) {
        cmp.set("v.disabled", false);
        cmp.set("v.isLoading", false);
    },

    importTableAndThrowEvent: function(cmp, evt, helper) {
        evt.stopPropagation();
        evt.preventDefault();
        try {
            const file = helper.validateFile(cmp, evt);
            helper.createFileRecord(cmp, evt, helper);
            helper.readExcelFile(file)
                .then($A.getCallback(excelFile => {
                    helper.throwSuccessEvent(cmp, excelFile);
                }))
                .catch($A.getCallback(exceptionMessage => {
                    helper.throwExceptionEvent(cmp, exceptionMessage);

                }))
                .finally($A.getCallback(() => {
                    helper.enableExcelInput(cmp);
                }))
        } catch (exceptionMessage) {
            helper.throwExceptionEvent(cmp, exceptionMessage);
            helper.enableExcelInput(cmp);
        }
    },

    validateFile: function(cmp, evt) {
        const files = evt.getSource().get("v.files");
        console.log('@@@ files - ' + files);
        if (!files || files.length === 0 || $A.util.isUndefinedOrNull(files[0])) {
            throw cmp.get("v.messageNoFileSpecified");
        }

        const file = files[0];
        console.log('@@@ file - ' + file);
        const fileSizeThreshold = cmp.get("v.fileSizeThreshold");
        if (file.size > fileSizeThreshold) {
            throw (cmp.get("v.messageFileSizeExceeded") + ': ' + fileSizeThreshold + 'b');
        }
        return file;
    },

    readExcelFile: function(file) {
        console.log('file readExelFile - ' + file);
        return new Promise(function (resolve, reject) {
            const fileReader = new FileReader();
            fileReader.onload = event => {
                let filename = file.name;
                let binary = "";
                console.log('@@@ event.target.result - ' + event.target.result);
                new Uint8Array(event.target.result).forEach(function (byte) {
                    binary += String.fromCharCode(byte);
                });

                try {
                    resolve({
                        "fileName": filename,
                        "xlsx": XLSX.read(binary, {type: 'binary', header: 1})
                    });
                } catch (error) {
                    reject(error);
                }
            };
            fileReader.readAsArrayBuffer(file);
        });
    },

    throwExceptionEvent: function(component, message) {
        const errorEvent = component.getEvent("onImport");
        errorEvent.setParams({
            "type": "ERROR",
            "message": message
        });
            console.log('@@@ Exception Event');
        errorEvent.fire();
    },

    throwSuccessEvent: function(component, parsedFile) {
        const successEvent = component.getEvent("onImport");
        successEvent.setParams({
            "type": "SUCCESS",
            "fileName": parsedFile.fileName,
            "table": parsedFile.xlsx
        });
        console.log('@@@ Success Event - ' + parsedFile.xlsx);
        //this.createRecords(component, parsedFile);
        successEvent.fire();
    },
    
    createRecords : function(component, parsedFile){
        const get = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
        var fieldList = component.get("v.listFieldLabels");
        var cellRange = parsedFile.xlsx.Sheets.Sheet1["!ref"]; 
        var startRange = cellRange.substring(0,cellRange.indexOf(":")).split(/([0-9]+)/);
        var endRange = cellRange.substring(cellRange.indexOf(":")+1,cellRange.length).split(/([0-9]+)/);
        if(fieldList != null && fieldList.length>0){
            for(var i=0;i<fieldList.length;i++){
                var cellLocation = this.findCellLocation(parsedFile,fieldList[i].MasterLabel, startRange, endRange);
                if(cellLocation != null){
                    var fieldDataText = "";
                    var fieldRange = cellLocation.split(/([0-9]+)/);
                    if(fieldList[i].Traverse_Path__c == "Row"){
                        for(var row = parseInt(fieldRange[1], 10)+1; row <= endRange[1]; row++){
                            var cellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],fieldRange[0]+row,'v'], parsedFile);
                            if(cellValue != null && cellValue != ""){
                                if(fieldList[i].Ending_Label__c != null && fieldList[i].Ending_Label__c !="" && 
                                   fieldList[i].Ending_Label__c == cellValue)
                                    break;
                                else
                                    fieldDataText = fieldDataText + '\n'+ cellValue;
                            }
                        }
                        fieldList[i].Field_Data__c = fieldDataText.toString();
                    }
                    if(fieldList[i].Traverse_Path__c == "Column"){
                        for(var col = fieldRange[0].charCodeAt(0)+1; col <= endRange[0].charCodeAt(0); col++){
                            var cellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],String.fromCharCode(col)+fieldRange[1],'v'], parsedFile);
                            if(cellValue != null && cellValue != ""){
                                if(fieldList[i].Ending_Label__c != null && fieldList[i].Ending_Label__c !="" && 
                                   fieldList[i].Ending_Label__c == cellValue)
                                    break;
                                else
                                    fieldDataText = fieldDataText + '\n' + cellValue;
                            }
                        }
                        fieldList[i].Field_Data__c = fieldDataText.toString();
                    }
                }
            }
            var action = component.get("c.createBLRecords");
            action.setParams({ listFormData :fieldList});
            action.setCallback(this, function(response){
            });
            $A.enqueueAction(action);
        }
    },
        
    findCellLocation : function(parsedFile, fieldLabel, startRange, endRange){
        const get = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
        for(var col = startRange[0].charCodeAt(0); col <= endRange[0].charCodeAt(0); col++){
            for(var row = startRange[1]; row <= endRange[1]; row++){
                if(fieldLabel == get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],String.fromCharCode(col)+row,'v'], parsedFile))
                    return String.fromCharCode(col)+row;
            }
        }
        return null;
    },
    
    createFileRecord : function(component, event, helper){
        var files = event.getSource().get("v.files");
            var file = files[0];
            var reader = new FileReader();
            reader.onloadend = function() {
                var dataURL = reader.result;
                var content = dataURL.match(/,(.*)$/)[1];
                helper.upload(component, file, content);
            }
            reader.readAsDataURL(file);
        
         
    },
    upload: function(component, file, base64Data) {
        var action = component.get("c.createFileBooking");
        console.log('type: ' + file.name);
        action.setParams({
            fileName: file.name,
            base64Data: base64Data,
            contentType: file.type
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                //callback(a.getReturnValue());
                console.log('@@@  File Uploaded');
            }
            console.log('@@@  File Upload In Progress');
        });
        $A.enqueueAction(action);
    }
})