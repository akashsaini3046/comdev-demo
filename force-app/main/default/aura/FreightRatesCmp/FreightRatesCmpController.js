({
    doInit: function (component, event, helper) {
        var screen = component.get('v.screen');
        var routeId;
        if (screen === 'BookingDetail') {
            var rates = component.get("v.rating");
            routeId = component.get("v.selectedRouteId");
            if (rates && rates !== null) {
                console.log("rates :", JSON.stringify(rates));
                var freightRates = rates[0].DocValuesData;
                var totalAmount = 0;
                var grandTotal = 0;
                var totalVat = 0;
                var contributionAmount = (contributionData.ContributionAmount && contributionData.ContributionAmount !== null ? contributionData.ContributionAmount : 0.00);
                var sumCost = (contributionData.SumCost && contributionData.SumCost !== null ? contributionData.SumCost : 0.00);
                var sumRevenue = (contributionData.SumRevenue && contributionData.SumRevenue !== null ? contributionData.SumRevenue : 0.00);
                for (var i = 0; i < freightRates.length; i++) {
                    totalAmount += freightRates[i].AmountTarget;
                    totalVat += 0;
                }
                grandTotal = totalAmount + totalVat;
                component.set("v.totalAmount", totalAmount);
                component.set("v.grandTotal", grandTotal);
                component.set("v.totalVat", totalVat);
                component.set("v.contributionAmount", contributionAmount);
                component.set("v.sumCost", sumCost);
                component.set("v.sumRevenue", sumRevenue);
            }
        } else {
            var bookingWrapper = component.get("v.bookingWrapper");
            routeId = bookingWrapper.selectedRouteId;
            if (typeof routeId !== "undefined") {
                var ratingResponse = component.get("v.RatingApiResponseWrapper");
                if (ratingResponse && ratingResponse.success) {
                    var i = 0;
                    for (; i < ratingResponse.length; i++) {
                        if (ratingResponse[i].RouteId == routeId) {
                            break;
                        }
                    }
                    bookingWrapper.booking.ChargeLineItem__c = JSON.stringify(ratingResponse.result[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0]);
                    bookingWrapper.booking.Selected_Route_Id__c = routeId.toString();
                    var schedulesResponse = ratingResponse.result[i].Schedules;
                    //bookingWrapper.shipment.listVogage=[];
                    for(var j=0; j < schedulesResponse.length; j++){                        
                        for(var k=0; k < schedulesResponse[j].Segments.length; k++){
                            if(schedulesResponse[j].Segments[k].IsOcean){                           
                                var schedules = new Object();
                                schedules.Vessel_Name__c = schedulesResponse[j].Segments[k].VesselName;
                                schedules.Voyage_Number__c = schedulesResponse[j].Segments[k].VoyageNumber.NumberX;
                                schedules.Loading_Location_Code__c = schedulesResponse[j].Segments[k].FromLocation.Code;
                                schedules.Discharge_Location_Code__c = schedulesResponse[j].Segments[k].ToLocation.Code;
                                schedules.Estimate_Sail_Date__c = new Date(schedulesResponse[j].StartSailingDate.UtcDateTime);
                                schedules.Estimate_Arrival_Date__c = new Date(schedulesResponse[j].EndSailingDate.UtcDateTime);
                                bookingWrapper.shipment.listVogage.push(schedules);
                            }

                        }
                        
                    }

                    component.set("v.bookingWrapper", bookingWrapper);
                    
                    var costValuesGroup =ratingResponse.result[i].CalculatedContributionResult.ItemValues[0].ValuesDataCost[0].ValuesGroup[0];
                    var freightRates = costValuesGroup.DocValuesData;
                    component.set("v.freightRates", freightRates);
                    component.set("v.dataCostAmount", costValuesGroup.SumOfValues);
                    component.set("v.dataCostVat", costValuesGroup.SumOfVat);
                    component.set("v.totalDataCostSum", costValuesGroup.TotalSum);

                    
                    var revenueValueGroup =ratingResponse.result[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0];
                    var revenueRates = revenueValueGroup.DocValuesData;
                    component.set("v.revenueRates", revenueRates);
                    component.set("v.revenueAmount", revenueValueGroup.SumOfValues);
                    component.set("v.revenueVat", revenueValueGroup.SumOfVat);
                    component.set("v.totalRevenueSum", revenueValueGroup.TotalSum);

                    var contributionData = ratingResponse.result[i].CalculatedContributionResult.ItemValues[0].ContributionData[0];
                    component.set("v.contributionData", contributionData);
                }
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Route Not Selected !",
                    "message": "Please select the Route in the Schedule Selection !",
                    "type": "warning"
                });
                toastEvent.fire();
            }
        }
    },
    next: function (component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "next"
        });
        bookingRecordIdEvent.fire();
    },
    submit: function (component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "submit"
        });
        bookingRecordIdEvent.fire();
    },
    saveAndBook: function (component, event, helper) {
        alert('Work in Progress');
    },
})