({
    getAllObjects : function(component,event) {
        var action = component.get("c.fetchObjects");
        console.log(component.get("v.recordId"));
        action.setParams({
            "shippingInstructionId" : component.get("v.recordId")
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                console.log('value ---->' + value);
                component.set("v.recordList",value);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    restoreValues : function(component,event){  
        component.set("v.message", "");
        component.set("v.showMessage", false);
        var url = window.location.href; 
        var value = url.substr(0,url.lastIndexOf('/') + 1);
        window.history.back();
        return false;
    },
    valueChanged : function(component,event){
        var recordList = component.get("v.recordList");
        var list = [];
        var serverlist = component.get("v.serverList");
        var changedId = event.target.dataset.index;
        var sequenceNumber = changedId.slice(0,changedId.indexOf('-'));
        console.log('seqeunceNumber --->'+sequenceNumber);
        for(var index = 0; index<serverlist.length ;index++){
            list.push(serverlist[index]);
        }
        for(var index = 0; index<recordList.length ; index++){
            if(sequenceNumber == recordList[index].sequence){
                if(this.checkIfObjPresent(recordList[index].sequence,list)){
                    list.push(recordList[index]);
                }
            }
        }
        component.set("v.serverList",list);
        console.log(list);
    },
    checkIfObjPresent : function(sequenceCount,list){
        for(var index = 0; index < list.length ; index++){
            if(sequenceCount == list[index].sequence){
                return false;
            }
        }
        return true;
    },
    saveRecords : function(component,event){
        
        var serverList = component.get("v.serverList");
        if(serverList.length == 0){   
            component.set("v.message", "No Changes to be Saved.");
            component.set("v.showMessage", true);
            $A.util.addClass(component.find('typeDiv'), 'error-message'); 
            window.setTimeout($A.getCallback(function () { 
                component.set("v.showMessage", false);
                component.set("v.message","");
            }), 3000);
            
        }
        var action = component.get("c.saveRecordData");
        action.setParams({
            "listWrapper" : serverList
        })
        action.setCallback(this, function (response) {
            var state = response.getState();   
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                if(value == 'TRUE'){
                    component.set("v.message", "Records Saved Successfully !");
                    component.set("v.showMessage", true);
                    while(serverList.length > 0){
                        serverList.pop();
                    }
                    $A.util.addClass(component.find('typeDiv'), 'success-message');  
                    window.setTimeout($A.getCallback(function () { 
                        component.set("v.showMessage", false);
                        component.set("v.message","");
                    }), 3000);
                }
                else if(value == 'FALSE'){  
                    
                    component.set("v.message", "Something went Wrong.Please try Again.");
                    component.set("v.showMessage", true);
                    $A.util.addClass(component.find('typeDiv'), 'error-message');    
                    window.setTimeout($A.getCallback(function () {         
                        component.set("v.showMessage", false);  
                        component.set("v.message","");
                    }), 3000);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})