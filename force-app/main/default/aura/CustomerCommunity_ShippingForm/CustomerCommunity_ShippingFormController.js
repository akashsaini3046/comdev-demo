({
    doInit : function(component, event, helper) { 
        helper.getAllObjects(component,event);
    },
    saveRecords : function(component,event,helper){
        helper.saveRecords(component,event);
    },
    valueChanged: function(component,event,helper){
        helper.valueChanged(component,event);
    },
    restoreValues : function(component,event,helper){
        helper.restoreValues(component,event);
    },
})