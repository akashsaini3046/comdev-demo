({
    searchHelper : function(component, event, getInputkeyWord) {
        var functionality = component.get("v.functionality");
        if(functionality === "findARoute:Country"){
            console.log('inside findARoute:Country');
            this.findARouteSearchCountry(component, event, getInputkeyWord);
        }
        if(functionality === "findARoute:Location"){
            var locType = component.get("v.locType");
            if(locType=='D')
                this.findARouteDoorLocation(component, event, getInputkeyWord);
            if(locType=='P')         
                this.findARoutePortLocation(component, event, getInputkeyWord);
            if(locType=='R')
                this.findARouteRailLocation(component, event, getInputkeyWord);
        }
    },
    findARouteDoorLocation : function (component, event, getInputkeyWord){
        var locCodeMap =[];
        var locationsList = [];
        var action = component.get("c.getZipLocations");
        action.setParams({
            searchKeyWord:getInputkeyWord,
            countryName:component.get("v.countrySelected")
        });
         action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if (responseData.length == 0) {                                        
                   component.set("v.Message", 'No Result Found...');
                } else {
                    for(var i in responseData){
                        var displayValue = responseData[i].Location_Name__c+' ('+responseData[i].Name+')' ;
                        locCodeMap.push({key :displayValue , value :  responseData[i].LcCode__c });
                        locationsList.push(displayValue);
                    }
                    component.set("v.Message", 'Search Result...');
                }
                component.set("v.objectToParent",locCodeMap);
                component.set("v.listOfSearchRecords", locationsList);
            }else if(state === "ERROR"){
                console.log("Error in fetchFCLPorts !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action); 
        
    },
    findARoutePortLocation : function (component, event, getInputkeyWord){ 
        var locCodeMap =[];
        var locationsList = [];
        var action = component.get("c.getPortLocations");
        action.setParams({
            searchKeyWord:getInputkeyWord,
            countryName:component.get("v.countrySelected")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if (responseData.length == 0) {                                        
                    component.set("v.Message", 'No Result Found...');
                } else {
                    for(var i in responseData){
                        var displayValue = responseData[i].City__c+' ('+responseData[i].Name+')';
                        locCodeMap.push({key:displayValue , value:responseData[i].Name});
                        locationsList.push(responseData[i].City__c+' ('+responseData[i].Name+')');
                    }
                    component.set("v.Message", 'Search Result...');
                }
                component.set("v.objectToParent",locCodeMap);
                component.set("v.listOfSearchRecords", locationsList);
            }else if(state === "ERROR"){
                console.log("Error in fetchFCLPorts !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action); 
        
    }
    ,
    findARouteRailLocation : function (component, event, getInputkeyWord){ 
        var locCodeMap =[];
        var locationsList = [];
        var action = component.get("c.getRailLocations");
        action.setParams({
            searchKeyWord:getInputkeyWord,
            countryName:component.get("v.countrySelected")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if (responseData.length == 0) {                                        
                    component.set("v.Message", 'No Result Found...');
                } else {
                    for(var i in responseData){
                        var displayValue = responseData[i].City__c+' ('+responseData[i].Name+')';
                        locCodeMap.push({key:displayValue , value:responseData[i].Name});
                        locationsList.push(responseData[i].City__c+' ('+responseData[i].Name+')');
                    }
                    component.set("v.Message", 'Search Result...');
                }
                component.set("v.objectToParent",locCodeMap);
                component.set("v.listOfSearchRecords", locationsList);
            }else if(state === "ERROR"){
                console.log("Error in fetchFCLPorts !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);         
    },
    
      findARouteSearchCountry : function (component, event, getInputkeyWord){  
    
        
        /*var countriesList = [];
        var countries = (component.get("v.listOfSearchRecordsFromParent"))[0];
        if(countries !== undefined){
            for(var country of countries.values()){
                var index = country.toUpperCase().indexOf(getInputkeyWord.toUpperCase());
                if(index !== -1){
                    countriesList.push(country);
                } 
            }}
            if (countriesList===null) {
                component.set("v.Message", 'No Result Found...');
            } 
            else {
                component.set("v.Message", 'Search Result...');
            }
            component.set("v.listOfSearchRecords", countriesList);
            component.set("v.loading", false);
        }*/
        
       
        var countriesList = [];
        var action = component.get("c.getCountries");
        action.setParams({
            searchKeyWord:getInputkeyWord,
            type:component.get("v.locType")
        });
          console.log(component.get("v.locType"));
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS"){
               
                var responseData = response.getReturnValue();
                 console.log('responseData '+responseData);
                if (responseData.length == 0) {                                        
                    component.set("v.Message", 'No Result Found...');
                } else {
                    for(var i in responseData){
                        
                        countriesList.push(responseData[i].Country_Name__c);
                    }
                    component.set("v.Message", 'Search Result...');
                }
               
                component.set("v.listOfSearchRecords", countriesList);
            }else if(state === "ERROR"){
                console.log("Error in findARouteSearchCountry !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);  } 
})