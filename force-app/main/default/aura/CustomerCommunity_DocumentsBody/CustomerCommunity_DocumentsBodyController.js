({
	doInit: function (component,event,helper) {
		var userId = $A.get("$SObjectType.CurrentUser.Id");
		component.set("v.userId", userId);
		helper.doInitHandler(component, event, helper);
        helper.getExcelFieldLabelList(component);
        helper.getExcelFieldValidationList(component);
        var bookingNumber = component.get("v.bookingNumber");
        if(typeof bookingNumber !== "undefined"){
            component.set("v.bookingId", bookingNumber);
            helper.getBookingId(component,event,helper);            
            component.set("v.showFieldErrorMsg",false);
        }
	},
    createShippingRecord : function(component,event,helper){
        console.log('---'+event.keyCode);
        //if(event.which === 13){
        //if(component.find('booking_Id').get('v.value').length>0){
            helper.getBookingId(component,event,helper);            
            component.set("v.showFieldErrorMsg",false);
            //alert(component.get("v.bookingRecordId"));
        //}else{
            //component.set("v.disableUploadButton", true);
        //}
    },
    createFileChange : function(component,event,helper){
        console.log('---------');      
    },
	handleActiveAllDocs: function (component, event, helper) {
		component.set("v.isUploadedVisible", true);
		component.set("v.isDeleteVisible", false);
		helper.doInitHandler(component, event,helper);
		$A.util.addClass(component.find('alldocsbtn'), 'btn-blue');
		$A.util.removeClass(component.find('mydocsbtn'), 'btn-blue');
	},
	handleActiveMyDocs: function (component, event, helper) {
		var documents = component.get("v.documentRecords");
		var currentUserId = component.get("v.userId");
		component.set("v.isDeleteVisible", true);
		var currentUserDocuments = [];
		for (var i = 0; i < documents.length; i++) {
			if (documents[i]['CreatedById'] === currentUserId) {
				currentUserDocuments.push(documents[i]);
			}
		}
		component.set("v.documentRecords", currentUserDocuments);
		helper.buildData(component, helper);
		component.set("v.isUploadedVisible", false);
		$A.util.addClass(component.find('mydocsbtn'), 'btn-blue');
		$A.util.removeClass(component.find('alldocsbtn'), 'btn-blue');
	},
    handleUploadFinished: function (component, event,helper) {
        alert("Your Document has been Uploaded Successfully !");
 	    component.set("v.bookingId","");
        component.set("v.disableUploadButton",true);
        helper.doInitHandler(component,event,helper);
  
    },
    handleExcelUpload : function(component, event, helper){
        helper.importTable(component, event,helper);
        component.set("v.bookingId","");
        component.set("v.disableUploadButton",true);
        helper.doInitHandler(component,event,helper);
    },
	clearValues: function (component, event, helper) {
		component.set("v.bookingId", null);
		component.set("v.containerId", null);
		component.set("v.fileName", null);
		component.set("v.isErrorVisible", false);
		component.set("v.ErrorMsg", null);
		component.set("v.showFieldErrorMsg", false);
	},
	viewDocument: function (component, event, helper) {
		var indexId = event.currentTarget.getAttribute("data-Id");
		console.log(indexId);
		helper.viewDocumentHelper(component, event, indexId);
	},
	closeModel: function (component, event, helper) {
		component.set("v.hasModalOpen", false);
		component.set("v.contentId", null);
	},
	onNext: function (component, event, helper) {
		var pageNumber = component.get("v.currentPageNumber");
		component.set("v.currentPageNumber", pageNumber + 1);
		helper.buildData(component, helper);
	},
	onPrev: function (component, event, helper) {
		var pageNumber = component.get("v.currentPageNumber");
		component.set("v.currentPageNumber", pageNumber - 1);
		helper.buildData(component, helper);
	},
	processMe: function (component, event, helper) {
		component.set("v.currentPageNumber", parseInt(event.target.name));
		helper.buildData(component, helper);
	},
	onFirst: function (component, event, helper) {
		component.set("v.currentPageNumber", 1);
		helper.buildData(component, helper);
	},
	onLast: function (component, event, helper) {
		component.set("v.currentPageNumber", component.get("v.totalPages"));
		helper.buildData(component, helper);
	},
	deleteDocument: function (component, event, helper) {
		var indexId = event.currentTarget.getAttribute("data-Id");
		var documentType = event.currentTarget.getAttribute("data-value");
        console.log(indexId);
		helper.deleteDocumentHelper(component, event, indexId, documentType);
	}
})