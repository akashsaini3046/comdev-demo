({
    next : function(component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction" : "next"
        });
        bookingRecordIdEvent.fire();
    },
})