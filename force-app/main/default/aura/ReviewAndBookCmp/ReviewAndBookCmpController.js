({
    doInit : function(component, event, helper) {
        
        helper.loadValues(component, event, helper);

    },
    submit : function(component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction" : "submit"
        });
        bookingRecordIdEvent.fire();

    },
    completeLater : function(component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction" : "completeLater"
        });
        bookingRecordIdEvent.fire();

    },
})