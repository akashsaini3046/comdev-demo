({
	doInit : function(component, event, helper) {
		$A.createComponent(
            "c:CustomerCommunity_DashboardScreen_1",{},
            function(newcomponent){
                if (component.isValid()) {
                     component.set("v.body",[]);
                    var body = component.get("v.body");
                    body.push(newcomponent);
                   
                    component.set("v.body", body);             
                }
            }            
        );
	},
    changeComponentBody: function(component, event, helper){
		var updatedExp = event.getParam("selectedMenu");
        updatedExp = "c:"+updatedExp;
        $A.createComponent(
            updatedExp,{},
            function(newcomponent){
                if (component.isValid()) {
                    component.set("v.body",[]);
                    var body = component.get("v.body");
                    body.push(newcomponent);
                    
                    component.set("v.body", body);             
                }
            }            
        );
	}
})