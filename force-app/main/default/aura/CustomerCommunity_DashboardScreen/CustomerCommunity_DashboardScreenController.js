({
	doInit : function(component, event, helper) {
       // helper.fireHighlightEvent(component, event);
	},
    changeVisibility : function(component, event, helper) {
        var isVisible = event.getParam("Visibility");
        component.set("v.IsLevel2Customer",isVisible);
    },
    updateHomeScreenView : function(component, event, helper) {
    	helper.switchHomeScreen(component, event);
	}
})