({
	fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
    fetchArticles : function(component) {
        var action = component.get("c.getArticles");
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(response.getReturnValue()!=null){
                component.set("v.fullArticleList",response.getReturnValue());
                var knowledgeList = component.get("v.fullArticleList");
                
                var pgSize = component.get("v.PageSize");
                if(knowledgeList.length <= pgSize) 
                    component.set("v.articleList",knowledgeList);
                else 
                    component.set("v.articleList",knowledgeList.slice(0,pgSize));
                component.set("v.PageNumber",1); 
                component.set("v.TotalPages",Math.ceil(knowledgeList.length/pgSize)); 
                component.set("v.TotalRecords",knowledgeList.length);
            }
            else{
                component.set("v.fullArticleList",null); 
                component.set("v.articleList",null);
                component.set("v.showErrorMessage",True);
            } 
        });
        $A.enqueueAction(action);
    },
    getSearchResults : function(component, event) {
        var keyword = component.find("inputtext").get("v.value").trim().toLowerCase();
        var fullArticleLists = component.get("v.fullArticleList");
        var matchingArticles = [];
        var pgSize = component.get("v.PageSize");
        for(var i=0; i<fullArticleLists.length; i++) {
            var testString1 = fullArticleLists[i].Title;
            var testString2 = fullArticleLists[i].Summary;
            testString1 = testString1.toLowerCase();
            testString2 = testString2.toLowerCase();
            if(testString1.includes(keyword) || testString2.includes(keyword)) {
                matchingArticles.push(fullArticleLists[i]);
            }
        }
        if(matchingArticles.length <= pgSize)
            component.set("v.articleList",matchingArticles);
        else 
            component.set("v.articleList",matchingArticles.slice(0,pgSize));
        component.set("v.PageNumber",1); 
        component.set("v.TotalPages",Math.ceil(matchingArticles.length/pgSize));
        component.set("v.TotalRecords",matchingArticles.length);  
    },
    gotoNextPage : function(component){
        var pgNumber = component.get("v.PageNumber");
        var pgSize = component.get("v.PageSize");
        var totalPgs = component.get("v.TotalPages");
        var fullArticleLists = component.get("v.fullArticleList");
        component.set("v.articleList",null);
        if(fullArticleLists.length >= pgSize*(pgNumber+1))
        	component.set("v.articleList",fullArticleLists.slice(pgNumber*pgSize,pgSize*(pgNumber+1)));
        else
            component.set("v.articleList",fullArticleLists.slice(pgNumber*pgSize,fullArticleLists.length));
        component.set("v.PageNumber",pgNumber+1);
    },
    gotoPreviousPage : function(component){
        var pgNumber = component.get("v.PageNumber");
        var pgSize = component.get("v.PageSize");
        var totalPgs = component.get("v.TotalPages");
        var fullArticleLists = component.get("v.fullArticleList");
        component.set("v.articleList",null);
        component.set("v.articleList",fullArticleLists.slice((pgNumber-2)*pgSize,pgSize*(pgNumber-1)));
        component.set("v.PageNumber",pgNumber-1);
    }
})