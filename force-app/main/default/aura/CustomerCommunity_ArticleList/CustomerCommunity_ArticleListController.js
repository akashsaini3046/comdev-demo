({
	doInit : function(component, event, helper) {
        helper.fireHighlightEvent(component, event);
        helper.fetchArticles(component);
	},
    searchArticle : function(component, event, helper) {
        helper.getSearchResults(component, event);
    },
    nextPage : function(component, event, helper) {
        helper.gotoNextPage(component);
    },
    previousPage : function(component, event, helper) {
        helper.gotoPreviousPage(component);
    }
})