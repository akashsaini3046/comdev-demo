({
    createBlankBooking : function(component, event){ 
        var action = component.get("c.createBlankBookingRecord");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.bookingRecordValues",responseData);
                this.fetchContactDetails(component, event);
                if(window.location.search.substr(1).split('&')[1] == 'param=book'){
                    this.getRecordBooking(component);
                }
            } 
        });
        $A.enqueueAction(action);
    },
    getRecordBooking: function(component) {
        var bookingRec = {};
        var action = component.get("c.retriveCacheRecord");
        var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
        action.setParams({
            userId : currentUserId 
        });  
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var res = response.getReturnValue().split("####");	
                var quoteRateId = res[1];	
                component.set( "v.quoteRateId", quoteRateId);	
                var responseData = res[0];	
                console.log('responseData --Cached->'+responseData);	
                var parsedResponse = JSON.parse(responseData);	
                var containerMode = parsedResponse['ContainerMode'];	
                console.log(bookingRec);	
                bookingRec.listCargoDetails = [];	
                if(typeof parsedResponse['cargodetailWrapperList'] !== "undefined" && parsedResponse['cargodetailWrapperList'] !== null){	
                    for(var i = 0 ; i < parsedResponse['cargodetailWrapperList'].length ; i ++){	
                        var cragoDetails = {};	
                        cragoDetails.isHazardous = parsedResponse['cargodetailWrapperList'][i]['isHazardous'];	
                        cragoDetails.requirementRecord = parsedResponse['cargodetailWrapperList'][i]['requirementRecord'];	
                        cragoDetails.sequenceId = parsedResponse['cargodetailWrapperList'][i]['sequenceId'];	
                        bookingRec.listCargoDetails[i] = cragoDetails;	
                    }	
                }else{	
                    var cragoDetails = {};	
                    cragoDetails.isHazardous = false;	
                    cragoDetails.listCommodityRecords = [];	
                    cragoDetails.sequenceId = 1;	
                    bookingRec.listCargoDetails[0] = cragoDetails;	
                }
                bookingRec.listPackingLineRecords = null;
                bookingRec.consigneeId = parsedResponse['consigneeId'];
                bookingRec.shipperId = parsedResponse['shipperId'];
                bookingRec.customerContactId = parsedResponse['customerId'];
                bookingRec.receiptTermVal = parsedResponse['receiptTermVal'];
                bookingRec.shipperContactId = parsedResponse['shipperContactId'];
                bookingRec.consigneeContactId = parsedResponse['consigneeContactId'];
                bookingRec.customerContactId = parsedResponse['customerContactId'];
                console.log(parsedResponse['shipperId']);
                component.find("shipperId1").set('v.value',parsedResponse['shipperId']);
                component.find("consigneeId1").set('v.value',parsedResponse['consigneeId']);
                
                component.find('customerContact').set('v.value',parsedResponse['customerContactRecord']['Id']);
                component.find('shipperContact').set('v.value',parsedResponse['shipperContactRecord']['Id']);
                component.find('consigneeContact').set('v.value',parsedResponse['consigneeContactRecord']['Id']);
                component.find('customerAddressRecordId').set("v.value",parsedResponse['customerAddressRecord']['Name']);
                component.find('shipperAddressRecordId').set("v.value",parsedResponse['shipperAddressRecord']['Name']);
                component.find('consigneeAddressRecordId').set("v.value",parsedResponse['consigneeAddressRecord']['Name']);
                
                component.set("v.disableOriginCode",false);
                component.set("v.originCodeValue",parsedResponse['originCode']);   
                bookingRec.deliveryTermVal = parsedResponse['deliveryTermVal'];
                bookingRec.estSailingDate = parsedResponse['estSailingDate'];  
                component.set("v.disableDestinationCode",false);
                component.set("v.destinationCodeValue",parsedResponse['destinationCode']);
                console.log(parsedResponse['ContainerMode']);
                component.find('bookingType').set('v.value',parsedResponse['ContainerMode']);
                if(parsedResponse['ContainerMode'] == 'LCL'){
                    component.find('bookingType').set('v.value',parsedResponse['ContainerMode']);	
                    bookingRec.totalVolume = parsedResponse['quoteRecord']['Total_Volume__c'];	
                    bookingRec.totalWeight = parsedResponse['quoteRecord']['Total_Weight__c'];
                    /*
                    bookingRec.totalVolume = parsedResponse['totalVolume'];
                    bookingRec.totalWeight = parsedResponse['totalWeight'];    
                }
                if(parsedResponse['ContainerMode'] == 'FCL'){
                    var commidityRecordsFromQuoteList = parsedResponse['commodityRecords'];
                    for (var i = 0; i < commidityRecordsFromQuoteList.length; i++) {
                        commidityRecordsFromQuoteList[i].freightRecord = {};
                    }
                    bookingRec.listCargoDetails = commidityRecordsFromQuoteList;*/
                }
                console.log(component.find('bookingType').get('v.value'));
                component.set("v.bookingRecordValues",bookingRec);
                this.getShipperRecords(component,event, parsedResponse);
                this.getConsigneeRecords(component,event, parsedResponse); 
                this.getOriginLocation(component,event);
                this.getDestinationLocation(component,event);
                console.log(JSON.stringify(component.get("v.bookingRecordValues")));
                component.set("v.quoteRecord", parsedResponse['quoteRecord']);
                //this.resetRecordListHelper(component, event);
            }
        });
        $A.enqueueAction(action); 	 
    },
    resetRecordListHelper : function (component, event){
        var bookingRec = component.get("v.bookingRecordValues");
        var newCargoArr = [];
        bookingRec.listCargoDetails[0].isHazardous = false;
        bookingRec.listCargoDetails[0].listCommodityRecords = [];
        bookingRec.listCargoDetails[0].sequenceId = 1;
        newCargoArr.push(bookingRec.listCargoDetails[0]);
        bookingRec.listCargoDetails = newCargoArr;
        bookingRec.listPackingLineRecords = null;
        component.set("v.bookingRecordValues",bookingRec);
        component.set("v.isIMDGBooking",false);  
    },
    createBlankFreightDetail : function(component){ 
        var bookingRec = component.get("v.bookingRecordValues"); 
        var action = component.get("c.createBlankFreightRecord");
        var bookingRecString = JSON.stringify(bookingRec);
        action.setParams({ bookingRecordString :bookingRecString});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.bookingRecordValues",responseData);
            }
        });
        $A.enqueueAction(action);
    },
    createBlankCommodityList : function(component, event){ 
        var bookingRec = component.get("v.bookingRecordValues"); 
        var elementId = event.currentTarget.id;
        var action = component.get("c.createCommodityRecordList");
        var bookingRecString = JSON.stringify(bookingRec);
        action.setParams({ bookingRecordString :bookingRecString,
                          sequenceNumber : elementId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.bookingRecordValues",responseData);
                component.set("v.isIMDGBooking",true);
            }
        });
        $A.enqueueAction(action);
    },
    createPackingLineList : function(component, event){
        var bookingRec = component.get("v.bookingRecordValues");
        var action = component.get("c.createPackingLineRecords");
        var bookingRecString = JSON.stringify(bookingRec);
        action.setParams({ bookingRecordString :bookingRecString});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.bookingRecordValues",responseData);
            }
        });
        $A.enqueueAction(action);
    },
    fetchContactDetails : function(component, event){
        var action = component.get("c.getContactDetails");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.contactRecordDetail",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
                this.populateCustomerRecord(component, event);
            }
            else{
                component.set("v.contactRecordDetail",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Contact Details could not be fetched. Please try again.");
            } 
        });
        $A.enqueueAction(action);
    },
    populateCustomerRecord : function(component, event){
        var contactDetails = component.get("v.contactRecordDetail");
        if(contactDetails != null){ 
            var action = component.get("c.getCustomerCVIF");
            action.setParams({ contactRecord :contactDetails});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    document.getElementById("customerId").value = responseData;
                    this.getCustomerRecords(component, event, responseData);
                    component.set("v.showErrorMessage",false);
                    component.set("v.errorMessage","");
                }
                else{
                    component.set("v.showErrorMessage",true);
                    component.set("v.errorMessage","Customer Details could not be fetched automatically. Please enter the Customer Details.");
                } 
            });
            $A.enqueueAction(action);
        }
    },
    fetchCFSLocations : function(component, event){
        var action = component.get("c.getCFSLocations");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.cfsLocations",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.cfsLocations",null);
            } 
        });
        $A.enqueueAction(action);
    },
    fetchPackageTypeRecords : function(component, event){
        var action = component.get("c.fetchPackageTypeList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.PackageTypePicklist",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.PackageTypePicklist",null);
            } 
        });
        $A.enqueueAction(action);
    },
    getCustomerRecords : function(component, event, responseData){
        var custRec = document.getElementById("customerId").value;
        var action = component.get("c.fetchContactsAndAddresses");
        var bookingRec = component.get("v.bookingRecordValues"); 
        var autofillFields = component.get("v.autoFillCustomerFields");
        action.setParams({ partyCode :custRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                if(responseData != null){
                	component.set("v.EnableCustomerRecords",true);
                    component.set("v.CustomerContactRecords",responseData);
                }
                
                for(var i=0; i<responseData.length; i++){
                    var contactRecord = component.get("v.contactRecordDetail");
                    if(responseData[i].ContactRecord.Id == contactRecord.Id){
                        bookingRec.customerAddressRecord = responseData[i].AddressRecord;
                        bookingRec.customerContactRecord = responseData[i].ContactRecord;
                        bookingRec.customerContactId = contactRecord.Id;
                    }
                }
                component.set("v.bookingRecordValues", bookingRec);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                bookingRec.customerAddressRecord = null;
                bookingRec.customerContactRecord = null;
                bookingRec.customerContactId = '';
                component.set("v.bookingRecordValues", bookingRec);
                component.set("v.EnableCustomerRecords",false);
                component.set("v.CustomerContactRecords",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered code not found. Please re-check the Customer Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    getShipperRecords : function(component, event, parsedResponse){
        if (typeof parsedResponse === 'undefined') {
            var custRec = document.getElementById("shipperId").value;
        }else{
            var custRec = parsedResponse['shipperId'];
        }
        var action = component.get("c.fetchContactsAndAddresses");
        var bookingRec = component.get("v.bookingRecordValues"); 
        action.setParams({ partyCode :custRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                if(responseData != null){
                	component.set("v.EnableShipperRecords",true);
                    component.set("v.ShipperContactRecords",responseData);
                    if(typeof parsedResponse !== 'undefined'){
                        var partylist = responseData;
                        for(var i=0; i<partylist.length; i++){
                            if(partylist[i].ContactRecord.Id == bookingRec.shipperContactId){
                                bookingRec.shipperAddressRecord = partylist[i].AddressRecord;
                                bookingRec.shipperContactRecord = partylist[i].ContactRecord;
                            }
                        }
                    }
                }
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                bookingRec.shipperAddressRecord = null;
                bookingRec.shipperContactRecord = null;
                bookingRec.shipperContactId = '';
                component.set("v.bookingRecordValues", bookingRec);
                component.set("v.EnableShipperRecords",false);
                component.set("v.ShipperContactRecords",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered code not found. Please re-check the Shipper Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    getConsigneeRecords : function(component, event, parsedResponse){
        if (typeof parsedResponse === 'undefined') {
            var custRec = document.getElementById("consigneeId").value;
        }else{
            var custRec = parsedResponse['consigneeId'];
        }
        var action = component.get("c.fetchContactsAndAddresses");
        var bookingRec = component.get("v.bookingRecordValues"); 
        action.setParams({ partyCode :custRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                if(responseData != null){
                	component.set("v.EnableConsigneeRecords",true);
                    component.set("v.ConsigneeContactRecords",responseData);
                    if(typeof parsedResponse !== 'undefined'){
                        var partylist = responseData;
                        for(var i=0; i<partylist.length; i++){
                            if(partylist[i].ContactRecord.Id == bookingRec.consigneeContactId){
                                bookingRec.consigneeAddressRecord = partylist[i].AddressRecord;
                                bookingRec.consigneeContactRecord = partylist[i].ContactRecord;
                            }
                        }
                    }
                }
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                bookingRec.consigneeAddressRecord = null;
                bookingRec.consigneeContactRecord = null;
                bookingRec.consigneeContactId = '';
                component.set("v.bookingRecordValues", bookingRec);
                component.set("v.EnableConsigneeRecords",false);
                component.set("v.ConsigneeContactRecords",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered code not found. Please re-check the Consignee Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    fetchIncoTerms : function(component){ 
        var action = component.get("c.fetchIncoTermList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.IncoTermsList",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    fetchReceiptDeliveryTerms : function(component){
        var action = component.get("c.fetchReceiptDeliveryTermsList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.ReceiptDeliveryTermsList",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    fetchCommodityRecords : function(component){
        var action = component.get("c.fetchCommodityList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.CommodityPicklist",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    fetchContainerTypeRecords : function(component){
        var action = component.get("c.fetchContainerTypeList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.ContainerList",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    getOriginLocationDetails : function(component, event){
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var originCode = component.get("v.originCodeValue");
        var action = component.get("c.getLocationDetails");
        action.setParams({ termCode :recTerm,
                          locCode :originCode});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.OriginLocation",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.OriginLocation","");
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered Origin code not found. Please re-check the Origin Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    getDestinationLocationDetails : function(component, event){
        var delTerm = component.find("DeliveryTerm").get("v.value");
        var destinationCode = component.get("v.destinationCodeValue");
        var action = component.get("c.getLocationDetails");
        action.setParams({ termCode :delTerm,
                          locCode :destinationCode});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.destinationLocation",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.destinationLocation","");
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered Destination code not found. Please re-check the Destination Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    updateAddressField : function(component, event){
        var partyType = event.getSource().getLocalId();
        var bookingRec = component.get("v.bookingRecordValues"); 
        if(partyType == "shipperContact"){
            var partylist = component.get("v.ShipperContactRecords");
            for(var i=0; i<partylist.length; i++){
                if(partylist[i].ContactRecord.Id == bookingRec.shipperContactId){
                    bookingRec.shipperAddressRecord = partylist[i].AddressRecord;
                    bookingRec.shipperContactRecord = partylist[i].ContactRecord;
                }
            }
        }
        if(partyType == "customerContact"){
            var partylist = component.get("v.CustomerContactRecords");
            for(var i=0; i<partylist.length; i++){
                if(partylist[i].ContactRecord.Id == bookingRec.customerContactId){
                    bookingRec.customerAddressRecord = partylist[i].AddressRecord;
                    bookingRec.customerContactRecord = partylist[i].ContactRecord;
                }
            }
        }
        if(partyType == "consigneeContact"){
            var partylist = component.get("v.ConsigneeContactRecords");
            for(var i=0; i<partylist.length; i++){
                if(partylist[i].ContactRecord.Id == bookingRec.consigneeContactId){
                    bookingRec.consigneeAddressRecord = partylist[i].AddressRecord;
                    bookingRec.consigneeContactRecord = partylist[i].ContactRecord;
                }
            }
        }
        component.set("v.bookingRecordValues", bookingRec);
    },
    checkValidations : function(component,event){
      
       var validationArray = [];
       var lclValid=true,fclValid=true,ArrayValid=true,imgdValid=true;
        
       var listBookingRecords = component.get("v.bookingRecordValues");
       var ReceiptTerm = component.find('ReceiptTerm');
       var ReadyDate = component.find('ReadyDate');
       var DeliveryTerm = component.find('DeliveryTerm');
       var IncoTerms = component.find("IncoTerms");
       var isHazBooking = component.get("v.isIMDGBooking");
             
        validationArray.push(ReceiptTerm);
        validationArray.push(ReadyDate);
        validationArray.push(DeliveryTerm);
        validationArray.push(IncoTerms);
        
        var picklistValue = component.find('bookingType').get("v.value");
     
        if(picklistValue == "LCL"){
            lclValid = component.find('cargoDimension').reduce(function (validFields, inputCmp) {	   
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
          }, true);
        }
        
        // For all list elements that have been on click of + arrow
        if(listBookingRecords.listCargoDetails.length >= 1 &&  picklistValue == "FCL"){
            fclValid = component.find('FormValPart').reduce(function (validFields, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validFields && inputCmp.get('v.validity').valid;
            }, true);
        }

        // For main elements that are being added using validationArray
          ArrayValid = validationArray.reduce(function (validFields, inputCmp) {	  
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        
      
        // For all IMDG Elements that are added on click of the checkbox
        var isDocUpload = component.get("v.isDocUpload");
        if(isHazBooking && !isDocUpload){
            imgdValid = component.find('FormVal').reduce(function (validFields, inputCmp) {	
                console.log(validFields);    
                inputCmp.showHelpMessageIfInvalid();
                return validFields && inputCmp.get('v.validity').valid;
            }, true);
        }
        if(lclValid && fclValid && ArrayValid &&imgdValid)
            return true;
        else
            return false;
    },
    createBookingRecord : function(component, event) {
        var isFormValid = this.checkValidations(component,event);
        if(isFormValid){
            var bookRecValues = component.get("v.bookingRecordValues");
            var contactDetails = component.get("v.contactRecordDetail");
            var bookingRequestRec = component.get("v.bookingRequestRecord");
            if(contactDetails != null)
                bookRecValues.contactRecord = contactDetails;
            var custId = document.getElementById("customerId").value;
            bookRecValues.customerId = custId;
            var shpId = document.getElementById("shipperId").value;
            bookRecValues.shipperId = shpId;
            var conId = document.getElementById("consigneeId").value;
            bookRecValues.consigneeId = conId;
            var originCodeVal = component.get("v.originCodeValue");
            bookRecValues.originCode = originCodeVal;
            var destinationCodeVal = component.get("v.destinationCodeValue");
            bookRecValues.destinationCode = destinationCodeVal;
            var isHazBooking = component.get("v.isIMDGBooking");
            bookRecValues.bookingType = component.get("v.bookingType");
            
            var action = component.get("c.createBookingRecord"); 
            console.log(JSON.stringify(bookRecValues));
            console.log(JSON.stringify(bookingRequestRec));
            var bookingRecValuesString = JSON.stringify(bookRecValues);   
            action.setParams({ bookingRecValuesString :bookingRecValuesString,
                              bookingRequestRecord : bookingRequestRec});
            console.log('bookingRecValuesString->'+bookingRecValuesString)
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    console.log('responseData--:'+JSON.stringify(response.getReturnValue()));
                    var responseData = response.getReturnValue();
                    component.set("v.bookingRequestRecord",responseData);
                    bookRecValues.bookingRequestNumber = responseData.Name;
                    component.set("v.bookingRecordValues",bookRecValues);
                    component.set("v.showErrorMessage",false);
                    component.set("v.errorMessage","");
                    var isDocUpload = component.get("v.isDocUpload");
                    if(isHazBooking && !isDocUpload){
                        this.getHazCheckerResponse(component, event);
                    }else if(isHazBooking && isDocUpload){
                        component.set("v.isBookingModalBoxOpen", true);
                        component.set("v.isBookingDocumentUpload", true);
                    }else{
                        this.getBookingNumber(component, event);
                    }
                }
                else{
                    component.set("v.showErrorMessage",true);
                    console.log("create");
                    component.set("v.errorMessage","We encounterd some error while processing your request. Please try again.");
                } 
            });
            $A.enqueueAction(action);
         }
    },
    getHazCheckerResponse : function(component, event){
        var bookingRec = component.get("v.bookingRequestRecord");
        var bookRecValues = component.get("v.bookingRecordValues");
        var bookingRecValuesString = JSON.stringify(bookRecValues);
        console.log('bookingRecValuesString --->:'+bookingRecValuesString);
        if(bookingRec != null){
            var action = component.get("c.validateHazardousBooking"); 
            action.setParams({ bookingId : bookingRec.Id,
                              bookingRecValuesString : bookingRecValuesString});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    console.log('responseData--:'+JSON.stringify(responseData));
                    if(responseData == 'True'){
                        component.set("v.HazValidationResponse", true);
                        this.getBookingNumber(component, event);
                    }
                    else{
                    	component.set("v.HazValidationResponse", false);
                        component.set("v.HazValidationMessage", responseData);
                        component.set("v.isHazModalBoxOpen", true);
                    }
                }
                else{
                    component.set("v.HazValidationResponse", false);
                    component.set("v.isHazModalBoxOpen", true);
                } 
            });
            $A.enqueueAction(action);
        }
    },
    getBookingNumber : function(component, event){
        var bookingRec = component.get("v.bookingRequestRecord");
        var bookingMode = component.get("v.bookingType");
        var quote = component.get("v.quoteRecord");	
        var quoteRateId = component.get("v.quoteRateId");
        var isHazBooking = component.get("v.isIMDGBooking");        
        if(bookingRec != null){
            var action = component.get("c.getCICSBookingNumber"); 
            action.setParams({ 	
                bookingId : bookingRec.Id,	
                quoteId : (quote != null ? quote.Id : null),	
                quoteRateId : (quoteRateId != null ? quoteRateId : null)	
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    component.set("v.bookingRequestRecord", responseData);
                    component.set("v.isBookingConfirmed", true);
                }
                else{
                    component.set("v.isBookingConfirmed", false);
                }
                if(bookingMode != null && bookingMode == "FCL"){	
                    this.getSoftshipRating(component, event);	
                } else {
                    console.log(JSON.stringify(component.get("v.quoteRecord")));
                    component.set("v.isQuoteBooked", true);	
                    component.set("v.isBookingModalBoxOpen", true);	
                } 
            });
            $A.enqueueAction(action);
        }
    },
       
    getSoftshipRating : function(component, event){
        var bookRecValues = component.get("v.bookingRecordValues");
        var action = component.get("c.getSoftshipRates"); 
        var bookingRecValuesString = JSON.stringify(bookRecValues); 
        console.log(bookingRecValuesString);
        action.setParams({ bookingRecValuesString :bookingRecValuesString, sourceType : "NewBooking"});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                console.log("@@@ responseData - " + responseData);
                if(responseData != null && responseData != "")
                	component.set("v.softshipRates", responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
                component.set("v.isBookingModalBoxOpen", true);
                component.set("v.isQuoteBooked", true);
            }
            else{
                component.set("v.showErrorMessage",true);
                console.log("shoftship");
                component.set("v.errorMessage","We encounterd some error while getting rates. Please try again.");
                component.set("v.isBookingModalBoxOpen", true);
            } 
        });
        $A.enqueueAction(action);
    },
    updateMeasurmentUnits : function(component, event){
        var measurmentUnit = document.getElementById("measurmentUnit").checked;
        var bookingRec = component.get("v.bookingRecordValues");
        if(measurmentUnit){
            bookingRec.totalVolumeUnit = "M3";
            bookingRec.totalWeightUnit = "KG";
            if(bookingRec.listPackingLineRecords != null && bookingRec.listPackingLineRecords.length > 0){
                for(var i=0;i<bookingRec.listPackingLineRecords.length;i++){
                    bookingRec.listPackingLineRecords[i].Length_Unit__c = 'meter';
                    bookingRec.listPackingLineRecords[i].Weight_Unit__c = 'KG';
                    bookingRec.listPackingLineRecords[i].Volume_Unit__c = 'M3';
                }
            }
        }
        else{
            bookingRec.totalVolumeUnit = "CF";
            bookingRec.totalWeightUnit = "LB";
            if(bookingRec.listPackingLineRecords != null && bookingRec.listPackingLineRecords.length > 0){
                for(var i=0;i<bookingRec.listPackingLineRecords.length;i++){
                    bookingRec.listPackingLineRecords[i].Length_Unit__c = 'feet';
                    bookingRec.listPackingLineRecords[i].Weight_Unit__c = 'LB';
                    bookingRec.listPackingLineRecords[i].Volume_Unit__c = 'CF';
                }
            }
        }
        component.set("v.bookingRecordValues",bookingRec);
    },
    formatDate : function(component){
        var currentDate = new Date();
        var minDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()), "YYYY-MM-DD");
        var maxDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()+365), "YYYY-MM-DD");
        component.set('v.minDate', minDate);
        component.set("v.maxDate", maxDate);
    },
    getOriginLocation : function(component, event){
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var originCode = component.get("v.originCodeValue");
        var cfsPorts = component.get("v.cfsLocations");
        if(recTerm == ''){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please select a Receipt Term and then enter Origin Code");
            component.set("v.OriginLocation","");
        }
        else if(originCode == '' || originCode == null){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please enter Origin Zip Code");
            component.set("v.OriginLocation","");
        }
        else if(recTerm == 'CFS'){
        	component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            var originLoc = '';
            for(var i=0;i<cfsPorts.length; i++){
                if(cfsPorts[i].Name == originCode){
                    if(cfsPorts[i].Street__c != '' && cfsPorts[i].Street__c != null)
                        originLoc = originLoc + cfsPorts[i].Street__c + ' ';
                    if(cfsPorts[i].Name != '' && cfsPorts[i].Name != null)
                        originLoc = originLoc + cfsPorts[i].Name + ' ';
                    if(cfsPorts[i].State_Abbreviation__c != '' && cfsPorts[i].State_Abbreviation__c != null)
                        originLoc = originLoc + cfsPorts[i].State_Abbreviation__c + ' ';
                    if(cfsPorts[i].Zip_Code__c != '' && cfsPorts[i].Zip_Code__c != null)
                        originLoc = originLoc + cfsPorts[i].Zip_Code__c + ' ';
                    if(cfsPorts[i].Country__c != '' && cfsPorts[i].Country__c != null)
                        originLoc = originLoc + cfsPorts[i].Country__c + ' ';
                	component.set("v.OriginLocation",originLoc);    
                }
            }
        }
        else{
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            if(recTerm == 'P' && originCode.length > 4)
                this.getOriginLocationDetails(component, event);
            else if(recTerm == 'D' && originCode.length > 3)
            	this.getOriginLocationDetails(component, event);
            else{
                component.set("v.OriginLocation","");
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered Origin code not found. Please re-check the Origin Code.");
             }
        }
    },
    getDestinationLocation : function(component, event){
        var delTerm = component.find("DeliveryTerm").get("v.value");
        var destinationCode = component.get("v.destinationCodeValue");
        var cfsPorts = component.get("v.cfsLocations");
        if(delTerm == ''){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please select a Delivery Term and then enter Destination Code");
            component.set("v.destinationLocation","");
        }
        else if(destinationCode == '' || destinationCode == null){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please enter Destination Code");
            component.set("v.destinationLocation","");
        }
        else if(delTerm == 'CFS'){
        	component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            var destinationLoc = '';
            for(var i=0;i<cfsPorts.length; i++){
                if(cfsPorts[i].Name == destinationCode){
                    if(cfsPorts[i].Street__c != '' && cfsPorts[i].Street__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Street__c + ' ';
                    if(cfsPorts[i].Name != '' && cfsPorts[i].Name != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Name + ' ';
                    if(cfsPorts[i].State_Abbreviation__c != '' && cfsPorts[i].State_Abbreviation__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].State_Abbreviation__c + ' ';
                    if(cfsPorts[i].Zip_Code__c != '' && cfsPorts[i].Zip_Code__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Zip_Code__c + ' ';
                    if(cfsPorts[i].Country__c != '' && cfsPorts[i].Country__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Country__c + ' ';
                	component.set("v.destinationLocation",destinationLoc);    
                }
            }
        }
        else{
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            if(delTerm == 'P' && destinationCode.length > 4)
                this.getDestinationLocationDetails(component, event);
            else if(delTerm == 'D' && destinationCode.length > 3)
                this.getDestinationLocationDetails(component, event);
                else{
                    component.set("v.destinationLocation","");
                    component.set("v.showErrorMessage",true);
                    component.set("v.errorMessage","Entered Destination code not found. Please re-check the Destination Code.");
                }
        }
    },
    columnSubstances : function(component){
       component.set('v.substanceColumns',[
            {label: 'UN Code', fieldName: 'Name', type: 'text'},
            {label: 'Name', fieldName: 'Substance_Name__c', type: 'text'},
            {label: 'Primary Class', fieldName: 'Primary_Class__c', type: 'text'},
            {label: 'Variation', fieldName: 'Variation__c', type: 'text'},           
            {label: 'Packaging Group', fieldName: 'Packing_Group__c', type: 'text'},
            {label: 'Secondary Class', fieldName: 'Secondary_Class__c', type: 'text'}
        ]);
    },
    searchUNSubstances : function(component, event, helper){
        var code = component.get("v.searchSubsString");
        //alert(code);
        var action = component.get("c.getSubstancesRecord");
        action.setParams({ searchKey :code});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                    alert('No Result Found...');
                } else {
                    component.set("v.Message", 'Search Result...');
                    //alert('Search Result...');
                }
                component.set("v.substanceRecords", storeResponse);
           
            }
        });
        $A.enqueueAction(action);
    },
    getHazardousDoc : function(component, event,bookingRec){
        var action = component.get("c.getHazardousDocument"); 
        action.setParams({bookingId : bookingRec.Id});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseVal = response.getReturnValue();
                //component.set("v.hazardousDoc",response.getReturnValue());
                if(responseVal!=null && responseVal.length>0){   
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                    "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/servlet/servlet.FileDownload?file="+responseVal[0].Id,
                    });
                    urlEvent.fire();
                }else{
                    alert('Document generation is in process. Please try again.');
                }
            }
        });
    	$A.enqueueAction(action);
	},
})