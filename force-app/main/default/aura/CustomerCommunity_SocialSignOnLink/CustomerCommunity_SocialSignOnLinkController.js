({
	doInit: function (component, event, helper) {
        helper.setUserSocialDetails(component);
    },
    
    handlerDisconnect : function (component, event, helper) {
        helper.handlerDisconnect(component, event, helper);
    }
})