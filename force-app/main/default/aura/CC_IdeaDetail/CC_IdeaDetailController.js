({
	doInit : function(cmp, event, helper) {
        console.log('Inside detail init');
        var ideaRec = cmp.get("v.ideaRecord");
        cmp.set("v.ideaId",ideaRec.Id); 
        var ideaId = cmp.get("v.ideaId");
        helper.getCurrentUserAction(cmp, helper, function(){          
            helper.getIdeaDescribeResultAction(cmp, helper);
            helper.getIdeaFieldDescribeResultAction(cmp, helper);  
            helper.getCommunityNetworkName(cmp,helper);
            helper.getIdeaCommentsAction(cmp, helper, ideaId);
            console.log('Idea Id  repeated '+cmp.get("v.ideaId"));
          
        });        
	},
    
    editHandler: function(cmp, event, helper){
        cmp.set("v.displayIdeasList",false);
        cmp.set("v.displayIdeaDetail",false);
        cmp.set("v.displayEditIdea",true);
        cmp.set("v.displayNewIdea",false);        
    },
    
    deleteIdeaHandler: function(cmp, event, helper){
        helper.deleteIdeaAction(cmp, helper);
    },
    
    upvoteIdeaHandler: function(cmp, event, helper){
        var ideaId = cmp.get("v.ideaId");
        helper.upvoteIdeaAction(cmp, helper, ideaId);
    },
    
    downvoteIdeaHandler: function(cmp, event, helper){
        var ideaId = cmp.get("v.ideaId");
        helper.downvoteIdeaAction(cmp, helper, ideaId);
    },
    
    likeCommentHandler: function(cmp, event, helper){
        console.log('likeCommnet -> ', event.getSource().get("v.value"));
        helper.likeCommentAction(cmp, helper, event.getSource().get("v.value"));
    },
    
    unlikeCommentHandler: function(cmp, event, helper){
        console.log('unlikeCommnet -> ', event.getSource().get("v.value"));
        helper.unlikeCommentAction(cmp, helper, event.getSource().get("v.value"));
    },
    
    editCommentHandler: function(cmp, event, helper){
        //console.log('editCommnet -> ', event.getSource().get("v.value"));
      
        var ideaCommentRecordId = event.getSource().get("v.value");
      /*  var ideaCommentList = cmp.get('v.ideaComments');
        var ideaCommentRecord;
        ideaCommentList.map((ideaComment) => {
            if(ideaComment.Id === ideaCommentRecordId){
            	ideaCommentRecord = ideaComment;
        	}
        }); */
        cmp.set("v.ideaCommentId",ideaCommentRecordId);
        console.log('move to '+ideaCommentRecordId);
        //console.log(ideaCommentRecord);
        //cmp.set("v.ideaComment",ideaCommentRecord);
        cmp.set("v.displayEditIdeaComment",true);  
    },
    
    deleteCommentHandler: function(cmp, event, helper){
        helper.deleteCommentAction(cmp, helper, event.getSource().get("v.value"));
    },
    
    addCommentHandler: function(cmp, event, helper){
        helper.addCommentAction(cmp, helper);
    },
    handleAllIdeasOnclick: function(cmp){
        cmp.set("v.displayIdeasList",true);
        cmp.set("v.displayIdeaDetail",false);
        cmp.set("v.displayEditIdea",false);
        cmp.set("v.displayNewIdea",false);       
    },
    handlePostIdea: function(cmp){
        cmp.set("v.displayIdeaDetail",false);
        cmp.set("v.displayNewIdea",true);   
        cmp.set("v.displayIdeasList",false);        
        cmp.set("v.displayEditIdea",false);
            
    },
})