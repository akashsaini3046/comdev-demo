({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', helper.getColumnDefinitions());
        helper.getTotalNumberOfBookings(cmp);
        helper.getDataInDataTable(cmp, 'CreatedDate', 'desc');
    },
    openEmailAddressModal : function(component, event, helper){
        component.set("v.isSendEmail", true);
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        console.log(event.getParam('selectedRows'));
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },
    
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        console.log(row.Id + " : " + action.name + " : " +JSON.stringify(action));
        switch (action.name) {
            case 'view_details':
                helper.showBookingDetails(cmp, row);
                break;
        }
    },
    
    handleLoadMoreBookings: function (component, event, helper) {
        console.log('handleLoadMoreBookings');
        component.set('v.isLoading', true);
        helper.getMoreBookings(component, component.get("v.sortedBy"), component.get("v.sortedDirection"));
    },
    closeBookingDetails : function(cmp, row) {
        cmp.set("v.openModal",false);
    },
    handleGetRates : function(component, event, helper){
        helper.handleGetRates(component, event, helper);  
    },
    handleValidateIMDG : function(component, event, helper){
        helper.handleValidateIMDG(component, event, helper);
    },
    closeModel : function(component,event,helper){
        component.set("v.isSendEmail", false);
    },
    sendEmailWithPDF : function(component,event,helper){
        var inputField = component.find('emailIds');
        var allValid = inputField.get('v.validity').valid;
        if (allValid) {
            helper.sendEmailPDF(component, event, helper);
        } else {
            inputField.set('v.validity', {valid:false, badInput :true});
            inputField.showHelpMessageIfInvalid();
        }
    },
})