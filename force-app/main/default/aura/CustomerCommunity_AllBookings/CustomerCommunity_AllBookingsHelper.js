({
    getDataInDataTable: function (component, fieldName, order) {
        var action = component.get("c.fetchBookingData");
        action.setParams({
            "recordLimit": component.get("v.initialRows"),
            "recordOffset": component.get("v.rowNumberOffset"),
            "fieldName": fieldName,
            "order": order
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bookingDetails = response.getReturnValue();
                bookingDetails.forEach(function(record){
                    record.linkName = '/CrowleyCustomerCommunity/'+record.Id;
                });
                if (bookingDetails != null) {
                    for (var i = 0; i < bookingDetails.length; i++) {
                        var bookingRow = bookingDetails[i];
                        if (bookingRow.Account__c) bookingRow.AccountName = bookingRow.Account__r.Name;
                    }
                    component.set('v.data', bookingDetails);
                    component.set("v.currentCount", component.get("v.initialRows"));
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
    sendEmailPDF : function (component, event, helper){
        var action = component.get("c.sendEmailPDF");
        var emailAddress = component.find('emailIds').get('v.value');
        var bookingId = component.get("v.bookingId");
        component.set("v.showSpinnerModal", true);
        action.setParams({
            bookingId : bookingId,
            emailAddress : emailAddress
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS"){
                toastEvent.setParams({
                    message: 'Email Sent Successfully',
                    type:'success'
                });               
            }else{
                toastEvent.setParams({
                    message: 'Error sending Email',
                    type:'error'
                }); 
            }
            component.set("v.showSpinnerModal", false);
            toastEvent.fire();  
            component.set("v.isSendEmail", false);
        });
        $A.enqueueAction(action);
        console.log("sendEmailPDF Call");
    },
    getTotalNumberOfBookings : function(component) {
        var action = component.get("c.getTotalBookings");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" ) {
                var resultData = response.getReturnValue();
                component.set("v.totalNumberOfRows", resultData);
            }
        });
        $A.enqueueAction(action);
    },
    
    getMoreBookings: function(component, fieldName, order){
        var rows = component.get('v.rowsToLoad');
        var action = component.get("c.fetchBookingData");
        var recordOffset = component.get("v.currentCount");
        var recordLimit = component.get("v.rowsToLoad");
        console.log(recordLimit + " : " + recordOffset + " : " + fieldName + " : " + order);
        action.setParams({
            "recordLimit": recordLimit,
            "recordOffset": recordOffset,
            "fieldName": fieldName,
            "order": order
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bookingDetails = response.getReturnValue();
                bookingDetails.forEach(function(record){
                    record.linkName = '/CrowleyCustomerCommunity/'+record.Id;
                });
                if (bookingDetails != null) {
                    if (component.get('v.data').length !== component.get('v.totalNumberOfRows')) {
                        var currentData = component.get('v.data');
                        var newData = currentData.concat(bookingDetails);
                        component.set('v.data', newData);
                        recordOffset = recordOffset + recordLimit;
                        component.set("v.currentCount", recordOffset);
                    }
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
            console.log("finish");
        });
        $A.enqueueAction(action);
    },
    
    getColumnDefinitions: function () {
        typeAttributes: {label: { fieldName: 'linkLabel' }}
        var columns = [
            {label: 'Booking Number', fieldName: 'Booking_Number__c', type: 'text'},
            {label: 'Booking Request Number', fieldName: 'Name', type: 'text', sortable: true},
            {label: 'Account', fieldName: 'AccountName', type: 'text', sortable: true},
            {label: 'Origin Type', fieldName: 'Origin_Type__c', type: 'text', sortable: true},
            {label: 'Destination Type', fieldName: 'Destination_Type__c', type: 'text', sortable: true},
            {label: 'Status', fieldName: 'Status__c', type: 'text', sortable: true},
            {label: 'Created Date', fieldName: 'CreatedDate', type: 'date', sortable: true},
            //{label: 'Record Id', fieldName: 'Id', type: 'text', sortable: true},
            {label: 'View Bookings', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View Quote Details'}},
            {label: 'Re-Book', type: 'button', initialWidth: 135, typeAttributes: { label: 'Re-Book', name: 're_book', title: 'Re-Book'}}
        ];
        return columns;
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var reverse = sortDirection !== 'asc';
        var action = component.get("c.fetchBookingData");
        action.setParams({
            "recordLimit": component.get("v.initialRows"),
            "recordOffset": component.get("v.rowNumberOffset"),
            fieldName : fieldName,
            order : sortDirection
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bookingDetails = response.getReturnValue();
                bookingDetails.forEach(function(record){
                    record.linkName = '/CrowleyCustomerCommunity/'+record.Id;
                });
                if (bookingDetails != null) {
                    for (var i = 0; i < bookingDetails.length; i++) {
                        var bookingRow = bookingDetails[i];
                        if (bookingRow.Account__c) bookingRow.AccountName = bookingRow.Account__r.Name;
                    }
                    component.set('v.data', bookingDetails);
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
    
    showBookingDetails : function(cmp, row) {
        /*var bookingRecordIdEvent = cmp.getEvent("bookingRecordIdEvent");
         bookingRecordIdEvent.setParams({
            "bookingId" : row.Id,
            "bookingName" : (row.Booking_Number__c !== null && typeof row.Booking_Number__c !== "undefined" ? row.Booking_Number__c : row.Name ),
            "selectedTabId" : "tab-view-details"
        });
        bookingRecordIdEvent.fire();*/
        cmp.set("v.bookingNumber",row.Booking_Number__c);
        cmp.set("v.bookingId",row.Id);
        cmp.set("v.openModal",true);
        var action = cmp.get("c.fetchIframeUrl");
        action.setParams({
            bookingId : row.Id
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.bookingUrl", response.getReturnValue());
                console.log('response.getReturnValue()',response.getReturnValue());
            } else if (state === "ERROR") {
                console.log("error");
                let errors = response.getError();
                let message = '';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                console.error(message);
            }
            cmp.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
    
    handleGetRates : function(component, event, helper){
        console.log('Entered handleGetRates');	       
        console.log('Record id : '+component.get('v.bookingId'));       
        
        if(component.get('v.displayRates')==true)
            component.set('v.displayRates',false);
        else if(component.get('v.displayRates')==false)
            component.set('v.displayRates',true);  
        var action =component.get("c.getRates");
        
        action.setParams({
            IdBooking: component.get('v.bookingId')
        });
        
        console.log('Before setCallback');
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log('start res @@@');
                console.log(responseData);
                console.log('stop res @@@');
                component.set("v.ratingData",responseData);
            }
            else if(state === "ERROR"){
                console.log("Error is : ");
                console.log(response.getError());
            }
        });
        console.log('After setCallback');
        $A.enqueueAction(action);
    }, 
    
    handleValidateIMDG : function(component, event, helper){
        
        this.showSpinner(component);
        console.log('Entered handleValidateIMDG in helper');
        var action= component.get("c.validateIMDG");
        action.setParams({
            IdBooking: component.get('v.bookingId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('State is : '+state)
            if (state === "SUCCESS"){                  
                
                var responseData = response.getReturnValue();
                if(responseData=="true"){
                    
                    this.allAttachments(component, event, helper);
                    
                }
                //$A.get('e.force:refreshView').fire();
                //console.log('Response Data : '+responseData);
                var childCom =component.find("bookingDetailComp");
                childCom.set("v.recordId",component.get('v.bookingId'));
                childCom.reInit();
            }
            else if(state=="ERROR"){
                console.log("Error is : ");
                console.log(response.getError());
            }	
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);   
    },
    
    showSpinner : function(component){
        component.set("v.spinner",true);
    },
    
    hideSpinner : function(component){
        component.set("v.spinner",false);
    }
})