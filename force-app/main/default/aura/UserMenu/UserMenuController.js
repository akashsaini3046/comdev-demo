({
    doInit : function(component, event, helper){
        helper.getUser(component, helper);
    },
    
    handleSelect : function(component, event, helper){
        var selectedMenuItemValue = event.getParam("value");
        if(selectedMenuItemValue == '1')
        	window.location.replace("/secur/logout.jsp?retUrl=%2Flogin");
    },
    
    logout: function(cmp){
        window.location.replace("/secur/logout.jsp?retUrl=%2Flogin");
    }
})