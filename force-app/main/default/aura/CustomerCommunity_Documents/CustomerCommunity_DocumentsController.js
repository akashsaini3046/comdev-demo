({
	doInit: function (component, event, helper) {
        helper.fireHighlightEvent(component, event);
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var bookingNumber = getUrlParameter('bookingNumber');
        var tabId = getUrlParameter('tabId');
        if(typeof bookingNumber !== "undefined" && typeof tabId !== "undefined"){
            component.set("v.bookingNumber", bookingNumber);
            component.set("v.selectedTab", tabId);
        }
	},
	handleActiveVGM: function (component, event, helper) {
		var tabVGM = component.find('tab-VGM');
		$A.createComponent(
			"c:CustomerCommunity_DocumentsBody", {
				"isContainerFieldVisible": true,
				"SelectedDocumentType": "VGM Upload"
			},
			function (CustomerCommunity_DocumentsBody, status, errorMessage) {
				if (status === "SUCCESS") {
					tabVGM.set("v.body", [CustomerCommunity_DocumentsBody]);
				}
                else{
                    console.log('error'+errorMessage);
                }
			}
		);
	},
	handleActiveShipping: function (component, event, helper) {
		var tabShipping = component.find('tab-shipping');
		$A.createComponent(
			"c:CustomerCommunity_DocumentsBody", {
				"isContainerFieldVisible": false,
				"SelectedDocumentType": "Shipping Instructions"
			},
			function (CustomerCommunity_DocumentsBody, status, errorMessage) {
				if (status === "SUCCESS") {
					tabShipping.set("v.body", [CustomerCommunity_DocumentsBody]);
				}
			}
		);
	},
	handleActiveHazardous: function (component, event, helper) {
		var tabHazardous = component.find('tab-hazardous');
        var bookingNumber = component.get("v.bookingNumber");
		$A.createComponent(
			"c:CustomerCommunity_DocumentsBody", {
				isContainerFieldVisible: false,
				SelectedDocumentType: "Hazardous Documentation",
                bookingNumber : bookingNumber
			},
			function (CustomerCommunity_DocumentsBody, status, errorMessage) {
				if (status === "SUCCESS") {
					tabHazardous.set("v.body", [CustomerCommunity_DocumentsBody]);
				}
			}
		);
	},
	handleActivePacking: function (component, event, helper) {
		var tabPacking = component.find('tab-packing');
		$A.createComponent(
			"c:CustomerCommunity_DocumentsBody", {
				isContainerFieldVisible: false,
				SelectedDocumentType: "Packing List"
			},
			function (CustomerCommunity_DocumentsBody, status, errorMessage) {
				if (status === "SUCCESS") {
					tabPacking.set("v.body", [CustomerCommunity_DocumentsBody]);
				}
			}
		);
	},
	handleActiveCommercial: function (component, event, helper) {
		var tabCommercial = component.find('tab-commercial');
		$A.createComponent(
			"c:CustomerCommunity_DocumentsBody", {
				isContainerFieldVisible: false,
				SelectedDocumentType: "Commercial Invoice"
			},
			function (CustomerCommunity_DocumentsBody, status, errorMessage) {
				if (status === "SUCCESS") {
					tabCommercial.set("v.body", [CustomerCommunity_DocumentsBody]);
				}
			}
		);
	}
})