({
    getAddressRecords: function(component, event) {
        var action = component.get("c.getAddresses");
        console.log(component.get("v.recordId"));
        action.setParams({
            "idHeader": component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            var listAddress = component.get("v.listAddress");
            var listAddressDisplay = [];
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                console.log(value);
                for (var index = 0; index < value.length; index++) {
                    listAddress.push(value[index]);
                    listAddressDisplay.push({
                        Name:value[index].Name,
                        Address__c:value[index].Address__c,
                        Type__c:value[index].Type__c,
                        ID_Code__c:value[index].ID_Code__c, 
                        FMC_Tax_Id__c:value[index].FMC_Tax_Id__c, 
                        Phone__c:value[index].Phone__c
                    });
                    if(value[index].Address__c != undefined){    
                        var mainString = value[index].Address__c;
                        if(mainString.includes('|')){ 
                            var splitAddress = value[index].Address__c.split('|');
                            var concatString = '';
                            for(var index1=0; index1<splitAddress.length;index1++){
                                concatString = concatString + splitAddress[index1] + '\n';
                            }
                            console.log('concatString --->'+ concatString); 
                            listAddressDisplay[index].Address__c = concatString    
                        }   
                    }
                }
                var keySet = ['N1','SH','BT','CN','FW','N2'];
                for(var index=0;index<keySet.length;index++){
                    const isInlist = this.isAddressPresent(keySet[index],listAddressDisplay);
                    if(!isInlist){
                        listAddressDisplay.push({Address__c:"",Name:"", Type__c:keySet[index], ID_Code__c:"", FMC_Tax_Id__c:"", Phone__c:""}); 
                    }
                }
                console.log(listAddress);
                console.log(listAddressDisplay);
                component.set("v.listAddress",listAddress);
                component.set("v.listAddressDisplay", listAddressDisplay);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    isAddressPresent : function(key,listAddress){   
        for(var index =0;index<listAddress.length; index++){
            if(key == listAddress[index].Type__c){
                return true;
            }     
        }
        return false;
    },
    addChangeValue : function(component,event){
        console.log('Inside addChangeValue');
        var listAddressDisplay = component.get("v.listAddressDisplay");  
        var listAddress = component.get("v.listAddress");
        var currentAddType = event.getSource().getLocalId() ;
        console.log('@@@ currentAddType - ' + currentAddType);
        console.log('onchange-->'+listAddress+'listAddressDisplay-->'+listAddressDisplay);
        for(var index = 0; index < listAddressDisplay.length ; index++){
            console.log('onchange-->'+listAddress+'type11-->'+listAddressDisplay[index].Type__c);
            var isPresent = this.isAddressPresent(listAddressDisplay[index].Type__c,listAddress);
            console.log('isPresent-->'+isPresent);
            if(!isPresent){
                console.log('nameee-->'+listAddressDisplay[index].Name+'addd-->'+listAddressDisplay[index].Address__c);
                var newObj = {
                    "Type__c" : listAddressDisplay[index].Type__c,
                    "Address__c" : listAddressDisplay[index].Address__c,
                    "Name" : listAddressDisplay[index].Name,
                    "ID_Code__c":listAddressDisplay[index].ID_Code__c, 
                    "FMC_Tax_Id__c":listAddressDisplay[index].FMC_Tax_Id__c, 
                    "Phone__c":listAddressDisplay[index].Phone__c   
                };
                listAddress.push(newObj);
            }
            if(listAddressDisplay[index].Type__c == currentAddType){
                console.log('@@@ Method reached');
                
                /*var currentAddVal = component.find(currentAddType).get("v.value");
                if(currentAddVal.includes("\n")){
                    console.log('@@@ Method reached 2');
                    listAddressDisplay[index].Name = currentAddVal.substring(0,currentAddVal.indexOf("\n"));
                    listAddressDisplay[index].Address__c = currentAddVal.substring(currentAddVal.indexOf("\n")+1, currentAddVal.length);
                    component.set("v.listAddressDisplay",listAddressDisplay);
                }  */
            }
        }  
    },
    getEquipmentRecords: function(component, event) {
        var action = component.get("c.getEquipments");
        console.log(component.get("v.recordId"));
        action.setParams({
            "idHeader": component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                component.set("v.listEquipment", value);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response)
            }
        });
        $A.enqueueAction(action);
    },
    getHeaderRecord: function(component, event) {
        var action = component.get("c.getHeader");
        console.log(component.get("v.recordId"));
        action.setParams({
            "idHeader": component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                console.log('HEADER DATA : '+JSON.stringify(value));
                component.set("v.headerRecord", value);
                if(value.Status__c == 'Approved By Customer'){
                    component.set("v.isFieldReadonly",true);
                }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response)
            }
        });
        $A.enqueueAction(action);
    },
    getHazardousLine : function(component, event, helper){
        var action = component.get("c.getHazardousLines");
        console.log(component.get("v.recordId"));
        action.setParams({
            "idHeader": component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                if (value !== null){
                    component.set("v.listHazardousLine", value);
                  /*  var emergencyName = '';
                    var emergencyPhone = '';
                    var bookingNumber = '';
                    var containerNumber = '';
					
                   bookingNumber=value[0].Booking_Number__c;
                    containerNumber=value[0].Container_Number__c;
                    var miscellaneousData ={BookingNumber : bookingNumber, ContainerNumber : containerNumber}
                    component.set("v.miscellaneousData",miscellaneousData);
                    for(var k=0; k < value.length ; k++){
                        emergencyName = value[k].Emergency_Contact_Name__c;
                        emergencyPhone = value[k].Emergency__c;
                    }
                    var emergency = { Name : emergencyName, Phone : emergencyPhone};
                    console.log(emergency)
                    component.set('v.emergencyResponse', emergency);   */
                }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response)
            }
        });
        $A.enqueueAction(action);
    },
    getHeaderRecordType : function(component, event, helper) {
        var action = component.get("c.getHeaderRecordType");
        console.log(component.get("v.recordId"));
        action.setParams({
            "idHeader": component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                console.log("Value", value);
                if(value === 'Shipping_Instruction'){
                    component.set("v.isShippingInstruction",true);
                } else if (value === 'Hazardous_Document'){
                    component.set("v.isHazardousDocument",true);
                }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response)
            }
        });
        $A.enqueueAction(action);
    },
    saveRecords: function(component, event) {
        
        var listAddressDisplay = component.get("v.listAddressDisplay");
        var objHeader = component.get("v.headerRecord");
        var listAddress = component.get("v.listAddress");
        var listEquipment = component.get("v.listEquipment");
        var listHazardousLine = component.get("v.listHazardousLine");
       // var emergencyResponse = component.get("v.emergencyResponse");
       // var miscellaneousData= component.get("v.miscellaneousData");
        console.log("objHeader", JSON.stringify(objHeader));
        var serverAddress;
        console.log('listAddressDisplay-->'+listAddressDisplay+'listAddress-->'+listAddress);
        
        for(var index = 0;index<listAddressDisplay.length;index++){
            for(var index1=0;index1<listAddress.length;index1++){
                if(listAddressDisplay[index].Type__c == listAddress[index1].Type__c){
                    listAddress[index].Address__c = listAddressDisplay[index1].Address__c;
                    listAddress[index].Name = listAddressDisplay[index1].Name;
                    listAddress[index].ID_Code__c = listAddressDisplay[index1].ID_Code__c;
                    listAddress[index].FMC_Tax_Id__c = listAddressDisplay[index1].FMC_Tax_Id__c;
                    listAddress[index].Phone__c = listAddressDisplay[index1].Phone__c;
                    console.log(listAddress[index].Type__c+'   '+listAddress[index].Name+'   '+listAddress[index].Address__c);
                    //console.log('Name111 --> '+listAddress[index].Name +' Name222 --> '+listAddressDisplay[index].Name);
                    //console.log('Type__c-->'+listAddress[index].Type__c+'address111-->'+listAddress[index].Address__c+'address222-->'+listAddressDisplay[index].Address__c);
                    break;
                }
            }
        }
        
      /*  for(var index=0;index<listHazardousLine.length;index++){
            listHazardousLine[index].Emergency_Contact_Name__c=emergencyResponse.Name;
            listHazardousLine[index].Emergency__c=emergencyResponse.Phone;
            listHazardousLine[index].Booking_Number__c=miscellaneousData.BookingNumber;
            listHazardousLine[index].Container_Number__c=miscellaneousData.ContainerNumber;
        }  */
        var action = component.get("c.saveBolRecords");
        action.setParams({
            "listAddress": listAddress,
            "objHeader": objHeader,
            "isStatusApprove" : false,
            "listEquipment" : listEquipment,
            "listHazardousLine" : listHazardousLine
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                if(value == "TRUE"){
                    component.set("v.modalMessageSave",'The Document has been saved Successfully !');
                    component.set("v.isModalOpenSave",true);
                    $A.get('e.force:refreshView').fire();
                }
                else if(value == "FALSE"){
                    component.set("v.modalMessageSave",'Something went Wrong. Please try Again.')
                    component.set("v.isModalOpenSave",false);
                }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response)
            }
        });
        $A.enqueueAction(action);
    },
    restoreValues : function(component,event){  
        
        var staticLabel = $A.get("$Label.c.CustomerCommunity_Booking");
        var bookingId = component.get("v.headerRecord.Booking__r.Id");  
        var url = staticLabel + '/' + bookingId; 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    confirmApprove : function(component,event){
        component.set('v.spinner', true);
        var objHeader = component.get("v.headerRecord");
        var listAddress = component.get("v.listAddress");
        var listEquipment = component.get("v.listEquipment");  
        var listAddressDisplay = component.get("v.listAddressDisplay");  
        var listHazardousLine = component.get("v.listHazardousLine");
        var action = component.get("c.saveBolRecords");	
        for(var index = 0;index<listAddressDisplay.length;index++){
            for(var index1=0;index1<listAddress.length;index1++){
                if(listAddressDisplay[index].Type__c == listAddress[index1].Type__c){
                    listAddress[index].Address__c = listAddressDisplay[index1].Address__c;
                    listAddress[index].Name = listAddressDisplay[index1].Name;
                    break;
                }
            }
        } 
        var statusUpdate = true;
        if(listHazardousLine.length > 0){
            statusUpdate = false;
        }
        action.setParams({
            "listAddress": listAddress,
            "objHeader": objHeader,
            "listEquipment" : listEquipment,
            "isStatusApprove" : statusUpdate,
            "listHazardousLine" : listHazardousLine
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(listHazardousLine.length > 0){
                    this.getBookingRecordValues(component, event);
                }else{
                    component.set('v.spinner', false);
                    $A.get('e.force:refreshView').fire(); 
                }
            } else if (state === "ERROR") {
                component.set('v.spinner', false);
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action); 
    },
    updateStatusToApprove: function(component, event){
        var objHeader = component.get("v.headerRecord");
        var action = component.get("c.updateStatusToApprove");
        action.setParams({"objHeader": objHeader});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() === 'True'){
                    console.log("Status Updated");
                }else{
                    console.log("Status not updated");
                    component.set('v.spinner', false);
                }
            } else if (state === "ERROR") {
                component.set('v.spinner', false);
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action); 
    },
    getBookingRecordValues : function(component, event){
        var objHeader = component.get("v.headerRecord");
        var action = component.get("c.getBookingRecordValues");
        action.setParams({"objHeader": objHeader});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("getBookingRecordValues", JSON.stringify(response.getReturnValue()));
                component.set("v.bookingRecordValues", response.getReturnValue());
                this.validateHazchecker(component, event);
            } else if (state === "ERROR") {
                component.set('v.spinner', false);
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action); 
    },
    validateHazchecker: function(component) {
        var objHeader = component.get("v.headerRecord");
        var action = component.get("c.validateHazchecker");
        action.setParams({"objHeader": objHeader, "bookingRecordValues" : component.get("v.bookingRecordValues")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {	
                if(response.getReturnValue() == 'True'){
                    this.getBookingNumber(component, event);
                }
                else{
                    component.set('v.spinner', false);
                    component.set("v.isHazModalBoxOpen", true);
                    component.set("v.HazValidationMessage", "Hazardous goods cannot be validated at the moment. Please try validating later.");
                }
            } else if (state === "ERROR") {
                component.set('v.spinner', false);
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action); 
    },
	getBookingNumber : function(component, event){
        var bookingRecordValues = component.get("v.bookingRecordValues");
        console.log("bookingRecordValues",bookingRecordValues);
        if(bookingRecordValues != null){
            var action = component.get("c.getCICSBookingNumber"); 
            action.setParams({ 
                bookingNumber : bookingRecordValues.bookingRequestNumber
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log(state);
                console.log(response.getReturnValue());
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    component.set("v.bookingRequestRecord", responseData);
                    component.set("v.isBookingConfirmed", true);
                    component.set("v.isIMDGBooking", true);
                    this.updateStatusToApprove(component, event);
                    component.set("v.isBookingModalBoxOpen", true);
                } else {
                    component.set("v.isBookingConfirmed", false);
                }
                if(bookingRecordValues.bookingType != null && bookingRecordValues.bookingType == "FCL"){
                    //this.getSoftshipRating(component, event);
                } else {
                    //component.set("v.isBookingModalBoxOpen", true);
                }
                component.set('v.spinner', false);
            });
            $A.enqueueAction(action);
        }
    },
    getSoftshipRating : function(component, event){
        var bookRecValues = component.get("v.bookingRecordValues");
        var action = component.get("c.getSoftshipRates"); 
        var bookingRecValuesString = JSON.stringify(bookRecValues);   
        action.setParams({ bookingRecValuesString :bookingRecValuesString, sourceType : "NewBooking"});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                if(responseData != null && responseData != ""){
                    component.set("v.softshipRates", responseData);
                } 
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
                component.set("v.isBookingModalBoxOpen", true); 
            }
            else{
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","We encounterd some error while getting rates. Please try again.");
                component.set("v.isBookingModalBoxOpen", true);
            } 
        });
        $A.enqueueAction(action);
    },  
    navigateToBookingRecord : function(component, event, helper){
        var bookingRec = component.get("v.bookingRecordValues");
        var action = component.get("c.getBookingId");
        action.setParams({ 
            bookingNumber : bookingRec.bookingRequestNumber
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var bookingRec = response.getReturnValue();
                if(bookingRec != null){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/booking/"+bookingRec,
                    });
                    urlEvent.fire();
                }
                else{
                    component.set("v.isBookingModalBoxOpen", false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    ErrorHandler: function(component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },
    
    getChargeLineItemRecord: function(component, event) {
        var action = component.get("c.getChargeLineItems");
        action.setParams({
            "idHeader": component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                component.set("v.listChargeLineItem", value);
                var total=0;
                if(value != null){
                    value.forEach(function(item, index) {
                        console.log(item.Line_Total__c);
                        if(item.Line_Total__c != null){  
                            total += item.Line_Total__c;
                        }               
                    });
                }
                component.set("v.sum", total);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response)
            }
        });
        $A.enqueueAction(action);
    }
})