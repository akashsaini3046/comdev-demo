({
	doInit : function(component, event, helper) {
		helper.getAddressRecords(component,event);
        helper.getEquipmentRecords(component,event);
        helper.getHeaderRecord(component,event);        
        helper.getChargeLineItemRecord(component,event);
        helper.getHeaderRecordType(component,event, helper);
        helper.getHazardousLine(component, event, helper);
	},
    handleOnBlur : function(component, event, helper) {
        //var cmpp = component.find('SH');
         //$A.util.addClass(cmpp, 'warninginput');
         console.log('==============');
         console.log(event.getSource().get("v.id"));
        //event.getSource().get("v.style").bordercolor='red';
         //console.log(event.getSource().get("v.style"));
        if(event.getSource().get("v.class") == 'warninginput') {
            event.getSource().set("v.class", "");
        } else {
            event.getSource().set("v.class", "warninginput");
        }
    },
    saveRecords : function(component,event,helper){
        helper.saveRecords(component,event);
    },
    restoreValues : function(component,event,helper){
        helper.restoreValues(component,event);
    },
    closeModelSave: function(component, event, helper){ 
      component.set("v.isModalOpenSave", false);
   },
    approveRecords : function(component,event,helper){
        component.set("v.isModalOpenApprove",true);
    },
    closeModelApprove : function(component,event,helper){
       component.set("v.isModalOpenApprove",false); 
    },
    closeHazModelBox: function(component,event,helper){
       component.set("v.isHazModalBoxOpen",false); 
        component.set("v.HazValidationMessage",null); 
    },
    confirmApprove : function(component,event,helper){
        component.set("v.isFieldReadonly",true);
        component.set("v.isModalOpenApprove",false);
        helper.confirmApprove(component,event);
    },
    addChangeValue : function(component,event,helper){
      helper.addChangeValue(component,event);
    },
    navigateToBookingRecord : function(component, event, helper){
        helper.navigateToBookingRecord(component, event, helper); 
    },
    navigateToNewBooking : function(component, event, helper){
        component.set("v.isBookingModalBoxOpen", false);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/booking/Booking__c/Default",
        });
        urlEvent.fire();
    },
    closeAndRefresh : function(component, event, helper){
        $A.get('e.force:refreshView').fire();
    },
    handleRenderer : function(component, event, helper){
        console.log('hello');
        var elem = component.find("SH").getElement();
        console.log('hii');
        console.log('elem --> '+elem);
    }
})