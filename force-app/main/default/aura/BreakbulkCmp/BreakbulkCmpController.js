({
    doInit : function(component, event, helper) {
        var bookingWrapperObj = component.get("v.BookingWrapperObj"); 
        if(bookingWrapperObj.length>0){
            helper.editCargoToContainer(component, event);
        }else{
            helper.addCargoToContainer(component, event);
        }
    },
    addItem : function(component, event, helper) {
        helper.addItem(component, event, helper);
       
    },
    removeItem : function(component, event, helper) {
        helper.removeItem(component, event, helper);
       
    },
})