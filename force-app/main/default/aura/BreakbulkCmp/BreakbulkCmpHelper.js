({
    addCargoToContainer: function (component, event) {       
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        objcargo["cargoType"] = 'breakbulk';
        objcargo["listFreightDetailWrapper"] = [{ commodityDesc: '', freightDetail: {}, listCommodityWrapper: [], listRequirementWrapper: [{ requirement: {}, commodityDesc: '' }] }];
        component.set("v.BookingWrapperObj", objcargo);
        console.log(component.get("v.BookingWrapperObj"));
    },
    editCargoToContainer: function (component, event) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        BookingWrapperObj[0].cargoType = 'breakbulk';
        if (BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper.length > 0) {
            component.set("v.IsHazardous", true);
        }
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    addItem: function (component, event, helper) {       
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        BookingWrapperObj[0].listFreightDetailWrapper.push({ freightDetail: {}, commodityDesc: '' });
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    removeItem: function (component, event, helper) {       
        var index = event.target.id;
        console.log(index);
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        BookingWrapperObj[0].listFreightDetailWrapper.splice(index, 1);
        console.log(BookingWrapperObj);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
})