({
    doInit : function(component, event, helper) {
        helper.fetchReceiptDeliveryTerms(component);
        helper.createBlankCargoDetail(component);
        helper.fetchCFSLocations(component, event);
        helper.createBlankQuotation(component, event);
        helper.formatDate(component);
        helper.fetchCommodityRecords(component);
        helper.fetchContainerTypeRecords(component);
        helper.fireHighlightEvent(component, event);
        //helper.navigationServiceBooking(component, event);
        helper.setReQuoteValues(component, event);
    },
    
    openChargeItemBox : function(component,event,helper){
        if(component.get("v.isChargeModalBoxOpen") === true){
            component.set("v.isChargeModalBoxOpen",false);
        }else{
            component.set("v.isChargeModalBoxOpen",true);
        }
    },
    closeToast : function(component,evnt,helper){
        component.set("v.isChargeModalBoxOpen",false);
    },
    closeModalBox : function(component,event,helper){
        component.set("v.isChargeBreakdownModelOpen",false);
    },
    openChargeBreakDownItemBox : function(component,event,helper){
        component.set("v.listChargeLines",null);
        component.set("v.rateLTL",null);
        var rowIndex = event.getSource().get("v.value");
        var cargoRateValue = component.get("v.quoteDataToDisplay");
        var ltlRate = cargoRateValue[rowIndex].Body.Rate;
        component.set("v.rateLTL",ltlRate);
        component.set('v.listChargeLines',component.get("v.mapChargeLineItems").get(rowIndex.toString()));  
        component.set("v.isChargeBreakdownModelOpen",true);
    }, 
    navigateToBookingScreen : function(component, event, helper) {
        helper.saveRateAndNavigateBooking(component, event); 
    },
    updateAddressData : function(component, event, helper){
        helper.updateAddressField(component, event);
    },
    fetchCustomerRecords : function(component, event, helper) {
        //if(event.which == 13){
            component.set("v.autoFillCustomerFields", false);
            helper.getCustomerRecords(component, event);
        //}
    },
    fetchShipperRecords : function(component, event, helper) {
        //if(event.which == 13){
        	helper.getShipperRecords(component, event);
        //}
    },
    fetchConsigneeRecords : function(component, event, helper) {
        //if(event.which == 13){
        	helper.getConsigneeRecords(component, event);
        //}
    },
    totalDimensionsChecked : function(component, event, helper) { 
        component.set("v.totalDimensions", true);
        component.set("v.packageDetails", false); 
    },
    packageDetailsChecked : function(component, event, helper) {
        component.set("v.totalDimensions", false);
        component.set("v.packageDetails", true);
    },
    saveRateOnQuote : function(component, event, helper) {
        helper.saveRateOnQuote(component, event);
    },
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    changePlaceholders : function(component, event, helper) {
        helper.modifyPlaceholders(component);
    },
    updateChargeableWeight : function(component, event, helper) {
        helper.modifyChargeableWeight(component);
    },
    getQuotes : function(component, event, helper) {
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var deliveryTerm = component.find("DeliveryTerm").get("v.value");
        var freightType = component.get("v.freightType");;
        console.log('recTerm111-->'+recTerm+'freightType--->'+freightType);
        console.log('deliveryTerm111-->'+deliveryTerm);
        component.set("v.quoteDataToDisplay", null);
        component.set("v.showErrorMessage", false);
        component.set("v.chargeLineItems", null);
        component.set("v.fclRatingResponse", null);        
        component.set("v.showCargoerror", false);
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var deliveryTerm = component.find("DeliveryTerm").get("v.value");
        helper.createQuoteRecord(component);
        if(freightType=='LCL' && recTerm=='D' && deliveryTerm=='CFS'){
             console.log('indandCFS-->');
              helper.getDoorToCFSRatesClone(component);
              component.set("v.chargeLineItems", null);          
		}
        else if(freightType=='LCL' && recTerm=='CFS' && deliveryTerm=='CFS'){
         //helper.getQuotationCFStoCFS(component); 
         helper.getQuotationCFStoCFSClone(component);   
        }
        else if(freightType=='FCL'){
         helper.getQuotationFCL(component);   
        }
    },
    getMoreQuotes : function(component, event, helper) {
        //helper.getMoreQuotations(component);
        helper.getQuotationCFStoCFS(component);
    },  
    clearForm : function(component, event, helper) {
        helper.clearValues(component);
    },
     handleFCLClick : function(component, event, helper) {
        helper.clearValues(component);
        helper.FCLBtnClick(component);
    },
    handleLCLClick : function(component, event, helper) {
        helper.clearValues(component);
        helper.LCLBtnClick(component);
    },
    updateOriginValues : function(component, event, helper){
    	var recTerm = component.find("ReceiptTerm").get("v.value");
        component.set("v.originLocation","");
        component.set("v.originCodeValue","");
        component.set("v.OriginLocation","");
        if(recTerm == 'D'){  
            component.set("v.showCFS", false);
            component.set("v.disableOriginCode", false);
            component.set("v.originCodePlaceholder","Enter Zip Code");
            component.set("v.originPlaceholder","Enter Zip Code to get location"); 
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            component.set("v.quoteRequest.originCode","");
        }
        else if(recTerm == 'P'){
            component.set("v.showCFS", false);
            component.set("v.disableOriginCode", false);
            component.set("v.quoteRequest.originCode", "");
            component.set("v.originCodePlaceholder","Enter Port Code");
            component.set("v.originPlaceholder","Enter Port Code to get location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else if(recTerm == 'CFS'){
            component.set("v.showCFS", true);
            component.set("v.disableOriginCode", false);
            component.set("v.quoteRequest.originCode", "");
            component.set("v.originCodePlaceholder","Enter Port Code");
            component.set("v.originPlaceholder","Enter Port Code to get location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else{
            component.set("v.showCFS", false);
            component.set("v.disableOriginCode", true);
            component.set("v.quoteRequest.originCode", "");
            component.set("v.originCodePlaceholder","Select Receipt Term");
            component.set("v.originPlaceholder","Select Receipt Term and then enter Origin Code");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
    },
    updateDestinationValues : function(component, event, helper){
    	var delTerm = component.find("DeliveryTerm").get("v.value"); 
        component.set("v.destinationLocation","");
        component.set("v.destinationCodeValue","");
        component.set("v.destinationLocation","");
        if(delTerm == 'D'){
            component.set("v.showDeliveryCFS", false);
            component.set("v.disableDestinationCode", false);
            component.set("v.destinationCodePlaceholder","Enter Zip Code");
            component.set("v.destinationPlaceholder","Enter Zip Code to get location");
            component.set("v.showErrorMessage",false); 
            component.set("v.errorMessage","");
            component.set("v.quoteRequest.destinationCode","");
        }
        else if(delTerm == 'P'){
            component.set("v.showDeliveryCFS", false);
            component.set("v.disableDestinationCode", false);
            component.set("v.destinationCodePlaceholder","Enter Port Code");
            component.set("v.destinationPlaceholder","Enter Port Code to get location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            component.set("v.quoteRequest.destinationCode","");
        }
        else if(delTerm == 'CFS'){
            component.set("v.showDeliveryCFS", true);
            component.set("v.disableDestinationCode", false);
            component.set("v.destinationCodePlaceholder","Enter Port Code");
            component.set("v.destinationPlaceholder","Enter Port Code to get location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else{
            component.set("v.showDeliveryCFS", false);
            component.set("v.disableDestinationCode", true);
            component.set("v.destinationCodePlaceholder","Select Delivery Term");
            component.set("v.destinationPlaceholder","Select Delivery Term and then enter Origin Code");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
            component.set("v.quoteRequest.destinationCode","");
        }
    },
    createCargoDetailElement: function(component, event, helper){
        var reqArrLimit = component.get("v.cargoListLimit");
        var cargoDetails = component.get("v.cargoDetailsValues");
        console.log('reqArrLimit-->'+reqArrLimit+'cargoDetails-->'+cargoDetails.length);
        if(cargoDetails.length < reqArrLimit){
            console.log('helperr-->');
            helper.createBlankCargoDetail(component);
        }
	},
    removeCargoDetailElement : function(component, event) {
        var elementId = event.currentTarget.id;
        console.log('elementId-->'+elementId);
        var cargoRec = component.get("v.cargoDetailsValues"); 
        var newCargoArr = []; var count = 1;
        for(var i=0; i<cargoRec.length; i++){
            if(cargoRec[i].sequenceId != elementId){
                cargoRec[i].sequenceId = count++;
                newCargoArr.push(cargoRec[i]);
            }
        }
        component.set("v.cargoDetailsValues",newCargoArr);
    },
    getOriginLocation : function(component, event, helper){
        helper.getOriginLocation(component, event);
    },
    getDestinationLocation : function(component, event, helper){
        helper.getDestinationLocation(component, event);
    }
})