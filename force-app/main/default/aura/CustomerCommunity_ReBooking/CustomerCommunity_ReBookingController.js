({
	CreateBooking : function(component, event, helper) {
        helper.createBookingRecord(component, event);
	},
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    clearValues : function(component, event, helper){
        component.set("v.ReadyDate", null)
    },
    navigateToBooking : function(component, event, helper){
        var bookingRec = component.get("v.bookingRequestRecord");
        if(bookingRec != null){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/booking/"+bookingRec.Id,
            });
            urlEvent.fire();
        }
    }
})