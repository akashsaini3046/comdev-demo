({
	doInit : function(component, event, helper) {
        helper.fireHighlightEvent(component, event);
        helper.fetchDestinationPortList(component, event);
        //helper.fetchPortsInformation(component, event);
	},
    handleFCLClick : function(component, event, helper) {
        helper.LCLBtnClick(component);
    },
    handleLCLClick : function(component, event, helper) {
        helper.LCLBtnClick(component);
    },
    changePlaceholders : function(component, event, helper) {
        helper.modifyPlaceholders(component);
    },
    updateChargeableWeight : function(component, event, helper) {
        helper.modifyChargeableWeight(component);
    },
    getQuotes : function(component, event, helper) {
        helper.getQuotation(component);
    },
    clearForm : function(component, event, helper) {
        helper.clearValues(component);
    },
    navigateToBookingScreen : function(component, event, helper) {
        helper.navigateToBooking(component);
    },
    searchCityState : function(component, event, helper) {
       console.log('vvv-->'+component.find("originCode").get("v.value"));
        if(component.find("originCode").get("v.value") != null && component.find("originCode").get("v.value") != ''){
        	helper.searchCityByZipcode(component);    
        }	
        else if(component.find("originCode").get("v.value") == ''){
       		console.log('elseee-->'+component.find("originCode").get("v.value"));
            component.set("v.originCityState","");  
        }
    },
    searchCityStateOnHit : function(component, event, helper) {
       console.log('vvv111-->'+component.find("originCode").get("v.value"));
        if(event.which == 13 && component.find("originCode").get("v.value") != null && component.find("originCode").get("v.value") != ''){
        	helper.searchCityByZipcode(component);    
        }	
        else if(component.find("originCode").get("v.value") ==''){
          console.log('elseee11-->'+component.find("originCode").get("v.value"));
          component.set("v.originCityState","");  
        }
    },
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },

    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    }
})