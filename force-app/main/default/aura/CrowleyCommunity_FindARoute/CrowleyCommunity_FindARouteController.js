({
	doInit : function(component, event, helper) {
        component.set("v.errorMessage",$A.get("$Label.c.CustomerCommunity_FARInputMessage"));
        helper.fetchFCLPorts(component);
        helper.fetchLCLPorts(component);
        helper.fireHighlightEvent(component, event);
        helper.fetchSailingWeeksPicklist(component);
        component.set("v.loading",false);
	},
    findRoutes : function(component, event,helper) {
        component.set("v.routeList", null);
        helper.getRouteList(component);
    },
    handleFCLClick : function(component, event, helper) {
        helper.FCLBtnClick(component);
    },
    handleLCLClick : function(component, event, helper) {
        helper.LCLBtnClick(component);
    },
    ExpandCollapseSection : function(component, event, helper) {
        helper.changeSectionState(component);
    },  
    PrintFullSchedule : function(component, event, helper) {
        helper.printFullList(component, event);
        window.print();
    },
    PrintSection : function (component, event, helper) {
        helper.printIndividualSection(component, event);
        window.print();
    },
    showInputBox : function (component, event, helper) {
        var selectedDiv = event.currentTarget.id;
        $A.util.addClass(component.find(selectedDiv), 'open-search');
    },
    handleSortingCriteria : function(component, event, helper){
        helper.handleSortingCriteria(component, event);
    }, 
    handleOnRender :  function(component,event,helper){
        helper.handleOnRender(component,event);
    },
    handleComponentEvent : function(component,event,helper){
        helper.handleComponentEvent(component,event,helper);
    }
})