({
	updateHomeScreenView : function(component, event, helper) {
    	helper.switchHomeScreen(component, event);
	},
    openChatWindow: function(component, event, helper) {
    	window.open("https://messenger.providesupport.com/messenger/18fs965gg46521bqvex5h9l5ys.html", '_blank');
	},
    doInit : function(component,event,helper){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
		component.set("v.userId", userId);
        var today = $A.localizationService.formatDate(new Date(), "DD MMMM");
        console.log('today-->'+today);
    	component.set('v.todayDate', today);
        helper.doInit(component,event);
        helper.getCountOfCases(component,event);
        helper.fetchAccountName(component,event);
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userResponse = response.getReturnValue();
                component.set("v.userInfo", userResponse);
            }
        });
        $A.enqueueAction(action);
	}
   
})