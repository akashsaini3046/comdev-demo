({
    switchHomeScreen : function(component, event){
        var appEvent = $A.get("e.c:CustomerCommunity_HomeScreenEvent");
        appEvent.setParams({"HomeScreenComponent" : "c:CustomerCommunity_DashboardScreen" });
        appEvent.fire();
    },
    getCountOfCases : function(component, event){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.countOfCases");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userResponse = response.getReturnValue();
                console.log('userResponse-->'+userResponse);
                component.set("v.countOfCases", userResponse);
            }
        });
        $A.enqueueAction(action);
    },
    fetchAccountName: function(component,event){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.fetchAccountName");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               var accountName = response.getReturnValue();
               console.log('accountName-->'+accountName);
                if(accountName != null){ 
                    component.set("v.accountName",accountName); 
                }
            }
            else if(state === "ERROR"){
                this.ErrorHandler(component,response);
            }
        });
        $A.enqueueAction(action);  
    },
    doInit : function(component,event){
        var action = component.get("c.fetchBooking");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var document = response.getReturnValue();
                if (document != null) {
                    console.log(document);
                    component.set("v.documentName",document.Name);
                    component.set("v.documentId",document.Id);
                    component.set("v.bookingName",document.Booking__r.Name);
                    component.set("v.bookingId",document.Booking__r.Id);	
                }
            } else if (state === "ERROR") {
                this.ErrorHandler();
            }
        });
        $A.enqueueAction(action);
    },
     ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    }
})