({
    getBookingWrap: function (component, event, helper) {
        var action = component.get("c.getQuoteBookingWrapper");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var bookingWrapper = response.getReturnValue();
                console.log("bookingWrapper", bookingWrapper);
                component.set("v.bookingWrapper", bookingWrapper);
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
    customerAccount: function (component, event) {
        console.log('Inside fetchContactDetails');
        var action = component.get("c.customerAccount");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.customerAccount", responseData);
                //component.set("v.selectedCustomer",responseData.Id);
                //component.set("v.contactRecordDetail",responseData);  
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
                //this.populateCustomerRecord(component, event);
            }
            else {
                component.set("v.contactRecordDetail", null);
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Contact Details could not be fetched. Please try again.");
            }
        });
        $A.enqueueAction(action);
    },
    fetchContactDetails: function (component, event) {
        console.log('Inside fetchContactDetails');
        var action = component.get("c.getContactDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.selectedCustomer", responseData.Account.CVIF__c);
                var bookingWrapper = component.get("v.bookingWrapper");
                bookingWrapper.booking.Account__c = responseData.Account.Id;
                component.set("v.bookingWrapper", bookingWrapper);
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
                // this.populateCustomerRecord(component, event);
            }
            else {
                component.set("v.contactRecordDetail", null);
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Contact Details could not be fetched. Please try again.");
            }
        });
        $A.enqueueAction(action);
    },
    fetchReceiptDeliveryTerms: function (component) {
        var action = component.get("c.fetchReceiptDeliveryTermsList");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.receiptDeliveryTermsList", responseData);
            }
        });
        $A.enqueueAction(action);
    },
    formatDate: function (component) {
        var currentDate = new Date();
        var minDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()), "YYYY-MM-DD");
        var maxDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate() + 365), "YYYY-MM-DD");
        component.set('v.minDate', minDate);
        component.set("v.maxDate", maxDate);
    },
    fetchCFSLocations: function (component, event) {
        var action = component.get("c.getCFSLocations");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.cfsLocations", responseData);
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
            }
            else {
                component.set("v.cfsLocations", null);
            }
        });
        $A.enqueueAction(action);
    },
    nextProgress: function (component, event, helper) {
        component.set("v.isSpinner", true);
        var currentStep = component.get("v.selectedStep");
        var intStep = parseInt(currentStep);
        //alert(intStep);
        console.log("currentStep : " + currentStep);
        intStep = intStep + 1;
        component.set("v.selectedStep", intStep.toString());
        component.set("v.isSpinner", false);
    },
    setAccount: function (component, event) {
        var cvifId = component.get("v.selectedCustomer");
        var bookingWrapper = component.get("c.bookingWrapper");
        var customerAccounts = component.get("v.customerAccount");
        for (var i = 0; i < customerAccounts.length; i++) {
            if (cvifId == customerAccounts[i].Account.CVIF__c) {
                bookingWrapper.booking.Account__c = customerAccounts[i].Account.Id;
                break;
            }
        }
        bookingWrapper.mapParty.consignee[0].Internal_Hidden__c = '';
        component.set("v.bookingWrapper", bookingWrapper);
    },
    populateCustomerRecord: function (component, event) {
        var contactDetails = component.get("v.contactRecordDetail");
        if (contactDetails != null) {
            var action = component.get("c.getCustomerCVIF");
            action.setParams({ contactRecord: contactDetails });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (response.getReturnValue() != null) {
                    this.getCustomerRecords(component, event, response.getReturnValue());
                    component.set("v.showErrorMessage", false);
                    component.set("v.errorMessage", "");
                }
                else {
                    component.set("v.showErrorMessage", true);
                    component.set("v.errorMessage", "Customer Details could not be fetched automatically. Please enter the Customer Details.");
                }
            });
            $A.enqueueAction(action);
        }
    },
    getCustomerRecords: function (component, event, responseData) {
        var action = component.get("c.fetchContactsAndAddresses");
        action.setParams({ partyCode: responseData });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                if (responseData != null) {
                    component.set("v.CustomerContactRecords", responseData);
                }
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
            }
            else {
                component.set("v.bookingRecordValues", bookingRec);
                component.set("v.CustomerContactRecords", null);
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Entered code not found. Please re-check the Customer Code.");
            }
        });
        $A.enqueueAction(action);
    },
    getDestinationLocation: function (component, event) {
        var delTerm = component.find("deliveryTermId").get("v.value");
        //var destinationCode =component.find("deliveryLocationId").get("v.value");
        var bookingWrapper = component.get("v.bookingWrapper");
        var destinationCode = bookingWrapper.booking.Customer_Destination_Code__c;
        var cfsPorts = component.get("v.cfsLocations");
        console.log(destinationCode);
        console.log('cfsPorts : ' + cfsPorts);
        if (delTerm == '') {
            component.set("v.showErrorMessage", true);
            component.set("v.errorMessage", "Please select a Delivery Term and then enter Destination Code");
            component.set("v.destinationLocation", "");
        }
        else if (destinationCode == '' || destinationCode == null) {
            component.set("v.showErrorMessage", true);
            component.set("v.errorMessage", "Please enter Destination Code");
            component.set("v.destinationLocation", "");
        }
        else if (delTerm == 'CFS') {
            component.set("v.showErrorMessage", false);
            component.set("v.errorMessage", "");
            var destinationLoc = '';
            for (var i = 0; i < cfsPorts.length; i++) {
                if (cfsPorts[i].Name == destinationCode) {
                    console.log('destinationCode : ' + destinationCode);
                    if (cfsPorts[i].Street__c != '' && cfsPorts[i].Street__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Street__c + ' ';
                    if (cfsPorts[i].Name != '' && cfsPorts[i].Name != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Name + ' ';
                    if (cfsPorts[i].State_Abbreviation__c != '' && cfsPorts[i].State_Abbreviation__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].State_Abbreviation__c + ' ';
                    if (cfsPorts[i].Zip_Code__c != '' && cfsPorts[i].Zip_Code__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Zip_Code__c + ' ';
                    if (cfsPorts[i].Country__c != '' && cfsPorts[i].Country__c != null)
                        destinationLoc = destinationLoc + cfsPorts[i].Country__c + ' ';
                    component.set("v.destinationLocation", destinationLoc);
                }
            }
        }
        else {
            component.set("v.showErrorMessage", false);
            component.set("v.errorMessage", "");
            if (delTerm == 'P' && destinationCode.length > 4)
                this.getDestinationLocationDetails(component, event, delTerm, destinationCode);
            else if (delTerm == 'D' && destinationCode.length > 3)
                this.getDestinationLocationDetails(component, event, delTerm, destinationCode);
            else {
                component.set("v.destinationLocation", "");
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Entered Destination code not found. Please re-check the Destination Code.");
            }
        }
        bookingWrapper.destinationLocation = component.get("v.destinationLocation");
        component.set("v.bookingWrapper", bookingWrapper);
    },
    getDestinationLocationDetails: function (component, event, delTerm, destinationCode) {
        //var delTerm = component.find("deliveryTermId").get("v.value");        
        //var destinationCode =component.find("deliveryLocationId").get("v.value");
        console.log('delTerm:' + delTerm);
        console.log('destinationCode:' + destinationCode);
        var action = component.get("c.getLocationDetails");
        action.setParams({
            termCode: delTerm,
            locCode: destinationCode
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                component.set("v.destinationLocation", responseData);
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
            }
            else {
                component.set("v.destinationLocation", "");
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Entered Destination code not found. Please re-check the Destination Code.");
            }
            var bookingWrapper = component.get("v.bookingWrapper");
            bookingWrapper.destinationLocation = component.get("v.destinationLocation");
            component.set("v.bookingWrapper", bookingWrapper);
        });
        $A.enqueueAction(action);
    },
    getOriginLocation: function (component, event) {
        var recTerm = component.find("receiptTermId").get("v.value");
        //var destinationCode =component.find("deliveryLocationId").get("v.value");
        var bookingWrapper = component.get("v.bookingWrapper");
        var originCode = bookingWrapper.booking.Customer_Origin_Code__c;
        console.log(recTerm, originCode);
        var cfsPorts = component.get("v.cfsLocations");
        if (typeof recTerm == "undefined" || recTerm == '') {
            component.set("v.showErrorMessage", true);
            component.set("v.errorMessage", "Please select a Receipt Term and then enter Origin Code");
            component.set("v.OriginLocation", "");
        } else if (originCode == '' || originCode == null) {
            component.set("v.showErrorMessage", true);
            component.set("v.errorMessage", "Please enter Origin Zip Code");
            component.set("v.OriginLocation", "");
        } else if (recTerm == 'CFS') {
            component.set("v.showErrorMessage", false);
            component.set("v.errorMessage", "");
            var originLoc = '';
            for (var i = 0; i < cfsPorts.length; i++) {
                if (cfsPorts[i].Name == originCode) {
                    if (cfsPorts[i].Street__c != '' && cfsPorts[i].Street__c != null)
                        originLoc = originLoc + cfsPorts[i].Street__c + ' ';
                    if (cfsPorts[i].Name != '' && cfsPorts[i].Name != null)
                        originLoc = originLoc + cfsPorts[i].Name + ' ';
                    if (cfsPorts[i].State_Abbreviation__c != '' && cfsPorts[i].State_Abbreviation__c != null)
                        originLoc = originLoc + cfsPorts[i].State_Abbreviation__c + ' ';
                    if (cfsPorts[i].Zip_Code__c != '' && cfsPorts[i].Zip_Code__c != null)
                        originLoc = originLoc + cfsPorts[i].Zip_Code__c + ' ';
                    if (cfsPorts[i].Country__c != '' && cfsPorts[i].Country__c != null)
                        originLoc = originLoc + cfsPorts[i].Country__c + ' ';
                    component.set("v.OriginLocation", originLoc);
                }
            }
        } else {
            component.set("v.showErrorMessage", false);
            component.set("v.errorMessage", "");
            if (recTerm == 'P' && originCode.length > 4)
                this.getOriginLocationDetails(component, event, recTerm, originCode);
            else if (recTerm == 'D' && originCode.length > 3)
                this.getOriginLocationDetails(component, event, recTerm, originCode);
            else {
                component.set("v.OriginLocation", "");
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Entered Origin code not found. Please re-check the Origin Code.");
            }
        }
        bookingWrapper.originLocation = component.get("v.OriginLocation");
        component.set("v.bookingWrapper", bookingWrapper);
    },
    getOriginLocationDetails: function (component, event, recTerm, originCode) {
        var action = component.get("c.getLocationDetails");
        action.setParams({
            termCode: recTerm,
            locCode: originCode
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                console.log("response.getReturnValue() : ", response.getReturnValue());
                var responseData = response.getReturnValue();
                component.set("v.OriginLocation", responseData);
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
            }
            else {
                console.log("response.getReturnValue() : ", response.getReturnValue());
                component.set("v.OriginLocation", "");
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Entered Origin code not found. Please re-check the Origin Code.");
            }
            var bookingWrapper = component.get("v.bookingWrapper");
            bookingWrapper.originLocation = component.get("v.OriginLocation");
            component.set("v.bookingWrapper", bookingWrapper);
        });
        $A.enqueueAction(action);
    },
    submit: function (component, event) {
        component.set("v.isSpinner", true);
        var bookingWrapper = component.get("v.bookingWrapper");
        console.log(bookingWrapper);
        var action = component.get("c.addNewCargo");
        action.setParams({
            "listCargoWrapper": bookingWrapper
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cargo = response.getReturnValue();
                component.set("v.bookingWrapper", cargo);
                //component.set("v.bookingRequestNumber",cargo.booking.Id);
                this.bookingDetail(component, event, cargo.booking.Id);
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
            component.set("v.isSpinner", false);
        });
        $A.enqueueAction(action);

    },
    bookingDetail: function (component, event, bookingId) {
        var action = component.get("c.bookingDetail");
        action.setParams({
            "bookingId": bookingId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var booking = response.getReturnValue();
                component.set("v.quoteNumber", booking.Quote_Number__c);
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);

    }
})