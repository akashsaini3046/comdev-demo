({
    changeHomeScreen : function(component,event) {
        var compName = event.getParam("HomeScreenComponent");
        var identifier = event.getParam("identifier");
        var userEmail = event.getParam("userEmail");
        var companyName = event.getParam("companyName");
        var firstName = event.getParam("firstName");
        var lastName = event.getParam("lastName");
        var userName = event.getParam("userName");
        var phoneNumber = event.getParam("phoneNumber");
        var businessLocationName = event.getParam("businessLocationName");
        var addressLine1 = event.getParam("addressLine1");
        var addressLine2 = event.getParam("addressLine2");
        var selectedCountry = event.getParam("selectedCountry");
        var selectedState = event.getParam("selectedState");
        var city = event.getParam("city");
        var zipCode = event.getParam("zipCode");
        var sendData = event.getParam("sendData");
        var newContact = event.getParam("newContact");
        var addNewAccount = event.getParam("addNewAccount");
        var addNewLocation =  event.getParam("addNewLocation");
        var accountRecordId = event.getParam("selectedAccountId");
        var contactRecordId = event.getParam("selectedContactId");
        var locationRecordId = event.getParam("selectedLocationId");
          
        component.set("v.DisplayComponent",compName);
        if(userEmail != null || userEmail != undefined){
            component.set("v.userEmail",userEmail);
        }
        if(contactRecordId != null || contactRecordId != undefined){
            component.set("v.contactRecordId",contactRecordId) 
        }
        if(locationRecordId != null || locationRecordId != undefined){
            component.set("v.locationRecordId",locationRecordId) 
        }
        if(accountRecordId != null || accountRecordId != undefined){
            component.set("v.accountRecordId",accountRecordId) 
        }
        if(addNewLocation != null || addNewLocation != undefined){
           component.set("v.addNewLocation",addNewLocation); 
        }
        if(addNewAccount != null || addNewAccount != undefined){
            component.set("v.addNewAccount",addNewAccount);
        }
        if(newContact != null || newContact != undefined){
           component.set("v.newContact",newContact); 
        }
        if(identifier != null || identifier != undefined){
            component.set("v.identifier",identifier);
        }
        if(companyName != null || companyName!= undefined){
            component.set("v.companyName",companyName);
        }
        if(firstName != null || firstName != undefined){
            component.set("v.userFirstName",firstName);
        }
        if(lastName != null || lastName != undefined){
            component.set("v.userLastName",lastName);
        }
        if(userName !=null || userName != undefined){
            component.set("v.userName",userName);
        }
        if(phoneNumber != null || phoneNumber != undefined){
            component.set("v.userPhoneNumber",phoneNumber);
        }
        if(businessLocationName !=null || businessLocationName != undefined){
            component.set("v.businessLocationName",businessLocationName);
        }
        if(addressLine1 != null || addressLine1 != undefined){
            component.set("v.addressLine1",addressLine1); 
        }
        if(addressLine2 != null || addressLine2 != undefined){
            component.set("v.addressLine2",addressLine2); 
        }
        if(selectedCountry != null || selectedCountry != undefined){
            component.set("v.selectedCountry",selectedCountry); 
        }
        if(selectedState != null || selectedState != undefined){
            component.set("v.selectedState",selectedState); 
        }
        if(city != null || city != undefined){
            component.set("v.city",city); 
        }
        if(zipCode != null || zipCode != undefined){
            component.set("v.zipCode",zipCode); 
        }
        if(sendData != null && sendData != undefined){
            component.set("v.sendData",sendData);
        }
        if(component.get('v.DisplayComponent') == 'c:CrowleyCommunity_CreateBusinessLocation' && newContact== false){
           var childCmp = component.find('childCmp');
           childCmp.sampleMethod(contactRecordId);   
        }
    },
    createUser : function(component,event){
      if(component.get("v.sendData") == true){
        var action = component.get("c.checkAccountExists");
        var accountParams = this.getAccountParams(component);  
        action.setParams({
            'accountDetails' : accountParams 
        });  
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var contactId = response.getReturnValue();
                if (contactId == 'EXCEPTION') {
                    component.set("v.userSuceessMsg", 'User already exists in the system. To reset your password, click on "Forgot Password".');
                    component.set("v.OnUserSuccess", true);
                } else if (contactId == 'EXCEPTION1') {
                    component.set("v.userSuceessMsg", 'User already exists in the system. To reset your password, click on "Forgot Password".');
                    component.set("v.OnUserSuccess", true);
                } else {
                    console.log('contactId '+contactId);
                    component.set("v.contactId", contactId);
                    component.set("v.OnUserSuccess", true);
                }
            } 
            else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
      }
    },
    navigateToLinkScreen : function(component,event){
      if(component.get("v.OnUserSuccess") == true){  
        var contactId = component.get("v.contactId");
        var action = component.get("c.registerUser");
        var userparams = this.getUserParams(component);
        action.setParams({
            'userDetails' : userparams,
            'contactId' : component.get("v.contactId")
        });  
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var IfUserCreated = response.getReturnValue();
                if (IfUserCreated === 'True') {
                    component.set("v.userSuceessMsg", 'User created successfully!! Activation link has been sent to your Email Id');   
                } else if (IfUserCreated === 'False') {
                    component.set("v.userSuceessMsg", 'User with this Email Id already exists. To reset password click on "Forgot Password".');     
                } else if (IfUserCreated === 'Exists') {
                    component.set("v.userSuceessMsg", 'Username already exists. Please enter a unique username.');    
                } else if (IfUserCreated === 'Error') {
                    component.set("v.userSuceessMsg", 'Something went wrong. Please try Again.');                    
                }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
      }
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },
    getUserParams : function(component){
        return {
            'firstName' : component.get("v.userFirstName"),
            'lastName' : component.get("v.userLastName"),
            'userEmail' : component.get("v.userEmail"),
            'userName' : component.get("v.userName"),
            'phoneNumber': component.get("v.userPhoneNumber"),
            'countryName' : component.get("v.selectedCountry"),
        }
    },
    getAccountParams : function(component){
        return {
            'companyName' : component.get("v.companyName"),
            'firstName' : component.get("v.userFirstName"),
            'lastName' : component.get("v.userLastName"),
            'userEmail' : component.get("v.userEmail"),
            'userName' : component.get("v.userName"),
            'phoneNumber': component.get("v.userPhoneNumber"),
            'businessLocationName' : component.get("v.businessLocationName"),
            'addressLine1': component.get("v.addressLine1"),
            'addressLine2': component.get("v.addressLine2"),
            'countryName' : component.get("v.selectedCountry"),
            'stateName' : component.get("v.selectedState"),
            'cityName' : component.get("v.city"),
            'zipCode' : component.get("v.zipCode"),
            'newContact' : component.get("v.newContact"),
            'addNewAccount' : component.get("v.addNewAccount"),
            'addNewLocation' : component.get("v.addNewLocation"),
            'accountRecordId' : component.get("v.accountRecordId"),
            'contactRecordId' : component.get("v.contactRecordId"),
            'locationRecordId' : component.get("v.locationRecordId")
        }
    }
})