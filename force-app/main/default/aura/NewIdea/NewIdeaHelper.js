({
	getURLParameter : function(key) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
			console.log('sParameterName -> ',sParameterName);
            if (sParameterName[0] == key) {
                return sParameterName[1];
            }
        }
        return '';
	},
    
    getIdeaDetails : function(cmp, helper, ideaId){
        var action = cmp.get("c.getIdeaDetails");
        action.setParams({ideaId: cmp.get("v.ideaId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('ideaRecord -> ',response.getReturnValue());
                cmp.set("v.ideaRecord", response.getReturnValue());
                console.log('Executed properly');
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getZonesList : function(cmp, helper){
        var action = cmp.get("c.getZonesList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getZonesList', response.getReturnValue());
                cmp.set("v.zonesOptions", JSON.parse(response.getReturnValue()) );
                // Let DOM state catch up.
                window.setTimeout(
                    $A.getCallback( function() {
                        // Now set our preferred value
                        if(cmp.get("v.ideaRecord")){
                            cmp.find("zones").set("v.value", cmp.get("v.ideaRecord").CommunityId);
                        }
                    }));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaStatuses : function(cmp, helper){
        var action = cmp.get("c.getIdeaStatuses");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaStatuses', JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaStatuses", JSON.parse(response.getReturnValue()));
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getCategories : function(cmp, helper){
        var action = cmp.get("c.getIdeaCategories");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaCategories', JSON.parse(response.getReturnValue()));
                cmp.set("v.categories", JSON.parse(response.getReturnValue()));
                helper.setCategoriesOptions(cmp);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getBenefits : function(cmp, helper){
        var action = cmp.get("c.getBenefits");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getBenefits', JSON.parse(response.getReturnValue()));
                cmp.set("v.benefits", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    setCategoriesOptions: function (cmp) {
        cmp.set("v.categoriesOptions", cmp.get("v.categories"));
        var categories = cmp.get("v.ideaRecord.Categories");
        console.log('categories', categories);
        var categoriesValues = [];
        if(categories){
            categoriesValues = categories.split(';');
        }
        console.log('categoriesValues', categoriesValues);
        // "values" must be a subset of values from "options"
        cmp.set("v.categoriesValues", categoriesValues);
    },
    
    saveIdea: function (cmp, event, helper) {
        console.log('ideaRecord -> ', cmp.get("v.ideaRecord"));
        var action = cmp.get("c.saveIdeaRecord");
        var param = { ideaRecord: cmp.get("v.ideaRecord") };
        console.log(param);
        action.setParams(param);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result -> ',result);
                if(result.Id){
                    cmp.set("v.ideaId", result.Id);
                    
                    // calling method of the child component "fileUploadCmp"
                    // for saving an uploaded file on Idea
                    
                    var childComponent = cmp.find("fileUploadCmp");
                    var message = childComponent.saveFile(result.Id);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });
        toastEvent.fire();
    },
    
    findSimilarIdeasAction : function(cmp, helper, communityId, title){
        var action = cmp.get("c.findSimilarIdeas");
        var param = { communityId : communityId, title : title };
        console.log('findSimilarIdeas param -> ', param);
        action.setParams(param);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('similar ideas -> ', response.getReturnValue());
                cmp.set("v.similarIdeas", response.getReturnValue());
                helper.showToast("Success", "The record has been updated successfully");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast('error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    getIdeaFieldDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaFieldDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('getIdeaFieldDescribe -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaFieldDescribe", JSON.parse(response.getReturnValue()));
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaDescribeResult -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaDescribe", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getRelevantRecordTypeId: function(cmp, helper, selectedZoneId){
        console.log('SELECTED ZONE ID '+selectedZoneId);
    	var action=cmp.get("c.getRecordTypeId");
        action.setParams({
    		zoneId : selectedZoneId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('getRelevantRecordTypeId state '+state);
            if (state === "SUCCESS") {
                console.log('ínside success');
                console.log('selectedZoneRecordTypeId -> ',response.getReturnValue());
                cmp.set("v.selectedZoneRecordTypeId", response.getReturnValue());
                helper.getRelevantPickListValues(cmp,helper,cmp.get("v.selectedZoneRecordTypeId"));
            }
             else {
                console.log(" getRelevantRecordTypeId failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    getRelevantPickListValues: function(cmp,helper,recordTypeId){
        console.log('getRelevantPickListValues  '+recordTypeId);
        var action=cmp.get("c.fetchRecordTypeSpecificPickListvalues");
        action.setParams({
    		recordTypeId : recordTypeId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PicklistValues -> ',response.getReturnValue());
                var statusData=response.getReturnValue()['Status'];
                var categoriesData=response.getReturnValue()['Categories'];
                var benefitsData=response.getReturnValue()['Benefits'];
                var status=[];
                var categories=[];
                var benefits=[];
                console.log('statusData '+JSON.stringify(statusData));
                console.log('categoriesData '+JSON.stringify(categoriesData));
                for(var key in statusData)
                    status.push({label:key,value:statusData[key]});
                for(var key in categoriesData)
                    categories.push({label:key,value:categoriesData[key]});
                for(var key in benefitsData)
                    benefits.push({label:key,value:benefitsData[key]});
                cmp.set("v.benefits",benefits);
                cmp.set("v.ideaStatuses",status);
                cmp.set("v.categoriesOptions",categories);
                
                helper.getIdeasListAction(cmp, helper);
                
                    
            }
            else {
                console.log(" getRelevantPickListValues failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
    }
})