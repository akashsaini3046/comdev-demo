({
    parseResponse : function(component, event, helper) {
        var results = component.get("v.RatingApiResponseWrapper")["result"];
        component.set("v.rates", results);
		console.log("results ",JSON.stringify(results));
        var bookingWrapper = component.get("v.bookingWrapper");
        component.set("v.originLocation", bookingWrapper.originLocation);
        component.set("v.destinationLocation", bookingWrapper.destinationLocation);
        component.set("v.originLocationCode", bookingWrapper.originLocationCode);
        component.set("v.destinationLocationCode", bookingWrapper.destinationLocationCode);
    },
})