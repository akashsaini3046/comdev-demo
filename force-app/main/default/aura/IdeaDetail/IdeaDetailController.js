({
	doInit : function(cmp, event, helper) {
        console.log('Inside detail init');
        console.log('Idea Id '+cmp.get("v.ideaId"));
        var ideaId = cmp.get("v.ideaId");
        helper.getCurrentUserAction(cmp, helper, function(){
            helper.getIdeaDescribeResultAction(cmp, helper);
            helper.getIdeaFieldDescribeResultAction(cmp, helper);
           // var ideaId = helper.getURLParameter('id');
          //  helper.getExpertsGroupMembers(cmp,helper,ideaId);
               helper.getIdeaDetails(cmp, helper, ideaId);
                helper.getIdeaCommentsAction(cmp, helper, ideaId);
             console.log('Idea Id '+cmp.get("v.ideaId"));
           /* if(ideaId){
                cmp.set('v.ideaId', ideaId);
                helper.getExpertsGroupMembers(cmp,helper,ideaId);
                helper.getIdeaDetails(cmp, helper, ideaId);
                helper.getIdeaCommentsAction(cmp, helper, ideaId);
            }else{
                cmp.set('v.ideaRecord',{});
            }  */
        });
        
	},
    
    editHandler: function(cmp, event, helper){
        var ideaId = cmp.get("v.ideaId");
        window.location.href= $A.get("$Label.c.Edit_Idea_Page")+'?id='+ideaId;
    },
    
    deleteIdeaHandler: function(cmp, event, helper){
        helper.deleteIdeaAction(cmp, helper);
    },
    
    upvoteIdeaHandler: function(cmp, event, helper){
        var ideaId = cmp.get("v.ideaId");
        helper.upvoteIdeaAction(cmp, helper, ideaId);
    },
    
    downvoteIdeaHandler: function(cmp, event, helper){
        var ideaId = cmp.get("v.ideaId");
        helper.downvoteIdeaAction(cmp, helper, ideaId);
    },
    
    likeCommentHandler: function(cmp, event, helper){
        console.log('likeCommnet -> ', event.getSource().get("v.value"));
        helper.likeCommentAction(cmp, helper, event.getSource().get("v.value"));
    },
    
    unlikeCommentHandler: function(cmp, event, helper){
        console.log('unlikeCommnet -> ', event.getSource().get("v.value"));
        helper.unlikeCommentAction(cmp, helper, event.getSource().get("v.value"));
    },
    
    editCommentHandler: function(cmp, event, helper){
        console.log('editCommnet -> ', event.getSource().get("v.value"));
        window.location.href=$A.get("$Label.c.Edit_Idea_Comment_Page")+'?id='+event.getSource().get("v.value");
    },
    
    deleteCommentHandler: function(cmp, event, helper){
        console.log('deleteCommnet -> ', event.getSource().get("v.value"));
        helper.deleteCommentAction(cmp, helper, event.getSource().get("v.value"));
    },
    
    addCommentHandler: function(cmp, event, helper){
        helper.addCommentAction(cmp, helper);
    }
})