({
	getURLParameter : function(key) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
			console.log('sParameterName -> ',sParameterName);
            if (sParameterName[0] == key) {
                return sParameterName[1];
            }
        }
        return '';
        //return '087M00000008klqIAA';
	},
    
    getCurrentUserAction : function(cmp, helper, callback){
        var action = cmp.get("c.getUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('currUser -> ',response.getReturnValue());
                var currUser = response.getReturnValue();
                cmp.set("v.currUser", currUser);
                callback();
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaDetails : function(cmp, helper, ideaId){
        var action = cmp.get("c.getIdeaDetails");
        action.setParams({ideaId: cmp.get("v.ideaId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('ideaRecord -> ',response.getReturnValue());
                var ideaRecord = response.getReturnValue();
                cmp.set("v.attachmentURI", encodeURI($A.get("$Label.c.Idea_Community_Path") + '/servlet/fileField?entityId='+cmp.get("v.ideaId")+'&field=AttachmentBody'));
                if(ideaRecord.Categories)
                    ideaRecord.Categories = ideaRecord.Categories.replace(/;/g,', ');
                cmp.set("v.ideaRecord", ideaRecord);
                var ideaDescribe = cmp.get("v.ideaDescribe");
                if(!ideaDescribe.idea.updateable){
                    cmp.set("v.disableForm", (ideaRecord.Status == 'Implemented and Closed' ? true : false));
                }
                helper.checkIsUpvotedorDownvoted(cmp, helper);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }else{
                console.log('Unknown state');
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaCommentsAction : function(cmp, helper, ideaId){
        var action = cmp.get("c.getIdeaComments");
        action.setParams({ideaId: ideaId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ideaComments = JSON.parse(response.getReturnValue());
                console.log('ideaComments -> ',ideaComments);
                cmp.set("v.ideaComments", ideaComments);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }else{
                console.log(state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaFieldDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaFieldDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('getIdeaFieldDescribe -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaFieldDescribe", JSON.parse(response.getReturnValue()));
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getIdeaDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaDescribeResult -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaDescribe", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    upvoteIdeaAction : function(cmp, helper, ideaId){
        var action = cmp.get("c.upvoteIdea");
        action.setParams({ideaId: ideaId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('upvoteIdea successful -> ', response.getReturnValue());
            	helper.getIdeaDetails(cmp, helper, ideaId);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    checkIsUpvotedorDownvoted: function(cmp, helper){
        var ideaRecord = cmp.get('v.ideaRecord');
        var userRecord = cmp.get('v.currUser');
        cmp.set("v.isUpvoted", false);
        cmp.set("v.isDownvoted", false);
        if(ideaRecord && ideaRecord.Votes && ideaRecord.Votes.length>0){
            for(var i=0; i<ideaRecord.Votes.length; i++){
                if(ideaRecord.Votes[i].CreatedById == userRecord.Id){
                    if(ideaRecord.Votes[i].Type=='Up')
                        cmp.set("v.isUpvoted",true);
                    else
                        cmp.set("v.isDownvoted",true);
                }
            }
        }
    },
    
    downvoteIdeaAction : function(cmp, helper, ideaId){
        var action = cmp.get("c.downvoteIdea");
        action.setParams({ideaId: ideaId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('downvoteIdea successful -> ', response.getReturnValue());
            	helper.getIdeaDetails(cmp, helper, ideaId);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    likeCommentAction : function(cmp, helper, commentId){
        var ideaId = cmp.get("v.ideaId");
        var action = cmp.get("c.likeComment");
        action.setParams({ideaCommentId: commentId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	helper.refreshIdeaDetails(cmp, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    unlikeCommentAction : function(cmp, helper, commentId){
        var ideaId = cmp.get("v.ideaId");
        var action = cmp.get("c.unlikeComment");
        action.setParams({ideaCommentId: commentId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	helper.refreshIdeaDetails(cmp, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    editCommentAction : function(cmp, helper, commentId){
        // redirect to edit comment component
    },
    
    deleteCommentAction : function(cmp, helper, commentId){
        var ideaId = cmp.get("v.ideaId");
        var action = cmp.get("c.deleteComment");
        action.setParams({ideaCommentId: commentId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	helper.refreshIdeaDetails(cmp, helper);                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    addCommentAction : function(cmp, helper){
        var ideaId = cmp.get("v.ideaId");
        var newComment = cmp.get('v.newComment');
        newComment.IdeaId = ideaId;
        var action = cmp.get("c.addComment");
        action.setParams({param: JSON.stringify(newComment)});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('result -> ', response.getReturnValue());
            	helper.refreshIdeaDetails(cmp, helper);
                cmp.set("v.newComment",{});
                helper.showToast(cmp, helper, 'success', 'Success!', 'Comment added successfully');
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    deleteIdeaAction : function(cmp, helper){
        var ideaId = cmp.get("v.ideaId");
        var action = cmp.get("c.deleteIdeaRecord");
        action.setParams({ideaId: ideaId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                window.location.href= $A.get("$Label.c.Ideas_List_Page");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    refreshIdeaDetails: function(cmp, helper){
        var ideaId = cmp.get("v.ideaId");
        helper.getIdeaDetails(cmp, helper, ideaId);
        helper.getIdeaCommentsAction(cmp, helper, ideaId);
    },
    
    showToast: function(cmp, helper, type, title, message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type":type
        });
        toastEvent.fire();
    },
    getExpertsGroupMembers: function(cmp,helper,ideaId){
        var action = cmp.get("c.getExpertsGroupMembers");
    }
})