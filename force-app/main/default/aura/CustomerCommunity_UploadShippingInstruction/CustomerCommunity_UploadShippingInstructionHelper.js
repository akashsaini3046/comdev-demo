({
    createShippingRecord : function(component, event){
        var action = component.get("c.createShippingInstructionRecord");
        action.setParams({bookingRecordId: component.get("v.recordId")});
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS" && response.getReturnValue() != null) {
                console.log('@@@ - ' + response.getReturnValue());
                var headerId = response.getReturnValue();
                component.set("v.recordId",headerId);
                component.set("v.disableUploadButton", false);
            } 
            else{
                component.set("v.PageMsg", "Something went wrong. Please try Again !");
                component.set("v.isMsgVisible", true);
                component.set("v.disableUploadButton", true);
                component.set("v.recordId","");
            }
        });
        $A.enqueueAction(action);
    }
})