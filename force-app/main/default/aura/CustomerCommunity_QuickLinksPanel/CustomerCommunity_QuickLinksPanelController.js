({
    doInit : function(component, event, helper){
        var action = component.get("c.getQuickLinks");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.qlinkRecords", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
     }
})