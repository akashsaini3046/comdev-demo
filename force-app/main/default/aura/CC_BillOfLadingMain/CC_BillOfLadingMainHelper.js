({ 
	handleActive: function (cmp, event) {
        var tab = event.getSource();
        var tabName = tab.get('v.id');
        if (tabName.includes("tab-all-bols")) {
            this.injectComponent(cmp, 'c:CC_BillOfLadingsList', tab);
        }
    },
    
    injectComponent: function (cmp, name, target) {
        var attr = {};
        $A.createComponent(name, attr, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                $A.createComponent(name, attr, function (contentComponent, status, error) {
                    if (status === "SUCCESS") {
                        target.set('v.body', contentComponent);
                    } else {
                        throw new Error(error);
                    }
                });
                throw new Error(error);
            }
        });
    }
})