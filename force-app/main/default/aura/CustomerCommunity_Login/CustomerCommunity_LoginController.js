({
    getInput: function (component, event, helper) {
        helper.userLogin(component, event);
    },
    keyCheck: function (component, event, helper) {
        if (event.which == 13) {
            helper.userLogin(component, event);
        }
    },
    handlecloseModal: function (component, event, helper) {
        var isModalVisible = event.getParam("isModalVisible");
        if (isModalVisible === true) {
            component.set("v.isModalOpen", false);
        }
    },
    handleShowModal: function (component, event, helper) {
        component.set("v.isModalOpen", true);
    },
    closeModalBox: function (component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    showPasswordModalBox: function (component, event, helper) {
        component.set("v.isPasswordModalOpen", true);
    },
    closePasswordModalBox: function (component, event, helper) {
        component.set("v.isPasswordModalOpen", false);
    },
    closePasswordModal: function (component, event, helper) {
        var closeBox = event.getParam("closeModalBox");
        if (closeBox === true) {
            component.set("v.isPasswordModalOpen", false);
        }
    },
    closePasswordModalTimer: function (component, event, helper) {
        var closeBoxTimer = event.getParam("isFogetModalVisible");
        if (closeBoxTimer === true) {
            window.setTimeout($A.getCallback(function () {
                if (component.isValid()) {
                    component.set("v.isPasswordModalOpen", false);
                } else {
                    console.log('Component is Invalid');
                }
            }), 5000);
        }
    },
    closeResendModalBox : function(component,event,helper){
       component.set("v.isResendModalVisible", false);
    },
    openLinkResendModal : function(component,event,helper){
      component.set("v.isResendModalVisible", true);
    },
    closeLinkModalBox : function(component,event,helper){
        var closeBox = event.getParam("closeModal");
        if (closeBox === true) {
            component.set("v.isResendModalVisible", false);
        } 
    },
    closeResendLinkModalTimer : function(component,event,helper){
        var closeBoxTimer = event.getParam("isLinkModalVisible");
        if (closeBoxTimer === true) {
            window.setTimeout($A.getCallback(function () {
                if (component.isValid()) {
                    component.set("v.isResendModalVisible", false);
                } else {
                    console.log('Component is Invalid');
                }
            }), 5000);
        }
    }
});