({
	getBillOfLadingRecord : function(component) {
        console.log('Inside getBillOfLadingRecord() helper');
		var action = component.get("c.fetchBillOfLadingRecord");
        action.setParams({
            billOfLadingId : component.get("v.recordId")
        })
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state=="SUCCESS"){
                var value=response.getReturnValue(); 
                component.set("v.billOfLadingRecord",value);
                if(value.Voyages__r !== undefined)
                	component.set("v.voyageData",value.Voyages__r[0]);
                var ref='';
                var refNmbr=[];
                refNmbr.push(value.Consignee_Reference_Number__c,value.Customer_Billing_Reference_Number__c,value.Numeric_Reference_Number__c,value.Export_Identification_Number__c,
                            value.Export_Reference_Number__c,value.Inbound_Reference_Number__c,value.Option_4_Reference_Number__c,value.Special_Reference_Number__c,value.Shipper_Reference_Number__c,
                            value.Supplier_Reference_Number__c,value.Tax_Reference_Number__c,value.Value_Reference_Number__c);
                let refNmbrset = new Set(refNmbr);
                for(var elem of refNmbrset){
                      if(elem!=null && elem!='' )
                          ref+='\n'+elem;
                    }
                component.set("v.referenceNumbers",ref);
                var refNumberMap=[];
                var refNmbrLabels = {'CON REF':value.Consignee_Reference_Number__c,'CUST REF':value.Customer_Billing_Reference_Number__c,'NUM REF':value.Numeric_Reference_Number__c,'EI REF':value.Export_Identification_Number__c,
                                     'EXP REF':value.Export_Reference_Number__c,'INB REF':value.Inbound_Reference_Number__c,'OPT REF':value.Option_4_Reference_Number__c,'SPL REF':value.Special_Reference_Number__c,'SHP REF':value.Shipper_Reference_Number__c,
                                     'SUP REF':value.Supplier_Reference_Number__c,'TAX REF':value.Tax_Reference_Number__c,'VAL REF':value.Value_Reference_Number__c};
                for(var i in refNmbrLabels){
                    if(refNmbrLabels[i] !== undefined)
                    refNumberMap.push({key:i,value:refNmbrLabels[i]});
                }
                component.set("v.referenceNumberObject",refNumberMap); 
                if(value.Free_Text_Lines_DSC__c !== undefined)
                	var freeTextArray = value.Free_Text_Lines_DSC__c.split('\r\n');
                else
                    var freeTextArray = value.Free_Text_Lines_DSC__c;
                for(var x in freeTextArray)
                	console.log(freeTextArray[x]);
                component.set("v.freeTextArray",freeTextArray);
            }
            else if(state=="ERROR"){
                console.log("Error");
            }
        });
        $A.enqueueAction(action);
	},
    getPartyRecords : function(component) {
        var action = component.get("c.fetchPartyRecords");
        action.setParams({
            billOfLadingId : component.get("v.recordId")
        })
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state=="SUCCESS"){
                var value=response.getReturnValue();               
                component.set("v.listParty",value);
            }
            else if(state=="ERROR"){
                console.log("Error");
            }
        });
        $A.enqueueAction(action);
    },
    getBillItemRecords : function(component) {
        var action = component.get("c.fetchBillItemRecords");
        action.setParams({
            billOfLadingId : component.get("v.recordId")
        })
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state=="SUCCESS"){
                var value=response.getReturnValue();
                console.log('bill item ');
               
                for(var index in value){
                    value[index].Weight__c=value[index].Weight__c.toFixed(2);
                    value[index].Cube_Meters__c=value[index].Cube_Meters__c.toFixed(2);
                    value[index].Items_Text_DSC__c=value[index].Items_Text_DSC__c.split('\r\n');
                }
                component.set("v.listBillItem",value);
            }
            else if(state=="ERROR"){
                console.log("Error");
            }
        });
        $A.enqueueAction(action);
    },
   getChargeLineItems : function(component) {
        var action = component.get("c.fetchChargeLineItems");
        action.setParams({
            billOfLadingId : component.get("v.recordId")
        })
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state=="SUCCESS"){
                var value=response.getReturnValue();
                var amountPrepay=0;
                var amountCollect=0;
                console.log(value);
                for(var index in value){
                    if(value[index].Prepaid_Collect__c==='PP')
                        amountPrepay+=value[index].Amount__c;
                    if(value[index].Prepaid_Collect__c==='CO')
                        amountCollect+=value[index].Amount__c;
                 
                    value[index].Quantity__c=value[index].Quantity__c.toFixed(2);
                   
                    value[index].Rate__c=value[index].Rate__c.toFixed(2);
                   
                    value[index].Amount__c=value[index].Amount__c.toFixed(2);
                    
                                                           
                }
                amountPrepay=amountPrepay.toFixed(2);
                amountCollect=amountCollect.toFixed(2);
                component.set("v.amountPrepay",amountPrepay);
                component.set("v.amountCollect",amountCollect);
                
                component.set("v.listChargeLineItem",value);
            }
            else if(state=="ERROR"){
                console.log("Error");
            }
        });
		$A.enqueueAction(action);
   },
    getEquipmentRecords : function(component) {
        var action = component.get("c.fetchEquipmentRecords");
        action.setParams({
            billOfLadingId : component.get("v.recordId")
        })
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state=="SUCCESS"){
                var value=response.getReturnValue();
               
                component.set("v.listEquipment",value);
               
            }
            else if(state=="ERROR"){
                console.log("Error");
            }
        });
        $A.enqueueAction(action);
    },
     getCommunityName : function(component){
         console.log('inside getCommunityName');
        var action = component.get("c.getCommunityUrlPathPrefix");
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('888 '+state);
            if (state === "SUCCESS") {
                console.log('888 '+response.getReturnValue());
                component.set("v.communityName",response.getReturnValue());
            }
        });
         $A.enqueueAction(action);
     },
    	
    getRates : function(component){
        console.log(component.get("v.billOfLadingRecord.Booking_Number__c"));
        var bookingId = component.get("v.billOfLadingRecord.Booking_Number__c");
        var action = component.get("c.handleGetRatesTest");
         action.setParams({
            bookingId : bookingId
        })
          action.setCallback(this,function(response){
              var state=response.getState();
              console.log('state '+state);
              if(state=="SUCCESS"){
                  var responseData=response.getReturnValue();
                  console.log(responseData);
                  var result = responseData.result[0].CalculatedContributionResult;
                  for(var r in result){
                      var valueDataCost = result[r].ItemValues;
                      for(var v in valueDataCost){
                          var valuesGroup = valueDataCost[v].ValuesGroup;
                          for(var vg in valuesGroup){
                              var DocValuesData = valuesGroup[vg].DocValuesData;
                          }
                      }
                  }
                  component.get("v.ratingAPIResponse",responseData);
                  
              }
              else if(state=="ERROR"){
                console.log("Error");
            }
          });
        $A.enqueueAction(action);
    },
    
})