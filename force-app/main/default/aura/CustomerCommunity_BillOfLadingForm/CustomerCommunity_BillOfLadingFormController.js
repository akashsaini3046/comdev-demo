({
	doInit : function(component, event, helper) {
        helper.getCommunityName(component);
		helper.getBillOfLadingRecord(component);
        helper.getPartyRecords(component);
        helper.getBillItemRecords(component);
        helper.getChargeLineItems(component);
        helper.getEquipmentRecords(component);
       
	},
    closeRecord : function(component, event, helper) {
        if(component.get("v.communityName")!== null)
        	window.location='/'+component.get("v.communityName")+'/s/bill-of-lading/Bill_Of_Lading__c/Recent?Bill_Of_Lading__c-filterId=00B0t000001ysSm';
        if(component.get("v.communityName")=== null)
            window.location='/lightning/o/Bill_Of_Lading__c/list?filterName=Recent';
    },
    printRecord : function(component, event, helper) {
        window.print();
    },
    getRates : function(component, event, helper){
        component.set("v.showNewRatesModal",true);
        helper.getRates(component);
    },
    closeModal : function(component, event, helper){
        component.set("v.showNewRatesModal",false);
    }
    
})