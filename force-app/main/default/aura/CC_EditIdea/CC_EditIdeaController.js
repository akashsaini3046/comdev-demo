({
	doInit : function(cmp, event, helper) {
        helper.getIdeaDescribeResultAction(cmp, helper);
        helper.getIdeaFieldDescribeResultAction(cmp, helper);
		var ideaRecord=cmp.get("v.ideaRecord");
        console.log(ideaRecord);
        var ideaId = ideaRecord.Id ; 
        if(ideaId){
            cmp.set('v.ideaId', ideaId);
            helper.getIdeaDetails(cmp, helper, ideaId);
            helper.getZonesList(cmp, helper);
        }
    },

    handleCategoriesChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        var selectedOptionValue = event.getParam("value");
        var categories = selectedOptionValue.toString().split(',').join(';');
        console.log('handleCategoriesChange : categories -> ', categories);
        cmp.set('v.ideaRecord.Categories', categories);
        console.log('Categories values -> ',cmp.get('v.categoriesValues'));
    },
    
    saveIdea: function(cmp, event, helper){
        helper.saveIdea(cmp, event, helper);
    },
       
    removeAttachment: function(cmp){
        cmp.set("v.ifRemoveAttachment", true);
    },
    
    undoRemoveAttachment: function(cmp){
        cmp.set("v.ifRemoveAttachment", false);
    },
    
    handleFileUploadEvent: function(cmp, event, helper){
        var state = event.getParam("state");
        if(state=="SUCCESS"){
            helper.showToast("success", "Success", "The record has been updated successfully");
            cmp.set("v.displayEditIdea",false);
            cmp.set("v.displayIdeaDetail",true);        
        }else{
            helper.showToast("error","Failed", event.getParam("message"));
        }
    },
    handleAllIdeasOnclick: function(cmp){
        cmp.set("v.displayIdeasList",true);
        cmp.set("v.displayIdeaDetail",false);
        cmp.set("v.displayEditIdea",false);
        cmp.set("v.displayNewIdea",false);
    },
    handleIdeaDetail: function(cmp){
        cmp.set("v.displayIdeasList",false);
        cmp.set("v.displayIdeaDetail",true);
        cmp.set("v.displayEditIdea",false);
        cmp.set("v.displayNewIdea",false);
    },
    handlePostIdea: function(cmp){
        cmp.set("v.displayIdeasList",false);
        cmp.set("v.displayIdeaDetail",false);
        cmp.set("v.displayEditIdea",false);
        cmp.set("v.displayNewIdea",true);
    }
})