({
    initiateChat : function(component) {
        this.initESW(null);
        if (!window.embedded_svc) {
            console.log('Entered Init');
            var s = document.createElement('script'); 
            console.log('@@@ Element - '+s);
            s.setAttribute('text', $A.get("$Resource.Chatbot_Init"));
            console.log('@@@ Element Attribute - '+s);
            s.onload = function() {
                console.log('Entered Onload');
                this.initESW(null);
            };
            document.body.appendChild(s);
        } else {
            console.log('Entered Init Else');
            this.initESW('https://service.force.com');
        }
    },
    initESW : function(component) {
        console.log('Entered InitESW');
        embedded_svc.settings.displayHelpButton = true; //Or false
		embedded_svc.settings.language = ''; //For example, enter 'en' or 'en-US'
        
		embedded_svc.settings.enabledFeatures = ['LiveAgent'];
		embedded_svc.settings.entryFeature = 'LiveAgent';

		embedded_svc.init(
			'https://crowley2--comdev.cs77.my.salesforce.com',
			'https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity',
			gslbBaseURL,
			'00D0t0000000vwZ',
			'Chat_Support',
			{
				baseLiveAgentContentURL: 'https://c.la2-c2cs-ord.salesforceliveagent.com/content',
				deploymentId: '5720t0000008OSK',
				buttonId: '5730t0000008OSj',
				baseLiveAgentURL: 'https://d.la2-c2cs-ord.salesforceliveagent.com/chat',
				eswLiveAgentDevName: 'EmbeddedServiceLiveAgent_Parent04I0t0000000043EAA_16a490d16a6',
				isOfflineSupportEnabled: false
			}
		);
    }
})