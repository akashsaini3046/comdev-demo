({
    onRender : function(component,event,helper){
      helper.getPicklistValues(component, event);
    },
    createUserRecord : function(component, event, helper) {
        helper.createUserRecord(component,event);
    },
    showInputBox : function(component,event){
        var selectedDiv = event.currentTarget.id;
        $A.util.addClass(component.find(selectedDiv), 'dock-search'); 
    },
    removeInputBox : function(component,event){
        var selectedDiv =  component.find('businessLocation');
        $A.util.removeClass(selectedDiv, 'dock-search');        
    }
})