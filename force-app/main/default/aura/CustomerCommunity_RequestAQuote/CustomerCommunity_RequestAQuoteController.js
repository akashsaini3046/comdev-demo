({
    handleActive: function (cmp, event, helper) {
        helper.handleActive(cmp, event);
    },

    handleComponentEvent: function (cmp, event) {
        var quoteId = event.getParam("quoteId");
        var quoteName = event.getParam("quoteName");
        var selectedTabId = event.getParam("selectedTabId");
        console.log("selectedTabId:" + selectedTabId);
        switch (selectedTabId) {
            case 'tab-new-quote':
                cmp.set("v.reQuoteRecordId", quoteId);
                cmp.find("tabs").set("v.selectedTabId", selectedTabId);
                break;
            case 'tab-view-details':
                cmp.set("v.showQuoteDetail", false);
                cmp.set("v.quoteId", quoteId);
                cmp.set("v.quoteLabel", "Quote (" + quoteName + ")");
                cmp.set("v.showQuoteDetail", true);
                cmp.find("tabs").set("v.selectedTabId", selectedTabId);
                break;
        }
    }
})