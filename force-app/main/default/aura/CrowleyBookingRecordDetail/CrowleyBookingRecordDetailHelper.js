({
    fetchBookingAndRelatedData : function(component, event, helper) {
        
        this.fetchFieldsDefinition(component, event, helper);
        
        var bookingId = component.get("v.recordId");
        var action = component.get("c.getBookingDetail");
        action.setParams({
            bookingId : bookingId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log(JSON.stringify(responseData));
                component.set("v.bookingUrl", responseData["bookingUrl"]);
                this.setParties(component, responseData);
                this.setTranports(component, responseData);
                this.setStops(component, responseData);
                this.setVoyages(component, responseData);
                this.setCommodities(component, responseData);
                this.setRequirements(component, responseData);
                this.setFreightDetails(component, responseData);
                this.setShipments(component, responseData);
                this.setBookingRemarks(component, responseData);
                this.setDockReceipts(component, responseData);
                this.setEquipments(component, responseData);
                this.setCustomerNotifications(component, responseData);
                this.setBolNumbers(component, responseData);
                this.isHazardous(component, responseData);
            } else if (state === "ERROR"){
                console.log("Error in controller");
            } else {
                console.log("Error Unknown !");
            }
        });
        $A.enqueueAction(action);
    },
    sendEmailPDF : function (component, event, helper){
        var action = component.get("c.sendEmailPDF");
        var emailAddress = component.find('emailIds').get('v.value');
        var bookingId = component.get("v.recordId");
        component.set("v.showSpinnerModal", true);
        action.setParams({
            bookingId : bookingId,
            emailAddress : emailAddress
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS"){
                toastEvent.setParams({
                    message: 'Email Sent Successfully',
                    type:'success'
                });               
            }else{
                toastEvent.setParams({
                    message: 'Error sending Email',
                    type:'error'
                }); 
            }
            component.set("v.showSpinnerModal", false);
            toastEvent.fire();  
            component.set("v.isSendEmail", false);
        });
        $A.enqueueAction(action);
        console.log("sendEmailPDF Call");
    },
    isHazardous : function (component, responseData) {
        if(typeof responseData["Is_Hazardous__c"] !== "undefined"){
            var hazardous = responseData["Is_Hazardous__c"];
            console.log(hazardous);            
            component.set("v.isHazardous", (hazardous[0]==='true'));
        }
    },
    setVoyages : function (component, responseData) {
        if(typeof responseData["voyages"] !== "undefined"){
            var voyages = responseData["voyages"];
            var finalVoyages = [];
            for(var i = 0 ; i < voyages.length; i++){
                var voyage = {};
                voyage.Id = voyages[i].split("___")[0];
                voyage.Name = voyages[i].split("___")[1];
                finalVoyages.push(voyage);
            }
            component.set("v.voyages", finalVoyages);
        }
    },
    setEquipments : function (component, responseData) {
        if(typeof responseData["equipments"] !== "undefined"){
            var equipments = responseData["equipments"];
            var finalEquipments = [];
            for(var i = 0 ; i < equipments.length; i++){
                var equipment = {};
                equipment.Id = equipments[i].split("___")[0];
                equipment.Name = equipments[i].split("___")[1];
                finalEquipments.push(equipment);
            }
            component.set("v.equipments", finalEquipments);
        }
    },
    setCommodities : function (component, responseData) {
        if(typeof responseData["commodities"] !== "undefined"){
            var commodities = responseData["commodities"];
            var finalCommodities = [];
            for(var i = 0 ; i < commodities.length; i++){
                var commodity = {};
                commodity.Id = commodities[i].split("___")[0];
                commodity.Name = commodities[i].split("___")[1];
                finalCommodities.push(commodity);
            }
            component.set("v.commodities", finalCommodities);
        }
    },
    setRequirements : function (component, responseData) {
        if(typeof responseData["requirements"] !== "undefined"){
            var requirements = responseData["requirements"];
            var finalRequirements = [];
            for(var i = 0 ; i < requirements.length; i++){
                var Requirement = {};
                Requirement.Id = requirements[i].split("___")[0];
                Requirement.Name = requirements[i].split("___")[1];
                finalRequirements.push(Requirement);
            }
            component.set("v.requirements", finalRequirements);
        }
    },
    setFreightDetails : function (component, responseData) {
        if(typeof responseData["freightDetails"] !== "undefined"){
            var freightDetails = responseData["freightDetails"];
            var finalFreightDetails = [];
            for(var i = 0 ; i < freightDetails.length; i++){
                var freightDetail = {};
                freightDetail.Id = freightDetails[i].split("___")[0];
                freightDetail.Name = freightDetails[i].split("___")[1];
                finalFreightDetails.push(freightDetail);
            }
            component.set("v.freightDetails", finalFreightDetails);
        }
    },
    setBolNumbers : function (component, responseData) {
        if(typeof responseData["bolNumbers"] !== "undefined"){
            var bolNumbers = responseData["bolNumbers"];
            component.set("v.bolNumbers", bolNumbers);
        }
    },
    setShipments : function (component, responseData) {
        if(typeof responseData["shipments"] !== "undefined"){
            var shipments = responseData["shipments"];
            var finalShipments = [];
            for(var i = 0 ; i < shipments.length; i++){
                var shipment = {};
                shipment.Id = shipments[i].split("___")[0];
                shipment.Name = shipments[i].split("___")[1];
                finalShipments.push(shipment);
            }
            component.set("v.shipments", finalShipments);
        }
    },
    setBookingRemarks : function (component, responseData) {
        if(typeof responseData["bookingRemarks"] !== "undefined"){
            var bookingRemarks = responseData["bookingRemarks"];
            var finalBookingRemarks = [];
            for(var i = 0 ; i < bookingRemarks.length; i++){
                var bookingRemark = {};
                bookingRemark.Id = bookingRemarks[i].split("___")[0];
                bookingRemark.Name = bookingRemarks[i].split("___")[1];
                finalBookingRemarks.push(bookingRemark);
            }
            component.set("v.bookingRemarks", finalBookingRemarks);
        }
    },
    setDockReceipts : function (component, responseData) {
        if(typeof responseData["dockReceipts"] !== "undefined"){
            var dockReceipts = responseData["dockReceipts"];
            var finalDockReceipts = [];
            for(var i = 0 ; i < dockReceipts.length; i++){
                var dockReceipt = {};
                dockReceipt.Id = dockReceipts[i].split("___")[0];
                dockReceipt.Name = dockReceipts[i].split("___")[1];
                finalDockReceipts.push(dockReceipt);
            }
            component.set("v.dockReceipts", finalDockReceipts);
        }
    },
    setCustomerNotifications : function (component, responseData) {
        if(typeof responseData["customerNotifications"] !== "undefined"){
            var customerNotifications = responseData["customerNotifications"];
            var finalCustomerNotifications = [];
            for(var i = 0 ; i < customerNotifications.length; i++){
                var customerNotification = {};
                customerNotification.Id = customerNotifications[i].split("___")[0];
                customerNotification.Name = customerNotifications[i].split("___")[1];
                finalCustomerNotifications.push(customerNotification);
            }
            component.set("v.customerNotifications", finalCustomerNotifications);
        }
    },
    setStops : function (component, responseData) {
        if(typeof responseData["stops"] !== "undefined"){
            var stops = responseData["stops"];
            var finalStops = [];
            for(var i = 0 ; i < stops.length; i++){
                var stop = {};
                stop.Id = stops[i].split("___")[0];
                stop.Name = stops[i].split("___")[1];
                finalStops.push(stop);
            }
            component.set("v.stops", finalStops);
        }
    },
    setTranports : function (component, responseData) {
        if(typeof responseData["transports"] !== "undefined"){
            var transports = responseData["transports"];
            var finalTransports = [];
            for(var i = 0 ; i < transports.length; i++){
                var transport = {};
                transport.Id = transports[i].split("___")[0];
                transport.Name = transports[i].split("___")[1];
                finalTransports.push(transport);
            }
            component.set("v.transports", finalTransports);
        }
    },
    setParties : function (component, responseData) {
        if(typeof responseData["parties"] !== "undefined"){
            var parties = responseData["parties"];
            var finalParties = [];
            for(var i = 0 ; i < parties.length; i++){
                var party = {};
                party.Id = parties[i].split("___")[0];
                party.Type__c = parties[i].split("___")[1];
                finalParties.push(party);
            }
            component.set("v.parties", finalParties);
        }
    },
    fetchFieldsDefinition : function (component, event, helper) {
        var action = component.get("c.getFieldsDefinition");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log(JSON.stringify(responseData));
                if(typeof responseData["bookingFields"] !== "undefined"){
                    component.set("v.bookingFields", responseData["bookingFields"]);
                }
                if(typeof responseData["bookingFieldsServiceType"] !== "undefined"){
                    component.set("v.bookingFieldsServiceType", responseData["bookingFieldsServiceType"]);
                }
                if(typeof responseData["bookingFieldsIMType"] !== "undefined"){
                    component.set("v.bookingFieldsIMType", responseData["bookingFieldsIMType"]);
                }
                if(typeof responseData["bookingFieldsTMSType"] !== "undefined"){
                    component.set("v.bookingFieldsTMSType", responseData["bookingFieldsTMSType"]);
                }
                if(typeof responseData["bookingFieldsCustomerOrigin"] !== "undefined"){
                    component.set("v.bookingFieldsCustomerOrigin", responseData["bookingFieldsCustomerOrigin"]);
                }
                if(typeof responseData["bookingFieldsCustomerDestination"] !== "undefined"){
                    component.set("v.bookingFieldsCustomerDestination", responseData["bookingFieldsCustomerDestination"]);
                }
                if(typeof responseData["bookingFieldsConnectAtLoc"] !== "undefined"){
                    component.set("v.bookingFieldsConnectAtLoc", responseData["bookingFieldsConnectAtLoc"]);
                }
                if(typeof responseData["bookingFieldsConnectingCarrier"] !== "undefined"){
                    component.set("v.bookingFieldsConnectingCarrier", responseData["bookingFieldsConnectingCarrier"]);
                }
                if(typeof responseData["bookingFieldsConnectToLoc"] !== "undefined"){
                    component.set("v.bookingFieldsConnectToLoc", responseData["bookingFieldsConnectToLoc"]);
                }
                if(typeof responseData["partyFields"] !== "undefined"){
                    component.set("v.partyFields", responseData["partyFields"]);
                }
                if(typeof responseData["transportFields"] !== "undefined"){
                    component.set("v.transportFields", responseData["transportFields"]);
                }
                if(typeof responseData["stopFields"] !== "undefined"){
                    component.set("v.stopFields", responseData["stopFields"]);
                }
                if(typeof responseData["bookingRemarkFields"] !== "undefined"){
                    component.set("v.bookingRemarkFields", responseData["bookingRemarkFields"]);
                }
                if(typeof responseData["shipmentFields"] !== "undefined"){
                    component.set("v.shipmentFields", responseData["shipmentFields"]);
                }
                if(typeof responseData["voyageFields"] !== "undefined"){
                    component.set("v.voyageFields", responseData["voyageFields"]);
                }
                if(typeof responseData["dockReceiptFields"] !== "undefined"){
                    component.set("v.dockReceiptFields", responseData["dockReceiptFields"]);
                }
                if(typeof responseData["freightDetailFields"] !== "undefined"){
                    component.set("v.freightDetailFields", responseData["freightDetailFields"]);
                }
                if(typeof responseData["commodityFields"] !== "undefined"){
                    component.set("v.commodityFields", responseData["commodityFields"]);
                }
                if(typeof responseData["requirementFields"] !== "undefined"){
                    component.set("v.requirementFields", responseData["requirementFields"]);
                }
                if(typeof responseData["equipmentFields"] !== "undefined"){
                    component.set("v.equipmentFields", responseData["equipmentFields"]);
                }
                if(typeof responseData["customerNotificationFields"] !== "undefined"){
                    component.set("v.customerNotificationFields", responseData["customerNotificationFields"]);
                }
            } else if (state === "ERROR"){
                console.log("Error in controller");
            } else {
                console.log("Error Unknown !");
            }
        });
        $A.enqueueAction(action);
    },
    
    handleGetRates : function(component, event, helper){
        console.log('Entered handleGetRates');	       
        console.log('Record id : '+component.get('v.recordId'));       
        
        if(component.get('v.displayRates')==true)
            component.set('v.displayRates',false);
        else if(component.get('v.displayRates')==false)
            component.set('v.displayRates',true);
        var action =component.get("c.getRates");
        
        action.setParams({
            IdBooking: component.get('v.recordId')
        });
        
        console.log('Before setCallback');
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log('start res @@@');
                console.log(responseData);
                console.log('stop res @@@');
            }
            else if(state === "ERROR"){
                console.log("Error is : ");
                console.log(response.getError());
            }
        });
        console.log('After setCallback');
        $A.enqueueAction(action);
    },
    handleValidateIMDG : function(component, event, helper){
        
        this.showSpinner(component);
        console.log('Entered handleValidateIMDG in helper');
        var action= component.get("c.validateIMDG");
        action.setParams({
            IdBooking: component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('State is : '+state)
            if (state === "SUCCESS"){                
                
                var responseData = response.getReturnValue();
                if(responseData=="true"){
                    this.allAttachments(component, event, helper);
                }
                $A.get('e.force:refreshView').fire();
                //console.log('Response Data : '+responseData);
            }
            else if(state=="ERROR"){
                console.log("Error is : ");
                console.log(response.getError());
            }	
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);   
    },
    allAttachments : function(component, event, helper){
        this.showSpinner(component);
        var action= component.get("c.getAllAttachments");
        action.setParams({
            parentId: component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                component.set('v.bookingAttachments',responseData)
            }else{
                console.log(response.getError());             
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action); 
    },
    showSpinner: function(component) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component){
        component.set("v.spinner", false);
    },
    viewPDF : function(component, event, helper){
        var bookingId = component.get("v.recordId");
        var action = component.get("c.generatePDF");
        action.setParams({
            bookingId : bookingId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                console.log("");
            }else if(state === "ERROR"){
                console.log("Error is : ", response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})