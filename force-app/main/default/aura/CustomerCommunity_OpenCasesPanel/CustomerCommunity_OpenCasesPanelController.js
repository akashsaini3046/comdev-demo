({
    doInit : function(component, event, helper){
        var action = component.get("c.fetchCaseRecords");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.caseRecords", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
     },
      navHome : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Case"
        });
        homeEvent.fire();
    },
    gotoCaseURL : function (component, event, helper) {
    	var urlEvent = $A.get("e.force:navigateToURL");
    	var staticLabel = $A.get("$Label.c.CustomerCommunity_CaseURL");
    	urlEvent.setParams({
      		"url": staticLabel,
	        "target": "_self",
            "isredirect":"true"
    	});   
    	urlEvent.fire();
	},
    navigateToMyComponent : function(component, event, helper) {
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:CustomerCommunity_CreateCaseScreen",
    });
    evt.fire();
}
})