({
	doInit : function(component, event, helper) {
        console.log('DoInit Community Idea PARENT'); 
        helper.fireHighlightEvent(component, event, helper);
        var ideaId = helper.getURLParameter('id');
        console.log(ideaId);
        if(ideaId){
            component.set("v.displayIdeasList",false);
            console.log('DoInit');
            helper.getIdeaDetails(component,helper,ideaId);
            console.log('Idea Rec '+component.get("v.ideaRecord"));                     
        }
		 
        
	}
})