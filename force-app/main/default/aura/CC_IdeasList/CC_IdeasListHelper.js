({
    getIdeasListAction: function (cmp, helper) {
        var action = cmp.get("c.getIdeasList");
        action.setParams({
            communityId: cmp.get("v.selectedCommunityId"),
            statuses: cmp.get("v.selectedStatuses"),
            searchText: cmp.get("v.searchText"),
            categories: cmp.get("v.selectedCategories")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var ideas = response.getReturnValue();
                var ideasPage = [];
                for (var i = 0; i < ideas.length; i++) {
                    if (ideas[i].Categories) {
                        ideas[i].Categories = ideas[i].Categories.replace(/;/g, ', ');
                    }
                    if (i < cmp.get("v.pageSize")) {
                        ideasPage.push(ideas[i]);
                    }
                }
                cmp.set("v.ideasPage", ideasPage);
                cmp.set("v.ideasList", ideas);
                cmp.set("v.pageStart", 1);
                cmp.set("v.pageNumber", 1);
                var pageSize = cmp.get("v.pageSize");
                if (ideas.length <= pageSize) {
                    cmp.set("v.lastPageNumber", 1);
                    cmp.set("v.pageEnd", ideas.length);
                } else {
                    if (ideas.length % pageSize == 0) {
                        cmp.set("v.lastPageNumber", parseInt(ideas.length / pageSize));
                    } else {
                        cmp.set("v.lastPageNumber", parseInt(ideas.length / pageSize + 1));
                    }
                    cmp.set("v.pageEnd", pageSize);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            cmp.set("v.loading", false);
        });
        $A.enqueueAction(action);
    },
    upvoteIdeaAction: function (cmp, helper, ideaId) {

        var action = cmp.get("c.upvoteIdea");
        action.setParams({
            ideaId: ideaId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                helper.getIdeasListAction(cmp, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    downvoteIdeaAction: function (cmp, helper, ideaId) {

        var action = cmp.get("c.downvoteIdea");
        action.setParams({
            ideaId: ideaId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                helper.getIdeasListAction(cmp, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getCurrentUserAction: function (cmp, helper) {
        var action = cmp.get("c.getUser");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('currUser -> ', response.getReturnValue());
                var currUser = response.getReturnValue();
                cmp.set("v.currUser", currUser);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getZoneOptions: function(cmp, event, helper) {
        var action = cmp.get("c.getZonesList");
        var selectedZoneId;
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getZonesList', response.getReturnValue());
                var zones = JSON.parse(response.getReturnValue());
                cmp.set("v.zonesOptions", zones);
                if (zones && zones.length > 0) {
                    selectedZoneId = zones[0].Id;
                    cmp.set("v.selectedCommunityId", selectedZoneId);
                    console.log('selectedZoneId', selectedZoneId);
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    getZonesList: function (cmp, helper) {
        var action = cmp.get("c.getZonesList");
        var selectedZoneId;
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getZonesList', response.getReturnValue());
                var zones = JSON.parse(response.getReturnValue());
                cmp.set("v.zonesOptions", JSON.parse(response.getReturnValue()));
                if (zones && zones.length > 0) {
                    selectedZoneId = zones[0].Id;
                    cmp.set("v.selectedCommunityId", zones[0].Id);
                    console.log('yeeeee  ' + zones[0].Id);
                    helper.getRelevantRecordTypeId(cmp, helper, selectedZoneId);
                    console.log('yeeeee');
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    nextPage: function (cmp) {
        var pageSize = cmp.get("v.pageSize");
        var ideas = cmp.get("v.ideasList");
        var pageNumber = cmp.get("v.pageNumber");
        pageNumber += 1;
        cmp.set("v.pageNumber", pageNumber);

        var start = (pageNumber - 1) * pageSize;
        var end = pageNumber * pageSize;
        var ideasPage = [];
        for (var i = start; i < end && i < ideas.length; i++) {
            ideasPage.push(ideas[i]);
        }
        cmp.set("v.ideasPage", ideasPage);
        cmp.set("v.pageStart", start + 1);
        cmp.set("v.pageEnd", start + ideasPage.length);
    },

    previousPage: function (cmp) {

        if (cmp.get("v.pageNumber") < 2) return;

        var pageNumber = cmp.get("v.pageNumber");
        pageNumber -= 1;
        cmp.set("v.pageNumber", pageNumber);

        var pageSize = cmp.get("v.pageSize");
        var ideas = cmp.get("v.ideasList");
        var start = (pageNumber - 1) * pageSize;
        var end = pageNumber * pageSize;
        var ideasPage = [];
        for (var i = start; i < end && i < ideas.length; i++) {
            ideasPage.push(ideas[i]);
        }
        cmp.set("v.ideasPage", ideasPage);
        cmp.set("v.pageStart", start + 1);
        cmp.set("v.pageEnd", start + ideasPage.length);
    },

    calcPageNumber: function (cmp) {
        var pageSize = cmp.get("v.pageSize");
        var pageStart = cmp.get("v.pageStart");
        var pageNumber = cmp.get("v.pageNumber");
        if (pageStart < pageSize) {
            pageNumber = 1;
        }
        else if (Math.Mod((pageStart + 1), pageSize) == 0) {
            pageNumber = (pageStart + 1) / pageSize;
        } else {
            pageNumber = ((pageStart + 1) / pageSize) + 1;
        }
    },

    getIdeaDescribeResultAction: function (cmp, helper) {
        var action = cmp.get("c.getIdeaDescribe");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.ideaDescribe", JSON.parse(response.getReturnValue()));
                console.log('describe set');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function (cmp, helper, type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'sticky',
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    getRelevantRecTypIdPicklistValues: function (cmp, event, helper, selectedZoneId) {
        var zoneId;
        if(selectedZoneId){
            zoneId = selectedZoneId;
        }else{
            zoneId = null;
        }
        var action = cmp.get("c.fetchRecordTypeSpecificPickListvalues");
        action.setParams({
            zoneId: zoneId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('PicklistValues -> ', response.getReturnValue());
                    var statusData = response.getReturnValue()['Status'];
                    var categoriesData = response.getReturnValue()['Categories'];
                    var status = [];
                    var categories = [];
                    for (var key in statusData){
                        status.push({ label: key, value: statusData[key] });
                    }
                    for (var key in categoriesData){
                        categories.push({ label: key, value: categoriesData[key] });
                    }
                    cmp.set("v.ideaStatuses", status);
                    cmp.set("v.categoriesOptions", categories);
                }
                else {
                    console.log(" getRelevantPickListValues failed with state: " + state);
                }
        });
        $A.enqueueAction(action);
    },
})