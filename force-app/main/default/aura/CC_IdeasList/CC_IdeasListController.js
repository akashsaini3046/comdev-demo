({
    /**
     * @description Get permission values for Idea object and its fields.
     * Initialize picklist of communities, categories, idea statuses
     */
    doInit : function(cmp, event, helper) {
        console.log('Inside list init');
        helper.getIdeasListAction(cmp, helper);
        helper.getZoneOptions(cmp, event, helper);
        helper.getRelevantRecTypIdPicklistValues(cmp, event, helper);
        helper.getIdeaDescribeResultAction(cmp, helper);
        console.log('after init');
    },
    
    upvoteIdeaHandler : function(cmp, event, helper){
        helper.upvoteIdeaAction(cmp, helper, event.getSource().get("v.value"));        
    },
    
    downvoteIdeaHandler : function(cmp, event, helper){
        helper.downvoteIdeaAction(cmp, helper, event.getSource().get("v.value"));        
    },
    
    searchByZone : function(cmp, event, helper){   
        helper.getRelevantRecTypIdPicklistValues(cmp, event, helper, cmp.get("v.selectedCommunityId"));
        helper.getIdeasListAction(cmp, helper);
    },

    searchByStatus : function(cmp, event, helper){
        helper.getIdeasListAction(cmp, helper);
    },
    
    searchByCategories : function(cmp, event, helper){
        helper.getIdeasListAction(cmp, helper);
    },
    
    
    searchByText : function(cmp, event, helper){
        helper.getIdeasListAction(cmp, helper);
    },
    gotoNext : function(cmp, event, helper){
        helper.nextPage(cmp);
    },
    gotoPrevious : function(cmp, event, helper){
        helper.previousPage(cmp);
    },
    
    refreshIdeasHandler : function(cmp, event, helper){
        helper.getIdeasListAction(cmp, helper);
    },
    handleOpenIdea : function(cmp, event, helper){
        var ideaRecordId=event.target.id;
        var ideaList = cmp.get('v.ideasPage');
        var ideaRecord;
        ideaList.map((idea) => {
            if(idea.Id === ideaRecordId){
            	ideaRecord = idea;
        	}
        });
        console.log(ideaRecord);
        cmp.set("v.ideaRecord",ideaRecord);
        cmp.set("v.displayIdeasList",false);       
        cmp.set("v.displayEditIdea",false);
        cmp.set("v.displayNewIdea",false);
        cmp.set("v.displayIdeaDetail",true);
        
    },
    handleIdeaPost:function(cmp){
        cmp.set("v.displayIdeasList",false);       
        cmp.set("v.displayEditIdea",false);
        cmp.set("v.displayNewIdea",true);
        cmp.set("v.displayIdeaDetail",false);
    }
    
})