({
    doInit: function (component, event, helper) {
        var rates = component.get("v.rating");
        if(rates && rates !== null){
            console.log("rates :", JSON.stringify(rates) );
            var freightRates = rates[0].DocValuesData;
            var totalAmount = 0;
            var grandTotal = 0;
            var totalVat = 0;
            for(var i=0; i < freightRates.length ; i++){
                totalAmount += freightRates[i].AmountTarget;
                totalVat += 0;
            }
            grandTotal = totalAmount + totalVat;
            component.set("v.totalAmount", totalAmount);
            component.set("v.grandTotal", grandTotal);
            component.set("v.totalVat", totalVat);
            component.set("v.freightRates", freightRates);
        }
       
    },
    next: function (component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "next"
        });
        bookingRecordIdEvent.fire();
    },
    submit: function (component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction": "submit"
        });
        bookingRecordIdEvent.fire();
    },    
    saveAndBook: function (component, event, helper) {
        alert('Work in Progress');
    },
})