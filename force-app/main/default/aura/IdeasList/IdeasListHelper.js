({
     fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
	getIdeasListAction : function(cmp, helper) {
        console.log('Inside getIdeasListAction');
		cmp.get("v.selectedCommunityId");
		cmp.get("v.selectedStatuses");
		cmp.get("v.selectedCategories");
		cmp.get("v.searchText");
        
        var action = cmp.get("c.getIdeasList");
        action.setParams({
            communityId: cmp.get("v.selectedCommunityId"), 
            statuses: cmp.get("v.selectedStatuses"), 
            searchText: cmp.get("v.searchText"), 
            categories: cmp.get("v.selectedCategories")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('response of getIdeasListAction' + JSON.stringify(response.getReturnValue()));
                var ideas = response.getReturnValue();
                var ideasPage = [];
                for(var i=0; i<ideas.length; i++){
                    if(ideas[i].Categories){
                        ideas[i].Categories = ideas[i].Categories.replace(/;/g,', ');
                    }
                    if(i<cmp.get("v.pageSize")){
                        ideasPage.push(ideas[i]);
                    }
                }
                cmp.set("v.ideasPage", ideasPage);
                cmp.set("v.ideasList",ideas);
                cmp.set("v.pageStart", 1);
                cmp.set("v.pageNumber", 1);
                var pageSize = cmp.get("v.pageSize");
                if(ideas.length<=pageSize){
                    cmp.set("v.lastPageNumber", 1);
                    cmp.set("v.pageEnd", ideas.length);
                }else{
                    if(ideas.length % pageSize == 0){
                        cmp.set("v.lastPageNumber", parseInt(ideas.length/pageSize));
                    }else{
                        cmp.set("v.lastPageNumber", parseInt(ideas.length/pageSize + 1));
                    }
                    cmp.set("v.pageEnd", pageSize);
                }
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            cmp.set("v.loading",false);
        });
        $A.enqueueAction(action);
    },
    upvoteIdeaAction : function(cmp, helper, ideaId) {
        
        var action = cmp.get("c.upvoteIdea");
        action.setParams({
            ideaId: ideaId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                helper.getIdeasListAction(cmp, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    downvoteIdeaAction : function(cmp, helper, ideaId) {
        
        var action = cmp.get("c.downvoteIdea");
        action.setParams({
            ideaId: ideaId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                helper.getIdeasListAction(cmp, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getCurrentUserAction : function(cmp, helper){
        var action = cmp.get("c.getUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('currUser -> ',response.getReturnValue());
                var currUser = response.getReturnValue();
                cmp.set("v.currUser", currUser);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getZonesList : function(cmp, helper){
        var action = cmp.get("c.getZonesList");
        var selectedZoneId;
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getZonesList', response.getReturnValue());
                var zones = JSON.parse(response.getReturnValue());
                cmp.set("v.zonesOptions", JSON.parse(response.getReturnValue()) );
                if(zones && zones.length>0){
                    selectedZoneId=zones[0].Id;
                    cmp.set("v.selectedCommunityId", zones[0].Id);
                    console.log('yeeeee  '+zones[0].Id);
                    helper.getRelevantRecordTypeId(cmp, helper,selectedZoneId);
                    console.log('yeeeee');
                    //helper.getIdeasListAction(cmp, helper);
                }
                
               
                
                // Let DOM state catch up.
              /*  window.setTimeout(
                    $A.getCallback( function() {
                        // Now set our preferred value
                        if(zones && zones.length>0){
                            cmp.find("zones").set("v.value", zones[0].Id);
                        }
                    }));  */
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
   /* getIdeaStatuses : function(cmp, helper){
        var action = cmp.get("c.getIdeaStatuses");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaStatuses', JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaStatuses", JSON.parse(response.getReturnValue()));
                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCategories : function(cmp, helper){
        var action = cmp.get("c.getIdeaCategories");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaCategories', JSON.parse(response.getReturnValue()));
                cmp.set("v.categories", JSON.parse(response.getReturnValue()));
                helper.setCategoriesOptions(cmp);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },  
    
    setCategoriesOptions: function (cmp) {
        cmp.set("v.categoriesOptions", cmp.get("v.categories"));
        var categories = cmp.get("v.ideaRecord.Categories");
        console.log('categories', categories);
        var categoriesValues = [];
        if(categories){
            categoriesValues = categories.split(';');
        }
        console.log('categoriesValues', categoriesValues);
        cmp.set("v.categoriesValues", categoriesValues);
    },  */
    
    nextPage: function (cmp) {
        var pageSize = cmp.get("v.pageSize");
        var ideas = cmp.get("v.ideasList");
        var pageNumber = cmp.get("v.pageNumber");
        pageNumber += 1;
        cmp.set("v.pageNumber", pageNumber);
        
        var start = (pageNumber-1)*pageSize;
        var end = pageNumber*pageSize;
        var ideasPage = [];
        for( var i=start; i<end && i<ideas.length;i++){
        	ideasPage.push(ideas[i]);    
        }
        cmp.set("v.ideasPage", ideasPage);
        cmp.set("v.pageStart", start + 1);
        cmp.set("v.pageEnd", start + ideasPage.length);
    },
    
    previousPage: function(cmp){
        
        if(cmp.get("v.pageNumber")<2) return;
        
        var pageNumber = cmp.get("v.pageNumber");
        pageNumber -= 1;
        cmp.set("v.pageNumber", pageNumber);
        
        var pageSize = cmp.get("v.pageSize");
        var ideas = cmp.get("v.ideasList");
        var start = (pageNumber-1)*pageSize;
        var end = pageNumber*pageSize;
        var ideasPage = [];
        for( var i=start; i<end && i<ideas.length;i++){
        	ideasPage.push(ideas[i]);    
        }
        cmp.set("v.ideasPage", ideasPage);
        cmp.set("v.pageStart", start + 1);
        cmp.set("v.pageEnd", start + ideasPage.length);
    },
    
    calcPageNumber : function(cmp){
        var pageSize = cmp.get("v.pageSize");
        var pageStart = cmp.get("v.pageStart");
        var pageNumber = cmp.get("v.pageNumber");
        if(pageStart<pageSize){
            pageNumber = 1;
        }
        else if(Math.Mod((pageStart+1), pageSize) == 0){
            pageNumber = (pageStart+1)/pageSize;
        }else{
            pageNumber = ((pageStart+1)/pageSize) + 1;
        }
    },
    
    getIdeaDescribeResultAction : function(cmp, helper){
        var action = cmp.get("c.getIdeaDescribe");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('getIdeaDescribeResult -> ',JSON.parse(response.getReturnValue()));
                cmp.set("v.ideaDescribe", JSON.parse(response.getReturnValue()));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast(cmp, helper, 'error', 'Error!', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function(cmp, helper, type, title, message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type":type
        });
        toastEvent.fire();
    },
    getRelevantRecordTypeId: function(cmp, helper, selectedZoneId){
    	var action=cmp.get("c.getRecordTypeId");
        action.setParams({
    		zoneId : selectedZoneId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.selectedZoneRecordTypeId", response.getReturnValue());
                helper.getRelevantPickListValues(cmp,helper,cmp.get("v.selectedZoneRecordTypeId"));
            }
             else {
                console.log(" getRelevantRecordTypeId failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    getRelevantPickListValues: function(cmp,helper,recordTypeId){
        var action=cmp.get("c.fetchRecordTypeSpecificPickListvalues");
        action.setParams({
    		recordTypeId : recordTypeId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PicklistValues -> ',response.getReturnValue());
                var statusData=response.getReturnValue()['Status'];
                var categoriesData=response.getReturnValue()['Categories'];
                var status=[];
                var categories=[];
                console.log('statusData '+JSON.stringify(statusData));
                console.log('categoriesData '+JSON.stringify(categoriesData));
                for(var key in statusData)
                    status.push({label:key,value:statusData[key]});
                for(var key in categoriesData)
                    categories.push({label:key,value:categoriesData[key]});
                cmp.set("v.ideaStatuses",status);
                cmp.set("v.categoriesOptions",categories);
                
                helper.getIdeasListAction(cmp, helper);
                
                    
            }
            else {
                console.log(" getRelevantPickListValues failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
    }
})