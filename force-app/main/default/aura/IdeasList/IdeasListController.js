({
    /**
     * @description Get permission values for Idea object and its fields.
     * Initialize picklist of communities, categories, idea statuses
     */
    doInit : function(cmp, event, helper) {
        console.log('Inside list init');
        helper.fireHighlightEvent(cmp, event);
        helper.getZonesList(cmp, helper);
        helper.getIdeaDescribeResultAction(cmp, helper);
        
        //helper.getCategories(cmp, helper);
        //helper.getIdeaStatuses(cmp, helper); 
    },
 
    upvoteIdeaHandler : function(cmp, event, helper){
		helper.upvoteIdeaAction(cmp, helper, event.getSource().get("v.value"));        
    },
    
    downvoteIdeaHandler : function(cmp, event, helper){
		helper.downvoteIdeaAction(cmp, helper, event.getSource().get("v.value"));        
    },
    
    /**
     * @description Redirect to open the idea detail page lightning component
     */
    openIdea : function(cmp, event, helper){
        
        window.location.href = $A.get("$Label.c.Idea_Detail_Page")+'?id='+event.getSource().get("v.value");
    },

    /**
     * @description Filter Ideas by Business/Module function (Community/Zones)
     */
    searchByZone : function(cmp, event, helper){
        //cmp.set("v.loading",true);
        //cmp.set("v.selectedCommunityId", event.getSource().get("v.value"));    
 		helper.getRelevantRecordTypeId(cmp,helper,cmp.get("v.selectedCommunityId"));
		//helper.getIdeasListAction(cmp, helper);
    },

    /**    value="{!v.selectedStatuses}"
     * @description Filter ideas by status
     */
    searchByStatus : function(cmp, event, helper){
        //cmp.set("v.loading",true);
        console.log(event.getSource().get("v.value"));
       // cmp.set("v.selectedStatuses", event.getSource().get("v.value"));
		helper.getIdeasListAction(cmp, helper);
    },

    /**
     * @description Filter ideas by categories
     */
    searchByCategories : function(cmp, event, helper){
        //cmp.set("v.loading",true);
        cmp.set("v.selectedCategories", event.getSource().get("v.value"));
		helper.getIdeasListAction(cmp, helper);
    },

    /**
     * @description Search ideas by their title
     */
    searchByText : function(cmp, event, helper){
        //cmp.set("v.loading",true);
        console.log(event.getSource().get("v.value"));
        cmp.set("v.searchText", event.getSource().get("v.value"));
		helper.getIdeasListAction(cmp, helper);
    },
    gotoNext : function(cmp, event, helper){
        helper.nextPage(cmp);
    },
    gotoPrevious : function(cmp, event, helper){
        helper.previousPage(cmp);
    },

    /**
     * @description Open a new tab to create a new Idea.
     */
    addNewIdeaHandler : function(cmp){
        window.location.href = $A.get("$Label.c.New_Idea_Page");
    },
    openIdeaDetail : function(cmp,event){
        var ideaId = event.target.id ;
        window.location.href = $A.get("$Label.c.Idea_Detail_Page")+ '?id='+ideaId;       //$Label.c.Idea_Detail_Page + '?id='+ideaRecord.Id
    },

    /**
     * @description Refreshes the ideas list
     */
    refreshIdeasHandler : function(cmp, event, helper){
        helper.getIdeasListAction(cmp, helper);
    }
    
})