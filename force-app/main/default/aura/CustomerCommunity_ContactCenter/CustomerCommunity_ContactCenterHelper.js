({
	switchHomeScreen : function(component, event){
        var appEvent = $A.get("e.c:CustomerCommunity_HomeScreenEvent");
        appEvent.setParams({"HomeScreenComponent" : "c:CustomerCommunity_DashboardScreen" });
        appEvent.fire();
    }
})