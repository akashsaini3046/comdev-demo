({	
    getInput: function (component, event, helper) {
        helper.userLogin(component, event);
    },
    togglePassword : function(component, event, helper) {
        if(component.get("v.showpassword",true)){
            component.set("v.showpassword",false);
        }else{
            component.set("v.showpassword",true);
        }
    },
    keyCheck: function (component, event, helper) {
        if (event.which == 13) {
            helper.userLogin(component, event);
        }
    },
    navigateToRegisterScreen : function(component,event,helper){
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_Register"      
        });
        homeScreenEvent.fire();
    },
    navigateToForgetPasswordScreen: function(component,event,helper){
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_ForgetPassword"      
        });
        homeScreenEvent.fire();
    },
    handleRenderer: function(component,event,helper){
        var elem= component.find("getDeviceHeight").getElement();
        var winHeight = window.innerHeight;
        elem.style.height = winHeight + "px";
    }
})