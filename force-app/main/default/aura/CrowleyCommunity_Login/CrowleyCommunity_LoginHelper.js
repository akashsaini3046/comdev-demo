({
    userLogin: function (component, event) {
        var currentURL = window.location.origin+'/CustomerCommunity/s/';
        console.log('currentURL-->'+currentURL);
        var validForm = component.find('FormVal').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if (validForm) {
            var Username = component.get("v.Username");
            var Password = component.get("v.Password");
            var action = component.get("c.checkPortal");
            action.setParams({
                "username": Username,
                "password": Password,
                "currentURL":currentURL
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var responseServer = response.getReturnValue();
                    if (responseServer == "False") {
                        component.set("v.errorMessage", "Please check your username and password. If you still can't log in, contact your Crowley administrator.");
                        component.set("v.showErrorMessage",true);  
                    }
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    }
})