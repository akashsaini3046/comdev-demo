({
	doInit : function(component, event, helper) {
        helper.getBookingWrap(component,event,helper);
        helper.customerAccount(component,event);
        helper.fetchContactDetails(component, event);
        helper.fetchReceiptDeliveryTerms(component);
        helper.formatDate(component);
        helper.fetchCFSLocations(component, event);
	},
    nextProgress : function(component, event, helper) {
        helper.nextProgress(component, event, helper);
    },
    submit : function(component, event, helper) {
               
    },
    setAccount : function(component, event, helper) {
        helper.setAccount(component, event);
    },
    
    selectOriginDestination : function(component,event,helper){
        console.log(component.get("v.bookingWrapper"));
        component.set("v.selectedStep", "1");
        
    },
    selectPartyDetail : function(component,event,helper){
        console.log(component.get("v.bookingWrapper"));
        component.set("v.selectedStep", "2");
    },
    selectCargoDetail : function(component,event,helper){
        console.log(component.get("v.bookingWrapper"));
        component.set("v.selectedStep", "3");
    },
	selectScheduleSelection : function(component,event,helper){
        console.log(component.get("v.bookingWrapper"));
        component.set("v.selectedStep", "4");
    },
	selectFreightRates : function(component,event,helper){
        console.log(component.get("v.bookingWrapper"));
        component.set("v.selectedStep", "5");
    },
	selectReviewBook : function(component,event,helper){
        console.log(component.get("v.bookingWrapper"));
        component.set("v.selectedStep", "6");
    },
    handleComponentEvent : function(component,event,helper) {
        var selectedStep = event.getParam("selectedAction");
        if(selectedStep=='next'){
            helper.nextProgress(component, event, helper);
        }
        if(selectedStep=='completeLater'){
            helper.submit(component, event);
        }
    },
    handleTermLocationComponentEvent: function (component, event, helper) {
        var selectedCode = event.getParam("selectedCode");
        var location = event.getParam("location");
        var termType = event.getParam("termType");
        var code = event.getParam("code");
        var bookingWrapper = component.get("v.bookingWrapper");
        if(termType === 'receiptTerm'){
            component.set("v.OriginLocation", location);
            bookingWrapper.originLocation = location;
            bookingWrapper.originLocationCode = code;
            component.set("v.bookingWrapper", bookingWrapper);
        }
        if(termType === 'deliveryTerm'){
            component.set("v.destinationLocation", location);
            bookingWrapper.destinationLocation = location;
            bookingWrapper.destinationLocationCode = code;
            component.set("v.bookingWrapper", bookingWrapper);
        }
        console.log(selectedCode, location, termType, code);
    },
    getBookingWrap : function(component,event,helper) {
        helper.getBookingWrap(component,event,helper);
    },
    getDestinationLocation : function(component, event, helper){
        console.log('getDestinationLocation');
        helper.getDestinationLocation(component, event);
    },
    getOriginLocation : function(component, event, helper){
        console.log('getOriginLocation');
        helper.getOriginLocation(component, event);
    },
    addOriginStop : function(component, event, helper){
        var bookingWrap = component.get("v.bookingWrapper");
        bookingWrap.transportOrigin.listStop.push({});
        component.set("v.bookingWrapper", bookingWrap);
    },
    removeOriginStop : function(component, event, helper){
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.id;
        var bookingWrap = component.get("v.bookingWrapper");
        bookingWrap.transportOrigin.listStop.splice(index, 1);
        component.set("v.bookingWrapper", bookingWrap);
    },
    addDestinationStop : function(component, event, helper){
        var bookingWrap = component.get("v.bookingWrapper");
        bookingWrap.transportDestination.listStop.push({});
        component.set("v.bookingWrapper", bookingWrap);
    },
    removeDestinationStop : function(component, event, helper){
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.id;
        console.log(index);
        var bookingWrap = component.get("v.bookingWrapper");
        bookingWrap.transportDestination.listStop.splice(index, 1);
        component.set("v.bookingWrapper", bookingWrap);
    },
    newBooking: function (component, event, helper) {
        component.set("v.selectedStep", "1");
        component.set("v.OriginLocation", "");
        component.set("v.destinationLocation", "");
        component.set("v.bookingRequestNumber", "");
        component.set("v.ratingApiResponseWrapper", null);        
        helper.getBookingWrap(component, event, helper);
    },
})