({
    doInit: function (component, event, helper) {
        console.log("RatingApiResponseWrapper ", JSON.stringify(component.get("v.RatingApiResponseWrapper")));
        console.log("bookingWrapper ", component.get("v.bookingWrapper"));
        component.set("v.selectedRouteId", null);
        console.log(component.get("v.rateMap"));
        //var ratingResponse = component.get("v.RatingApiResponseWrapper");
        // if (ratingResponse && ratingResponse.success) {
        helper.parseResponse(component, event, helper);
        // }
    },
    next: function (component, event, helper) {
        var routeId = component.get("v.bookingWrapper").selectedRouteId;
        console.log("routeId next : ", routeId);
        if (routeId) {
            var bookingRecordIdEvent = component.getEvent("bookingEventData");
            bookingRecordIdEvent.setParams({
                "selectedAction": "next"
            });
            bookingRecordIdEvent.fire();
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Route Not Selected !",
                "message": "Please select the Route !",
                "type": "warning"
            });
            toastEvent.fire();
        }
    },
    selectedRateHandler: function (component, event, helper) {
        var routeId = event.getSource().get("v.value");
        console.log("selectedRouteId : ", routeId);
        var selectionType =event.getSource().get("v.name");
        var rates = component.get("v.rates");
        var totalAmount = 0;
        var newRates = rates.map((item) => {
            if (item.name.includes(selectionType)) {
                item.selected = routeId;
                for(var i=0;i<item.rates.length; i++)
                if(item.rates[i].RouteId==routeId){
                    //item.selectedAmount = item.rates[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0].TotalSum+' '+item.rates[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].TargetCurrency;
                    item.selectedAmount = item.rates[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0].TotalSum;
                    break;
                }                
            }
            totalAmount+=item.selectedAmount;
            return item;
        });
        console.log("newRates", newRates);
        component.set("v.rates", newRates);
        component.set("v.totalAmount", totalAmount);
        component.set("v.currency", 'USD');
        //var bookingWrapper = component.get("v.bookingWrapper");
        //bookingWrapper.selectedRouteId = routeId;
        //component.set("v.bookingWrapper", bookingWrapper);
    },
    showRateBreakup: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var selectionType = selectedItem.dataset.id;
        console.log(selectionType);
        var selectedItems = component.get("v.selectedItems");
        var rates = component.get("v.rates");
        var newRates = rates.map((item) => {
            if (item.name.includes(selectionType)) {
                for(var i=0;i<item.rates.length; i++)
                if(item.rates[i].RouteId==item.selected){
                    //item.selectedAmount = item.rates[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0].TotalSum+' '+item.rates[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].TargetCurrency;
                    selectedItems = item.rates[i].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup;
                    break;
                }                
            }
        });
        component.set("v.selectedItems", selectedItems);
        component.set("v.isDisplayBreakDown", true);
    },
    closeBreakDown: function (component, event, helper) {
        component.set("v.isDisplayBreakDown", false);
    }
})