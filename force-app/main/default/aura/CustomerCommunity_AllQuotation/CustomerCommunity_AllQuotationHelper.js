({
    getDataInDataTable: function (component) {
        var action = component.get("c.fetchQuoteData");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var quoteDetails = response.getReturnValue();
                if (quoteDetails != null) {
                    component.set('v.data', quoteDetails);
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
        });
        $A.enqueueAction(action);
    },
    
    getColumnDefinitions: function () {
        var columns = [
            {label: 'Quote', fieldName: 'quoteNumber', type: 'text', sortable: true},
            {label: 'Container Mode', fieldName: 'conatinerMode', type: 'text', sortable: true},
            {label: 'Origin Type', fieldName: 'originType', type: 'text', sortable: true},
            {label: 'Origin', fieldName: 'origin', type: 'text', sortable: true},
            {label: 'Destination Type', fieldName: 'destinationType', type: 'text', sortable: true},
            {label: 'Destination', fieldName: 'destination', type: 'text', sortable: true},
            {label: 'Status', fieldName: 'quoteStatus', type: 'text', sortable: true},
            {label: '', type: 'button', initialWidth: 135, typeAttributes: { label: 'View Details', name: 'view_details', title: 'Click to View Quote Details'}},
            {label: '', type: 'button', initialWidth: 135, typeAttributes: { label: 'Book', name: 're_quote', title: 'Re-Quote'}}
        ];
        return columns;
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var reverse = sortDirection !== 'asc';
        var action = component.get("c.fetchQuoteDataSorted");
        action.setParams({
            fieldName : fieldName,
            order : sortDirection
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var quoteDetails = response.getReturnValue();
                if (quoteDetails != null) {
                    component.set('v.data', quoteDetails);
                }else{
                    component.set('v.data', null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },
    
    navigateToReQuote : function(cmp, row) {
        var quoteRecordIdEvent = cmp.getEvent("quoteRecordIdEvent");
        quoteRecordIdEvent.setParams({
            "quoteId" : row.Id,
            "quoteName" : row.Name,
            "selectedTabId" : "tab-new-quote"
        });
        quoteRecordIdEvent.fire();
    },
    
    showQuoteDetails : function(cmp, row) {
        var quoteRecordIdEvent = cmp.getEvent("quoteRecordIdEvent");
        quoteRecordIdEvent.setParams({
            "quoteId" : row.Id,
            "quoteName" : row.Name,
            "selectedTabId" : "tab-view-details"
        });
        quoteRecordIdEvent.fire();
    }
})