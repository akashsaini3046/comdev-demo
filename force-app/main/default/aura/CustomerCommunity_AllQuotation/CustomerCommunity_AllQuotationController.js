({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', helper.getColumnDefinitions());
        helper.getDataInDataTable(cmp);
        helper.sortData(cmp, 'quoteNumber', 'desc');
    },
    
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },
    
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view_details':
                alert("Work in progress. Working on detail page !");
                break;
            case 're_quote':
                alert("Work in progress");
                break;
            default:
                alert("Work in progress");
                break;
        }
    }
})