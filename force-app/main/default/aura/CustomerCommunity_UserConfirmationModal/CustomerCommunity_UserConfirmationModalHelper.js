({
    closeToast: function (component) {
        var toastDiv = component.find('toastDiv');
        $A.util.removeClass(toastDiv, "slds-show");
        $A.util.addClass(toastDiv, "slds-hide");
    }
})