({
    onRender: function (component, event, helper) {
        var userMsg = component.get("v.userStatus");
        if (component.get('v.isError') === false) {
            $A.util.addClass(component.find('typeDiv'), 'slds-theme--success');
            component.set("v.message", userMsg)
        } else {
            $A.util.addClass(component.find('typeDiv'), 'slds-theme--error');
            component.set("v.message", userMsg);
        }
    },
    closeToast: function (component, event, helper) {
        helper.closeToast(component);
    }
});