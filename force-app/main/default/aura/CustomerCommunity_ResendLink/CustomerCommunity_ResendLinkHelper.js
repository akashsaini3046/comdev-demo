({
	sendRegistrationLink : function(component,event) {
        var userN = component.get("v.userName");
        if (userN == "") {
            component.set("v.Message", "Fill in username to search for your account.");
            component.set("v.showMessage", true);
        } else {
            var action = component.get("c.sentRegistrationEmail");
            action.setParams({
                "username": userN
            });
            action.setCallback(this, function (a) {
                var state = a.getState();
                if (state === "SUCCESS") {
                    if (a.getReturnValue() == "True") {
                        component.set("v.Message", "Link has been send Successfully !.");
                        component.set("v.showMessage", true);
                        component.set("v.closeModalLink", true);
                    } else if (a.getReturnValue() == "False") {
                        component.set("v.showMessage", true);
                        component.set("v.Message", "Please check your username. If you still face issues, contact your Crowley administrator");
                    } else if (a.getReturnValue() == "Error") {
                        component.set("v.Message", "Something went wrong. Please contact Crowley Administrator.");
                        component.set("v.showMessage", true);
                    }
                } else {
                    component.set("v.Message", "Something went wrong. Please contact Crowley Administrator.");
                    component.set("v.showMessage", true);
                }
            });
       $A.enqueueAction(action);
	}
   }
})