({
    handleClose : function(component, event, helper) {
        var closeModalEvent = $A.get("e.c:CustomerCommunity_RegistrationLinkEvent");
        closeModalEvent.setParams({
            "closeModal": true
        });
        closeModalEvent.fire();
    },
    closeModal : function(component,event,helper){
        var closeEvent = $A.get("e.c:CustomerCommunity_CloseResendLink");
        closeEvent.setParams({
            "isLinkModalVisible": true
        });
        closeEvent.fire(); 
    },
    sendRegistrationLink : function(component,event,helper){
        helper.sendRegistrationLink(component,event);
    }
})