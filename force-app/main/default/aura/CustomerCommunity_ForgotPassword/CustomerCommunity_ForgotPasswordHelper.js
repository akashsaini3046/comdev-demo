({
    resetPassword: function (component) {
        var userN = component.get("v.userName");
        if (userN == "") {
            component.set("v.Message", "Fill in username to search for your account.");
            component.set("v.showMessage", true);
        } else {
            var action = component.get("c.usernameExists");
            action.setParams({
                "username": userN
            });
            action.setCallback(this, function (a) {
                var state = a.getState();
                if (state === "SUCCESS") {
                    if (a.getReturnValue() == "True") {
                        component.set("v.Message", "Please reset the password using the link sent to your registered email id.");
                        component.set("v.showMessage", true);
                        component.set("v.closeModalPassword", true);
                    } else if (a.getReturnValue() == "False") {
                        component.set("v.showMessage", true);
                        component.set("v.Message", "Please check your username. If you still can't log in, contact your Crowley administrator");
                    } else if (a.getReturnValue() == "Error") {
                        component.set("v.Message", "Something went wrong. Please contact Crowley Administrator.");
                        component.set("v.showMessage", true);
                    }
                } else {
                    component.set("v.Message", "Something went wrong. Please contact Crowley Administrator.");
                    component.set("v.showMessage", true);
                }
            });
            $A.enqueueAction(action);
        }
    }
})