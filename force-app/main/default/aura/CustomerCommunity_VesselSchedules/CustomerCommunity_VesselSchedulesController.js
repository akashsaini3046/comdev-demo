({
    doInit : function(component, event, helper) {
        component.find("arrivalDate").set("v.disabled", true);
        component.find("depDateCheck").set("v.checked",true);
        helper.formatDate(component);
        helper.getPortOfLoadingAndDischarge(component);
        helper.setTodayDate(component);
        helper.fireHighlightEvent(component, event);
    },                
    setDepartureDate: function(component, event, helper) {
        helper.setTodayDate(component);      
        component.find("depDateCheck").set("v.checked",true);
        component.find("arrivalDateCheck").set("v.checked",false);
        //component.set("v.depDateValue",true);
        //component.set("v.arrivalDateValue",false);
        component.set("v.arrivalToday","");
        component.find("depDate").set("v.disabled", false);
        component.find("arrivalDate").set("v.disabled", true); 
    },
    setArrivalDate: function(component, event, helper) {
        component.find("depDate").set("v.disabled", true);
        component.find("arrivalDate").set("v.disabled", false);        
        component.find("depDateCheck").set("v.checked",false);
        component.find("arrivalDateCheck").set("v.checked",true);
        //component.set("v.depDateValue",false);
        //component.set("v.arrivalDateValue",true);
        component.set("v.depToday","");
    },
    showSchedules: function(component, event,helper){
        helper.getVesselScheduleList(component);
    },
    clearForm : function(component, event,helper) {
        var portList = component.get("v.portOfLoadingAndDischarge");
        component.set("v.portOfLoadingAndDischarge","");
        component.find("loadingPortId").set("v.value","");
        component.find("dischargePortId").set("v.value","1 Week");
        component.find("rangeId").set("v.value","");
        component.set("v.portOfLoadingAndDischarge",portList);
        component.find("arrivalDate").set("v.disabled", true);
        component.set("v.arrivalToday","");
        component.find("depDateCheck").set("v.value",true);
        component.find("arrivalDateCheck").set("v.value",false);
        component.find("depDate").set("v.disabled", false);
        component.set("v.scheduleList",null);
        component.set("v.showErrorMessage",false);
        component.set("v.showAPIErrorMessage", false);
        helper.getPortOfLoadingAndDischarge(component);
        helper.setTodayDate(component);
    },
    printResults: function(component, event, helper){
        window.print();
    }
})