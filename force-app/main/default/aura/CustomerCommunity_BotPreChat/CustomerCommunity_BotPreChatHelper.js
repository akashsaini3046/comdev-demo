({
    startChat: function(component, event, helper){
        var fields = [
            {
                label: 'FirstName',
                name: 'FirstName',
                value: component.get('v.firstName')
            } ,
            {
                label: 'LastName',
                name: 'LastName',
                value: component.get('v.lastName')
            }  ,
            {
                label: 'Email',
                name: 'Email',
                value: component.get('v.email')
            },{
                label: 'Username',
                name: 'Username__c',
                value: component.get('v.userId'),
            },{
                label: 'Language',
                name: 'Language__c',
                value: component.get('v.language'),
            }
        ];
        console.log("fields", fields);
        if(component.find("prechatAPI").validateFields(fields).valid) {
            component.find("prechatAPI").startChat(fields);
            
        }
    }
})