({
	FetchListItems : function(component){
        var action = component.get("c.getMenuItems");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.listitems", a.getReturnValue());
                
            } 
        });
        $A.enqueueAction(action);
    },
    highlightListItems : function(component, event){
        var cmpEvent = component.getEvent("highlightMenuOption");
        var selecteditem = event.currentTarget.id;
        cmpEvent.setParams({"selectedMenu" : selecteditem}); 
        cmpEvent.fire(); 
        document.getElementById(selecteditem).setAttribute("aria-current","page");
         /* console.log('@@@ evt - ');
        //Create component dynamically
        $A.createComponent(
            "c:CustomerCommunity_DashboardScreen_1",{},
            function(newcomponent){
                if (component.isValid()) {
                    var body = component.get("v.body");
                    body.push(newcomponent);
                    component.set("v.body", []);
                    component.set("v.body", body);             
                }
            }            
        );
        var selecteditem = event.currentTarget.id;
        selecteditem = "c:"+selecteditem+"Screen";
        selecteditem="c:CustomerCommunity_HomeScreen_1";
        console.log('@@@ selecteditem - ' + selecteditem);
        
        var evt = $A.get("e.force:navigateToComponent");
       
        evt.setParams({
            componentDef : selecteditem
        });
        evt.fire();
         console.log('@@@ evt - ' + json.stringify(evt));
        
        
        var menuItem = event.getParam("selectedMenu");
        var action = component.get("c.getMenuItems");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                var menuOptions = a.getReturnValue();
                component.set("v.listitems", a.getReturnValue());
                var i=0; var highlightSequence = 0;
                for(i=0; i<menuOptions.length; i++) {
                    if(menuOptions[i]['Lightning_Component_Name__c'] == menuItem) {
                        highlightSequence = i+1;
                    }
                }
                if(highlightSequence != 0) {
                    component.set("v.highlightedItem",highlightSequence);
                }
            } 
        });
        $A.enqueueAction(action); */
       
    }
})