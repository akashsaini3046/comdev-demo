({
    getColumnOneItem: function (component, event) {
        var action = component.get("c.queryColumnOneValues");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseServer = response.getReturnValue();
                component.set("v.footerColumnOne", responseServer);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    getColumnTwoItem: function (component, event) {
        var action = component.get("c.queryColumnTwoValues");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var responseServer = response.getReturnValue();
                component.set("v.footerColumnTwo", responseServer);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    getColumnThreeItem: function (component, event) {
        var action = component.get("c.queryColumnThreeValues");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var responseServer = response.getReturnValue();
                component.set("v.footerColumnThree", responseServer);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    }
});