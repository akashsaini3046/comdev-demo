({
    doInit: function (component, event, helper) {
        helper.createWhatsAppIDArray(component);
        helper.createSmsIDArray(component);
        helper.setUserDetails(component);
    },
    addWhatsAppId: function (component, event, helper) {
        helper.createWhatsAppIDElement(component);
    },
    removeWhatsAppElement: function (component, event, helper) {
        helper.removeWhatsAppIDElement(component, event);
    },
    removeSmsElement: function (component, event, helper) {
        helper.removeSmsIDElement(component, event);
    },
    addSmsId: function (component, event, helper) {
        helper.createSmsIDElement(component, event);
    },
    updateUserDetails: function (component, event, helper) {
        helper.updateUserDetails(component, event);
    },
    clearDetails: function (component, event, helper) {
        helper.clearDetails(component, event);
    }
})