({
    doInit: function (component, event, helper) {
        window.localStorage.clear();
        window.sessionStorage.clear();
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.readyDate', today);
        component.set('v.selectedMovementTypeOrigin','P');
        component.set('v.selectedMovementTypeDestination','P');
       /* helper.getZipCountries(component, event, helper);
        helper.getPortCountries(component, event, helper);
        helper.getRailCountries(component, event, helper);*/
    },
    handleOriginMovementTypeChange: function (component, event, helper){
        console.log('Entered handleOriginMovementTypeChange');
        component.set("v.showErrorMessage",false);
        component.set("v.numberOfSchedules",0);
        var selectedMovementTypeOrigin = component.get("v.selectedMovementTypeOrigin");
        
		component.set("v.originCountriesList",null);
        component.set("v.selectedOriginCountry",null);
        component.set("v.selectedOriginLocation",null);
        component.set("v.originCode",null);
        component.set("v.selectedOriginSublocation",null);
		component.set("v.selectedOriginReceiptTerm",null);
        
        
        if(selectedMovementTypeOrigin === 'D'){
            component.set("v.displayOriginSublocation",false);  
            component.set("v.displayOriginReceiptType",true);           
            var receiptTerms = [];
            receiptTerms.push({value:'',label:'None'});
            receiptTerms.push({value:'A',label:'All Motor'});
            component.set("v.originReceiptTerms",receiptTerms);
            component.set("v.defaultOriginReceiptType",'');
            component.set("v.selectedOriginReceiptTerm",'');
           // component.set("v.originCountriesList",component.get("v.zipCountries"));
            
        }
        
        if(selectedMovementTypeOrigin === 'P'){
            
            component.set("v.displayOriginReceiptType",false);
            component.set("v.displayOriginSublocation",false); 
           
          //  component.set("v.originCountriesList",component.get("v.portCountries"));
           
        }
        
        if(selectedMovementTypeOrigin === 'R'){
            component.set("v.displayOriginReceiptType",true);
            component.set("v.displayOriginSublocation",false);
            var receiptTerms = [];
            receiptTerms.push({value:'M',label:'Motor/Rail'});
            component.set("v.originReceiptTerms",receiptTerms);
            component.set("v.defaultOriginReceiptType",'M');
          //  component.set("v.originCountriesList",component.get("v.railCountries"));
        }
        
       /* var action = component.get("c.getLocations");
        action.setParams({
            type : selectedMovementTypeOrigin
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                var zipLocations = response.getReturnValue();
                component.set("v.zipLocations",zipLocations);
                var originCountriesList =[]
                for(var i in zipLocations){
                    originCountriesList.push(zipLocations[i].Country_Name__c);
                }
                var originCountriesSet = new Set(originCountriesList);
                
                component.set("v.originCountriesList",originCountriesSet);
                console.log(component.get("v.originCountriesList"));
            }
            else{
                console.log('Error occured in handleOriginMovementTypeChange');
            }
        });
        $A.enqueueAction(action);  */
    },
    handleDestinationMovementTypeChange: function (component, event, helper){
        console.log('Entered handleDestinationMovementTypeChange');
        component.set("v.showErrorMessage",false);
        component.set("v.numberOfSchedules",0);
        var selectedMovementTypeDestination = component.get("v.selectedMovementTypeDestination");
        
        component.set("v.destinationCountriesList",null);
        component.set("v.selectedDestinationCountry",null);
        component.set("v.selectedDestinationLocation",null);
        component.set("v.destinationCode",null);
        component.set("v.selectedDestinationSublocation",null);
		component.set("v.selectedDestinationReceiptTerm",null);
        
        if(selectedMovementTypeDestination === 'D'){
            component.set("v.displayDestinationSublocation",false);   
            component.set("v.displayDestinationReceiptType",true);
            var receiptTerms = [];
            receiptTerms.push({value:'',label:'None'});
            receiptTerms.push({value:'A',label:'All Motor'});
            component.set("v.destinationReceiptTerms",receiptTerms);           
            component.set("v.defaultDestinationReceiptType",'');
            component.set("v.selectedOriginReceiptTerm",'');
          //  component.set("v.destinationCountriesList",component.get("v.zipCountries"));
        }
        
        if(selectedMovementTypeDestination === 'P'){
            component.set("v.displayDestinationReceiptType",false);
            component.set("v.displayDestinationSublocation",false); 
          //  component.set("v.destinationCountriesList",component.get("v.portCountries"));
        }
        
        if(selectedMovementTypeDestination === 'R'){
            component.set("v.displayDestinationReceiptType",true);
            component.set("v.displayDestinationSublocation",false);
            var receiptTerms = [];
            receiptTerms.push({value:'M',label:'Motor/Rail'});
            component.set("v.destinationReceiptTerms",receiptTerms);
            component.set("v.defaultDestinationReceiptType",'M');
         //   component.set("v.destinationCountriesList",component.get("v.railCountries"));
        }
        /* var action = component.get("c.getLocations");
        action.setParams({
            type : selectedMovementTypeDestination
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                var locations = response.getReturnValue();
                component.set("v.zipLocations",locations);
                var destinationCountriesList =[]
                for(var i in locations){
                    destinationCountriesList.push(locations[i].Country_Name__c);
                }
                var destinationCountriesSet = new Set(destinationCountriesList);
                
                component.set("v.destinationCountriesList",destinationCountriesSet);
                console.log(component.get("v.destinationCountriesList"));
            }
            else{
                console.log('Error occured in handleDestinationMovementTypeChange');
            }
        });
        $A.enqueueAction(action); */
    },
    handleOriginCountryChange : function(component,event,helper){
        console.log('hello ,, I am here');
        component.set("v.showErrorMessage",false);
        component.set("v.numberOfSchedules",0);
        component.set("v.selectedOriginLocation",null);
    },
    handleComponentEvent : function(component,event,helper){
        component.set("v.showErrorMessage",false);
        component.set("v.numberOfSchedules",0);
        helper.handleComponentEvent(component,event,helper);
    },
    searchFindRoute : function(component,event,helper){
        helper.searchFindRoute(component,event,helper);
    },
    handleClearSearchEvent : function(component,event,helper){
        component.set("v.showErrorMessage",false);
        component.set("v.numberOfSchedules",0);
        helper.handleClearSearchEvent(component,event,helper);
    },
    
})