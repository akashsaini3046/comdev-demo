({
    newBooking : function(component, event, helper) {
        component.set("v.isCreateBooking",true);
    },
    copyBookingFrom : function(component, event, helper) {       
        helper.copyBookingFrom(component, event);
    }
})