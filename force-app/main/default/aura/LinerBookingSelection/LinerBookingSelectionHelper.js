({
    copyBookingFrom : function(component, event) {
        var action = component.get("c.getBookingFromNumber");        
       var bookingNo = document.getElementById("copyBookingFromId").value;
       alert(bookingNo);
        action.setParams({
            bookingNumber:bookingNo
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null) {  
                var responseData = response.getReturnValue();  
                
                if(responseData.Id!=null){
                    component.set("v.message",'');
                    component.set("v.bookingId",responseData.Id);
                    component.set("v.isCreateBooking",true);
                }
                component.set("v.message",'No Booking Found');
            }
            else{
                component.set("v.contactRecordDetail",null);
                component.set("v.isCreateBooking",false);
                component.set("v.message",'No Booking Found');
            } 
        });
        $A.enqueueAction(action);
    }
})